// Karma configuration
// http://karma-runner.github.io/0.10/config/configuration-file.html

module.exports = function(config) {
  config.set({
    // base path, that will be used to resolve files and exclude
    basePath: '',

    // testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ['jasmine'],

    // list of files / patterns to load in the browser
    files: [
      'app/bower_components/jquery/jquery.js',
      'app/bower_components/angular/angular.js',
      'app/bower_components/angular-mocks/angular-mocks.js',
      'app/bower_components/angular-bootstrap/ui-bootstrap.js',
      'app/bower_components/underscore/underscore.js',
      'app/bower_components/angular-animate/angular-animate.js',
      'app/bower_components/angular-resource/angular-resource.js',
      'app/bower_components/angular-cookies/angular-cookies.js',
      'app/bower_components/angular-sanitize/angular-sanitize.js',
      'app/bower_components/angular-route/angular-route.js',
      'app/bower_components/angular-raven/angular-raven.js ',
      'app/bower_components/ng-file-upload/angular-file-upload.js',
      'app/bower_components/angulartics/src/angulartics.js',
      'app/bower_components/angulartics/src/angulartics-ga-cordova.js',
      'app/bower_components/angulartics/src/angulartics-ga.js',
      'app/bower_components/angular-pdf/dist/angular-pdf.js',
      'app/bower_components/angular-local-storage/dist/angular-local-storage.js',
      'app/bower_components/angular-touch/angular-touch.js',
      'app/bower_components/CryptoJS/build/rollups/md5.js',
      'app/bower_components/CryptoJS/build/components/enc-base64.js',
      'app/scripts/**/*.js',
      'app/scripts/*.js',
      'app/components/**/*.js',
      'app/components/**/*.html',
      'test/**/*.js',
      'app/styles/plugins/bootstrap.min.css',
      'app/styles/plugins/normalize.css',
      'app/styles/plugins/grid.css',
      'app/styles/plugins/intlTelInput.css',
      'app/styles/main.css'
    ],

    // list of files / patterns to exclude
    exclude: [],

    // web server port
    port: 8080,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: ['PhantomJS'],

    preprocessors: {
      '**/*.html': ['ng-html2js']
    },

    ngHtml2JsPreprocessor: {
      // removes "app/" from the created $templateCache entries, making the templates available to unit tests
      stripPrefix: "app/",

      // the name of the Angular module to create
      moduleName: "cvc.templates"
    },

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false
  });
};

/*!
 * Bootstrap v3.1.1 (http://getbootstrap.com)
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */
if("undefined"==typeof jQuery)throw new Error("Bootstrap's JavaScript requires jQuery");+function(a){"use strict";function b(){var a=document.createElement("bootstrap"),b={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};for(var c in b)if(void 0!==a.style[c])return{end:b[c]};return!1}a.fn.emulateTransitionEnd=function(b){var c=!1,d=this;a(this).one(a.support.transition.end,function(){c=!0});var e=function(){c||a(d).trigger(a.support.transition.end)};return setTimeout(e,b),this},a(function(){a.support.transition=b()})}(jQuery),+function(a){"use strict";var b='[data-dismiss="alert"]',c=function(c){a(c).on("click",b,this.close)};c.prototype.close=function(b){function c(){f.trigger("closed.bs.alert").remove()}var d=a(this),e=d.attr("data-target");e||(e=d.attr("href"),e=e&&e.replace(/.*(?=#[^\s]*$)/,""));var f=a(e);b&&b.preventDefault(),f.length||(f=d.hasClass("alert")?d:d.parent()),f.trigger(b=a.Event("close.bs.alert")),b.isDefaultPrevented()||(f.removeClass("in"),a.support.transition&&f.hasClass("fade")?f.one(a.support.transition.end,c).emulateTransitionEnd(150):c())};var d=a.fn.alert;a.fn.alert=function(b){return this.each(function(){var d=a(this),e=d.data("bs.alert");e||d.data("bs.alert",e=new c(this)),"string"==typeof b&&e[b].call(d)})},a.fn.alert.Constructor=c,a.fn.alert.noConflict=function(){return a.fn.alert=d,this},a(document).on("click.bs.alert.data-api",b,c.prototype.close)}(jQuery),+function(a){"use strict";var b=function(c,d){this.$element=a(c),this.options=a.extend({},b.DEFAULTS,d),this.isLoading=!1};b.DEFAULTS={loadingText:"loading..."},b.prototype.setState=function(b){var c="disabled",d=this.$element,e=d.is("input")?"val":"html",f=d.data();b+="Text",f.resetText||d.data("resetText",d[e]()),d[e](f[b]||this.options[b]),setTimeout(a.proxy(function(){"loadingText"==b?(this.isLoading=!0,d.addClass(c).attr(c,c)):this.isLoading&&(this.isLoading=!1,d.removeClass(c).removeAttr(c))},this),0)},b.prototype.toggle=function(){var a=!0,b=this.$element.closest('[data-toggle="buttons"]');if(b.length){var c=this.$element.find("input");"radio"==c.prop("type")&&(c.prop("checked")&&this.$element.hasClass("active")?a=!1:b.find(".active").removeClass("active")),a&&c.prop("checked",!this.$element.hasClass("active")).trigger("change")}a&&this.$element.toggleClass("active")};var c=a.fn.button;a.fn.button=function(c){return this.each(function(){var d=a(this),e=d.data("bs.button"),f="object"==typeof c&&c;e||d.data("bs.button",e=new b(this,f)),"toggle"==c?e.toggle():c&&e.setState(c)})},a.fn.button.Constructor=b,a.fn.button.noConflict=function(){return a.fn.button=c,this},a(document).on("click.bs.button.data-api","[data-toggle^=button]",function(b){var c=a(b.target);c.hasClass("btn")||(c=c.closest(".btn")),c.button("toggle"),b.preventDefault()})}(jQuery),+function(a){"use strict";var b=function(b,c){this.$element=a(b),this.$indicators=this.$element.find(".carousel-indicators"),this.options=c,this.paused=this.sliding=this.interval=this.$active=this.$items=null,"hover"==this.options.pause&&this.$element.on("mouseenter",a.proxy(this.pause,this)).on("mouseleave",a.proxy(this.cycle,this))};b.DEFAULTS={interval:5e3,pause:"hover",wrap:!0},b.prototype.cycle=function(b){return b||(this.paused=!1),this.interval&&clearInterval(this.interval),this.options.interval&&!this.paused&&(this.interval=setInterval(a.proxy(this.next,this),this.options.interval)),this},b.prototype.getActiveIndex=function(){return this.$active=this.$element.find(".item.active"),this.$items=this.$active.parent().children(),this.$items.index(this.$active)},b.prototype.to=function(b){var c=this,d=this.getActiveIndex();return b>this.$items.length-1||0>b?void 0:this.sliding?this.$element.one("slid.bs.carousel",function(){c.to(b)}):d==b?this.pause().cycle():this.slide(b>d?"next":"prev",a(this.$items[b]))},b.prototype.pause=function(b){return b||(this.paused=!0),this.$element.find(".next, .prev").length&&a.support.transition&&(this.$element.trigger(a.support.transition.end),this.cycle(!0)),this.interval=clearInterval(this.interval),this},b.prototype.next=function(){return this.sliding?void 0:this.slide("next")},b.prototype.prev=function(){return this.sliding?void 0:this.slide("prev")},b.prototype.slide=function(b,c){var d=this.$element.find(".item.active"),e=c||d[b](),f=this.interval,g="next"==b?"left":"right",h="next"==b?"first":"last",i=this;if(!e.length){if(!this.options.wrap)return;e=this.$element.find(".item")[h]()}if(e.hasClass("active"))return this.sliding=!1;var j=a.Event("slide.bs.carousel",{relatedTarget:e[0],direction:g});return this.$element.trigger(j),j.isDefaultPrevented()?void 0:(this.sliding=!0,f&&this.pause(),this.$indicators.length&&(this.$indicators.find(".active").removeClass("active"),this.$element.one("slid.bs.carousel",function(){var b=a(i.$indicators.children()[i.getActiveIndex()]);b&&b.addClass("active")})),a.support.transition&&this.$element.hasClass("slide")?(e.addClass(b),e[0].offsetWidth,d.addClass(g),e.addClass(g),d.one(a.support.transition.end,function(){e.removeClass([b,g].join(" ")).addClass("active"),d.removeClass(["active",g].join(" ")),i.sliding=!1,setTimeout(function(){i.$element.trigger("slid.bs.carousel")},0)}).emulateTransitionEnd(1e3*d.css("transition-duration").slice(0,-1))):(d.removeClass("active"),e.addClass("active"),this.sliding=!1,this.$element.trigger("slid.bs.carousel")),f&&this.cycle(),this)};var c=a.fn.carousel;a.fn.carousel=function(c){return this.each(function(){var d=a(this),e=d.data("bs.carousel"),f=a.extend({},b.DEFAULTS,d.data(),"object"==typeof c&&c),g="string"==typeof c?c:f.slide;e||d.data("bs.carousel",e=new b(this,f)),"number"==typeof c?e.to(c):g?e[g]():f.interval&&e.pause().cycle()})},a.fn.carousel.Constructor=b,a.fn.carousel.noConflict=function(){return a.fn.carousel=c,this},a(document).on("click.bs.carousel.data-api","[data-slide], [data-slide-to]",function(b){var c,d=a(this),e=a(d.attr("data-target")||(c=d.attr("href"))&&c.replace(/.*(?=#[^\s]+$)/,"")),f=a.extend({},e.data(),d.data()),g=d.attr("data-slide-to");g&&(f.interval=!1),e.carousel(f),(g=d.attr("data-slide-to"))&&e.data("bs.carousel").to(g),b.preventDefault()}),a(window).on("load",function(){a('[data-ride="carousel"]').each(function(){var b=a(this);b.carousel(b.data())})})}(jQuery),+function(a){"use strict";var b=function(c,d){this.$element=a(c),this.options=a.extend({},b.DEFAULTS,d),this.transitioning=null,this.options.parent&&(this.$parent=a(this.options.parent)),this.options.toggle&&this.toggle()};b.DEFAULTS={toggle:!0},b.prototype.dimension=function(){var a=this.$element.hasClass("width");return a?"width":"height"},b.prototype.show=function(){if(!this.transitioning&&!this.$element.hasClass("in")){var b=a.Event("show.bs.collapse");if(this.$element.trigger(b),!b.isDefaultPrevented()){var c=this.$parent&&this.$parent.find("> .panel > .in");if(c&&c.length){var d=c.data("bs.collapse");if(d&&d.transitioning)return;c.collapse("hide"),d||c.data("bs.collapse",null)}var e=this.dimension();this.$element.removeClass("collapse").addClass("collapsing")[e](0),this.transitioning=1;var f=function(){this.$element.removeClass("collapsing").addClass("collapse in")[e]("auto"),this.transitioning=0,this.$element.trigger("shown.bs.collapse")};if(!a.support.transition)return f.call(this);var g=a.camelCase(["scroll",e].join("-"));this.$element.one(a.support.transition.end,a.proxy(f,this)).emulateTransitionEnd(350)[e](this.$element[0][g])}}},b.prototype.hide=function(){if(!this.transitioning&&this.$element.hasClass("in")){var b=a.Event("hide.bs.collapse");if(this.$element.trigger(b),!b.isDefaultPrevented()){var c=this.dimension();this.$element[c](this.$element[c]())[0].offsetHeight,this.$element.addClass("collapsing").removeClass("collapse").removeClass("in"),this.transitioning=1;var d=function(){this.transitioning=0,this.$element.trigger("hidden.bs.collapse").removeClass("collapsing").addClass("collapse")};return a.support.transition?void this.$element[c](0).one(a.support.transition.end,a.proxy(d,this)).emulateTransitionEnd(350):d.call(this)}}},b.prototype.toggle=function(){this[this.$element.hasClass("in")?"hide":"show"]()};var c=a.fn.collapse;a.fn.collapse=function(c){return this.each(function(){var d=a(this),e=d.data("bs.collapse"),f=a.extend({},b.DEFAULTS,d.data(),"object"==typeof c&&c);!e&&f.toggle&&"show"==c&&(c=!c),e||d.data("bs.collapse",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.collapse.Constructor=b,a.fn.collapse.noConflict=function(){return a.fn.collapse=c,this},a(document).on("click.bs.collapse.data-api","[data-toggle=collapse]",function(b){var c,d=a(this),e=d.attr("data-target")||b.preventDefault()||(c=d.attr("href"))&&c.replace(/.*(?=#[^\s]+$)/,""),f=a(e),g=f.data("bs.collapse"),h=g?"toggle":d.data(),i=d.attr("data-parent"),j=i&&a(i);g&&g.transitioning||(j&&j.find('[data-toggle=collapse][data-parent="'+i+'"]').not(d).addClass("collapsed"),d[f.hasClass("in")?"addClass":"removeClass"]("collapsed")),f.collapse(h)})}(jQuery),+function(a){"use strict";function b(b){a(d).remove(),a(e).each(function(){var d=c(a(this)),e={relatedTarget:this};d.hasClass("open")&&(d.trigger(b=a.Event("hide.bs.dropdown",e)),b.isDefaultPrevented()||d.removeClass("open").trigger("hidden.bs.dropdown",e))})}function c(b){var c=b.attr("data-target");c||(c=b.attr("href"),c=c&&/#[A-Za-z]/.test(c)&&c.replace(/.*(?=#[^\s]*$)/,""));var d=c&&a(c);return d&&d.length?d:b.parent()}var d=".dropdown-backdrop",e="[data-toggle=dropdown]",f=function(b){a(b).on("click.bs.dropdown",this.toggle)};f.prototype.toggle=function(d){var e=a(this);if(!e.is(".disabled, :disabled")){var f=c(e),g=f.hasClass("open");if(b(),!g){"ontouchstart"in document.documentElement&&!f.closest(".navbar-nav").length&&a('<div class="dropdown-backdrop"/>').insertAfter(a(this)).on("click",b);var h={relatedTarget:this};if(f.trigger(d=a.Event("show.bs.dropdown",h)),d.isDefaultPrevented())return;f.toggleClass("open").trigger("shown.bs.dropdown",h),e.focus()}return!1}},f.prototype.keydown=function(b){if(/(38|40|27)/.test(b.keyCode)){var d=a(this);if(b.preventDefault(),b.stopPropagation(),!d.is(".disabled, :disabled")){var f=c(d),g=f.hasClass("open");if(!g||g&&27==b.keyCode)return 27==b.which&&f.find(e).focus(),d.click();var h=" li:not(.divider):visible a",i=f.find("[role=menu]"+h+", [role=listbox]"+h);if(i.length){var j=i.index(i.filter(":focus"));38==b.keyCode&&j>0&&j--,40==b.keyCode&&j<i.length-1&&j++,~j||(j=0),i.eq(j).focus()}}}};var g=a.fn.dropdown;a.fn.dropdown=function(b){return this.each(function(){var c=a(this),d=c.data("bs.dropdown");d||c.data("bs.dropdown",d=new f(this)),"string"==typeof b&&d[b].call(c)})},a.fn.dropdown.Constructor=f,a.fn.dropdown.noConflict=function(){return a.fn.dropdown=g,this},a(document).on("click.bs.dropdown.data-api",b).on("click.bs.dropdown.data-api",".dropdown form",function(a){a.stopPropagation()}).on("click.bs.dropdown.data-api",e,f.prototype.toggle).on("keydown.bs.dropdown.data-api",e+", [role=menu], [role=listbox]",f.prototype.keydown)}(jQuery),+function(a){"use strict";var b=function(b,c){this.options=c,this.$element=a(b),this.$backdrop=this.isShown=null,this.options.remote&&this.$element.find(".modal-content").load(this.options.remote,a.proxy(function(){this.$element.trigger("loaded.bs.modal")},this))};b.DEFAULTS={backdrop:!0,keyboard:!0,show:!0},b.prototype.toggle=function(a){return this[this.isShown?"hide":"show"](a)},b.prototype.show=function(b){var c=this,d=a.Event("show.bs.modal",{relatedTarget:b});this.$element.trigger(d),this.isShown||d.isDefaultPrevented()||(this.isShown=!0,this.escape(),this.$element.on("click.dismiss.bs.modal",'[data-dismiss="modal"]',a.proxy(this.hide,this)),this.backdrop(function(){var d=a.support.transition&&c.$element.hasClass("fade");c.$element.parent().length||c.$element.appendTo(document.body),c.$element.show().scrollTop(0),d&&c.$element[0].offsetWidth,c.$element.addClass("in").attr("aria-hidden",!1),c.enforceFocus();var e=a.Event("shown.bs.modal",{relatedTarget:b});d?c.$element.find(".modal-dialog").one(a.support.transition.end,function(){c.$element.focus().trigger(e)}).emulateTransitionEnd(300):c.$element.focus().trigger(e)}))},b.prototype.hide=function(b){b&&b.preventDefault(),b=a.Event("hide.bs.modal"),this.$element.trigger(b),this.isShown&&!b.isDefaultPrevented()&&(this.isShown=!1,this.escape(),a(document).off("focusin.bs.modal"),this.$element.removeClass("in").attr("aria-hidden",!0).off("click.dismiss.bs.modal"),a.support.transition&&this.$element.hasClass("fade")?this.$element.one(a.support.transition.end,a.proxy(this.hideModal,this)).emulateTransitionEnd(300):this.hideModal())},b.prototype.enforceFocus=function(){a(document).off("focusin.bs.modal").on("focusin.bs.modal",a.proxy(function(a){this.$element[0]===a.target||this.$element.has(a.target).length||this.$element.focus()},this))},b.prototype.escape=function(){this.isShown&&this.options.keyboard?this.$element.on("keyup.dismiss.bs.modal",a.proxy(function(a){27==a.which&&this.hide()},this)):this.isShown||this.$element.off("keyup.dismiss.bs.modal")},b.prototype.hideModal=function(){var a=this;this.$element.hide(),this.backdrop(function(){a.removeBackdrop(),a.$element.trigger("hidden.bs.modal")})},b.prototype.removeBackdrop=function(){this.$backdrop&&this.$backdrop.remove(),this.$backdrop=null},b.prototype.backdrop=function(b){var c=this.$element.hasClass("fade")?"fade":"";if(this.isShown&&this.options.backdrop){var d=a.support.transition&&c;if(this.$backdrop=a('<div class="modal-backdrop '+c+'" />').appendTo(document.body),this.$element.on("click.dismiss.bs.modal",a.proxy(function(a){a.target===a.currentTarget&&("static"==this.options.backdrop?this.$element[0].focus.call(this.$element[0]):this.hide.call(this))},this)),d&&this.$backdrop[0].offsetWidth,this.$backdrop.addClass("in"),!b)return;d?this.$backdrop.one(a.support.transition.end,b).emulateTransitionEnd(150):b()}else!this.isShown&&this.$backdrop?(this.$backdrop.removeClass("in"),a.support.transition&&this.$element.hasClass("fade")?this.$backdrop.one(a.support.transition.end,b).emulateTransitionEnd(150):b()):b&&b()};var c=a.fn.modal;a.fn.modal=function(c,d){return this.each(function(){var e=a(this),f=e.data("bs.modal"),g=a.extend({},b.DEFAULTS,e.data(),"object"==typeof c&&c);f||e.data("bs.modal",f=new b(this,g)),"string"==typeof c?f[c](d):g.show&&f.show(d)})},a.fn.modal.Constructor=b,a.fn.modal.noConflict=function(){return a.fn.modal=c,this},a(document).on("click.bs.modal.data-api",'[data-toggle="modal"]',function(b){var c=a(this),d=c.attr("href"),e=a(c.attr("data-target")||d&&d.replace(/.*(?=#[^\s]+$)/,"")),f=e.data("bs.modal")?"toggle":a.extend({remote:!/#/.test(d)&&d},e.data(),c.data());c.is("a")&&b.preventDefault(),e.modal(f,this).one("hide",function(){c.is(":visible")&&c.focus()})}),a(document).on("show.bs.modal",".modal",function(){a(document.body).addClass("modal-open")}).on("hidden.bs.modal",".modal",function(){a(document.body).removeClass("modal-open")})}(jQuery),+function(a){"use strict";var b=function(a,b){this.type=this.options=this.enabled=this.timeout=this.hoverState=this.$element=null,this.init("tooltip",a,b)};b.DEFAULTS={animation:!0,placement:"top",selector:!1,template:'<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,container:!1},b.prototype.init=function(b,c,d){this.enabled=!0,this.type=b,this.$element=a(c),this.options=this.getOptions(d);for(var e=this.options.trigger.split(" "),f=e.length;f--;){var g=e[f];if("click"==g)this.$element.on("click."+this.type,this.options.selector,a.proxy(this.toggle,this));else if("manual"!=g){var h="hover"==g?"mouseenter":"focusin",i="hover"==g?"mouseleave":"focusout";this.$element.on(h+"."+this.type,this.options.selector,a.proxy(this.enter,this)),this.$element.on(i+"."+this.type,this.options.selector,a.proxy(this.leave,this))}}this.options.selector?this._options=a.extend({},this.options,{trigger:"manual",selector:""}):this.fixTitle()},b.prototype.getDefaults=function(){return b.DEFAULTS},b.prototype.getOptions=function(b){return b=a.extend({},this.getDefaults(),this.$element.data(),b),b.delay&&"number"==typeof b.delay&&(b.delay={show:b.delay,hide:b.delay}),b},b.prototype.getDelegateOptions=function(){var b={},c=this.getDefaults();return this._options&&a.each(this._options,function(a,d){c[a]!=d&&(b[a]=d)}),b},b.prototype.enter=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs."+this.type);return clearTimeout(c.timeout),c.hoverState="in",c.options.delay&&c.options.delay.show?void(c.timeout=setTimeout(function(){"in"==c.hoverState&&c.show()},c.options.delay.show)):c.show()},b.prototype.leave=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs."+this.type);return clearTimeout(c.timeout),c.hoverState="out",c.options.delay&&c.options.delay.hide?void(c.timeout=setTimeout(function(){"out"==c.hoverState&&c.hide()},c.options.delay.hide)):c.hide()},b.prototype.show=function(){var b=a.Event("show.bs."+this.type);if(this.hasContent()&&this.enabled){if(this.$element.trigger(b),b.isDefaultPrevented())return;var c=this,d=this.tip();this.setContent(),this.options.animation&&d.addClass("fade");var e="function"==typeof this.options.placement?this.options.placement.call(this,d[0],this.$element[0]):this.options.placement,f=/\s?auto?\s?/i,g=f.test(e);g&&(e=e.replace(f,"")||"top"),d.detach().css({top:0,left:0,display:"block"}).addClass(e),this.options.container?d.appendTo(this.options.container):d.insertAfter(this.$element);var h=this.getPosition(),i=d[0].offsetWidth,j=d[0].offsetHeight;if(g){var k=this.$element.parent(),l=e,m=document.documentElement.scrollTop||document.body.scrollTop,n="body"==this.options.container?window.innerWidth:k.outerWidth(),o="body"==this.options.container?window.innerHeight:k.outerHeight(),p="body"==this.options.container?0:k.offset().left;e="bottom"==e&&h.top+h.height+j-m>o?"top":"top"==e&&h.top-m-j<0?"bottom":"right"==e&&h.right+i>n?"left":"left"==e&&h.left-i<p?"right":e,d.removeClass(l).addClass(e)}var q=this.getCalculatedOffset(e,h,i,j);this.applyPlacement(q,e),this.hoverState=null;var r=function(){c.$element.trigger("shown.bs."+c.type)};a.support.transition&&this.$tip.hasClass("fade")?d.one(a.support.transition.end,r).emulateTransitionEnd(150):r()}},b.prototype.applyPlacement=function(b,c){var d,e=this.tip(),f=e[0].offsetWidth,g=e[0].offsetHeight,h=parseInt(e.css("margin-top"),10),i=parseInt(e.css("margin-left"),10);isNaN(h)&&(h=0),isNaN(i)&&(i=0),b.top=b.top+h,b.left=b.left+i,a.offset.setOffset(e[0],a.extend({using:function(a){e.css({top:Math.round(a.top),left:Math.round(a.left)})}},b),0),e.addClass("in");var j=e[0].offsetWidth,k=e[0].offsetHeight;if("top"==c&&k!=g&&(d=!0,b.top=b.top+g-k),/bottom|top/.test(c)){var l=0;b.left<0&&(l=-2*b.left,b.left=0,e.offset(b),j=e[0].offsetWidth,k=e[0].offsetHeight),this.replaceArrow(l-f+j,j,"left")}else this.replaceArrow(k-g,k,"top");d&&e.offset(b)},b.prototype.replaceArrow=function(a,b,c){this.arrow().css(c,a?50*(1-a/b)+"%":"")},b.prototype.setContent=function(){var a=this.tip(),b=this.getTitle();a.find(".tooltip-inner")[this.options.html?"html":"text"](b),a.removeClass("fade in top bottom left right")},b.prototype.hide=function(){function b(){"in"!=c.hoverState&&d.detach(),c.$element.trigger("hidden.bs."+c.type)}var c=this,d=this.tip(),e=a.Event("hide.bs."+this.type);return this.$element.trigger(e),e.isDefaultPrevented()?void 0:(d.removeClass("in"),a.support.transition&&this.$tip.hasClass("fade")?d.one(a.support.transition.end,b).emulateTransitionEnd(150):b(),this.hoverState=null,this)},b.prototype.fixTitle=function(){var a=this.$element;(a.attr("title")||"string"!=typeof a.attr("data-original-title"))&&a.attr("data-original-title",a.attr("title")||"").attr("title","")},b.prototype.hasContent=function(){return this.getTitle()},b.prototype.getPosition=function(){var b=this.$element[0];return a.extend({},"function"==typeof b.getBoundingClientRect?b.getBoundingClientRect():{width:b.offsetWidth,height:b.offsetHeight},this.$element.offset())},b.prototype.getCalculatedOffset=function(a,b,c,d){return"bottom"==a?{top:b.top+b.height,left:b.left+b.width/2-c/2}:"top"==a?{top:b.top-d,left:b.left+b.width/2-c/2}:"left"==a?{top:b.top+b.height/2-d/2,left:b.left-c}:{top:b.top+b.height/2-d/2,left:b.left+b.width}},b.prototype.getTitle=function(){var a,b=this.$element,c=this.options;return a=b.attr("data-original-title")||("function"==typeof c.title?c.title.call(b[0]):c.title)},b.prototype.tip=function(){return this.$tip=this.$tip||a(this.options.template)},b.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".tooltip-arrow")},b.prototype.validate=function(){this.$element[0].parentNode||(this.hide(),this.$element=null,this.options=null)},b.prototype.enable=function(){this.enabled=!0},b.prototype.disable=function(){this.enabled=!1},b.prototype.toggleEnabled=function(){this.enabled=!this.enabled},b.prototype.toggle=function(b){var c=b?a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs."+this.type):this;c.tip().hasClass("in")?c.leave(c):c.enter(c)},b.prototype.destroy=function(){clearTimeout(this.timeout),this.hide().$element.off("."+this.type).removeData("bs."+this.type)};var c=a.fn.tooltip;a.fn.tooltip=function(c){return this.each(function(){var d=a(this),e=d.data("bs.tooltip"),f="object"==typeof c&&c;(e||"destroy"!=c)&&(e||d.data("bs.tooltip",e=new b(this,f)),"string"==typeof c&&e[c]())})},a.fn.tooltip.Constructor=b,a.fn.tooltip.noConflict=function(){return a.fn.tooltip=c,this}}(jQuery),+function(a){"use strict";var b=function(a,b){this.init("popover",a,b)};if(!a.fn.tooltip)throw new Error("Popover requires tooltip.js");b.DEFAULTS=a.extend({},a.fn.tooltip.Constructor.DEFAULTS,{placement:"right",trigger:"click",content:"",template:'<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'}),b.prototype=a.extend({},a.fn.tooltip.Constructor.prototype),b.prototype.constructor=b,b.prototype.getDefaults=function(){return b.DEFAULTS},b.prototype.setContent=function(){var a=this.tip(),b=this.getTitle(),c=this.getContent();a.find(".popover-title")[this.options.html?"html":"text"](b),a.find(".popover-content")[this.options.html?"string"==typeof c?"html":"append":"text"](c),a.removeClass("fade top bottom left right in"),a.find(".popover-title").html()||a.find(".popover-title").hide()},b.prototype.hasContent=function(){return this.getTitle()||this.getContent()},b.prototype.getContent=function(){var a=this.$element,b=this.options;return a.attr("data-content")||("function"==typeof b.content?b.content.call(a[0]):b.content)},b.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".arrow")},b.prototype.tip=function(){return this.$tip||(this.$tip=a(this.options.template)),this.$tip};var c=a.fn.popover;a.fn.popover=function(c){return this.each(function(){var d=a(this),e=d.data("bs.popover"),f="object"==typeof c&&c;(e||"destroy"!=c)&&(e||d.data("bs.popover",e=new b(this,f)),"string"==typeof c&&e[c]())})},a.fn.popover.Constructor=b,a.fn.popover.noConflict=function(){return a.fn.popover=c,this}}(jQuery),+function(a){"use strict";function b(c,d){var e,f=a.proxy(this.process,this);this.$element=a(a(c).is("body")?window:c),this.$body=a("body"),this.$scrollElement=this.$element.on("scroll.bs.scroll-spy.data-api",f),this.options=a.extend({},b.DEFAULTS,d),this.selector=(this.options.target||(e=a(c).attr("href"))&&e.replace(/.*(?=#[^\s]+$)/,"")||"")+" .nav li > a",this.offsets=a([]),this.targets=a([]),this.activeTarget=null,this.refresh(),this.process()}b.DEFAULTS={offset:10},b.prototype.refresh=function(){var b=this.$element[0]==window?"offset":"position";this.offsets=a([]),this.targets=a([]);{var c=this;this.$body.find(this.selector).map(function(){var d=a(this),e=d.data("target")||d.attr("href"),f=/^#./.test(e)&&a(e);return f&&f.length&&f.is(":visible")&&[[f[b]().top+(!a.isWindow(c.$scrollElement.get(0))&&c.$scrollElement.scrollTop()),e]]||null}).sort(function(a,b){return a[0]-b[0]}).each(function(){c.offsets.push(this[0]),c.targets.push(this[1])})}},b.prototype.process=function(){var a,b=this.$scrollElement.scrollTop()+this.options.offset,c=this.$scrollElement[0].scrollHeight||this.$body[0].scrollHeight,d=c-this.$scrollElement.height(),e=this.offsets,f=this.targets,g=this.activeTarget;if(b>=d)return g!=(a=f.last()[0])&&this.activate(a);if(g&&b<=e[0])return g!=(a=f[0])&&this.activate(a);for(a=e.length;a--;)g!=f[a]&&b>=e[a]&&(!e[a+1]||b<=e[a+1])&&this.activate(f[a])},b.prototype.activate=function(b){this.activeTarget=b,a(this.selector).parentsUntil(this.options.target,".active").removeClass("active");var c=this.selector+'[data-target="'+b+'"],'+this.selector+'[href="'+b+'"]',d=a(c).parents("li").addClass("active");d.parent(".dropdown-menu").length&&(d=d.closest("li.dropdown").addClass("active")),d.trigger("activate.bs.scrollspy")};var c=a.fn.scrollspy;a.fn.scrollspy=function(c){return this.each(function(){var d=a(this),e=d.data("bs.scrollspy"),f="object"==typeof c&&c;e||d.data("bs.scrollspy",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.scrollspy.Constructor=b,a.fn.scrollspy.noConflict=function(){return a.fn.scrollspy=c,this},a(window).on("load",function(){a('[data-spy="scroll"]').each(function(){var b=a(this);b.scrollspy(b.data())})})}(jQuery),+function(a){"use strict";var b=function(b){this.element=a(b)};b.prototype.show=function(){var b=this.element,c=b.closest("ul:not(.dropdown-menu)"),d=b.data("target");if(d||(d=b.attr("href"),d=d&&d.replace(/.*(?=#[^\s]*$)/,"")),!b.parent("li").hasClass("active")){var e=c.find(".active:last a")[0],f=a.Event("show.bs.tab",{relatedTarget:e});if(b.trigger(f),!f.isDefaultPrevented()){var g=a(d);this.activate(b.parent("li"),c),this.activate(g,g.parent(),function(){b.trigger({type:"shown.bs.tab",relatedTarget:e})})}}},b.prototype.activate=function(b,c,d){function e(){f.removeClass("active").find("> .dropdown-menu > .active").removeClass("active"),b.addClass("active"),g?(b[0].offsetWidth,b.addClass("in")):b.removeClass("fade"),b.parent(".dropdown-menu")&&b.closest("li.dropdown").addClass("active"),d&&d()}var f=c.find("> .active"),g=d&&a.support.transition&&f.hasClass("fade");g?f.one(a.support.transition.end,e).emulateTransitionEnd(150):e(),f.removeClass("in")};var c=a.fn.tab;a.fn.tab=function(c){return this.each(function(){var d=a(this),e=d.data("bs.tab");e||d.data("bs.tab",e=new b(this)),"string"==typeof c&&e[c]()})},a.fn.tab.Constructor=b,a.fn.tab.noConflict=function(){return a.fn.tab=c,this},a(document).on("click.bs.tab.data-api",'[data-toggle="tab"], [data-toggle="pill"]',function(b){b.preventDefault(),a(this).tab("show")})}(jQuery),+function(a){"use strict";var b=function(c,d){this.options=a.extend({},b.DEFAULTS,d),this.$window=a(window).on("scroll.bs.affix.data-api",a.proxy(this.checkPosition,this)).on("click.bs.affix.data-api",a.proxy(this.checkPositionWithEventLoop,this)),this.$element=a(c),this.affixed=this.unpin=this.pinnedOffset=null,this.checkPosition()};b.RESET="affix affix-top affix-bottom",b.DEFAULTS={offset:0},b.prototype.getPinnedOffset=function(){if(this.pinnedOffset)return this.pinnedOffset;this.$element.removeClass(b.RESET).addClass("affix");var a=this.$window.scrollTop(),c=this.$element.offset();return this.pinnedOffset=c.top-a},b.prototype.checkPositionWithEventLoop=function(){setTimeout(a.proxy(this.checkPosition,this),1)},b.prototype.checkPosition=function(){if(this.$element.is(":visible")){var c=a(document).height(),d=this.$window.scrollTop(),e=this.$element.offset(),f=this.options.offset,g=f.top,h=f.bottom;"top"==this.affixed&&(e.top+=d),"object"!=typeof f&&(h=g=f),"function"==typeof g&&(g=f.top(this.$element)),"function"==typeof h&&(h=f.bottom(this.$element));var i=null!=this.unpin&&d+this.unpin<=e.top?!1:null!=h&&e.top+this.$element.height()>=c-h?"bottom":null!=g&&g>=d?"top":!1;if(this.affixed!==i){this.unpin&&this.$element.css("top","");var j="affix"+(i?"-"+i:""),k=a.Event(j+".bs.affix");this.$element.trigger(k),k.isDefaultPrevented()||(this.affixed=i,this.unpin="bottom"==i?this.getPinnedOffset():null,this.$element.removeClass(b.RESET).addClass(j).trigger(a.Event(j.replace("affix","affixed"))),"bottom"==i&&this.$element.offset({top:c-h-this.$element.height()}))}}};var c=a.fn.affix;a.fn.affix=function(c){return this.each(function(){var d=a(this),e=d.data("bs.affix"),f="object"==typeof c&&c;e||d.data("bs.affix",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.affix.Constructor=b,a.fn.affix.noConflict=function(){return a.fn.affix=c,this},a(window).on("load",function(){a('[data-spy="affix"]').each(function(){var b=a(this),c=b.data();c.offset=c.offset||{},c.offsetBottom&&(c.offset.bottom=c.offsetBottom),c.offsetTop&&(c.offset.top=c.offsetTop),b.affix(c)})})}(jQuery);
/**
 * bootstrap-multiselect.js
 * https://github.com/davidstutz/bootstrap-multiselect
 *
 * Copyright 2012, 2013 David Stutz
 *
 * Dual licensed under the BSD-3-Clause and the Apache License, Version 2.0.
 */
!function($) {

    "use strict";// jshint ;_;

    if (typeof ko !== 'undefined' && ko.bindingHandlers && !ko.bindingHandlers.multiselect) {
        ko.bindingHandlers.multiselect = {
            init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {},
            update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

               var config = ko.utils.unwrapObservable(valueAccessor());
               var selectOptions = allBindingsAccessor().options;
               var ms = $(element).data('multiselect');

               if (!ms) {
                  $(element).multiselect(config);
               }
               else {
                  ms.updateOriginalOptions();
                  if (selectOptions && selectOptions().length !== ms.originalOptions.length) {
                     $(element).multiselect('rebuild');
                  }
               }
            }
        };
    }

    /**
     * Constructor to create a new multiselect using the given select.
     * 
     * @param {jQuery} select
     * @param {Object} options
     * @returns {Multiselect}
     */
    function Multiselect(select, options) {

        this.options = this.mergeOptions(options);
        this.$select = $(select);

        // Initialization.
        // We have to clone to create a new reference.
        this.originalOptions = this.$select.clone()[0].options;
        this.query = '';
        this.searchTimeout = null;

        this.options.multiple = this.$select.attr('multiple') === "multiple";
        this.options.onChange = $.proxy(this.options.onChange, this);
        this.options.onDropdownShow = $.proxy(this.options.onDropdownShow, this);
        this.options.onDropdownHide = $.proxy(this.options.onDropdownHide, this);

        // Build select all if enabled.
        this.buildContainer();
        this.buildButton();
        this.buildSelectAll();
        this.buildDropdown();
        this.buildDropdownOptions();
        this.buildFilter();
        
        this.updateButtonText();
        this.updateSelectAll();
        
        this.$select.hide().after(this.$container);
    };

    Multiselect.prototype = {

        defaults: {
            /**
             * Default text function will either print 'None selected' in case no
             * option is selected or a list of the selected options up to a length of 3 selected options.
             * 
             * @param {jQuery} options
             * @param {jQuery} select
             * @returns {String}
             */
            buttonText: function(options, select) {
                if (options.length === 0) {
                    return this.nonSelectedText + ' <b class="caret"></b>';
                }
                else {
                    if (options.length > this.numberDisplayed) {
                        return options.length + ' ' + this.nSelectedText + ' <b class="caret"></b>';
                    }
                    else {
                        var selected = '';
                        options.each(function() {
                            var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();

                            selected += label + ', ';
                        });
                        return selected.substr(0, selected.length - 2) + ' <b class="caret"></b>';
                    }
                }
            },
            /**
             * Updates the title of the button similar to the buttonText function.
             * @param {jQuery} options
             * @param {jQuery} select
             * @returns {@exp;selected@call;substr}
             */
            buttonTitle: function(options, select) {
                if (options.length === 0) {
                    return this.nonSelectedText;
                }
                else {
                    var selected = '';
                    options.each(function () {
                        selected += $(this).text() + ', ';
                    });
                    return selected.substr(0, selected.length - 2);
                }
            },
            /**
             * Create a label.
             * 
             * @param {jQuery} element
             * @returns {String}
             */
            label: function(element){
                return $(element).attr('label') || $(element).html();
            },
            /**
             * Triggered on change of the multiselect.
             * Not triggered when selecting/deselecting options manually.
             * 
             * @param {jQuery} option
             * @param {Boolean} checked
             */
            onChange : function(option, checked) {

            },
            /**
             * Triggered when the dropdown is shown.
             * 
             * @param {jQuery} event
             */
            onDropdownShow: function(event) {

            },
            /**
             * Triggered when the dropdown is hidden.
             * 
             * @param {jQuery} event
             */
            onDropdownHide: function(event) {

            },
            buttonClass: 'btn btn-default',
            dropRight: false,
            selectedClass: 'active',
            buttonWidth: 'auto',
            buttonContainer: '<div class="btn-group" />',
            // Maximum height of the dropdown menu.
            // If maximum height is exceeded a scrollbar will be displayed.
            maxHeight: false,
            includeSelectAllOption: false,
            selectAllText: ' Select all',
            selectAllValue: 'multiselect-all',
            enableFiltering: false,
            enableCaseInsensitiveFiltering: false,
            filterPlaceholder: 'Search',
            // possible options: 'text', 'value', 'both'
            filterBehavior: 'text',
            preventInputChangeEvent: false,
            nonSelectedText: 'None selected',
            nSelectedText: 'selected',
            numberDisplayed: 3
        },

        templates: {
            button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
            ul: '<ul class="multiselect-container dropdown-menu"></ul>',
            filter: '<div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span><input class="form-control multiselect-search" type="text"></div>',
            li: '<li><a href="javascript:void(0);"><label></label></a></li>',
            divider: '<li class="divider"></li>',
            liGroup: '<li><label class="multiselect-group"></label></li>'
        },

        constructor: Multiselect,

        /**
         * Builds the container of the multiselect.
         */
        buildContainer: function() {
            this.$container = $(this.options.buttonContainer);
            this.$container.on('show.bs.dropdown', this.options.onDropdownShow);
            this.$container.on('hide.bs.dropdown', this.options.onDropdownHide);
        },

        /**
         * Builds the button of the multiselect.
         */
        buildButton: function() {
            this.$button = $(this.templates.button).addClass(this.options.buttonClass);

            // Adopt active state.
            if (this.$select.prop('disabled')) {
                this.disable();
            }
            else {
                this.enable();
            }

            // Manually add button width if set.
            if (this.options.buttonWidth && this.options.buttonWidth != 'auto') {
                this.$button.css({
                    'width' : this.options.buttonWidth
                });
            }

            // Keep the tab index from the select.
            var tabindex = this.$select.attr('tabindex');
            if (tabindex) {
                this.$button.attr('tabindex', tabindex);
            }

            this.$container.prepend(this.$button);
        },

        /**
         * Builds the ul representing the dropdown menu.
         */
        buildDropdown: function() {

            // Build ul.
            this.$ul = $(this.templates.ul);

            if (this.options.dropRight) {
                this.$ul.addClass('pull-right');
            }

            // Set max height of dropdown menu to activate auto scrollbar.
            if (this.options.maxHeight) {
                // TODO: Add a class for this option to move the css declarations.
                this.$ul.css({
                    'max-height': this.options.maxHeight + 'px',
                    'overflow-y': 'auto',
                    'overflow-x': 'hidden'
                });
            }

            this.$container.append(this.$ul);
        },

        /**
         * Build the dropdown options and binds all nessecary events.
         * Uses createDivider and createOptionValue to create the necessary options.
         */
        buildDropdownOptions: function() {

            this.$select.children().each($.proxy(function(index, element) {
                
                // Support optgroups and options without a group simultaneously.
                var tag = $(element).prop('tagName')
                    .toLowerCase();

                if (tag === 'optgroup') {
                    this.createOptgroup(element);
                }
                else if (tag === 'option') {

                    if ($(element).data('role') === 'divider') {
                        this.createDivider();
                    }
                    else {
                        this.createOptionValue(element);
                    }

                }
                
                // Other illegal tags will be ignored.
            }, this));

            // Bind the change event on the dropdown elements.
            $('li input', this.$ul).on('change', $.proxy(function(event) {
                var checked = $(event.target).prop('checked') || false;
                var isSelectAllOption = $(event.target).val() === this.options.selectAllValue;

                // Apply or unapply the configured selected class.
                if (this.options.selectedClass) {
                    if (checked) {
                        $(event.target).parents('li')
                            .addClass(this.options.selectedClass);
                    }
                    else {
                        $(event.target).parents('li')
                            .removeClass(this.options.selectedClass);
                    }
                }

                // Get the corresponding option.
                var value = $(event.target).val();
                var $option = this.getOptionByValue(value);

                var $optionsNotThis = $('option', this.$select).not($option);
                var $checkboxesNotThis = $('input', this.$container).not($(event.target));

                if (isSelectAllOption) {
                    if (this.$select[0][0].value === this.options.selectAllValue) {
                        var values = [];
                        var options = $('option[value!="' + this.options.selectAllValue + '"]', this.$select);
                        for (var i = 0; i < options.length; i++) {
                            // Additionally check whether the option is visible within the dropcown.
                            if (options[i].value !== this.options.selectAllValue && this.getInputByValue(options[i].value).is(':visible')) {
                                values.push(options[i].value);
                            }
                        }

                        if (checked) {
                            this.select(values);
                        }
                        else {
                            this.deselect(values);
                        }
                    }
                }

                if (checked) {
                    $option.prop('selected', true);

                    if (this.options.multiple) {
                        // Simply select additional option.
                        $option.prop('selected', true);
                    }
                    else {
                        // Unselect all other options and corresponding checkboxes.
                        if (this.options.selectedClass) {
                            $($checkboxesNotThis).parents('li').removeClass(this.options.selectedClass);
                        }

                        $($checkboxesNotThis).prop('checked', false);
                        $optionsNotThis.prop('selected', false);

                        // It's a single selection, so close.
                        this.$button.click();
                    }

                    if (this.options.selectedClass === "active") {
                        $optionsNotThis.parents("a").css("outline", "");
                    }
                }
                else {
                    // Unselect option.
                    $option.prop('selected', false);
                }

                this.$select.change();
                this.options.onChange($option, checked);
                
                this.updateButtonText();
                this.updateSelectAll();

                if(this.options.preventInputChangeEvent) {
                    return false;
                }
            }, this));

            $('li a', this.$ul).on('touchstart click', function(event) {
                event.stopPropagation();

                if (event.shiftKey) {
                    var checked = $(event.target).prop('checked') || false;

                    if (checked) {
                        var prev = $(event.target).parents('li:last')
                            .siblings('li[class="active"]:first');

                        var currentIdx = $(event.target).parents('li')
                            .index();
                        var prevIdx = prev.index();

                        if (currentIdx > prevIdx) {
                            $(event.target).parents("li:last").prevUntil(prev).each(
                                function() {
                                    $(this).find("input:first").prop("checked", true)
                                        .trigger("change");
                                }
                            );
                        }
                        else {
                            $(event.target).parents("li:last").nextUntil(prev).each(
                                function() {
                                    $(this).find("input:first").prop("checked", true)
                                        .trigger("change");
                                }
                            );
                        }
                    }
                }

                $(event.target).blur();
            });

            // Keyboard support.
            this.$container.on('keydown', $.proxy(function(event) {
                if ($('input[type="text"]', this.$container).is(':focus')) {
                    return;
                }
                if ((event.keyCode === 9 || event.keyCode === 27)
                        && this.$container.hasClass('open')) {
                    
                    // Close on tab or escape.
                    this.$button.click();
                }
                else {
                    var $items = $(this.$container).find("li:not(.divider):visible a");

                    if (!$items.length) {
                        return;
                    }

                    var index = $items.index($items.filter(':focus'));

                    // Navigation up.
                    if (event.keyCode === 38 && index > 0) {
                        index--;
                    }
                    // Navigate down.
                    else if (event.keyCode === 40 && index < $items.length - 1) {
                        index++;
                    }
                    else if (!~index) {
                        index = 0;
                    }

                    var $current = $items.eq(index);
                    $current.focus();

                    if (event.keyCode === 32 || event.keyCode === 13) {
                        var $checkbox = $current.find('input');

                        $checkbox.prop("checked", !$checkbox.prop("checked"));
                        $checkbox.change();
                    }

                    event.stopPropagation();
                    event.preventDefault();
                }
            }, this));
        },

        /**
         * Create an option using the given select option.
         * 
         * @param {jQuery} element
         */
        createOptionValue: function(element) {
            if ($(element).is(':selected')) {
                $(element).prop('selected', true);
            }

            // Support the label attribute on options.
            var label = this.options.label(element);
            var value = $(element).val();
            var inputType = this.options.multiple ? "checkbox" : "radio";

            var $li = $(this.templates.li);
            $('label', $li).addClass(inputType);
            $('label', $li).append('<input type="' + inputType + '" />');

            var selected = $(element).prop('selected') || false;
            var $checkbox = $('input', $li);
            $checkbox.val(value);

            if (value === this.options.selectAllValue) {
                $checkbox.parent().parent()
                    .addClass('multiselect-all');
            }

            $('label', $li).append(" " + label);

            this.$ul.append($li);

            if ($(element).is(':disabled')) {
                $checkbox.attr('disabled', 'disabled')
                    .prop('disabled', true)
                    .parents('li')
                    .addClass('disabled');
            }

            $checkbox.prop('checked', selected);

            if (selected && this.options.selectedClass) {
                $checkbox.parents('li')
                    .addClass(this.options.selectedClass);
            }
        },

        /**
         * Creates a divider using the given select option.
         * 
         * @param {jQuery} element
         */
        createDivider: function(element) {
            var $divider = $(this.templates.divider);
            this.$ul.append($divider);
        },

        /**
         * Creates an optgroup.
         * 
         * @param {jQuery} group
         */
        createOptgroup: function(group) {
            var groupName = $(group).prop('label');

            // Add a header for the group.
            var $li = $(this.templates.liGroup);
            $('label', $li).text(groupName);

            this.$ul.append($li);

            // Add the options of the group.
            $('option', group).each($.proxy(function(index, element) {
                this.createOptionValue(element);
            }, this));
        },

        /**
         * Build the selct all.
         * Checks if a select all ahs already been created.
         */
        buildSelectAll: function() {
            var alreadyHasSelectAll = this.hasSelectAll();
            
            // If options.includeSelectAllOption === true, add the include all checkbox.
            if (this.options.includeSelectAllOption && this.options.multiple && !alreadyHasSelectAll) {
                this.$select.prepend('<option value="' + this.options.selectAllValue + '">' + this.options.selectAllText + '</option>');
            }
        },

        /**
         * Builds the filter.
         */
        buildFilter: function() {

            // Build filter if filtering OR case insensitive filtering is enabled and the number of options exceeds (or equals) enableFilterLength.
            if (this.options.enableFiltering || this.options.enableCaseInsensitiveFiltering) {
                var enableFilterLength = Math.max(this.options.enableFiltering, this.options.enableCaseInsensitiveFiltering);

                if (this.$select.find('option').length >= enableFilterLength) {

                    this.$filter = $(this.templates.filter);
                    $('input', this.$filter).attr('placeholder', this.options.filterPlaceholder);
                    this.$ul.prepend(this.$filter);

                    this.$filter.val(this.query).on('click', function(event) {
                        event.stopPropagation();
                    }).on('input keydown', $.proxy(function(event) {
                        // This is useful to catch "keydown" events after the browser has updated the control.
                        clearTimeout(this.searchTimeout);

                        this.searchTimeout = this.asyncFunction($.proxy(function() {

                            if (this.query !== event.target.value) {
                                this.query = event.target.value;

                                $.each($('li', this.$ul), $.proxy(function(index, element) {
                                    var value = $('input', element).val();
                                    var text = $('label', element).text();

                                    if (value !== this.options.selectAllValue && text) {
                                        // by default lets assume that element is not
                                        // interesting for this search
                                        var showElement = false;

                                        var filterCandidate = '';
                                        if ((this.options.filterBehavior === 'text' || this.options.filterBehavior === 'both')) {
                                            filterCandidate = text;
                                        }
                                        if ((this.options.filterBehavior === 'value' || this.options.filterBehavior === 'both')) {
                                            filterCandidate = value;
                                        }

                                        if (this.options.enableCaseInsensitiveFiltering && filterCandidate.toLowerCase().indexOf(this.query.toLowerCase()) > -1) {
                                            showElement = true;
                                        }
                                        else if (filterCandidate.indexOf(this.query) > -1) {
                                            showElement = true;
                                        }

                                        if (showElement) {
                                            $(element).show();
                                        }
                                        else {
                                            $(element).hide();
                                        }
                                    }
                                }, this));
                            }

                            // TODO: check whether select all option needs to be updated.
                        }, this), 300, this);
                    }, this));
                }
            }
        },

        /**
         * Unbinds the whole plugin.
         */
        destroy: function() {
            this.$container.remove();
            this.$select.show();
        },

        /**
         * Refreshs the multiselect based on the selected options of the select.
         */
        refresh: function() {
            $('option', this.$select).each($.proxy(function(index, element) {
                var $input = $('li input', this.$ul).filter(function() {
                    return $(this).val() === $(element).val();
                });

                if ($(element).is(':selected')) {
                    $input.prop('checked', true);

                    if (this.options.selectedClass) {
                        $input.parents('li')
                            .addClass(this.options.selectedClass);
                    }
                }
                else {
                    $input.prop('checked', false);

                    if (this.options.selectedClass) {
                        $input.parents('li')
                            .removeClass(this.options.selectedClass);
                    }
                }

                if ($(element).is(":disabled")) {
                    $input.attr('disabled', 'disabled')
                        .prop('disabled', true)
                        .parents('li')
                        .addClass('disabled');
                }
                else {
                    $input.prop('disabled', false)
                        .parents('li')
                        .removeClass('disabled');
                }
            }, this));

            this.updateButtonText();
            this.updateSelectAll();
        },

        /**
         * Select all options of the given values.
         * 
         * @param {Array} selectValues
         */
        select: function(selectValues) {
            if(selectValues && !$.isArray(selectValues)) {
                selectValues = [selectValues];
            }

            for (var i = 0; i < selectValues.length; i++) {
                var value = selectValues[i];

                var $option = this.getOptionByValue(value);
                var $checkbox = this.getInputByValue(value);

                if (this.options.selectedClass) {
                    $checkbox.parents('li')
                        .addClass(this.options.selectedClass);
                }

                $checkbox.prop('checked', true);
                $option.prop('selected', true);
            }

            this.updateButtonText();
        },

        /**
         * Deselects all options of the given values.
         * 
         * @param {Array} deselectValues
         */
        deselect: function(deselectValues) {
            if(deselectValues && !$.isArray(deselectValues)) {
                deselectValues = [deselectValues];
            }

            for (var i = 0; i < deselectValues.length; i++) {

                var value = deselectValues[i];

                var $option = this.getOptionByValue(value);
                var $checkbox = this.getInputByValue(value);

                if (this.options.selectedClass) {
                    $checkbox.parents('li')
                        .removeClass(this.options.selectedClass);
                }

                $checkbox.prop('checked', false);
                $option.prop('selected', false);
            }

            this.updateButtonText();
        },

        /**
         * Rebuild the plugin.
         * Rebuilds the dropdown, the filter and the select all option.
         */
        rebuild: function() {
            this.$ul.html('');

            // Remove select all option in select.
            $('option[value="' + this.options.selectAllValue + '"]', this.$select).remove();

            // Important to distinguish between radios and checkboxes.
            this.options.multiple = this.$select.attr('multiple') === "multiple";

            this.buildSelectAll();
            this.buildDropdownOptions();
            this.buildFilter();
            
            this.updateButtonText();
            this.updateSelectAll();
        },

        /**
         * The provided data will be used to build the dropdown.
         * 
         * @param {Array} dataprovider
         */
        dataprovider: function(dataprovider) {
            var optionDOM = "";
            dataprovider.forEach(function (option) {
                optionDOM += '<option value="' + option.value + '">' + option.label + '</option>';
            });

            this.$select.html(optionDOM);
            this.rebuild();
        },

        /**
         * Enable the multiselect.
         */
        enable: function() {
            this.$select.prop('disabled', false);
            this.$button.prop('disabled', false)
                .removeClass('disabled');
        },

        /**
         * Disable the multiselect.
         */
        disable: function() {
            this.$select.prop('disabled', true);
            this.$button.prop('disabled', true)
                .addClass('disabled');
        },

        /**
         * Set the options.
         * 
         * @param {Array} options
         */
        setOptions: function(options) {
            this.options = this.mergeOptions(options);
        },

        /**
         * Merges the given options with the default options.
         * 
         * @param {Array} options
         * @returns {Array}
         */
        mergeOptions: function(options) {
            return $.extend({}, this.defaults, options);
        },
        
        /**
         * Checks whether a select all option is present.
         * 
         * @returns {Boolean}
         */
        hasSelectAll: function() {
            return this.$select[0][0] ? this.$select[0][0].value === this.options.selectAllValue : false;
        },
        
        /**
         * Updates the select all option based on the currently selected options.
         */
        updateSelectAll: function() {
            if (this.hasSelectAll()) {
                var selected = this.getSelected();
                
                if (selected.length === $('option', this.$select).length - 1) {
                    this.select(this.options.selectAllValue);
                }
                else {
                    this.deselect(this.options.selectAllValue);
                }
            }
        },
        
        /**
         * Update the button text and its title base don the currenty selected options.
         */
        updateButtonText: function() {
            var options = this.getSelected();
            
            // First update the displayed button text.
            $('button', this.$container).html(this.options.buttonText(options, this.$select));
            
            // Now update the title attribute of the button.
            $('button', this.$container).attr('title', this.options.buttonTitle(options, this.$select));

        },

        /**
         * Get all selected options.
         * 
         * @returns {jQUery}
         */
        getSelected: function() {
            return $('option[value!="' + this.options.selectAllValue + '"]:selected', this.$select).filter(function() {
                return $(this).prop('selected');
            });
        },

        /**
         * Gets a select option by its value.
         * 
         * @param {String} value
         * @returns {jQuery}
         */
        getOptionByValue: function(value) {
            return $('option', this.$select).filter(function() {
                return $(this).val() === value;
            });
        },

        /**
         * Get the input (radio/checkbox) by its value.
         * 
         * @param {String} value
         * @returns {jQuery}
         */
        getInputByValue: function(value) {
            return $('li input', this.$ul).filter(function() {
                return $(this).val() === value;
            });
        },

        /**
         * Used for knockout integration.
         */
        updateOriginalOptions: function() {
            this.originalOptions = this.$select.clone()[0].options;
        },

        asyncFunction: function(callback, timeout, self) {
            var args = Array.prototype.slice.call(arguments, 3);
            return setTimeout(function() {
                callback.apply(self || window, args);
            }, timeout);
        }
    };

    $.fn.multiselect = function(option, parameter) {
        return this.each(function() {
            var data = $(this).data('multiselect');
            var options = typeof option === 'object' && option;

            // Initialize the multiselect.
            if (!data) {
                $(this).data('multiselect', ( data = new Multiselect(this, options)));
            }

            // Call multiselect method.
            if (typeof option === 'string') {
                data[option](parameter);
            }
        });
    };

    $.fn.multiselect.Constructor = Multiselect;

    $(function() {
        $("select[data-role=multiselect]").multiselect();
    });

}(window.jQuery);

angular.module('truncate', [])
    .filter('characters', function () {
        return function (input, chars, breakOnWord) {
            if (isNaN(chars)) return input;
            if (chars <= 0) return '';
            if (input && input.length > chars) {
                input = input.substring(0, chars);

                if (!breakOnWord) {
                    var lastspace = input.lastIndexOf(' ');
                    //get last space
                    if (lastspace !== -1) {
                        input = input.substr(0, lastspace);
                    }
                }else{
                    while(input.charAt(input.length-1) === ' '){
                        input = input.substr(0, input.length -1);
                    }
                }
                return input + '...';
            }
            return input;
        };
    })
    .filter('words', function () {
        return function (input, words) {
            if (isNaN(words)) return input;
            if (words <= 0) return '';
            if (input) {
                var inputWords = input.split(/\s+/);
                if (inputWords.length > words) {
                    input = inputWords.slice(0, words).join(' ') + '...';
                }
            }
            return input;
        };
    });

////
// balanced.js
// version: 1.1.19
// built: 2014-03-25
// https://github.com/balanced/balanced-js
////

(function(exports, global) {
    global["balanced"] = exports;
    var capabilities = {
        system_timezone: -new Date().getTimezoneOffset() / 60,
        user_agent: navigator.userAgent,
        language: navigator.userLanguage || navigator.language,
        kp: 0,
        cli: 0,
        loaded: new Date() * 1,
        screen_width: screen.width,
        screen_length: screen.height,
        hist: window.history.length,
        cookie: function() {
            var cookie = document.cookie.match(/__b=([a-zA-Z0-9\-!\.]+)/);
            if (!cookie) {
                cookie = new Date() * 1 + "." + Math.random().toString().substr(2) + ".0!0";
            } else {
                cookie = cookie[1];
            }
            cookie = cookie.split("!");
            var cookie_parts = cookie[0].split(".");
            if (cookie_parts.length < 3) {
                cookie_parts[1] = Math.random().toString().substr(2);
                cookie_parts[2] = 0;
            }
            cookie_parts[2]++;
            cookie = cookie_parts.join(".") + "!" + cookie[1];
            var cookie_date = new Date();
            cookie_date.setDate(cookie_date.getDate() + 365);
            document.cookie = "__b=" + cookie + " ;expires=" + cookie_date.toUTCString();
            return cookie;
        }()
    };
    var marketplace_href = null, dialog = null, root_url = "https://api.balancedpayments.com";
    function preparePayload(data) {
        if (!data.meta) {
            data.meta = {};
        }
        capabilities.submitted = new Date() * 1;
        capabilities.scrollX = window.scrollX;
        capabilities.scrollY = window.scrollY;
        for (var k in capabilities) {
            if (!("capabilities_" + k in data.meta)) {
                data.meta["capabilities_" + k] = capabilities[k];
            }
        }
        return data;
    }
    function addEvent(obj, type, fn) {
        if (obj.addEventListener) {
            obj.addEventListener(type, fn, false);
        } else if (obj.attachEvent) {
            obj["e" + type + fn] = fn;
            obj[type + fn] = function() {
                obj["e" + type + fn](window.event);
            };
            obj.attachEvent("on" + type, obj[type + fn]);
        }
    }
    var shifted = false;
    function icl(e) {
        e = e ? e : window.event;
        var shifton = false;
        if (e.shiftKey) {
            shifton = e.shiftKey;
        } else if (e.modifiers) {
            shifton = !!(e.modifiers & 4);
        }
        if (shifton) {
            shifted = true;
        }
        return shifted;
    }
    function isEmpty(obj) {
        if (obj == null) {
            return true;
        }
        for (var key in obj) {
            if (hasOwnProperty.call(obj, key)) {
                return false;
            }
        }
        return true;
    }
    addEvent(window, "keydown", function(e) {
        if (!capabilities.cl) {
            capabilities.cl = icl(e);
        }
        capabilities.kp++;
    });
    addEvent(window, "paste", function() {
        capabilities.ps = true;
    });
    addEvent(window, "click", function() {
        capabilities.cli++;
    });
    if (!String.prototype.trim) {
        String.prototype.trim = function() {
            return this.replace(/^\s+|\s+$/g, "");
        };
    }
    function buildErrorObject(key, message) {
        var error = {};
        var extras = {};
        if (typeof key === "object") {
            for (var i = 0; i < key.length; i++) {
                extras[key[i]] = message;
            }
        } else {
            extras[key] = message;
        }
        error.description = message;
        error.extras = extras;
        return error;
    }
    function validateData(requiredKeys, data, errors) {
        for (var i = 0; i < requiredKeys.length; i++) {
            var key = requiredKeys[i];
            if (!data || !(key in data) || !data[key]) {
                errors.push(buildErrorObject(key, "Invalid field [" + key + '] - Missing field "' + key + '"'));
            }
        }
    }
    function validate(details, requiredKeys, validationMethod) {
        var errors = [];
        validateData(requiredKeys, details, errors);
        var additionalErrors = validationMethod(details);
        errors = errors.concat(additionalErrors);
        for (var i = 0; i < errors.length; i++) {
            errors[i].status = "Bad Request";
            errors[i].category_code = "request";
            errors[i].additional = null;
            errors[i].status_code = 400;
            errors[i].category_type = "request";
        }
        return errors;
    }
    function noDataError(callback, message) {
        var m = message ? message : "No data supplied";
        if (!callback) {
            throw m;
        } else {
            callback({
                errors: [ {
                    description: m,
                    status: "Bad Request",
                    category_code: "request",
                    additional: null,
                    status_code: 400,
                    category_type: "request",
                    extras: {}
                } ],
                status_code: 400
            });
        }
    }
    var cc = {
        isCardNumberValid: function(cardNumber) {
            if (!cardNumber) {
                return false;
            }
            cardNumber = (cardNumber + "").replace(/\D+/g, "").split("").reverse();
            if (!cardNumber.length || cardNumber.length < 12) {
                return false;
            }
            var total = 0, i;
            for (i = 0; i < cardNumber.length; i++) {
                cardNumber[i] = parseInt(cardNumber[i], 10);
                total += i % 2 ? 2 * cardNumber[i] - (cardNumber[i] > 4 ? 9 : 0) : cardNumber[i];
            }
            return total % 10 === 0;
        },
        cardType: function(cardNumber) {
            var p = {};
            p["51"] = p["52"] = p["53"] = p["54"] = p["55"] = "Mastercard";
            p["34"] = p["37"] = "American Express";
            p["4"] = "VISA";
            p["6"] = "Discover Card";
            p["35"] = "JCB";
            p["30"] = p["36"] = p["38"] = "Diners Club";
            if (cardNumber) {
                cardNumber = cardNumber.toString().trim();
                for (var k in p) {
                    if (cardNumber.indexOf(k) === 0) {
                        return p[k];
                    }
                }
            }
            return null;
        },
        isCVVValid: function(cardNumber, cvv) {
            var cardType = cc.cardType(cardNumber);
            if (!cardType) {
                return false;
            }
            var requiredLength = cardType === "American Express" ? 4 : 3;
            if (typeof cvv === "string" || typeof cvv === "number") {
                if (cvv.toString().replace(/\D+/g, "").length === requiredLength) {
                    return true;
                }
            }
            return false;
        },
        isSecurityCodeValid: function(cardNumber, securityCode) {
            return cc.isCVVValid(cardNumber, securityCode);
        },
        isExpiryValid: function(expiryMonth, expiryYear) {
            if (!expiryMonth || !expiryYear) {
                return false;
            }
            expiryMonth = parseInt(expiryMonth, 10);
            expiryYear = parseInt(expiryYear, 10);
            if (isNaN(expiryMonth) || isNaN(expiryYear) || expiryMonth > 12 || expiryMonth < 1) {
                return false;
            }
            var today = new Date();
            return !(today.getFullYear() > expiryYear || today.getFullYear() === expiryYear && today.getMonth() >= expiryMonth);
        },
        validate: function(cardData) {
            if (cardData.number) {
                cardData.number = cardData.number.toString().trim();
            }
            var number = cardData.number, cvv = cardData.cvv, expiryMonth = cardData.expiration_month, expiryYear = cardData.expiration_year;
            var errors = [];
            if (!cc.isCardNumberValid(number)) {
                errors.push(buildErrorObject("number", 'Invalid field [number] - "' + number + '" is not a valid credit card number'));
            }
            if (typeof cvv !== "undefined" && cvv !== null && !cc.isCVVValid(number, cvv)) {
                errors.push(buildErrorObject("cvv", 'Invalid field [cvv] - "' + cvv + '" is not a valid credit card security code'));
            }
            if (!cc.isExpiryValid(expiryMonth, expiryYear)) {
                errors.push(buildErrorObject([ "expiration_month", "expiration_year" ], 'Invalid field [expiration_month,expiration_year] - "' + expiryMonth + "-" + expiryYear + '" is not a valid credit card expiration date'));
            }
            return errors;
        },
        create: function(data, callback) {
            if (!data) {
                noDataError(callback);
                return;
            }
            var requiredKeys = [ "number", "expiration_month", "expiration_year" ];
            var errors = validate(data, requiredKeys, cc.validate);
            if (!isEmpty(errors)) {
                callback({
                    errors: errors,
                    status_code: 400
                });
            } else {
                jsonp(make_url("/jsonp/cards", preparePayload(data)), make_callback(callback));
            }
        }
    };
    var em = {
        validate: function(emailAddress) {
            if (emailAddress && emailAddress.match(/[a-z0-9!#$%&'*+\/=?\^_`{|}~\-]+(?:\.[a-z0-9!#$%&'*+\/=?\^_`{|}~\-]+)*@(?:[a-z0-9](?:[a-z0-9\-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9\-]*[a-z0-9])?/i)) {
                return true;
            }
            return false;
        }
    };
    var ba = {
        types: [ "savings", "checking" ],
        validate: function(accountData) {
            var noun = "routing_number" in accountData ? "routing_number" : "bank_code";
            var bankCode = accountData[noun];
            var errors = [];
            if (!ba.isRoutingNumberValid(bankCode)) {
                errors.push(buildErrorObject(noun, "Invalid field [" + noun + '] - "' + bankCode + '" is not a valid ' + noun.replace("_", " ")));
            }
            var account_type = accountData.type || accountData.account_type;
            var type_name = "type" in accountData ? "type" : "account_type";
            if (accountData[type_name] && !ba.validateType(account_type)) {
                errors.push(buildErrorObject(type_name, "Invalid field [" + type_name + '] - "' + account_type + '" must be one of: "' + ba.types.join('", "') + '"'));
            }
            return errors;
        },
        isRoutingNumberValid: function(routingNumber) {
            if (!routingNumber) {
                return false;
            }
            routingNumber = routingNumber.toString().match(/\d+/g);
            if (!routingNumber) {
                return false;
            }
            routingNumber = routingNumber.join("");
            if (!routingNumber || routingNumber.length !== 9) {
                return false;
            }
            var a = routingNumber.toString().split("");
            var d = [];
            for (var i = 0; i < a.length; i++) {
                d.push(parseInt(a[i], 10));
            }
            return d[8] === (7 * (d[0] + d[3] + d[6]) + 3 * (d[1] + d[4] + d[7]) + 9 * (d[2] + d[5])) % 10;
        },
        validateRoutingNumber: function(routingNumber) {
            return ba.isRoutingNumberValid(routingNumber);
        },
        lookupRoutingNumber: function(routingNumber, callback) {
            if (!routingNumber) {
                noDataError(callback);
                return;
            }
            var uri = "/bank_accounts/routing_numbers/" + routingNumber;
            jsonp(make_url(uri, null, make_callback(callback)));
        },
        validateType: function(type) {
            if (!type) {
                return true;
            }
            return ba.types.indexOf(type) >= 0;
        },
        create: function(data, callback) {
            if (!data) {
                noDataError(callback);
                return;
            }
            var requiredKeys = [ "name", "account_number", "routing_number" ];
            var errors = validate(data, requiredKeys, ba.validate);
            if (!isEmpty(errors)) {
                callback({
                    errors: errors,
                    status_code: 400
                });
            } else {
                if ("type" in data && !"account_type" in data) {
                    data.account_type = data.type;
                }
                jsonp(make_url("/jsonp/bank_accounts", preparePayload(data)), make_callback(callback));
            }
        }
    };
    var ea = {
        providers: {
            coinbase: {
                url: "https://coinbase.com/oauth/authorize",
                params: {
                    response_type: "code",
                    client_id: "050f4c85231a51c147a3fb011e012755d81cdb499cc50b5354b7bcdf9bf805ad",
                    redirect_uri: "https://js.balancedpayments.com/callback.html",
                    scope: "send"
                }
            }
        },
        provider_name: null,
        callback: null,
        create: function(provider_name, callback) {
            var provider = ea.providers[provider_name];
            var url = provider.url;
            var params = provider.params;
            var params_array = [];
            ea.provider_name = provider_name;
            ea.callback = callback;
            if (params) {
                for (var param in params) {
                    params_array.push(param + "=" + params[param]);
                }
                url = url + "?" + params_array.join("&");
            }
            dialog = window.open(url, "", "top=" + (window.outerHeight / 2 - 275) + ", left=" + (window.outerWidth / 2 - 250) + ", width=500, height=550");
        },
        configure: function() {}
    };
    addEvent(window, "message", function(event) {
        if (event.origin !== "https://js.balancedpayments.com") {
            return;
        }
        if (!event.data) {
            noDataError(ea.callback);
            return;
        }
        var data = event.data;
        data.token = event.data.token || event.data.code;
        data.provider = ea.provider_name;
        jsonp(make_url("/jsonp/external_accounts", data), make_callback(ea.callback));
        if (dialog) dialog.close();
    });
    function jsonp(path, callback) {
        var funct = "balanced_jsonp_" + Math.random().toString().substr(2);
        var tag = document.createElement("script");
        tag.type = "text/javascript";
        tag.async = true;
        tag.src = path.replace("{callback}", funct);
        var where = document.getElementsByTagName("script")[0];
        where.parentNode.insertBefore(tag, where);
        window[funct] = function(result) {
            try {
                callback(result);
            } catch (e) {
                if (typeof console !== "undefined" && console.error) {
                    console.error(e);
                }
            }
            tag.parentNode.removeChild(tag);
        };
    }
    function make_url(path, data) {
        return root_url + path + "?callback={callback}&data=" + encodeURIComponent(JSON.stringify(data));
    }
    function make_callback(callback) {
        var called_back = false;
        function ret(data) {
            if (called_back) {
                return;
            }
            called_back = true;
            if (!data || !data.status) {
                callback({
                    errors: [ {
                        description: "Unable to connect to the balanced servers",
                        status: "Internal Server Error",
                        category_code: "server-error",
                        additional: null,
                        status_code: 500,
                        category_type: "server-error",
                        extras: {}
                    } ],
                    status_code: 500
                });
                return;
            }
            var body = JSON.parse(data.body);
            if (typeof data.status !== "undefined") {
                body.status_code = data.status;
            }
            callback(body);
        }
        setTimeout(ret, 6e4);
        return ret;
    }
    if (typeof JSON !== "object") {
        jsonp("https://js.balancedpayments.com/json2.js");
    }
    var balanced = global.balanced = {
        card: cc,
        bankAccount: ba,
        externalAccount: ea,
        emailAddress: em,
        init: function(args) {
            if (typeof args == "string") {
                marketplace_href = args;
            } else {
                if (args && "server" in args) {
                    root_url = args.server;
                }
                if (args && "marketplace_href" in args) {
                    marketplace_href = args.marketplace_href;
                }
                if (args && "providers" in args) {
                    ea.providers = args.providers;
                }
            }
        }
    };
})({}, function() {
    return this;
}());
"use strict";

 angular.module("config", [])

.constant("ENV", {
  "emptyImageDataUri": "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==",
  "idleTimeoutInSeconds": 1800,
  "idlePollIntervalMillis": 5000,
  "assertsEnabled": true,
  "name": "local",
  "apiEndpoint": "http://127.0.0.1:8000",
  "sentryURL": "",
  "enableRealtimeConfig": true,
  "logging": {
    "level": "all"
  }
})

;
/*
International Telephone Input v2.0.10
https://github.com/Bluefieldscom/intl-tel-input.git
*/
// wrap in UMD - see https://github.com/umdjs/umd/blob/master/jqueryPlugin.js
(function(factory) {
    if (typeof define === "function" && define.amd) {
        define([ "jquery" ], function($) {
            factory($, window, document);
        });
    } else {
        factory(jQuery, window, document);
    }
})(function($, window, document, undefined) {
    "use strict";
    var pluginName = "intlTelInput", id = 1, // give each instance it's own id for namespaced event handling
    defaults = {
        // automatically format the number according to the selected country
        autoFormat: true,
        // if there is just a dial code in the input: remove it on blur, and re-add it on focus
        autoHideDialCode: true,
        // default country
        defaultCountry: "",
        // don't insert international dial codes
        nationalMode: false,
        // display only these countries
        onlyCountries: [],
        // the countries at the top of the list. defaults to united states and united kingdom
        preferredCountries: [ "us", "gb" ],
        // make the dropdown the same width as the input
        responsiveDropdown: false,
        // specify the path to the libphonenumber script to enable validation
        validationScript: ""
    }, keys = {
        UP: 38,
        DOWN: 40,
        ENTER: 13,
        ESC: 27,
        PLUS: 43,
        A: 65,
        Z: 90,
        ZERO: 48,
        NINE: 57,
        SPACE: 32,
        BSPACE: 8,
        DEL: 46,
        CTRL: 17,
        CMD1: 91,
        // Chrome
        CMD2: 224
    }, windowLoaded = false;
    // keep track of if the window.load event has fired as impossible to check after the fact
    $(window).load(function() {
        windowLoaded = true;
    });
    function Plugin(element, options) {
        this.element = element;
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        // event namespace
        this.ns = "." + pluginName + id++;
        // Chrome, FF, Safari, IE9+
        this.isGoodBrowser = Boolean(element.setSelectionRange);
        this._name = pluginName;
        this.init();
    }
    Plugin.prototype = {
        init: function() {
            // if in nationalMode, disable options relating to dial codes
            if (this.options.nationalMode) {
                this.options.autoFormat = this.options.autoHideDialCode = false;
            }
            // chrome on android has issues with key events
            // backspace issues for inputs with type=text: https://code.google.com/p/chromium/issues/detail?id=184812
            // and improper key codes for keyup and keydown: https://code.google.com/p/chromium/issues/detail?id=118639
            if (navigator.userAgent.match(/Android/i) && navigator.userAgent.match(/Chrome/i)) {
                this.options.autoFormat = false;
            }
            // process all the data: onlyCounties, preferredCountries, defaultCountry etc
            this._processCountryData();
            // generate the markup
            this._generateMarkup();
            // set the initial state of the input value and the selected flag
            this._setInitialState();
            // start all of the event listeners: autoHideDialCode, input keydown, selectedFlag click
            this._initListeners();
        },
        /********************
     *  PRIVATE METHODS
     ********************/
        // prepare all of the country data, including onlyCountries, preferredCountries and
        // defaultCountry options
        _processCountryData: function() {
            // set the instances country data objects
            this._setInstanceCountryData();
            // set the preferredCountries property
            this._setPreferredCountries();
        },
        // process onlyCountries array if present
        _setInstanceCountryData: function() {
            var that = this;
            if (this.options.onlyCountries.length) {
                var newCountries = [], newCountryCodes = {}, dialCode, i;
                for (i = 0; i < this.options.onlyCountries.length; i++) {
                    var countryCode = this.options.onlyCountries[i], countryData = that._getCountryData(countryCode, true, false);
                    if (countryData) {
                        newCountries.push(countryData);
                        // add this country's dial code to the countryCodes
                        dialCode = countryData.dialCode;
                        if (newCountryCodes[dialCode]) {
                            newCountryCodes[dialCode].push(countryCode);
                        } else {
                            newCountryCodes[dialCode] = [ countryCode ];
                        }
                    }
                }
                // maintain country priority
                for (dialCode in newCountryCodes) {
                    if (newCountryCodes[dialCode].length > 1) {
                        var sortedCountries = [];
                        // go through all of the allCountryCodes countries for this dialCode and create a new (ordered) array of values (if they're in the newCountryCodes array)
                        for (i = 0; i < allCountryCodes[dialCode].length; i++) {
                            var country = allCountryCodes[dialCode][i];
                            if ($.inArray(newCountryCodes[dialCode], country)) {
                                sortedCountries.push(country);
                            }
                        }
                        newCountryCodes[dialCode] = sortedCountries;
                    }
                }
                this.countries = newCountries;
                this.countryCodes = newCountryCodes;
            } else {
                this.countries = allCountries;
                this.countryCodes = allCountryCodes;
            }
        },
        // process preferred countries - iterate through the preferences,
        // fetching the country data for each one
        _setPreferredCountries: function() {
            var that = this;
            this.preferredCountries = [];
            for (var i = 0; i < this.options.preferredCountries.length; i++) {
                var countryCode = this.options.preferredCountries[i], countryData = that._getCountryData(countryCode, false, true);
                if (countryData) {
                    that.preferredCountries.push(countryData);
                }
            }
        },
        // generate all of the markup for the plugin: the selected flag overlay, and the dropdown
        _generateMarkup: function() {
            // telephone input
            this.telInput = $(this.element);
            // containers (mostly for positioning)
            this.telInput.wrap($("<div>", {
                "class": "intl-tel-input"
            }));
            var flagsContainer = $("<div>", {
                "class": "flag-dropdown"
            }).insertAfter(this.telInput);
            // currently selected flag (displayed to left of input)
            var selectedFlag = $("<div>", {
                "class": "selected-flag"
            }).appendTo(flagsContainer);
            this.selectedFlagInner = $("<div>", {
                "class": "flag"
            }).appendTo(selectedFlag);
            // CSS triangle
            $("<div>", {
                "class": "arrow"
            }).appendTo(this.selectedFlagInner);
            // country list contains: preferred countries, then divider, then all countries
            this.countryList = $("<ul>", {
                "class": "country-list v-hide"
            }).appendTo(flagsContainer);
            if (this.preferredCountries.length) {
                this._appendListItems(this.preferredCountries, "preferred");
                $("<li>", {
                    "class": "divider"
                }).appendTo(this.countryList);
            }
            this._appendListItems(this.countries, "");
            // now we can grab the dropdown height, and hide it properly
            this.dropdownHeight = this.countryList.outerHeight();
            this.countryList.removeClass("v-hide").addClass("hide");
            // and set the width
            if (this.options.responsiveDropdown) {
                this.countryList.outerWidth(this.telInput.outerWidth());
            }
            // this is useful in lots of places
            this.countryListItems = this.countryList.children(".country");
        },
        // add a country <li> to the countryList <ul> container
        _appendListItems: function(countries, className) {
            // we create so many DOM elements, I decided it was faster to build a temp string
            // and then add everything to the DOM in one go at the end
            var tmp = "";
            // for each country
            for (var i = 0; i < countries.length; i++) {
                var c = countries[i];
                // open the list item
                tmp += "<li class='country " + className + "' data-dial-code='" + c.dialCode + "' data-country-code='" + c.iso2 + "'>";
                // add the flag
                tmp += "<div class='flag " + c.iso2 + "'></div>";
                // and the country name and dial code
                tmp += "<span class='country-name'>" + c.name + "</span>";
                tmp += "<span class='dial-code'>+" + c.dialCode + "</span>";
                // close the list item
                tmp += "</li>";
            }
            this.countryList.append(tmp);
        },
        // set the initial state of the input value and the selected flag
        _setInitialState: function() {
            var val = this.telInput.val();
            // if the input is not pre-populated, or if it doesn't contain a valid dial code, fall back to the default country
            // Note: calling setNumber will also format the number
            if (!val || !this.setNumber(val)) {
                // flag is not set, so set to the default country
                var defaultCountry;
                // check the defaultCountry option, else fall back to the first in the list
                if (this.options.defaultCountry) {
                    defaultCountry = this._getCountryData(this.options.defaultCountry, false, false);
                } else {
                    defaultCountry = this.preferredCountries.length ? this.preferredCountries[0] : this.countries[0];
                }
                this._selectFlag(defaultCountry.iso2);
                // if autoHideDialCode is disabled, insert the default dial code
                if (!this.options.autoHideDialCode) {
                    this._resetToDialCode(defaultCountry.dialCode);
                }
            }
        },
        // initialise the main event listeners: input keydown, and click selected flag
        _initListeners: function() {
            var that = this;
            // auto hide dial code option
            if (this.options.autoHideDialCode) {
                this._initAutoHideDialCode();
            }
            // hack for input nested inside label: clicking the selected-flag to open the dropdown would then automatically trigger a 2nd click on the input which would close it again
            var label = this.telInput.closest("label");
            if (label.length) {
                label.on("click" + this.ns, function(e) {
                    // if the dropdown is closed, then focus the input, else ignore the click
                    if (that.countryList.hasClass("hide")) {
                        that.telInput.focus();
                    } else {
                        e.preventDefault();
                    }
                });
            }
            if (this.options.autoFormat) {
                // use keydown to prevent deleting the '+' prefix
                this.telInput.on("keydown" + this.ns, function(e) {
                    if ((e.which == keys.BSPACE || e.which == keys.DEL) && that.telInput.val() == "+") {
                        e.preventDefault();
                    }
                });
                // format number and update flag on keypress
                // use keypress event as we want to ignore all input except for a select few keys,
                // but we dont want to ignore the navigation keys like the arrows etc.
                // NOTE: no point in refactoring this to only bind these listeners on focus/blur because then you would need to have those 2 listeners running the whole time anyway...
                this.telInput.on("keypress" + this.ns, function(e) {
                    // 32 is space, and after that it's all chars (not meta/nav keys)
                    // this fix is needed for Firefox, which triggers keypress event for some meta/nav keys
                    if (e.which >= keys.SPACE) {
                        e.preventDefault();
                        // allowed keys are now just numeric keys
                        var isAllowed = e.which >= keys.ZERO && e.which <= keys.NINE, input = that.telInput[0], noSelection = that.isGoodBrowser && input.selectionStart == input.selectionEnd;
                        // still reformat even if not an allowed key as they could by typing a formatting char, but ignore if there's a selection as doesn't make sense to replace selection with illegal char and then immediately remove it
                        if (isAllowed || noSelection) {
                            var newChar = isAllowed ? String.fromCharCode(e.which) : null;
                            that._handleInputKey(newChar, false);
                        }
                    }
                });
            }
            // handle keyup event
            // for autoFormat: we use keyup to catch delete events after the fact
            this.telInput.on("keyup" + this.ns, function(e) {
                if (that.options.autoFormat) {
                    var isCtrl = e.which == keys.CTRL || e.which == keys.CMD1 || e.which == keys.CMD2, input = that.telInput[0], noSelection = that.isGoodBrowser && input.selectionStart == input.selectionEnd, cursorAtEnd = that.isGoodBrowser && input.selectionStart == that.telInput.val().length;
                    // if delete: format with suffix
                    // if backspace: format (if cursorAtEnd: no suffix)
                    // if ctrl and no selection (i.e. could be paste): format with suffix
                    if (e.which == keys.DEL || e.which == keys.BSPACE || isCtrl && noSelection) {
                        var preventFormatSuffix = e.which == keys.BSPACE && cursorAtEnd;
                        that._handleInputKey(null, preventFormatSuffix);
                    }
                    // if at the end the input is empty, then re-add the plus
                    if (!that.telInput.val()) {
                        that.telInput.val("+");
                    }
                } else {
                    // if no autoFormat, just update flag
                    that._updateFlag();
                }
            });
            // toggle country dropdown on click
            var selectedFlag = this.selectedFlagInner.parent();
            selectedFlag.on("click" + this.ns, function(e) {
                // only intercept this event if we're opening the dropdown
                // else let it bubble up to the top ("click-off-to-close" listener)
                // we cannot just stopPropagation as it may be needed to close another instance
                if (that.countryList.hasClass("hide") && !that.telInput.prop("disabled")) {
                    that._showDropdown();
                }
            });
            // if the user has specified the path to the validation script
            // inject a new script element for it at the end of the body
            if (this.options.validationScript) {
                var injectValidationScript = function() {
                    var script = document.createElement("script");
                    script.type = "text/javascript";
                    script.src = that.options.validationScript;
                    document.body.appendChild(script);
                };
                // if the plugin is being initialised after the window.load event has already been fired
                if (windowLoaded) {
                    injectValidationScript();
                } else {
                    // wait until the load event so we don't block any other requests e.g. the flags image
                    $(window).load(injectValidationScript);
                }
            }
        },
        // handle various key events on the input: the 2 main situations are 1) adding a new number character, which will replace any selection, reformat, and try to preserve the cursor position. and 2) reformatting on backspace, or paste event
        _handleInputKey: function(newNumericChar, preventFormatSuffix) {
            var val = this.telInput.val(), newCursor = null, cursorAtEnd = false, // raw DOM element
            input = this.telInput[0];
            if (this.isGoodBrowser) {
                var selectionEnd = input.selectionEnd, originalLen = val.length;
                cursorAtEnd = selectionEnd == originalLen;
                // if handling a new number character: insert it in the right place and calculate the new cursor position
                if (newNumericChar) {
                    // replace any selection they may have made with the new char
                    val = val.substring(0, input.selectionStart) + newNumericChar + val.substring(selectionEnd, originalLen);
                    // if the cursor was not at the end then calculate it's new pos
                    if (!cursorAtEnd) {
                        newCursor = selectionEnd + (val.length - originalLen);
                    }
                } else {
                    // here we're not handling a new char, we're just doing a re-format, but we still need to maintain the cursor position
                    newCursor = input.selectionStart;
                }
            } else if (newNumericChar) {
                val += newNumericChar;
            }
            // update the number and flag
            this.setNumber(val, preventFormatSuffix);
            // update the cursor position
            if (this.isGoodBrowser) {
                // if it was at the end, keep it there
                if (cursorAtEnd) {
                    newCursor = this.telInput.val().length;
                }
                input.setSelectionRange(newCursor, newCursor);
            }
        },
        // on focus: if empty add dial code. on blur: if just dial code, then empty it
        _initAutoHideDialCode: function() {
            var that = this;
            // mousedown decides where the cursor goes, so if we're focusing
            // we must preventDefault as we'll be inserting the dial code,
            // and we want the cursor to be at the end no matter where they click
            this.telInput.on("mousedown" + this.ns, function(e) {
                if (!that.telInput.is(":focus") && !that.telInput.val()) {
                    e.preventDefault();
                    // but this also cancels the focus, so we must trigger that manually
                    that.telInput.focus();
                }
            });
            // on focus: if empty, insert the dial code for the currently selected flag
            this.telInput.on("focus" + this.ns, function() {
                if (!that.telInput.val()) {
                    that._updateVal("+" + that.selectedCountryData.dialCode, true);
                    // after auto-inserting a dial code, if the first key they hit is '+' then assume
                    // they are entering a new number, so remove the dial code.
                    // use keypress instead of keydown because keydown gets triggered for the shift key
                    // (required to hit the + key), and instead of keyup because that shows the new '+'
                    // before removing the old one
                    that.telInput.one("keypress.plus" + that.ns, function(e) {
                        if (e.which == keys.PLUS) {
                            that.telInput.val("+");
                        }
                    });
                    // after tabbing in, make sure the cursor is at the end
                    // we must use setTimeout to get outside of the focus handler as it seems the 
                    // selection happens after that
                    setTimeout(function() {
                        that._cursorToEnd();
                    });
                }
            });
            // on blur: if just a dial code then remove it
            this.telInput.on("blur" + this.ns, function() {
                var value = that.telInput.val(), startsPlus = value.substring(0, 1) == "+";
                if (startsPlus) {
                    var numeric = value.replace(/\D/g, ""), clean = "+" + numeric;
                    // if just a plus, or if just a dial code
                    if (!numeric || that.selectedCountryData.dialCode == numeric) {
                        that.telInput.val("");
                    }
                }
                // remove the keypress listener we added on focus
                that.telInput.off("keypress.plus" + that.ns);
            });
        },
        // put the cursor to the end of the input (usually after a focus event)
        _cursorToEnd: function() {
            var input = this.telInput[0];
            if (this.isGoodBrowser) {
                var len = this.telInput.val().length;
                input.setSelectionRange(len, len);
            }
        },
        // show the dropdown
        _showDropdown: function() {
            this._setDropdownPosition();
            // update highlighting and scroll to active list item
            var activeListItem = this.countryList.children(".active");
            this._highlightListItem(activeListItem);
            // show it
            this.countryList.removeClass("hide");
            this._scrollTo(activeListItem);
            // bind all the dropdown-related listeners: mouseover, click, click-off, keydown
            this._bindDropdownListeners();
            // update the arrow
            this.selectedFlagInner.children(".arrow").addClass("up");
        },
        // decide where to position dropdown (depends on position within viewport, and scroll)
        _setDropdownPosition: function() {
            var inputTop = this.telInput.offset().top, windowTop = $(window).scrollTop(), // dropdownFitsBelow = (dropdownBottom < windowBottom)
            dropdownFitsBelow = inputTop + this.telInput.outerHeight() + this.dropdownHeight < windowTop + $(window).height(), dropdownFitsAbove = inputTop - this.dropdownHeight > windowTop;
            // dropdownHeight - 1 for border
            var cssTop = !dropdownFitsBelow && dropdownFitsAbove ? "-" + (this.dropdownHeight - 1) + "px" : "";
            this.countryList.css("top", cssTop);
        },
        // we only bind dropdown listeners when the dropdown is open
        _bindDropdownListeners: function() {
            var that = this;
            // when mouse over a list item, just highlight that one
            // we add the class "highlight", so if they hit "enter" we know which one to select
            this.countryList.on("mouseover" + this.ns, ".country", function(e) {
                that._highlightListItem($(this));
            });
            // listen for country selection
            this.countryList.on("click" + this.ns, ".country", function(e) {
                that._selectListItem($(this));
            });
            // click off to close
            // (except when this initial opening click is bubbling up)
            // we cannot just stopPropagation as it may be needed to close another instance
            var isOpening = true;
            $("html").on("click" + this.ns, function(e) {
                if (!isOpening) {
                    that._closeDropdown();
                }
                isOpening = false;
            });
            // listen for up/down scrolling, enter to select, or letters to jump to country name.
            // use keydown as keypress doesn't fire for non-char keys and we want to catch if they
            // just hit down and hold it to scroll down (no keyup event).
            // listen on the document because that's where key events are triggered if no input has focus
            var query = "", queryTimer = null;
            $(document).on("keydown" + this.ns, function(e) {
                // prevent down key from scrolling the whole page,
                // and enter key from submitting a form etc
                e.preventDefault();
                if (e.which == keys.UP || e.which == keys.DOWN) {
                    // up and down to navigate
                    that._handleUpDownKey(e.which);
                } else if (e.which == keys.ENTER) {
                    // enter to select
                    that._handleEnterKey();
                } else if (e.which == keys.ESC) {
                    // esc to close
                    that._closeDropdown();
                } else if (e.which >= keys.A && e.which <= keys.Z || e.which == keys.SPACE) {
                    // upper case letters (note: keyup/keydown only return upper case letters)
                    // jump to countries that start with the query string
                    if (queryTimer) {
                        clearTimeout(queryTimer);
                    }
                    query += String.fromCharCode(e.which);
                    that._searchForCountry(query);
                    // if the timer hits 1 second, reset the query
                    queryTimer = setTimeout(function() {
                        query = "";
                    }, 1e3);
                }
            });
        },
        // highlight the next/prev item in the list (and ensure it is visible)
        _handleUpDownKey: function(key) {
            var current = this.countryList.children(".highlight").first();
            var next = key == keys.UP ? current.prev() : current.next();
            if (next.length) {
                // skip the divider
                if (next.hasClass("divider")) {
                    next = key == keys.UP ? next.prev() : next.next();
                }
                this._highlightListItem(next);
                this._scrollTo(next);
            }
        },
        // select the currently highlighted item
        _handleEnterKey: function() {
            var currentCountry = this.countryList.children(".highlight").first();
            if (currentCountry.length) {
                this._selectListItem(currentCountry);
            }
        },
        // find the first list item whose name starts with the query string
        _searchForCountry: function(query) {
            for (var i = 0; i < this.countries.length; i++) {
                if (this._startsWith(this.countries[i].name, query)) {
                    var listItem = this.countryList.children("[data-country-code=" + this.countries[i].iso2 + "]").not(".preferred");
                    // update highlighting and scroll
                    this._highlightListItem(listItem);
                    this._scrollTo(listItem, true);
                    break;
                }
            }
        },
        // check if (uppercase) string a starts with string b
        _startsWith: function(a, b) {
            return a.substr(0, b.length).toUpperCase() == b;
        },
        // update the input's value to the given val
        // if autoFormat=true, format it first according to the country-specific formatting rules
        _updateVal: function(val, hasDialCode, preventFormatSuffix) {
            var formatted = "", pure;
            if (this.options.autoFormat) {
                pure = val.replace(/\D/g, "");
                // only bother trying to format a number with a dialCode
                if (hasDialCode) {
                    // format the number
                    var format = this.selectedCountryData.format;
                    if (format) {
                        for (var i = 0; i < format.length; i++) {
                            // if the format character is a dot, add the number
                            if (format[i] == ".") {
                                // only break out of looping over the formatted chars when we need to insert another digit, but we cant
                                if (!pure) {
                                    break;
                                }
                                formatted += pure.substring(0, 1);
                                pure = pure.substring(1);
                                // in the case of a backspace event, stop at the last digit - dont add any extra formatting chars afterwards
                                if (!pure && preventFormatSuffix) {
                                    break;
                                }
                            } else {
                                formatted += format[i];
                            }
                        }
                    }
                }
                // if no formatted version, we're left with the "pure" numbers, so just make sure to preserve any initial '+'
                if (!formatted && val.substring(0, 1) == "+") {
                    formatted = "+";
                }
            } else {
                // no autoFormat, so just insert the true value
                pure = val;
            }
            this.telInput.val(formatted + pure);
        },
        // update the selected flag
        _updateFlag: function(number) {
            // try and extract valid dial code from input
            var dialCode = this._getDialCode(number);
            if (dialCode) {
                // check if one of the matching countries is already selected
                var countryCodes = this.countryCodes[dialCode.replace(/\D/g, "")], alreadySelected = false;
                // countries with area codes: we must always update the flag as if it's not an exact match
                // we should always default to the first country in the list. This is to avoid having to
                // explicitly define every possible area code in America (there are 999 possible area codes)
                if (!this.selectedCountryData || !this.selectedCountryData.hasAreaCodes) {
                    for (var i = 0; i < countryCodes.length; i++) {
                        if (this.selectedFlagInner.hasClass(countryCodes[i])) {
                            alreadySelected = true;
                        }
                    }
                }
                // else choose the first in the list
                if (!alreadySelected) {
                    this._selectFlag(countryCodes[0]);
                }
            }
            return dialCode;
        },
        // reset the input value to just a dial code
        _resetToDialCode: function(dialCode) {
            // if nationalMode is enabled then don't insert the dial code
            var value = this.options.nationalMode ? "" : "+" + dialCode;
            this.telInput.val(value);
        },
        // remove highlighting from other list items and highlight the given item
        _highlightListItem: function(listItem) {
            this.countryListItems.removeClass("highlight");
            listItem.addClass("highlight");
        },
        // find the country data for the given country code
        // the ignoreOnlyCountriesOption is only used during init() while parsing the onlyCountries array
        _getCountryData: function(countryCode, ignoreOnlyCountriesOption, allowFail) {
            var countryList = ignoreOnlyCountriesOption ? allCountries : this.countries;
            for (var i = 0; i < countryList.length; i++) {
                if (countryList[i].iso2 == countryCode) {
                    return countryList[i];
                }
            }
            if (allowFail) {
                return null;
            } else {
                throw new Error("No country data for '" + countryCode + "'");
            }
        },
        // update the selected flag and the active list item
        _selectFlag: function(countryCode) {
            this.selectedFlagInner.attr("class", "flag " + countryCode);
            // update the title attribute
            this.selectedCountryData = this._getCountryData(countryCode, false, false);
            var title = this.selectedCountryData.name + ": +" + this.selectedCountryData.dialCode;
            this.selectedFlagInner.parent().attr("title", title);
            // update the active list item
            var listItem = this.countryListItems.children(".flag." + countryCode).first().parent();
            this.countryListItems.removeClass("active");
            listItem.addClass("active");
        },
        // called when the user selects a list item from the dropdown
        _selectListItem: function(listItem) {
            // update selected flag and active list item
            var countryCode = listItem.attr("data-country-code");
            this._selectFlag(countryCode);
            this._closeDropdown();
            // update input value
            if (!this.options.nationalMode) {
                this._updateDialCode("+" + listItem.attr("data-dial-code"));
            }
            // always fire the change event as even if nationalMode=true (and we haven't updated
            // the input val), the system as a whole has still changed - see country-sync example
            this.telInput.trigger("change");
            // focus the input
            this.telInput.focus();
            this._cursorToEnd();
        },
        // close the dropdown and unbind any listeners
        _closeDropdown: function() {
            this.countryList.addClass("hide");
            // update the arrow
            this.selectedFlagInner.children(".arrow").removeClass("up");
            // unbind key events
            $(document).off(this.ns);
            // unbind click-off-to-close
            $("html").off(this.ns);
            // unbind hover and click listeners
            this.countryList.off(this.ns);
        },
        // check if an element is visible within it's container, else scroll until it is
        _scrollTo: function(element, middle) {
            var container = this.countryList, containerHeight = container.height(), containerTop = container.offset().top, containerBottom = containerTop + containerHeight, elementHeight = element.outerHeight(), elementTop = element.offset().top, elementBottom = elementTop + elementHeight, newScrollTop = elementTop - containerTop + container.scrollTop(), middleOffset = containerHeight / 2 - elementHeight / 2;
            if (elementTop < containerTop) {
                // scroll up
                if (middle) {
                    newScrollTop -= middleOffset;
                }
                container.scrollTop(newScrollTop);
            } else if (elementBottom > containerBottom) {
                // scroll down
                if (middle) {
                    newScrollTop += middleOffset;
                }
                var heightDifference = containerHeight - elementHeight;
                container.scrollTop(newScrollTop - heightDifference);
            }
        },
        // replace any existing dial code with the new one
        _updateDialCode: function(newDialCode) {
            var inputVal = this.telInput.val(), prevDialCode = this._getDialCode(), newNumber;
            // if the previous number contained a valid dial code, replace it
            // (if more than just a plus character)
            if (prevDialCode.length > 1) {
                newNumber = inputVal.replace(prevDialCode, newDialCode);
            } else {
                // if the previous number didn't contain a dial code, we should persist it
                var existingNumber = inputVal && inputVal.substr(0, 1) != "+" ? $.trim(inputVal) : "";
                newNumber = newDialCode + existingNumber;
            }
            this._updateVal(newNumber, true);
        },
        // try and extract a valid international dial code from a full telephone number
        // Note: returns the raw string inc plus character and any whitespace/dots etc
        _getDialCode: function(number) {
            var dialCode = "", inputVal = number || this.telInput.val();
            // only interested in international numbers (starting with a plus)
            if (inputVal.charAt(0) == "+") {
                var numericChars = "";
                // iterate over chars
                for (var i = 0; i < inputVal.length; i++) {
                    var c = inputVal.charAt(i);
                    // if char is number
                    if ($.isNumeric(c)) {
                        numericChars += c;
                        // if current numericChars make a valid dial code
                        if (this.countryCodes[numericChars]) {
                            // store the actual raw string (useful for matching later)
                            dialCode = inputVal.substring(0, i + 1);
                        }
                        // longest dial code is 4 chars
                        if (numericChars.length == 4) {
                            break;
                        }
                    }
                }
            }
            return dialCode;
        },
        /********************
     *  PUBLIC METHODS
     ********************/
        // remove plugin
        destroy: function() {
            // make sure the dropdown is closed (and unbind listeners)
            this._closeDropdown();
            // key events, and focus/blur events if autoHideDialCode=true
            this.telInput.off(this.ns);
            // click event to open dropdown
            this.selectedFlagInner.parent().off(this.ns);
            // label click hack
            this.telInput.closest("label").off(this.ns);
            // remove markup
            var container = this.telInput.parent();
            container.before(this.telInput).remove();
        },
        // get the country data for the currently selected flag
        getSelectedCountryData: function() {
            return this.selectedCountryData;
        },
        // validate the input val - assumes the global function isValidNumber
        // pass in true if you want to allow national numbers (no country dial code)
        isValidNumber: function(allowNational) {
            var val = $.trim(this.telInput.val()), countryCode = allowNational ? this.selectedCountryData.iso2 : "", // libphonenumber allows alpha chars, but in order to allow that, we'd need a method to retrieve the processed number, with letters replaced with numbers
            containsAlpha = /[a-zA-Z]/.test(val);
            return !containsAlpha && window.isValidNumber(val, countryCode);
        },
        // update the selected flag, and if the input is empty: insert the new dial code
        selectCountry: function(countryCode) {
            // check if already selected
            if (!this.selectedFlagInner.hasClass(countryCode)) {
                this._selectFlag(countryCode);
                if (!this.telInput.val() && !this.options.autoHideDialCode) {
                    this._resetToDialCode(this.selectedCountryData.dialCode);
                }
            }
        },
        // set the input value and update the flag
        setNumber: function(number, preventFormatSuffix) {
            // we must update the flag first, which updates this.selectedCountryData, which is used later for formatting the number before displaying it
            var dialCode = this._updateFlag(number);
            this._updateVal(number, dialCode, preventFormatSuffix);
            return dialCode;
        }
    };
    // adapted to allow public functions
    // using https://github.com/jquery-boilerplate/jquery-boilerplate/wiki/Extending-jQuery-Boilerplate
    $.fn[pluginName] = function(options) {
        var args = arguments;
        // Is the first parameter an object (options), or was omitted,
        // instantiate a new instance of the plugin.
        if (options === undefined || typeof options === "object") {
            return this.each(function() {
                if (!$.data(this, "plugin_" + pluginName)) {
                    $.data(this, "plugin_" + pluginName, new Plugin(this, options));
                }
            });
        } else if (typeof options === "string" && options[0] !== "_" && options !== "init") {
            // If the first parameter is a string and it doesn't start
            // with an underscore or "contains" the `init`-function,
            // treat this as a call to a public method.
            // Cache the method call to make it possible to return a value
            var returns;
            this.each(function() {
                var instance = $.data(this, "plugin_" + pluginName);
                // Tests that there's already a plugin-instance
                // and checks that the requested public method exists
                if (instance instanceof Plugin && typeof instance[options] === "function") {
                    // Call the method of our plugin instance,
                    // and pass it the supplied arguments.
                    returns = instance[options].apply(instance, Array.prototype.slice.call(args, 1));
                }
                // Allow instances to be destroyed via the 'destroy' method
                if (options === "destroy") {
                    $.data(this, "plugin_" + pluginName, null);
                }
            });
            // If the earlier cached method gives a value back return the value,
            // otherwise return this to preserve chainability.
            return returns !== undefined ? returns : this;
        }
    };
    /********************
   *  STATIC METHODS
   ********************/
    // get the country data object
    $.fn[pluginName].getCountryData = function() {
        return allCountries;
    };
    // set the country data object
    $.fn[pluginName].setCountryData = function(obj) {
        allCountries = obj;
    };
    // Tell JSHint to ignore this warning: "character may get silently deleted by one or more browsers"
    // jshint -W100
    // Array of country objects for the flag dropdown.
    // Each contains a name, country code (ISO 3166-1 alpha-2) and dial code.
    // Originally from https://github.com/mledoze/countries
    // then modified using the following JavaScript (NOW OUT OF DATE):
    /*
var result = [];
_.each(countries, function(c) {
  // ignore countries without a dial code
  if (c.callingCode[0].length) {
    result.push({
      // var locals contains country names with localised versions in brackets
      n: _.findWhere(locals, {
        countryCode: c.cca2
      }).name,
      i: c.cca2.toLowerCase(),
      d: c.callingCode[0]
    });
  }
});
JSON.stringify(result);
*/
    // then with a couple of manual re-arrangements to be alphabetical
    // then changed Kazakhstan from +76 to +7
    // and Vatican City from +379 to +39 (see issue 50)
    // and Caribean Netherlands from +5997 to +599
    // and Curacao from +5999 to +599
    // Removed: Åland Islands, Christmas Island, Cocos Islands, Guernsey, Isle of Man, Jersey, Kosovo, Mayotte, Pitcairn Islands, South Georgia, Svalbard, Western Sahara
    // Update: converted objects to arrays to save bytes!
    // Update: added formats for some countries
    // Update: added "priority" for countries with the same dialCode as others
    // Update: added array of area codes for countries with the same dialCode as others
    // So each country array has the following information:
    // [
    //    Country name,
    //    iso2 code,
    //    International dial code,
    //    Format (if available),
    //    Order (if >1 country with same dial code),
    //    Area codes (if >1 country with same dial code)
    // ]
    var allCountries = [ [ "Afghanistan (‫افغانستان‬‎)", "af", "93" ], [ "Albania (Shqipëri)", "al", "355" ], [ "Algeria (‫الجزائر‬‎)", "dz", "213" ], [ "American Samoa", "as", "1684" ], [ "Andorra", "ad", "376" ], [ "Angola", "ao", "244" ], [ "Anguilla", "ai", "1264" ], [ "Antigua and Barbuda", "ag", "1268" ], [ "Argentina", "ar", "54" ], [ "Armenia (Հայաստան)", "am", "374" ], [ "Aruba", "aw", "297" ], [ "Australia", "au", "61", "+.. ... ... ..." ], [ "Austria (Österreich)", "at", "43" ], [ "Azerbaijan (Azərbaycan)", "az", "994" ], [ "Bahamas", "bs", "1242" ], [ "Bahrain (‫البحرين‬‎)", "bh", "973" ], [ "Bangladesh (বাংলাদেশ)", "bd", "880" ], [ "Barbados", "bb", "1246" ], [ "Belarus (Беларусь)", "by", "375" ], [ "Belgium (België)", "be", "32", "+.. ... .. .. .." ], [ "Belize", "bz", "501" ], [ "Benin (Bénin)", "bj", "229" ], [ "Bermuda", "bm", "1441" ], [ "Bhutan (འབྲུག)", "bt", "975" ], [ "Bolivia", "bo", "591" ], [ "Bosnia and Herzegovina (Босна и Херцеговина)", "ba", "387" ], [ "Botswana", "bw", "267" ], [ "Brazil (Brasil)", "br", "55" ], [ "British Indian Ocean Territory", "io", "246" ], [ "British Virgin Islands", "vg", "1284" ], [ "Brunei", "bn", "673" ], [ "Bulgaria (България)", "bg", "359" ], [ "Burkina Faso", "bf", "226" ], [ "Burundi (Uburundi)", "bi", "257" ], [ "Cambodia (កម្ពុជា)", "kh", "855" ], [ "Cameroon (Cameroun)", "cm", "237" ], [ "Canada", "ca", "1", "+. (...) ...-....", 1, [ "204", "236", "249", "250", "289", "306", "343", "365", "387", "403", "416", "418", "431", "437", "438", "450", "506", "514", "519", "548", "579", "581", "587", "604", "613", "639", "647", "672", "705", "709", "742", "778", "780", "782", "807", "819", "825", "867", "873", "902", "905" ] ], [ "Cape Verde (Kabu Verdi)", "cv", "238" ], [ "Caribbean Netherlands", "bq", "599", "", 1 ], [ "Cayman Islands", "ky", "1345" ], [ "Central African Republic (République centrafricaine)", "cf", "236" ], [ "Chad (Tchad)", "td", "235" ], [ "Chile", "cl", "56" ], [ "China (中国)", "cn", "86", "+.. ..-........" ], [ "Colombia", "co", "57" ], [ "Comoros (‫جزر القمر‬‎)", "km", "269" ], [ "Congo (DRC) (Jamhuri ya Kidemokrasia ya Kongo)", "cd", "243" ], [ "Congo (Republic) (Congo-Brazzaville)", "cg", "242" ], [ "Cook Islands", "ck", "682" ], [ "Costa Rica", "cr", "506", "+... ....-...." ], [ "Côte d’Ivoire", "ci", "225" ], [ "Croatia (Hrvatska)", "hr", "385" ], [ "Cuba", "cu", "53" ], [ "Curaçao", "cw", "599", "", 0 ], [ "Cyprus (Κύπρος)", "cy", "357" ], [ "Czech Republic (Česká republika)", "cz", "420" ], [ "Denmark (Danmark)", "dk", "45", "+.. .. .. .. .." ], [ "Djibouti", "dj", "253" ], [ "Dominica", "dm", "1767" ], [ "Dominican Republic (República Dominicana)", "do", "1", "", 2, [ "809", "829", "849" ] ], [ "Ecuador", "ec", "593" ], [ "Egypt (‫مصر‬‎)", "eg", "20" ], [ "El Salvador", "sv", "503", "+... ....-...." ], [ "Equatorial Guinea (Guinea Ecuatorial)", "gq", "240" ], [ "Eritrea", "er", "291" ], [ "Estonia (Eesti)", "ee", "372" ], [ "Ethiopia", "et", "251" ], [ "Falkland Islands (Islas Malvinas)", "fk", "500" ], [ "Faroe Islands (Føroyar)", "fo", "298" ], [ "Fiji", "fj", "679" ], [ "Finland (Suomi)", "fi", "358", "+... .. ... .. .." ], [ "France", "fr", "33", "+.. . .. .. .. .." ], [ "French Guiana (Guyane française)", "gf", "594" ], [ "French Polynesia (Polynésie française)", "pf", "689" ], [ "Gabon", "ga", "241" ], [ "Gambia", "gm", "220" ], [ "Georgia (საქართველო)", "ge", "995" ], [ "Germany (Deutschland)", "de", "49", "+.. ... ......." ], [ "Ghana (Gaana)", "gh", "233" ], [ "Gibraltar", "gi", "350" ], [ "Greece (Ελλάδα)", "gr", "30" ], [ "Greenland (Kalaallit Nunaat)", "gl", "299" ], [ "Grenada", "gd", "1473" ], [ "Guadeloupe", "gp", "590", "", 0 ], [ "Guam", "gu", "1671" ], [ "Guatemala", "gt", "502", "+... ....-...." ], [ "Guinea (Guinée)", "gn", "224" ], [ "Guinea-Bissau (Guiné Bissau)", "gw", "245" ], [ "Guyana", "gy", "592" ], [ "Haiti", "ht", "509", "+... ....-...." ], [ "Honduras", "hn", "504" ], [ "Hong Kong (香港)", "hk", "852", "+... .... ...." ], [ "Hungary (Magyarország)", "hu", "36" ], [ "Iceland (Ísland)", "is", "354", "+... ... ...." ], [ "India (भारत)", "in", "91", "+.. .....-....." ], [ "Indonesia", "id", "62" ], [ "Iran (‫ایران‬‎)", "ir", "98" ], [ "Iraq (‫العراق‬‎)", "iq", "964" ], [ "Ireland", "ie", "353", "+... .. ......." ], [ "Israel (‫ישראל‬‎)", "il", "972" ], [ "Italy (Italia)", "it", "39", "+.. ... ......", 0 ], [ "Jamaica", "jm", "1876" ], [ "Japan (日本)", "jp", "81", "+.. ... .. ...." ], [ "Jordan (‫الأردن‬‎)", "jo", "962" ], [ "Kazakhstan (Казахстан)", "kz", "7", "+. ... ...-..-..", 1 ], [ "Kenya", "ke", "254" ], [ "Kiribati", "ki", "686" ], [ "Kuwait (‫الكويت‬‎)", "kw", "965" ], [ "Kyrgyzstan (Кыргызстан)", "kg", "996" ], [ "Laos (ລາວ)", "la", "856" ], [ "Latvia (Latvija)", "lv", "371" ], [ "Lebanon (‫لبنان‬‎)", "lb", "961" ], [ "Lesotho", "ls", "266" ], [ "Liberia", "lr", "231" ], [ "Libya (‫ليبيا‬‎)", "ly", "218" ], [ "Liechtenstein", "li", "423" ], [ "Lithuania (Lietuva)", "lt", "370" ], [ "Luxembourg", "lu", "352" ], [ "Macau (澳門)", "mo", "853" ], [ "Macedonia (FYROM) (Македонија)", "mk", "389" ], [ "Madagascar (Madagasikara)", "mg", "261" ], [ "Malawi", "mw", "265" ], [ "Malaysia", "my", "60", "+.. ..-....-...." ], [ "Maldives", "mv", "960" ], [ "Mali", "ml", "223" ], [ "Malta", "mt", "356" ], [ "Marshall Islands", "mh", "692" ], [ "Martinique", "mq", "596" ], [ "Mauritania (‫موريتانيا‬‎)", "mr", "222" ], [ "Mauritius (Moris)", "mu", "230" ], [ "Mexico (México)", "mx", "52" ], [ "Micronesia", "fm", "691" ], [ "Moldova (Republica Moldova)", "md", "373" ], [ "Monaco", "mc", "377" ], [ "Mongolia (Монгол)", "mn", "976" ], [ "Montenegro (Crna Gora)", "me", "382" ], [ "Montserrat", "ms", "1664" ], [ "Morocco (‫المغرب‬‎)", "ma", "212" ], [ "Mozambique (Moçambique)", "mz", "258" ], [ "Myanmar (Burma) (မြန်မာ)", "mm", "95" ], [ "Namibia (Namibië)", "na", "264" ], [ "Nauru", "nr", "674" ], [ "Nepal (नेपाल)", "np", "977" ], [ "Netherlands (Nederland)", "nl", "31", "+.. .. ........" ], [ "New Caledonia (Nouvelle-Calédonie)", "nc", "687" ], [ "New Zealand", "nz", "64", "+.. ...-...-...." ], [ "Nicaragua", "ni", "505" ], [ "Niger (Nijar)", "ne", "227" ], [ "Nigeria", "ng", "234" ], [ "Niue", "nu", "683" ], [ "Norfolk Island", "nf", "672" ], [ "North Korea (조선 민주주의 인민 공화국)", "kp", "850" ], [ "Northern Mariana Islands", "mp", "1670" ], [ "Norway (Norge)", "no", "47", "+.. ... .. ..." ], [ "Oman (‫عُمان‬‎)", "om", "968" ], [ "Pakistan (‫پاکستان‬‎)", "pk", "92", "+.. ...-......." ], [ "Palau", "pw", "680" ], [ "Palestine (‫فلسطين‬‎)", "ps", "970" ], [ "Panama (Panamá)", "pa", "507" ], [ "Papua New Guinea", "pg", "675" ], [ "Paraguay", "py", "595" ], [ "Peru (Perú)", "pe", "51" ], [ "Philippines", "ph", "63", "+.. ... ...." ], [ "Poland (Polska)", "pl", "48", "+.. ...-...-..." ], [ "Portugal", "pt", "351" ], [ "Puerto Rico", "pr", "1", "", 3, [ "787", "939" ] ], [ "Qatar (‫قطر‬‎)", "qa", "974" ], [ "Réunion (La Réunion)", "re", "262" ], [ "Romania (România)", "ro", "40" ], [ "Russia (Россия)", "ru", "7", "+. ... ...-..-..", 0 ], [ "Rwanda", "rw", "250" ], [ "Saint Barthélemy (Saint-Barthélemy)", "bl", "590", "", 1 ], [ "Saint Helena", "sh", "290" ], [ "Saint Kitts and Nevis", "kn", "1869" ], [ "Saint Lucia", "lc", "1758" ], [ "Saint Martin (Saint-Martin (partie française))", "mf", "590", "", 2 ], [ "Saint Pierre and Miquelon (Saint-Pierre-et-Miquelon)", "pm", "508" ], [ "Saint Vincent and the Grenadines", "vc", "1784" ], [ "Samoa", "ws", "685" ], [ "San Marino", "sm", "378" ], [ "São Tomé and Príncipe (São Tomé e Príncipe)", "st", "239" ], [ "Saudi Arabia (‫المملكة العربية السعودية‬‎)", "sa", "966" ], [ "Senegal (Sénégal)", "sn", "221" ], [ "Serbia (Србија)", "rs", "381" ], [ "Seychelles", "sc", "248" ], [ "Sierra Leone", "sl", "232" ], [ "Singapore", "sg", "65", "+.. ....-...." ], [ "Sint Maarten", "sx", "1721" ], [ "Slovakia (Slovensko)", "sk", "421" ], [ "Slovenia (Slovenija)", "si", "386" ], [ "Solomon Islands", "sb", "677" ], [ "Somalia (Soomaaliya)", "so", "252" ], [ "South Africa", "za", "27" ], [ "South Korea (대한민국)", "kr", "82" ], [ "South Sudan (‫جنوب السودان‬‎)", "ss", "211" ], [ "Spain (España)", "es", "34", "+.. ... ... ..." ], [ "Sri Lanka (ශ්‍රී ලංකාව)", "lk", "94" ], [ "Sudan (‫السودان‬‎)", "sd", "249" ], [ "Suriname", "sr", "597" ], [ "Swaziland", "sz", "268" ], [ "Sweden (Sverige)", "se", "46" ], [ "Switzerland (Schweiz)", "ch", "41", "+.. .. ... .. .." ], [ "Syria (‫سوريا‬‎)", "sy", "963" ], [ "Taiwan (台灣)", "tw", "886" ], [ "Tajikistan", "tj", "992" ], [ "Tanzania", "tz", "255" ], [ "Thailand (ไทย)", "th", "66" ], [ "Timor-Leste", "tl", "670" ], [ "Togo", "tg", "228" ], [ "Tokelau", "tk", "690" ], [ "Tonga", "to", "676" ], [ "Trinidad and Tobago", "tt", "1868" ], [ "Tunisia (‫تونس‬‎)", "tn", "216" ], [ "Turkey (Türkiye)", "tr", "90", "+.. ... ... .. .." ], [ "Turkmenistan", "tm", "993" ], [ "Turks and Caicos Islands", "tc", "1649" ], [ "Tuvalu", "tv", "688" ], [ "U.S. Virgin Islands", "vi", "1340" ], [ "Uganda", "ug", "256" ], [ "Ukraine (Україна)", "ua", "380" ], [ "United Arab Emirates (‫الإمارات العربية المتحدة‬‎)", "ae", "971" ], [ "United Kingdom", "gb", "44", "+.. .... ......" ], [ "United States", "us", "1", "+. (...) ...-....", 0 ], [ "Uruguay", "uy", "598" ], [ "Uzbekistan (Oʻzbekiston)", "uz", "998" ], [ "Vanuatu", "vu", "678" ], [ "Vatican City (Città del Vaticano)", "va", "39", "+.. .. .... ....", 1 ], [ "Venezuela", "ve", "58" ], [ "Vietnam (Việt Nam)", "vn", "84" ], [ "Wallis and Futuna", "wf", "681" ], [ "Yemen (‫اليمن‬‎)", "ye", "967" ], [ "Zambia", "zm", "260" ], [ "Zimbabwe", "zw", "263" ] ];
    // we will build this in the loop below
    var allCountryCodes = {};
    var addCountryCode = function(iso2, dialCode, priority) {
        if (!(dialCode in allCountryCodes)) {
            allCountryCodes[dialCode] = [];
        }
        var index = priority || 0;
        allCountryCodes[dialCode][index] = iso2;
    };
    // loop over all of the countries above
    for (var i = 0; i < allCountries.length; i++) {
        // countries
        var c = allCountries[i];
        allCountries[i] = {
            name: c[0],
            iso2: c[1],
            dialCode: c[2]
        };
        // format
        if (c[3]) {
            allCountries[i].format = c[3];
        }
        // area codes
        if (c[5]) {
            allCountries[i].hasAreaCodes = true;
            for (var j = 0; j < c[5].length; j++) {
                // full dial code is country code + dial code
                var dialCode = c[2] + c[5][j];
                addCountryCode(c[1], dialCode);
            }
        }
        // dial codes
        addCountryCode(c[1], c[2], c[4]);
    }
});
(function(){function f(a){throw a;}var i=!0,j=null,l=!1;function n(a,b){function c(){}c.prototype=b.prototype;a.ea=b.prototype;a.prototype=new c;a.prototype.constructor=a};function aa(a,b,c){this.z=a;this.ca=b.name||j;this.la=b.i||j;this.s=b.ka;this.h={};for(a=0;a<c.length;a++)b=c[a],this.h[b.n]=b}aa.prototype.p=function(){return!this.s?j:this.s.c()};function ba(a,b,c){this.da=a;this.n=b;this.ca=c.name;this.w=!!c.m;this.ma=!!c.required;this.k=c.a;this.r=c.type;this.u=l;switch(this.k){case ca:case la:case ma:case na:case oa:this.u=i}this.o=c.defaultValue}var ca=3,la=4,ma=6,na=16,oa=18;ba.prototype.p=function(){return this.da.c()};function p(){this.e={};this.h=this.c().h;this.d=this.q=j}p.prototype.c=function(){var a=this.constructor,b;if(!(b=a.t)){var c;b=a.aa;var d=[],e;for(e in b)b.hasOwnProperty(e)&&(0==e?c=b[0]:d.push(new ba(a,e,b[e])));c=new aa(a,c,d);b=a.t=c}return b};p.prototype.set=function(a,b){a.p();this.c();q(this,a.n,b)};p.prototype.clear=function(a){a.p();this.c();pa(this,a.n)};function s(a,b){return b in a.e&&void 0!==a.e[b]&&a.e[b]!==j}
function qa(a,b){var c=b.n;if(!c in a.e)return j;var d=a.e[c];if(d==j)return j;if(a.q){if(!(c in a.d)){var e=a.q;if(d!=j)if(b.w){for(var g=[],h=0;h<d.length;h++)g[h]=e.l(b,d[h]);d=g}else d=e.l(b,d);return a.d[c]=d}return a.d[c]}return d}function t(a,b,c){b=a.h[b];a=qa(a,b);return b.w?a[c||0]:a}function u(a,b){var c;if(s(a,b))c=t(a,b,void 0);else{c=a.h[b];if(void 0===c.o){var d=c.r;c.o=d===Boolean?l:d===Number?0:d===String?"":new d}c=c.o}return c}function q(a,b,c){a.e[b]=c;a.d&&(a.d[b]=c)}
function pa(a,b){delete a.e[b];a.d&&delete a.d[b]}function v(a,b){a.aa=b;a.c=function(){return a.t||(new a).c()}};function x(){}x.prototype.g=function(a){new a.z;f(Error("Unimplemented"))};x.prototype.l=function(a,b){if(11==a.k||10==a.k)return b instanceof p?b:this.g(a.r.c(),b);if(!a.u)return b;var c=a.r;if(c===String){if("number"===typeof b)return String(b)}else if(c===Number&&"string"===typeof b&&/^-?[0-9]+$/.test(b))return Number(b);return b};function z(){}n(z,x);z.prototype.g=function(a,b){var c=new a.z;c.q=this;c.e=b;c.d={};return c};function A(){}n(A,z);A.prototype.fa=l;A.prototype.l=function(a,b){return 8==a.k?1===b:x.prototype.l.apply(this,arguments)};A.prototype.g=function(a,b){var c=b;if(this.fa){var c=[],d;for(d in b)c[parseInt(d,10)+1]=b[d]}return A.ea.g.call(this,a,c)};function B(a,b){a!=j&&this.append.apply(this,arguments)}B.prototype.b="";B.prototype.set=function(a){this.b=""+a};B.prototype.append=function(a,b,c){this.b+=a;if(b!=j)for(var d=1;d<arguments.length;d++)this.b+=arguments[d];return this};B.prototype.clear=function(){this.b=""};B.prototype.toString=function(){return this.b};/*

 Protocol Buffer 2 Copyright 2008 Google Inc.
 All other code copyright its respective owners.
 Copyright (C) 2010 The Libphonenumber Authors

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function F(){p.apply(this)}n(F,p);function G(){p.apply(this)}n(G,p);function H(){p.apply(this)}n(H,p);H.prototype.f=function(){return u(this,10)};H.prototype.j=function(a){q(this,10,a)};function ra(){p.apply(this)}n(ra,p);
v(F,{"0":{name:"NumberFormat",i:"i18n.phonenumbers.NumberFormat"},1:{name:"pattern",required:i,a:9,type:String},2:{name:"format",required:i,a:9,type:String},3:{name:"leading_digits_pattern",m:i,a:9,type:String},4:{name:"national_prefix_formatting_rule",a:9,type:String},6:{name:"national_prefix_optional_when_formatting",a:8,type:Boolean},5:{name:"domestic_carrier_code_formatting_rule",a:9,type:String}});
v(G,{"0":{name:"PhoneNumberDesc",i:"i18n.phonenumbers.PhoneNumberDesc"},2:{name:"national_number_pattern",a:9,type:String},3:{name:"possible_number_pattern",a:9,type:String},6:{name:"example_number",a:9,type:String}});
v(H,{"0":{name:"PhoneMetadata",i:"i18n.phonenumbers.PhoneMetadata"},1:{name:"general_desc",required:i,a:11,type:G},2:{name:"fixed_line",required:i,a:11,type:G},3:{name:"mobile",required:i,a:11,type:G},4:{name:"toll_free",required:i,a:11,type:G},5:{name:"premium_rate",required:i,a:11,type:G},6:{name:"shared_cost",required:i,a:11,type:G},7:{name:"personal_number",required:i,a:11,type:G},8:{name:"voip",required:i,a:11,type:G},21:{name:"pager",required:i,a:11,type:G},25:{name:"uan",required:i,a:11,type:G},
27:{name:"emergency",required:i,a:11,type:G},28:{name:"voicemail",required:i,a:11,type:G},24:{name:"no_international_dialling",required:i,a:11,type:G},9:{name:"id",required:i,a:9,type:String},10:{name:"country_code",required:i,a:5,type:Number},11:{name:"international_prefix",required:i,a:9,type:String},17:{name:"preferred_international_prefix",a:9,type:String},12:{name:"national_prefix",a:9,type:String},13:{name:"preferred_extn_prefix",a:9,type:String},15:{name:"national_prefix_for_parsing",a:9,type:String},
16:{name:"national_prefix_transform_rule",a:9,type:String},18:{name:"same_mobile_and_fixed_line_pattern",a:8,defaultValue:l,type:Boolean},19:{name:"number_format",m:i,a:11,type:F},20:{name:"intl_number_format",m:i,a:11,type:F},22:{name:"main_country_for_code",a:8,defaultValue:l,type:Boolean},23:{name:"leading_digits",a:9,type:String},26:{name:"leading_zero_possible",a:8,defaultValue:l,type:Boolean}});
v(ra,{"0":{name:"PhoneMetadataCollection",i:"i18n.phonenumbers.PhoneMetadataCollection"},1:{name:"metadata",m:i,a:11,type:H}});/*

 Protocol Buffer 2 Copyright 2008 Google Inc.
 All other code copyright its respective owners.
 Copyright (C) 2010 The Libphonenumber Authors

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function I(){p.apply(this)}n(I,p);I.prototype.f=function(){return u(this,1)};I.prototype.j=function(a){q(this,1,a)};
v(I,{"0":{name:"PhoneNumber",i:"i18n.phonenumbers.PhoneNumber"},1:{name:"country_code",required:i,a:5,type:Number},2:{name:"national_number",required:i,a:4,type:Number},3:{name:"extension",a:9,type:String},4:{name:"italian_leading_zero",a:8,type:Boolean},8:{name:"number_of_leading_zeros",a:5,defaultValue:1,type:Number},5:{name:"raw_input",a:9,type:String},6:{name:"country_code_source",a:14,defaultValue:1,type:{ja:1,ia:5,ha:10,ga:20}},7:{name:"preferred_domestic_carrier_code",a:9,type:String}});/*

 Copyright (C) 2010 The Libphonenumber Authors

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
var sa={1:"US AG AI AS BB BM BS CA DM DO GD GU JM KN KY LC MP MS PR SX TC TT VC VG VI".split(" "),7:["RU","KZ"],20:["EG"],27:["ZA"],30:["GR"],31:["NL"],32:["BE"],33:["FR"],34:["ES"],36:["HU"],39:["IT"],40:["RO"],41:["CH"],43:["AT"],44:["GB","GG","IM","JE"],45:["DK"],46:["SE"],47:["NO","SJ"],48:["PL"],49:["DE"],51:["PE"],52:["MX"],53:["CU"],54:["AR"],55:["BR"],56:["CL"],57:["CO"],58:["VE"],60:["MY"],61:["AU","CC","CX"],62:["ID"],63:["PH"],64:["NZ"],65:["SG"],66:["TH"],81:["JP"],82:["KR"],84:["VN"],
86:["CN"],90:["TR"],91:["IN"],92:["PK"],93:["AF"],94:["LK"],95:["MM"],98:["IR"],211:["SS"],212:["MA","EH"],213:["DZ"],216:["TN"],218:["LY"],220:["GM"],221:["SN"],222:["MR"],223:["ML"],224:["GN"],225:["CI"],226:["BF"],227:["NE"],228:["TG"],229:["BJ"],230:["MU"],231:["LR"],232:["SL"],233:["GH"],234:["NG"],235:["TD"],236:["CF"],237:["CM"],238:["CV"],239:["ST"],240:["GQ"],241:["GA"],242:["CG"],243:["CD"],244:["AO"],245:["GW"],246:["IO"],247:["AC"],248:["SC"],249:["SD"],250:["RW"],251:["ET"],252:["SO"],
253:["DJ"],254:["KE"],255:["TZ"],256:["UG"],257:["BI"],258:["MZ"],260:["ZM"],261:["MG"],262:["RE","YT"],263:["ZW"],264:["NA"],265:["MW"],266:["LS"],267:["BW"],268:["SZ"],269:["KM"],290:["SH","TA"],291:["ER"],297:["AW"],298:["FO"],299:["GL"],350:["GI"],351:["PT"],352:["LU"],353:["IE"],354:["IS"],355:["AL"],356:["MT"],357:["CY"],358:["FI","AX"],359:["BG"],370:["LT"],371:["LV"],372:["EE"],373:["MD"],374:["AM"],375:["BY"],376:["AD"],377:["MC"],378:["SM"],379:["VA"],380:["UA"],381:["RS"],382:["ME"],385:["HR"],
386:["SI"],387:["BA"],389:["MK"],420:["CZ"],421:["SK"],423:["LI"],500:["FK"],501:["BZ"],502:["GT"],503:["SV"],504:["HN"],505:["NI"],506:["CR"],507:["PA"],508:["PM"],509:["HT"],590:["GP","BL","MF"],591:["BO"],592:["GY"],593:["EC"],594:["GF"],595:["PY"],596:["MQ"],597:["SR"],598:["UY"],599:["CW","BQ"],670:["TL"],672:["NF"],673:["BN"],674:["NR"],675:["PG"],676:["TO"],677:["SB"],678:["VU"],679:["FJ"],680:["PW"],681:["WF"],682:["CK"],683:["NU"],685:["WS"],686:["KI"],687:["NC"],688:["TV"],689:["PF"],690:["TK"],
691:["FM"],692:["MH"],800:["001"],808:["001"],850:["KP"],852:["HK"],853:["MO"],855:["KH"],856:["LA"],870:["001"],878:["001"],880:["BD"],881:["001"],882:["001"],883:["001"],886:["TW"],888:["001"],960:["MV"],961:["LB"],962:["JO"],963:["SY"],964:["IQ"],965:["KW"],966:["SA"],967:["YE"],968:["OM"],970:["PS"],971:["AE"],972:["IL"],973:["BH"],974:["QA"],975:["BT"],976:["MN"],977:["NP"],979:["001"],992:["TJ"],993:["TM"],994:["AZ"],995:["GE"],996:["KG"],998:["UZ"]},ta={AC:[,[,,"[2-467]\\d{3}","\\d{4}"],[,
,"(?:[267]\\d|3[0-5]|4[4-69])\\d{2}","\\d{4}",,,"6889"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"AC",247,"00",,,,,,,,,,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],AD:[,[,,"(?:[346-9]|180)\\d{5}","\\d{6,8}"],[,,"[78]\\d{5}","\\d{6}",,,"712345"],[,,"[346]\\d{5}","\\d{6}",,,"312345"],[,,"180[02]\\d{4}","\\d{8}",,,"18001234"],[,,"9\\d{5}","\\d{6}",,,"912345"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"AD",376,"00",,,,,,,,[[,"(\\d{3})(\\d{3})",
"$1 $2",["[346-9]"],"","",0],[,"(180[02])(\\d{4})","$1 $2",["1"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],AE:[,[,,"[2-79]\\d{7,8}|800\\d{2,9}","\\d{5,12}"],[,,"[2-4679][2-8]\\d{6}","\\d{7,8}",,,"22345678"],[,,"5[0256]\\d{7}","\\d{9}",,,"501234567"],[,,"400\\d{6}|800\\d{2,9}","\\d{5,12}",,,"800123456"],[,,"900[02]\\d{5}","\\d{9}",,,"900234567"],[,,"700[05]\\d{5}","\\d{9}",,,"700012345"],[,,"NA","NA"],[,,"NA","NA"],"AE",971,"00","0",,,"0",,,,[[,"([2-4679])(\\d{3})(\\d{4})",
"$1 $2 $3",["[2-4679][2-8]"],"0$1","",0],[,"(5[0256])(\\d{3})(\\d{4})","$1 $2 $3",["5"],"0$1","",0],[,"([479]00)(\\d)(\\d{5})","$1 $2 $3",["[479]0"],"$1","",0],[,"([68]00)(\\d{2,9})","$1 $2",["60|8"],"$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"600[25]\\d{5}","\\d{9}",,,"600212345"],,,[,,"NA","NA"]],AF:[,[,,"[2-7]\\d{8}","\\d{7,9}"],[,,"(?:[25][0-8]|[34][0-4]|6[0-5])[2-9]\\d{6}","\\d{7,9}",,,"234567890"],[,,"7[057-9]\\d{7}","\\d{9}",,,"701234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA",
"NA"],[,,"NA","NA"],"AF",93,"00","0",,,"0",,,,[[,"([2-7]\\d)(\\d{3})(\\d{4})","$1 $2 $3",,"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],AG:[,[,,"[2589]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"268(?:4(?:6[0-38]|84)|56[0-2])\\d{4}","\\d{7}(?:\\d{3})?",,,"2684601234"],[,,"268(?:464|7(?:2[0-9]|64|7[0-689]|8[02-68]))\\d{4}","\\d{10}",,,"2684641234"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002123456"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002123456"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}",
"\\d{10}",,,"5002345678"],[,,"26848[01]\\d{4}","\\d{10}",,,"2684801234"],"AG",1,"011","1",,,"1",,,,,,[,,"26840[69]\\d{4}","\\d{10}",,,"2684061234"],,"268",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],AI:[,[,,"[2589]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"2644(?:6[12]|9[78])\\d{4}","\\d{7}(?:\\d{3})?",,,"2644612345"],[,,"264(?:235|476|5(?:3[6-9]|8[1-4])|7(?:29|72))\\d{4}","\\d{10}",,,"2642351234"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002123456"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002123456"],
[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"AI",1,"011","1",,,"1",,,,,,[,,"NA","NA"],,"264",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],AL:[,[,,"[2-57]\\d{7}|6\\d{8}|8\\d{5,7}|9\\d{5}","\\d{5,9}"],[,,"(?:2(?:[168][1-9]|[247]\\d|9[1-7])|3(?:1[1-3]|[2-6]\\d|[79][1-8]|8[1-9])|4\\d{2}|5(?:1[1-4]|[2-578]\\d|6[1-5]|9[1-7])|8(?:[19][1-5]|[2-6]\\d|[78][1-7]))\\d{5}","\\d{5,8}",,,"22345678"],[,,"6[6-9]\\d{7}","\\d{9}",,,"661234567"],[,,"800\\d{4}","\\d{7}",,,"8001234"],
[,,"900\\d{3}","\\d{6}",,,"900123"],[,,"808\\d{3}","\\d{6}",,,"808123"],[,,"700\\d{5}","\\d{8}",,,"70012345"],[,,"NA","NA"],"AL",355,"00","0",,,"0",,,,[[,"(4)(\\d{3})(\\d{4})","$1 $2 $3",["4[0-6]"],"0$1","",0],[,"(6[6-9])(\\d{3})(\\d{4})","$1 $2 $3",["6"],"0$1","",0],[,"(\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["[2358][2-5]|4[7-9]"],"0$1","",0],[,"(\\d{3})(\\d{3,5})","$1 $2",["[235][16-9]|8[016-9]|[79]"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],AM:[,[,,"[1-9]\\d{7}",
"\\d{5,8}"],[,,"(?:1[01]\\d|2(?:2[2-46]|3[1-8]|4[2-69]|5[2-7]|6[1-9]|8[1-7])|3[12]2|47\\d)\\d{5}","\\d{5,8}",,,"10123456"],[,,"(?:55|77|9[1-9])\\d{6}","\\d{8}",,,"77123456"],[,,"800\\d{5}","\\d{8}",,,"80012345"],[,,"90[016]\\d{5}","\\d{8}",,,"90012345"],[,,"80[1-4]\\d{5}","\\d{8}",,,"80112345"],[,,"NA","NA"],[,,"60[2-6]\\d{5}","\\d{8}",,,"60271234"],"AM",374,"00","0",,,"0",,,,[[,"(\\d{2})(\\d{6})","$1 $2",["1|47"],"(0$1)","",0],[,"(\\d{2})(\\d{6})","$1 $2",["[5-7]|9[1-9]"],"0$1","",0],[,"(\\d{3})(\\d{5})",
"$1 $2",["[23]"],"(0$1)","",0],[,"(\\d{3})(\\d{2})(\\d{3})","$1 $2 $3",["8|90"],"0 $1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],AO:[,[,,"[29]\\d{8}","\\d{9}"],[,,"2\\d(?:[26-9]\\d|\\d[26-9])\\d{5}","\\d{9}",,,"222123456"],[,,"9[1-49]\\d{7}","\\d{9}",,,"923123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"AO",244,"00",,,,,,,,[[,"(\\d{3})(\\d{3})(\\d{3})","$1 $2 $3",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],AR:[,
[,,"[1-368]\\d{9}|9\\d{10}","\\d{6,11}"],[,,"11\\d{8}|(?:2(?:2(?:[013]\\d|2[13-79]|4[1-6]|5[2457]|6[124-8]|7[1-4]|8[13-6]|9[1267])|3(?:1[467]|2[03-6]|3[13-8]|[49][2-6]|5[2-8]|[067]\\d)|4(?:7[3-8]|9\\d)|6(?:[01346]\\d|2[24-6]|5[15-8])|80\\d|9(?:[0124789]\\d|3[1-6]|5[234]|6[2-46]))|3(?:3(?:2[79]|6\\d|8[2578])|4(?:[78]\\d|0[0124-9]|[1-35]\\d|4[24-7]|6[02-9]|9[123678])|5(?:[138]\\d|2[1245]|4[1-9]|6[2-4]|7[1-6])|6[24]\\d|7(?:[0469]\\d|1[1568]|2[013-9]|3[145]|5[14-8]|7[2-57]|8[0-24-9])|8(?:[013578]\\d|2[15-7]|4[13-6]|6[1-357-9]|9[124]))|670\\d)\\d{6}",
"\\d{6,10}",,,"1123456789"],[,,"675\\d{7}|9(?:11[2-9]\\d{7}|(?:2(?:2[013]|3[067]|49|6[01346]|80|9[147-9])|3(?:36|4[12358]|5[138]|6[24]|7[069]|8[013578]))[2-9]\\d{6}|\\d{4}[2-9]\\d{5})","\\d{6,11}",,,"91123456789"],[,,"800\\d{7}","\\d{10}",,,"8001234567"],[,,"60[04579]\\d{7}","\\d{10}",,,"6001234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"AR",54,"00","0",,,"0?(?:(11|2(?:2(?:02?|[13]|2[13-79]|4[1-6]|5[2457]|6[124-8]|7[1-4]|8[13-6]|9[1267])|3(?:02?|1[467]|2[03-6]|3[13-8]|[49][2-6]|5[2-8]|[67])|4(?:7[3-578]|9)|6(?:[0136]|2[24-6]|4[6-8]?|5[15-8])|80|9(?:0[1-3]|[19]|2\\d|3[1-6]|4[02568]?|5[2-4]|6[2-46]|72?|8[23]?))|3(?:3(?:2[79]|6|8[2578])|4(?:0[124-9]|[12]|3[5-8]?|4[24-7]|5[4-68]?|6[02-9]|7[126]|8[2379]?|9[1-36-8])|5(?:1|2[1245]|3[237]?|4[1-46-9]|6[2-4]|7[1-6]|8[2-5]?)|6[24]|7(?:1[1568]|2[15]|3[145]|4[13]|5[14-8]|[069]|7[2-57]|8[126])|8(?:[01]|2[15-7]|3[2578]?|4[13-6]|5[4-8]?|6[1-357-9]|7[36-8]?|8[5-8]?|9[124])))15)?",
"9$1",,,[[,"([68]\\d{2})(\\d{3})(\\d{4})","$1-$2-$3",["[68]"],"0$1","",0],[,"(9)(11)(\\d{4})(\\d{4})","$2 15-$3-$4",["911"],"0$1","",0],[,"(9)(\\d{3})(\\d{3})(\\d{4})","$2 15-$3-$4",["9(?:2[234689]|3[3-8])","9(?:2(?:2[013]|3[067]|49|6[01346]|80|9[147-9])|3(?:36|4[12358]|5[138]|6[24]|7[069]|8[013578]))","9(?:2(?:2[013]|3[067]|49|6[01346]|80|9(?:[17-9]|4[13479]))|3(?:36|4[12358]|5(?:[18]|3[014-689])|6[24]|7[069]|8(?:[01]|3[013469]|5[0-39]|7[0-2459]|8[0-49])))"],"0$1","",0],[,"(9)(\\d{4})(\\d{3})(\\d{3})",
"$2 15-$3-$4",["93[58]","9(?:3(?:53|8[78]))","9(?:3(?:537|8(?:73|88)))"],"0$1","",0],[,"(9)(\\d{4})(\\d{2})(\\d{4})","$2 15-$3-$4",["9[23]"],"0$1","",0],[,"(11)(\\d{4})(\\d{4})","$1 $2-$3",["1"],"0$1","",1],[,"(\\d{3})(\\d{3})(\\d{4})","$1 $2-$3",["2(?:2[013]|3[067]|49|6[01346]|80|9[147-9])|3(?:36|4[12358]|5[138]|6[24]|7[069]|8[013578])","2(?:2[013]|3[067]|49|6[01346]|80|9(?:[17-9]|4[13479]))|3(?:36|4[12358]|5(?:[18]|3[0-689])|6[24]|7[069]|8(?:[01]|3[013469]|5[0-39]|7[0-2459]|8[0-49]))"],"0$1","",
1],[,"(\\d{4})(\\d{3})(\\d{3})","$1 $2-$3",["3(?:53|8[78])","3(?:537|8(?:73|88))"],"0$1","",1],[,"(\\d{4})(\\d{2})(\\d{4})","$1 $2-$3",["[23]"],"0$1","",1],[,"(\\d{3})","$1",["1[012]|911"],"$1","",0]],[[,"([68]\\d{2})(\\d{3})(\\d{4})","$1-$2-$3",["[68]","[68]"],"0$1","",0],[,"(9)(11)(\\d{4})(\\d{4})","$1 $2 $3-$4",["911"]],[,"(9)(\\d{3})(\\d{3})(\\d{4})","$1 $2 $3-$4",["9(?:2[234689]|3[3-8])","9(?:2(?:2[013]|3[067]|49|6[01346]|80|9[147-9])|3(?:36|4[12358]|5[138]|6[24]|7[069]|8[013578]))","9(?:2(?:2[013]|3[067]|49|6[01346]|80|9(?:[17-9]|4[13479]))|3(?:36|4[12358]|5(?:[18]|3[014-689])|6[24]|7[069]|8(?:[01]|3[013469]|5[0-39]|7[0-2459]|8[0-49])))"]],
[,"(9)(\\d{4})(\\d{3})(\\d{3})","$1 $2 $3-$4",["93[58]","9(?:3(?:53|8[78]))","9(?:3(?:537|8(?:73|88)))"]],[,"(9)(\\d{4})(\\d{2})(\\d{4})","$1 $2 $3-$4",["9[23]"]],[,"(11)(\\d{4})(\\d{4})","$1 $2-$3",["1","1"],"0$1","",1],[,"(\\d{3})(\\d{3})(\\d{4})","$1 $2-$3",["2(?:2[013]|3[067]|49|6[01346]|80|9[147-9])|3(?:36|4[12358]|5[138]|6[24]|7[069]|8[013578])","2(?:2[013]|3[067]|49|6[01346]|80|9(?:[17-9]|4[13479]))|3(?:36|4[12358]|5(?:[18]|3[0-689])|6[24]|7[069]|8(?:[01]|3[013469]|5[0-39]|7[0-2459]|8[0-49]))",
"2(?:2[013]|3[067]|49|6[01346]|80|9[147-9])|3(?:36|4[12358]|5[138]|6[24]|7[069]|8[013578])","2(?:2[013]|3[067]|49|6[01346]|80|9(?:[17-9]|4[13479]))|3(?:36|4[12358]|5(?:[18]|3[0-689])|6[24]|7[069]|8(?:[01]|3[013469]|5[0-39]|7[0-2459]|8[0-49]))"],"0$1","",1],[,"(\\d{4})(\\d{3})(\\d{3})","$1 $2-$3",["3(?:53|8[78])","3(?:537|8(?:73|88))","3(?:53|8[78])","3(?:537|8(?:73|88))"],"0$1","",1],[,"(\\d{4})(\\d{2})(\\d{4})","$1 $2-$3",["[23]","[23]"],"0$1","",1]],[,,"NA","NA"],,,[,,"810\\d{7}","\\d{10}",,,"8101234567"],
[,,"810\\d{7}","\\d{10}",,,"8101234567"],,,[,,"NA","NA"]],AS:[,[,,"[5689]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"6846(?:22|33|44|55|77|88|9[19])\\d{4}","\\d{7}(?:\\d{3})?",,,"6846221234"],[,,"684(?:733|25[2468])\\d{4}","\\d{10}",,,"6847331234"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002123456"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002123456"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"AS",1,"011","1",,,"1",,,,,,[,,"NA","NA"],,"684",[,,"NA","NA"],[,,"NA",
"NA"],,,[,,"NA","NA"]],AT:[,[,,"[1-9]\\d{3,12}","\\d{3,13}"],[,,"1\\d{3,12}|(?:2(?:1[467]|2[13-8]|5[2357]|6[1-46-8]|7[1-8]|8[124-7]|9[1458])|3(?:1[1-8]|3[23568]|4[5-7]|5[1378]|6[1-38]|8[3-68])|4(?:2[1-8]|35|63|7[1368]|8[2457])|5(?:12|2[1-8]|3[357]|4[147]|5[12578]|6[37])|6(?:13|2[1-47]|4[1-35-8]|5[468]|62)|7(?:2[1-8]|3[25]|4[13478]|5[68]|6[16-8]|7[1-6]|9[45]))\\d{3,10}","\\d{3,13}",,,"1234567890"],[,,"6(?:44|5[0-3579]|6[013-9]|[7-9]\\d)\\d{4,10}","\\d{7,13}",,,"644123456"],[,,"80[02]\\d{6,10}","\\d{9,13}",
,,"800123456"],[,,"(?:711|9(?:0[01]|3[019]))\\d{6,10}","\\d{9,13}",,,"900123456"],[,,"8(?:10|2[018])\\d{6,10}","\\d{9,13}",,,"810123456"],[,,"NA","NA"],[,,"780\\d{6,10}","\\d{9,13}",,,"780123456"],"AT",43,"00","0",,,"0",,,,[[,"(1)(\\d{3,12})","$1 $2",["1"],"0$1","",0],[,"(5\\d)(\\d{3,5})","$1 $2",["5[079]"],"0$1","",0],[,"(5\\d)(\\d{3})(\\d{3,4})","$1 $2 $3",["5[079]"],"0$1","",0],[,"(5\\d)(\\d{4})(\\d{4,7})","$1 $2 $3",["5[079]"],"0$1","",0],[,"(\\d{3})(\\d{3,10})","$1 $2",["316|46|51|732|6(?:44|5[0-3579]|[6-9])|7(?:1|[28]0)|[89]"],
"0$1","",0],[,"(\\d{4})(\\d{3,9})","$1 $2",["2|3(?:1[1-578]|[3-8])|4[2378]|5[2-6]|6(?:[12]|4[1-35-9]|5[468])|7(?:2[1-8]|35|4[1-8]|[5-79])"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"5(?:(?:0[1-9]|17)\\d{2,10}|[79]\\d{3,11})|720\\d{6,10}","\\d{5,13}",,,"50123"],,,[,,"NA","NA"]],AU:[,[,,"[1-578]\\d{5,9}","\\d{6,10}"],[,,"[237]\\d{8}|8(?:[68]\\d{3}|7[0-69]\\d{2}|9(?:[02-9]\\d{2}|1(?:[0-57-9]\\d|6[0135-9])))\\d{4}","\\d{8,9}",,,"212345678"],[,,"14(?:5\\d|71)\\d{5}|4(?:[0-2]\\d|3[0-57-9]|4[47-9]|5[0-25-9]|6[6-9]|7[0457-9]|8[17-9]|9[07-9])\\d{6}",
"\\d{9}",,,"412345678"],[,,"180(?:0\\d{3}|2)\\d{3}","\\d{7,10}",,,"1800123456"],[,,"190[0126]\\d{6}","\\d{10}",,,"1900123456"],[,,"13(?:00\\d{2})?\\d{4}","\\d{6,10}",,,"1300123456"],[,,"500\\d{6}","\\d{9}",,,"500123456"],[,,"550\\d{6}","\\d{9}",,,"550123456"],"AU",61,"(?:14(?:1[14]|34|4[17]|[56]6|7[47]|88))?001[14-689]","0",,,"0",,"0011",,[[,"([2378])(\\d{4})(\\d{4})","$1 $2 $3",["[2378]"],"(0$1)","",0],[,"(\\d{3})(\\d{3})(\\d{3})","$1 $2 $3",["[45]|14"],"0$1","",0],[,"(16)(\\d{3})(\\d{2,4})","$1 $2 $3",
["16"],"0$1","",0],[,"(1[389]\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["1(?:[38]0|90)","1(?:[38]00|90)"],"$1","",0],[,"(180)(2\\d{3})","$1 $2",["180","1802"],"$1","",0],[,"(19\\d)(\\d{3})","$1 $2",["19[13]"],"$1","",0],[,"(19\\d{2})(\\d{4})","$1 $2",["19[67]"],"$1","",0],[,"(13)(\\d{2})(\\d{2})","$1 $2 $3",["13[1-9]"],"$1","",0]],,[,,"16\\d{3,7}","\\d{5,9}",,,"1612345"],1,,[,,"1(?:3(?:\\d{4}|00\\d{6})|80(?:0\\d{6}|2\\d{3}))","\\d{6,10}",,,"1300123456"],[,,"NA","NA"],,,[,,"NA","NA"]],AW:[,[,,"[25-9]\\d{6}",
"\\d{7}"],[,,"5(?:2\\d|8[1-9])\\d{4}","\\d{7}",,,"5212345"],[,,"(?:5(?:6\\d|9[2-478])|6(?:[039]0|22|4[01]|6[0-2])|7[34]\\d|9(?:6[45]|9[4-8]))\\d{4}","\\d{7}",,,"5601234"],[,,"800\\d{4}","\\d{7}",,,"8001234"],[,,"900\\d{4}","\\d{7}",,,"9001234"],[,,"NA","NA"],[,,"NA","NA"],[,,"28\\d{5}|501\\d{4}","\\d{7}",,,"5011234"],"AW",297,"00",,,,,,,,[[,"(\\d{3})(\\d{4})","$1 $2",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],AX:[,[,,"[135]\\d{5,9}|[27]\\d{4,9}|4\\d{5,10}|6\\d{7,8}|8\\d{6,9}",
"\\d{5,12}"],[,,"18[1-8]\\d{3,9}","\\d{6,12}",,,"1812345678"],[,,"4\\d{5,10}|50\\d{4,8}","\\d{6,11}",,,"412345678"],[,,"800\\d{4,7}","\\d{7,10}",,,"8001234567"],[,,"[67]00\\d{5,6}","\\d{8,9}",,,"600123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"AX",358,"00|99[049]","0",,,"0",,,,,,[,,"NA","NA"],,,[,,"[13]00\\d{3,7}|2(?:0(?:0\\d{3,7}|2[023]\\d{1,6}|9[89]\\d{1,6}))|60(?:[12]\\d{5,6}|6\\d{7})|7(?:1\\d{7}|3\\d{8}|5[03-9]\\d{2,7})","\\d{5,10}",,,"100123"],[,,"[13]0\\d{4,8}|2(?:0(?:[016-8]\\d{3,7}|[2-59]\\d{2,7})|9\\d{4,8})|60(?:[12]\\d{5,6}|6\\d{7})|7(?:1\\d{7}|3\\d{8}|5[03-9]\\d{2,7})",
"\\d{5,10}",,,"10112345"],,,[,,"NA","NA"]],AZ:[,[,,"[1-9]\\d{8}","\\d{7,9}"],[,,"(?:1[28]\\d|2(?:02|1[24]|2[2-4]|33|[45]2|6[23])|365)\\d{6}","\\d{7,9}",,,"123123456"],[,,"(?:4[04]|5[015]|60|7[07])\\d{7}","\\d{9}",,,"401234567"],[,,"88\\d{7}","\\d{9}",,,"881234567"],[,,"900200\\d{3}","\\d{9}",,,"900200123"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"AZ",994,"00","0",,,"0",,,,[[,"(\\d{2})(\\d{3})(\\d{2})(\\d{2})","$1 $2 $3 $4",["(?:1[28]|2(?:[45]2|[0-36])|365)"],"(0$1)","",0],[,"(\\d{2})(\\d{3})(\\d{2})(\\d{2})",
"$1 $2 $3 $4",["[4-8]"],"0$1","",0],[,"(\\d{3})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["9"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],BA:[,[,,"[3-9]\\d{7,8}","\\d{6,9}"],[,,"(?:[35]\\d|49)\\d{6}","\\d{6,8}",,,"30123456"],[,,"6(?:03|44|71|[1-356])\\d{6}","\\d{8,9}",,,"61123456"],[,,"8[08]\\d{6}","\\d{8}",,,"80123456"],[,,"9[0246]\\d{6}","\\d{8}",,,"90123456"],[,,"8[12]\\d{6}","\\d{8}",,,"82123456"],[,,"NA","NA"],[,,"NA","NA"],"BA",387,"00","0",,,"0",,,,[[,"(\\d{2})(\\d{3})(\\d{3})",
"$1 $2-$3",["[3-5]"],"0$1","",0],[,"(\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["6[1-356]|[7-9]"],"0$1","",0],[,"(\\d{2})(\\d{2})(\\d{2})(\\d{3})","$1 $2 $3 $4",["6[047]"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"70[23]\\d{5}","\\d{8}",,,"70223456"],,,[,,"NA","NA"]],BB:[,[,,"[2589]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"246[2-9]\\d{6}","\\d{7}(?:\\d{3})?",,,"2462345678"],[,,"246(?:(?:2[346]|45|82)\\d|25[0-4])\\d{4}","\\d{10}",,,"2462501234"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002123456"],
[,,"900[2-9]\\d{6}","\\d{10}",,,"9002123456"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"BB",1,"011","1",,,"1",,,,,,[,,"NA","NA"],,"246",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],BD:[,[,,"[2-79]\\d{5,9}|1\\d{9}|8[0-7]\\d{4,8}","\\d{6,10}"],[,,"2(?:7(?:1[0-267]|2[0-289]|3[0-29]|[46][01]|5[1-3]|7[017]|91)|8(?:0[125]|[139][1-6]|2[0157-9]|6[1-35]|7[1-5]|8[1-8])|9(?:0[0-2]|1[1-4]|2[568]|3[3-6]|5[5-7]|6[0167]|7[15]|8[016-8]))\\d{4}|3(?:12?[5-7]\\d{2}|0(?:2(?:[025-79]\\d|[348]\\d{1,2})|3(?:[2-4]\\d|[56]\\d?))|2(?:1\\d{2}|2(?:[12]\\d|[35]\\d{1,2}|4\\d?))|3(?:1\\d{2}|2(?:[2356]\\d|4\\d{1,2}))|4(?:1\\d{2}|2(?:2\\d{1,2}|[47]|5\\d{2}))|5(?:1\\d{2}|29)|[67]1\\d{2}|8(?:1\\d{2}|2(?:2\\d{2}|3|4\\d)))\\d{3}|4(?:0(?:2(?:[09]\\d|7)|33\\d{2})|1\\d{3}|2(?:1\\d{2}|2(?:[25]\\d?|[348]\\d|[67]\\d{1,2}))|3(?:1\\d{2}(?:\\d{2})?|2(?:[045]\\d|[236-9]\\d{1,2})|32\\d{2})|4(?:[18]\\d{2}|2(?:[2-46]\\d{2}|3)|5[25]\\d{2})|5(?:1\\d{2}|2(?:3\\d|5))|6(?:[18]\\d{2}|2(?:3(?:\\d{2})?|[46]\\d{1,2}|5\\d{2}|7\\d)|5(?:3\\d?|4\\d|[57]\\d{1,2}|6\\d{2}|8))|71\\d{2}|8(?:[18]\\d{2}|23\\d{2}|54\\d{2})|9(?:[18]\\d{2}|2[2-5]\\d{2}|53\\d{1,2}))\\d{3}|5(?:02[03489]\\d{2}|1\\d{2}|2(?:1\\d{2}|2(?:2(?:\\d{2})?|[457]\\d{2}))|3(?:1\\d{2}|2(?:[37](?:\\d{2})?|[569]\\d{2}))|4(?:1\\d{2}|2[46]\\d{2})|5(?:1\\d{2}|26\\d{1,2})|6(?:[18]\\d{2}|2|53\\d{2})|7(?:1|24)\\d{2}|8(?:1|26)\\d{2}|91\\d{2})\\d{3}|6(?:0(?:1\\d{2}|2(?:3\\d{2}|4\\d{1,2}))|2(?:2[2-5]\\d{2}|5(?:[3-5]\\d{2}|7)|8\\d{2})|3(?:1|2[3478])\\d{2}|4(?:1|2[34])\\d{2}|5(?:1|2[47])\\d{2}|6(?:[18]\\d{2}|6(?:2(?:2\\d|[34]\\d{2})|5(?:[24]\\d{2}|3\\d|5\\d{1,2})))|72[2-5]\\d{2}|8(?:1\\d{2}|2[2-5]\\d{2})|9(?:1\\d{2}|2[2-6]\\d{2}))\\d{3}|7(?:(?:02|[3-589]1|6[12]|72[24])\\d{2}|21\\d{3}|32)\\d{3}|8(?:(?:4[12]|[5-7]2|1\\d?)|(?:0|3[12]|[5-7]1|217)\\d)\\d{4}|9(?:[35]1|(?:[024]2|81)\\d|(?:1|[24]1)\\d{2})\\d{3}",
"\\d{6,9}",,,"27111234"],[,,"(?:1[13-9]\\d|(?:3[78]|44)[02-9]|6(?:44|6[02-9]))\\d{7}","\\d{10}",,,"1812345678"],[,,"80[03]\\d{7}","\\d{10}",,,"8001234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"96(?:0[49]|1[0-4]|6[69])\\d{6}","\\d{10}",,,"9604123456"],"BD",880,"00[12]?","0",,,"0",,"00",,[[,"(2)(\\d{7})","$1-$2",["2"],"0$1","",0],[,"(\\d{2})(\\d{4,6})","$1-$2",["[3-79]1"],"0$1","",0],[,"(\\d{4})(\\d{3,6})","$1-$2",["1|3(?:0|[2-58]2)|4(?:0|[25]2|3[23]|[4689][25])|5(?:[02-578]2|6[25])|6(?:[0347-9]2|[26][25])|7[02-9]2|8(?:[023][23]|[4-7]2)|9(?:[02][23]|[458]2|6[016])"],
"0$1","",0],[,"(\\d{3})(\\d{3,7})","$1-$2",["[3-79][2-9]|8"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],BE:[,[,,"[1-9]\\d{7,8}","\\d{8,9}"],[,,"(?:1[0-69]|[49][23]|5\\d|6[013-57-9]|71|8[0-79])[1-9]\\d{5}|[23][2-8]\\d{6}","\\d{8}",,,"12345678"],[,,"4(?:[679]\\d|8[03-9])\\d{6}","\\d{9}",,,"470123456"],[,,"800\\d{5}","\\d{8}",,,"80012345"],[,,"(?:70[2-7]|90\\d)\\d{5}","\\d{8}",,,"90123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"BE",32,"00","0",,,"0",,,,[[,"(4[6-9]\\d)(\\d{2})(\\d{2})(\\d{2})",
"$1 $2 $3 $4",["4[6-9]"],"0$1","",0],[,"([2-49])(\\d{3})(\\d{2})(\\d{2})","$1 $2 $3 $4",["[23]|[49][23]"],"0$1","",0],[,"([15-8]\\d)(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["[156]|7[018]|8(?:0[1-9]|[1-79])"],"0$1","",0],[,"([89]\\d{2})(\\d{2})(\\d{3})","$1 $2 $3",["(?:80|9)0"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"78\\d{6}","\\d{8}",,,"78123456"],,,[,,"NA","NA"]],BF:[,[,,"[24-7]\\d{7}","\\d{8}"],[,,"(?:20(?:49|5[23]|9[016-9])|40(?:4[569]|5[4-6]|7[0179])|50(?:[34]\\d|50))\\d{4}","\\d{8}",
,,"20491234"],[,,"6(?:[0-24-689]\\d|3[0-7]|7[0-2])\\d{5}|7\\d{7}","\\d{8}",,,"70123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"BF",226,"00",,,,,,,,[[,"(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],BG:[,[,,"[23567]\\d{5,7}|[489]\\d{6,8}","\\d{5,9}"],[,,"2(?:[0-8]\\d{5,6}|9\\d{4,6})|(?:[36]\\d|5[1-9]|8[1-6]|9[1-7])\\d{5,6}|(?:4(?:[124-7]\\d|3[1-6])|7(?:0[1-9]|[1-9]\\d))\\d{4,5}","\\d{5,8}",,,
"2123456"],[,,"(?:8[7-9]|98)\\d{7}|4(?:3[0789]|8\\d)\\d{5}","\\d{8,9}",,,"48123456"],[,,"800\\d{5}","\\d{8}",,,"80012345"],[,,"90\\d{6}","\\d{8}",,,"90123456"],[,,"NA","NA"],[,,"700\\d{5}","\\d{5,9}",,,"70012345"],[,,"NA","NA"],"BG",359,"00","0",,,"0",,,,[[,"(2)(\\d{5})","$1 $2",["29"],"0$1","",0],[,"(2)(\\d{3})(\\d{3,4})","$1 $2 $3",["2"],"0$1","",0],[,"(\\d{3})(\\d{4})","$1 $2",["43[124-7]|70[1-9]"],"0$1","",0],[,"(\\d{3})(\\d{3})(\\d{2})","$1 $2 $3",["43[124-7]|70[1-9]"],"0$1","",0],[,"(\\d{3})(\\d{2})(\\d{3})",
"$1 $2 $3",["[78]00"],"0$1","",0],[,"(\\d{2})(\\d{3})(\\d{2,3})","$1 $2 $3",["[356]|4[124-7]|7[1-9]|8[1-6]|9[1-7]"],"0$1","",0],[,"(\\d{2})(\\d{3})(\\d{3,4})","$1 $2 $3",["48|8[7-9]|9[08]"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],BH:[,[,,"[136-9]\\d{7}","\\d{8}"],[,,"(?:1(?:3[13-6]|6[0156]|7\\d)\\d|6(?:1[16]\\d|500|6(?:0\\d|3[12]|44|88)|9[69][69])|7(?:7\\d{2}|178))\\d{4}","\\d{8}",,,"17001234"],[,,"(?:3(?:[1-4679]\\d|5[0135]|8[0-48])\\d|6(?:3(?:00|33|6[16])|6(?:[69]\\d|3[03-9])))\\d{4}",
"\\d{8}",,,"36001234"],[,,"80\\d{6}","\\d{8}",,,"80123456"],[,,"(?:87|9[014578])\\d{6}","\\d{8}",,,"90123456"],[,,"84\\d{6}","\\d{8}",,,"84123456"],[,,"NA","NA"],[,,"NA","NA"],"BH",973,"00",,,,,,,,[[,"(\\d{4})(\\d{4})","$1 $2",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],BI:[,[,,"[27]\\d{7}","\\d{8}"],[,,"22(?:2[0-7]|[3-5]0)\\d{4}","\\d{8}",,,"22201234"],[,,"(?:29|7[14-9])\\d{6}","\\d{8}",,,"79561234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],
"BI",257,"00",,,,,,,,[[,"([27]\\d)(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],BJ:[,[,,"[2689]\\d{7}|7\\d{3}","\\d{4,8}"],[,,"2(?:02|1[037]|2[45]|3[68])\\d{5}","\\d{8}",,,"20211234"],[,,"(?:6[146-8]|9[03-9])\\d{6}","\\d{8}",,,"90011234"],[,,"7[3-5]\\d{2}","\\d{4}",,,"7312"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"857[58]\\d{4}","\\d{8}",,,"85751234"],"BJ",229,"00",,,,,,,,[[,"(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",,
"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"81\\d{6}","\\d{8}",,,"81123456"],,,[,,"NA","NA"]],BL:[,[,,"[56]\\d{8}","\\d{9}"],[,,"590(?:2[7-9]|5[12]|87)\\d{4}","\\d{9}",,,"590271234"],[,,"690(?:0[0-7]|[1-9]\\d)\\d{4}","\\d{9}",,,"690301234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"BL",590,"00","0",,,"0",,,,,,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],BM:[,[,,"[4589]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"441(?:2(?:02|23|61|[3479]\\d)|[46]\\d{2}|5(?:4\\d|60|89)|824)\\d{4}",
"\\d{7}(?:\\d{3})?",,,"4412345678"],[,,"441(?:[37]\\d|5[0-39])\\d{5}","\\d{10}",,,"4413701234"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002123456"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002123456"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"BM",1,"011","1",,,"1",,,,,,[,,"NA","NA"],,"441",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],BN:[,[,,"[2-578]\\d{6}","\\d{7}"],[,,"[2-5]\\d{6}","\\d{7}",,,"2345678"],[,,"[78]\\d{6}","\\d{7}",,,"7123456"],[,,"NA",
"NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"BN",673,"00",,,,,,,,[[,"([2-578]\\d{2})(\\d{4})","$1 $2",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],BO:[,[,,"[23467]\\d{7}","\\d{7,8}"],[,,"(?:2(?:2\\d{2}|5(?:11|[258]\\d|9[67])|6(?:12|2\\d|9[34])|8(?:2[34]|39|62))|3(?:3\\d{2}|4(?:6\\d|8[24])|8(?:25|42|5[257]|86|9[25])|9(?:2\\d|3[234]|4[248]|5[24]|6[2-6]|7\\d))|4(?:4\\d{2}|6(?:11|[24689]\\d|72)))\\d{4}","\\d{7,8}",,,"22123456"],[,,"[67]\\d{7}","\\d{8}",,,
"71234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"BO",591,"00(1\\d)?","0",,,"0(1\\d)?",,,,[[,"([234])(\\d{7})","$1 $2",["[234]"],"","0$CC $1",0],[,"([67]\\d{7})","$1",["[67]"],"","0$CC $1",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],BQ:[,[,,"[347]\\d{6}","\\d{7}"],[,,"(?:318[023]|416[023]|7(?:1[578]|50)\\d)\\d{3}","\\d{7}",,,"7151234"],[,,"(?:318[14-68]|416[15-9]|7(?:0[01]|7[07]|[89]\\d)\\d)\\d{3}","\\d{7}",,,"3181234"],[,,"NA","NA"],[,,"NA",
"NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"BQ",599,"00",,,,,,,,,,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],BR:[,[,,"[1-46-9]\\d{7,10}|5\\d{8,9}","\\d{8,11}"],[,,"1[1-9][2-5]\\d{7}|(?:[4689][1-9]|2[12478]|3[1-578]|5[13-5]|7[13-579])[2-5]\\d{7}","\\d{8,11}",,,"1123456789"],[,,"1[1-9]9\\d{8}|2[12478]9?[6-9]\\d{7}|(?:3[1-578]|[4689][1-9]|5[13-5]|7[13-579])[6-9]\\d{7}","\\d{10,11}",,,"11961234567"],[,,"800\\d{6,7}","\\d{8,11}",,,"800123456"],[,,"[359]00\\d{6,7}","\\d{8,11}",,,
"300123456"],[,,"[34]00\\d{5}","\\d{8}",,,"40041234"],[,,"NA","NA"],[,,"NA","NA"],"BR",55,"00(?:1[45]|2[135]|[34]1|43)","0",,,"0(?:(1[245]|2[135]|[34]1)(\\d{10,11}))?","$2",,,[[,"(\\d{4})(\\d{4})","$1-$2",["[2-9](?:[1-9]|0[1-9])"],"$1","",0],[,"(\\d{5})(\\d{4})","$1-$2",["9(?:[1-9]|0[1-9])"],"$1","",0],[,"(\\d{3,5})","$1",["1[125689]"],"$1","",0],[,"(\\d{2})(\\d{5})(\\d{4})","$1 $2-$3",["(?:1[1-9]|2[12478])9"],"($1)","0 $CC ($1)",0],[,"(\\d{2})(\\d{4})(\\d{4})","$1 $2-$3",["[1-9][1-9]"],"($1)","0 $CC ($1)",
0],[,"([34]00\\d)(\\d{4})","$1-$2",["[34]00"],"","",0],[,"([3589]00)(\\d{2,3})(\\d{4})","$1 $2 $3",["[3589]00"],"0$1","",0]],[[,"(\\d{2})(\\d{5})(\\d{4})","$1 $2-$3",["(?:1[1-9]|2[12478])9","(?:1[1-9]|2[12478])9"],"($1)","0 $CC ($1)",0],[,"(\\d{2})(\\d{4})(\\d{4})","$1 $2-$3",["[1-9][1-9]","[1-9][1-9]"],"($1)","0 $CC ($1)",0],[,"([34]00\\d)(\\d{4})","$1-$2",["[34]00","[34]00"],"","",0],[,"([3589]00)(\\d{2,3})(\\d{4})","$1 $2 $3",["[3589]00","[3589]00"],"0$1","",0]],[,,"NA","NA"],,,[,,"[34]00\\d{5}",
"\\d{8}",,,"40041234"],[,,"NA","NA"],,,[,,"NA","NA"]],BS:[,[,,"[2589]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"242(?:3(?:02|[236][1-9]|4[0-24-9]|5[0-68]|7[3467]|8[0-4]|9[2-467])|461|502|6(?:12|7[67]|8[78]|9[89])|702)\\d{4}","\\d{7}(?:\\d{3})?",,,"2423456789"],[,,"242(?:3(?:5[79]|[79]5)|4(?:[2-4][1-9]|5[1-8]|6[2-8]|7\\d|81)|5(?:2[45]|3[35]|44|5[1-9]|65|77)|6[34]6|727)\\d{4}","\\d{10}",,,"2423591234"],[,,"242300\\d{4}|8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002123456"],[,,"900[2-9]\\d{6}","\\d{10}",
,,"9002123456"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"BS",1,"011","1",,,"1",,,,,,[,,"NA","NA"],,"242",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],BT:[,[,,"[1-8]\\d{6,7}","\\d{6,8}"],[,,"(?:2[3-6]|[34][5-7]|5[236]|6[2-46]|7[246]|8[2-4])\\d{5}","\\d{6,7}",,,"2345678"],[,,"[17]7\\d{6}","\\d{8}",,,"17123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"BT",975,"00",,,,,,,,[[,"([17]7)(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["1|77"],
"","",0],[,"([2-8])(\\d{3})(\\d{3})","$1 $2 $3",["[2-68]|7[246]"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],BW:[,[,,"[2-79]\\d{6,7}","\\d{7,8}"],[,,"(?:2(?:4[0-48]|6[0-24]|9[0578])|3(?:1[0235-9]|55|6\\d|7[01]|9[0-57])|4(?:6[03]|7[1267]|9[0-5])|5(?:3[0389]|4[0489]|7[1-47]|88|9[0-49])|6(?:2[1-35]|5[149]|8[067]))\\d{4}","\\d{7}",,,"2401234"],[,,"7(?:[1-35]\\d{6}|[46][0-7]\\d{5}|7[01467]\\d{5})","\\d{8}",,,"71123456"],[,,"NA","NA"],[,,"90\\d{5}","\\d{7}",,,"9012345"],[,,
"NA","NA"],[,,"NA","NA"],[,,"79[12][01]\\d{4}","\\d{8}",,,"79101234"],"BW",267,"00",,,,,,,,[[,"(\\d{3})(\\d{4})","$1 $2",["[2-6]"],"","",0],[,"(7\\d)(\\d{3})(\\d{3})","$1 $2 $3",["7"],"","",0],[,"(90)(\\d{5})","$1 $2",["9"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],BY:[,[,,"[1-4]\\d{8}|[89]\\d{9,10}","\\d{7,11}"],[,,"(?:1(?:5(?:1[1-5]|[24]\\d|6[2-4]|9[1-7])|6(?:[235]\\d|4[1-7])|7\\d{2})|2(?:1(?:[246]\\d|3[0-35-9]|5[1-9])|2(?:[235]\\d|4[0-8])|3(?:[26]\\d|3[02-79]|4[024-7]|5[03-7])))\\d{5}",
"\\d{7,9}",,,"152450911"],[,,"(?:2(?:5[5679]|9[1-9])|33\\d|44\\d)\\d{6}","\\d{9}",,,"294911911"],[,,"8(?:0[13]|20\\d)\\d{7}","\\d{10,11}",,,"8011234567"],[,,"(?:810|902)\\d{7}","\\d{10}",,,"9021234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"BY",375,"810","8",,,"8?0?",,"8~10",,[[,"(\\d{2})(\\d{3})(\\d{2})(\\d{2})","$1 $2-$3-$4",["17[0-3589]|2[4-9]|[34]","17(?:[02358]|1[0-2]|9[0189])|2[4-9]|[34]"],"8 0$1","",0],[,"(\\d{3})(\\d{2})(\\d{2})(\\d{2})","$1 $2-$3-$4",["1(?:5[24]|6[235]|7[467])|2(?:1[246]|2[25]|3[26])",
"1(?:5[24]|6(?:2|3[04-9]|5[0346-9])|7(?:[46]|7[37-9]))|2(?:1[246]|2[25]|3[26])"],"8 0$1","",0],[,"(\\d{4})(\\d{2})(\\d{3})","$1 $2-$3",["1(?:5[169]|6[3-5]|7[179])|2(?:1[35]|2[34]|3[3-5])","1(?:5[169]|6(?:3[1-3]|4|5[125])|7(?:1[3-9]|7[0-24-6]|9[2-7]))|2(?:1[35]|2[34]|3[3-5])"],"8 0$1","",0],[,"([89]\\d{2})(\\d{3})(\\d{4})","$1 $2 $3",["8[01]|9"],"8 $1","",0],[,"(8\\d{2})(\\d{4})(\\d{4})","$1 $2 $3",["82"],"8 $1","",0]],,[,,"NA","NA"],,,[,,"8(?:[013]|[12]0)\\d{8}|902\\d{7}","\\d{10,11}",,,"82012345678"],
[,,"NA","NA"],,,[,,"NA","NA"]],BZ:[,[,,"[2-8]\\d{6}|0\\d{10}","\\d{7}(?:\\d{4})?"],[,,"[234578][02]\\d{5}","\\d{7}",,,"2221234"],[,,"6[0-367]\\d{5}","\\d{7}",,,"6221234"],[,,"0800\\d{7}","\\d{11}",,,"08001234123"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"BZ",501,"00",,,,,,,,[[,"(\\d{3})(\\d{4})","$1-$2",["[2-8]"],"","",0],[,"(0)(800)(\\d{4})(\\d{3})","$1-$2-$3-$4",["0"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],1,,[,,"NA","NA"]],CA:[,[,,"[2-9]\\d{9}|3\\d{6}","\\d{7}(?:\\d{3})?"],
[,,"(?:2(?:04|[23]6|[48]9|50)|3(?:06|43|65)|4(?:03|1[68]|3[178]|50)|5(?:06|1[49]|79|8[17])|6(?:0[04]|13|39|47)|7(?:0[59]|78|80)|8(?:[06]7|19|73)|90[25])[2-9]\\d{6}|310\\d{4}","\\d{7}(?:\\d{3})?",,,"2042345678"],[,,"(?:2(?:04|[23]6|[48]9|50)|3(?:06|43|65)|4(?:03|1[68]|3[178]|50)|5(?:06|1[49]|79|8[17])|6(?:0[04]|13|39|47)|7(?:0[59]|78|80)|8(?:[06]7|19|73)|90[25])[2-9]\\d{6}","\\d{7}(?:\\d{3})?",,,"2042345678"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}|310\\d{4}","\\d{7}(?:\\d{3})?",,,"8002123456"],[,,"900[2-9]\\d{6}",
"\\d{10}",,,"9002123456"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"CA",1,"011","1",,,"1",,,,,,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],CC:[,[,,"[1458]\\d{5,9}","\\d{6,10}"],[,,"89162\\d{4}","\\d{8,9}",,,"891621234"],[,,"4(?:[0-2]\\d|3[0-57-9]|4[47-9]|5[0-37-9]|6[6-9]|7[07-9]|8[7-9])\\d{6}","\\d{9}",,,"412345678"],[,,"1(?:80(?:0\\d{2})?|3(?:00\\d{2})?)\\d{4}","\\d{6,10}",,,"1800123456"],[,,"190[0126]\\d{6}","\\d{10}",,,"1900123456"],[,
,"NA","NA"],[,,"500\\d{6}","\\d{9}",,,"500123456"],[,,"550\\d{6}","\\d{9}",,,"550123456"],"CC",61,"(?:14(?:1[14]|34|4[17]|[56]6|7[47]|88))?001[14-689]","0",,,"0",,"0011",,,,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],CD:[,[,,"[2-6]\\d{6}|[18]\\d{6,8}|9\\d{8}","\\d{7,9}"],[,,"1(?:2\\d{7}|\\d{6})|[2-6]\\d{6}","\\d{7,9}",,,"1234567"],[,,"8(?:[0-2459]\\d{2}|8)\\d{5}|9[7-9]\\d{7}","\\d{7,9}",,,"991234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"CD",243,
"00","0",,,"0",,,,[[,"(\\d{2})(\\d{3})(\\d{4})","$1 $2 $3",["12"],"0$1","",0],[,"([89]\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["8[0-2459]|9"],"0$1","",0],[,"(\\d{2})(\\d{2})(\\d{3})","$1 $2 $3",["88"],"0$1","",0],[,"(\\d{2})(\\d{5})","$1 $2",["[1-6]"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],CF:[,[,,"[278]\\d{7}","\\d{8}"],[,,"2[12]\\d{6}","\\d{8}",,,"21612345"],[,,"7[0257]\\d{6}","\\d{8}",,,"70012345"],[,,"NA","NA"],[,,"8776\\d{4}","\\d{8}",,,"87761234"],[,,"NA","NA"],
[,,"NA","NA"],[,,"NA","NA"],"CF",236,"00",,,,,,,,[[,"(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],CG:[,[,,"[028]\\d{8}","\\d{9}"],[,,"222[1-589]\\d{5}","\\d{9}",,,"222123456"],[,,"0[14-6]\\d{7}","\\d{9}",,,"061234567"],[,,"800\\d{6}","\\d{9}",,,"800123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"CG",242,"00",,,,,,,,[[,"(\\d{2})(\\d{3})(\\d{4})","$1 $2 $3",["[02]"],"","",0],[,"(\\d)(\\d{4})(\\d{4})","$1 $2 $3",
["8"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],1,,[,,"NA","NA"]],CH:[,[,,"[2-9]\\d{8}|860\\d{9}","\\d{9}(?:\\d{3})?"],[,,"(?:2[12467]|3[1-4]|4[134]|5[256]|6[12]|[7-9]1)\\d{7}","\\d{9}",,,"212345678"],[,,"7[5-9]\\d{7}","\\d{9}",,,"781234567"],[,,"800\\d{6}","\\d{9}",,,"800123456"],[,,"90[016]\\d{6}","\\d{9}",,,"900123456"],[,,"84[0248]\\d{6}","\\d{9}",,,"840123456"],[,,"878\\d{6}","\\d{9}",,,"878123456"],[,,"NA","NA"],"CH",41,"00","0",,,"0",,,,[[,"([2-9]\\d)(\\d{3})(\\d{2})(\\d{2})",
"$1 $2 $3 $4",["[2-7]|[89]1"],"0$1","",0],[,"([89]\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["8[047]|90"],"0$1","",0],[,"(\\d{3})(\\d{2})(\\d{3})(\\d{2})(\\d{2})","$1 $2 $3 $4 $5",["860"],"0$1","",0]],,[,,"74[0248]\\d{6}","\\d{9}",,,"740123456"],,,[,,"NA","NA"],[,,"5[18]\\d{7}","\\d{9}",,,"581234567"],,,[,,"860\\d{9}","\\d{12}",,,"860123456789"]],CI:[,[,,"[02-7]\\d{7}","\\d{8}"],[,,"(?:2(?:0[023]|1[02357]|[23][045]|4[03-5])|3(?:0[06]|1[069]|[2-4][07]|5[09]|6[08]))\\d{5}","\\d{8}",,,"21234567"],[,,"(?:0[1-9]|4[0-24-9]|5[4-9]|6[015-79]|77)\\d{6}",
"\\d{8}",,,"01234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"CI",225,"00",,,,,,,,[[,"(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],1,,[,,"NA","NA"]],CK:[,[,,"[2-57]\\d{4}","\\d{5}"],[,,"(?:2\\d|3[13-7]|4[1-5])\\d{3}","\\d{5}",,,"21234"],[,,"(?:5[0-68]|7\\d)\\d{3}","\\d{5}",,,"71234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"CK",682,"00",,,,,,,,[[,"(\\d{2})(\\d{3})","$1 $2",,"",
"",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],CL:[,[,,"(?:[2-9]|600|123)\\d{7,8}","\\d{6,11}"],[,,"(?:22|3[2-5]|[47][1-35]|5[1-3578]|6[1347])\\d{7}|65\\d{6,7}","\\d{6,9}",,,"221234567"],[,,"9[5-9]\\d{7}","\\d{8,9}",,,"961234567"],[,,"800\\d{6}|1230\\d{7}","\\d{9,11}",,,"800123456"],[,,"NA","NA"],[,,"600\\d{7,8}","\\d{10,11}",,,"6001234567"],[,,"NA","NA"],[,,"44\\d{7}","\\d{9}",,,"441234567"],"CL",56,"(?:0|1(?:1[0-69]|2[0-57]|5[13-58]|69|7[0167]|8[018]))0","0",,,"0|(1(?:1[0-69]|2[0-57]|5[13-58]|69|7[0167]|8[018]))",
,,,[[,"(\\d)(\\d{4})(\\d{4})","$1 $2 $3",["2"],"($1)","$CC ($1)",0],[,"(\\d{2})(\\d{3})(\\d{4})","$1 $2 $3",["[357]|4[1-35]|6[13-57]"],"($1)","$CC ($1)",0],[,"(\\d{2})(\\d{2})(\\d{4})","$1 $2 $3",["65"],"($1)","$CC ($1)",0],[,"(9)([5-9]\\d{3})(\\d{4})","$1 $2 $3",["9"],"0$1","",0],[,"(44)(\\d{3})(\\d{4})","$1 $2 $3",["44"],"0$1","",0],[,"([68]00)(\\d{3})(\\d{3,4})","$1 $2 $3",["60|8"],"$1","",0],[,"(600)(\\d{3})(\\d{2})(\\d{3})","$1 $2 $3 $4",["60"],"$1","",0],[,"(1230)(\\d{3})(\\d{4})","$1 $2 $3",
["1"],"$1","",0],[,"(\\d{4,5})","$1",["[1-9]"],"$1","",0]],[[,"(\\d)(\\d{4})(\\d{4})","$1 $2 $3",["2","2"],"($1)","$CC ($1)",0],[,"(\\d{2})(\\d{3})(\\d{4})","$1 $2 $3",["[357]|4[1-35]|6[13-57]","[357]|4[1-35]|6[13-57]"],"($1)","$CC ($1)",0],[,"(\\d{2})(\\d{2})(\\d{4})","$1 $2 $3",["65","65"],"($1)","$CC ($1)",0],[,"(9)([5-9]\\d{3})(\\d{4})","$1 $2 $3",["9","9"],"0$1","",0],[,"(44)(\\d{3})(\\d{4})","$1 $2 $3",["44","44"],"0$1","",0],[,"([68]00)(\\d{3})(\\d{3,4})","$1 $2 $3",["60|8","60|8"],"$1","",
0],[,"(600)(\\d{3})(\\d{2})(\\d{3})","$1 $2 $3 $4",["60","60"],"$1","",0],[,"(1230)(\\d{3})(\\d{4})","$1 $2 $3",["1","1"],"$1","",0]],[,,"NA","NA"],,,[,,"600\\d{7,8}","\\d{10,11}",,,"6001234567"],[,,"NA","NA"],,,[,,"NA","NA"]],CM:[,[,,"[2357-9]\\d{7}","\\d{8}"],[,,"(?:22|33)\\d{6}","\\d{8}",,,"22123456"],[,,"[579]\\d{7}","\\d{8}",,,"71234567"],[,,"800\\d{5}","\\d{8}",,,"80012345"],[,,"88\\d{6}","\\d{8}",,,"88012345"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"CM",237,"00",,,,,,,,[[,"([2357-9]\\d)(\\d{2})(\\d{2})(\\d{2})",
"$1 $2 $3 $4",["[23579]|88"],"","",0],[,"(800)(\\d{2})(\\d{3})","$1 $2 $3",["80"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],CN:[,[,,"[1-7]\\d{6,11}|8[0-357-9]\\d{6,9}|9\\d{9}","\\d{4,12}"],[,,"21(?:100\\d{2}|95\\d{3,4}|\\d{8,10})|(?:10|2[02-57-9]|3(?:11|7[179])|4(?:[15]1|3[12])|5(?:1\\d|2[37]|3[12]|51|7[13-79]|9[15])|7(?:31|5[457]|6[09]|91)|8(?:71|98))(?:100\\d{2}|95\\d{3,4}|\\d{8})|(?:3(?:1[02-9]|35|49|5\\d|7[02-68]|9[1-68])|4(?:1[02-9]|2[179]|3[3-9]|5[2-9]|6[4789]|7\\d|8[23])|5(?:3[03-9]|4[36]|5[02-9]|6[1-46]|7[028]|80|9[2-46-9])|6(?:3[1-5]|6[0238]|9[12])|7(?:01|[17]\\d|2[248]|3[04-9]|4[3-6]|5[0-3689]|6[2368]|9[02-9])|8(?:1[236-8]|2[5-7]|3\\d|5[1-9]|7[02-9]|8[3678]|9[1-7])|9(?:0[1-3689]|1[1-79]|[379]\\d|4[13]|5[1-5]))(?:100\\d{2}|95\\d{3,4}|\\d{7})|80(?:29|6[03578]|7[018]|81)\\d{4}",
"\\d{4,12}",,,"1012345678"],[,,"1(?:[38]\\d|4[57]|5[0-35-9])\\d{8}","\\d{11}",,,"13123456789"],[,,"(?:10)?800\\d{7}","\\d{10,12}",,,"8001234567"],[,,"16[08]\\d{5}","\\d{8}",,,"16812345"],[,,"400\\d{7}","\\d{10}",,,"4001234567"],[,,"NA","NA"],[,,"NA","NA"],"CN",86,"(1[1279]\\d{3})?00","0",,,"(1[1279]\\d{3})|0",,"00",,[[,"(80\\d{2})(\\d{4})","$1 $2",["80[2678]"],"0$1","$CC $1",1],[,"([48]00)(\\d{3})(\\d{4})","$1 $2 $3",["[48]00"],"","",0],[,"(\\d{5,6})","$1",["100|95"],"","",0],[,"(\\d{2})(\\d{5,6})",
"$1 $2",["(?:10|2\\d)[19]","(?:10|2\\d)(?:10|95)","(?:10|2\\d)(?:100|95)"],"0$1","$CC $1",0],[,"(\\d{3})(\\d{5,6})","$1 $2",["[3-9]","[3-9]\\d{2}[19]","[3-9]\\d{2}(?:10|95)"],"0$1","$CC $1",0],[,"(\\d{3,4})(\\d{4})","$1 $2",["[2-9]"],"","",0],[,"(21)(\\d{4})(\\d{4,6})","$1 $2 $3",["21"],"0$1","$CC $1",1],[,"([12]\\d)(\\d{4})(\\d{4})","$1 $2 $3",["10[1-9]|2[02-9]","10[1-9]|2[02-9]","10(?:[1-79]|8(?:[1-9]|0[1-9]))|2[02-9]"],"0$1","$CC $1",1],[,"(\\d{3})(\\d{4})(\\d{4})","$1 $2 $3",["3(?:11|7[179])|4(?:[15]1|3[12])|5(?:1|2[37]|3[12]|51|7[13-79]|9[15])|7(?:31|5[457]|6[09]|91)|8(?:71|98)"],
"0$1","$CC $1",1],[,"(\\d{3})(\\d{3})(\\d{4})","$1 $2 $3",["3(?:1[02-9]|35|49|5|7[02-68]|9[1-68])|4(?:1[02-9]|2[179]|[35][2-9]|6[4789]|7\\d|8[23])|5(?:3[03-9]|4[36]|5[02-9]|6[1-46]|7[028]|80|9[2-46-9])|6(?:3[1-5]|6[0238]|9[12])|7(?:01|[1579]|2[248]|3[04-9]|4[3-6]|6[2368])|8(?:1[236-8]|2[5-7]|3|5[1-9]|7[02-9]|8[3678]|9[1-7])|9(?:0[1-3689]|1[1-79]|[379]|4[13]|5[1-5])"],"0$1","$CC $1",1],[,"(1[3-58]\\d)(\\d{4})(\\d{4})","$1 $2 $3",["1[3-58]"],"","$CC $1",0],[,"(10800)(\\d{3})(\\d{4})","$1 $2 $3",["108",
"1080","10800"],"","",0]],[[,"(80\\d{2})(\\d{4})","$1 $2",["80[2678]","80[2678]"],"0$1","$CC $1",1],[,"([48]00)(\\d{3})(\\d{4})","$1 $2 $3",["[48]00","[48]00"],"","",0],[,"(\\d{2})(\\d{5,6})","$1 $2","(?:10|2\\d)[19] (?:10|2\\d)(?:10|95) (?:10|2\\d)(?:100|95) (?:10|2\\d)[19] (?:10|2\\d)(?:10|95) (?:10|2\\d)(?:100|95)".split(" "),"0$1","$CC $1",0],[,"(\\d{3})(\\d{5,6})","$1 $2","[3-9] [3-9]\\d{2}[19] [3-9]\\d{2}(?:10|95) [3-9] [3-9]\\d{2}[19] [3-9]\\d{2}(?:10|95)".split(" "),"0$1","$CC $1",0],[,"(21)(\\d{4})(\\d{4,6})",
"$1 $2 $3",["21","21"],"0$1","$CC $1",1],[,"([12]\\d)(\\d{4})(\\d{4})","$1 $2 $3","10[1-9]|2[02-9] 10[1-9]|2[02-9] 10(?:[1-79]|8(?:[1-9]|0[1-9]))|2[02-9] 10[1-9]|2[02-9] 10[1-9]|2[02-9] 10(?:[1-79]|8(?:[1-9]|0[1-9]))|2[02-9]".split(" "),"0$1","$CC $1",1],[,"(\\d{3})(\\d{4})(\\d{4})","$1 $2 $3",["3(?:11|7[179])|4(?:[15]1|3[12])|5(?:1|2[37]|3[12]|51|7[13-79]|9[15])|7(?:31|5[457]|6[09]|91)|8(?:71|98)","3(?:11|7[179])|4(?:[15]1|3[12])|5(?:1|2[37]|3[12]|51|7[13-79]|9[15])|7(?:31|5[457]|6[09]|91)|8(?:71|98)"],
"0$1","$CC $1",1],[,"(\\d{3})(\\d{3})(\\d{4})","$1 $2 $3",["3(?:1[02-9]|35|49|5|7[02-68]|9[1-68])|4(?:1[02-9]|2[179]|[35][2-9]|6[4789]|7\\d|8[23])|5(?:3[03-9]|4[36]|5[02-9]|6[1-46]|7[028]|80|9[2-46-9])|6(?:3[1-5]|6[0238]|9[12])|7(?:01|[1579]|2[248]|3[04-9]|4[3-6]|6[2368])|8(?:1[236-8]|2[5-7]|3|5[1-9]|7[02-9]|8[3678]|9[1-7])|9(?:0[1-3689]|1[1-79]|[379]|4[13]|5[1-5])","3(?:1[02-9]|35|49|5|7[02-68]|9[1-68])|4(?:1[02-9]|2[179]|[35][2-9]|6[4789]|7\\d|8[23])|5(?:3[03-9]|4[36]|5[02-9]|6[1-46]|7[028]|80|9[2-46-9])|6(?:3[1-5]|6[0238]|9[12])|7(?:01|[1579]|2[248]|3[04-9]|4[3-6]|6[2368])|8(?:1[236-8]|2[5-7]|3|5[1-9]|7[02-9]|8[3678]|9[1-7])|9(?:0[1-3689]|1[1-79]|[379]|4[13]|5[1-5])"],
"0$1","$CC $1",1],[,"(1[3-58]\\d)(\\d{4})(\\d{4})","$1 $2 $3",["1[3-58]","1[3-58]"],"","$CC $1",0],[,"(10800)(\\d{3})(\\d{4})","$1 $2 $3","108 1080 10800 108 1080 10800".split(" "),"","",0]],[,,"NA","NA"],,,[,,"(?:4|(?:10)?8)00\\d{7}","\\d{10,12}",,,"4001234567"],[,,"NA","NA"],,,[,,"NA","NA"]],CO:[,[,,"(?:[13]\\d{0,3}|[24-8])\\d{7}","\\d{7,11}"],[,,"[124-8][2-9]\\d{6}","\\d{8}",,,"12345678"],[,,"3(?:0[0-5]|1\\d|[25][01])\\d{7}","\\d{10}",,,"3211234567"],[,,"1800\\d{7}","\\d{11}",,,"18001234567"],
[,,"19(?:0[01]|4[78])\\d{7}","\\d{11}",,,"19001234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"CO",57,"00(?:4(?:[14]4|56)|[579])","0",,,"0([3579]|4(?:44|56))?",,,,[[,"(\\d)(\\d{7})","$1 $2",["1(?:8[2-9]|9[0-3]|[2-7])|[24-8]","1(?:8[2-9]|9(?:09|[1-3])|[2-7])|[24-8]"],"($1)","0$CC $1",0],[,"(\\d{3})(\\d{7})","$1 $2",["3"],"","0$CC $1",0],[,"(1)(\\d{3})(\\d{7})","$1-$2-$3",["1(?:80|9[04])","1(?:800|9(?:0[01]|4[78]))"],"0$1","",0]],[[,"(\\d)(\\d{7})","$1 $2",["1(?:8[2-9]|9[0-3]|[2-7])|[24-8]","1(?:8[2-9]|9(?:09|[1-3])|[2-7])|[24-8]",
"1(?:8[2-9]|9[0-3]|[2-7])|[24-8]","1(?:8[2-9]|9(?:09|[1-3])|[2-7])|[24-8]"],"($1)","0$CC $1",0],[,"(\\d{3})(\\d{7})","$1 $2",["3","3"],"","0$CC $1",0],[,"(1)(\\d{3})(\\d{7})","$1 $2 $3",["1(?:80|9[04])","1(?:800|9(?:0[01]|4[78]))"]]],[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],CR:[,[,,"[24-9]\\d{7,9}","\\d{8,10}"],[,,"2[24-7]\\d{6}","\\d{8}",,,"22123456"],[,,"5(?:0[0-4]|7[0-3])\\d{5}|6(?:[0-2]\\d|30)\\d{5}|7[0-3]\\d{6}|8[3-9]\\d{6}","\\d{8}",,,"83123456"],[,,"800\\d{7}","\\d{10}",
,,"8001234567"],[,,"90[059]\\d{7}","\\d{10}",,,"9001234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"210[0-6]\\d{4}|4(?:0(?:[04]0\\d{4}|10[0-3]\\d{3}|2900\\d{2}|3[01]\\d{4}|5\\d{5}|70[01]\\d{3}|8[0-2]\\d{4})|1[01]\\d{5}|20[0-3]\\d{4}|400\\d{4}|70[0-2]\\d{4})|5100\\d{4}","\\d{8}",,,"40001234"],"CR",506,"00",,,,"(19(?:0[01468]|19|20|66|77))",,,,[[,"(\\d{4})(\\d{4})","$1 $2",["[24-7]|8[3-9]"],"","$CC $1",0],[,"(\\d{3})(\\d{3})(\\d{4})","$1-$2-$3",["[89]0"],"","$CC $1",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA",
"NA"],,,[,,"NA","NA"]],CU:[,[,,"[2-57]\\d{5,7}","\\d{4,8}"],[,,"2[1-4]\\d{5,6}|3(?:1\\d{6}|[23]\\d{4,6})|4(?:[125]\\d{5,6}|[36]\\d{6}|[78]\\d{4,6})|7\\d{6,7}","\\d{4,8}",,,"71234567"],[,,"5\\d{7}","\\d{8}",,,"51234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"CU",53,"119","0",,,"0",,,,[[,"(\\d)(\\d{6,7})","$1 $2",["7"],"(0$1)","",0],[,"(\\d{2})(\\d{4,6})","$1 $2",["[2-4]"],"(0$1)","",0],[,"(\\d)(\\d{7})","$1 $2",["5"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA",
"NA"],,,[,,"NA","NA"]],CV:[,[,,"[259]\\d{6}","\\d{7}"],[,,"2(?:2[1-7]|3[0-8]|4[12]|5[1256]|6\\d|7[1-3]|8[1-5])\\d{4}","\\d{7}",,,"2211234"],[,,"(?:9\\d|59)\\d{5}","\\d{7}",,,"9911234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"CV",238,"0",,,,,,,,[[,"(\\d{3})(\\d{2})(\\d{2})","$1 $2 $3",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],CW:[,[,,"[169]\\d{6,7}","\\d{7,8}"],[,,"9(?:[48]\\d{2}|50\\d|7(?:2[0-24]|[34]\\d|6[35-7]|77|8[7-9]))\\d{4}","\\d{7,8}",
,,"94151234"],[,,"9(?:5(?:[1246]\\d|3[01])|6(?:[16-9]\\d|3[01]))\\d{4}","\\d{7,8}",,,"95181234"],[,,"NA","NA"],[,,"NA","NA"],[,,"(?:10|69)\\d{5}","\\d{7}",,,"1011234"],[,,"NA","NA"],[,,"NA","NA"],"CW",599,"00",,,,,,,,[[,"(\\d{3})(\\d{4})","$1 $2",["[13-7]"],"","",0],[,"(9)(\\d{3})(\\d{4})","$1 $2 $3",["9"],"","",0]],,[,,"955\\d{5}","\\d{7,8}",,,"95581234"],1,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],CX:[,[,,"[1458]\\d{5,9}","\\d{6,10}"],[,,"89164\\d{4}","\\d{8,9}",,,"891641234"],[,,"4(?:[0-2]\\d|3[0-57-9]|4[47-9]|5[0-37-9]|6[6-9]|7[07-9]|8[7-9])\\d{6}",
"\\d{9}",,,"412345678"],[,,"1(?:80(?:0\\d{2})?|3(?:00\\d{2})?)\\d{4}","\\d{6,10}",,,"1800123456"],[,,"190[0126]\\d{6}","\\d{10}",,,"1900123456"],[,,"NA","NA"],[,,"500\\d{6}","\\d{9}",,,"500123456"],[,,"550\\d{6}","\\d{9}",,,"550123456"],"CX",61,"(?:14(?:1[14]|34|4[17]|[56]6|7[47]|88))?001[14-689]","0",,,"0",,"0011",,,,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],CY:[,[,,"[257-9]\\d{7}","\\d{8}"],[,,"2[2-6]\\d{6}","\\d{8}",,,"22345678"],[,,"9[5-79]\\d{6}","\\d{8}",,,"96123456"],[,,
"800\\d{5}","\\d{8}",,,"80001234"],[,,"90[09]\\d{5}","\\d{8}",,,"90012345"],[,,"80[1-9]\\d{5}","\\d{8}",,,"80112345"],[,,"700\\d{5}","\\d{8}",,,"70012345"],[,,"NA","NA"],"CY",357,"00",,,,,,,,[[,"(\\d{2})(\\d{6})","$1 $2",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"(?:50|77)\\d{6}","\\d{8}",,,"77123456"],,,[,,"NA","NA"]],CZ:[,[,,"[2-8]\\d{8}|9\\d{8,11}","\\d{9,12}"],[,,"2\\d{8}|(?:3[1257-9]|4[16-9]|5[13-9])\\d{7}","\\d{9,12}",,,"212345678"],[,,"(?:60[1-8]|7(?:0[2-5]|[2379]\\d))\\d{6}","\\d{9,12}",
,,"601123456"],[,,"800\\d{6}","\\d{9,12}",,,"800123456"],[,,"9(?:0[05689]|76)\\d{6}","\\d{9,12}",,,"900123456"],[,,"8[134]\\d{7}","\\d{9,12}",,,"811234567"],[,,"70[01]\\d{6}","\\d{9,12}",,,"700123456"],[,,"9[17]0\\d{6}","\\d{9,12}",,,"910123456"],"CZ",420,"00",,,,,,,,[[,"([2-9]\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["[2-8]|9[015-7]"],"","",0],[,"(96\\d)(\\d{3})(\\d{3})(\\d{3})","$1 $2 $3 $4",["96"],"","",0],[,"(9\\d)(\\d{3})(\\d{3})(\\d{3})","$1 $2 $3 $4",["9[36]"],"","",0]],,[,,"NA","NA"],,,[,,"NA",
"NA"],[,,"9(?:5\\d|7[234])\\d{6}","\\d{9,12}",,,"972123456"],,,[,,"9(?:3\\d{9}|6\\d{7,10})","\\d{9,12}",,,"93123456789"]],DE:[,[,,"[1-35-9]\\d{3,14}|4(?:[0-8]\\d{4,12}|9(?:[0-37]\\d|4(?:[1-35-8]|4\\d?)|5\\d{1,2}|6[1-8]\\d?)\\d{2,7})","\\d{2,15}"],[,,"[246]\\d{5,13}|3(?:0\\d{3,13}|2\\d{9}|[3-9]\\d{4,13})|5(?:0[2-8]|[1256]\\d|[38][0-8]|4\\d{0,2}|[79][0-7])\\d{3,11}|7(?:0[2-8]|[1-9]\\d)\\d{3,10}|8(?:0[2-9]|[1-9]\\d)\\d{3,10}|9(?:0[6-9]\\d{3,10}|1\\d{4,12}|[2-9]\\d{4,11})","\\d{2,15}",,,"30123456"],[,
,"1(?:5[0-2579]\\d{8}|6[023]\\d{7,8}|7(?:[0-57-9]\\d?|6\\d)\\d{7})","\\d{10,11}",,,"15123456789"],[,,"800\\d{7,12}","\\d{10,15}",,,"8001234567890"],[,,"900(?:[135]\\d{6}|9\\d{7})","\\d{10,11}",,,"9001234567"],[,,"180\\d{5,11}","\\d{8,14}",,,"18012345"],[,,"700\\d{8}","\\d{11}",,,"70012345678"],[,,"NA","NA"],"DE",49,"00","0",,,"0",,,,[[,"(1\\d{2})(\\d{7,8})","$1 $2",["1[67]"],"0$1","",0],[,"(1\\d{3})(\\d{7})","$1 $2",["15"],"0$1","",0],[,"(\\d{2})(\\d{3,11})","$1 $2",["3[02]|40|[68]9"],"0$1","",0],
[,"(\\d{3})(\\d{3,11})","$1 $2",["2(?:\\d1|0[2389]|1[24]|28|34)|3(?:[3-9][15]|40)|[4-8][1-9]1|9(?:06|[1-9]1)"],"0$1","",0],[,"(\\d{4})(\\d{2,11})","$1 $2",["[24-6]|[7-9](?:\\d[1-9]|[1-9]\\d)|3(?:[3569][02-46-9]|4[2-4679]|7[2-467]|8[2-46-8])","[24-6]|[7-9](?:\\d[1-9]|[1-9]\\d)|3(?:3(?:0[1-467]|2[127-9]|3[124578]|[46][1246]|7[1257-9]|8[1256]|9[145])|4(?:2[135]|3[1357]|4[13578]|6[1246]|7[1356]|9[1346])|5(?:0[14]|2[1-3589]|3[1357]|4[1246]|6[1-4]|7[1346]|8[13568]|9[1246])|6(?:0[356]|2[1-489]|3[124-6]|4[1347]|6[13]|7[12579]|8[1-356]|9[135])|7(?:2[1-7]|3[1357]|4[145]|6[1-5]|7[1-4])|8(?:21|3[1468]|4[1347]|6[0135-9]|7[1467]|8[136])|9(?:0[12479]|2[1358]|3[1357]|4[134679]|6[1-9]|7[136]|8[147]|9[1468]))"],
"0$1","",0],[,"(3\\d{4})(\\d{1,10})","$1 $2",["3"],"0$1","",0],[,"(800)(\\d{7,12})","$1 $2",["800"],"0$1","",0],[,"(177)(99)(\\d{7,8})","$1 $2 $3",["177","1779","17799"],"0$1","",0],[,"(\\d{3})(\\d)(\\d{4,10})","$1 $2 $3",["(?:18|90)0","180|900[1359]"],"0$1","",0],[,"(1\\d{2})(\\d{5,11})","$1 $2",["181"],"0$1","",0],[,"(18\\d{3})(\\d{6})","$1 $2",["185","1850","18500"],"0$1","",0],[,"(18\\d{2})(\\d{7})","$1 $2",["18[68]"],"0$1","",0],[,"(18\\d)(\\d{8})","$1 $2",["18[2-579]"],"0$1","",0],[,"(700)(\\d{4})(\\d{4})",
"$1 $2 $3",["700"],"0$1","",0]],,[,,"16(?:4\\d{1,10}|[89]\\d{1,11})","\\d{4,14}",,,"16412345"],,,[,,"NA","NA"],[,,"18(?:1\\d{5,11}|[2-9]\\d{8})","\\d{8,14}",,,"18500123456"],,,[,,"17799\\d{7,8}","\\d{12,13}",,,"177991234567"]],DJ:[,[,,"[27]\\d{7}","\\d{8}"],[,,"2(?:1[2-5]|7[45])\\d{5}","\\d{8}",,,"21360003"],[,,"77[6-8]\\d{5}","\\d{8}",,,"77831001"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"DJ",253,"00",,,,,,,,[[,"(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",,"","",
0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],DK:[,[,,"[2-9]\\d{7}","\\d{8}"],[,,"(?:[2-7]\\d|8[126-9]|9[126-9])\\d{6}","\\d{8}",,,"32123456"],[,,"(?:[2-7]\\d|8[126-9]|9[126-9])\\d{6}","\\d{8}",,,"20123456"],[,,"80\\d{6}","\\d{8}",,,"80123456"],[,,"90\\d{6}","\\d{8}",,,"90123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"DK",45,"00",,,,,,,1,[[,"(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],DM:[,[,,"[57-9]\\d{9}",
"\\d{7}(?:\\d{3})?"],[,,"767(?:2(?:55|66)|4(?:2[01]|4[0-25-9])|50[0-4])\\d{4}","\\d{7}(?:\\d{3})?",,,"7674201234"],[,,"767(?:2(?:[234689]5|7[5-7])|31[5-7]|61[2-7])\\d{4}","\\d{10}",,,"7672251234"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002123456"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002123456"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"DM",1,"011","1",,,"1",,,,,,[,,"NA","NA"],,"767",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],DO:[,[,,"[589]\\d{9}",
"\\d{7}(?:\\d{3})?"],[,,"8(?:[04]9[2-9]\\d{6}|29(?:2(?:[0-59]\\d|6[04-9]|7[0-27]|8[0237-9])|3(?:[0-35-9]\\d|4[7-9])|[45]\\d{2}|6(?:[0-27-9]\\d|[3-5][1-9]|6[0135-8])|7(?:0[013-9]|[1-37]\\d|4[1-35689]|5[1-4689]|6[1-57-9]|8[1-79]|9[1-8])|8(?:0[146-9]|1[0-48]|[248]\\d|3[1-79]|5[01589]|6[013-68]|7[124-8]|9[0-8])|9(?:[0-24]\\d|3[02-46-9]|5[0-79]|60|7[0169]|8[57-9]|9[02-9]))\\d{4})","\\d{7}(?:\\d{3})?",,,"8092345678"],[,,"8[024]9[2-9]\\d{6}","\\d{7}(?:\\d{3})?",,,"8092345678"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}",
"\\d{10}",,,"8002123456"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002123456"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"DO",1,"011","1",,,"1",,,,,,[,,"NA","NA"],,"8[024]9",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],DZ:[,[,,"(?:[1-4]|[5-9]\\d)\\d{7}","\\d{8,9}"],[,,"(?:1\\d|2[014-79]|3[0-8]|4[0135689])\\d{6}|9619\\d{5}","\\d{8,9}",,,"12345678"],[,,"(?:5[4-6]|7[7-9])\\d{7}|6(?:[569]\\d|7[0-2])\\d{6}","\\d{9}",,,"551234567"],[,,"800\\d{6}","\\d{9}",,,"800123456"],
[,,"80[3-689]1\\d{5}","\\d{9}",,,"808123456"],[,,"80[12]1\\d{5}","\\d{9}",,,"801123456"],[,,"NA","NA"],[,,"98[23]\\d{6}","\\d{9}",,,"983123456"],"DZ",213,"00","0",,,"0",,,,[[,"([1-4]\\d)(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["[1-4]"],"0$1","",0],[,"([5-8]\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["[5-8]"],"0$1","",0],[,"(9\\d)(\\d{3})(\\d{2})(\\d{2})","$1 $2 $3 $4",["9"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],EC:[,[,,"1\\d{9,10}|[2-8]\\d{7}|9\\d{8}","\\d{7,11}"],
[,,"[2-7][2-7]\\d{6}","\\d{7,8}",,,"22123456"],[,,"9(?:39|[45][89]|[67][7-9]|[89]\\d)\\d{6}","\\d{9}",,,"991234567"],[,,"1800\\d{6,7}","\\d{10,11}",,,"18001234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"[2-7]890\\d{4}","\\d{8}",,,"28901234"],"EC",593,"00","0",,,"0",,,,[[,"(\\d)(\\d{3})(\\d{4})","$1 $2-$3",["[247]|[356][2-8]"],"(0$1)","",0],[,"(\\d{2})(\\d{3})(\\d{4})","$1 $2 $3",["9"],"0$1","",0],[,"(1800)(\\d{3})(\\d{3,4})","$1 $2 $3",["1"],"$1","",0]],[[,"(\\d)(\\d{3})(\\d{4})","$1-$2-$3",
["[247]|[356][2-8]"]],[,"(\\d{2})(\\d{3})(\\d{4})","$1 $2 $3",["9","9"],"0$1","",0],[,"(1800)(\\d{3})(\\d{3,4})","$1 $2 $3",["1","1"],"$1","",0]],[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],EE:[,[,,"1\\d{3,4}|[3-9]\\d{6,7}|800\\d{6,7}","\\d{4,10}"],[,,"(?:3[23589]|4(?:0\\d|[3-8])|6\\d|7[1-9]|88)\\d{5}","\\d{7,8}",,,"3212345"],[,,"(?:5\\d|8[1-5])\\d{6}|5(?:[02]\\d{2}|1(?:[0-8]\\d|95)|5[0-478]\\d|64[0-4]|65[1-589])\\d{3}","\\d{7,8}",,,"51234567"],[,,"800(?:0\\d{3}|1\\d|[2-9])\\d{3}",
"\\d{7,10}",,,"80012345"],[,,"900\\d{4}","\\d{7}",,,"9001234"],[,,"NA","NA"],[,,"70[0-2]\\d{5}","\\d{8}",,,"70012345"],[,,"NA","NA"],"EE",372,"00",,,,,,,,[[,"([3-79]\\d{2})(\\d{4})","$1 $2",["[369]|4[3-8]|5(?:[0-2]|5[0-478]|6[45])|7[1-9]","[369]|4[3-8]|5(?:[02]|1(?:[0-8]|95)|5[0-478]|6(?:4[0-4]|5[1-589]))|7[1-9]"],"","",0],[,"(70)(\\d{2})(\\d{4})","$1 $2 $3",["70"],"","",0],[,"(8000)(\\d{3})(\\d{3})","$1 $2 $3",["800","8000"],"","",0],[,"([458]\\d{3})(\\d{3,4})","$1 $2",["40|5|8(?:00|[1-5])","40|5|8(?:00[1-9]|[1-5])"],
"","",0]],,[,,"NA","NA"],,,[,,"1\\d{3,4}|800[2-9]\\d{3}","\\d{4,7}",,,"8002123"],[,,"1(?:2[01245]|3[0-6]|4[1-489]|5[0-59]|6[1-46-9]|7[0-27-9]|8[189]|9[012])\\d{1,2}","\\d{4,5}",,,"12123"],,,[,,"NA","NA"]],EG:[,[,,"1\\d{4,9}|[2456]\\d{8}|3\\d{7}|[89]\\d{8,9}","\\d{5,10}"],[,,"(?:1(3[23]\\d|5(?:[23]|9\\d))|2[2-4]\\d{2}|3\\d{2}|4(?:0[2-5]|[578][23]|64)\\d|5(?:0[2-7]|[57][23])\\d|6[24-689]3\\d|8(?:2[2-57]|4[26]|6[237]|8[2-4])\\d|9(?:2[27]|3[24]|52|6[2356]|7[2-4])\\d)\\d{5}|1[69]\\d{3}","\\d{5,9}",,,"234567890"],
[,,"1(?:0[0-269]|1[0-245]|2[0-278])\\d{7}","\\d{10}",,,"1001234567"],[,,"800\\d{7}","\\d{10}",,,"8001234567"],[,,"900\\d{7}","\\d{10}",,,"9001234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"EG",20,"00","0",,,"0",,,,[[,"(\\d)(\\d{7,8})","$1 $2",["[23]"],"0$1","",0],[,"(\\d{3})(\\d{3})(\\d{4})","$1 $2 $3",["1[012]|[89]00"],"0$1","",0],[,"(\\d{2})(\\d{6,7})","$1 $2",["1[35]|[4-6]|[89][2-9]"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],EH:[,[,,"[5689]\\d{8}","\\d{9}"],
[,,"528[89]\\d{5}","\\d{9}",,,"528812345"],[,,"6(?:0[0-8]|[12-7]\\d|8[01]|9[27-9])\\d{6}","\\d{9}",,,"650123456"],[,,"80\\d{7}","\\d{9}",,,"801234567"],[,,"89\\d{7}","\\d{9}",,,"891234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"EH",212,"00","0",,,"0",,,,,,[,,"NA","NA"],,"528[89]",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],ER:[,[,,"[178]\\d{6}","\\d{6,7}"],[,,"1(?:1[12568]|20|40|55|6[146])\\d{4}|8\\d{6}","\\d{6,7}",,,"8370362"],[,,"17[1-3]\\d{4}|7\\d{6}","\\d{7}",,,"7123456"],[,,"NA","NA"],
[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"ER",291,"00","0",,,"0",,,,[[,"(\\d)(\\d{3})(\\d{3})","$1 $2 $3",,"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],ES:[,[,,"[5-9]\\d{8}","\\d{9}"],[,,"8(?:[13]0|[28][0-8]|[47][1-9]|5[01346-9]|6[0457-9])\\d{6}|9(?:[1238][0-8]\\d{6}|4[1-9]\\d{6}|5\\d{7}|6(?:[0-8]\\d{6}|9(?:0(?:[0-57-9]\\d{4}|6(?:0[0-8]|1[1-9]|[2-9]\\d)\\d{2})|[1-9]\\d{5}))|7(?:[124-9]\\d{2}|3(?:[0-8]\\d|9[1-9]))\\d{4})","\\d{9}",,,"810123456"],[,,"(?:6\\d{6}|7[1-4]\\d{5}|9(?:6906(?:09|10)|7390\\d{2}))\\d{2}",
"\\d{9}",,,"612345678"],[,,"[89]00\\d{6}","\\d{9}",,,"800123456"],[,,"80[367]\\d{6}","\\d{9}",,,"803123456"],[,,"90[12]\\d{6}","\\d{9}",,,"901123456"],[,,"70\\d{7}","\\d{9}",,,"701234567"],[,,"NA","NA"],"ES",34,"00",,,,,,,,[[,"([5-9]\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["[568]|[79][0-8]"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"51\\d{7}","\\d{9}",,,"511234567"],,,[,,"NA","NA"]],ET:[,[,,"[1-59]\\d{8}","\\d{7,9}"],[,,"(?:11(?:1(?:1[124]|2[2-57]|3[1-5]|5[5-8]|8[6-8])|2(?:13|3[6-8]|5[89]|7[05-9]|8[2-6])|3(?:2[01]|3[0-289]|4[1289]|7[1-4]|87)|4(?:1[69]|3[2-49]|4[0-3]|6[5-8])|5(?:1[57]|44|5[0-4])|6(?:18|2[69]|4[5-7]|5[1-5]|6[0-59]|8[015-8]))|2(?:2(?:11[1-9]|22[0-7]|33\\d|44[1467]|66[1-68])|5(?:11[124-6]|33[2-8]|44[1467]|55[14]|66[1-3679]|77[124-79]|880))|3(?:3(?:11[0-46-8]|22[0-6]|33[0134689]|44[04]|55[0-6]|66[01467])|4(?:44[0-8]|55[0-69]|66[0-3]|77[1-5]))|4(?:6(?:22[0-24-7]|33[1-5]|44[13-69]|55[14-689]|660|88[1-4])|7(?:11[1-9]|22[1-9]|33[13-7]|44[13-6]|55[1-689]))|5(?:7(?:227|55[05]|(?:66|77)[14-8])|8(?:11[149]|22[013-79]|33[0-68]|44[013-8]|550|66[1-5]|77\\d)))\\d{4}",
"\\d{7,9}",,,"111112345"],[,,"9(?:[1-3]\\d|5[89])\\d{6}","\\d{9}",,,"911234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"ET",251,"00","0",,,"0",,,,[[,"([1-59]\\d)(\\d{3})(\\d{4})","$1 $2 $3",,"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],FI:[,[,,"1\\d{4,11}|[2-9]\\d{4,10}","\\d{5,12}"],[,,"1(?:[3569][1-8]\\d{3,9}|[47]\\d{5,10})|2[1-8]\\d{3,9}|3(?:[1-8]\\d{3,9}|9\\d{4,8})|[5689][1-8]\\d{3,9}","\\d{5,12}",,,"1312345678"],[,,"4\\d{5,10}|50\\d{4,8}",
"\\d{6,11}",,,"412345678"],[,,"800\\d{4,7}","\\d{7,10}",,,"8001234567"],[,,"[67]00\\d{5,6}","\\d{8,9}",,,"600123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"FI",358,"00|99[049]","0",,,"0",,,,[[,"(\\d{3})(\\d{3,7})","$1 $2",["(?:[1-3]00|[6-8]0)"],"0$1","",0],[,"(\\d{2})(\\d{4,10})","$1 $2",["[14]|2[09]|50|7[135]"],"0$1","",0],[,"(\\d)(\\d{4,11})","$1 $2",["[25689][1-8]|3"],"0$1","",0]],,[,,"NA","NA"],1,,[,,"[13]00\\d{3,7}|2(?:0(?:0\\d{3,7}|2[023]\\d{1,6}|9[89]\\d{1,6}))|60(?:[12]\\d{5,6}|6\\d{7})|7(?:1\\d{7}|3\\d{8}|5[03-9]\\d{2,7})",
"\\d{5,10}",,,"100123"],[,,"[13]0\\d{4,8}|2(?:0(?:[016-8]\\d{3,7}|[2-59]\\d{2,7})|9\\d{4,8})|60(?:[12]\\d{5,6}|6\\d{7})|7(?:1\\d{7}|3\\d{8}|5[03-9]\\d{2,7})","\\d{5,10}",,,"10112345"],,,[,,"NA","NA"]],FJ:[,[,,"[36-9]\\d{6}|0\\d{10}","\\d{7}(?:\\d{4})?"],[,,"(?:3[0-5]|6[25-7]|8[58])\\d{5}","\\d{7}",,,"3212345"],[,,"(?:7[0-8]|8[03467]|9\\d)\\d{5}","\\d{7}",,,"7012345"],[,,"0800\\d{7}","\\d{11}",,,"08001234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"FJ",679,"0(?:0|52)",,,,,,"00",,
[[,"(\\d{3})(\\d{4})","$1 $2",["[36-9]"],"","",0],[,"(\\d{4})(\\d{3})(\\d{4})","$1 $2 $3",["0"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],1,,[,,"NA","NA"]],FK:[,[,,"[2-7]\\d{4}","\\d{5}"],[,,"[2-47]\\d{4}","\\d{5}",,,"31234"],[,,"[56]\\d{4}","\\d{5}",,,"51234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"FK",500,"00",,,,,,,,,,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],FM:[,[,,"[39]\\d{6}","\\d{7}"],[,,"3[2357]0[1-9]\\d{3}|9[2-6]\\d{5}","\\d{7}",
,,"3201234"],[,,"3[2357]0[1-9]\\d{3}|9[2-7]\\d{5}","\\d{7}",,,"3501234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"FM",691,"00",,,,,,,,[[,"(\\d{3})(\\d{4})","$1 $2",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],FO:[,[,,"[2-9]\\d{5}","\\d{6}"],[,,"(?:20|[3-4]\\d|8[19])\\d{4}","\\d{6}",,,"201234"],[,,"(?:2[1-9]|5\\d|7[1-79])\\d{4}","\\d{6}",,,"211234"],[,,"80[257-9]\\d{3}","\\d{6}",,,"802123"],[,,"90(?:[1345][15-7]|2[125-7]|99)\\d{2}","\\d{6}",
,,"901123"],[,,"NA","NA"],[,,"NA","NA"],[,,"(?:6[0-36]|88)\\d{4}","\\d{6}",,,"601234"],"FO",298,"00",,,,"(10(?:01|[12]0|88))",,,,[[,"(\\d{6})","$1",,"","$CC $1",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],FR:[,[,,"[1-9]\\d{8}","\\d{9}"],[,,"[1-5]\\d{8}","\\d{9}",,,"123456789"],[,,"6\\d{8}|7[0-24-9]\\d{7}","\\d{9}",,,"612345678"],[,,"80\\d{7}","\\d{9}",,,"801234567"],[,,"89[1-37-9]\\d{6}","\\d{9}",,,"891123456"],[,,"8(?:1[019]|2[0156]|84|90)\\d{6}","\\d{9}",,,"810123456"],[,,
"NA","NA"],[,,"9\\d{8}","\\d{9}",,,"912345678"],"FR",33,"00","0",,,"0",,,,[[,"([1-79])(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4 $5",["[1-79]"],"0$1","",0],[,"(1\\d{2})(\\d{3})","$1 $2",["11"],"$1","",0],[,"(8\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["8"],"0 $1","",0]],[[,"([1-79])(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4 $5",["[1-79]","[1-79]"],"0$1","",0],[,"(8\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["8","8"],"0 $1","",0]],[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA",
"NA"]],GA:[,[,,"0\\d{7}","\\d{8}"],[,,"01\\d{6}","\\d{8}",,,"01441234"],[,,"0[2-7]\\d{6}","\\d{8}",,,"06031234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"GA",241,"00",,,,,,,,[[,"(0\\d)(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],1,,[,,"NA","NA"]],GB:[,[,,"\\d{7,10}","\\d{4,10}"],[,,"2(?:0[01378]|3[0189]|4[017]|8[0-46-9]|9[012])\\d{7}|1(?:(?:1(?:3[0-48]|[46][0-4]|5[012789]|7[0-49]|8[01349])|21[0-7]|31[0-8]|[459]1\\d|61[0-46-9]))\\d{6}|1(?:2(?:0[024-9]|2[3-9]|3[3-79]|4[1-689]|[58][02-9]|6[0-4789]|7[013-9]|9\\d)|3(?:0\\d|[25][02-9]|3[02-579]|[468][0-46-9]|7[1235679]|9[24578])|4(?:0[03-9]|[28][02-5789]|[37]\\d|4[02-69]|5[0-8]|[69][0-79])|5(?:0[1235-9]|2[024-9]|3[015689]|4[02-9]|5[03-9]|6\\d|7[0-35-9]|8[0-468]|9[0-5789])|6(?:0[034689]|2[0-35689]|[38][013-9]|4[1-467]|5[0-69]|6[13-9]|7[0-8]|9[0124578])|7(?:0[0246-9]|2\\d|3[023678]|4[03-9]|5[0-46-9]|6[013-9]|7[0-35-9]|8[024-9]|9[02-9])|8(?:0[35-9]|2[1-5789]|3[02-578]|4[0-578]|5[124-9]|6[2-69]|7\\d|8[02-9]|9[02569])|9(?:0[02-589]|2[02-689]|3[1-5789]|4[2-9]|5[0-579]|6[234789]|7[0124578]|8\\d|9[2-57]))\\d{6}|1(?:2(?:0(?:46[1-4]|87[2-9])|545[1-79]|76(?:2\\d|3[1-8]|6[1-6])|9(?:7(?:2[0-4]|3[2-5])|8(?:2[2-8]|7[0-4789]|8[345])))|3(?:638[2-5]|647[23]|8(?:47[04-9]|64[015789]))|4(?:044[1-7]|20(?:2[23]|8\\d)|6(?:0(?:30|5[2-57]|6[1-8]|7[2-8])|140)|8(?:052|87[123]))|5(?:24(?:3[2-79]|6\\d)|276\\d|6(?:26[06-9]|686))|6(?:06(?:4\\d|7[4-79])|295[567]|35[34]\\d|47(?:24|61)|59(?:5[08]|6[67]|74)|955[0-4])|7(?:26(?:6[13-9]|7[0-7])|442\\d|50(?:2[0-3]|[3-68]2|76))|8(?:27[56]\\d|37(?:5[2-5]|8[239])|84(?:3[2-58]))|9(?:0(?:0(?:6[1-8]|85)|52\\d)|3583|4(?:66[1-8]|9(?:2[01]|81))|63(?:23|3[1-4])|9561))\\d{3}|176888[234678]\\d{2}|16977[23]\\d{3}",
"\\d{4,10}",,,"1212345678"],[,,"7(?:[1-4]\\d\\d|5(?:0[0-8]|[13-9]\\d|2[0-35-9])|7(?:0[1-9]|[1-7]\\d|8[02-9]|9[0-689])|8(?:[014-9]\\d|[23][0-8])|9(?:[04-9]\\d|1[02-9]|2[0-35-9]|3[0-689]))\\d{6}","\\d{10}",,,"7400123456"],[,,"80(?:0(?:1111|\\d{6,7})|8\\d{7})|500\\d{6}","\\d{7}(?:\\d{2,3})?",,,"8001234567"],[,,"(?:87[123]|9(?:[01]\\d|8[2349]))\\d{7}","\\d{10}",,,"9012345678"],[,,"8(?:4(?:5464\\d|[2-5]\\d{7})|70\\d{7})","\\d{7}(?:\\d{3})?",,,"8431234567"],[,,"70\\d{8}","\\d{10}",,,"7012345678"],[,,"56\\d{8}",
"\\d{10}",,,"5612345678"],"GB",44,"00","0"," x",,"0",,,,[[,"(\\d{2})(\\d{4})(\\d{4})","$1 $2 $3",["2|5[56]|7(?:0|6[013-9])","2|5[56]|7(?:0|6(?:[013-9]|2[0-35-9]))"],"0$1","",0],[,"(\\d{3})(\\d{3})(\\d{4})","$1 $2 $3",["1(?:1|\\d1)|3|9[018]"],"0$1","",0],[,"(\\d{5})(\\d{4,5})","$1 $2",["1(?:38|5[23]|69|76|94)","1(?:387|5(?:24|39)|697|768|946)","1(?:3873|5(?:242|39[456])|697[347]|768[347]|9467)"],"0$1","",0],[,"(1\\d{3})(\\d{5,6})","$1 $2",["1"],"0$1","",0],[,"(7\\d{3})(\\d{6})","$1 $2",["7(?:[1-5789]|62)",
"7(?:[1-5789]|624)"],"0$1","",0],[,"(800)(\\d{4})","$1 $2",["800","8001","80011","800111","8001111"],"0$1","",0],[,"(845)(46)(4\\d)","$1 $2 $3",["845","8454","84546","845464"],"0$1","",0],[,"(8\\d{2})(\\d{3})(\\d{4})","$1 $2 $3",["8(?:4[2-5]|7[0-3])"],"0$1","",0],[,"(80\\d)(\\d{3})(\\d{4})","$1 $2 $3",["80"],"0$1","",0],[,"([58]00)(\\d{6})","$1 $2",["[58]00"],"0$1","",0]],,[,,"76(?:0[012]|2[356]|4[0134]|5[49]|6[0-369]|77|81|9[39])\\d{6}","\\d{10}",,,"7640123456"],1,,[,,"NA","NA"],[,,"(?:3[0347]|55)\\d{8}",
"\\d{10}",,,"5512345678"],,,[,,"NA","NA"]],GD:[,[,,"[4589]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"473(?:2(?:3[0-2]|69)|3(?:2[89]|86)|4(?:[06]8|3[5-9]|4[0-49]|5[5-79]|68|73|90)|63[68]|7(?:58|84)|938)\\d{4}","\\d{7}(?:\\d{3})?",,,"4732691234"],[,,"473(?:4(?:0[3-79]|1[04-9]|20|58)|5(?:2[01]|3[3-8]))\\d{4}","\\d{10}",,,"4734031234"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002123456"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002123456"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],
[,,"NA","NA"],"GD",1,"011","1",,,"1",,,,,,[,,"NA","NA"],,"473",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],GE:[,[,,"[34578]\\d{8}","\\d{6,9}"],[,,"(?:3(?:[256]\\d|4[124-9]|7[0-4])|4(?:1\\d|2[2-7]|3[1-79]|4[2-8]|7[239]|9[1-7]))\\d{6}","\\d{6,9}",,,"322123456"],[,,"5(?:14|5[01578]|68|7[0147-9]|9[0-35-9])\\d{6}","\\d{9}",,,"555123456"],[,,"800\\d{6}","\\d{9}",,,"800123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"706\\d{6}","\\d{9}",,,"706123456"],"GE",995,"00","0",,,"0",,,,[[,"(\\d{3})(\\d{2})(\\d{2})(\\d{2})",
"$1 $2 $3 $4",["[348]"],"0$1","",0],[,"(\\d{3})(\\d{3})(\\d{3})","$1 $2 $3",["7"],"0$1","",0],[,"(\\d{3})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["5"],"$1","",0]],,[,,"NA","NA"],,,[,,"706\\d{6}","\\d{9}",,,"706123456"],[,,"NA","NA"],,,[,,"NA","NA"]],GF:[,[,,"[56]\\d{8}","\\d{9}"],[,,"594(?:10|2[012457-9]|3[0-57-9]|4[3-9]|5[7-9]|6[0-3]|9[014])\\d{4}","\\d{9}",,,"594101234"],[,,"694(?:[04][0-7]|1[0-5]|3[018]|[29]\\d)\\d{4}","\\d{9}",,,"694201234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],
[,,"NA","NA"],"GF",594,"00","0",,,"0",,,,[[,"(\\d{3})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",,"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],GG:[,[,,"[135789]\\d{6,9}","\\d{6,10}"],[,,"1481\\d{6}","\\d{6,10}",,,"1481456789"],[,,"7(?:781|839|911)\\d{6}","\\d{10}",,,"7781123456"],[,,"80(?:0(?:1111|\\d{6,7})|8\\d{7})|500\\d{6}","\\d{7}(?:\\d{2,3})?",,,"8001234567"],[,,"(?:87[123]|9(?:[01]\\d|8[0-3]))\\d{7}","\\d{10}",,,"9012345678"],[,,"8(?:4(?:5464\\d|[2-5]\\d{7})|70\\d{7})",
"\\d{7}(?:\\d{3})?",,,"8431234567"],[,,"70\\d{8}","\\d{10}",,,"7012345678"],[,,"56\\d{8}","\\d{10}",,,"5612345678"],"GG",44,"00","0"," x",,"0",,,,,,[,,"76(?:0[012]|2[356]|4[0134]|5[49]|6[0-369]|77|81|9[39])\\d{6}","\\d{10}",,,"7640123456"],,,[,,"NA","NA"],[,,"(?:3[0347]|55)\\d{8}","\\d{10}",,,"5512345678"],,,[,,"NA","NA"]],GH:[,[,,"[235]\\d{8}|8\\d{7}","\\d{7,9}"],[,,"3(?:0[237]\\d|[167](?:2[0-6]|7\\d)|2(?:2[0-5]|7\\d)|3(?:2[0-3]|7\\d)|4(?:2[013-9]|3[01]|7\\d)|5(?:2[0-7]|7\\d)|8(?:2[0-2]|7\\d)|9(?:20|7\\d))\\d{5}",
"\\d{7,9}",,,"302345678"],[,,"(?:2[034678]|5[047])\\d{7}","\\d{9}",,,"231234567"],[,,"800\\d{5}","\\d{8}",,,"80012345"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"GH",233,"00","0",,,"0",,,,[[,"(\\d{2})(\\d{3})(\\d{4})","$1 $2 $3",["[235]"],"0$1","",0],[,"(\\d{3})(\\d{5})","$1 $2",["8"],"0$1","",0]],,[,,"NA","NA"],,,[,,"800\\d{5}","\\d{8}",,,"80012345"],[,,"NA","NA"],,,[,,"NA","NA"]],GI:[,[,,"[2568]\\d{7}","\\d{8}"],[,,"2(?:00\\d|1(?:6[24-7]|9\\d)|2(?:00|2[2457]))\\d{4}","\\d{8}",,,"20012345"],
[,,"(?:5[46-8]|62)\\d{6}","\\d{8}",,,"57123456"],[,,"80\\d{6}","\\d{8}",,,"80123456"],[,,"8[1-689]\\d{6}","\\d{8}",,,"88123456"],[,,"87\\d{6}","\\d{8}",,,"87123456"],[,,"NA","NA"],[,,"NA","NA"],"GI",350,"00",,,,,,,,[[,"(\\d{3})(\\d{5})","$1 $2",["2"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],GL:[,[,,"[1-689]\\d{5}","\\d{6}"],[,,"(?:19|3[1-6]|6[14689]|8[14-79]|9\\d)\\d{4}","\\d{6}",,,"321000"],[,,"[245][2-9]\\d{4}","\\d{6}",,,"221234"],[,,"80\\d{4}","\\d{6}",,,"801234"],
[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"3[89]\\d{4}","\\d{6}",,,"381234"],"GL",299,"00",,,,,,,,[[,"(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],GM:[,[,,"[2-9]\\d{6}","\\d{7}"],[,,"(?:4(?:[23]\\d{2}|4(?:1[024679]|[6-9]\\d))|5(?:54[0-7]|6(?:[67]\\d)|7(?:1[04]|2[035]|3[58]|48))|8\\d{3})\\d{3}","\\d{7}",,,"5661234"],[,,"(?:2[0-6]|[3679]\\d)\\d{5}","\\d{7}",,,"3012345"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA",
"NA"],"GM",220,"00",,,,,,,,[[,"(\\d{3})(\\d{4})","$1 $2",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],GN:[,[,,"[367]\\d{7,8}","\\d{8,9}"],[,,"30(?:24|3[12]|4[1-35-7]|5[13]|6[189]|[78]1|9[1478])\\d{4}","\\d{8}",,,"30241234"],[,,"6[02356]\\d{7}","\\d{9}",,,"601123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"722\\d{6}","\\d{9}",,,"722123456"],"GN",224,"00",,,,,,,,[[,"(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["3"],"","",0],[,"(\\d{3})(\\d{2})(\\d{2})(\\d{2})",
"$1 $2 $3 $4",["[67]"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],GP:[,[,,"[56]\\d{8}","\\d{9}"],[,,"590(?:0[13468]|1[012]|2[0-68]|3[28]|4[0-8]|5[579]|6[0189]|70|8[0-689]|9\\d)\\d{4}","\\d{9}",,,"590201234"],[,,"690(?:0[0-7]|[1-9]\\d)\\d{4}","\\d{9}",,,"690301234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"GP",590,"00","0",,,"0",,,,[[,"([56]90)(\\d{2})(\\d{4})","$1 $2-$3",,"0$1","",0]],,[,,"NA","NA"],1,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA",
"NA"]],GQ:[,[,,"[23589]\\d{8}","\\d{9}"],[,,"3(?:3(?:3\\d[7-9]|[0-24-9]\\d[46])|5\\d{2}[7-9])\\d{4}","\\d{9}",,,"333091234"],[,,"(?:222|551)\\d{6}","\\d{9}",,,"222123456"],[,,"80\\d[1-9]\\d{5}","\\d{9}",,,"800123456"],[,,"90\\d[1-9]\\d{5}","\\d{9}",,,"900123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"GQ",240,"00",,,,,,,,[[,"(\\d{3})(\\d{3})(\\d{3})","$1 $2 $3",["[235]"],"","",0],[,"(\\d{3})(\\d{6})","$1 $2",["[89]"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],GR:[,
[,,"[26-9]\\d{9}","\\d{10}"],[,,"2(?:1\\d{2}|2(?:2[1-46-9]|3[1-8]|4[1-7]|5[1-4]|6[1-8]|7[1-5]|[89][1-9])|3(?:1\\d|2[1-57]|[35][1-3]|4[13]|7[1-7]|8[124-6]|9[1-79])|4(?:1\\d|2[1-8]|3[1-4]|4[13-5]|6[1-578]|9[1-5])|5(?:1\\d|[29][1-4]|3[1-5]|4[124]|5[1-6])|6(?:1\\d|3[1245]|4[1-7]|5[13-9]|[269][1-6]|7[14]|8[1-5])|7(?:1\\d|2[1-5]|3[1-6]|4[1-7]|5[1-57]|6[135]|9[125-7])|8(?:1\\d|2[1-5]|[34][1-4]|9[1-57]))\\d{6}","\\d{10}",,,"2123456789"],[,,"69\\d{8}","\\d{10}",,,"6912345678"],[,,"800\\d{7}","\\d{10}",,,"8001234567"],
[,,"90[19]\\d{7}","\\d{10}",,,"9091234567"],[,,"8(?:0[16]|12|25)\\d{7}","\\d{10}",,,"8011234567"],[,,"70\\d{8}","\\d{10}",,,"7012345678"],[,,"NA","NA"],"GR",30,"00",,,,,,,,[[,"([27]\\d)(\\d{4})(\\d{4})","$1 $2 $3",["21|7"],"","",0],[,"(\\d{3})(\\d{3})(\\d{4})","$1 $2 $3",["2[2-9]1|[689]"],"","",0],[,"(2\\d{3})(\\d{6})","$1 $2",["2[2-9][02-9]"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],GT:[,[,,"[2-7]\\d{7}|1[89]\\d{9}","\\d{8}(?:\\d{3})?"],[,,"[267][2-9]\\d{6}","\\d{8}",
,,"22456789"],[,,"[345]\\d{7}","\\d{8}",,,"51234567"],[,,"18[01]\\d{8}","\\d{11}",,,"18001112222"],[,,"19\\d{9}","\\d{11}",,,"19001112222"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"GT",502,"00",,,,,,,,[[,"(\\d{4})(\\d{4})","$1 $2",["[2-7]"],"","",0],[,"(\\d{4})(\\d{3})(\\d{4})","$1 $2 $3",["1"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],GU:[,[,,"[5689]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"671(?:3(?:00|3[39]|4[349]|55|6[26])|4(?:56|7[1-9]|8[236-9])|5(?:55|6[2-5]|88)|6(?:3[2-578]|4[24-9]|5[34]|78|8[5-9])|7(?:[079]7|2[0167]|3[45]|8[789])|8(?:[2-5789]8|6[48])|9(?:2[29]|6[79]|7[179]|8[789]|9[78]))\\d{4}",
"\\d{7}(?:\\d{3})?",,,"6713001234"],[,,"671(?:3(?:00|3[39]|4[349]|55|6[26])|4(?:56|7[1-9]|8[236-9])|5(?:55|6[2-5]|88)|6(?:3[2-578]|4[24-9]|5[34]|78|8[5-9])|7(?:[079]7|2[0167]|3[45]|8[789])|8(?:[2-5789]8|6[48])|9(?:2[29]|6[79]|7[179]|8[789]|9[78]))\\d{4}","\\d{7}(?:\\d{3})?",,,"6713001234"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002123456"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002123456"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"GU",1,"011","1",
,,"1",,,1,,,[,,"NA","NA"],,"671",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],GW:[,[,,"[3-79]\\d{6}","\\d{7}"],[,,"3(?:2[0125]|3[1245]|4[12]|5[1-4]|70|9[1-467])\\d{4}","\\d{7}",,,"3201234"],[,,"(?:[5-7]\\d|9[012])\\d{5}","\\d{7}",,,"5012345"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"40\\d{5}","\\d{7}",,,"4012345"],"GW",245,"00",,,,,,,,[[,"(\\d{3})(\\d{4})","$1 $2",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],GY:[,[,,"[2-4679]\\d{6}","\\d{7}"],[,,"(?:2(?:1[6-9]|2[0-35-9]|3[1-4]|5[3-9]|6\\d|7[0-24-79])|3(?:2[25-9]|3\\d)|4(?:4[0-24]|5[56])|77[1-57])\\d{4}",
"\\d{7}",,,"2201234"],[,,"6\\d{6}","\\d{7}",,,"6091234"],[,,"(?:289|862)\\d{4}","\\d{7}",,,"2891234"],[,,"9008\\d{3}","\\d{7}",,,"9008123"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"GY",592,"001",,,,,,,,[[,"(\\d{3})(\\d{4})","$1 $2",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],HK:[,[,,"[235-7]\\d{7}|8\\d{7,8}|9\\d{4,10}","\\d{5,11}"],[,,"(?:[23]\\d|5[78])\\d{6}","\\d{8}",,,"21234567"],[,,"(?:5[1-69]\\d|6\\d{2}|9(?:0[1-9]|[1-8]\\d))\\d{5}","\\d{8}",,,"51234567"],[,,"800\\d{6}",
"\\d{9}",,,"800123456"],[,,"900(?:[0-24-9]\\d{7}|3\\d{1,4})","\\d{5,11}",,,"90012345678"],[,,"NA","NA"],[,,"8[1-3]\\d{6}","\\d{8}",,,"81123456"],[,,"NA","NA"],"HK",852,"00",,,,,,,,[[,"(\\d{4})(\\d{4})","$1 $2",["[235-7]|[89](?:0[1-9]|[1-9])"],"","",0],[,"(800)(\\d{3})(\\d{3})","$1 $2 $3",["800"],"","",0],[,"(900)(\\d{2})(\\d{3})(\\d{3})","$1 $2 $3 $4",["900"],"","",0],[,"(900)(\\d{2,5})","$1 $2",["900"],"","",0]],,[,,"7\\d{7}","\\d{8}",,,"71234567"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],
HN:[,[,,"[237-9]\\d{7}","\\d{8}"],[,,"2(?:2(?:0[019]|1[1-36]|[23]\\d|4[056]|5[57]|7[01389]|8[0146-9]|9[012])|4(?:2[3-59]|3[13-689]|4[0-68]|5[1-35])|5(?:4[3-5]|5\\d|6[56]|74)|6(?:[056]\\d|4[0-378]|[78][0-8]|9[01])|7(?:6[46-9]|7[02-9]|8[34])|8(?:79|8[0-35789]|9[1-57-9]))\\d{4}","\\d{8}",,,"22123456"],[,,"[37-9]\\d{7}","\\d{8}",,,"91234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"HN",504,"00",,,,,,,,[[,"(\\d{4})(\\d{4})","$1-$2",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],
[,,"NA","NA"],,,[,,"NA","NA"]],HR:[,[,,"[1-7]\\d{5,8}|[89]\\d{6,11}","\\d{6,12}"],[,,"1\\d{7}|(?:2[0-3]|3[1-5]|4[02-47-9]|5[1-3])\\d{6}","\\d{6,8}",,,"12345678"],[,,"9[1257-9]\\d{6,10}","\\d{8,12}",,,"912345678"],[,,"80[01]\\d{4,7}","\\d{7,10}",,,"8001234567"],[,,"6(?:[09]\\d{7}|[145]\\d{4,7})","\\d{6,9}",,,"611234"],[,,"NA","NA"],[,,"7[45]\\d{4,7}","\\d{6,9}",,,"741234567"],[,,"NA","NA"],"HR",385,"00","0",,,"0",,,,[[,"(1)(\\d{4})(\\d{3})","$1 $2 $3",["1"],"0$1","",0],[,"(6[09])(\\d{4})(\\d{3})",
"$1 $2 $3",["6[09]"],"0$1","",0],[,"(62)(\\d{3})(\\d{3,4})","$1 $2 $3",["62"],"0$1","",0],[,"([2-5]\\d)(\\d{3})(\\d{3})","$1 $2 $3",["[2-5]"],"0$1","",0],[,"(9\\d)(\\d{3})(\\d{3,4})","$1 $2 $3",["9"],"0$1","",0],[,"(9\\d)(\\d{4})(\\d{4})","$1 $2 $3",["9"],"0$1","",0],[,"(9\\d)(\\d{3,4})(\\d{3})(\\d{3})","$1 $2 $3 $4",["9"],"0$1","",0],[,"(\\d{2})(\\d{2})(\\d{2,3})","$1 $2 $3",["6[145]|7"],"0$1","",0],[,"(\\d{2})(\\d{3,4})(\\d{3})","$1 $2 $3",["6[145]|7"],"0$1","",0],[,"(80[01])(\\d{2})(\\d{2,3})",
"$1 $2 $3",["8"],"0$1","",0],[,"(80[01])(\\d{3,4})(\\d{3})","$1 $2 $3",["8"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"62\\d{6,7}","\\d{8,9}",,,"62123456"],,,[,,"NA","NA"]],HT:[,[,,"[2-489]\\d{7}","\\d{8}"],[,,"2(?:[24]\\d|5[1-5]|94)\\d{5}","\\d{8}",,,"22453300"],[,,"(?:3[1-9]|4\\d)\\d{6}","\\d{8}",,,"34101234"],[,,"8\\d{7}","\\d{8}",,,"80012345"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"98[89]\\d{5}","\\d{8}",,,"98901234"],"HT",509,"00",,,,,,,,[[,"(\\d{2})(\\d{2})(\\d{4})","$1 $2 $3",
,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],HU:[,[,,"[1-9]\\d{7,8}","\\d{6,9}"],[,,"(?:1\\d|2(?:1\\d|[2-9])|3[2-7]|4[24-9]|5[2-79]|6[23689]|7(?:1\\d|[2-9])|8[2-57-9]|9[2-69])\\d{6}","\\d{6,9}",,,"12345678"],[,,"(?:[27]0|3[01])\\d{7}","\\d{9}",,,"201234567"],[,,"80\\d{6}","\\d{8}",,,"80123456"],[,,"9[01]\\d{6}","\\d{8}",,,"90123456"],[,,"40\\d{6}","\\d{8}",,,"40123456"],[,,"NA","NA"],[,,"NA","NA"],"HU",36,"00","06",,,"06",,,,[[,"(1)(\\d{3})(\\d{4})","$1 $2 $3",["1"],"($1)",
"",0],[,"(\\d{2})(\\d{3})(\\d{3,4})","$1 $2 $3",["[2-9]"],"($1)","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],ID:[,[,,"[1-9]\\d{6,10}","\\d{5,11}"],[,,"2(?:1(?:[0-8]\\d{6,7}|9\\d{6})|[24]\\d{7,8})|(?:2(?:[35][1-4]|6[0-8]|7[1-6]|8\\d|9[1-8])|3(?:1|2[1-578]|3[1-68]|4[1-3]|5[1-8]|6[1-3568]|7[0-46]|8\\d)|4(?:0[1-589]|1[01347-9]|2[0-36-8]|3[0-24-68]|5[1-378]|6[1-5]|7[134]|8[1245])|5(?:1[1-35-9]|2[25-8]|3[1246-9]|4[1-3589]|5[1-46]|6[1-8])|6(?:19?|[25]\\d|3[1-469]|4[1-6])|7(?:1[1-46-9]|2[14-9]|[36]\\d|4[1-8]|5[1-9]|7[0-36-9])|9(?:0[12]|1[013-8]|2[0-479]|5[125-8]|6[23679]|7[159]|8[01346]))\\d{5,8}",
"\\d{5,10}",,,"612345678"],[,,"(?:2(?:1(?:3[145]|4[01]|5[1-469]|60|8[0359]|9\\d)|2(?:88|9[1256])|3[1-4]9|4(?:36|91)|5(?:1[349]|[2-4]9)|6[0-7]9|7(?:[1-36]9|4[39])|8[1-5]9|9[1-48]9)|3(?:19[1-3]|2[12]9|3[13]9|4(?:1[69]|39)|5[14]9|6(?:1[69]|2[89])|709)|4[13]19|5(?:1(?:19|8[39])|4[129]9|6[12]9)|6(?:19[12]|2(?:[23]9|77))|7(?:1[13]9|2[15]9|419|5(?:1[89]|29)|6[15]9|7[178]9))\\d{5,6}|8[1-35-9]\\d{7,9}","\\d{9,11}",,,"812345678"],[,,"177\\d{6,8}|800\\d{5,7}","\\d{8,11}",,,"8001234567"],[,,"809\\d{7}","\\d{10}",
,,"8091234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"ID",62,"0(?:0[1789]|10(?:00|1[67]))","0",,,"0",,,,[[,"(\\d{2})(\\d{7,8})","$1 $2",["2[124]|[36]1"],"(0$1)","",0],[,"(\\d{3})(\\d{5,7})","$1 $2",["[4579]|2[035-9]|[36][02-9]"],"(0$1)","",0],[,"(8\\d{2})(\\d{3,4})(\\d{3,4})","$1-$2-$3",["8[1-35-9]"],"0$1","",0],[,"(177)(\\d{6,8})","$1 $2",["1"],"0$1","",0],[,"(800)(\\d{5,7})","$1 $2",["800"],"0$1","",0],[,"(809)(\\d)(\\d{3})(\\d{3})","$1 $2 $3 $4",["809"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA",
"NA"],[,,"NA","NA"],,,[,,"NA","NA"]],IE:[,[,,"[124-9]\\d{6,9}","\\d{5,10}"],[,,"1\\d{7,8}|2(?:1\\d{6,7}|3\\d{7}|[24-9]\\d{5})|4(?:0[24]\\d{5}|[1-469]\\d{7}|5\\d{6}|7\\d{5}|8[0-46-9]\\d{7})|5(?:0[45]\\d{5}|1\\d{6}|[23679]\\d{7}|8\\d{5})|6(?:1\\d{6}|[237-9]\\d{5}|[4-6]\\d{7})|7[14]\\d{7}|9(?:1\\d{6}|[04]\\d{7}|[35-9]\\d{5})","\\d{5,10}",,,"2212345"],[,,"8(?:22\\d{6}|[35-9]\\d{7})","\\d{9}",,,"850123456"],[,,"1800\\d{6}","\\d{10}",,,"1800123456"],[,,"15(?:1[2-8]|[2-8]0|9[089])\\d{6}","\\d{10}",,,"1520123456"],
[,,"18[59]0\\d{6}","\\d{10}",,,"1850123456"],[,,"700\\d{6}","\\d{9}",,,"700123456"],[,,"76\\d{7}","\\d{9}",,,"761234567"],"IE",353,"00","0",,,"0",,,,[[,"(1)(\\d{3,4})(\\d{4})","$1 $2 $3",["1"],"(0$1)","",0],[,"(\\d{2})(\\d{5})","$1 $2",["2[24-9]|47|58|6[237-9]|9[35-9]"],"(0$1)","",0],[,"(\\d{3})(\\d{5})","$1 $2",["40[24]|50[45]"],"(0$1)","",0],[,"(48)(\\d{4})(\\d{4})","$1 $2 $3",["48"],"(0$1)","",0],[,"(818)(\\d{3})(\\d{3})","$1 $2 $3",["81"],"(0$1)","",0],[,"(\\d{2})(\\d{3})(\\d{3,4})","$1 $2 $3",
["[24-69]|7[14]"],"(0$1)","",0],[,"([78]\\d)(\\d{3,4})(\\d{4})","$1 $2 $3",["76|8[35-9]"],"0$1","",0],[,"(700)(\\d{3})(\\d{3})","$1 $2 $3",["70"],"0$1","",0],[,"(\\d{4})(\\d{3})(\\d{3})","$1 $2 $3",["1(?:8[059]|5)","1(?:8[059]0|5)"],"$1","",0]],,[,,"NA","NA"],,,[,,"18[59]0\\d{6}","\\d{10}",,,"1850123456"],[,,"818\\d{6}","\\d{9}",,,"818123456"],,,[,,"8[35-9]\\d{8}","\\d{10}",,,"8501234567"]],IL:[,[,,"[17]\\d{6,9}|[2-589]\\d{3}(?:\\d{3,6})?|6\\d{3}","\\d{4,10}"],[,,"[2-489]\\d{7}","\\d{7,8}",,,"21234567"],
[,,"5(?:[02347-9]\\d{2}|5(?:2[23]|3[34]|4[45]|5[5689]|6[67]|7[78]|8[89])|6[2-9]\\d)\\d{5}","\\d{9}",,,"501234567"],[,,"1(?:80[019]\\d{3}|255)\\d{3}","\\d{7,10}",,,"1800123456"],[,,"1(?:212|(?:9(?:0[01]|19)|200)\\d{2})\\d{4}","\\d{8,10}",,,"1919123456"],[,,"1700\\d{6}","\\d{10}",,,"1700123456"],[,,"NA","NA"],[,,"7(?:2[23]\\d|3[237]\\d|47\\d|6(?:5\\d|8[08])|7\\d{2}|8(?:33|55|77|81))\\d{5}","\\d{9}",,,"771234567"],"IL",972,"0(?:0|1[2-9])","0",,,"0",,,,[[,"([2-489])(\\d{3})(\\d{4})","$1-$2-$3",["[2-489]"],
"0$1","",0],[,"([57]\\d)(\\d{3})(\\d{4})","$1-$2-$3",["[57]"],"0$1","",0],[,"(1)([7-9]\\d{2})(\\d{3})(\\d{3})","$1-$2-$3-$4",["1[7-9]"],"$1","",0],[,"(1255)(\\d{3})","$1-$2",["125"],"$1","",0],[,"(1200)(\\d{3})(\\d{3})","$1-$2-$3",["120"],"$1","",0],[,"(1212)(\\d{2})(\\d{2})","$1-$2-$3",["121"],"$1","",0],[,"(1599)(\\d{6})","$1-$2",["15"],"$1","",0],[,"(\\d{4})","*$1",["[2-689]"],"$1","",0]],,[,,"NA","NA"],,,[,,"1700\\d{6}|[2-689]\\d{3}","\\d{4,10}",,,"1700123456"],[,,"[2-689]\\d{3}|1599\\d{6}","\\d{4}(?:\\d{6})?",
,,"1599123456"],,,[,,"NA","NA"]],IM:[,[,,"[135789]\\d{6,9}","\\d{6,10}"],[,,"1624\\d{6}","\\d{6,10}",,,"1624456789"],[,,"7[569]24\\d{6}","\\d{10}",,,"7924123456"],[,,"808162\\d{4}","\\d{10}",,,"8081624567"],[,,"(?:872299|90[0167]624)\\d{4}","\\d{10}",,,"9016247890"],[,,"8(?:4(?:40[49]06|5624\\d)|70624\\d)\\d{3}","\\d{10}",,,"8456247890"],[,,"70\\d{8}","\\d{10}",,,"7012345678"],[,,"56\\d{8}","\\d{10}",,,"5612345678"],"IM",44,"00","0"," x",,"0",,,,,,[,,"NA","NA"],,,[,,"NA","NA"],[,,"3(?:08162\\d|3\\d{5}|4(?:40[49]06|5624\\d)|7(?:0624\\d|2299\\d))\\d{3}|55\\d{8}",
"\\d{10}",,,"5512345678"],,,[,,"NA","NA"]],IN:[,[,,"1\\d{7,12}|[2-9]\\d{9,10}","\\d{6,13}"],[,,"(?:11|2[02]|33|4[04]|79)[2-7]\\d{7}|80[2-467]\\d{7}|(?:1(?:2[0-249]|3[0-25]|4[145]|[59][14]|6[014]|7[1257]|8[01346])|2(?:1[257]|3[013]|4[01]|5[0137]|6[0158]|78|8[1568]|9[14])|3(?:26|4[1-3]|5[34]|6[01489]|7[02-46]|8[159])|4(?:1[36]|2[1-47]|3[15]|5[12]|6[126-9]|7[0-24-9]|8[013-57]|9[014-7])|5(?:[136][25]|22|4[28]|5[12]|[78]1|9[15])|6(?:12|[2345]1|57|6[13]|7[14]|80)|7(?:12|2[14]|3[134]|4[47]|5[15]|[67]1|88)|8(?:16|2[014]|3[126]|6[136]|7[078]|8[34]|91))[2-7]\\d{6}|(?:(?:1(?:2[35-8]|3[346-9]|4[236-9]|[59][0235-9]|6[235-9]|7[34689]|8[257-9])|2(?:1[134689]|3[24-8]|4[2-8]|5[25689]|6[2-4679]|7[13-79]|8[2-479]|9[235-9])|3(?:01|1[79]|2[1-5]|4[25-8]|5[125689]|6[235-7]|7[157-9]|8[2-467])|4(?:1[14578]|2[5689]|3[2-467]|5[4-7]|6[35]|73|8[2689]|9[2389])|5(?:[16][146-9]|2[14-8]|3[1346]|4[14-69]|5[46]|7[2-4]|8[2-8]|9[246])|6(?:1[1358]|2[2457]|3[2-4]|4[235-7]|[57][2-689]|6[24-58]|8[1-6])|8(?:1[1357-9]|2[235-8]|3[03-57-9]|4[0-24-9]|5\\d|6[2457-9]|7[1-6]|8[1256]|9[2-4]))\\d|7(?:(?:1[013-9]|2[0235-9]|3[2679]|4[1-35689]|5[2-46-9]|[67][02-9]|9\\d)\\d|8(?:2[0-6]|[013-8]\\d)))[2-7]\\d{5}",
"\\d{6,10}",,,"1123456789"],[,,"(?:7(?:2(?:0[04-9]|5[09]|7[5-8]|9[389])|3(?:0[1-9]|[58]\\d|7[3679]|9[689])|4(?:0[1-9]|1[15-9]|[29][89]|39|8[389])|5(?:[034678]\\d|2[03-9]|5[017-9]|9[7-9])|6(?:0[0127]|1[0-257-9]|2[0-4]|3[19]|5[4589]|[6-9]\\d)|7(?:0[2-9]|[1-79]\\d|8[1-9])|8(?:[0-7]\\d|9[013-9]))|8(?:0(?:[01589]\\d|6[67])|1(?:[02-589]\\d|1[0135-9]|7[0-79])|2(?:[236-9]\\d|5[1-9])|3(?:[0357-9]\\d|4[1-9])|[45]\\d{2}|6[02457-9]\\d|7[1-69]\\d|8(?:[0-26-9]\\d|44|5[2-9])|9(?:[035-9]\\d|2[2-9]|4[0-8]))|9\\d{3})\\d{6}",
"\\d{10}",,,"9123456789"],[,,"1(?:600\\d{6}|80(?:0\\d{4,8}|3\\d{9}))","\\d{8,13}",,,"1800123456"],[,,"186[12]\\d{9}","\\d{13}",,,"1861123456789"],[,,"1860\\d{7}","\\d{11}",,,"18603451234"],[,,"NA","NA"],[,,"NA","NA"],"IN",91,"00","0",,,"0",,,,[[,"(\\d{2})(\\d{2})(\\d{6})","$1 $2 $3",["7(?:2[0579]|3[057-9]|4[0-389]|6[0-35-9]|[57]|8[0-79])|8(?:0[015689]|1[0-57-9]|2[2356-9]|3[0-57-9]|[45]|6[02457-9]|7[1-69]|8[0124-9]|9[02-9])|9","7(?:2(?:0[04-9]|5[09]|7[5-8]|9[389])|3(?:0[1-9]|[58]|7[3679]|9[689])|4(?:0[1-9]|1[15-9]|[29][89]|39|8[389])|5(?:[034678]|2[03-9]|5[017-9]|9[7-9])|6(?:0[0-27]|1[0-257-9]|2[0-4]|3[19]|5[4589]|[6-9])|7(?:0[2-9]|[1-79]|8[1-9])|8(?:[0-7]|9[013-9]))|8(?:0(?:[01589]|6[67])|1(?:[02-589]|1[0135-9]|7[0-79])|2(?:[236-9]|5[1-9])|3(?:[0357-9]|4[1-9])|[45]|6[02457-9]|7[1-69]|8(?:[0-26-9]|44|5[2-9])|9(?:[035-9]|2[2-9]|4[0-8]))|9"],
"0$1","",1],[,"(\\d{2})(\\d{4})(\\d{4})","$1 $2 $3",["11|2[02]|33|4[04]|79|80[2-46]"],"0$1","",1],[,"(\\d{3})(\\d{3})(\\d{4})","$1 $2 $3",["1(?:2[0-249]|3[0-25]|4[145]|[569][14]|7[1257]|8[1346]|[68][1-9])|2(?:1[257]|3[013]|4[01]|5[0137]|6[0158]|78|8[1568]|9[14])|3(?:26|4[1-3]|5[34]|6[01489]|7[02-46]|8[159])|4(?:1[36]|2[1-47]|3[15]|5[12]|6[126-9]|7[0-24-9]|8[013-57]|9[014-7])|5(?:[136][25]|22|4[28]|5[12]|[78]1|9[15])|6(?:12|[2345]1|57|6[13]|7[14]|80)"],"0$1","",1],[,"(\\d{3})(\\d{3})(\\d{4})","$1 $2 $3",
["7(?:12|2[14]|3[134]|4[47]|5[15]|[67]1|88)","7(?:12|2[14]|3[134]|4[47]|5(?:1|5[2-6])|[67]1|88)"],"0$1","",1],[,"(\\d{3})(\\d{3})(\\d{4})","$1 $2 $3",["8(?:16|2[014]|3[126]|6[136]|7[078]|8[34]|91)"],"0$1","",1],[,"(\\d{4})(\\d{3})(\\d{3})","$1 $2 $3",["1(?:[23579]|[468][1-9])|[2-8]"],"0$1","",1],[,"(1600)(\\d{2})(\\d{4})","$1 $2 $3",["160","1600"],"$1","",1],[,"(1800)(\\d{4,5})","$1 $2",["180","1800"],"$1","",1],[,"(18[06]0)(\\d{2,4})(\\d{4})","$1 $2 $3",["18[06]","18[06]0"],"$1","",1],[,"(140)(\\d{3})(\\d{4})",
"$1 $2 $3",["140"],"$1","",1],[,"(\\d{4})(\\d{3})(\\d{4})(\\d{2})","$1 $2 $3 $4",["18[06]","18(?:03|6[12])"],"$1","",1]],,[,,"NA","NA"],,,[,,"1(?:600\\d{6}|8(?:0(?:0\\d{4,8}|3\\d{9})|6(?:0\\d{7}|[12]\\d{9})))","\\d{8,13}",,,"1800123456"],[,,"140\\d{7}","\\d{10}",,,"1409305260"],,,[,,"NA","NA"]],IO:[,[,,"3\\d{6}","\\d{7}"],[,,"37\\d{5}","\\d{7}",,,"3709100"],[,,"38\\d{5}","\\d{7}",,,"3801234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"IO",246,"00",,,,,,,,[[,"(\\d{3})(\\d{4})",
"$1 $2",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],IQ:[,[,,"[1-7]\\d{7,9}","\\d{6,10}"],[,,"1\\d{7}|(?:2[13-5]|3[02367]|4[023]|5[03]|6[026])\\d{6,7}","\\d{6,9}",,,"12345678"],[,,"7[3-9]\\d{8}","\\d{10}",,,"7912345678"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"IQ",964,"00","0",,,"0",,,,[[,"(1)(\\d{3})(\\d{4})","$1 $2 $3",["1"],"0$1","",0],[,"([2-6]\\d)(\\d{3})(\\d{3,4})","$1 $2 $3",["[2-6]"],"0$1","",0],[,"(7\\d{2})(\\d{3})(\\d{4})","$1 $2 $3",
["7"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],IR:[,[,,"[14-8]\\d{6,9}|[23]\\d{4,9}|9(?:[1-4]\\d{8}|9\\d{2,8})","\\d{4,10}"],[,,"1(?:[13-589][12]|[27][1-4])\\d{7}|2(?:1\\d{3,8}|3[12]\\d{7}|4(?:1\\d{4,7}|2\\d{7})|5(?:1\\d{3,7}|[2356]\\d{7})|6\\d{8}|7[34]\\d{7}|[89][12]\\d{7})|3(?:1(?:1\\d{4,7}|2\\d{7})|2[1-4]\\d{7}|3(?:[125]\\d{7}|4\\d{6,7})|4(?:1\\d{6,7}[24-9]\\d{7})|5(?:1\\d{4,7}|[23]\\d{7})|[6-9][12]\\d{7})|4(?:[135-9][12]\\d{7}|2[1-467]\\d{7}|4(?:1\\d{4,7}|[2-4]\\d{7}))|5(?:1(?:1\\d{4,7}|2\\d{7})|2[89]\\d{7}|3[1-5]\\d{7}|4(?:1\\d{4,7}|[2-8]\\d{7})|[5-7][12]\\d{7}|8[1245]\\d{7})|6(?:1(?:1\\d{6,7}|2\\d{7})|[347-9][12]\\d{7}|5(?:1\\d{7}|2\\d{6,7})|6[1-6]\\d{7})|7(?:[13589][12]|2[1289]|4[1-4]|6[1-6]|7[1-3])\\d{7}|8(?:[145][12]|3[124578]|6[1256]|7[1245])\\d{7}",
"\\d{5,10}",,,"2123456789"],[,,"9[1-3]\\d{8}","\\d{10}",,,"9123456789"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"(?:[2-6]0\\d|993)\\d{7}","\\d{10}",,,"9932123456"],"IR",98,"00","0",,,"0",,,,[[,"(2[15])(\\d{3,5})","$1 $2",["2(?:1|5[0-47-9])"],"0$1","",0],[,"(2[15])(\\d{3})(\\d{3,4})","$1 $2 $3",["2(?:1|5[0-47-9])"],"0$1","",0],[,"(2\\d)(\\d{4})(\\d{4})","$1 $2 $3",["2(?:[16]|5[0-47-9])"],"0$1","",0],[,"(\\d{3})(\\d{3})(\\d{3,4})","$1 $2 $3",["[13-9]|2[02-57-9]"],"0$1","",0],[,"(\\d{3})(\\d{2})(\\d{2,3})",
"$1 $2 $3",["[13-9]|2[02-57-9]"],"0$1","",0],[,"(\\d{3})(\\d{3})","$1 $2",["[13-9]|2[02-57-9]"],"0$1","",0]],,[,,"943\\d{7}","\\d{10}",,,"9432123456"],,,[,,"NA","NA"],[,,"9990\\d{0,6}","\\d{4,10}",,,"9990123456"],,,[,,"NA","NA"]],IS:[,[,,"[4-9]\\d{6}|38\\d{7}","\\d{7,9}"],[,,"(?:4(?:[14][0-245]|2[0-7]|[37][0-8]|5[0-3568]|6\\d|8[0-36-8])|5(?:05|[156]\\d|2[02578]|3[013-7]|4[03-7]|7[0-2578]|8[0-35-9]|9[013-689])|87[23])\\d{4}","\\d{7}",,,"4101234"],[,,"38[589]\\d{6}|(?:6(?:1[0-8]|3[0-27-9]|4[0-27]|5[0-29]|[67][0-69]|9\\d)|7(?:5[057]|7\\d|8[0-3])|8(?:2[0-5]|[469]\\d|5[1-9]))\\d{4}",
"\\d{7,9}",,,"6101234"],[,,"800\\d{4}","\\d{7}",,,"8001234"],[,,"90\\d{5}","\\d{7}",,,"9011234"],[,,"NA","NA"],[,,"NA","NA"],[,,"49[0-24-79]\\d{4}","\\d{7}",,,"4921234"],"IS",354,"00",,,,,,,,[[,"(\\d{3})(\\d{4})","$1 $2",["[4-9]"],"","",0],[,"(3\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["3"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"(?:6(?:2[0-8]|49|8\\d)|8(?:2[6-9]|[38]\\d|50|7[014-9])|95[48])\\d{4}","\\d{7}",,,"6201234"]],IT:[,[,,"[01589]\\d{5,10}|3(?:[12457-9]\\d{8}|[36]\\d{7,9})",
"\\d{6,11}"],[,,"0(?:[26]\\d{4,9}|(?:1(?:[0159]\\d|[27][1-5]|31|4[1-4]|6[1356]|8[2-57])|3(?:[0159]\\d|2[1-4]|3[12]|[48][1-6]|6[2-59]|7[1-7])|4(?:[0159]\\d|[23][1-9]|4[245]|6[1-5]|7[1-4]|81)|5(?:[0159]\\d|2[1-5]|3[2-6]|4[1-79]|6[4-6]|7[1-578]|8[3-8])|7(?:[0159]\\d|2[12]|3[1-7]|4[2346]|6[13569]|7[13-6]|8[1-59])|8(?:[0159]\\d|2[34578]|3[1-356]|[6-8][1-5])|9(?:[0159]\\d|[238][1-5]|4[12]|6[1-8]|7[1-6]))\\d{2,7})","\\d{6,11}",,,"0212345678"],[,,"3(?:[12457-9]\\d{8}|6\\d{7,8}|3\\d{7,9})","\\d{9,11}",,,"3123456789"],
[,,"80(?:0\\d{6}|3\\d{3})","\\d{6,9}",,,"800123456"],[,,"0878\\d{5}|1(?:44|6[346])\\d{6}|89(?:2\\d{3}|4(?:[0-4]\\d{2}|[5-9]\\d{4})|5(?:[0-4]\\d{2}|[5-9]\\d{6})|9\\d{6})","\\d{6,10}",,,"899123456"],[,,"84(?:[08]\\d{6}|[17]\\d{3})","\\d{6,9}",,,"848123456"],[,,"1(?:78\\d|99)\\d{6}","\\d{9,10}",,,"1781234567"],[,,"55\\d{8}","\\d{10}",,,"5512345678"],"IT",39,"00",,,,,,,,[[,"(\\d{2})(\\d{3,4})(\\d{4})","$1 $2 $3",["0[26]|55"],"","",0],[,"(0[26])(\\d{4})(\\d{5})","$1 $2 $3",["0[26]"],"","",0],[,"(0[26])(\\d{4,6})",
"$1 $2",["0[26]"],"","",0],[,"(0\\d{2})(\\d{3,4})(\\d{4})","$1 $2 $3",["0[13-57-9][0159]"],"","",0],[,"(\\d{3})(\\d{3,6})","$1 $2",["0[13-57-9][0159]|8(?:03|4[17]|9[245])","0[13-57-9][0159]|8(?:03|4[17]|9(?:2|[45][0-4]))"],"","",0],[,"(0\\d{3})(\\d{3})(\\d{4})","$1 $2 $3",["0[13-57-9][2-46-8]"],"","",0],[,"(0\\d{3})(\\d{2,6})","$1 $2",["0[13-57-9][2-46-8]"],"","",0],[,"(\\d{3})(\\d{3})(\\d{3,4})","$1 $2 $3",["[13]|8(?:00|4[08]|9[59])","[13]|8(?:00|4[08]|9(?:5[5-9]|9))"],"","",0],[,"(\\d{4})(\\d{4})",
"$1 $2",["894","894[5-9]"],"","",0],[,"(\\d{3})(\\d{4})(\\d{4})","$1 $2 $3",["3"],"","",0]],,[,,"NA","NA"],,,[,,"848\\d{6}","\\d{9}",,,"848123456"],[,,"NA","NA"],1,,[,,"NA","NA"]],JE:[,[,,"[135789]\\d{6,9}","\\d{6,10}"],[,,"1534\\d{6}","\\d{6,10}",,,"1534456789"],[,,"7(?:509|7(?:00|97)|829|937)\\d{6}","\\d{10}",,,"7797123456"],[,,"80(?:07(?:35|81)|8901)\\d{4}","\\d{10}",,,"8007354567"],[,,"(?:871206|90(?:066[59]|1810|71(?:07|55)))\\d{4}","\\d{10}",,,"9018105678"],[,,"8(?:4(?:4(?:4(?:05|42|69)|703)|5(?:041|800))|70002)\\d{4}",
"\\d{10}",,,"8447034567"],[,,"701511\\d{4}","\\d{10}",,,"7015115678"],[,,"56\\d{8}","\\d{10}",,,"5612345678"],"JE",44,"00","0"," x",,"0",,,,,,[,,"76(?:0[012]|2[356]|4[0134]|5[49]|6[0-369]|77|81|9[39])\\d{6}","\\d{10}",,,"7640123456"],,,[,,"NA","NA"],[,,"3(?:0(?:07(?:35|81)|8901)|3\\d{4}|4(?:4(?:4(?:05|42|69)|703)|5(?:041|800))|7(?:0002|1206))\\d{4}|55\\d{8}","\\d{10}",,,"5512345678"],,,[,,"NA","NA"]],JM:[,[,,"[589]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"876(?:5(?:0[12]|1[0-468]|2[35]|63)|6(?:0[1-3579]|1[027-9]|[23]\\d|40|5[06]|6[2-589]|7[05]|8[04]|9[4-9])|7(?:0[2-689]|[1-6]\\d|8[056]|9[45])|9(?:0[1-8]|1[02378]|[2-8]\\d|9[2-468]))\\d{4}",
"\\d{7}(?:\\d{3})?",,,"8765123456"],[,,"876(?:2[1789]\\d|[348]\\d{2}|5(?:08|27|6[0-24-9]|[3-578]\\d)|7(?:0[07]|7\\d|8[1-47-9]|9[0-36-9])|9(?:[01]9|9[0579]))\\d{4}","\\d{10}",,,"8762101234"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002123456"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002123456"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"JM",1,"011","1",,,"1",,,,,,[,,"NA","NA"],,"876",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],JO:[,[,,"[235-9]\\d{7,8}",
"\\d{7,9}"],[,,"(?:2(?:6(?:2[0-35-9]|3[0-57-8]|4[24-7]|5[0-24-8]|[6-8][02]|9[0-2])|7(?:0[1-79]|10|2[014-7]|3[0-689]|4[019]|5[0-3578]))|32(?:0[1-69]|1[1-35-7]|2[024-7]|3\\d|4[0-2]|[57][02]|60)|53(?:0[0-2]|[13][02]|2[0-59]|49|5[0-35-9]|6[15]|7[45]|8[1-6]|9[0-36-9])|6(?:2[50]0|300|4(?:0[0125]|1[2-7]|2[0569]|[38][07-9]|4[025689]|6[0-589]|7\\d|9[0-2])|5(?:[01][056]|2[034]|3[0-57-9]|4[17-8]|5[0-69]|6[0-35-9]|7[1-379]|8[0-68]|9[02-39]))|87(?:[02]0|7[08]|9[09]))\\d{4}","\\d{7,8}",,,"62001234"],[,,"7(?:55|7[25-9]|8[05-9]|9[015-9])\\d{6}",
"\\d{9}",,,"790123456"],[,,"80\\d{6}","\\d{8}",,,"80012345"],[,,"900\\d{5}","\\d{8}",,,"90012345"],[,,"85\\d{6}","\\d{8}",,,"85012345"],[,,"70\\d{7}","\\d{9}",,,"700123456"],[,,"NA","NA"],"JO",962,"00","0",,,"0",,,,[[,"(\\d)(\\d{3})(\\d{4})","$1 $2 $3",["[2356]|87"],"(0$1)","",0],[,"(7)(\\d{4})(\\d{4})","$1 $2 $3",["7[457-9]"],"0$1","",0],[,"(\\d{3})(\\d{5,6})","$1 $2",["70|8[0158]|9"],"0$1","",0]],,[,,"74(?:66|77)\\d{5}","\\d{9}",,,"746612345"],,,[,,"NA","NA"],[,,"8(?:10|8\\d)\\d{5}","\\d{8}",,,
"88101234"],,,[,,"NA","NA"]],JP:[,[,,"[1-9]\\d{8,9}|00(?:[36]\\d{7,14}|7\\d{5,7}|8\\d{7})","\\d{8,17}"],[,,"(?:1(?:1[235-8]|2[3-6]|3[3-9]|4[2-6]|[58][2-8]|6[2-7]|7[2-9]|9[1-9])|2[2-9]\\d|[36][1-9]\\d|4(?:6[02-8]|[2-578]\\d|9[2-59])|5(?:6[1-9]|7[2-8]|[2-589]\\d)|7(?:3[4-9]|4[02-9]|[25-9]\\d)|8(?:3[2-9]|4[5-9]|5[1-9]|8[03-9]|[2679]\\d)|9(?:[679][1-9]|[2-58]\\d))\\d{6}","\\d{9}",,,"312345678"],[,,"(?:[79]0\\d|80[1-9])\\d{7}","\\d{10}",,,"7012345678"],[,,"120\\d{6}|800\\d{7}|00(?:37\\d{6,13}|66\\d{6,13}|777(?:[01]\\d{2}|5\\d{3}|8\\d{4})|882[1245]\\d{4})",
"\\d{8,17}",,,"120123456"],[,,"990\\d{6}","\\d{9}",,,"990123456"],[,,"NA","NA"],[,,"60\\d{7}","\\d{9}",,,"601234567"],[,,"50[1-9]\\d{7}","\\d{10}",,,"5012345678"],"JP",81,"010","0",,,"0",,,,[[,"(\\d{3})(\\d{3})(\\d{3})","$1-$2-$3",["(?:12|57|99)0"],"0$1","",0],[,"(\\d{3})(\\d{3})(\\d{4})","$1-$2-$3",["800"],"0$1","",0],[,"(\\d{4})(\\d{4})","$1-$2",["0077"],"$1","",0],[,"(\\d{4})(\\d{2})(\\d{3,4})","$1-$2-$3",["0077"],"$1","",0],[,"(\\d{4})(\\d{2})(\\d{4})","$1-$2-$3",["0088"],"$1","",0],[,"(\\d{4})(\\d{3})(\\d{3,4})",
"$1-$2-$3",["00(?:37|66)"],"$1","",0],[,"(\\d{4})(\\d{4})(\\d{4,5})","$1-$2-$3",["00(?:37|66)"],"$1","",0],[,"(\\d{4})(\\d{5})(\\d{5,6})","$1-$2-$3",["00(?:37|66)"],"$1","",0],[,"(\\d{4})(\\d{6})(\\d{6,7})","$1-$2-$3",["00(?:37|66)"],"$1","",0],[,"(\\d{2})(\\d{4})(\\d{4})","$1-$2-$3",["[2579]0|80[1-9]"],"0$1","",0],[,"(\\d{4})(\\d)(\\d{4})","$1-$2-$3",["1(?:26|3[79]|4[56]|5[4-68]|6[3-5])|5(?:76|97)|499|746|8(?:3[89]|63|47|51)|9(?:49|80|9[16])","1(?:267|3(?:7[247]|9[278])|4(?:5[67]|66)|5(?:47|58|64|8[67])|6(?:3[245]|48|5[4-68]))|5(?:76|97)9|499[2468]|7468|8(?:3(?:8[78]|96)|636|477|51[24])|9(?:496|802|9(?:1[23]|69))",
"1(?:267|3(?:7[247]|9[278])|4(?:5[67]|66)|5(?:47|58|64|8[67])|6(?:3[245]|48|5[4-68]))|5(?:769|979[2-69])|499[2468]|7468|8(?:3(?:8[78]|96[2457-9])|636[2-57-9]|477|51[24])|9(?:496|802|9(?:1[23]|69))"],"0$1","",0],[,"(\\d{3})(\\d{2})(\\d{4})","$1-$2-$3",["1(?:2[3-6]|3[3-9]|4[2-6]|5[2-8]|[68][2-7]|7[2-689]|9[1-578])|2(?:2[03-689]|3[3-58]|4[0-468]|5[04-8]|6[013-8]|7[06-9]|8[02-57-9]|9[13])|4(?:2[28]|3[689]|6[035-7]|7[05689]|80|9[3-5])|5(?:3[1-36-9]|4[4578]|5[013-8]|6[1-9]|7[2-8]|8[14-7]|9[4-9])|7(?:2[15]|3[5-9]|4[02-9]|6[135-8]|7[0-4689]|9[014-9])|8(?:2[49]|3[3-8]|4[5-8]|5[2-9]|6[35-9]|7[579]|8[03-579]|9[2-8])|9(?:[23]0|4[02-46-9]|5[024-79]|6[4-9]|7[2-47-9]|8[02-7]|9[3-7])",
"1(?:2[3-6]|3[3-9]|4[2-6]|5(?:[236-8]|[45][2-69])|[68][2-7]|7[2-689]|9[1-578])|2(?:2(?:[04-689]|3[23])|3[3-58]|4[0-468]|5(?:5[78]|7[2-4]|[0468][2-9])|6(?:[0135-8]|4[2-5])|7(?:[0679]|8[2-7])|8(?:[024578]|3[25-9]|9[6-9])|9(?:11|3[2-4]))|4(?:2(?:2[2-9]|8[237-9])|3[689]|6[035-7]|7(?:[059][2-8]|[68])|80|9[3-5])|5(?:3[1-36-9]|4[4578]|5[013-8]|6[1-9]|7[2-8]|8[14-7]|9(?:[89][2-8]|[4-7]))|7(?:2[15]|3[5-9]|4[02-9]|6[135-8]|7[0-4689]|9(?:[017-9]|4[6-8]|5[2-478]|6[2-589]))|8(?:2(?:4[4-8]|9[2-8])|3(?:7[2-6]|[3-6][2-9]|8[2-5])|4[5-8]|5[2-9]|6(?:[37]|5[4-7]|6[2-9]|8[2-8]|9[236-9])|7[579]|8[03-579]|9[2-8])|9(?:[23]0|4[02-46-9]|5[024-79]|6[4-9]|7[2-47-9]|8[02-7]|9(?:3[34]|[4-7]))",
"1(?:2[3-6]|3[3-9]|4[2-6]|5(?:[236-8]|[45][2-69])|[68][2-7]|7[2-689]|9[1-578])|2(?:2(?:[04-689]|3[23])|3[3-58]|4[0-468]|5(?:5[78]|7[2-4]|[0468][2-9])|6(?:[0135-8]|4[2-5])|7(?:[0679]|8[2-7])|8(?:[024578]|3[25-9]|9[6-9])|9(?:11|3[2-4]))|4(?:2(?:2[2-9]|8[237-9])|3[689]|6[035-7]|7(?:[059][2-8]|[68])|80|9[3-5])|5(?:3[1-36-9]|4[4578]|5[013-8]|6[1-9]|7[2-8]|8[14-7]|9(?:[89][2-8]|[4-7]))|7(?:2[15]|3[5-9]|4[02-9]|6[135-8]|7[0-4689]|9(?:[017-9]|4[6-8]|5[2-478]|6[2-589]))|8(?:2(?:4[4-8]|9(?:[3578]|20|4[04-9]|6[56]))|3(?:7(?:[2-5]|6[0-59])|[3-6][2-9]|8[2-5])|4[5-8]|5[2-9]|6(?:[37]|5(?:[467]|5[014-9])|6(?:[2-8]|9[02-69])|8[2-8]|9(?:[236-8]|9[23]))|7[579]|8[03-579]|9[2-8])|9(?:[23]0|4[02-46-9]|5[024-79]|6[4-9]|7[2-47-9]|8[02-7]|9(?:3(?:3[02-9]|4[0-24689])|4[2-69]|[5-7]))",
"1(?:2[3-6]|3[3-9]|4[2-6]|5(?:[236-8]|[45][2-69])|[68][2-7]|7[2-689]|9[1-578])|2(?:2(?:[04-689]|3[23])|3[3-58]|4[0-468]|5(?:5[78]|7[2-4]|[0468][2-9])|6(?:[0135-8]|4[2-5])|7(?:[0679]|8[2-7])|8(?:[024578]|3[25-9]|9[6-9])|9(?:11|3[2-4]))|4(?:2(?:2[2-9]|8[237-9])|3[689]|6[035-7]|7(?:[059][2-8]|[68])|80|9[3-5])|5(?:3[1-36-9]|4[4578]|5[013-8]|6[1-9]|7[2-8]|8[14-7]|9(?:[89][2-8]|[4-7]))|7(?:2[15]|3[5-9]|4[02-9]|6[135-8]|7[0-4689]|9(?:[017-9]|4[6-8]|5[2-478]|6[2-589]))|8(?:2(?:4[4-8]|9(?:[3578]|20|4[04-9]|6(?:5[25]|60)))|3(?:7(?:[2-5]|6[0-59])|[3-6][2-9]|8[2-5])|4[5-8]|5[2-9]|6(?:[37]|5(?:[467]|5[014-9])|6(?:[2-8]|9[02-69])|8[2-8]|9(?:[236-8]|9[23]))|7[579]|8[03-579]|9[2-8])|9(?:[23]0|4[02-46-9]|5[024-79]|6[4-9]|7[2-47-9]|8[02-7]|9(?:3(?:3[02-9]|4[0-24689])|4[2-69]|[5-7]))"],
"0$1","",0],[,"(\\d{2})(\\d{3})(\\d{4})","$1-$2-$3",["1|2(?:2[37]|5[5-9]|64|78|8[39]|91)|4(?:2[2689]|64|7[347])|5(?:[2-589]|39)|60|8(?:[46-9]|3[279]|2[124589])|9(?:[235-8]|93)","1|2(?:2[37]|5(?:[57]|[68]0|9[19])|64|78|8[39]|917)|4(?:2(?:[68]|20|9[178])|64|7[347])|5(?:[2-589]|39[67])|60|8(?:[46-9]|3[279]|2[124589])|9(?:[235-8]|93[34])","1|2(?:2[37]|5(?:[57]|[68]0|9(?:17|99))|64|78|8[39]|917)|4(?:2(?:[68]|20|9[178])|64|7[347])|5(?:[2-589]|39[67])|60|8(?:[46-9]|3[279]|2[124589])|9(?:[235-8]|93(?:31|4))"],
"0$1","",0],[,"(\\d{3})(\\d{2})(\\d{4})","$1-$2-$3",["2(?:9[14-79]|74|[34]7|[56]9)|82|993"],"0$1","",0],[,"(\\d)(\\d{4})(\\d{4})","$1-$2-$3",["3|4(?:2[09]|7[01])|6[1-9]"],"0$1","",0],[,"(\\d{2})(\\d{3})(\\d{4})","$1-$2-$3",["[2479][1-9]"],"0$1","",0]],[[,"(\\d{3})(\\d{3})(\\d{3})","$1-$2-$3",["(?:12|57|99)0","(?:12|57|99)0"],"0$1","",0],[,"(\\d{3})(\\d{3})(\\d{4})","$1-$2-$3",["800","800"],"0$1","",0],[,"(\\d{2})(\\d{4})(\\d{4})","$1-$2-$3",["[2579]0|80[1-9]","[2579]0|80[1-9]"],"0$1","",0],[,"(\\d{4})(\\d)(\\d{4})",
"$1-$2-$3","1(?:26|3[79]|4[56]|5[4-68]|6[3-5])|5(?:76|97)|499|746|8(?:3[89]|63|47|51)|9(?:49|80|9[16]) 1(?:267|3(?:7[247]|9[278])|4(?:5[67]|66)|5(?:47|58|64|8[67])|6(?:3[245]|48|5[4-68]))|5(?:76|97)9|499[2468]|7468|8(?:3(?:8[78]|96)|636|477|51[24])|9(?:496|802|9(?:1[23]|69)) 1(?:267|3(?:7[247]|9[278])|4(?:5[67]|66)|5(?:47|58|64|8[67])|6(?:3[245]|48|5[4-68]))|5(?:769|979[2-69])|499[2468]|7468|8(?:3(?:8[78]|96[2457-9])|636[2-57-9]|477|51[24])|9(?:496|802|9(?:1[23]|69)) 1(?:26|3[79]|4[56]|5[4-68]|6[3-5])|5(?:76|97)|499|746|8(?:3[89]|63|47|51)|9(?:49|80|9[16]) 1(?:267|3(?:7[247]|9[278])|4(?:5[67]|66)|5(?:47|58|64|8[67])|6(?:3[245]|48|5[4-68]))|5(?:76|97)9|499[2468]|7468|8(?:3(?:8[78]|96)|636|477|51[24])|9(?:496|802|9(?:1[23]|69)) 1(?:267|3(?:7[247]|9[278])|4(?:5[67]|66)|5(?:47|58|64|8[67])|6(?:3[245]|48|5[4-68]))|5(?:769|979[2-69])|499[2468]|7468|8(?:3(?:8[78]|96[2457-9])|636[2-57-9]|477|51[24])|9(?:496|802|9(?:1[23]|69))".split(" "),
"0$1","",0],[,"(\\d{3})(\\d{2})(\\d{4})","$1-$2-$3","1(?:2[3-6]|3[3-9]|4[2-6]|5[2-8]|[68][2-7]|7[2-689]|9[1-578])|2(?:2[03-689]|3[3-58]|4[0-468]|5[04-8]|6[013-8]|7[06-9]|8[02-57-9]|9[13])|4(?:2[28]|3[689]|6[035-7]|7[05689]|80|9[3-5])|5(?:3[1-36-9]|4[4578]|5[013-8]|6[1-9]|7[2-8]|8[14-7]|9[4-9])|7(?:2[15]|3[5-9]|4[02-9]|6[135-8]|7[0-4689]|9[014-9])|8(?:2[49]|3[3-8]|4[5-8]|5[2-9]|6[35-9]|7[579]|8[03-579]|9[2-8])|9(?:[23]0|4[02-46-9]|5[024-79]|6[4-9]|7[2-47-9]|8[02-7]|9[3-7]) 1(?:2[3-6]|3[3-9]|4[2-6]|5(?:[236-8]|[45][2-69])|[68][2-7]|7[2-689]|9[1-578])|2(?:2(?:[04-689]|3[23])|3[3-58]|4[0-468]|5(?:5[78]|7[2-4]|[0468][2-9])|6(?:[0135-8]|4[2-5])|7(?:[0679]|8[2-7])|8(?:[024578]|3[25-9]|9[6-9])|9(?:11|3[2-4]))|4(?:2(?:2[2-9]|8[237-9])|3[689]|6[035-7]|7(?:[059][2-8]|[68])|80|9[3-5])|5(?:3[1-36-9]|4[4578]|5[013-8]|6[1-9]|7[2-8]|8[14-7]|9(?:[89][2-8]|[4-7]))|7(?:2[15]|3[5-9]|4[02-9]|6[135-8]|7[0-4689]|9(?:[017-9]|4[6-8]|5[2-478]|6[2-589]))|8(?:2(?:4[4-8]|9[2-8])|3(?:7[2-6]|[3-6][2-9]|8[2-5])|4[5-8]|5[2-9]|6(?:[37]|5[4-7]|6[2-9]|8[2-8]|9[236-9])|7[579]|8[03-579]|9[2-8])|9(?:[23]0|4[02-46-9]|5[024-79]|6[4-9]|7[2-47-9]|8[02-7]|9(?:3[34]|[4-7])) 1(?:2[3-6]|3[3-9]|4[2-6]|5(?:[236-8]|[45][2-69])|[68][2-7]|7[2-689]|9[1-578])|2(?:2(?:[04-689]|3[23])|3[3-58]|4[0-468]|5(?:5[78]|7[2-4]|[0468][2-9])|6(?:[0135-8]|4[2-5])|7(?:[0679]|8[2-7])|8(?:[024578]|3[25-9]|9[6-9])|9(?:11|3[2-4]))|4(?:2(?:2[2-9]|8[237-9])|3[689]|6[035-7]|7(?:[059][2-8]|[68])|80|9[3-5])|5(?:3[1-36-9]|4[4578]|5[013-8]|6[1-9]|7[2-8]|8[14-7]|9(?:[89][2-8]|[4-7]))|7(?:2[15]|3[5-9]|4[02-9]|6[135-8]|7[0-4689]|9(?:[017-9]|4[6-8]|5[2-478]|6[2-589]))|8(?:2(?:4[4-8]|9(?:[3578]|20|4[04-9]|6[56]))|3(?:7(?:[2-5]|6[0-59])|[3-6][2-9]|8[2-5])|4[5-8]|5[2-9]|6(?:[37]|5(?:[467]|5[014-9])|6(?:[2-8]|9[02-69])|8[2-8]|9(?:[236-8]|9[23]))|7[579]|8[03-579]|9[2-8])|9(?:[23]0|4[02-46-9]|5[024-79]|6[4-9]|7[2-47-9]|8[02-7]|9(?:3(?:3[02-9]|4[0-24689])|4[2-69]|[5-7])) 1(?:2[3-6]|3[3-9]|4[2-6]|5(?:[236-8]|[45][2-69])|[68][2-7]|7[2-689]|9[1-578])|2(?:2(?:[04-689]|3[23])|3[3-58]|4[0-468]|5(?:5[78]|7[2-4]|[0468][2-9])|6(?:[0135-8]|4[2-5])|7(?:[0679]|8[2-7])|8(?:[024578]|3[25-9]|9[6-9])|9(?:11|3[2-4]))|4(?:2(?:2[2-9]|8[237-9])|3[689]|6[035-7]|7(?:[059][2-8]|[68])|80|9[3-5])|5(?:3[1-36-9]|4[4578]|5[013-8]|6[1-9]|7[2-8]|8[14-7]|9(?:[89][2-8]|[4-7]))|7(?:2[15]|3[5-9]|4[02-9]|6[135-8]|7[0-4689]|9(?:[017-9]|4[6-8]|5[2-478]|6[2-589]))|8(?:2(?:4[4-8]|9(?:[3578]|20|4[04-9]|6(?:5[25]|60)))|3(?:7(?:[2-5]|6[0-59])|[3-6][2-9]|8[2-5])|4[5-8]|5[2-9]|6(?:[37]|5(?:[467]|5[014-9])|6(?:[2-8]|9[02-69])|8[2-8]|9(?:[236-8]|9[23]))|7[579]|8[03-579]|9[2-8])|9(?:[23]0|4[02-46-9]|5[024-79]|6[4-9]|7[2-47-9]|8[02-7]|9(?:3(?:3[02-9]|4[0-24689])|4[2-69]|[5-7])) 1(?:2[3-6]|3[3-9]|4[2-6]|5[2-8]|[68][2-7]|7[2-689]|9[1-578])|2(?:2[03-689]|3[3-58]|4[0-468]|5[04-8]|6[013-8]|7[06-9]|8[02-57-9]|9[13])|4(?:2[28]|3[689]|6[035-7]|7[05689]|80|9[3-5])|5(?:3[1-36-9]|4[4578]|5[013-8]|6[1-9]|7[2-8]|8[14-7]|9[4-9])|7(?:2[15]|3[5-9]|4[02-9]|6[135-8]|7[0-4689]|9[014-9])|8(?:2[49]|3[3-8]|4[5-8]|5[2-9]|6[35-9]|7[579]|8[03-579]|9[2-8])|9(?:[23]0|4[02-46-9]|5[024-79]|6[4-9]|7[2-47-9]|8[02-7]|9[3-7]) 1(?:2[3-6]|3[3-9]|4[2-6]|5(?:[236-8]|[45][2-69])|[68][2-7]|7[2-689]|9[1-578])|2(?:2(?:[04-689]|3[23])|3[3-58]|4[0-468]|5(?:5[78]|7[2-4]|[0468][2-9])|6(?:[0135-8]|4[2-5])|7(?:[0679]|8[2-7])|8(?:[024578]|3[25-9]|9[6-9])|9(?:11|3[2-4]))|4(?:2(?:2[2-9]|8[237-9])|3[689]|6[035-7]|7(?:[059][2-8]|[68])|80|9[3-5])|5(?:3[1-36-9]|4[4578]|5[013-8]|6[1-9]|7[2-8]|8[14-7]|9(?:[89][2-8]|[4-7]))|7(?:2[15]|3[5-9]|4[02-9]|6[135-8]|7[0-4689]|9(?:[017-9]|4[6-8]|5[2-478]|6[2-589]))|8(?:2(?:4[4-8]|9[2-8])|3(?:7[2-6]|[3-6][2-9]|8[2-5])|4[5-8]|5[2-9]|6(?:[37]|5[4-7]|6[2-9]|8[2-8]|9[236-9])|7[579]|8[03-579]|9[2-8])|9(?:[23]0|4[02-46-9]|5[024-79]|6[4-9]|7[2-47-9]|8[02-7]|9(?:3[34]|[4-7])) 1(?:2[3-6]|3[3-9]|4[2-6]|5(?:[236-8]|[45][2-69])|[68][2-7]|7[2-689]|9[1-578])|2(?:2(?:[04-689]|3[23])|3[3-58]|4[0-468]|5(?:5[78]|7[2-4]|[0468][2-9])|6(?:[0135-8]|4[2-5])|7(?:[0679]|8[2-7])|8(?:[024578]|3[25-9]|9[6-9])|9(?:11|3[2-4]))|4(?:2(?:2[2-9]|8[237-9])|3[689]|6[035-7]|7(?:[059][2-8]|[68])|80|9[3-5])|5(?:3[1-36-9]|4[4578]|5[013-8]|6[1-9]|7[2-8]|8[14-7]|9(?:[89][2-8]|[4-7]))|7(?:2[15]|3[5-9]|4[02-9]|6[135-8]|7[0-4689]|9(?:[017-9]|4[6-8]|5[2-478]|6[2-589]))|8(?:2(?:4[4-8]|9(?:[3578]|20|4[04-9]|6[56]))|3(?:7(?:[2-5]|6[0-59])|[3-6][2-9]|8[2-5])|4[5-8]|5[2-9]|6(?:[37]|5(?:[467]|5[014-9])|6(?:[2-8]|9[02-69])|8[2-8]|9(?:[236-8]|9[23]))|7[579]|8[03-579]|9[2-8])|9(?:[23]0|4[02-46-9]|5[024-79]|6[4-9]|7[2-47-9]|8[02-7]|9(?:3(?:3[02-9]|4[0-24689])|4[2-69]|[5-7])) 1(?:2[3-6]|3[3-9]|4[2-6]|5(?:[236-8]|[45][2-69])|[68][2-7]|7[2-689]|9[1-578])|2(?:2(?:[04-689]|3[23])|3[3-58]|4[0-468]|5(?:5[78]|7[2-4]|[0468][2-9])|6(?:[0135-8]|4[2-5])|7(?:[0679]|8[2-7])|8(?:[024578]|3[25-9]|9[6-9])|9(?:11|3[2-4]))|4(?:2(?:2[2-9]|8[237-9])|3[689]|6[035-7]|7(?:[059][2-8]|[68])|80|9[3-5])|5(?:3[1-36-9]|4[4578]|5[013-8]|6[1-9]|7[2-8]|8[14-7]|9(?:[89][2-8]|[4-7]))|7(?:2[15]|3[5-9]|4[02-9]|6[135-8]|7[0-4689]|9(?:[017-9]|4[6-8]|5[2-478]|6[2-589]))|8(?:2(?:4[4-8]|9(?:[3578]|20|4[04-9]|6(?:5[25]|60)))|3(?:7(?:[2-5]|6[0-59])|[3-6][2-9]|8[2-5])|4[5-8]|5[2-9]|6(?:[37]|5(?:[467]|5[014-9])|6(?:[2-8]|9[02-69])|8[2-8]|9(?:[236-8]|9[23]))|7[579]|8[03-579]|9[2-8])|9(?:[23]0|4[02-46-9]|5[024-79]|6[4-9]|7[2-47-9]|8[02-7]|9(?:3(?:3[02-9]|4[0-24689])|4[2-69]|[5-7]))".split(" "),
"0$1","",0],[,"(\\d{2})(\\d{3})(\\d{4})","$1-$2-$3","1|2(?:2[37]|5[5-9]|64|78|8[39]|91)|4(?:2[2689]|64|7[347])|5(?:[2-589]|39)|60|8(?:[46-9]|3[279]|2[124589])|9(?:[235-8]|93) 1|2(?:2[37]|5(?:[57]|[68]0|9[19])|64|78|8[39]|917)|4(?:2(?:[68]|20|9[178])|64|7[347])|5(?:[2-589]|39[67])|60|8(?:[46-9]|3[279]|2[124589])|9(?:[235-8]|93[34]) 1|2(?:2[37]|5(?:[57]|[68]0|9(?:17|99))|64|78|8[39]|917)|4(?:2(?:[68]|20|9[178])|64|7[347])|5(?:[2-589]|39[67])|60|8(?:[46-9]|3[279]|2[124589])|9(?:[235-8]|93(?:31|4)) 1|2(?:2[37]|5[5-9]|64|78|8[39]|91)|4(?:2[2689]|64|7[347])|5(?:[2-589]|39)|60|8(?:[46-9]|3[279]|2[124589])|9(?:[235-8]|93) 1|2(?:2[37]|5(?:[57]|[68]0|9[19])|64|78|8[39]|917)|4(?:2(?:[68]|20|9[178])|64|7[347])|5(?:[2-589]|39[67])|60|8(?:[46-9]|3[279]|2[124589])|9(?:[235-8]|93[34]) 1|2(?:2[37]|5(?:[57]|[68]0|9(?:17|99))|64|78|8[39]|917)|4(?:2(?:[68]|20|9[178])|64|7[347])|5(?:[2-589]|39[67])|60|8(?:[46-9]|3[279]|2[124589])|9(?:[235-8]|93(?:31|4))".split(" "),
"0$1","",0],[,"(\\d{3})(\\d{2})(\\d{4})","$1-$2-$3",["2(?:9[14-79]|74|[34]7|[56]9)|82|993","2(?:9[14-79]|74|[34]7|[56]9)|82|993"],"0$1","",0],[,"(\\d)(\\d{4})(\\d{4})","$1-$2-$3",["3|4(?:2[09]|7[01])|6[1-9]","3|4(?:2[09]|7[01])|6[1-9]"],"0$1","",0],[,"(\\d{2})(\\d{3})(\\d{4})","$1-$2-$3",["[2479][1-9]","[2479][1-9]"],"0$1","",0]],[,,"20\\d{8}","\\d{10}",,,"2012345678"],,,[,,"00(?:37\\d{6,13}|66\\d{6,13}|777(?:[01]\\d{2}|5\\d{3}|8\\d{4})|882[1245]\\d{4})","\\d{8,17}",,,"00777012"],[,,"570\\d{6}","\\d{9}",
,,"570123456"],1,,[,,"NA","NA"]],KE:[,[,,"20\\d{6,7}|[4-9]\\d{6,9}","\\d{5,10}"],[,,"20\\d{6,7}|4(?:[013]\\d{7}|[24-6]\\d{5,7})|5(?:[0-36-8]\\d{5,7}|[459]\\d{5})|6(?:[08]\\d{5}|[14-79]\\d{5,7}|2\\d{7})","\\d{5,9}",,,"202012345"],[,,"7(?:0[0-8]|[123]\\d|5[0-6]|7[0-5]|8[5-9])\\d{6}","\\d{9}",,,"712123456"],[,,"800[24-8]\\d{5,6}","\\d{9,10}",,,"800223456"],[,,"900[02-578]\\d{5}","\\d{9}",,,"900223456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"KE",254,"000","0",,,"0",,,,[[,"(\\d{2})(\\d{4,7})","$1 $2",
["[24-6]"],"0$1","",0],[,"(\\d{3})(\\d{6,7})","$1 $2",["7"],"0$1","",0],[,"(\\d{3})(\\d{3})(\\d{3,4})","$1 $2 $3",["[89]"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],KG:[,[,,"[35-8]\\d{8,9}","\\d{5,10}"],[,,"(?:3(?:1(?:2\\d|3[1-9]|47|5[02]|6[1-8])|2(?:22|3[0-479]|6[0-7])|4(?:22|5[6-9]|6[0-4])|5(?:22|3[4-7]|59|6[0-5])|6(?:22|5[35-7]|6[0-3])|7(?:22|3[468]|4[1-9]|59|6\\d|7[5-7])|9(?:22|4[1-8]|6[0-8]))|6(?:09|12|2[2-4])\\d)\\d{5}","\\d{5,10}",,,"312123456"],[,,"5[124-7]\\d{7}|7(?:0[0-357-9]|7\\d)\\d{6}",
"\\d{9}",,,"700123456"],[,,"800\\d{6,7}","\\d{9,10}",,,"800123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"KG",996,"00","0",,,"0",,,,[[,"(\\d{3})(\\d{3})(\\d{3})","$1 $2 $3",["31[25]|[5-7]"],"0$1","",0],[,"(\\d{4})(\\d{5})","$1 $2",["3(?:1[36]|[2-9])"],"0$1","",0],[,"(\\d{3})(\\d{3})(\\d)(\\d{3})","$1 $2 $3 $4",["8"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],KH:[,[,,"[1-9]\\d{7,9}","\\d{6,10}"],[,,"(?:2[3-6]|3[2-6]|4[2-4]|[5-7][2-5])(?:[237-9]|4[56]|5\\d|6\\d?)\\d{5}|238\\d{6}",
"\\d{6,9}",,,"23756789"],[,,"(?:1(?:[013-9]|2\\d?)|31\\d|6[016-9]|7(?:[07-9]|6\\d)|8(?:[013-79]|8\\d)|9(?:6\\d|7\\d?|[0-589]))\\d{6}","\\d{8,9}",,,"91234567"],[,,"1800(?:1\\d|2[019])\\d{4}","\\d{10}",,,"1800123456"],[,,"1900(?:1\\d|2[09])\\d{4}","\\d{10}",,,"1900123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"KH",855,"00[14-9]","0",,,"0",,,,[[,"(\\d{2})(\\d{3})(\\d{3,4})","$1 $2 $3",["1\\d[1-9]|[2-9]"],"0$1","",0],[,"(1[89]00)(\\d{3})(\\d{3})","$1 $2 $3",["1[89]0"],"","",0]],,[,,"NA","NA"],,,
[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],KI:[,[,,"[2-58]\\d{4}|7\\d{7}","\\d{5,8}"],[,,"(?:[24]\\d|3[1-9]|50|8[0-5])\\d{3}","\\d{5}",,,"31234"],[,,"7(?:[24]\\d|3[1-9]|8[0-5])\\d{5}","\\d{8}",,,"72012345"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"KI",686,"00",,,,"0",,,,,,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],KM:[,[,,"[379]\\d{6}","\\d{7}"],[,,"7(?:6[0-37-9]|7[0-57-9])\\d{4}","\\d{7}",,,"7712345"],[,,"3[234]\\d{5}","\\d{7}",,,"3212345"],[,,"NA",
"NA"],[,,"(?:39[01]|9[01]0)\\d{4}","\\d{7}",,,"9001234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"KM",269,"00",,,,,,,,[[,"(\\d{3})(\\d{2})(\\d{2})","$1 $2 $3",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],KN:[,[,,"[589]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"869(?:2(?:29|36)|302|4(?:6[5-9]|70))\\d{4}","\\d{7}(?:\\d{3})?",,,"8692361234"],[,,"869(?:5(?:5[6-8]|6[5-7])|66\\d|76[02-6])\\d{4}","\\d{10}",,,"8695561234"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002123456"],
[,,"900[2-9]\\d{6}","\\d{10}",,,"9002123456"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"KN",1,"011","1",,,"1",,,,,,[,,"NA","NA"],,"869",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],KP:[,[,,"1\\d{9}|[28]\\d{7}","\\d{6,8}|\\d{10}"],[,,"2\\d{7}|85\\d{6}","\\d{6,8}",,,"21234567"],[,,"19[123]\\d{7}","\\d{10}",,,"1921234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"KP",850,"00|99","0",,,"0",,,,[[,"(\\d{3})(\\d{3})(\\d{4})","$1 $2 $3",
["1"],"0$1","",0],[,"(\\d)(\\d{3})(\\d{4})","$1 $2 $3",["2"],"0$1","",0],[,"(\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["8"],"0$1","",0]],,[,,"NA","NA"],,,[,,"2(?:[0-24-9]\\d{2}|3(?:[0-79]\\d|8[02-9]))\\d{4}","\\d{8}",,,"23821234"],[,,"NA","NA"],,,[,,"NA","NA"]],KR:[,[,,"[1-7]\\d{3,9}|8\\d{8}","\\d{4,10}"],[,,"(?:2|3[1-3]|[46][1-4]|5[1-5])(?:1\\d{2,3}|[1-9]\\d{6,7})","\\d{4,10}",,,"22123456"],[,,"1[0-26-9]\\d{7,8}","\\d{9,10}",,,"1023456789"],[,,"80\\d{7}","\\d{9}",,,"801234567"],[,,"60[2-9]\\d{6}","\\d{9}",
,,"602345678"],[,,"NA","NA"],[,,"50\\d{8}","\\d{10}",,,"5012345678"],[,,"70\\d{8}","\\d{10}",,,"7012345678"],"KR",82,"00(?:[124-68]|[37]\\d{2})","0",,,"0(8[1-46-8]|85\\d{2})?",,,,[[,"(\\d{2})(\\d{4})(\\d{4})","$1-$2-$3",["1(?:0|1[19]|[69]9|5[458])|[57]0","1(?:0|1[19]|[69]9|5(?:44|59|8))|[57]0"],"0$1","0$CC-$1",0],[,"(\\d{2})(\\d{3,4})(\\d{4})","$1-$2-$3",["1(?:[169][2-8]|[78]|5[1-4])|[68]0|[3-6][1-9][1-9]","1(?:[169][2-8]|[78]|5(?:[1-3]|4[56]))|[68]0|[3-6][1-9][1-9]"],"0$1","0$CC-$1",0],[,"(\\d{3})(\\d)(\\d{4})",
"$1-$2-$3",["131","1312"],"0$1","0$CC-$1",0],[,"(\\d{3})(\\d{2})(\\d{4})","$1-$2-$3",["131","131[13-9]"],"0$1","0$CC-$1",0],[,"(\\d{3})(\\d{3})(\\d{4})","$1-$2-$3",["13[2-9]"],"0$1","0$CC-$1",0],[,"(\\d{2})(\\d{2})(\\d{3})(\\d{4})","$1-$2-$3-$4",["30"],"0$1","0$CC-$1",0],[,"(\\d)(\\d{3,4})(\\d{4})","$1-$2-$3",["2[1-9]"],"0$1","0$CC-$1",0],[,"(\\d)(\\d{3,4})","$1-$2",["21[0-46-9]"],"0$1","0$CC-$1",0],[,"(\\d{2})(\\d{3,4})","$1-$2",["[3-6][1-9]1","[3-6][1-9]1(?:[0-46-9])"],"0$1","0$CC-$1",0],[,"(\\d{4})(\\d{4})",
"$1-$2",["1(?:5[46-9]|6[04678])","1(?:5(?:44|66|77|88|99)|6(?:00|44|6[16]|70|88))"],"$1","0$CC-$1",0]],,[,,"15\\d{7,8}","\\d{9,10}",,,"1523456789"],,,[,,"NA","NA"],[,,"1(?:5(?:44|66|77|88|99)|6(?:00|44|6[16]|70|88))\\d{4}","\\d{8}",,,"15441234"],,,[,,"NA","NA"]],KW:[,[,,"[12569]\\d{6,7}","\\d{7,8}"],[,,"(?:18\\d|2(?:[23]\\d{2}|4(?:[1-35-9]\\d|44)|5(?:0[034]|[2-46]\\d|5[1-3]|7[1-7])))\\d{4}","\\d{7,8}",,,"22345678"],[,,"(?:5(?:1[0-5]|[05]\\d)|6(?:0[034679]|5[015-9]|6\\d|7[067]|9[0369])|9(?:0[09]|4[049]|6[069]|[79]\\d|8[08]))\\d{5}",
"\\d{8}",,,"50012345"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"KW",965,"00",,,,,,,,[[,"(\\d{4})(\\d{3,4})","$1 $2",["[1269]"],"","",0],[,"(5[015]\\d)(\\d{5})","$1 $2",["5"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],KY:[,[,,"[3589]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"345(?:2(?:22|44)|444|6(?:23|38|40)|7(?:4[35-79]|6[6-9]|77)|8(?:00|1[45]|25|[48]8)|9(?:14|4[035-9]))\\d{4}","\\d{7}(?:\\d{3})?",,,"3452221234"],[,,"345(?:32[1-9]|5(?:1[67]|2[5-7]|4[6-8]|76)|9(?:1[67]|2[3-9]|3[689]))\\d{4}",
"\\d{10}",,,"3453231234"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002345678"],[,,"900[2-9]\\d{6}|345976\\d{4}","\\d{10}",,,"9002345678"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"KY",1,"011","1",,,"1",,,,,,[,,"345849\\d{4}","\\d{10}",,,"3458491234"],,"345",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],KZ:[,[,,"(?:33\\d|7\\d{2}|80[09])\\d{7}","\\d{10}"],[,,"33622\\d{5}|7(?:1(?:0(?:[23]\\d|4[023]|59|63)|1(?:[23]\\d|4[0-79]|59)|2(?:[23]\\d|59)|3(?:2\\d|3[1-79]|4[0-35-9]|59)|4(?:2\\d|3[013-79]|4[0-8]|5[1-79])|5(?:2\\d|3[1-8]|4[1-7]|59)|6(?:[234]\\d|5[19]|61)|72\\d|8(?:[27]\\d|3[1-46-9]|4[0-5]))|2(?:1(?:[23]\\d|4[46-9]|5[3469])|2(?:2\\d|3[0679]|46|5[12679])|3(?:[234]\\d|5[139])|4(?:2\\d|3[1235-9]|59)|5(?:[23]\\d|4[01246-8]|59|61)|6(?:2\\d|3[1-9]|4[0-4]|59)|7(?:[237]\\d|40|5[279])|8(?:[23]\\d|4[0-3]|59)|9(?:2\\d|3[124578]|59)))\\d{5}",
"\\d{10}",,,"7123456789"],[,,"7(?:0[01257]|47|6[02-4]|7[15-8]|85)\\d{7}","\\d{10}",,,"7710009998"],[,,"800\\d{7}","\\d{10}",,,"8001234567"],[,,"809\\d{7}","\\d{10}",,,"8091234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"751\\d{7}","\\d{10}",,,"7511234567"],"KZ",7,"810","8",,,"8",,"8~10",,,,[,,"NA","NA"],,,[,,"751\\d{7}","\\d{10}",,,"7511234567"],[,,"NA","NA"],,,[,,"NA","NA"]],LA:[,[,,"[2-8]\\d{7,9}","\\d{6,10}"],[,,"(?:2[13]|[35-7][14]|41|8[1468])\\d{6}","\\d{6,8}",,,"21212862"],[,,"20(?:2[2389]|5[4-689]|7[6-8]|9[57-9])\\d{6}",
"\\d{10}",,,"2023123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"LA",856,"00","0",,,"0",,,,[[,"(20)(\\d{2})(\\d{3})(\\d{3})","$1 $2 $3 $4",["20"],"0$1","",0],[,"([2-8]\\d)(\\d{3})(\\d{3})","$1 $2 $3",["2[13]|[3-8]"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],LB:[,[,,"[13-9]\\d{6,7}","\\d{7,8}"],[,,"(?:[14-6]\\d{2}|7(?:[2-579]\\d|62|8[0-7])|[89][2-9]\\d)\\d{4}","\\d{7}",,,"1123456"],[,,"(?:3\\d|7(?:[01]\\d|6[013-9]|8[89]|91))\\d{5}","\\d{7,8}",
,,"71123456"],[,,"NA","NA"],[,,"9[01]\\d{6}","\\d{8}",,,"90123456"],[,,"8[01]\\d{6}","\\d{8}",,,"80123456"],[,,"NA","NA"],[,,"NA","NA"],"LB",961,"00","0",,,"0",,,,[[,"(\\d)(\\d{3})(\\d{3})","$1 $2 $3",["[13-6]|7(?:[2-579]|62|8[0-7])|[89][2-9]"],"0$1","",0],[,"([7-9]\\d)(\\d{3})(\\d{3})","$1 $2 $3",["[89][01]|7(?:[01]|6[013-9]|8[89]|91)"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],LC:[,[,,"[5789]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"758(?:234|4(?:30|5[0-9]|6[2-9]|8[0-2])|572|638|758)\\d{4}",
"\\d{7}(?:\\d{3})?",,,"7582345678"],[,,"758(?:28[4-7]|384|4(?:6[01]|8[4-9])|5(?:1[89]|20|84)|7(?:1[2-9]|2[0-6]))\\d{4}","\\d{10}",,,"7582845678"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002123456"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002123456"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"LC",1,"011","1",,,"1",,,,,,[,,"NA","NA"],,"758",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],LI:[,[,,"6\\d{8}|[23789]\\d{6}","\\d{7,9}"],[,,"(?:2(?:01|1[27]|3\\d|6[02-578]|96)|3(?:7[0135-7]|8[048]|9[0269]))\\d{4}",
"\\d{7}",,,"2345678"],[,,"6(?:51[01]|6(?:[01][0-4]|2[016-9]|88)|710)\\d{5}|7(?:36|4[25]|56|[7-9]\\d)\\d{4}","\\d{7,9}",,,"661234567"],[,,"80(?:0(?:2[238]|79)|9\\d{2})\\d{2}","\\d{7}",,,"8002222"],[,,"90(?:0(?:2[278]|79)|1(?:23|3[012])|6(?:4\\d|6[0126]))\\d{2}","\\d{7}",,,"9002222"],[,,"NA","NA"],[,,"701\\d{4}","\\d{7}",,,"7011234"],[,,"NA","NA"],"LI",423,"00","0",,,"0",,,,[[,"(\\d{3})(\\d{2})(\\d{2})","$1 $2 $3",["[23]|7[3-57-9]|87"],"","",0],[,"(6\\d)(\\d{3})(\\d{3})","$1 $2 $3",["6"],"","",0],[,
"(6[567]\\d)(\\d{3})(\\d{3})","$1 $2 $3",["6[567]"],"","",0],[,"(69)(7\\d{2})(\\d{4})","$1 $2 $3",["697"],"","",0],[,"([7-9]0\\d)(\\d{2})(\\d{2})","$1 $2 $3",["[7-9]0"],"","",0],[,"([89]0\\d)(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["[89]0"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"87(?:0[128]|7[0-4])\\d{3}","\\d{7}",,,"8770123"],,,[,,"697(?:[35]6|4[25]|[7-9]\\d)\\d{4}","\\d{9}",,,"697361234"]],LK:[,[,,"[1-9]\\d{8}","\\d{7,9}"],[,,"(?:[189]1|2[13-7]|3[1-8]|4[157]|5[12457]|6[35-7])[2-57]\\d{6}",
"\\d{7,9}",,,"112345678"],[,,"7[125-8]\\d{7}","\\d{9}",,,"712345678"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"LK",94,"00","0",,,"0",,,,[[,"(\\d{2})(\\d{1})(\\d{6})","$1 $2 $3",["[1-689]"],"0$1","",0],[,"(\\d{2})(\\d{3})(\\d{4})","$1 $2 $3",["7"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],LR:[,[,,"(?:[29]\\d|[4-6]|7\\d{1,2}|[38]\\d{2})\\d{6}","\\d{7,9}"],[,,"2\\d{7}","\\d{8}",,,"21234567"],[,,"(?:4[67]|5\\d|6[4-8]|77?\\d{2}|88\\d{2})\\d{5}",
"\\d{7,9}",,,"4612345"],[,,"NA","NA"],[,,"90\\d{6}","\\d{8}",,,"90123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"33200\\d{4}","\\d{9}",,,"332001234"],"LR",231,"00","0",,,"0",,,,[[,"([279]\\d)(\\d{3})(\\d{3})","$1 $2 $3",["[279]"],"0$1","",0],[,"(7\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["7"],"0$1","",0],[,"([4-6])(\\d{3})(\\d{3})","$1 $2 $3",["[4-6]"],"0$1","",0],[,"(\\d{2})(\\d{3})(\\d{4})","$1 $2 $3",["[38]"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],LS:[,[,,"[2568]\\d{7}",
"\\d{8}"],[,,"2\\d{7}","\\d{8}",,,"22123456"],[,,"[56]\\d{7}","\\d{8}",,,"50123456"],[,,"800[256]\\d{4}","\\d{8}",,,"80021234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"LS",266,"00",,,,,,,,[[,"(\\d{4})(\\d{4})","$1 $2",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],LT:[,[,,"[3-9]\\d{7}","\\d{8}"],[,,"(?:3[1478]|4[124-6]|52)\\d{6}","\\d{8}",,,"31234567"],[,,"6\\d{7}","\\d{8}",,,"61234567"],[,,"800\\d{5}","\\d{8}",,,"80012345"],[,,"9(?:0[0239]|10)\\d{5}","\\d{8}",
,,"90012345"],[,,"808\\d{5}","\\d{8}",,,"80812345"],[,,"700\\d{5}","\\d{8}",,,"70012345"],[,,"NA","NA"],"LT",370,"00","8",,,"[08]",,,,[[,"([34]\\d)(\\d{6})","$1 $2",["37|4(?:1|5[45]|6[2-4])"],"(8-$1)","",1],[,"([3-6]\\d{2})(\\d{5})","$1 $2",["3[148]|4(?:[24]|6[09])|528|6"],"(8-$1)","",1],[,"([7-9]\\d{2})(\\d{2})(\\d{3})","$1 $2 $3",["[7-9]"],"8 $1","",1],[,"(5)(2\\d{2})(\\d{4})","$1 $2 $3",["52[0-79]"],"(8-$1)","",1]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"70[67]\\d{5}","\\d{8}",,,"70712345"],,,[,,"NA",
"NA"]],LU:[,[,,"[24-9]\\d{3,10}|3(?:[0-46-9]\\d{2,9}|5[013-9]\\d{1,8})","\\d{4,11}"],[,,"(?:2(?:2\\d{1,2}|3[2-9]|[67]\\d|4[1-8]\\d?|5[1-5]\\d?|9[0-24-9]\\d?)|3(?:[059][05-9]|[13]\\d|[26][015-9]|4[0-26-9]|7[0-389]|8[08])\\d?|4\\d{2,3}|5(?:[01458]\\d|[27][0-69]|3[0-3]|[69][0-7])\\d?|7(?:1[019]|2[05-9]|3[05]|[45][07-9]|[679][089]|8[06-9])\\d?|8(?:0[2-9]|1[0-36-9]|3[3-9]|[469]9|[58][7-9]|7[89])\\d?|9(?:0[89]|2[0-49]|37|49|5[0-27-9]|7[7-9]|9[0-478])\\d?)\\d{1,7}","\\d{4,11}",,,"27123456"],[,,"6[269][18]\\d{6}",
"\\d{9}",,,"628123456"],[,,"800\\d{5}","\\d{8}",,,"80012345"],[,,"90[01]\\d{5}","\\d{8}",,,"90012345"],[,,"801\\d{5}","\\d{8}",,,"80112345"],[,,"70\\d{6}","\\d{8}",,,"70123456"],[,,"20(?:1\\d{5}|[2-689]\\d{1,7})","\\d{4,10}",,,"20201234"],"LU",352,"00",,,,"(15(?:0[06]|1[12]|35|4[04]|55|6[26]|77|88|99)\\d)",,,,[[,"(\\d{2})(\\d{3})","$1 $2",["[2-5]|7[1-9]|[89](?:[1-9]|0[2-9])"],"","$CC $1",0],[,"(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3",["[2-5]|7[1-9]|[89](?:[1-9]|0[2-9])"],"","$CC $1",0],[,"(\\d{2})(\\d{2})(\\d{3})",
"$1 $2 $3",["20"],"","$CC $1",0],[,"(\\d{2})(\\d{2})(\\d{2})(\\d{1,2})","$1 $2 $3 $4",["2(?:[0367]|4[3-8])"],"","$CC $1",0],[,"(\\d{2})(\\d{2})(\\d{2})(\\d{3})","$1 $2 $3 $4",["20"],"","$CC $1",0],[,"(\\d{2})(\\d{2})(\\d{2})(\\d{2})(\\d{1,2})","$1 $2 $3 $4 $5",["2(?:[0367]|4[3-8])"],"","$CC $1",0],[,"(\\d{2})(\\d{2})(\\d{2})(\\d{1,4})","$1 $2 $3 $4",["2(?:[12589]|4[12])|[3-5]|7[1-9]|[89](?:[1-9]|0[2-9])"],"","$CC $1",0],[,"(\\d{3})(\\d{2})(\\d{3})","$1 $2 $3",["[89]0[01]|70"],"","$CC $1",0],[,"(\\d{3})(\\d{3})(\\d{3})",
"$1 $2 $3",["6"],"","$CC $1",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],LV:[,[,,"[2689]\\d{7}","\\d{8}"],[,,"6[3-8]\\d{6}","\\d{8}",,,"63123456"],[,,"2\\d{7}","\\d{8}",,,"21234567"],[,,"80\\d{6}","\\d{8}",,,"80123456"],[,,"90\\d{6}","\\d{8}",,,"90123456"],[,,"81\\d{6}","\\d{8}",,,"81123456"],[,,"NA","NA"],[,,"NA","NA"],"LV",371,"00",,,,,,,,[[,"([2689]\\d)(\\d{3})(\\d{3})","$1 $2 $3",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],LY:[,[,,"[25679]\\d{8}",
"\\d{7,9}"],[,,"(?:2[1345]|5[1347]|6[123479]|71)\\d{7}","\\d{7,9}",,,"212345678"],[,,"9[1-6]\\d{7}","\\d{9}",,,"912345678"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"LY",218,"00","0",,,"0",,,,[[,"([25679]\\d)(\\d{7})","$1-$2",,"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],MA:[,[,,"[5689]\\d{8}","\\d{9}"],[,,"5(?:2(?:(?:[015-7]\\d|2[2-9]|3[2-57]|4[2-8]|8[235-7])\\d|9(?:0\\d|[89]0))|3(?:(?:[0-4]\\d|[57][2-9]|6[235-8]|9[3-9])\\d|8(?:0\\d|[89]0)))\\d{4}",
"\\d{9}",,,"520123456"],[,,"6(?:0[0-8]|[12-7]\\d|8[01]|9[27-9])\\d{6}","\\d{9}",,,"650123456"],[,,"80\\d{7}","\\d{9}",,,"801234567"],[,,"89\\d{7}","\\d{9}",,,"891234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"MA",212,"00","0",,,"0",,,,[[,"([56]\\d{2})(\\d{6})","$1-$2",["5(?:2[015-7]|3[0-4])|6"],"0$1","",0],[,"([58]\\d{3})(\\d{5})","$1-$2",["5(?:2[2-489]|3[5-9])|892","5(?:2(?:[2-48]|90)|3(?:[5-79]|80))|892"],"0$1","",0],[,"(5\\d{4})(\\d{4})","$1-$2",["5(?:29|38)","5(?:29|38)[89]"],"0$1","",0],
[,"(8[09])(\\d{7})","$1-$2",["8(?:0|9[013-9])"],"0$1","",0]],,[,,"NA","NA"],1,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],MC:[,[,,"[4689]\\d{7,8}","\\d{8,9}"],[,,"9[2-47-9]\\d{6}","\\d{8}",,,"99123456"],[,,"6\\d{8}|4\\d{7}","\\d{8,9}",,,"612345678"],[,,"(?:8\\d|90)\\d{6}","\\d{8}",,,"90123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"MC",377,"00","0",,,"0",,,,[[,"(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["[89]"],"$1","",0],[,"(\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["4"],"0$1",
"",0],[,"(6)(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4 $5",["6"],"0$1","",0]],,[,,"NA","NA"],,,[,,"8\\d{7}","\\d{8}"],[,,"NA","NA"],,,[,,"NA","NA"]],MD:[,[,,"[235-9]\\d{7}","\\d{8}"],[,,"(?:2(?:1[0569]|2\\d|3[015-7]|4[1-46-9]|5[0-24689]|6[2-589]|7[1-37]|9[1347-9])|5(?:33|5[257]))\\d{5}","\\d{8}",,,"22212345"],[,,"(?:562|6(?:50|7[1-6]|[089]\\d)|7(?:67|7[457-9]|[89]\\d))\\d{5}","\\d{8}",,,"65012345"],[,,"800\\d{5}","\\d{8}",,,"80012345"],[,,"90[056]\\d{5}","\\d{8}",,,"90012345"],[,,"808\\d{5}",
"\\d{8}",,,"80812345"],[,,"NA","NA"],[,,"3[08]\\d{6}","\\d{8}",,,"30123456"],"MD",373,"00","0",,,"0",,,,[[,"(\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["22|3"],"0$1","",0],[,"([25-7]\\d{2})(\\d{2})(\\d{3})","$1 $2 $3",["2[13-79]|[5-7]"],"0$1","",0],[,"([89]\\d{2})(\\d{5})","$1 $2",["[89]"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"8(?:03|14)\\d{5}","\\d{8}",,,"80312345"],,,[,,"NA","NA"]],ME:[,[,,"[2-9]\\d{7,8}","\\d{6,9}"],[,,"(?:20[2-8]|3(?:0[2-7]|1[35-7]|2[3567]|3[4-7])|4(?:0[237]|1[27])|5(?:0[47]|1[27]|2[378]))\\d{5}",
"\\d{6,8}",,,"30234567"],[,,"6(?:32\\d|[89]\\d{2}|7(?:[0-8]\\d|9(?:[3-9]|[0-2]\\d)))\\d{4}","\\d{8,9}",,,"67622901"],[,,"800[28]\\d{4}","\\d{8}",,,"80080002"],[,,"(?:88\\d|9(?:4[13-8]|5[16-8]))\\d{5}","\\d{8}",,,"94515151"],[,,"NA","NA"],[,,"NA","NA"],[,,"78[1-9]\\d{5}","\\d{8}",,,"78108780"],"ME",382,"00","0",,,"0",,,,[[,"(\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["[2-57-9]|6[3789]","[2-57-9]|6(?:[389]|7(?:[0-8]|9[3-9]))"],"0$1","",0],[,"(67)(9)(\\d{3})(\\d{3})","$1 $2 $3 $4",["679","679[0-2]"],"0$1",
"",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"77\\d{6}","\\d{8}",,,"77273012"],,,[,,"NA","NA"]],MF:[,[,,"[56]\\d{8}","\\d{9}"],[,,"590(?:[02][79]|13|5[0-268]|[78]7)\\d{4}","\\d{9}",,,"590271234"],[,,"690(?:0[0-7]|[1-9]\\d)\\d{4}","\\d{9}",,,"690301234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"MF",590,"00","0",,,"0",,,,,,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],MG:[,[,,"[23]\\d{8}","\\d{7,9}"],[,,"20(?:2\\d{2}|4[47]\\d|5[3467]\\d|6[279]\\d|7(?:2[29]|[35]\\d)|8[268]\\d|9[245]\\d)\\d{4}",
"\\d{7,9}",,,"202123456"],[,,"3[2-49]\\d{7}","\\d{9}",,,"321234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"22\\d{7}","\\d{9}",,,"221234567"],"MG",261,"00","0",,,"0",,,,[[,"([23]\\d)(\\d{2})(\\d{3})(\\d{2})","$1 $2 $3 $4",,"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],MH:[,[,,"[2-6]\\d{6}","\\d{7}"],[,,"(?:247|528|625)\\d{4}","\\d{7}",,,"2471234"],[,,"(?:235|329|45[56]|545)\\d{4}","\\d{7}",,,"2351234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,
"NA","NA"],[,,"635\\d{4}","\\d{7}",,,"6351234"],"MH",692,"011","1",,,"1",,,,[[,"(\\d{3})(\\d{4})","$1-$2",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],MK:[,[,,"[2-578]\\d{7}","\\d{8}"],[,,"(?:2(?:[23]\\d|5[124578]|6[01])|3(?:1[3-6]|[23][2-6]|4[2356])|4(?:[23][2-6]|4[3-6]|5[256]|6[25-8]|7[24-6]|8[4-6]))\\d{5}","\\d{6,8}",,,"22212345"],[,,"7(?:[0-25-8]\\d{2}|32\\d|421)\\d{4}","\\d{8}",,,"72345678"],[,,"800\\d{5}","\\d{8}",,,"80012345"],[,,"5[02-9]\\d{6}","\\d{8}",,,"50012345"],
[,,"8(?:0[1-9]|[1-9]\\d)\\d{5}","\\d{8}",,,"80123456"],[,,"NA","NA"],[,,"NA","NA"],"MK",389,"00","0",,,"0",,,,[[,"(2)(\\d{3})(\\d{4})","$1 $2 $3",["2"],"0$1","",0],[,"([347]\\d)(\\d{3})(\\d{3})","$1 $2 $3",["[347]"],"0$1","",0],[,"([58]\\d{2})(\\d)(\\d{2})(\\d{2})","$1 $2 $3 $4",["[58]"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],ML:[,[,,"[246-9]\\d{7}","\\d{8}"],[,,"(?:2(?:0(?:2[0-589]|7\\d)|1(?:2[5-7]|[3-689]\\d|7[2-4689]))|44[239]\\d)\\d{4}","\\d{8}",,,"20212345"],
[,,"[67]\\d{7}|9[0-25-9]\\d{6}","\\d{8}",,,"65012345"],[,,"800\\d{5}","\\d{8}",,,"80012345"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"ML",223,"00",,,,,,,,[[,"(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["[246-9]"],"","",0],[,"(\\d{4})","$1",["67|74"],"","",0]],[[,"(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["[246-9]","[246-9]"],"","",0]],[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],MM:[,[,,"[14578]\\d{5,7}|[26]\\d{5,8}|9(?:2\\d{0,2}|[58]|3\\d|4\\d{1,2}|[679]\\d?)\\d{6}",
"\\d{5,10}"],[,,"1(?:2\\d{1,2}|[3-5]\\d|6\\d?|[89][0-6]\\d)\\d{4}|2(?:[236-9]\\d{4}|4(?:0\\d{5}|\\d{4})|5(?:1\\d{3,6}|[02-9]\\d{3,5}))|4(?:2[245-8]|[346][2-6]|5[3-5])\\d{4}|5(?:2(?:20?|[3-8])|3[2-68]|4(?:21?|[4-8])|5[23]|6[2-4]|7[2-8]|8[24-7]|9[2-7])\\d{4}|6(?:0[23]|1[2356]|[24][2-6]|3[24-6]|5[2-4]|6[2-8]|7(?:[2367]|4\\d|5\\d?|8[145]\\d)|8[245]|9[24])\\d{4}|7(?:[04][24-8]|[15][2-7]|22|3[2-4])\\d{4}|8(?:1(?:2\\d?|[3-689])|2[2-8]|3[24]|4[24-7]|5[245]|6[23])\\d{4}","\\d{5,9}",,,"1234567"],[,,"17[01]\\d{4}|9(?:2(?:[0-4]|5\\d{2})|3[136]\\d|4(?:0[0-4]\\d|[1379]\\d|[24][0-589]\\d|5\\d{2}|88)|5[0-6]|61?\\d|73\\d|8\\d|9(?:1\\d|[089]))\\d{5}",
"\\d{7,10}",,,"92123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"1333\\d{4}","\\d{8}",,,"13331234"],"MM",95,"00","0",,,"0",,,,[[,"(\\d)(\\d{3})(\\d{3,4})","$1 $2 $3",["1|2[45]"],"0$1","",0],[,"(2)(\\d{4})(\\d{4})","$1 $2 $3",["251"],"0$1","",0],[,"(\\d)(\\d{2})(\\d{3})","$1 $2 $3",["16|2"],"0$1","",0],[,"(\\d{2})(\\d{3})(\\d{3,4})","$1 $2 $3",["67|81"],"0$1","",0],[,"(\\d{2})(\\d{2})(\\d{3,4})","$1 $2 $3",["[4-8]"],"0$1","",0],[,"(9)(\\d{3})(\\d{4,5})","$1 $2 $3",["9(?:2[0-4]|[35-9]|4[13789])"],
"0$1","",0],[,"(9)(4\\d{4})(\\d{4})","$1 $2 $3",["94[0245]"],"0$1","",0],[,"(9)(\\d{3})(\\d{3})(\\d{3})","$1 $2 $3 $4",["925"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],MN:[,[,,"[12]\\d{7,9}|[57-9]\\d{7}","\\d{6,10}"],[,,"[12](?:1\\d|2(?:[1-3]\\d?|7\\d)|3[2-8]\\d{1,2}|4[2-68]\\d{1,2}|5[1-4689]\\d{1,2})\\d{5}|5[0568]\\d{6}","\\d{6,10}",,,"50123456"],[,,"(?:8[89]|9[013-9])\\d{6}","\\d{8}",,,"88123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"7[05-8]\\d{6}",
"\\d{8}",,,"75123456"],"MN",976,"001","0",,,"0",,,,[[,"([12]\\d)(\\d{2})(\\d{4})","$1 $2 $3",["[12]1"],"0$1","",0],[,"([12]2\\d)(\\d{5,6})","$1 $2",["[12]2[1-3]"],"0$1","",0],[,"([12]\\d{3})(\\d{5})","$1 $2",["[12](?:27|[3-5])","[12](?:27|[3-5]\\d)2"],"0$1","",0],[,"(\\d{4})(\\d{4})","$1 $2",["[57-9]"],"$1","",0],[,"([12]\\d{4})(\\d{4,5})","$1 $2",["[12](?:27|[3-5])","[12](?:27|[3-5]\\d)[4-9]"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],MO:[,[,,"[268]\\d{7}","\\d{8}"],
[,,"(?:28[2-57-9]|8[2-57-9]\\d)\\d{5}","\\d{8}",,,"28212345"],[,,"6[236]\\d{6}","\\d{8}",,,"66123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"MO",853,"00",,,,,,,,[[,"([268]\\d{3})(\\d{4})","$1 $2",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],MP:[,[,,"[5689]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"670(?:2(?:3[3-7]|56|8[5-8])|32[1238]|4(?:33|8[348])|5(?:32|55|88)|6(?:64|70|82)|78[589]|8[3-9]8|989)\\d{4}","\\d{7}(?:\\d{3})?",,,"6702345678"],[,,"670(?:2(?:3[3-7]|56|8[5-8])|32[1238]|4(?:33|8[348])|5(?:32|55|88)|6(?:64|70|82)|78[589]|8[3-9]8|989)\\d{4}",
"\\d{7}(?:\\d{3})?",,,"6702345678"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002123456"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002123456"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"MP",1,"011","1",,,"1",,,1,,,[,,"NA","NA"],,"670",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],MQ:[,[,,"[56]\\d{8}","\\d{9}"],[,,"596(?:0[2-5]|[12]0|3[05-9]|4[024-8]|[5-7]\\d|89|9[4-8])\\d{4}","\\d{9}",,,"596301234"],[,,"696(?:[0-479]\\d|5[01]|8[0-689])\\d{4}","\\d{9}",
,,"696201234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"MQ",596,"00","0",,,"0",,,,[[,"(\\d{3})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",,"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],MR:[,[,,"[2-48]\\d{7}","\\d{8}"],[,,"25[08]\\d{5}|35\\d{6}|45[1-7]\\d{5}","\\d{8}",,,"35123456"],[,,"(?:2(?:2\\d|70)|3(?:3\\d|6[1-36]|7[1-3])|4(?:4\\d|6[0457-9]|7[4-9]|8[01346-8]))\\d{5}","\\d{8}",,,"22123456"],[,,"800\\d{5}","\\d{8}",,,"80012345"],[,,"NA","NA"],
[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"MR",222,"00",,,,,,,,[[,"([2-48]\\d)(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],MS:[,[,,"[5689]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"664491\\d{4}","\\d{7}(?:\\d{3})?",,,"6644912345"],[,,"66449[2-6]\\d{4}","\\d{10}",,,"6644923456"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002123456"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002123456"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",
,,"5002345678"],[,,"NA","NA"],"MS",1,"011","1",,,"1",,,,,,[,,"NA","NA"],,"664",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],MT:[,[,,"[2357-9]\\d{7}","\\d{8}"],[,,"2(?:0(?:1[0-6]|3[1-4]|[69]\\d)|[1-357]\\d{2})\\d{4}","\\d{8}",,,"21001234"],[,,"(?:7(?:210|[79]\\d{2})|9(?:2(?:1[01]|31)|696|8(?:1[1-3]|89|97)|9\\d{2}))\\d{4}","\\d{8}",,,"96961234"],[,,"800[3467]\\d{4}","\\d{8}",,,"80071234"],[,,"5(?:0(?:0(?:37|43)|6\\d{2}|70\\d|9[0168])|[12]\\d0[1-5])\\d{3}","\\d{8}",,,"50037123"],[,,"NA","NA"],[,,"NA",
"NA"],[,,"3550\\d{4}","\\d{8}",,,"35501234"],"MT",356,"00",,,,,,,,[[,"(\\d{4})(\\d{4})","$1 $2",,"","",0]],,[,,"7117\\d{4}","\\d{8}",,,"71171234"],,,[,,"NA","NA"],[,,"501\\d{5}","\\d{8}",,,"50112345"],,,[,,"NA","NA"]],MU:[,[,,"[2-9]\\d{6,7}","\\d{7,8}"],[,,"(?:2(?:[03478]\\d|1[0-7]|6[1-69])|4(?:[013568]\\d|2[4-7])|5(44\\d|471)|6\\d{2}|8(?:14|3[129]))\\d{4}","\\d{7,8}",,,"2012345"],[,,"5(?:2[59]\\d|4(?:2[1-389]|4\\d|7[1-9]|9\\d)|7\\d{2}|8(?:[256]\\d|7[15-8])|9[0-8]\\d)\\d{4}","\\d{8}",,,"52512345"],
[,,"80[012]\\d{4}","\\d{7}",,,"8001234"],[,,"30\\d{5}","\\d{7}",,,"3012345"],[,,"NA","NA"],[,,"NA","NA"],[,,"3(?:20|9\\d)\\d{4}","\\d{7}",,,"3201234"],"MU",230,"0(?:0|[2-7]0|33)",,,,,,"020",,[[,"([2-46-9]\\d{2})(\\d{4})","$1 $2",["[2-46-9]"],"","",0],[,"(5\\d{3})(\\d{4})","$1 $2",["5"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],MV:[,[,,"[3467]\\d{6}|9(?:00\\d{7}|\\d{6})","\\d{7,10}"],[,,"(?:3(?:0[01]|3[0-59])|6(?:[567][02468]|8[024689]|90))\\d{4}","\\d{7}",,,"6701234"],
[,,"(?:46[46]|7[3-9]\\d|9[16-9]\\d)\\d{4}","\\d{7}",,,"7712345"],[,,"NA","NA"],[,,"900\\d{7}","\\d{10}",,,"9001234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"MV",960,"0(?:0|19)",,,,,,"00",,[[,"(\\d{3})(\\d{4})","$1-$2",["[3467]|9(?:[1-9]|0[1-9])"],"","",0],[,"(\\d{3})(\\d{3})(\\d{4})","$1 $2 $3",["900"],"","",0]],,[,,"781\\d{4}","\\d{7}",,,"7812345"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],MW:[,[,,"(?:1(?:\\d{2})?|[2789]\\d{2})\\d{6}","\\d{7,9}"],[,,"(?:1[2-9]|21\\d{2})\\d{5}","\\d{7,9}",
,,"1234567"],[,,"(?:111|77\\d|88\\d|99\\d)\\d{6}","\\d{9}",,,"991234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"MW",265,"00","0",,,"0",,,,[[,"(\\d)(\\d{3})(\\d{3})","$1 $2 $3",["1"],"0$1","",0],[,"(2\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["2"],"0$1","",0],[,"(\\d{3})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["[1789]"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],MX:[,[,,"[1-9]\\d{9,10}","\\d{7,11}"],[,,"(?:33|55|81)\\d{8}|(?:2(?:2[2-9]|3[1-35-8]|4[13-9]|7[1-689]|8[1-578]|9[467])|3(?:1[1-79]|[2458][1-9]|7[1-8]|9[1-5])|4(?:1[1-57-9]|[24-6][1-9]|[37][1-8]|8[1-35-9]|9[2-689])|5(?:88|9[1-79])|6(?:1[2-68]|[234][1-9]|5[1-3689]|6[12457-9]|7[1-7]|8[67]|9[4-8])|7(?:[13467][1-9]|2[1-8]|5[13-9]|8[1-69]|9[17])|8(?:2[13-689]|3[1-6]|4[124-6]|6[1246-9]|7[1-378]|9[12479])|9(?:1[346-9]|2[1-4]|3[2-46-8]|5[1348]|[69][1-9]|7[12]|8[1-8]))\\d{7}",
"\\d{7,10}",,,"2221234567"],[,,"1(?:(?:33|55|81)\\d{8}|(?:2(?:2[2-9]|3[1-35-8]|4[13-9]|7[1-689]|8[1-578]|9[467])|3(?:1[1-79]|[2458][1-9]|7[1-8]|9[1-5])|4(?:1[1-57-9]|[24-6][1-9]|[37][1-8]|8[1-35-9]|9[2-689])|5(?:88|9[1-79])|6(?:1[2-68]|[2-4][1-9]|5[1-3689]|6[12457-9]|7[1-7]|8[67]|9[4-8])|7(?:[13467][1-9]|2[1-8]|5[13-9]|8[1-69]|9[17])|8(?:2[13-689]|3[1-6]|4[124-6]|6[1246-9]|7[1-378]|9[12479])|9(?:1[346-9]|2[1-4]|3[2-46-8]|5[1348]|[69][1-9]|7[12]|8[1-8]))\\d{7})","\\d{11}",,,"12221234567"],[,,"800\\d{7}",
"\\d{10}",,,"8001234567"],[,,"900\\d{7}","\\d{10}",,,"9001234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"MX",52,"0[09]","01",,,"0[12]|04[45](\\d{10})","1$1",,,[[,"([358]\\d)(\\d{4})(\\d{4})","$1 $2 $3",["33|55|81"],"01 $1","",1],[,"(\\d{3})(\\d{3})(\\d{4})","$1 $2 $3",["[2467]|3[12457-9]|5[89]|8[02-9]|9[0-35-9]"],"01 $1","",1],[,"(1)([358]\\d)(\\d{4})(\\d{4})","044 $2 $3 $4",["1(?:33|55|81)"],"$1","",1],[,"(1)(\\d{3})(\\d{3})(\\d{4})","044 $2 $3 $4",["1(?:[2467]|3[12457-9]|5[89]|8[2-9]|9[1-35-9])"],
"$1","",1]],[[,"([358]\\d)(\\d{4})(\\d{4})","$1 $2 $3",["33|55|81","33|55|81"],"01 $1","",1],[,"(\\d{3})(\\d{3})(\\d{4})","$1 $2 $3",["[2467]|3[12457-9]|5[89]|8[02-9]|9[0-35-9]","[2467]|3[12457-9]|5[89]|8[02-9]|9[0-35-9]"],"01 $1","",1],[,"(1)([358]\\d)(\\d{4})(\\d{4})","$1 $2 $3 $4",["1(?:33|55|81)"]],[,"(1)(\\d{3})(\\d{3})(\\d{4})","$1 $2 $3 $4",["1(?:[2467]|3[12457-9]|5[89]|8[2-9]|9[1-35-9])"]]],[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],1,,[,,"NA","NA"]],MY:[,[,,"[13-9]\\d{7,9}","\\d{6,10}"],
[,,"(?:3[2-9]\\d|[4-9][2-9])\\d{6}","\\d{6,9}",,,"323456789"],[,,"1(?:1[1-3]\\d{2}|[02-4679][2-9]\\d|59\\d{2}|8(?:1[23]|[2-9]\\d))\\d{5}","\\d{9,10}",,,"123456789"],[,,"1[378]00\\d{6}","\\d{10}",,,"1300123456"],[,,"1600\\d{6}","\\d{10}",,,"1600123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"154\\d{7}","\\d{10}",,,"1541234567"],"MY",60,"00","0",,,"0",,,,[[,"([4-79])(\\d{3})(\\d{4})","$1-$2 $3",["[4-79]"],"0$1","",0],[,"(3)(\\d{4})(\\d{4})","$1-$2 $3",["3"],"0$1","",0],[,"([18]\\d)(\\d{3})(\\d{3,4})","$1-$2 $3",
["1[02-46-9][1-9]|8"],"0$1","",0],[,"(1)([36-8]00)(\\d{2})(\\d{4})","$1-$2-$3-$4",["1[36-8]0"],"","",0],[,"(11)(\\d{4})(\\d{4})","$1-$2 $3",["11"],"0$1","",0],[,"(15[49])(\\d{3})(\\d{4})","$1-$2 $3",["15"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],MZ:[,[,,"[28]\\d{7,8}","\\d{8,9}"],[,,"2(?:[1346]\\d|5[0-2]|[78][12]|93)\\d{5}","\\d{8}",,,"21123456"],[,,"8[246]\\d{7}","\\d{9}",,,"821234567"],[,,"800\\d{6}","\\d{9}",,,"800123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA",
"NA"],[,,"NA","NA"],"MZ",258,"00",,,,,,,,[[,"([28]\\d)(\\d{3})(\\d{3,4})","$1 $2 $3",["2|8[246]"],"","",0],[,"(80\\d)(\\d{3})(\\d{3})","$1 $2 $3",["80"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],NA:[,[,,"[68]\\d{7,8}","\\d{8,9}"],[,,"6(?:1(?:17|2(?:[0189]\\d|[2-6]|7\\d?)|3(?:[01378]|2\\d)|4[01]|69|7[014])|2(?:17|5(?:[0-36-8]|4\\d?)|69|70)|3(?:17|2(?:[0237]\\d?|[14-689])|34|6[29]|7[01]|81)|4(?:17|2(?:[012]|7?)|4(?:[06]|1\\d)|5(?:[01357]|[25]\\d?)|69|7[01])|5(?:17|2(?:[0459]|[23678]\\d?)|69|7[01])|6(?:17|2(?:5|6\\d?)|38|42|69|7[01])|7(?:17|2(?:[569]|[234]\\d?)|3(?:0\\d?|[13])|69|7[01]))\\d{4}",
"\\d{8,9}",,,"61221234"],[,,"(?:60|8[125])\\d{7}","\\d{9}",,,"811234567"],[,,"NA","NA"],[,,"8701\\d{5}","\\d{9}",,,"870123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"8(3\\d{2}|86)\\d{5}","\\d{8,9}",,,"88612345"],"NA",264,"00","0",,,"0",,,,[[,"(8\\d)(\\d{3})(\\d{4})","$1 $2 $3",["8[1235]"],"0$1","",0],[,"(6\\d)(\\d{2,3})(\\d{4})","$1 $2 $3",["6"],"0$1","",0],[,"(88)(\\d{3})(\\d{3})","$1 $2 $3",["88"],"0$1","",0],[,"(870)(\\d{3})(\\d{3})","$1 $2 $3",["870"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,
"NA","NA"],,,[,,"NA","NA"]],NC:[,[,,"[2-57-9]\\d{5}","\\d{6}"],[,,"(?:2[03-9]|3[0-5]|4[1-7]|88)\\d{4}","\\d{6}",,,"201234"],[,,"(?:5[0-4]|[79]\\d|8[0-79])\\d{4}","\\d{6}",,,"751234"],[,,"NA","NA"],[,,"36\\d{4}","\\d{6}",,,"366711"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"NC",687,"00",,,,,,,,[[,"(\\d{2})(\\d{2})(\\d{2})","$1.$2.$3",["[2-46-9]|5[0-4]"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],NE:[,[,,"[0289]\\d{7}","\\d{8}"],[,,"2(?:0(?:20|3[1-7]|4[134]|5[14]|6[14578]|7[1-578])|1(?:4[145]|5[14]|6[14-68]|7[169]|88))\\d{4}",
"\\d{8}",,,"20201234"],[,,"(?:89|9[0-46-9])\\d{6}","\\d{8}",,,"93123456"],[,,"08\\d{6}","\\d{8}",,,"08123456"],[,,"09\\d{6}","\\d{8}",,,"09123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"NE",227,"00",,,,,,,,[[,"(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["[289]|09"],"","",0],[,"(08)(\\d{3})(\\d{3})","$1 $2 $3",["08"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],1,,[,,"NA","NA"]],NF:[,[,,"[13]\\d{5}","\\d{5,6}"],[,,"(?:1(?:06|17|28|39)|3[012]\\d)\\d{3}","\\d{5,6}",,,"106609"],[,
,"38\\d{4}","\\d{5,6}",,,"381234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"NF",672,"00",,,,,,,,[[,"(\\d{2})(\\d{4})","$1 $2",["1"],"","",0],[,"(\\d)(\\d{5})","$1 $2",["3"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],NG:[,[,,"[1-6]\\d{5,8}|9\\d{5,9}|[78]\\d{5,13}","\\d{5,14}"],[,,"[12]\\d{6,7}|9(?:0[3-9]|[1-9]\\d)\\d{5}|(?:3\\d|4[023568]|5[02368]|6[02-469]|7[4-69]|8[2-9])\\d{6}|(?:4[47]|5[14579]|6[1578]|7[0-357])\\d{5,6}|(?:78|41)\\d{5}","\\d{5,9}",
,,"12345678"],[,,"(?:1(?:7[34]\\d|8(?:04|[124579]\\d|8[0-3])|95\\d)|287[0-7]|3(?:18[1-8]|88[0-7]|9(?:8[5-9]|6[1-5]))|4(?:28[0-2]|6(?:7[1-9]|8[02-47])|88[0-2])|5(?:2(?:7[7-9]|8\\d)|38[1-79]|48[0-7]|68[4-7])|6(?:2(?:7[7-9]|8\\d)|4(?:3[7-9]|[68][129]|7[04-69]|9[1-8])|58[0-2]|98[7-9])|7(?:38[0-7]|69[1-8]|78[2-4])|8(?:28[3-9]|38[0-2]|4(?:2[12]|3[147-9]|5[346]|7[4-9]|8[014-689]|90)|58[1-8]|78[2-9]|88[5-7])|98[07]\\d)\\d{4}|(?:70(?:[13-9]\\d|2[1-9])|8(?:0[2-9]|1\\d)\\d|90[39]\\d)\\d{6}","\\d{8,10}",,,"8021234567"],
[,,"800\\d{7,11}","\\d{10,14}",,,"80017591759"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"NG",234,"009","0",,,"0",,,,[[,"([129])(\\d{3})(\\d{3,4})","$1 $2 $3",["[129]"],"0$1","",0],[,"(\\d{2})(\\d{3})(\\d{2,3})","$1 $2 $3",["[3-6]|7(?:[1-79]|0[1-9])|8[2-9]"],"0$1","",0],[,"(\\d{3})(\\d{3})(\\d{3,4})","$1 $2 $3",["70|8[01]|90[39]"],"0$1","",0],[,"([78]00)(\\d{4})(\\d{4,5})","$1 $2 $3",["[78]00"],"0$1","",0],[,"([78]00)(\\d{5})(\\d{5,6})","$1 $2 $3",["[78]00"],"0$1","",0],[,"(78)(\\d{2})(\\d{3})",
"$1 $2 $3",["78"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"700\\d{7,11}","\\d{10,14}",,,"7001234567"],,,[,,"NA","NA"]],NI:[,[,,"[12578]\\d{7}","\\d{8}"],[,,"2\\d{7}","\\d{8}",,,"21234567"],[,,"5(?:500\\d{4}|7\\d{6})|78\\d{6}|8\\d{7}","\\d{8}",,,"81234567"],[,,"1800\\d{4}","\\d{8}",,,"18001234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"NI",505,"00",,,,,,,,[[,"(\\d{4})(\\d{4})","$1 $2",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],NL:[,[,,"1\\d{4,8}|[2-7]\\d{8}|[89]\\d{6,9}",
"\\d{5,10}"],[,,"(?:1[0135-8]|2[02-69]|3[0-68]|4[0135-9]|[57]\\d|8[478])\\d{7}","\\d{9}",,,"101234567"],[,,"6[1-58]\\d{7}","\\d{9}",,,"612345678"],[,,"800\\d{4,7}","\\d{7,10}",,,"8001234"],[,,"90[069]\\d{4,7}","\\d{7,10}",,,"9061234"],[,,"NA","NA"],[,,"NA","NA"],[,,"85\\d{7}","\\d{9}",,,"851234567"],"NL",31,"00","0",,,"0",,,,[[,"([1-578]\\d)(\\d{3})(\\d{4})","$1 $2 $3",["1[035]|2[0346]|3[03568]|4[0356]|5[0358]|7|8[4578]"],"0$1","",0],[,"([1-5]\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["1[16-8]|2[259]|3[124]|4[17-9]|5[124679]"],
"0$1","",0],[,"(6)(\\d{8})","$1 $2",["6[0-57-9]"],"0$1","",0],[,"(66)(\\d{7})","$1 $2",["66"],"0$1","",0],[,"(14)(\\d{3,4})","$1 $2",["14"],"$1","",0],[,"([89]0\\d)(\\d{4,7})","$1 $2",["80|9"],"0$1","",0]],,[,,"66\\d{7}","\\d{9}",,,"662345678"],,,[,,"14\\d{3,4}","\\d{5,6}"],[,,"140(?:1(?:[035]|[16-8]\\d)|2(?:[0346]|[259]\\d)|3(?:[03568]|[124]\\d)|4(?:[0356]|[17-9]\\d)|5(?:[0358]|[124679]\\d)|7\\d|8[458])","\\d{5,6}",,,"14020"],,,[,,"NA","NA"]],NO:[,[,,"0\\d{4}|[2-9]\\d{7}","\\d{5}(?:\\d{3})?"],[,
,"(?:2[1-4]|3[1-3578]|5[1-35-7]|6[1-4679]|7[0-8])\\d{6}","\\d{8}",,,"21234567"],[,,"(?:4[015-8]|5[89]|9\\d)\\d{6}","\\d{8}",,,"41234567"],[,,"80[01]\\d{5}","\\d{8}",,,"80012345"],[,,"82[09]\\d{5}","\\d{8}",,,"82012345"],[,,"810(?:0[0-6]|[2-8]\\d)\\d{3}","\\d{8}",,,"81021234"],[,,"880\\d{5}","\\d{8}",,,"88012345"],[,,"85[0-5]\\d{5}","\\d{8}",,,"85012345"],"NO",47,"00",,,,,,,,[[,"([489]\\d{2})(\\d{2})(\\d{3})","$1 $2 $3",["[489]"],"","",0],[,"([235-7]\\d)(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["[235-7]"],
"","",0]],,[,,"NA","NA"],1,,[,,"NA","NA"],[,,"0\\d{4}|81(?:0(?:0[7-9]|1\\d)|5\\d{2})\\d{3}","\\d{5}(?:\\d{3})?",,,"01234"],1,,[,,"81[23]\\d{5}","\\d{8}",,,"81212345"]],NP:[,[,,"[1-8]\\d{7}|9(?:[1-69]\\d{6}|7[2-6]\\d{5,7}|8\\d{8})","\\d{6,10}"],[,,"(?:1[0124-6]|2[13-79]|3[135-8]|4[146-9]|5[135-7]|6[13-9]|7[15-9]|8[1-46-9]|9[1-79])\\d{6}","\\d{6,8}",,,"14567890"],[,,"9(?:7[45]|8[01456])\\d{7}","\\d{10}",,,"9841234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"NP",977,"00",
"0",,,"0",,,,[[,"(1)(\\d{7})","$1-$2",["1[2-6]"],"0$1","",0],[,"(\\d{2})(\\d{6})","$1-$2",["1[01]|[2-8]|9(?:[1-69]|7[15-9])"],"0$1","",0],[,"(9\\d{2})(\\d{7})","$1-$2",["9(?:7[45]|8)"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],NR:[,[,,"[458]\\d{6}","\\d{7}"],[,,"(?:444|888)\\d{4}","\\d{7}",,,"4441234"],[,,"55[5-9]\\d{4}","\\d{7}",,,"5551234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"NR",674,"00",,,,,,,,[[,"(\\d{3})(\\d{4})","$1 $2",,"",
"",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],NU:[,[,,"[1-5]\\d{3}","\\d{4}"],[,,"[34]\\d{3}","\\d{4}",,,"4002"],[,,"[125]\\d{3}","\\d{4}",,,"1234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"NU",683,"00",,,,,,,,,,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],NZ:[,[,,"6[235-9]\\d{6}|[2-57-9]\\d{7,10}","\\d{7,11}"],[,,"(?:3[2-79]|[49][2-689]|6[235-9]|7[2-5789])\\d{6}|24099\\d{3}","\\d{7,8}",,,"32345678"],[,,"2(?:[028]\\d{7,8}|1(?:[03]\\d{5,7}|[12457]\\d{5,6}|[689]\\d{5})|[79]\\d{7})",
"\\d{8,10}",,,"211234567"],[,,"508\\d{6,7}|80\\d{6,8}","\\d{8,10}",,,"800123456"],[,,"90\\d{7,9}","\\d{9,11}",,,"900123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"NZ",64,"0(?:0|161)","0",,,"0",,"00",,[[,"([34679])(\\d{3})(\\d{4})","$1-$2 $3",["[3467]|9[1-9]"],"0$1","",0],[,"(24099)(\\d{3})","$1 $2",["240","2409","24099"],"0$1","",0],[,"(\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["21"],"0$1","",0],[,"(\\d{2})(\\d{3})(\\d{3,5})","$1 $2 $3",["2(?:1[1-9]|[69]|7[0-35-9])|86"],"0$1","",0],[,"(2\\d)(\\d{3,4})(\\d{4})",
"$1 $2 $3",["2[028]"],"0$1","",0],[,"(\\d{3})(\\d{3})(\\d{3,4})","$1 $2 $3",["2(?:10|74)|5|[89]0"],"0$1","",0]],,[,,"[28]6\\d{6,7}","\\d{8,9}",,,"26123456"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],OM:[,[,,"(?:2[2-6]|5|9[1-9])\\d{6}|800\\d{5,6}","\\d{7,9}"],[,,"2[2-6]\\d{6}","\\d{8}",,,"23123456"],[,,"9[1-9]\\d{6}","\\d{8}",,,"92123456"],[,,"8007\\d{4,5}|500\\d{4}","\\d{7,9}",,,"80071234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"OM",968,"00",,,,,,,,[[,"(2\\d)(\\d{6})","$1 $2",
["2"],"","",0],[,"(9\\d{3})(\\d{4})","$1 $2",["9"],"","",0],[,"([58]00)(\\d{4,6})","$1 $2",["[58]"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],PA:[,[,,"[1-9]\\d{6,7}","\\d{7,8}"],[,,"(?:1(?:0[02-579]|19|2[37]|3[03]|4[479]|57|65|7[016-8]|8[58]|9[1349])|2(?:[0235679]\\d|1[0-7]|4[04-9]|8[028])|3(?:[09]\\d|1[14-7]|2[0-3]|3[03]|4[0457]|5[56]|6[068]|7[06-8]|8[089])|4(?:3[013-69]|4\\d|7[0-689])|5(?:[01]\\d|2[0-7]|[56]0|79)|7(?:0[09]|2[0-267]|3[06]|[49]0|5[06-9]|7[0-24-7]|8[89])|8(?:[34]\\d|5[0-4]|8[02])|9(?:0[6-8]|1[016-8]|2[036-8]|3[3679]|40|5[0489]|6[06-9]|7[046-9]|8[36-8]|9[1-9]))\\d{4}",
"\\d{7}",,,"2001234"],[,,"(?:1[16]1|21[89]|8(?:1[01]|7[23]))\\d{4}|6(?:[024-9]\\d|1[0-5]|3[04-9])\\d{5}","\\d{7,8}",,,"60012345"],[,,"80[09]\\d{4}","\\d{7}",,,"8001234"],[,,"(?:779|8(?:2[235]|55|60|7[578]|86|95)|9(?:0[0-2]|81))\\d{4}","\\d{7}",,,"8601234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"PA",507,"00",,,,,,,,[[,"(\\d{3})(\\d{4})","$1-$2",["[1-57-9]"],"","",0],[,"(\\d{4})(\\d{4})","$1-$2",["6"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],PE:[,[,,"[14-9]\\d{7,8}",
"\\d{6,9}"],[,,"(?:1\\d|4[1-4]|5[1-46]|6[1-7]|7[2-46]|8[2-4])\\d{6}","\\d{6,8}",,,"11234567"],[,,"9\\d{8}","\\d{9}",,,"912345678"],[,,"800\\d{5}","\\d{8}",,,"80012345"],[,,"805\\d{5}","\\d{8}",,,"80512345"],[,,"801\\d{5}","\\d{8}",,,"80112345"],[,,"80[24]\\d{5}","\\d{8}",,,"80212345"],[,,"NA","NA"],"PE",51,"19(?:1[124]|77|90)00","0"," Anexo ",,"0",,,,[[,"(1)(\\d{7})","$1 $2",["1"],"(0$1)","",0],[,"([4-8]\\d)(\\d{6})","$1 $2",["[4-7]|8[2-4]"],"(0$1)","",0],[,"(\\d{3})(\\d{5})","$1 $2",["80"],"(0$1)",
"",0],[,"(9\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["9"],"$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],PF:[,[,,"[2-79]\\d{5}|8\\d{5,7}","\\d{6}(?:\\d{2})?"],[,,"(?:4(?:[02-9]\\d|1[02-9])|[5689]\\d{2})\\d{3}","\\d{6}",,,"401234"],[,,"(?:[27]\\d{2}|3[0-79]\\d|411|89\\d{3})\\d{3}","\\d{6}(?:\\d{2})?",,,"212345"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"PF",689,"00",,,,,,,,[[,"(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["89"],"","",0],[,"(\\d{2})(\\d{2})(\\d{2})",
"$1 $2 $3",,"","",0]],,[,,"NA","NA"],,,[,,"44\\d{4}","\\d{6}",,,"441234"],[,,"NA","NA"],,,[,,"NA","NA"]],PG:[,[,,"[1-9]\\d{6,7}","\\d{7,8}"],[,,"(?:3[0-2]\\d|4[25]\\d|5[34]\\d|64[1-9]|77(?:[0-24]\\d|30)|85[02-46-9]|9[78]\\d)\\d{4}","\\d{7}",,,"3123456"],[,,"(?:68|7(?:[0-369]\\d|75))\\d{5}","\\d{7,8}",,,"6812345"],[,,"180\\d{4}","\\d{7}",,,"1801234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"275\\d{4}","\\d{7}",,,"2751234"],"PG",675,"00",,,,,,,,[[,"(\\d{3})(\\d{4})","$1 $2",["[1-689]"],"","",0],
[,"(7\\d{3})(\\d{4})","$1 $2",["7"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],PH:[,[,,"2\\d{5,7}|[3-9]\\d{7,9}|1800\\d{7,9}","\\d{5,13}"],[,,"2\\d{5}(?:\\d{2})?|(?:3[2-68]|4[2-9]|5[2-6]|6[2-58]|7[24578]|8[2-8])\\d{7}|88(?:22\\d{6}|42\\d{4})","\\d{5,10}",,,"21234567"],[,,"(?:81[37]|9(?:0[5-9]|1[025-9]|2[0-35-9]|3[02-9]|4[236-9]|7[3479]|89|9[46-9]))\\d{7}","\\d{10}",,,"9051234567"],[,,"1800\\d{7,9}","\\d{11,13}",,,"180012345678"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],
[,,"NA","NA"],"PH",63,"00","0",,,"0",,,,[[,"(2)(\\d{3})(\\d{4})","$1 $2 $3",["2"],"(0$1)","",0],[,"(2)(\\d{5})","$1 $2",["2"],"(0$1)","",0],[,"(\\d{4})(\\d{4,6})","$1 $2",["3(?:23|39|46)|4(?:2[3-6]|[35]9|4[26]|76)|5(?:22|44)|642|8(?:62|8[245])","3(?:230|397|461)|4(?:2(?:35|[46]4|51)|396|4(?:22|63)|59[347]|76[15])|5(?:221|446)|642[23]|8(?:622|8(?:[24]2|5[13]))"],"(0$1)","",0],[,"(\\d{5})(\\d{4})","$1 $2",["346|4(?:27|9[35])|883","3469|4(?:279|9(?:30|56))|8834"],"(0$1)","",0],[,"([3-8]\\d)(\\d{3})(\\d{4})",
"$1 $2 $3",["[3-8]"],"(0$1)","",0],[,"(\\d{3})(\\d{3})(\\d{4})","$1 $2 $3",["81|9"],"0$1","",0],[,"(1800)(\\d{3})(\\d{4})","$1 $2 $3",["1"],"","",0],[,"(1800)(\\d{1,2})(\\d{3})(\\d{4})","$1 $2 $3 $4",["1"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],PK:[,[,,"1\\d{8}|[2-8]\\d{5,11}|9(?:[013-9]\\d{4,9}|2\\d(?:111\\d{6}|\\d{3,7}))","\\d{6,12}"],[,,"(?:21|42)[2-9]\\d{7}|(?:2[25]|4[0146-9]|5[1-35-7]|6[1-8]|7[14]|8[16]|91)[2-9]\\d{6}|(?:2(?:3[2358]|4[2-4]|9[2-8])|45[3479]|54[2-467]|60[468]|72[236]|8(?:2[2-689]|3[23578]|4[3478]|5[2356])|9(?:1|2[2-8]|3[27-9]|4[2-6]|6[3569]|9[25-8]))[2-9]\\d{5,6}|58[126]\\d{7}",
"\\d{6,10}",,,"2123456789"],[,,"3(?:0\\d|[12][0-5]|[34][1-7]|55|64)\\d{7}","\\d{10}",,,"3012345678"],[,,"800\\d{5}","\\d{8}",,,"80012345"],[,,"900\\d{5}","\\d{8}",,,"90012345"],[,,"NA","NA"],[,,"122\\d{6}","\\d{9}",,,"122044444"],[,,"NA","NA"],"PK",92,"00","0",,,"0",,,,[[,"(\\d{2})(111)(\\d{3})(\\d{3})","$1 $2 $3 $4",["(?:2[125]|4[0-246-9]|5[1-35-7]|6[1-8]|7[14]|8[16]|91)1","(?:2[125]|4[0-246-9]|5[1-35-7]|6[1-8]|7[14]|8[16]|91)11","(?:2[125]|4[0-246-9]|5[1-35-7]|6[1-8]|7[14]|8[16]|91)111"],"(0$1)",
"",0],[,"(\\d{3})(111)(\\d{3})(\\d{3})","$1 $2 $3 $4",["2[349]|45|54|60|72|8[2-5]|9[2-9]","(?:2[349]|45|54|60|72|8[2-5]|9[2-9])\\d1","(?:2[349]|45|54|60|72|8[2-5]|9[2-9])\\d11","(?:2[349]|45|54|60|72|8[2-5]|9[2-9])\\d111"],"(0$1)","",0],[,"(\\d{2})(\\d{7,8})","$1 $2",["(?:2[125]|4[0-246-9]|5[1-35-7]|6[1-8]|7[14]|8[16]|91)[2-9]"],"(0$1)","",0],[,"(\\d{3})(\\d{6,7})","$1 $2",["2[349]|45|54|60|72|8[2-5]|9[2-9]","(?:2[349]|45|54|60|72|8[2-5]|9[2-9])\\d[2-9]"],"(0$1)","",0],[,"(3\\d{2})(\\d{7})","$1 $2",
["3"],"0$1","",0],[,"([15]\\d{3})(\\d{5,6})","$1 $2",["58[12]|1"],"(0$1)","",0],[,"(586\\d{2})(\\d{5})","$1 $2",["586"],"(0$1)","",0],[,"([89]00)(\\d{3})(\\d{2})","$1 $2 $3",["[89]00"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"(?:2(?:[125]|3[2358]|4[2-4]|9[2-8])|4(?:[0-246-9]|5[3479])|5(?:[1-35-7]|4[2-467])|6(?:[1-8]|0[468])|7(?:[14]|2[236])|8(?:[16]|2[2-689]|3[23578]|4[3478]|5[2356])|9(?:1|22|3[27-9]|4[2-6]|6[3569]|9[2-7]))111\\d{6}","\\d{11,12}",,,"21111825888"],,,[,,"NA","NA"]],PL:[,[,,"[1-58]\\d{6,8}|9\\d{8}|[67]\\d{5,8}",
"\\d{6,9}"],[,,"(?:1[2-8]|2[2-59]|3[2-4]|4[1-468]|5[24-689]|6[1-3578]|7[14-6]|8[1-7])\\d{5,7}|77\\d{4,7}|(?:89|9[145])\\d{7}","\\d{6,9}",,,"123456789"],[,,"(?:5[0137]|6[069]|7[2389]|88)\\d{7}","\\d{9}",,,"512345678"],[,,"800\\d{6}","\\d{9}",,,"800123456"],[,,"70\\d{7}","\\d{9}",,,"701234567"],[,,"801\\d{6}","\\d{9}",,,"801234567"],[,,"NA","NA"],[,,"39\\d{7}","\\d{9}",,,"391234567"],"PL",48,"00",,,,,,,,[[,"(\\d{2})(\\d{3})(\\d{2})(\\d{2})","$1 $2 $3 $4",["[124]|3[2-4]|5[24-689]|6[1-3578]|7[14-7]|8[1-79]|9[145]"],
"","",0],[,"(\\d{2})(\\d{4,6})","$1 $2",["[124]|3[2-4]|5[24-689]|6[1-3578]|7[14-7]|8[1-7]"],"","",0],[,"(\\d{3})(\\d{3})(\\d{3})","$1 $2 $3",["39|5[0137]|6[0469]|7[02389]|8[08]"],"","",0],[,"(\\d{3})(\\d{2})(\\d{2,3})","$1 $2 $3",["64"],"","",0],[,"(\\d{3})(\\d{3})","$1 $2",["64"],"","",0]],,[,,"642\\d{3,6}","\\d{6,9}",,,"642123456"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],PM:[,[,,"[45]\\d{5}","\\d{6}"],[,,"41\\d{4}","\\d{6}",,,"411234"],[,,"55\\d{4}","\\d{6}",,,"551234"],[,,"NA","NA"],[,,
"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"PM",508,"00","0",,,"0",,,,[[,"([45]\\d)(\\d{2})(\\d{2})","$1 $2 $3",,"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],PR:[,[,,"[5789]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"(?:787|939)[2-9]\\d{6}","\\d{7}(?:\\d{3})?",,,"7872345678"],[,,"(?:787|939)[2-9]\\d{6}","\\d{7}(?:\\d{3})?",,,"7872345678"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002345678"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002345678"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}",
"\\d{10}",,,"5002345678"],[,,"NA","NA"],"PR",1,"011","1",,,"1",,,1,,,[,,"NA","NA"],,"787|939",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],PS:[,[,,"[24589]\\d{7,8}|1(?:[78]\\d{8}|[49]\\d{2,3})","\\d{4,10}"],[,,"(?:22[234789]|42[45]|82[01458]|92[369])\\d{5}","\\d{7,8}",,,"22234567"],[,,"5[69]\\d{7}","\\d{9}",,,"599123456"],[,,"1800\\d{6}","\\d{10}",,,"1800123456"],[,,"1(?:4|9\\d)\\d{2}","\\d{4,5}",,,"19123"],[,,"1700\\d{6}","\\d{10}",,,"1700123456"],[,,"NA","NA"],[,,"NA","NA"],"PS",970,"00","0",,,
"0",,,,[[,"([2489])(2\\d{2})(\\d{4})","$1 $2 $3",["[2489]"],"0$1","",0],[,"(5[69]\\d)(\\d{3})(\\d{3})","$1 $2 $3",["5"],"0$1","",0],[,"(1[78]00)(\\d{3})(\\d{3})","$1 $2 $3",["1[78]"],"$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],PT:[,[,,"[2-46-9]\\d{8}","\\d{9}"],[,,"2(?:[12]\\d|[35][1-689]|4[1-59]|6[1-35689]|7[1-9]|8[1-69]|9[1256])\\d{6}","\\d{9}",,,"212345678"],[,,"9(?:[136]\\d{2}|2[0-79]\\d|480)\\d{5}","\\d{9}",,,"912345678"],[,,"80[02]\\d{6}","\\d{9}",,,"800123456"],
[,,"76(?:0[1-57]|1[2-47]|2[237])\\d{5}","\\d{9}",,,"760123456"],[,,"80(?:8\\d|9[1579])\\d{5}","\\d{9}",,,"808123456"],[,,"884[128]\\d{5}","\\d{9}",,,"884123456"],[,,"30\\d{7}","\\d{9}",,,"301234567"],"PT",351,"00",,,,,,,,[[,"(2\\d)(\\d{3})(\\d{4})","$1 $2 $3",["2[12]"],"","",0],[,"([2-46-9]\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["2[3-9]|[346-9]"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"70(?:7\\d|8[17])\\d{5}","\\d{9}",,,"707123456"],,,[,,"NA","NA"]],PW:[,[,,"[2-8]\\d{6}","\\d{7}"],[,,"2552255|(?:277|345|488|5(?:35|44|87)|6(?:22|54|79)|7(?:33|47)|8(?:24|55|76))\\d{4}",
"\\d{7}",,,"2771234"],[,,"(?:6[234689]0|77[45789])\\d{4}","\\d{7}",,,"6201234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"PW",680,"01[12]",,,,,,,,[[,"(\\d{3})(\\d{4})","$1 $2",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],PY:[,[,,"5[0-5]\\d{4,7}|[2-46-9]\\d{5,8}","\\d{5,9}"],[,,"(?:[26]1|3[289]|4[124678]|7[123]|8[1236])\\d{5,7}|(?:2(?:2[4568]|7[15]|9[1-5])|3(?:18|3[167]|4[2357]|51)|4(?:18|2[45]|3[12]|5[13]|64|71|9[1-47])|5(?:[1-4]\\d|5[0234])|6(?:3[1-3]|44|7[1-4678])|7(?:17|4[0-4]|6[1-578]|75|8[0-8])|858)\\d{5,6}",
"\\d{5,9}",,,"212345678"],[,,"9(?:6[12]|[78][1-6]|9[1-5])\\d{6}","\\d{9}",,,"961456789"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"8700[0-4]\\d{4}","\\d{9}",,,"870012345"],"PY",595,"00","0",,,"0",,,,[[,"(\\d{2})(\\d{5,7})","$1 $2",["(?:[26]1|3[289]|4[124678]|7[123]|8[1236])"],"($1)","",0],[,"(\\d{3})(\\d{3,6})","$1 $2",["[2-9]0"],"0$1","",0],[,"(\\d{3})(\\d{6})","$1 $2",["9[1-9]"],"0$1","",0],[,"(\\d{2})(\\d{3})(\\d{4})","$1 $2 $3",["8700"],"","",0],[,"(\\d{3})(\\d{4,6})","$1 $2",
["[2-8][1-9]"],"($1)","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"[2-9]0\\d{4,7}","\\d{6,9}",,,"201234567"],,,[,,"NA","NA"]],QA:[,[,,"[2-8]\\d{6,7}","\\d{7,8}"],[,,"4[04]\\d{6}","\\d{7,8}",,,"44123456"],[,,"[3567]\\d{7}","\\d{7,8}",,,"33123456"],[,,"800\\d{4}","\\d{7,8}",,,"8001234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"QA",974,"00",,,,,,,,[[,"([28]\\d{2})(\\d{4})","$1 $2",["[28]"],"","",0],[,"([3-7]\\d{3})(\\d{4})","$1 $2",["[3-7]"],"","",0]],,[,,"2(?:[12]\\d|61)\\d{4}","\\d{7}",
,,"2123456"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],RE:[,[,,"[268]\\d{8}","\\d{9}"],[,,"262\\d{6}","\\d{9}",,,"262161234"],[,,"6(?:9[23]|47)\\d{6}","\\d{9}",,,"692123456"],[,,"80\\d{7}","\\d{9}",,,"801234567"],[,,"89[1-37-9]\\d{6}","\\d{9}",,,"891123456"],[,,"8(?:1[019]|2[0156]|84|90)\\d{6}","\\d{9}",,,"810123456"],[,,"NA","NA"],[,,"NA","NA"],"RE",262,"00","0",,,"0",,,,[[,"([268]\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",,"0$1","",0]],,[,,"NA","NA"],1,"262|6[49]|8",[,,"NA","NA"],[,,"NA",
"NA"],,,[,,"NA","NA"]],RO:[,[,,"2\\d{5,8}|[37-9]\\d{8}","\\d{6,9}"],[,,"2(?:1(?:\\d{7}|9\\d{3})|[3-6](?:\\d{7}|\\d9\\d{2}))|3[13-6]\\d{7}","\\d{6,9}",,,"211234567"],[,,"7(?:000|[1-8]\\d{2}|99\\d)\\d{5}","\\d{9}",,,"712345678"],[,,"800\\d{6}","\\d{9}",,,"800123456"],[,,"90[036]\\d{6}","\\d{9}",,,"900123456"],[,,"801\\d{6}","\\d{9}",,,"801123456"],[,,"802\\d{6}","\\d{9}",,,"802123456"],[,,"NA","NA"],"RO",40,"00","0"," int ",,"0",,,,[[,"([237]\\d)(\\d{3})(\\d{4})","$1 $2 $3",["[23]1"],"0$1","",0],[,
"(21)(\\d{4})","$1 $2",["21"],"0$1","",0],[,"(\\d{3})(\\d{3})(\\d{3})","$1 $2 $3",["[23][3-7]|[7-9]"],"0$1","",0],[,"(2\\d{2})(\\d{3})","$1 $2",["2[3-6]"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"37\\d{7}","\\d{9}",,,"372123456"],,,[,,"NA","NA"]],RS:[,[,,"[126-9]\\d{4,11}|3(?:[0-79]\\d{3,10}|8[2-9]\\d{2,9})","\\d{5,12}"],[,,"(?:1(?:[02-9][2-9]|1[1-9])\\d|2(?:[0-24-7][2-9]\\d|[389](?:0[2-9]|[2-9]\\d))|3(?:[0-8][2-9]\\d|9(?:[2-9]\\d|0[2-9])))\\d{3,8}","\\d{5,12}",,,"10234567"],[,,"6(?:[0-689]|7\\d)\\d{6,7}",
"\\d{8,10}",,,"601234567"],[,,"800\\d{3,9}","\\d{6,12}",,,"80012345"],[,,"(?:90[0169]|78\\d)\\d{3,7}","\\d{6,12}",,,"90012345"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"RS",381,"00","0",,,"0",,,,[[,"([23]\\d{2})(\\d{4,9})","$1 $2",["(?:2[389]|39)0"],"0$1","",0],[,"([1-3]\\d)(\\d{5,10})","$1 $2",["1|2(?:[0-24-7]|[389][1-9])|3(?:[0-8]|9[1-9])"],"0$1","",0],[,"(6\\d)(\\d{6,8})","$1 $2",["6"],"0$1","",0],[,"([89]\\d{2})(\\d{3,9})","$1 $2",["[89]"],"0$1","",0],[,"(7[26])(\\d{4,9})","$1 $2",["7[26]"],
"0$1","",0],[,"(7[08]\\d)(\\d{4,9})","$1 $2",["7[08]"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"7[06]\\d{4,10}","\\d{6,12}",,,"700123456"],,,[,,"NA","NA"]],RU:[,[,,"[3489]\\d{9}","\\d{10}"],[,,"(?:3(?:0[12]|4[1-35-79]|5[1-3]|8[1-58]|9[0145])|4(?:01|1[1356]|2[13467]|7[1-5]|8[1-7]|9[1-689])|8(?:1[1-8]|2[01]|3[13-6]|4[0-8]|5[15]|6[1-35-7]|7[1-37-9]))\\d{7}","\\d{10}",,,"3011234567"],[,,"9\\d{9}","\\d{10}",,,"9123456789"],[,,"80[04]\\d{7}","\\d{10}",,,"8001234567"],[,,"80[39]\\d{7}","\\d{10}",
,,"8091234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"RU",7,"810","8",,,"8",,"8~10",,[[,"(\\d{3})(\\d{2})(\\d{2})","$1-$2-$3",["[1-79]"],"$1","",1],[,"([3489]\\d{2})(\\d{3})(\\d{2})(\\d{2})","$1 $2-$3-$4",["[34689]"],"8 ($1)","",1],[,"(7\\d{2})(\\d{3})(\\d{4})","$1 $2 $3",["7"],"8 ($1)","",1]],[[,"([3489]\\d{2})(\\d{3})(\\d{2})(\\d{2})","$1 $2-$3-$4",["[34689]","[34689]"],"8 ($1)","",1],[,"(7\\d{2})(\\d{3})(\\d{4})","$1 $2 $3",["7","7"],"8 ($1)","",1]],[,,"NA","NA"],1,,[,,"NA","NA"],[,,"NA",
"NA"],,,[,,"NA","NA"]],RW:[,[,,"[027-9]\\d{7,8}","\\d{8,9}"],[,,"2[258]\\d{7}|06\\d{6}","\\d{8,9}",,,"250123456"],[,,"7[238]\\d{7}","\\d{9}",,,"720123456"],[,,"800\\d{6}","\\d{9}",,,"800123456"],[,,"900\\d{6}","\\d{9}",,,"900123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"RW",250,"00","0",,,"0",,,,[[,"(2\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["2"],"$1","",0],[,"([7-9]\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["[7-9]"],"0$1","",0],[,"(0\\d)(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["0"],"","",0]],,[,,"NA",
"NA"],,,[,,"NA","NA"],[,,"NA","NA"],1,,[,,"NA","NA"]],SA:[,[,,"1\\d{7,8}|(?:[2-467]|92)\\d{7}|5\\d{8}|8\\d{9}","\\d{7,10}"],[,,"11\\d{7}|1?(?:2[24-8]|3[35-8]|4[3-68]|6[2-5]|7[235-7])\\d{6}","\\d{7,9}",,,"112345678"],[,,"(?:5[013-689]|811)\\d{7}","\\d{9,10}",,,"512345678"],[,,"800\\d{7}","\\d{10}",,,"8001234567"],[,,"NA","NA"],[,,"92[05]\\d{6}","\\d{9}",,,"920012345"],[,,"NA","NA"],[,,"NA","NA"],"SA",966,"00","0",,,"0",,,,[[,"([1-467])(\\d{3})(\\d{4})","$1 $2 $3",["[1-467]"],"0$1","",0],[,"(1\\d)(\\d{3})(\\d{4})",
"$1 $2 $3",["1[1-467]"],"0$1","",0],[,"(5\\d)(\\d{3})(\\d{4})","$1 $2 $3",["5"],"0$1","",0],[,"(92\\d{2})(\\d{5})","$1 $2",["92"],"$1","",0],[,"(800)(\\d{3})(\\d{4})","$1 $2 $3",["80"],"$1","",0],[,"(811)(\\d{3})(\\d{3,4})","$1 $2 $3",["81"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],SB:[,[,,"[1-9]\\d{4,6}","\\d{5,7}"],[,,"(?:1[4-79]|[23]\\d|4[01]|5[03]|6[0-37])\\d{3}","\\d{5}",,,"40123"],[,,"48\\d{3}|7(?:[46-8]\\d|5[025-9]|90)\\d{4}|8[4-8]\\d{5}|9(?:[46]\\d|5[0-46-9]|7[0-689]|8[0-79]|9[0-8])\\d{4}",
"\\d{5,7}",,,"7421234"],[,,"1[38]\\d{3}","\\d{5}",,,"18123"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"5[12]\\d{3}","\\d{5}",,,"51123"],"SB",677,"0[01]",,,,,,,,[[,"(\\d{3})(\\d{4})","$1 $2",["[7-9]"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],SC:[,[,,"[24689]\\d{5,6}","\\d{6,7}"],[,,"4[2-46]\\d{5}","\\d{7}",,,"4217123"],[,,"2[5-8]\\d{5}","\\d{7}",,,"2510123"],[,,"8000\\d{2}","\\d{6}",,,"800000"],[,,"98\\d{4}","\\d{6}",,,"981234"],[,,"NA","NA"],[,,"NA","NA"],[,,"64\\d{5}",
"\\d{7}",,,"6412345"],"SC",248,"0[0-2]",,,,,,"00",,[[,"(\\d{3})(\\d{3})","$1 $2",["[89]"],"","",0],[,"(\\d)(\\d{3})(\\d{3})","$1 $2 $3",["[246]"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],SD:[,[,,"[19]\\d{8}","\\d{9}"],[,,"1(?:[125]\\d|8[3567])\\d{6}","\\d{9}",,,"121231234"],[,,"9[012569]\\d{7}","\\d{9}",,,"911231234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"SD",249,"00","0",,,"0",,,,[[,"(\\d{2})(\\d{3})(\\d{4})","$1 $2 $3",,"0$1","",0]],
,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],SE:[,[,,"[1-9]\\d{5,9}","\\d{5,10}"],[,,"1(?:0[1-8]\\d{6}|[136]\\d{5,7}|(?:2[0-35]|4[0-4]|5[0-25-9]|7[13-6]|[89]\\d)\\d{5,6})|2(?:[136]\\d{5,7}|(?:2[0-7]|4[0136-8]|5[0138]|7[018]|8[01]|9[0-57])\\d{5,6})|3(?:[356]\\d{5,7}|(?:0[0-4]|1\\d|2[0-25]|4[056]|7[0-2]|8[0-3]|9[023])\\d{5,6})|4(?:0[1-9]\\d{4,6}|[246]\\d{5,7}|(?:1[013-8]|3[0135]|5[14-79]|7[0-246-9]|8[0156]|9[0-689])\\d{5,6})|5(?:0[0-6]|[15][0-5]|2[0-68]|3[0-4]|4\\d|6[03-5]|7[013]|8[0-79]|9[01])\\d{5,6}|6(?:0[1-9]\\d{4,6}|3\\d{5,7}|(?:1[1-3]|2[0-4]|4[02-57]|5[0-37]|6[0-3]|7[0-2]|8[0247]|9[0-356])\\d{5,6})|8[1-9]\\d{5,7}|9(?:0[1-9]\\d{4,6}|(?:1[0-68]|2\\d|3[02-5]|4[0-3]|5[0-4]|[68][01]|7[0135-8])\\d{5,6})",
"\\d{5,9}",,,"8123456"],[,,"7[0236]\\d{7}","\\d{9}",,,"701234567"],[,,"20(?:0(?:0\\d{2}|[1-9](?:0\\d{1,4}|[1-9]\\d{4}))|1(?:0\\d{4}|[1-9]\\d{4,5})|[2-9]\\d{5})","\\d{6,9}",,,"20123456"],[,,"9(?:00|39|44)(?:1(?:[0-26]\\d{5}|[3-57-9]\\d{2})|2(?:[0-2]\\d{5}|[3-9]\\d{2})|3(?:[0139]\\d{5}|[24-8]\\d{2})|4(?:[045]\\d{5}|[1-36-9]\\d{2})|5(?:5\\d{5}|[0-46-9]\\d{2})|6(?:[679]\\d{5}|[0-58]\\d{2})|7(?:[078]\\d{5}|[1-69]\\d{2})|8(?:[578]\\d{5}|[0-469]\\d{2}))","\\d{7}(?:\\d{3})?",,,"9001234567"],[,,"77(?:0(?:0\\d{2}|[1-9](?:0\\d|[1-9]\\d{4}))|[1-6][1-9]\\d{5})",
"\\d{6}(?:\\d{3})?",,,"771234567"],[,,"75[1-8]\\d{6}","\\d{9}",,,"751234567"],[,,"NA","NA"],"SE",46,"00","0",,,"0",,,,[[,"(8)(\\d{2,3})(\\d{2,3})(\\d{2})","$1-$2 $3 $4",["8"],"0$1","",0],[,"([1-69]\\d)(\\d{2,3})(\\d{2})(\\d{2})","$1-$2 $3 $4",["1[013689]|2[0136]|3[1356]|4[0246]|54|6[03]|90"],"0$1","",0],[,"([1-69]\\d)(\\d{3})(\\d{2})","$1-$2 $3",["1[13689]|2[136]|3[1356]|4[0246]|54|6[03]|90"],"0$1","",0],[,"(\\d{3})(\\d{2})(\\d{2})(\\d{2})","$1-$2 $3 $4",["1[2457]|2[2457-9]|3[0247-9]|4[1357-9]|5[0-35-9]|6[124-9]|9(?:[125-8]|3[0-5]|4[0-3])"],
"0$1","",0],[,"(\\d{3})(\\d{2,3})(\\d{2})","$1-$2 $3",["1[2457]|2[2457-9]|3[0247-9]|4[1357-9]|5[0-35-9]|6[124-9]|9(?:[125-8]|3[0-5]|4[0-3])"],"0$1","",0],[,"(7\\d)(\\d{3})(\\d{2})(\\d{2})","$1-$2 $3 $4",["7"],"0$1","",0],[,"(77)(\\d{2})(\\d{2})","$1-$2$3",["7"],"0$1","",0],[,"(20)(\\d{2,3})(\\d{2})","$1-$2 $3",["20"],"0$1","",0],[,"(9[034]\\d)(\\d{2})(\\d{2})(\\d{3})","$1-$2 $3 $4",["9[034]"],"0$1","",0],[,"(9[034]\\d)(\\d{4})","$1-$2",["9[034]"],"0$1","",0]],[[,"(8)(\\d{2,3})(\\d{2,3})(\\d{2})",
"$1 $2 $3 $4",["8"]],[,"([1-69]\\d)(\\d{2,3})(\\d{2})(\\d{2})","$1 $2 $3 $4",["1[013689]|2[0136]|3[1356]|4[0246]|54|6[03]|90"]],[,"([1-69]\\d)(\\d{3})(\\d{2})","$1 $2 $3",["1[13689]|2[136]|3[1356]|4[0246]|54|6[03]|90"]],[,"(\\d{3})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["1[2457]|2[2457-9]|3[0247-9]|4[1357-9]|5[0-35-9]|6[124-9]|9(?:[125-8]|3[0-5]|4[0-3])"]],[,"(\\d{3})(\\d{2,3})(\\d{2})","$1 $2 $3",["1[2457]|2[2457-9]|3[0247-9]|4[1357-9]|5[0-35-9]|6[124-9]|9(?:[125-8]|3[0-5]|4[0-3])"]],[,"(7\\d)(\\d{3})(\\d{2})(\\d{2})",
"$1 $2 $3 $4",["7"]],[,"(77)(\\d{2})(\\d{2})","$1 $2 $3",["7"]],[,"(20)(\\d{2,3})(\\d{2})","$1 $2 $3",["20"]],[,"(9[034]\\d)(\\d{2})(\\d{2})(\\d{3})","$1 $2 $3 $4",["9[034]"]],[,"(9[034]\\d)(\\d{4})","$1 $2",["9[034]"]]],[,,"74[02-9]\\d{6}","\\d{9}",,,"740123456"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],SG:[,[,,"[36]\\d{7}|[17-9]\\d{7,10}","\\d{8,11}"],[,,"6[1-9]\\d{6}","\\d{8}",,,"61234567"],[,,"(?:8[1-7]|9[0-8])\\d{6}","\\d{8}",,,"81234567"],[,,"1?800\\d{7}","\\d{10,11}",,,"18001234567"],
[,,"1900\\d{7}","\\d{11}",,,"19001234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"3[12]\\d{6}","\\d{8}",,,"31234567"],"SG",65,"0[0-3]\\d",,,,,,,,[[,"([3689]\\d{3})(\\d{4})","$1 $2",["[369]|8[1-9]"],"","",0],[,"(1[89]00)(\\d{3})(\\d{4})","$1 $2 $3",["1[89]"],"","",0],[,"(7000)(\\d{4})(\\d{3})","$1 $2 $3",["70"],"","",0],[,"(800)(\\d{3})(\\d{4})","$1 $2 $3",["80"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"7000\\d{7}","\\d{11}",,,"70001234567"],,,[,,"NA","NA"]],SH:[,[,,"[2-79]\\d{3,4}","\\d{4,5}"],[,,"2(?:[0-57-9]\\d|6[4-9])\\d{2}|(?:[2-46]\\d|7[01])\\d{2}",
"\\d{4,5}",,,"2158"],[,,"NA","NA"],[,,"NA","NA"],[,,"(?:[59]\\d|7[2-9])\\d{2}","\\d{4,5}",,,"5012"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"SH",290,"00",,,,,,,,,,[,,"NA","NA"],1,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],SI:[,[,,"[1-7]\\d{6,7}|[89]\\d{4,7}","\\d{5,8}"],[,,"(?:1\\d|[25][2-8]|3[4-8]|4[24-8]|7[3-8])\\d{6}","\\d{7,8}",,,"11234567"],[,,"(?:[37][01]|4[0139]|51|6[48])\\d{6}","\\d{8}",,,"31234567"],[,,"80\\d{4,6}","\\d{6,8}",,,"80123456"],[,,"90\\d{4,6}|89[1-3]\\d{2,5}","\\d{5,8}",
,,"90123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"(?:59|8[1-3])\\d{6}","\\d{8}",,,"59012345"],"SI",386,"00","0",,,"0",,,,[[,"(\\d)(\\d{3})(\\d{2})(\\d{2})","$1 $2 $3 $4",["[12]|3[4-8]|4[24-8]|5[2-8]|7[3-8]"],"(0$1)","",0],[,"([3-7]\\d)(\\d{3})(\\d{3})","$1 $2 $3",["[37][01]|4[0139]|51|6"],"0$1","",0],[,"([89][09])(\\d{3,6})","$1 $2",["[89][09]"],"0$1","",0],[,"([58]\\d{2})(\\d{5})","$1 $2",["59|8[1-3]"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],SJ:[,[,,"0\\d{4}|[4789]\\d{7}",
"\\d{5}(?:\\d{3})?"],[,,"79\\d{6}","\\d{8}",,,"79123456"],[,,"(?:4[015-8]|5[89]|9\\d)\\d{6}","\\d{8}",,,"41234567"],[,,"80[01]\\d{5}","\\d{8}",,,"80012345"],[,,"82[09]\\d{5}","\\d{8}",,,"82012345"],[,,"810(?:0[0-6]|[2-8]\\d)\\d{3}","\\d{8}",,,"81021234"],[,,"880\\d{5}","\\d{8}",,,"88012345"],[,,"85[0-5]\\d{5}","\\d{8}",,,"85012345"],"SJ",47,"00",,,,,,,,,,[,,"NA","NA"],,,[,,"NA","NA"],[,,"0\\d{4}|81(?:0(?:0[7-9]|1\\d)|5\\d{2})\\d{3}","\\d{5}(?:\\d{3})?",,,"01234"],1,,[,,"81[23]\\d{5}","\\d{8}",,,"81212345"]],
SK:[,[,,"[2-689]\\d{8}","\\d{9}"],[,,"[2-5]\\d{8}","\\d{9}",,,"212345678"],[,,"9(?:0[1-8]|1[0-24-9]|4[0489])\\d{6}","\\d{9}",,,"912123456"],[,,"800\\d{6}","\\d{9}",,,"800123456"],[,,"9(?:[78]\\d{7}|00\\d{6})","\\d{9}",,,"900123456"],[,,"8[5-9]\\d{7}","\\d{9}",,,"850123456"],[,,"NA","NA"],[,,"6(?:5[0-4]|9[0-6])\\d{6}","\\d{9}",,,"690123456"],"SK",421,"00","0",,,"0",,,,[[,"(2)(\\d{3})(\\d{3})(\\d{2})","$1/$2 $3 $4",["2"],"0$1","",0],[,"([3-5]\\d)(\\d{3})(\\d{2})(\\d{2})","$1/$2 $3 $4",["[3-5]"],"0$1",
"",0],[,"([689]\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["[689]"],"0$1","",0]],,[,,"NA","NA"],,,[,,"(?:8(?:00|[5-9]\\d)|9(?:00|[78]\\d))\\d{6}","\\d{9}",,,"800123456"],[,,"96\\d{7}","\\d{9}",,,"961234567"],,,[,,"NA","NA"]],SL:[,[,,"[2-578]\\d{7}","\\d{6,8}"],[,,"[235]2[2-4][2-9]\\d{4}","\\d{6,8}",,,"22221234"],[,,"(?:2[15]|3[034]|4[04]|5[05]|7[6-9]|88)\\d{6}","\\d{6,8}",,,"25123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"SL",232,"00","0",,,"0",,,,[[,"(\\d{2})(\\d{6})",
"$1 $2",,"(0$1)","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],SM:[,[,,"[05-7]\\d{7,9}","\\d{6,10}"],[,,"0549(?:8[0157-9]|9\\d)\\d{4}","\\d{6,10}",,,"0549886377"],[,,"6[16]\\d{6}","\\d{8}",,,"66661212"],[,,"NA","NA"],[,,"7[178]\\d{6}","\\d{8}",,,"71123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"5[158]\\d{6}","\\d{8}",,,"58001110"],"SM",378,"00",,,,"(?:0549)?([89]\\d{5})","0549$1",,,[[,"(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["[5-7]"],"","",0],[,"(0549)(\\d{6})","$1 $2",["0"],
"","",0],[,"(\\d{6})","0549 $1",["[89]"],"","",0]],[[,"(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["[5-7]","[5-7]"],"","",0],[,"(0549)(\\d{6})","($1) $2",["0"]],[,"(\\d{6})","(0549) $1",["[89]"]]],[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],1,,[,,"NA","NA"]],SN:[,[,,"[378]\\d{8}","\\d{9}"],[,,"3(?:0(?:1[0-2]|80)|211|3(?:8[1-9]|9[2-9])|90[1-5])\\d{5}","\\d{9}",,,"301012345"],[,,"7(?:[07]\\d|21|6[1-9]|8[0-26]|90)\\d{6}","\\d{9}",,,"701234567"],[,,"800\\d{6}","\\d{9}",,,"800123456"],[,,"88[4689]\\d{6}",
"\\d{9}",,,"884123456"],[,,"81[02468]\\d{6}","\\d{9}",,,"810123456"],[,,"NA","NA"],[,,"391\\d{6}","\\d{9}",,,"391011234"],"SN",221,"00",,,,,,,,[[,"(\\d{2})(\\d{3})(\\d{2})(\\d{2})","$1 $2 $3 $4",["[37]"],"","",0],[,"(\\d{3})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",["8"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],SO:[,[,,"[1-79]\\d{6,8}","\\d{7,9}"],[,,"(?:[134]\\d|2[0-79]|5[57-9])\\d{5}","\\d{7}",,,"5522010"],[,,"(?:15\\d|2(?:4\\d|8)|6[137-9]?\\d{2}|7\\d{2}|9(?:07|[13-9])\\d)\\d{5}",
"\\d{7,9}",,,"907792024"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"SO",252,"00","0",,,"0",,,,[[,"(\\d)(\\d{6})","$1 $2",["2[0-79]|[13-5]"],"","",0],[,"(\\d)(\\d{7})","$1 $2",["24|[67]"],"","",0],[,"(\\d{2})(\\d{5,7})","$1 $2",["15|28|6[1378]|9"],"","",0],[,"(69\\d)(\\d{6})","$1 $2",["69"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],SR:[,[,,"[2-8]\\d{5,6}","\\d{6,7}"],[,,"(?:2[1-3]|3[0-7]|4\\d|5[2-58]|68\\d)\\d{4}","\\d{6,7}",,,"211234"],[,
,"(?:7(?:[1-357]\\d|4[0-5])|8[1-9]\\d)\\d{4}","\\d{7}",,,"7412345"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"56\\d{4}","\\d{6}",,,"561234"],"SR",597,"00",,,,,,,,[[,"(\\d{3})(\\d{3})","$1-$2",["[2-4]|5[2-58]"],"","",0],[,"(\\d{2})(\\d{2})(\\d{2})","$1-$2-$3",["56"],"","",0],[,"(\\d{3})(\\d{4})","$1-$2",["[6-8]"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],SS:[,[,,"[19]\\d{8}","\\d{9}"],[,,"18\\d{7}","\\d{9}",,,"181234567"],[,,"(?:12|9[1257])\\d{7}","\\d{9}",
,,"977123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"SS",211,"00","0",,,"0",,,,[[,"(\\d{3})(\\d{3})(\\d{3})","$1 $2 $3",,"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],ST:[,[,,"[29]\\d{6}","\\d{7}"],[,,"22\\d{5}","\\d{7}",,,"2221234"],[,,"9[89]\\d{5}","\\d{7}",,,"9812345"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"ST",239,"00",,,,,,,,[[,"(\\d{3})(\\d{4})","$1 $2",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,
"NA","NA"],,,[,,"NA","NA"]],SV:[,[,,"[267]\\d{7}|[89]\\d{6}(?:\\d{4})?","\\d{7,8}|\\d{11}"],[,,"2[1-6]\\d{6}","\\d{8}",,,"21234567"],[,,"[67]\\d{7}","\\d{8}",,,"70123456"],[,,"800\\d{4}(?:\\d{4})?","\\d{7}(?:\\d{4})?",,,"8001234"],[,,"900\\d{4}(?:\\d{4})?","\\d{7}(?:\\d{4})?",,,"9001234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"SV",503,"00",,,,,,,,[[,"(\\d{4})(\\d{4})","$1 $2",["[267]"],"","",0],[,"(\\d{3})(\\d{4})","$1 $2",["[89]"],"","",0],[,"(\\d{3})(\\d{4})(\\d{4})","$1 $2 $3",["[89]"],"",
"",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],SX:[,[,,"[5789]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"7215(?:4[2-8]|8[239]|9[056])\\d{4}","\\d{7}(?:\\d{3})?",,,"7215425678"],[,,"7215(?:1[02]|2\\d|5[034679]|8[014-8])\\d{4}","\\d{10}",,,"7215205678"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002123456"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002123456"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"SX",1,"011","1",,,"1",,,,,,[,,"NA","NA"],
,"721",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],SY:[,[,,"[1-59]\\d{7,8}","\\d{6,9}"],[,,"(?:1(?:1\\d?|4\\d|[2356])|2(?:1\\d?|[235])|3(?:[13]\\d|4)|4[13]|5[1-3])\\d{6}","\\d{6,9}",,,"112345678"],[,,"9(?:22|[35][0-8]|4\\d|6[024-9]|88|9[0-489])\\d{6}","\\d{9}",,,"944567890"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"SY",963,"00","0",,,"0",,,,[[,"(\\d{2})(\\d{3})(\\d{3,4})","$1 $2 $3",["[1-5]"],"0$1","",1],[,"(9\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["9"],"0$1","",1]],,[,
,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],SZ:[,[,,"[027]\\d{7}","\\d{8}"],[,,"2(?:2(?:0[07]|[13]7|2[57])|3(?:0[34]|[1278]3|3[23]|[46][34])|(?:40[4-69]|67)|5(?:0[5-7]|1[6-9]|[23][78]|48|5[01]))\\d{4}","\\d{8}",,,"22171234"],[,,"7[6-8]\\d{6}","\\d{8}",,,"76123456"],[,,"0800\\d{4}","\\d{8}",,,"08001234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"SZ",268,"00",,,,,,,,[[,"(\\d{4})(\\d{4})","$1 $2",["[027]"],"","",0]],,[,,"NA","NA"],,,[,,"0800\\d{4}","\\d{8}",,,"08001234"],
[,,"NA","NA"],1,,[,,"NA","NA"]],TA:[,[,,"8\\d{3}","\\d{4}"],[,,"8\\d{3}","\\d{4}",,,"8999"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"TA",290,"00",,,,,,,,,,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],TC:[,[,,"[5689]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"649(?:712|9(?:4\\d|50))\\d{4}","\\d{7}(?:\\d{3})?",,,"6497121234"],[,,"649(?:2(?:3[129]|4[1-7])|3(?:3[1-389]|4[1-7])|4[34][1-3])\\d{4}","\\d{10}",,,"6492311234"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}",
"\\d{10}",,,"8002345678"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002345678"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"64971[01]\\d{4}","\\d{10}",,,"6497101234"],"TC",1,"011","1",,,"1",,,,,,[,,"NA","NA"],,"649",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],TD:[,[,,"[2679]\\d{7}","\\d{8}"],[,,"22(?:[3789]0|5[0-5]|6[89])\\d{4}","\\d{8}",,,"22501234"],[,,"(?:6[02368]\\d|77\\d|9(?:5[0-4]|9\\d))\\d{5}","\\d{8}",,,"63012345"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],
[,,"NA","NA"],"TD",235,"00|16",,,,,,"00",,[[,"(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],TG:[,[,,"[29]\\d{7}","\\d{8}"],[,,"2(?:2[2-7]|3[23]|44|55|66|77)\\d{5}","\\d{8}",,,"22212345"],[,,"9[0-289]\\d{6}","\\d{8}",,,"90112345"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"TG",228,"00",,,,,,,,[[,"(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3 $4",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],
,,[,,"NA","NA"]],TH:[,[,,"[2-9]\\d{7,8}|1\\d{3}(?:\\d{6})?","\\d{4}|\\d{8,10}"],[,,"(?:2[1-9]|3[2-9]|4[2-5]|5[2-6]|7[3-7])\\d{6}","\\d{8}",,,"21234567"],[,,"[89]\\d{8}","\\d{9}",,,"812345678"],[,,"1800\\d{6}","\\d{10}",,,"1800123456"],[,,"1900\\d{6}","\\d{10}",,,"1900123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"60\\d{7}","\\d{9}",,,"601234567"],"TH",66,"00","0",,,"0",,,,[[,"(2)(\\d{3})(\\d{4})","$1 $2 $3",["2"],"0$1","",0],[,"([3-9]\\d)(\\d{3})(\\d{3,4})","$1 $2 $3",["[3-9]"],"0$1","",0],[,"(1[89]00)(\\d{3})(\\d{3})",
"$1 $2 $3",["1"],"$1","",0]],,[,,"NA","NA"],,,[,,"1\\d{3}","\\d{4}",,,"1100"],[,,"1\\d{3}","\\d{4}",,,"1100"],,,[,,"NA","NA"]],TJ:[,[,,"[3-59]\\d{8}","\\d{3,9}"],[,,"(?:3(?:1[3-5]|2[245]|3[12]|4[24-7]|5[25]|72)|4(?:46|74|87))\\d{6}","\\d{3,9}",,,"372123456"],[,,"(?:50[15]|9[0-35-9]\\d)\\d{6}","\\d{9}",,,"917123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"TJ",992,"810","8",,,"8",,"8~10",,[[,"([349]\\d{2})(\\d{2})(\\d{4})","$1 $2 $3",["[34]7|91[78]"],"(8) $1","",1],[,
"([459]\\d)(\\d{3})(\\d{4})","$1 $2 $3",["4[48]|5|9(?:1[59]|[0235-9])"],"(8) $1","",1],[,"(331700)(\\d)(\\d{2})","$1 $2 $3",["331","3317","33170","331700"],"(8) $1","",1],[,"(\\d{4})(\\d)(\\d{4})","$1 $2 $3",["3[1-5]","3(?:[1245]|3(?:[02-9]|1[0-589]))"],"(8) $1","",1]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],TK:[,[,,"[2-9]\\d{3}","\\d{4}"],[,,"[2-4]\\d{3}","\\d{4}",,,"3010"],[,,"[5-9]\\d{3}","\\d{4}",,,"5190"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],
"TK",690,"00",,,,,,,,,,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],TL:[,[,,"[2-489]\\d{6}|7\\d{6,7}","\\d{7,8}"],[,,"(?:2[1-5]|3[1-9]|4[1-4])\\d{5}","\\d{7}",,,"2112345"],[,,"7[3-8]\\d{6}","\\d{8}",,,"77212345"],[,,"80\\d{5}","\\d{7}",,,"8012345"],[,,"90\\d{5}","\\d{7}",,,"9012345"],[,,"NA","NA"],[,,"70\\d{5}","\\d{7}",,,"7012345"],[,,"NA","NA"],"TL",670,"00",,,,,,,,[[,"(\\d{3})(\\d{4})","$1 $2",["[2-489]"],"","",0],[,"(\\d{4})(\\d{4})","$1 $2",["7"],"","",0]],,[,,"NA","NA"],,,[,
,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],TM:[,[,,"[1-6]\\d{7}","\\d{8}"],[,,"(?:1(?:2\\d|3[1-9])|2(?:22|4[0-35-8])|3(?:22|4[03-9])|4(?:22|3[128]|4\\d|6[15])|5(?:22|5[7-9]|6[014-689]))\\d{5}","\\d{8}",,,"12345678"],[,,"6[2-8]\\d{6}","\\d{8}",,,"66123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"TM",993,"810","8",,,"8",,"8~10",,[[,"(\\d{2})(\\d{2})(\\d{2})(\\d{2})","$1 $2-$3-$4",["12"],"(8 $1)","",0],[,"(\\d{2})(\\d{6})","$1 $2",["6"],"8 $1","",0],[,"(\\d{3})(\\d)(\\d{2})(\\d{2})",
"$1 $2-$3-$4",["13|[2-5]"],"(8 $1)","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],TN:[,[,,"[2-57-9]\\d{7}","\\d{8}"],[,,"3[012]\\d{6}|7\\d{7}|81200\\d{3}","\\d{8}",,,"71234567"],[,,"(?:[259]\\d|4[0-2])\\d{6}","\\d{8}",,,"20123456"],[,,"8010\\d{4}","\\d{8}",,,"80101234"],[,,"88\\d{6}","\\d{8}",,,"88123456"],[,,"8[12]10\\d{4}","\\d{8}",,,"81101234"],[,,"NA","NA"],[,,"NA","NA"],"TN",216,"00",,,,,,,,[[,"(\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],
[,,"NA","NA"],,,[,,"NA","NA"]],TO:[,[,,"[02-8]\\d{4,6}","\\d{5,7}"],[,,"(?:2\\d|3[1-8]|4[1-4]|[56]0|7[0149]|8[05])\\d{3}","\\d{5}",,,"20123"],[,,"(?:7[578]|8[7-9])\\d{5}","\\d{7}",,,"7715123"],[,,"0800\\d{3}","\\d{7}",,,"0800222"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"TO",676,"00",,,,,,,,[[,"(\\d{2})(\\d{3})","$1-$2",["[1-6]|7[0-4]|8[05]"],"","",0],[,"(\\d{3})(\\d{4})","$1 $2",["7[5-9]|8[7-9]"],"","",0],[,"(\\d{4})(\\d{3})","$1 $2",["0"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],
[,,"NA","NA"],1,,[,,"NA","NA"]],TR:[,[,,"[2-589]\\d{9}|444\\d{4}","\\d{7,10}"],[,,"(?:2(?:[13][26]|[28][2468]|[45][268]|[67][246])|3(?:[13][28]|[24-6][2468]|[78][02468]|92)|4(?:[16][246]|[23578][2468]|4[26]))\\d{7}","\\d{10}",,,"2123456789"],[,,"5(?:0[1-7]|22|[34]\\d|5[1-59]|9[246])\\d{7}","\\d{10}",,,"5012345678"],[,,"800\\d{7}","\\d{10}",,,"8001234567"],[,,"900\\d{7}","\\d{10}",,,"9001234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"TR",90,"00","0",,,"0",,,,[[,"(\\d{3})(\\d{3})(\\d{4})","$1 $2 $3",
["[23]|4(?:[0-35-9]|4[0-35-9])"],"(0$1)","",1],[,"(\\d{3})(\\d{3})(\\d{4})","$1 $2 $3",["[589]"],"0$1","",1],[,"(444)(\\d{1})(\\d{3})","$1 $2 $3",["444"],"","",0]],,[,,"512\\d{7}","\\d{10}",,,"5123456789"],,,[,,"444\\d{4}","\\d{7}",,,"4441444"],[,,"444\\d{4}|850\\d{7}","\\d{7,10}",,,"4441444"],,,[,,"NA","NA"]],TT:[,[,,"[589]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"868(?:2(?:01|2[1-5])|6(?:07|1[4-6]|2[1-9]|[3-6]\\d|7[0-79]|9[0-8])|82[12])\\d{4}","\\d{7}(?:\\d{3})?",,,"8682211234"],[,,"868(?:2(?:8[5-9]|9\\d)|3(?:0[1-9]|1[02-9]|[2-9]\\d)|4[6-9]\\d|6(?:20|78|8\\d)|7(?:1[02-9]|[02-9]\\d))\\d{4}",
"\\d{10}",,,"8682911234"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002345678"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002345678"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"TT",1,"011","1",,,"1",,,,,,[,,"NA","NA"],,"868",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],TV:[,[,,"[29]\\d{4,5}","\\d{5,6}"],[,,"2[02-9]\\d{3}","\\d{5}",,,"20123"],[,,"90\\d{4}","\\d{6}",,,"901234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"TV",688,
"00",,,,,,,,,,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],TW:[,[,,"[2-9]\\d{7,8}","\\d{8,9}"],[,,"[2-8]\\d{7,8}","\\d{8,9}",,,"21234567"],[,,"9\\d{8}","\\d{9}",,,"912345678"],[,,"800\\d{6}","\\d{9}",,,"800123456"],[,,"900\\d{6}","\\d{9}",,,"900123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"TW",886,"0(?:0[25679]|19)","0","#",,"0",,,,[[,"([2-8])(\\d{3,4})(\\d{4})","$1 $2 $3",["[2-7]|8[1-9]"],"0$1","",0],[,"([89]\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["80|9"],"0$1","",0]],,[,,"NA",
"NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],TZ:[,[,,"\\d{9}","\\d{7,9}"],[,,"2[2-8]\\d{7}","\\d{7,9}",,,"222345678"],[,,"(?:6[158]|7[1-9])\\d{7}","\\d{9}",,,"612345678"],[,,"80[08]\\d{6}","\\d{9}",,,"800123456"],[,,"90\\d{7}","\\d{9}",,,"900123456"],[,,"8(?:40|6[01])\\d{6}","\\d{9}",,,"840123456"],[,,"NA","NA"],[,,"41\\d{7}","\\d{9}",,,"412345678"],"TZ",255,"00[056]","0",,,"0",,,,[[,"([24]\\d)(\\d{3})(\\d{4})","$1 $2 $3",["[24]"],"0$1","",0],[,"([67]\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["[67]"],
"0$1","",0],[,"([89]\\d{2})(\\d{2})(\\d{4})","$1 $2 $3",["[89]"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],UA:[,[,,"[3-689]\\d{8}","\\d{5,9}"],[,,"(?:3[1-8]|4[13-8]|5[1-7]|6[12459])\\d{7}","\\d{5,9}",,,"311234567"],[,,"(?:39|50|6[36-8]|9[1-9])\\d{7}","\\d{9}",,,"391234567"],[,,"800\\d{6}","\\d{9}",,,"800123456"],[,,"900\\d{6}","\\d{9}",,,"900123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"89\\d{7}","\\d{9}",,,"891234567"],"UA",380,"00","0",,,"0",,"0~0",,[[,"([3-689]\\d)(\\d{3})(\\d{4})",
"$1 $2 $3",["[38]9|4(?:[45][0-5]|87)|5(?:0|6[37]|7[37])|6[36-8]|9[1-9]","[38]9|4(?:[45][0-5]|87)|5(?:0|6(?:3[14-7]|7)|7[37])|6[36-8]|9[1-9]"],"0$1","",0],[,"([3-689]\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["3[1-8]2|4[13678]2|5(?:[12457]2|6[24])|6(?:[49]2|[12][29]|5[24])|8[0-8]|90","3(?:[1-46-8]2[013-9]|52)|4(?:[1378]2|62[013-9])|5(?:[12457]2|6[24])|6(?:[49]2|[12][29]|5[24])|8[0-8]|90"],"0$1","",0],[,"([3-6]\\d{3})(\\d{5})","$1 $2",["3(?:5[013-9]|[1-46-8])|4(?:[137][013-9]|6|[45][6-9]|8[4-6])|5(?:[1245][013-9]|6[0135-9]|3|7[4-6])|6(?:[49][013-9]|5[0135-9]|[12][13-8])",
"3(?:5[013-9]|[1-46-8](?:22|[013-9]))|4(?:[137][013-9]|6(?:[013-9]|22)|[45][6-9]|8[4-6])|5(?:[1245][013-9]|6(?:3[02389]|[015689])|3|7[4-6])|6(?:[49][013-9]|5[0135-9]|[12][13-8])"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],UG:[,[,,"\\d{9}","\\d{5,9}"],[,,"20(?:[0147]\\d{2}|2(?:40|[5-9]\\d)|3[23]\\d|5[0-4]\\d|6[03]\\d|8[0-2]\\d)\\d{4}|[34]\\d{8}","\\d{5,9}",,,"312345678"],[,,"7(?:0[0-7]|[15789]\\d|[23]0|[46][0-4])\\d{6}","\\d{9}",,,"712345678"],[,,"800[123]\\d{5}","\\d{9}",
,,"800123456"],[,,"90[123]\\d{6}","\\d{9}",,,"901123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"UG",256,"00[057]","0",,,"0",,,,[[,"(\\d{3})(\\d{6})","$1 $2",["[7-9]|20(?:[013-8]|2[5-9])|4(?:6[45]|[7-9])"],"0$1","",0],[,"(\\d{2})(\\d{7})","$1 $2",["3|4(?:[1-5]|6[0-36-9])"],"0$1","",0],[,"(2024)(\\d{5})","$1 $2",["2024"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],US:[,[,,"[2-9]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"(?:2(?:0[1-35-9]|1[02-9]|2[4589]|3[149]|4[08]|5[1-46]|6[0279]|7[026]|8[13])|3(?:0[1-57-9]|1[02-9]|2[0135]|3[014679]|47|5[12]|6[014]|8[056])|4(?:0[124-9]|1[02-579]|2[3-5]|3[0245]|4[0235]|58|69|7[0589]|8[04])|5(?:0[1-57-9]|1[0235-8]|20|3[0149]|4[01]|5[19]|6[1-37]|7[013-5]|8[056])|6(?:0[1-35-9]|1[024-9]|2[036]|3[016]|4[16]|5[017]|6[0-279]|78|8[12])|7(?:0[1-46-8]|1[02-9]|2[0457]|3[1247]|4[07]|5[47]|6[02359]|7[02-59]|8[156])|8(?:0[1-68]|1[02-8]|28|3[0-25]|4[3578]|5[06-9]|6[02-5]|7[028])|9(?:0[1346-9]|1[02-9]|2[0589]|3[1678]|4[0179]|5[1246]|7[0-3589]|8[0459]))[2-9]\\d{6}",
"\\d{7}(?:\\d{3})?",,,"2015555555"],[,,"(?:2(?:0[1-35-9]|1[02-9]|2[4589]|3[149]|4[08]|5[1-46]|6[0279]|7[026]|8[13])|3(?:0[1-57-9]|1[02-9]|2[0135]|3[014679]|47|5[12]|6[014]|8[056])|4(?:0[124-9]|1[02-579]|2[3-5]|3[0245]|4[0235]|58|69|7[0589]|8[04])|5(?:0[1-57-9]|1[0235-8]|20|3[0149]|4[01]|5[19]|6[1-37]|7[013-5]|8[056])|6(?:0[1-35-9]|1[024-9]|2[036]|3[016]|4[16]|5[017]|6[0-279]|78|8[12])|7(?:0[1-46-8]|1[02-9]|2[0457]|3[1247]|4[07]|5[47]|6[02359]|7[02-59]|8[156])|8(?:0[1-68]|1[02-8]|28|3[0-25]|4[3578]|5[06-9]|6[02-5]|7[028])|9(?:0[1346-9]|1[02-9]|2[0589]|3[1678]|4[0179]|5[1246]|7[0-3589]|8[0459]))[2-9]\\d{6}",
"\\d{7}(?:\\d{3})?",,,"2015555555"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002345678"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002345678"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"US",1,"011","1",,,"1",,,1,[[,"(\\d{3})(\\d{4})","$1-$2",,"","",1],[,"(\\d{3})(\\d{3})(\\d{4})","($1) $2-$3",,"","",1]],[[,"(\\d{3})(\\d{3})(\\d{4})","$1-$2-$3"]],[,,"NA","NA"],1,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],UY:[,[,,"[2489]\\d{6,7}","\\d{7,8}"],[,,"2\\d{7}|4[2-7]\\d{6}",
"\\d{7,8}",,,"21231234"],[,,"9[1-9]\\d{6}","\\d{8}",,,"94231234"],[,,"80[05]\\d{4}","\\d{7}",,,"8001234"],[,,"90[0-8]\\d{4}","\\d{7}",,,"9001234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"UY",598,"0(?:1[3-9]\\d|0)","0"," int. ",,"0",,"00",,[[,"(\\d{4})(\\d{4})","$1 $2",["[24]"],"","",0],[,"(\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["9[1-9]"],"0$1","",0],[,"(\\d{3})(\\d{4})","$1 $2",["[89]0"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],UZ:[,[,,"[679]\\d{8}","\\d{7,9}"],
[,,"(?:6(?:1(?:22|3[124]|4[1-4]|5[123578]|64)|2(?:22|3[0-57-9]|41)|5(?:22|3[3-7]|5[024-8])|6\\d{2}|7(?:[23]\\d|7[69])|9(?:22|4[1-8]|6[135]))|7(?:0(?:5[4-9]|6[0146]|7[12456]|9[135-8])|1[12]\\d|2(?:22|3[1345789]|4[123579]|5[14])|3(?:2\\d|3[1578]|4[1-35-7]|5[1-57]|61)|4(?:2\\d|3[1-579]|7[1-79])|5(?:22|5[1-9]|6[1457])|6(?:22|3[12457]|4[13-8])|9(?:22|5[1-9])))\\d{5}","\\d{7,9}",,,"662345678"],[,,"6(?:1(?:2(?:98|2[01])|35[0-4]|50\\d|61[23]|7(?:[01][017]|4\\d|55|9[5-9]))|2(?:11\\d|2(?:[12]1|9[01379])|5(?:[126]\\d|3[0-4])|7\\d{2})|5(?:19[01]|2(?:27|9[26])|30\\d|59\\d|7\\d{2})|6(?:2(?:1[5-9]|2[0367]|38|41|52|60)|3[79]\\d|4(?:56|83)|7(?:[07]\\d|1[017]|3[07]|4[047]|5[057]|67|8[0178]|9[79])|9[0-3]\\d)|7(?:2(?:24|3[237]|4[5-9]|7[15-8])|5(?:7[12]|8[0589])|7(?:0\\d|[39][07])|9(?:0\\d|7[079]))|9(2(?:1[1267]|5\\d|3[01]|7[0-4])|5[67]\\d|6(?:2[0-26]|8\\d)|7\\d{2}))\\d{4}|7(?:0\\d{3}|1(?:13[01]|6(?:0[47]|1[67]|66)|71[3-69]|98\\d)|2(?:2(?:2[79]|95)|3(?:2[5-9]|6[0-6])|57\\d|7(?:0\\d|1[17]|2[27]|3[37]|44|5[057]|66|88))|3(?:2(?:1[0-6]|21|3[469]|7[159])|33\\d|5(?:0[0-4]|5[579]|9\\d)|7(?:[0-3579]\\d|4[0467]|6[67]|8[078])|9[4-6]\\d)|4(?:2(?:29|5[0257]|6[0-7]|7[1-57])|5(?:1[0-4]|8\\d|9[5-9])|7(?:0\\d|1[024589]|2[0127]|3[0137]|[46][07]|5[01]|7[5-9]|9[079])|9(?:7[015-9]|[89]\\d))|5(?:112|2(?:0\\d|2[29]|[49]4)|3[1568]\\d|52[6-9]|7(?:0[01578]|1[017]|[23]7|4[047]|[5-7]\\d|8[78]|9[079]))|6(?:2(?:2[1245]|4[2-4])|39\\d|41[179]|5(?:[349]\\d|5[0-2])|7(?:0[017]|[13]\\d|22|44|55|67|88))|9(?:22[128]|3(?:2[0-4]|7\\d)|57[05629]|7(?:2[05-9]|3[37]|4\\d|60|7[2579]|87|9[07])))\\d{4}|9[0-57-9]\\d{7}",
"\\d{7,9}",,,"912345678"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"UZ",998,"810","8",,,"8",,"8~10",,[[,"([679]\\d)(\\d{3})(\\d{2})(\\d{2})","$1 $2 $3 $4",,"8 $1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],VA:[,[,,"06\\d{8}","\\d{10}"],[,,"06698\\d{5}","\\d{10}",,,"0669812345"],[,,"N/A","N/A"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"VA",379,"00",,,,,,,,[[,"(06)(\\d{4})(\\d{4})","$1 $2 $3",,"","",0]],,[,,"NA","NA"],
,,[,,"NA","NA"],[,,"NA","NA"],1,,[,,"NA","NA"]],VC:[,[,,"[5789]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"784(?:266|3(?:6[6-9]|7\\d|8[0-24-6])|4(?:38|5[0-36-8]|8\\d|9[01])|555|638|784)\\d{4}","\\d{7}(?:\\d{3})?",,,"7842661234"],[,,"784(?:4(?:3[0-4]|5[45]|9[2-5])|5(?:2[6-9]|3[0-4]|93))\\d{4}","\\d{10}",,,"7844301234"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002345678"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002345678"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],
"VC",1,"011","1",,,"1",,,,,,[,,"NA","NA"],,"784",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],VE:[,[,,"[24589]\\d{9}","\\d{7,10}"],[,,"(?:2(?:12|3[457-9]|[58][1-9]|[467]\\d|9[1-6])|50[01])\\d{7}","\\d{7,10}",,,"2121234567"],[,,"4(?:1[24-8]|2[46])\\d{7}","\\d{10}",,,"4121234567"],[,,"800\\d{7}","\\d{10}",,,"8001234567"],[,,"900\\d{7}","\\d{10}",,,"9001234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"VE",58,"00","0",,,"0",,,,[[,"(\\d{3})(\\d{7})","$1-$2",,"0$1","$CC $1",0]],,[,,"NA","NA"],,,[,,"NA",
"NA"],[,,"NA","NA"],,,[,,"NA","NA"]],VG:[,[,,"[2589]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"284(?:(?:229|4(?:22|9[45])|774|8(?:52|6[459]))\\d{4}|496[0-5]\\d{3})","\\d{7}(?:\\d{3})?",,,"2842291234"],[,,"284(?:(?:3(?:0[0-3]|4[0-367])|4(?:4[0-6]|68|99)|54[0-57])\\d{4}|496[6-9]\\d{3})","\\d{10}",,,"2843001234"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}","\\d{10}",,,"8002345678"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002345678"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"VG",
1,"011","1",,,"1",,,,,,[,,"NA","NA"],,"284",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],VI:[,[,,"[3589]\\d{9}","\\d{7}(?:\\d{3})?"],[,,"340(?:2(?:01|2[067]|36|44|77)|3(?:32|44)|4(?:4[38]|7[34])|5(?:1[34]|55)|6(?:26|4[23]|77|9[023])|7(?:[17]\\d|27)|884|998)\\d{4}","\\d{7}(?:\\d{3})?",,,"3406421234"],[,,"340(?:2(?:01|2[067]|36|44|77)|3(?:32|44)|4(?:4[38]|7[34])|5(?:1[34]|55)|6(?:26|4[23]|77|9[023])|7(?:[17]\\d|27)|884|998)\\d{4}","\\d{7}(?:\\d{3})?",,,"3406421234"],[,,"8(?:00|44|55|66|77|88)[2-9]\\d{6}",
"\\d{10}",,,"8002345678"],[,,"900[2-9]\\d{6}","\\d{10}",,,"9002345678"],[,,"NA","NA"],[,,"5(?:00|33|44)[2-9]\\d{6}","\\d{10}",,,"5002345678"],[,,"NA","NA"],"VI",1,"011","1",,,"1",,,1,,,[,,"NA","NA"],,"340",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],VN:[,[,,"[17]\\d{6,9}|[2-69]\\d{7,9}|8\\d{6,8}","\\d{7,10}"],[,,"(?:2(?:[025-79]|1[0189]|[348][01])|3(?:[0136-9]|[25][01])|4\\d|5(?:[01][01]|[2-9])|6(?:[0-46-8]|5[01])|7(?:[02-79]|[18][01])|8[1-9])\\d{7}","\\d{9,10}",,,"2101234567"],[,,"(?:9\\d|1(?:2\\d|6[2-9]|8[68]|99))\\d{7}",
"\\d{9,10}",,,"912345678"],[,,"1800\\d{4,6}","\\d{8,10}",,,"1800123456"],[,,"1900\\d{4,6}","\\d{8,10}",,,"1900123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"VN",84,"00","0",,,"0",,,,[[,"([17]99)(\\d{4})","$1 $2",["[17]99"],"0$1","",1],[,"([48])(\\d{4})(\\d{4})","$1 $2 $3",["[48]"],"0$1","",1],[,"([235-7]\\d)(\\d{4})(\\d{3})","$1 $2 $3",["2[025-79]|3[0136-9]|5[2-9]|6[0-46-8]|7[02-79]"],"0$1","",1],[,"(80)(\\d{5})","$1 $2",["80"],"0$1","",1],[,"(69\\d)(\\d{4,5})","$1 $2",["69"],"0$1","",1],[,"([235-7]\\d{2})(\\d{4})(\\d{3})",
"$1 $2 $3",["2[1348]|3[25]|5[01]|65|7[18]"],"0$1","",1],[,"(9\\d)(\\d{3})(\\d{2})(\\d{2})","$1 $2 $3 $4",["9"],"0$1","",1],[,"(1[2689]\\d)(\\d{3})(\\d{4})","$1 $2 $3",["1(?:[26]|8[68]|99)"],"0$1","",1],[,"(1[89]00)(\\d{4,6})","$1 $2",["1[89]0"],"$1","",1]],,[,,"NA","NA"],,,[,,"[17]99\\d{4}|69\\d{5,6}","\\d{7,8}",,,"1992000"],[,,"[17]99\\d{4}|69\\d{5,6}|80\\d{5}","\\d{7,8}",,,"1992000"],,,[,,"NA","NA"]],VU:[,[,,"[2-57-9]\\d{4,6}","\\d{5,7}"],[,,"(?:2[02-9]\\d|3(?:[5-7]\\d|8[0-8])|48[4-9]|88\\d)\\d{2}",
"\\d{5}",,,"22123"],[,,"(?:5(?:7[2-5]|[3-69]\\d)|7[013-7]\\d)\\d{4}","\\d{7}",,,"5912345"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"VU",678,"00",,,,,,,,[[,"(\\d{3})(\\d{4})","$1 $2",["[579]"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"3[03]\\d{3}|900\\d{4}","\\d{5,7}",,,"30123"],,,[,,"NA","NA"]],WF:[,[,,"[5-7]\\d{5}","\\d{6}"],[,,"(?:50|68|72)\\d{4}","\\d{6}",,,"501234"],[,,"(?:50|68|72)\\d{4}","\\d{6}",,,"501234"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA",
"NA"],[,,"NA","NA"],"WF",681,"00",,,,,,,1,[[,"(\\d{2})(\\d{2})(\\d{2})","$1 $2 $3",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],WS:[,[,,"[2-8]\\d{4,6}","\\d{5,7}"],[,,"(?:[2-5]\\d|6[1-9]|84\\d{2})\\d{3}","\\d{5,7}",,,"22123"],[,,"(?:60|7[25-7]\\d)\\d{4}","\\d{6,7}",,,"601234"],[,,"800\\d{3}","\\d{6}",,,"800123"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"WS",685,"0",,,,,,,,[[,"(8\\d{2})(\\d{3,4})","$1 $2",["8"],"","",0],[,"(7\\d)(\\d{5})","$1 $2",["7"],"",
"",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],YE:[,[,,"[1-7]\\d{6,8}","\\d{6,9}"],[,,"(?:1(?:7\\d|[2-68])|2[2-68]|3[2358]|4[2-58]|5[2-6]|6[3-58]|7[24-68])\\d{5}","\\d{6,8}",,,"1234567"],[,,"7[0137]\\d{7}","\\d{9}",,,"712345678"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"YE",967,"00","0",,,"0",,,,[[,"([1-7])(\\d{3})(\\d{3,4})","$1 $2 $3",["[1-6]|7[24-68]"],"0$1","",0],[,"(7\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["7[0137]"],"0$1","",0]],,[,,"NA","NA"],
,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],YT:[,[,,"[268]\\d{8}","\\d{9}"],[,,"2696[0-4]\\d{4}","\\d{9}",,,"269601234"],[,,"639\\d{6}","\\d{9}",,,"639123456"],[,,"80\\d{7}","\\d{9}",,,"801234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"YT",262,"00","0",,,"0",,,,,,[,,"NA","NA"],,"269|63",[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],ZA:[,[,,"[1-79]\\d{8}|8(?:[067]\\d{7}|[1-4]\\d{3,7})","\\d{5,9}"],[,,"(?:1[0-8]|2[0-378]|3[1-69]|4\\d|5[1346-8])\\d{7}","\\d{9}",,,"101234567"],[,
,"(?:6[0-5]|7[0-46-9])\\d{7}|8[1-4]\\d{3,7}","\\d{5,9}",,,"711234567"],[,,"80\\d{7}","\\d{9}",,,"801234567"],[,,"86[2-9]\\d{6}|90\\d{7}","\\d{9}",,,"862345678"],[,,"860\\d{6}","\\d{9}",,,"860123456"],[,,"NA","NA"],[,,"87\\d{7}","\\d{9}",,,"871234567"],"ZA",27,"00","0",,,"0",,,,[[,"(860)(\\d{3})(\\d{3})","$1 $2 $3",["860"],"0$1","",0],[,"(\\d{2})(\\d{3})(\\d{4})","$1 $2 $3",["[1-79]|8(?:[0-47]|6[1-9])"],"0$1","",0],[,"(\\d{2})(\\d{3,4})","$1 $2",["8[1-4]"],"0$1","",0],[,"(\\d{2})(\\d{3})(\\d{2,3})",
"$1 $2 $3",["8[1-4]"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"861\\d{6}","\\d{9}",,,"861123456"],,,[,,"NA","NA"]],ZM:[,[,,"[289]\\d{8}","\\d{9}"],[,,"21[1-8]\\d{6}","\\d{9}",,,"211234567"],[,,"9(?:5[05]|6\\d|7[13-9])\\d{6}","\\d{9}",,,"955123456"],[,,"800\\d{6}","\\d{9}",,,"800123456"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"ZM",260,"00","0",,,"0",,,,[[,"([29]\\d)(\\d{7})","$1 $2",["[29]"],"0$1","",0],[,"(800)(\\d{3})(\\d{3})","$1 $2 $3",["8"],"0$1","",0]],,[,,"NA","NA"],
,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],ZW:[,[,,"2(?:[012457-9]\\d{3,8}|6\\d{3,6})|[13-79]\\d{4,8}|8[06]\\d{8}","\\d{3,10}"],[,,"(?:1[3-9]|2(?:0[45]|[16]|2[28]|[49]8?|58[23]|7[246]|8[1346-9])|3(?:08?|17?|3[78]|[2456]|7[1569]|8[379])|5(?:[07-9]|1[78]|483|5(?:7?|8))|6(?:0|28|37?|[45][68][78]|98?)|848)\\d{3,6}|(?:2(?:27|5|7[135789]|8[25])|3[39]|5[1-46]|6[126-8])\\d{4,6}|2(?:(?:0|70)\\d{5,6}|2[05]\\d{7})|(?:4\\d|9[2-8])\\d{4,7}","\\d{3,10}",,,"1312345"],[,,"7[1378]\\d{7}|86(?:22|44)\\d{6}","\\d{9,10}",
,,"711234567"],[,,"800\\d{7}","\\d{10}",,,"8001234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"86(?:1[12]|30|55|77|8[367]|99)\\d{6}","\\d{10}",,,"8686123456"],"ZW",263,"00","0",,,"0",,,,[[,"([49])(\\d{3})(\\d{2,5})","$1 $2 $3",["4|9[2-9]"],"0$1","",0],[,"([179]\\d)(\\d{3})(\\d{3,4})","$1 $2 $3",["[19]1|7"],"0$1","",0],[,"(86\\d{2})(\\d{3})(\\d{3})","$1 $2 $3",["86[24]"],"0$1","",0],[,"([2356]\\d{2})(\\d{3,5})","$1 $2",["2(?:[278]|0[45]|[49]8)|3(?:08|17|3[78]|[78])|5[15][78]|6(?:[29]8|37|[68][78])"],
"0$1","",0],[,"(\\d{3})(\\d{3})(\\d{3,4})","$1 $2 $3",["2(?:[278]|0[45]|48)|3(?:08|17|3[78]|[78])|5[15][78]|6(?:[29]8|37|[68][78])|80"],"0$1","",0],[,"([1-356]\\d)(\\d{3,5})","$1 $2",["1[3-9]|2(?:[1-469]|0[0-35-9]|[45][0-79])|3(?:0[0-79]|1[0-689]|[24-69]|3[0-69])|5(?:[02-46-9]|[15][0-69])|6(?:[0145]|[29][0-79]|3[0-689]|[68][0-69])"],"0$1","",0],[,"([1-356]\\d)(\\d{3})(\\d{3})","$1 $2 $3",["1[3-9]|2(?:[1-469]|0[0-35-9]|[45][0-79])|3(?:0[0-79]|1[0-689]|[24-69]|3[0-69])|5(?:[02-46-9]|[15][0-69])|6(?:[0145]|[29][0-79]|3[0-689]|[68][0-69])"],
"0$1","",0],[,"([25]\\d{3})(\\d{3,5})","$1 $2",["(?:25|54)8","258[23]|5483"],"0$1","",0],[,"([25]\\d{3})(\\d{3})(\\d{3})","$1 $2 $3",["(?:25|54)8","258[23]|5483"],"0$1","",0],[,"(8\\d{3})(\\d{6})","$1 $2",["86"],"0$1","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],800:[,[,,"\\d{8}","\\d{8}",,,"12345678"],[,,"NA","NA",,,"12345678"],[,,"NA","NA",,,"12345678"],[,,"\\d{8}","\\d{8}",,,"12345678"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"001",800,"",,,,,,,1,[[,"(\\d{4})(\\d{4})",
"$1 $2",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],1,,[,,"NA","NA"]],808:[,[,,"\\d{8}","\\d{8}",,,"12345678"],[,,"NA","NA",,,"12345678"],[,,"NA","NA",,,"12345678"],[,,"NA","NA"],[,,"NA","NA"],[,,"\\d{8}","\\d{8}",,,"12345678"],[,,"NA","NA"],[,,"NA","NA"],"001",808,"",,,,,,,1,[[,"(\\d{4})(\\d{4})","$1 $2",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],1,,[,,"NA","NA"]],870:[,[,,"[35-7]\\d{8}","\\d{9}",,,"301234567"],[,,"NA","NA",,,"301234567"],[,,"(?:[356]\\d|7[6-8])\\d{7}","\\d{9}",
,,"301234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"001",870,"",,,,,,,,[[,"(\\d{3})(\\d{3})(\\d{3})","$1 $2 $3",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],878:[,[,,"1\\d{11}","\\d{12}",,,"101234567890"],[,,"NA","NA",,,"101234567890"],[,,"NA","NA",,,"101234567890"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"10\\d{10}","\\d{12}",,,"101234567890"],"001",878,"",,,,,,,1,[[,"(\\d{2})(\\d{5})(\\d{5})","$1 $2 $3",,"","",0]],
,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],881:[,[,,"[67]\\d{8}","\\d{9}",,,"612345678"],[,,"NA","NA",,,"612345678"],[,,"[67]\\d{8}","\\d{9}",,,"612345678"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"001",881,"",,,,,,,,[[,"(\\d)(\\d{3})(\\d{5})","$1 $2 $3",["[67]"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],882:[,[,,"[13]\\d{6,11}","\\d{7,12}",,,"3451234567"],[,,"NA","NA",,,"3451234567"],[,,"3(?:2\\d{3}|37\\d{2}|4(?:2|7\\d{3}))\\d{4}",
"\\d{7,10}",,,"3451234567"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"1(?:3(?:0[0347]|[13][0139]|2[035]|4[013568]|6[0459]|7[06]|8[15678]|9[0689])\\d{4}|6\\d{5,10})|345\\d{7}","\\d{7,12}",,,"3451234567"],"001",882,"",,,,,,,,[[,"(\\d{2})(\\d{4})(\\d{3})","$1 $2 $3",["3[23]"],"","",0],[,"(\\d{2})(\\d{5})","$1 $2",["16|342"],"","",0],[,"(\\d{2})(\\d{4})(\\d{4})","$1 $2 $3",["34[57]"],"","",0],[,"(\\d{3})(\\d{4})(\\d{4})","$1 $2 $3",["348"],"","",0],[,"(\\d{2})(\\d{2})(\\d{4})","$1 $2 $3",
["1"],"","",0],[,"(\\d{2})(\\d{3,4})(\\d{4})","$1 $2 $3",["16"],"","",0],[,"(\\d{2})(\\d{4,5})(\\d{5})","$1 $2 $3",["16"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"348[57]\\d{7}","\\d{11}",,,"3451234567"]],883:[,[,,"51\\d{7}(?:\\d{3})?","\\d{9}(?:\\d{3})?",,,"510012345"],[,,"NA","NA",,,"510012345"],[,,"NA","NA",,,"510012345"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"51(?:00\\d{5}(?:\\d{3})?|[13]0\\d{8})","\\d{9}(?:\\d{3})?",,,"510012345"],"001",883,"",,,,,,,1,
[[,"(\\d{3})(\\d{3})(\\d{3})","$1 $2 $3",["510"],"","",0],[,"(\\d{3})(\\d{3})(\\d{3})(\\d{3})","$1 $2 $3 $4",["510"],"","",0],[,"(\\d{4})(\\d{4})(\\d{4})","$1 $2 $3",["51[13]"],"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],,,[,,"NA","NA"]],888:[,[,,"\\d{11}","\\d{11}",,,"12345678901"],[,,"NA","NA",,,"12345678901"],[,,"NA","NA",,,"12345678901"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"001",888,"",,,,,,,1,[[,"(\\d{3})(\\d{3})(\\d{5})","$1 $2 $3",,"","",0]],,[,
,"NA","NA"],,,[,,"NA","NA"],[,,"\\d{11}","\\d{11}",,,"12345678901"],1,,[,,"NA","NA"]],979:[,[,,"\\d{9}","\\d{9}",,,"123456789"],[,,"NA","NA",,,"123456789"],[,,"NA","NA",,,"123456789"],[,,"NA","NA"],[,,"\\d{9}","\\d{9}",,,"123456789"],[,,"NA","NA"],[,,"NA","NA"],[,,"NA","NA"],"001",979,"",,,,,,,1,[[,"(\\d)(\\d{4})(\\d{4})","$1 $2 $3",,"","",0]],,[,,"NA","NA"],,,[,,"NA","NA"],[,,"NA","NA"],1,,[,,"NA","NA"]]};/*

 Copyright (C) 2010 The Libphonenumber Authors.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function J(){this.$={}}J.ba=function(){return J.v?J.v:J.v=new J};
var ua={"0":"0",1:"1",2:"2",3:"3",4:"4",5:"5",6:"6",7:"7",8:"8",9:"9","\uff10":"0","\uff11":"1","\uff12":"2","\uff13":"3","\uff14":"4","\uff15":"5","\uff16":"6","\uff17":"7","\uff18":"8","\uff19":"9","\u0660":"0","\u0661":"1","\u0662":"2","\u0663":"3","\u0664":"4","\u0665":"5","\u0666":"6","\u0667":"7","\u0668":"8","\u0669":"9","\u06f0":"0","\u06f1":"1","\u06f2":"2","\u06f3":"3","\u06f4":"4","\u06f5":"5","\u06f6":"6","\u06f7":"7","\u06f8":"8","\u06f9":"9"},va={"0":"0",1:"1",2:"2",3:"3",4:"4",5:"5",
6:"6",7:"7",8:"8",9:"9","\uff10":"0","\uff11":"1","\uff12":"2","\uff13":"3","\uff14":"4","\uff15":"5","\uff16":"6","\uff17":"7","\uff18":"8","\uff19":"9","\u0660":"0","\u0661":"1","\u0662":"2","\u0663":"3","\u0664":"4","\u0665":"5","\u0666":"6","\u0667":"7","\u0668":"8","\u0669":"9","\u06f0":"0","\u06f1":"1","\u06f2":"2","\u06f3":"3","\u06f4":"4","\u06f5":"5","\u06f6":"6","\u06f7":"7","\u06f8":"8","\u06f9":"9",A:"2",B:"2",C:"2",D:"3",E:"3",F:"3",G:"4",H:"4",I:"4",J:"5",K:"5",L:"5",M:"6",N:"6",O:"6",
P:"7",Q:"7",R:"7",S:"7",T:"8",U:"8",V:"8",W:"9",X:"9",Y:"9",Z:"9"},K=RegExp("^[+\uff0b]+"),wa=RegExp("([0-9\uff10-\uff19\u0660-\u0669\u06f0-\u06f9])"),xa=RegExp("[+\uff0b0-9\uff10-\uff19\u0660-\u0669\u06f0-\u06f9]"),ya=/[\\\/] *x/,Ja=RegExp("[^0-9\uff10-\uff19\u0660-\u0669\u06f0-\u06f9A-Za-z#]+$"),Ka=/(?:.*?[A-Za-z]){3}.*/,La=RegExp("(?:;ext=([0-9\uff10-\uff19\u0660-\u0669\u06f0-\u06f9]{1,7})|[ \u00a0\\t,]*(?:e?xt(?:ensi(?:o\u0301?|\u00f3))?n?|\uff45?\uff58\uff54\uff4e?|[,x\uff58#\uff03~\uff5e]|int|anexo|\uff49\uff4e\uff54)[:\\.\uff0e]?[ \u00a0\\t,-]*([0-9\uff10-\uff19\u0660-\u0669\u06f0-\u06f9]{1,7})#?|[- ]+([0-9\uff10-\uff19\u0660-\u0669\u06f0-\u06f9]{1,5})#)$",
"i"),Ma=RegExp("^[0-9\uff10-\uff19\u0660-\u0669\u06f0-\u06f9]{2}$|^[+\uff0b]*(?:[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*]*[0-9\uff10-\uff19\u0660-\u0669\u06f0-\u06f9]){3,}[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*A-Za-z0-9\uff10-\uff19\u0660-\u0669\u06f0-\u06f9]*(?:;ext=([0-9\uff10-\uff19\u0660-\u0669\u06f0-\u06f9]{1,7})|[ \u00a0\\t,]*(?:e?xt(?:ensi(?:o\u0301?|\u00f3))?n?|\uff45?\uff58\uff54\uff4e?|[,x\uff58#\uff03~\uff5e]|int|anexo|\uff49\uff4e\uff54)[:\\.\uff0e]?[ \u00a0\\t,-]*([0-9\uff10-\uff19\u0660-\u0669\u06f0-\u06f9]{1,7})#?|[- ]+([0-9\uff10-\uff19\u0660-\u0669\u06f0-\u06f9]{1,5})#)?$",
"i");function Na(a){var b=a.search(xa);0<=b?(a=a.substring(b),a=a.replace(Ja,""),b=a.search(ya),0<=b&&(a=a.substring(0,b))):a="";return a}function Oa(a){var b=L(Ka,a.toString())?M(a.toString(),va):M(a.toString(),ua);a.clear();a.append(b)}function M(a,b){for(var c=new B,d,e=a.length,g=0;g<e;++g)d=a.charAt(g),d=b[d.toUpperCase()],d!=j&&c.append(d);return c.toString()}function Pa(a){var b=""+t(a,2);return s(a,4)&&t(a,4)?Array(u(a,8)+1).join("0")+b:b}
function Qa(a,b){var c=t(b,1);return!s(c,2)||!R(a,c)?-1:R(a,t(b,5))?4:R(a,t(b,4))?3:R(a,t(b,6))?5:R(a,t(b,8))?6:R(a,t(b,7))?7:R(a,t(b,21))?8:R(a,t(b,25))?9:R(a,t(b,28))?10:R(a,t(b,2))?t(b,18)||R(a,t(b,3))?2:0:!t(b,18)&&R(a,t(b,3))?1:-1}function S(a,b){if(b==j)return j;b=b.toUpperCase();var c=a.$[b];if(c==j){c=ta[b];if(c==j)return j;c=(new A).g(H.c(),c);a.$[b]=c}return c}function R(a,b){return L(u(b,3),a)&&L(u(b,2),a)}
function Ra(a,b,c,d){if(0==a.length)return 0;a=new B(a);var e;b!=j&&(e=t(b,11));e==j&&(e="NonMatch");var g=a.toString();if(0==g.length)e=20;else if(K.test(g))g=g.replace(K,""),a.clear(),a.append(L(Ka,g)?M(g,va):M(g,ua)),e=1;else{g=RegExp(e);Oa(a);e=a.toString();if(0==e.search(g)){var g=e.match(g)[0].length,h=e.substring(g).match(wa);h&&(h[1]!=j&&0<h[1].length)&&"0"==M(h[1],ua)?e=l:(a.clear(),a.append(e.substring(g)),e=i)}else e=l;e=e?5:20}q(d,6,e);if(20!=e){2>=a.b.length&&f("Phone number too short after IDD");
a:{e=a.toString();if(!(0==e.length||"0"==e.charAt(0)))for(var h=e.length,k=1;3>=k&&k<=h;++k)if(g=parseInt(e.substring(0,k),10),g in sa){c.append(e.substring(k));e=g;break a}e=0}if(0!=e)return d.j(e),e;f("Invalid country calling code")}if(b!=j&&(e=b.f(),g=""+e,h=a.toString(),0==h.lastIndexOf(g,0)&&(k=new B(h.substring(g.length)),h=t(b,1),g=RegExp(u(h,2)),Sa(k,b,j),b=k.toString(),h=u(h,3),!L(g,a.toString())&&L(g,b)||3==(L(h,a.toString())?0:0==a.toString().search(h)?3:2))))return c.append(b),q(d,6,10),
d.j(e),e;d.j(0);return 0}function Sa(a,b,c){var d=a.toString(),e=d.length,g=t(b,15);if(!(0==e||g==j||0==g.length))if(g=RegExp("^(?:"+g+")"),e=g.exec(d)){var h=RegExp,k;k=t(b,1);k=u(k,2);h=h(k);k=L(h,d);var y=e.length-1;b=t(b,16);if(b==j||0==b.length||e[y]==j||0==e[y].length){if(!k||L(h,d.substring(e[0].length)))c!=j&&(0<y&&e[y]!=j)&&c.append(e[1]),a.set(d.substring(e[0].length))}else if(d=d.replace(g,b),!k||L(h,d))c!=j&&0<y&&c.append(e[1]),a.set(d)}}
function L(a,b){var c="string"==typeof a?b.match("^(?:"+a+")$"):b.match(a);return c&&c[0].length==b.length?i:l};function Ta(a,b){try{var c=J.ba();!(b!=j&&isNaN(b)&&b.toUpperCase()in ta)&&(0<a.length&&"+"!=a.charAt(0))&&f("Invalid country calling code");a==j&&f("The string supplied did not seem to be a phone number");250<a.length&&f("The string supplied is too long to be a phone number");var d=new B,e=a.indexOf(";phone-context=");if(0<e){var g=e+15;if("+"==a.charAt(g)){var h=a.indexOf(";",g);0<h?d.append(a.substring(g,h)):d.append(a.substring(g))}d.append(a.substring(a.indexOf("tel:")+4,e))}else d.append(Na(a));
var k=d.toString(),y=k.indexOf(";isub=");0<y&&(d.clear(),d.append(k.substring(0,y)));(2>d.toString().length?0:L(Ma,d.toString()))||f("The string supplied did not seem to be a phone number");b!=j&&isNaN(b)&&b.toUpperCase()in ta||d.toString()!=j&&0<d.toString().length&&K.test(d.toString())||f("Invalid country calling code");var m=new I;q(m,5,a);var r;a:{var N=d.toString(),V=N.search(La);if(0<=V&&(2>N.substring(0,V).length?0:L(Ma,N.substring(0,V))))for(var W=N.match(La),Ua=W.length,e=1;e<Ua;++e)if(W[e]!=
j&&0<W[e].length){d.clear();d.append(N.substring(0,V));r=W[e];break a}r=""}0<r.length&&q(m,3,r);var C=S(c,b),D=new B;r=0;var O=d.toString();try{r=Ra(O,C,D,m)}catch(da){"Invalid country calling code"==da&&K.test(O)?(O=O.replace(K,""),r=Ra(O,C,D,m),0==r&&f(da)):f(da)}if(0!=r){var X,za=sa[r];X=za==j?"ZZ":za[0];X!=b&&(C="001"==X?S(c,""+r):S(c,X))}else Oa(d),D.append(d.toString()),b!=j?(r=C.f(),m.j(r)):pa(m,6);2>D.b.length&&f("The string supplied is too short to be a phone number");if(C!=j){var Aa=new B,
ea=new B(D.toString());Sa(ea,C,Aa);var Ba=ea.toString(),fa,Va=t(C,1);fa=u(Va,3);if(2!=(L(fa,Ba)?0:0==Ba.search(fa)?3:2))D=ea,q(m,7,Aa.toString())}var w=D.toString(),Ca=w.length;2>Ca&&f("The string supplied is too short to be a phone number");16<Ca&&f("The string supplied is too long to be a phone number");if(1<w.length&&"0"==w.charAt(0)){q(m,4,i);for(d=1;d<w.length-1&&"0"==w.charAt(d);)d++;1!=d&&q(m,8,d)}q(m,2,parseInt(w,10));var E;if(m==j)E=j;else{var Wa=m.f(),P=sa[Wa],ga;if(P==j)ga=j;else{var Q;
if(1==P.length)Q=P[0];else b:{for(var Da=Pa(m),Y,Xa=P.length,w=0;w<Xa;w++){Y=P[w];var ha=S(c,Y);if(s(ha,23)){if(0==Da.search(t(ha,23))){Q=Y;break b}}else if(-1!=Qa(Da,ha)){Q=Y;break b}}Q=j}ga=Q}E=ga}var Z;var Ea=m.f(),ia="001"==E?S(c,""+Ea):S(c,E),ja;if(!(ja=ia==j)){var ka;if(ka="001"!=E){var Fa,Ga=S(c,E);Ga==j&&f("Invalid region code: "+E);Fa=Ga.f();ka=Ea!=Fa}ja=ka}if(ja)Z=l;else{var Ya=t(ia,1),Ha=Pa(m);if(s(Ya,2))Z=-1!=Qa(Ha,ia);else{var Ia=Ha.length;Z=2<Ia&&16>=Ia}}return Z}catch(Za){}return l}
var T=["isValidNumber"],U=this;!(T[0]in U)&&U.execScript&&U.execScript("var "+T[0]);for(var $;T.length&&($=T.shift());)!T.length&&void 0!==Ta?U[$]=Ta:U=U[$]?U[$]:U[$]={};})();

'use strict';

// Listing all APIs
angular.module('myApp.url',[]).constant('URLS',{
  'HOME': '/',
  'ABOUT': '/about',
  'ADMIN_DASHBOARD': '/admin-dashboard',
  'REVIEW_COMPANY_INFO':'/review-company-info/',
  'REVIEW_COMPANY_INFO_II': '/review-complete-company-info/',
  'SIGN_IN': '/sign-in',
  'ENT_REGISTRATION': '/ent-registration',
  'SUCCESSFUL_SUBMISSION':'/successful-submission',
  'INVESTOR_REGISTRATION':'/investor-registration',
  'REVIEW_INVESTOR_FORM': '/review-investor-info-form/',
  'REVIEW_ACCREDITED_QUESTIONS': '/review-accredited-questions/',
  'REVIEW_QUALIFIED_QUESTIONS': '/review-qualified-questions/',
  'REVIEW_INVESTOR_UPLOADS': '/review-investor-documents/',
  'ENT_REGISTRATION_II':'/ent-registration-step2/',
  'EDIT_COMPANY':'/edit-company/',
  'FUND_RAISING': '/raise-fund/',
  'REVIEW_FUND_RAISING': '/review-fund-raising/',
  'REWORK_FUND_RAISING': '/rework-fund-raising/',
  'UPLOAD_DOCUMENTS':'/upload-documents',
  'INVESTOR_QUESTIONNAIRE_I':'/begin-investor-questionnaire/',
  'INVESTOR_LANDING': '/investor-landing-page',
  'INVESTOR_DASHBOARD':'/investor-dashboard',
  'VERIFY_TOKEN':'/verify-token/:token',
  'COMPANY':'/company/',
  'COMPANY_V1':'/company_v1/',
  'PREVIEW_COMPANY': '/preview-company/',
  'UTIL':'/util',
  'LEGAL_DOCUMENTS':'/agreement/',
  'MY_PROFILE':'/account',
  'FRAME_TEST':'/frame-test/', //HORRIBLE NAME! not for testing at all, docusign flow depends on this
  'BANK_PAYMENT':'/bank-payment/',
  'REVIEW_INVESTOR_ANSWERS':'/review-investor-answers/',
  'NEW_SIGN_IN':'/new-sign-in',
  'NEW_ABOUT':'/new-about',
  'USER_HISTORY':'/history/',
  'NEW_FOUNDER_REGISTER': '/new-founder-register',
  'NEW_INVESTOR_GENERAL_INFO': '/new-investor-general-info/',    //done
  'ACTIVATE_PROFILE': '/profile-activate/:token', //activate page
  'NEW_REG_FOUNDER': '/new-reg-founder/', //step2 founder
  'NEW_ACCREDITED_INVESTOR_QUESTIONS1':'/new-accredited-investor-questions1/',
  'NEW_FOUNDER_REGISTER_STEP_ONE':'/founder-step-one',
  'REVIEW_INVITED_USER':'/review-invited-user/',
  'PRIVACY_POLICY':'/privacy-policy/',
  'INVITE':'/invite/',
  'FAQ':'/faq/',
  'TERMS_OF_SERVICE':'/terms-of-service/',
  'TRADEMARK_POLICY':'/trademark-policy/',
  'TEAMS':'/teams/',
  'CONTACT_US':'/contact-us/',
  'FOUROFOUR':'/error',
  'PRESS_COVERAGE':'/press-coverage/',
  'ACCOUNT_SUSPENDED':'/suspended',
  'STOCKS':'/stocks',
  'DIRECT_ACCESS':'/direct-access',
  'INVESTOR_QUESTIONNAIRE':'/investor-questionnaire/',
  'RISK_FACTOR_ACK':'/risk-factor-ack/',
  'BEGIN_INVESTMENT':'/begin-investment',
  'IRA':'/ira/',
  'FRAME_TEST_IRA':'/frame-test-irs/', //HORRIBLE NAME! not for testing at all, docusign flow depends on this
  'INVESTMENT_COMPLETE' : '/investment-done/',
  'MAINTENANCE':'/maintenance',
  'ADVISORS': '/advisors',
  'FIRMS': '/firms',
  'FIRM_SETTINGS': function(firmSlug) {
    return '/firms/' + firmSlug;
  },
  'FIRM_BROKERS': function(firmSlug) {
    return '/firms/' + firmSlug + '/brokers';
  },
  'OUR_INVESTMENTS': '/our-investments',
  'PDF_VIEWER': function(fileId) {
    return '/pdf-viewer' + '/' + fileId;
  },
  'UI_CATALOG': function(item) {
    var extension = item ? '/' + item : '';
    return '/ui-catalog' + extension;
  },
  'INVESTMENT_OFFERINGS': '/invest/offerings',
  'FIRM_OFFERINGS_DASHBOARD': function(firmSlug, offeringId) {
    var path = offeringId ? '/' + offeringId : '';
    return '/firms/' + firmSlug + '/offerings' + path;
  },
  'INVESTMENT_APPLICATION': function(seriesId) {
    seriesId = seriesId || ':seriesId';

    return '/invest/series/' + seriesId + '/apply';
  },
  'FUNDRAISE': function(companySlug, seriesId) {
    companySlug = companySlug || ':companySlug';
    var base = '/company/' + companySlug;
    var queryString = seriesId? ('?series=' + seriesId): '';
    return base + queryString;
  },
  url: function(path) {
    return path;
  }
});

'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', [
  'ngRoute',
  'ngCookies',
  'ngAnimate',
  'ngRaven',
  'ngSanitize',
  'ui.bootstrap',
  'angularFileUpload',
  'truncate',
  'myApp.api',
  'myApp.url',
  'myApp.msg',
  'myApp.invest_msg',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers',
  'myApp.authentication',
  'myExceptions',
  'config',
  'angulartics',
  'angulartics.google.analytics',
  'LocalStorageModule',
  'ngTouch'
]).
config(["$routeProvider", "$httpProvider", "$locationProvider", "URLS", "ENV", "localStorageServiceProvider", function($routeProvider, $httpProvider, $locationProvider, URLS, ENV, localStorageServiceProvider) {
  var access = {
    admin: 1,
    entrepreneur: 2,
    investor: 4,
    anon: 8,
    public: 15,
    company_edit: 3,
    investor_edit: 5,
    logged: 7
  };
  localStorageServiceProvider.setPrefix('citizenvc'); // Namespace any localStorage additions by the app
  $locationProvider.html5Mode(true); // Non-hashtagged URLs
  // Access Level : Anonymous
  $routeProvider.when(URLS.SIGN_IN, {templateUrl: 'views/login.html', controller: 'UtilCtrl', access: access.anon});
  $routeProvider.when(URLS.VERIFY_TOKEN, {templateUrl: 'views/user-activate.html', controller: 'UtilCtrl', access: access.anon});

  // Access Level : Admin
  $routeProvider.when(URLS.ADMIN_DASHBOARD, {templateUrl: 'views/admin-dashboard.html', controller: 'AdminCtrl', access: access.admin});
  $routeProvider.when(URLS.ADMIN_DASHBOARD+'/'+':id', {templateUrl: 'views/admin-dashboard.html', controller: 'AdminCtrl', access: access.admin});
  $routeProvider.when(URLS.REVIEW_COMPANY_INFO+':id', {templateUrl: 'views/founder-step-one.html', controller: 'AdminCtrl', access: access.admin});
  $routeProvider.when(URLS.REVIEW_COMPANY_INFO_II+':id', {templateUrl: 'views/company-details-form.html', controller: 'RegisterCompanyCtrl', access: access.admin});
  $routeProvider.when(URLS.REVIEW_FUND_RAISING+':id', {templateUrl: 'views/fund-raising.html', controller: 'AdminCtrl', access: access.admin});

  $routeProvider.when(URLS.USER_HISTORY+':id', {templateUrl: 'views/user-history.html', controller: 'AdminCtrl', access: access.admin});
  $routeProvider.when(URLS.REVIEW_INVESTOR_ANSWERS+':type/:id', {templateUrl: 'views/new-investor-general-info.html', controller: 'RegisterInvestorCtrl', access: access.admin});
  $routeProvider.when(URLS.REVIEW_ACCREDITED_QUESTIONS+':type/:id', {templateUrl: 'views/new-accredited-investor-questions1.html', controller: 'RegisterInvestorCtrl', access:access.admin});
  $routeProvider.when(URLS.REVIEW_INVITED_USER+':id', {templateUrl: 'views/founder-step-one-two.html', controller: 'AdminCtrl', access: access.admin});

  // Access Level- Public
  $routeProvider.when(URLS.HOME, {templateUrl: 'views/new-home.html', controller: 'UtilCtrl', access: access.public});
  $routeProvider.when(URLS.ABOUT, {templateUrl: 'views/aboutus.html', controller: 'UtilCtrl', access: access.public});
  $routeProvider.when(URLS.ADVISORS, {templateUrl: 'components/advisors/advisors.html', controller: 'AdvisorsController', access: access.public});
  $routeProvider.when(URLS.FRAME_TEST+':id', {templateUrl:'views/frame-test.html', controller:'CompanyCtrl', access: access.public}); //HORRIBLE NAME! not for testing at all, docusign flow depends on this
  $routeProvider.when(URLS.FRAME_TEST_IRA+':id', {templateUrl:'views/ira-frame.html', controller:'CompanyCtrl', access: access.public}); //HORRIBLE NAME! not for testing at all, docusign flow depends on this
  $routeProvider.when(URLS.SUCCESSFUL_SUBMISSION, {templateUrl: 'views/new-application-submitted.html', controller: 'RegisterCompanyCtrl', access: access.public});
  $routeProvider.when(URLS.FOUROFOUR, {templateUrl: 'components/404/404.html', controller: 'NotFoundController', access: access.public});
  $routeProvider.when(URLS.PRESS_COVERAGE, {templateUrl: 'views/press-coverage.html', controller: 'UtilCtrl', access: access.public});
  $routeProvider.when(URLS.STOCKS, {templateUrl: 'views/stocks.html', controller: 'UtilCtrl', access: access.public});
  $routeProvider.when(URLS.DIRECT_ACCESS, {templateUrl: 'views/select-access.html', controller: 'UtilCtrl', access: access.public});
  $routeProvider.when(URLS.INVESTMENT_COMPLETE+':id', {templateUrl: 'views/invest-complete.html', controller: 'CompanyCtrl', access:access.public});
  $routeProvider.when(URLS.PRIVACY_POLICY, {templateUrl: 'views/privacy-policy.html', controller: 'UtilCtrl', access:access.public});
  $routeProvider.when(URLS.INVITE, {templateUrl: 'views/invite.html', controller: 'UtilCtrl', access:access.public});
  $routeProvider.when(URLS.FAQ, {templateUrl: 'views/faq.html', controller: 'UtilCtrl', access:access.public});
  $routeProvider.when(URLS.TERMS_OF_SERVICE, {templateUrl: 'views/terms-of-service.html', controller: 'UtilCtrl', access:access.public});
  $routeProvider.when(URLS.TRADEMARK_POLICY, {templateUrl: 'views/trademark-policy.html', controller: 'UtilCtrl', access:access.public});
  $routeProvider.when(URLS.TEAMS, {templateUrl: 'views/teams.html', controller: 'UtilCtrl', access:access.public});
  $routeProvider.when(URLS.CONTACT_US, {templateUrl: 'views/contact-us.html', controller: 'CompanyCtrl', access:access.public});
  $routeProvider.when(URLS.ACCOUNT_SUSPENDED, {templateUrl: 'views/suspended.html', controller: 'UtilCtrl', access:access.public});
  $routeProvider.when(URLS.INVESTOR_QUESTIONNAIRE, {templateUrl: 'views/investor-questionnaire.html', controller: 'CompanyCtrl', access:access.public});
  $routeProvider.when(URLS.MAINTENANCE, {templateUrl: 'views/maintenance.html', controller: 'UtilCtrl', access:access.public});
  $routeProvider.when(URLS.OUR_INVESTMENTS, {templateUrl: 'components/our-investments/our-investments.html', controller: 'OurInvestmentsController', access:access.public});

  // Access Level : Entrepreneur
  $routeProvider.when(URLS.ENT_REGISTRATION_II+':id', {templateUrl: 'views/company-details-form.html', controller: 'RegisterCompanyCtrl', access: access.entrepreneur});
  $routeProvider.when(URLS.FUND_RAISING, {templateUrl: 'views/fund-raising.html', controller: 'CompanyCtrl', access: access.entrepreneur});
  $routeProvider.when(URLS.REWORK_FUND_RAISING+':id', {templateUrl: 'views/fund-raising.html', controller: 'CompanyCtrl', access: access.entrepreneur});

  // Access Level : Entrepreneur + Admin
  $routeProvider.when(URLS.EDIT_COMPANY+':id', {templateUrl: 'views/company-details-form.html', controller: 'RegisterCompanyCtrl', access: access.company_edit});
  $routeProvider.when(URLS.PREVIEW_COMPANY+':id', {templateUrl: 'views/preview.html', controller: 'RegisterCompanyCtrl', access: access.company_edit});

  // Access Level : Investor
  $routeProvider.when(URLS.INVESTOR_LANDING, {templateUrl: 'views/new-application-submitted.html', controller: 'InvestorCtrl', access: access.investor});
  $routeProvider.when(URLS.INVESTOR_DASHBOARD, {templateUrl: 'views/investor-dashboard.html', controller: 'InvestorCtrl', access: access.investor});
  $routeProvider.when(URLS.INVESTOR_QUESTIONNAIRE_I+':id', {templateUrl: 'views/prospective-investor-questionnaire.html', controller: 'RegisterInvestorCtrl', access: access.investor});
  $routeProvider.when(URLS.LEGAL_DOCUMENTS+':id', {templateUrl: 'views/legal-steps.html', controller: 'CompanyCtrl', access: access.investor});
  $routeProvider.when(URLS.BANK_PAYMENT+':id', {templateUrl: 'views/bank_payment.html', controller:'CompanyCtrl', access: access.investor});
  $routeProvider.when(URLS.NEW_INVESTOR_GENERAL_INFO+':type/:id', {templateUrl: 'views/new-investor-general-info.html', controller: 'RegisterInvestorCtrl', access: access.investor});
  $routeProvider.when(URLS.INVESTMENT_OFFERINGS, {templateUrl: 'components/investment-offerings-page/investment-offerings-page.html', controller: 'InvestmentOfferingsPageController', access: access.investor});

  // currently using the below template only
  $routeProvider.when(URLS.NEW_ACCREDITED_INVESTOR_QUESTIONS1+':type/:id', {templateUrl: 'views/new-accredited-investor-questions1.html', controller: 'RegisterInvestorCtrl', access:access.investor});
  $routeProvider.when(URLS.INVESTOR_QUESTIONNAIRE+':id', {templateUrl: 'views/investor-questionnaire.html', controller: 'CompanyCtrl', access:access.investor});
  $routeProvider.when(URLS.RISK_FACTOR_ACK + ':id', {templateUrl: 'views/risk-factor-ack.html', controller: 'CompanyCtrl', access:access.investor});
  $routeProvider.when(URLS.IRA + ':id', {templateUrl: 'views/sign-irs.html', controller: 'CompanyCtrl', access:access.investor});

  // Access Level : Logged In
  $routeProvider.when(URLS.COMPANY_V1+':id', {templateUrl: 'views/company.html', controller: 'CompanyCtrl', access: access.logged});
  $routeProvider.when(URLS.COMPANY_V1+':id/snapshot/:snapshot', {templateUrl: 'views/company.html', controller: 'CompanyCtrl', access: access.logged});
  $routeProvider.when(URLS.COMPANY_V1+':id/:tab', {templateUrl: 'views/company.html', controller: 'CompanyCtrl', access: access.logged});
  $routeProvider.when(URLS.COMPANY+':id', {templateUrl: 'components/company-page/company-page.html', controller: 'CompanyPageController', permission: 'can_view_some_investments'});
  $routeProvider.when(URLS.COMPANY+':id/snapshot/:snapshot', {templateUrl: 'components/company-page/company-page.html', controller: 'CompanyPageController', access: access.logged});
  $routeProvider.when(URLS.COMPANY+':id/:tab', {templateUrl: 'components/company-page/company-page.html', controller: 'CompanyPageController', access: access.logged});
  $routeProvider.when(URLS.MY_PROFILE, {
    templateUrl: 'views/myaccount.html',
    controller: 'RegisterCompanyCtrl',
    permission: 'can_use_site'
  });
  $routeProvider.when(URLS.INVESTOR_REGISTRATION, {templateUrl: 'views/new-investor-registration.html', controller: 'RegisterInvestorCtrl', access: access.logged});
  $routeProvider.when(URLS.ENT_REGISTRATION, {templateUrl: 'views/founder-step-one.html', controller: 'RegisterCompanyCtrl', access: access.logged});
  $routeProvider.when(URLS.PDF_VIEWER(':fileId'), {templateUrl: 'components/pdf-viewer/pdf-viewer-standalone.html', controller: 'PDFViewerStandaloneController', permission: 'can_use_site'});

  // TODO: Access Levels to be set
  $routeProvider.when(URLS.FIRMS, {
    templateUrl: 'components/brokerages-page/brokerages-page.html',
    controller: 'BrokeragesPageController',
    permission: 'can_administrate_brokerages'
  });

  $routeProvider.when(URLS.FIRM_SETTINGS(':firmSlug'), {
    templateUrl: 'components/brokerage-settings-page/brokerage-settings-page.html',
    controller: 'BrokerageSettingsPageController',
    permission: 'can_administrate_brokerages'
  });

  $routeProvider.when(URLS.FIRM_OFFERINGS_DASHBOARD(':firmSlug'), {
    templateUrl: 'components/brokerage-offerings-dashboard-page/brokerage-offerings-dashboard-page.html',
    controller: 'BrokerageOfferingsDashboardPageController',
    permission: 'can_administrate_brokerages'
  });

  $routeProvider.when(URLS.FIRM_OFFERINGS_DASHBOARD(':firmSlug', ':offeringId'), {
    templateUrl: 'components/brokerage-offerings-dashboard-page/brokerage-offerings-dashboard-page.html',
    controller: 'BrokerageOfferingsDashboardPageController',
    permission: 'can_administrate_brokerages'
  });

  $routeProvider.when(URLS.INVESTMENT_APPLICATION(':seriesId'), {
    templateUrl: 'components/investment-application-page/investment-application-page.html',
    controller: 'InvestmentApplicationPageController',
    access: access.public
  });

  $routeProvider.otherwise({templateUrl: 'components/404/404.html', controller: 'NotFoundController', access: access.public});

// Request changes for DRF API calls
  delete $httpProvider.defaults.headers.common['X-Requested-With']; //= 'XMLHttpRequest';
  $httpProvider.defaults.useXDomain = true;
  $httpProvider.defaults.headers.post['Content-Type'] = 'application/json';

  var interceptor = ['$rootScope', '$location', '$q', 'URLS', 'Users', function($rootScope, $location, $q, URLS, Users) {
    function success(response) {
      $('.loader-overlay').fadeOut('fast');
      return response;
    }

    function error(response) {
      $('.loader-overlay').fadeOut('fast');
      if(response.status === 503){
        $location.path(URLS.MAINTENANCE);
      }
      if(response.status === 500) {
        var authedUser = Users.authorized();
        $location.path(authedUser? authedUser.homeUrl() : URLS.HOME);
      }
      return $q.reject(response);
    }

    return {
      response: success,
      responseError: error
    };
  }];

  $httpProvider.interceptors.push(interceptor);
}]);

'use strict';

// Listing all flash messages
angular.module('myApp.msg',[]).constant('MSG',{
	'SUCCESS':'Requested operation was successfull.',
	'FAILURE':'Sorry, something has gone wrong. Please try again shortly.',
	'USER_SUBMISSION':'Your submission has been sent for approval.',
	'ADMIN_APPROVE':'You have approved the request.',
	'ADMIN_REJECT':'You have rejected the request.',
	'FORGOT_PASSWORD':'Request for reset password has been sent.',
	'CHANGE_PASSWORD':'Your password has been changed.',
	'LOGOUT':'Successfully logged out.',
	'DOC_UPLOAD_SUCCESS':'Document successfully uploaded.',
	'SUCCESSFULL_TRANSACTION':'Transaction successfully performed.',
	'PREVIEW_COMPANY': 'Your changes have not yet been approved. This is a preview page.',
	'NO_SUCH_USER': 'No such user exists. Please try again.',
	'VALID_AMOUNT':'Please enter the amount you wish to invest. Please do not include commas.',
	'ALREADY_ACTIVATED': 'Your account has already been registered. Use the "Forgot password?" link below if needed.',
	'ALREADY_MODERATED': 'This request has already been moderated.',
	'USER_EXIST':'A user with this email address already exists.',
	'FUNDING_AMOUNT_OUT_OF_RANGE': function(minValue, maxValue) {
		return 'Investment amount should be between ' + minValue + ' and ' + maxValue;
	},
	'INVITATION_SENT':'Invitation has been sent.',
	'INVALID_FORM':'You have not filled out all required fields. Please check below.',
	'SUCCESSFULL_INVITE':'Invitation sent!',
	'NO_COMPANIES_FOR_INVESTMENT':'There are currently no companies available for investment. Watch this space for updates.',
	'NO_COMPANIES_TO_FOLLOW':'You are not following any companies.',
	'REGISTER_AS_ENTREPRENEUR':'Register your company',
	'REGISTER_AS_INVESTOR':'Register as an investor',
	'REGISTER_AS_ADMIN':'Register as admin',
	'DISBURSE_SUCCESS':'Disbursal successfully done!',
	'INACTIVITY_LOGOUT_MSG':'You have been signed out for your security. Please sign in again.',
	'EMAIL_CHANGE_LOGOUT_MSG':'Please login again',
	'SAVE_COMPANY_FIELDS_REQUIRED_ERROR':'You must fill out the first four fields of this form before you can Save. You will be able to edit them later.',
	'SAVE_COMPANY_ERROR':'Cannot save data',
	'NOTE_SAVE':'Note saved',
	'CANNOT_OPEN_PDF':'Cannot open pdf.'

});

/* jshint ignore:start */

/*
 * jQuery FlexSlider v2.2.2
 * Copyright 2012 WooThemes
 * Contributing Author: Tyler Smith
 */
;
(function ($) {

  //FlexSlider: Object Instance
  $.flexslider = function(el, options) {
    var slider = $(el);

    // making variables public
    slider.vars = $.extend({}, $.flexslider.defaults, options);

    var namespace = slider.vars.namespace,
        msGesture = window.navigator && window.navigator.msPointerEnabled && window.MSGesture,
        touch = (( "ontouchstart" in window ) || msGesture || window.DocumentTouch && document instanceof DocumentTouch) && slider.vars.touch,
        // depricating this idea, as devices are being released with both of these events
        //eventType = (touch) ? "touchend" : "click",
        eventType = "click touchend MSPointerUp keyup",
        watchedEvent = "",
        watchedEventClearTimer,
        vertical = slider.vars.direction === "vertical",
        reverse = slider.vars.reverse,
        carousel = (slider.vars.itemWidth > 0),
        fade = slider.vars.animation === "fade",
        asNav = slider.vars.asNavFor !== "",
        methods = {},
        focused = true;

    // Store a reference to the slider object
    $.data(el, "flexslider", slider);

    // Private slider methods
    methods = {
      init: function() {
        slider.animating = false;
        // Get current slide and make sure it is a number
        slider.currentSlide = parseInt( ( slider.vars.startAt ? slider.vars.startAt : 0), 10 );
        if ( isNaN( slider.currentSlide ) ) slider.currentSlide = 0;
        slider.animatingTo = slider.currentSlide;
        slider.atEnd = (slider.currentSlide === 0 || slider.currentSlide === slider.last);
        slider.containerSelector = slider.vars.selector.substr(0,slider.vars.selector.search(' '));
        slider.slides = $(slider.vars.selector, slider);
        slider.container = $(slider.containerSelector, slider);
        slider.count = slider.slides.length;
        // SYNC:
        slider.syncExists = $(slider.vars.sync).length > 0;
        // SLIDE:
        if (slider.vars.animation === "slide") slider.vars.animation = "swing";
        slider.prop = (vertical) ? "top" : "marginLeft";
        slider.args = {};
        // SLIDESHOW:
        slider.manualPause = false;
        slider.stopped = false;
        //PAUSE WHEN INVISIBLE
        slider.started = false;
        slider.startTimeout = null;
        // TOUCH/USECSS:
        slider.transitions = !slider.vars.video && !fade && slider.vars.useCSS && (function() {
          var obj = document.createElement('div'),
              props = ['perspectiveProperty', 'WebkitPerspective', 'MozPerspective', 'OPerspective', 'msPerspective'];
          for (var i in props) {
            if ( obj.style[ props[i] ] !== undefined ) {
              slider.pfx = props[i].replace('Perspective','').toLowerCase();
              slider.prop = "-" + slider.pfx + "-transform";
              return true;
            }
          }
          return false;
        }());
        slider.ensureAnimationEnd = '';
        // CONTROLSCONTAINER:
        if (slider.vars.controlsContainer !== "") slider.controlsContainer = $(slider.vars.controlsContainer).length > 0 && $(slider.vars.controlsContainer);
        // MANUAL:
        if (slider.vars.manualControls !== "") slider.manualControls = $(slider.vars.manualControls).length > 0 && $(slider.vars.manualControls);

        // RANDOMIZE:
        if (slider.vars.randomize) {
          slider.slides.sort(function() { return (Math.round(Math.random())-0.5); });
          slider.container.empty().append(slider.slides);
        }

        slider.doMath();

        // INIT
        slider.setup("init");

        // CONTROLNAV:
        if (slider.vars.controlNav) methods.controlNav.setup();

        // DIRECTIONNAV:
        if (slider.vars.directionNav) methods.directionNav.setup();

        // KEYBOARD:
        if (slider.vars.keyboard && ($(slider.containerSelector).length === 1 || slider.vars.multipleKeyboard)) {
          $(document).bind('keyup', function(event) {
            var keycode = event.keyCode;
            if (!slider.animating && (keycode === 39 || keycode === 37)) {
              var target = (keycode === 39) ? slider.getTarget('next') :
                           (keycode === 37) ? slider.getTarget('prev') : false;
              slider.flexAnimate(target, slider.vars.pauseOnAction);
            }
          });
        }
        // MOUSEWHEEL:
        if (slider.vars.mousewheel) {
          slider.bind('mousewheel', function(event, delta, deltaX, deltaY) {
            event.preventDefault();
            var target = (delta < 0) ? slider.getTarget('next') : slider.getTarget('prev');
            slider.flexAnimate(target, slider.vars.pauseOnAction);
          });
        }

        // PAUSEPLAY
        if (slider.vars.pausePlay) methods.pausePlay.setup();

        //PAUSE WHEN INVISIBLE
        if (slider.vars.slideshow && slider.vars.pauseInvisible) methods.pauseInvisible.init();

        // SLIDSESHOW
        if (slider.vars.slideshow) {
          if (slider.vars.pauseOnHover) {
            slider.hover(function() {
              if (!slider.manualPlay && !slider.manualPause) slider.pause();
            }, function() {
              if (!slider.manualPause && !slider.manualPlay && !slider.stopped) slider.play();
            });
          }
          // initialize animation
          //If we're visible, or we don't use PageVisibility API
          if(!slider.vars.pauseInvisible || !methods.pauseInvisible.isHidden()) {
            (slider.vars.initDelay > 0) ? slider.startTimeout = setTimeout(slider.play, slider.vars.initDelay) : slider.play();
          }
        }

        // ASNAV:
        if (asNav) methods.asNav.setup();

        // TOUCH
        if (touch && slider.vars.touch) methods.touch();

        // FADE&&SMOOTHHEIGHT || SLIDE:
        if (!fade || (fade && slider.vars.smoothHeight)) $(window).bind("resize orientationchange focus", methods.resize);

        slider.find("img").attr("draggable", "false");

        // API: start() Callback
        setTimeout(function(){
          slider.vars.start(slider);
        }, 200);
      },
      asNav: {
        setup: function() {
          slider.asNav = true;
          slider.animatingTo = Math.floor(slider.currentSlide/slider.move);
          slider.currentItem = slider.currentSlide;
          slider.slides.removeClass(namespace + "active-slide").eq(slider.currentItem).addClass(namespace + "active-slide");
          if(!msGesture){
              slider.slides.on(eventType, function(e){
                e.preventDefault();
                var $slide = $(this),
                    target = $slide.index();
                var posFromLeft = $slide.offset().left - $(slider).scrollLeft(); // Find position of slide relative to left of slider container
                if( posFromLeft <= 0 && $slide.hasClass( namespace + 'active-slide' ) ) {
                  slider.flexAnimate(slider.getTarget("prev"), true);
                } else if (!$(slider.vars.asNavFor).data('flexslider').animating && !$slide.hasClass(namespace + "active-slide")) {
                  slider.direction = (slider.currentItem < target) ? "next" : "prev";
                  slider.flexAnimate(target, slider.vars.pauseOnAction, false, true, true);
                }
              });
          }else{
              el._slider = slider;
              slider.slides.each(function (){
                  var that = this;
                  that._gesture = new MSGesture();
                  that._gesture.target = that;
                  that.addEventListener("MSPointerDown", function (e){
                      e.preventDefault();
                      if(e.currentTarget._gesture)
                          e.currentTarget._gesture.addPointer(e.pointerId);
                  }, false);
                  that.addEventListener("MSGestureTap", function (e){
                      e.preventDefault();
                      var $slide = $(this),
                          target = $slide.index();
                      if (!$(slider.vars.asNavFor).data('flexslider').animating && !$slide.hasClass('active')) {
                          slider.direction = (slider.currentItem < target) ? "next" : "prev";
                          slider.flexAnimate(target, slider.vars.pauseOnAction, false, true, true);
                      }
                  });
              });
          }
        }
      },
      controlNav: {
        setup: function() {
          if (!slider.manualControls) {
            methods.controlNav.setupPaging();
          } else { // MANUALCONTROLS:
            methods.controlNav.setupManual();
          }
        },
        setupPaging: function() {
          var type = (slider.vars.controlNav === "thumbnails") ? 'control-thumbs' : 'control-paging',
              j = 1,
              item,
              slide;

          slider.controlNavScaffold = $('<ol class="'+ namespace + 'control-nav ' + namespace + type + '"></ol>');

          if (slider.pagingCount > 1) {
            for (var i = 0; i < slider.pagingCount; i++) {
              slide = slider.slides.eq(i);
              item = (slider.vars.controlNav === "thumbnails") ? '<img src="' + slide.attr( 'data-thumb' ) + '"/>' : '<a>' + j + '</a>';
              if ( 'thumbnails' === slider.vars.controlNav && true === slider.vars.thumbCaptions ) {
                var captn = slide.attr( 'data-thumbcaption' );
                if ( '' != captn && undefined != captn ) item += '<span class="' + namespace + 'caption">' + captn + '</span>';
              }
              slider.controlNavScaffold.append('<li>' + item + '</li>');
              j++;
            }
          }

          // CONTROLSCONTAINER:
          (slider.controlsContainer) ? $(slider.controlsContainer).append(slider.controlNavScaffold) : slider.append(slider.controlNavScaffold);
          methods.controlNav.set();

          methods.controlNav.active();

          slider.controlNavScaffold.delegate('a, img', eventType, function(event) {
            event.preventDefault();

            if (watchedEvent === "" || watchedEvent === event.type) {
              var $this = $(this),
                  target = slider.controlNav.index($this);

              if (!$this.hasClass(namespace + 'active')) {
                slider.direction = (target > slider.currentSlide) ? "next" : "prev";
                slider.flexAnimate(target, slider.vars.pauseOnAction);
              }
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();

          });
        },
        setupManual: function() {
          slider.controlNav = slider.manualControls;
          methods.controlNav.active();

          slider.controlNav.bind(eventType, function(event) {
            event.preventDefault();

            if (watchedEvent === "" || watchedEvent === event.type) {
              var $this = $(this),
                  target = slider.controlNav.index($this);

              if (!$this.hasClass(namespace + 'active')) {
                (target > slider.currentSlide) ? slider.direction = "next" : slider.direction = "prev";
                slider.flexAnimate(target, slider.vars.pauseOnAction);
              }
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        set: function() {
          var selector = (slider.vars.controlNav === "thumbnails") ? 'img' : 'a';
          slider.controlNav = $('.' + namespace + 'control-nav li ' + selector, (slider.controlsContainer) ? slider.controlsContainer : slider);
        },
        active: function() {
          slider.controlNav.removeClass(namespace + "active").eq(slider.animatingTo).addClass(namespace + "active");
        },
        update: function(action, pos) {
          if (slider.pagingCount > 1 && action === "add") {
            slider.controlNavScaffold.append($('<li><a>' + slider.count + '</a></li>'));
          } else if (slider.pagingCount === 1) {
            slider.controlNavScaffold.find('li').remove();
          } else {
            slider.controlNav.eq(pos).closest('li').remove();
          }
          methods.controlNav.set();
          (slider.pagingCount > 1 && slider.pagingCount !== slider.controlNav.length) ? slider.update(pos, action) : methods.controlNav.active();
        }
      },
      directionNav: {
        setup: function() {
          var directionNavScaffold = $('<ul class="' + namespace + 'direction-nav"><li><a class="' + namespace + 'prev" href="#"></a></li><li><a class="' + namespace + 'next" href="#"></a></li></ul>');

          // CONTROLSCONTAINER:
          if (slider.controlsContainer) {
            $(slider.controlsContainer).append(directionNavScaffold);
            slider.directionNav = $('.' + namespace + 'direction-nav li a', slider.controlsContainer);
          } else {
            slider.append(directionNavScaffold);
            slider.directionNav = $('.' + namespace + 'direction-nav li a', slider);
          }

          methods.directionNav.update();

          slider.directionNav.bind(eventType, function(event) {
            event.preventDefault();
            var target;

            if (watchedEvent === "" || watchedEvent === event.type) {
              target = ($(this).hasClass(namespace + 'next')) ? slider.getTarget('next') : slider.getTarget('prev');
              slider.flexAnimate(target, slider.vars.pauseOnAction);
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        update: function() {
          var disabledClass = namespace + 'disabled';
          if (slider.pagingCount === 1) {
            slider.directionNav.addClass(disabledClass).attr('tabindex', '-1');
          } else if (!slider.vars.animationLoop) {
            if (slider.animatingTo === 0) {
              slider.directionNav.removeClass(disabledClass).filter('.' + namespace + "prev").addClass(disabledClass).attr('tabindex', '-1');
            } else if (slider.animatingTo === slider.last) {
              slider.directionNav.removeClass(disabledClass).filter('.' + namespace + "next").addClass(disabledClass).attr('tabindex', '-1');
            } else {
              slider.directionNav.removeClass(disabledClass).removeAttr('tabindex');
            }
          } else {
            slider.directionNav.removeClass(disabledClass).removeAttr('tabindex');
          }
        }
      },
      pausePlay: {
        setup: function() {
          var pausePlayScaffold = $('<div class="' + namespace + 'pauseplay"><a></a></div>');

          // CONTROLSCONTAINER:
          if (slider.controlsContainer) {
            slider.controlsContainer.append(pausePlayScaffold);
            slider.pausePlay = $('.' + namespace + 'pauseplay a', slider.controlsContainer);
          } else {
            slider.append(pausePlayScaffold);
            slider.pausePlay = $('.' + namespace + 'pauseplay a', slider);
          }

          methods.pausePlay.update((slider.vars.slideshow) ? namespace + 'pause' : namespace + 'play');

          slider.pausePlay.bind(eventType, function(event) {
            event.preventDefault();

            if (watchedEvent === "" || watchedEvent === event.type) {
              if ($(this).hasClass(namespace + 'pause')) {
                slider.manualPause = true;
                slider.manualPlay = false;
                slider.pause();
              } else {
                slider.manualPause = false;
                slider.manualPlay = true;
                slider.play();
              }
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        update: function(state) {
          (state === "play") ? slider.pausePlay.removeClass(namespace + 'pause').addClass(namespace + 'play').html(slider.vars.playText) : slider.pausePlay.removeClass(namespace + 'play').addClass(namespace + 'pause').html(slider.vars.pauseText);
        }
      },
      touch: function() {
        var startX,
          startY,
          offset,
          cwidth,
          dx,
          startT,
          scrolling = false,
          localX = 0,
          localY = 0,
          accDx = 0;

        if(!msGesture){
            el.addEventListener('touchstart', onTouchStart, false);

            function onTouchStart(e) {
              if (slider.animating) {
                e.preventDefault();
              } else if ( ( window.navigator.msPointerEnabled ) || e.touches.length === 1 ) {
                slider.pause();
                // CAROUSEL:
                cwidth = (vertical) ? slider.h : slider. w;
                startT = Number(new Date());
                // CAROUSEL:

                // Local vars for X and Y points.
                localX = e.touches[0].pageX;
                localY = e.touches[0].pageY;

                offset = (carousel && reverse && slider.animatingTo === slider.last) ? 0 :
                         (carousel && reverse) ? slider.limit - (((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo) :
                         (carousel && slider.currentSlide === slider.last) ? slider.limit :
                         (carousel) ? ((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.currentSlide :
                         (reverse) ? (slider.last - slider.currentSlide + slider.cloneOffset) * cwidth : (slider.currentSlide + slider.cloneOffset) * cwidth;
                startX = (vertical) ? localY : localX;
                startY = (vertical) ? localX : localY;

                el.addEventListener('touchmove', onTouchMove, false);
                el.addEventListener('touchend', onTouchEnd, false);
              }
            }

            function onTouchMove(e) {
              // Local vars for X and Y points.

              localX = e.touches[0].pageX;
              localY = e.touches[0].pageY;

              dx = (vertical) ? startX - localY : startX - localX;
              scrolling = (vertical) ? (Math.abs(dx) < Math.abs(localX - startY)) : (Math.abs(dx) < Math.abs(localY - startY));

              var fxms = 500;

              if ( ! scrolling || Number( new Date() ) - startT > fxms ) {
                e.preventDefault();
                if (!fade && slider.transitions) {
                  if (!slider.vars.animationLoop) {
                    dx = dx/((slider.currentSlide === 0 && dx < 0 || slider.currentSlide === slider.last && dx > 0) ? (Math.abs(dx)/cwidth+2) : 1);
                  }
                  slider.setProps(offset + dx, "setTouch");
                }
              }
            }

            function onTouchEnd(e) {
              // finish the touch by undoing the touch session
              el.removeEventListener('touchmove', onTouchMove, false);

              if (slider.animatingTo === slider.currentSlide && !scrolling && !(dx === null)) {
                var updateDx = (reverse) ? -dx : dx,
                    target = (updateDx > 0) ? slider.getTarget('next') : slider.getTarget('prev');

                if (slider.canAdvance(target) && (Number(new Date()) - startT < 550 && Math.abs(updateDx) > 50 || Math.abs(updateDx) > cwidth/2)) {
                  slider.flexAnimate(target, slider.vars.pauseOnAction);
                } else {
                  if (!fade) slider.flexAnimate(slider.currentSlide, slider.vars.pauseOnAction, true);
                }
              }
              el.removeEventListener('touchend', onTouchEnd, false);

              startX = null;
              startY = null;
              dx = null;
              offset = null;
            }
        }else{
            el.style.msTouchAction = "none";
            el._gesture = new MSGesture();
            el._gesture.target = el;
            el.addEventListener("MSPointerDown", onMSPointerDown, false);
            el._slider = slider;
            el.addEventListener("MSGestureChange", onMSGestureChange, false);
            el.addEventListener("MSGestureEnd", onMSGestureEnd, false);

            function onMSPointerDown(e){
                e.stopPropagation();
                if (slider.animating) {
                    e.preventDefault();
                }else{
                    slider.pause();
                    el._gesture.addPointer(e.pointerId);
                    accDx = 0;
                    cwidth = (vertical) ? slider.h : slider. w;
                    startT = Number(new Date());
                    // CAROUSEL:

                    offset = (carousel && reverse && slider.animatingTo === slider.last) ? 0 :
                        (carousel && reverse) ? slider.limit - (((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo) :
                            (carousel && slider.currentSlide === slider.last) ? slider.limit :
                                (carousel) ? ((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.currentSlide :
                                    (reverse) ? (slider.last - slider.currentSlide + slider.cloneOffset) * cwidth : (slider.currentSlide + slider.cloneOffset) * cwidth;
                }
            }

            function onMSGestureChange(e) {
                e.stopPropagation();
                var slider = e.target._slider;
                if(!slider){
                    return;
                }
                var transX = -e.translationX,
                    transY = -e.translationY;

                //Accumulate translations.
                accDx = accDx + ((vertical) ? transY : transX);
                dx = accDx;
                scrolling = (vertical) ? (Math.abs(accDx) < Math.abs(-transX)) : (Math.abs(accDx) < Math.abs(-transY));

                if(e.detail === e.MSGESTURE_FLAG_INERTIA){
                    setImmediate(function (){
                        el._gesture.stop();
                    });

                    return;
                }

                if (!scrolling || Number(new Date()) - startT > 500) {
                    e.preventDefault();
                    if (!fade && slider.transitions) {
                        if (!slider.vars.animationLoop) {
                            dx = accDx / ((slider.currentSlide === 0 && accDx < 0 || slider.currentSlide === slider.last && accDx > 0) ? (Math.abs(accDx) / cwidth + 2) : 1);
                        }
                        slider.setProps(offset + dx, "setTouch");
                    }
                }
            }

            function onMSGestureEnd(e) {
                e.stopPropagation();
                var slider = e.target._slider;
                if(!slider){
                    return;
                }
                if (slider.animatingTo === slider.currentSlide && !scrolling && !(dx === null)) {
                    var updateDx = (reverse) ? -dx : dx,
                        target = (updateDx > 0) ? slider.getTarget('next') : slider.getTarget('prev');

                    if (slider.canAdvance(target) && (Number(new Date()) - startT < 550 && Math.abs(updateDx) > 50 || Math.abs(updateDx) > cwidth/2)) {
                        slider.flexAnimate(target, slider.vars.pauseOnAction);
                    } else {
                        if (!fade) slider.flexAnimate(slider.currentSlide, slider.vars.pauseOnAction, true);
                    }
                }

                startX = null;
                startY = null;
                dx = null;
                offset = null;
                accDx = 0;
            }
        }
      },
      resize: function() {
        if (!slider.animating && slider.is(':visible')) {
          if (!carousel) slider.doMath();

          if (fade) {
            // SMOOTH HEIGHT:
            methods.smoothHeight();
          } else if (carousel) { //CAROUSEL:
            slider.slides.width(slider.computedW);
            slider.update(slider.pagingCount);
            slider.setProps();
          }
          else if (vertical) { //VERTICAL:
            slider.viewport.height(slider.h);
            slider.setProps(slider.h, "setTotal");
          } else {
            // SMOOTH HEIGHT:
            if (slider.vars.smoothHeight) methods.smoothHeight();
            slider.newSlides.width(slider.computedW);
            slider.setProps(slider.computedW, "setTotal");
          }
        }
      },
      smoothHeight: function(dur) {
        if (!vertical || fade) {
          var $obj = (fade) ? slider : slider.viewport;
          (dur) ? $obj.animate({"height": slider.slides.eq(slider.animatingTo).height()}, dur) : $obj.height(slider.slides.eq(slider.animatingTo).height());
        }
      },
      sync: function(action) {
        var $obj = $(slider.vars.sync).data("flexslider"),
            target = slider.animatingTo;

        switch (action) {
          case "animate": $obj.flexAnimate(target, slider.vars.pauseOnAction, false, true); break;
          case "play": if (!$obj.playing && !$obj.asNav) { $obj.play(); } break;
          case "pause": $obj.pause(); break;
        }
      },
      uniqueID: function($clone) {
        $clone.find( '[id]' ).each(function() {
          var $this = $(this);
          $this.attr( 'id', $this.attr( 'id' ) + '_clone' );
        });
        return $clone;
      },
      pauseInvisible: {
        visProp: null,
        init: function() {
          var prefixes = ['webkit','moz','ms','o'];

          if ('hidden' in document) return 'hidden';
          for (var i = 0; i < prefixes.length; i++) {
            if ((prefixes[i] + 'Hidden') in document)
            methods.pauseInvisible.visProp = prefixes[i] + 'Hidden';
          }
          if (methods.pauseInvisible.visProp) {
            var evtname = methods.pauseInvisible.visProp.replace(/[H|h]idden/,'') + 'visibilitychange';
            document.addEventListener(evtname, function() {
              if (methods.pauseInvisible.isHidden()) {
                if(slider.startTimeout) clearTimeout(slider.startTimeout); //If clock is ticking, stop timer and prevent from starting while invisible
                else slider.pause(); //Or just pause
              }
              else {
                if(slider.started) slider.play(); //Initiated before, just play
                else (slider.vars.initDelay > 0) ? setTimeout(slider.play, slider.vars.initDelay) : slider.play(); //Didn't init before: simply init or wait for it
              }
            });
          }
        },
        isHidden: function() {
          return document[methods.pauseInvisible.visProp] || false;
        }
      },
      setToClearWatchedEvent: function() {
        clearTimeout(watchedEventClearTimer);
        watchedEventClearTimer = setTimeout(function() {
          watchedEvent = "";
        }, 3000);
      }
    };

    // public methods
    slider.flexAnimate = function(target, pause, override, withSync, fromNav) {
      if (!slider.vars.animationLoop && target !== slider.currentSlide) {
        slider.direction = (target > slider.currentSlide) ? "next" : "prev";
      }

      if (asNav && slider.pagingCount === 1) slider.direction = (slider.currentItem < target) ? "next" : "prev";

      if (!slider.animating && (slider.canAdvance(target, fromNav) || override) && slider.is(":visible")) {
        if (asNav && withSync) {
          var master = $(slider.vars.asNavFor).data('flexslider');
          slider.atEnd = target === 0 || target === slider.count - 1;
          master.flexAnimate(target, true, false, true, fromNav);
          slider.direction = (slider.currentItem < target) ? "next" : "prev";
          master.direction = slider.direction;

          if (Math.ceil((target + 1)/slider.visible) - 1 !== slider.currentSlide && target !== 0) {
            slider.currentItem = target;
            slider.slides.removeClass(namespace + "active-slide").eq(target).addClass(namespace + "active-slide");
            target = Math.floor(target/slider.visible);
          } else {
            slider.currentItem = target;
            slider.slides.removeClass(namespace + "active-slide").eq(target).addClass(namespace + "active-slide");
            return false;
          }
        }

        slider.animating = true;
        slider.animatingTo = target;

        // SLIDESHOW:
        if (pause) slider.pause();

        // API: before() animation Callback
        slider.vars.before(slider);

        // SYNC:
        if (slider.syncExists && !fromNav) methods.sync("animate");

        // CONTROLNAV
        if (slider.vars.controlNav) methods.controlNav.active();

        // !CAROUSEL:
        // CANDIDATE: slide active class (for add/remove slide)
        if (!carousel) slider.slides.removeClass(namespace + 'active-slide').eq(target).addClass(namespace + 'active-slide');

        // INFINITE LOOP:
        // CANDIDATE: atEnd
        slider.atEnd = target === 0 || target === slider.last;

        // DIRECTIONNAV:
        if (slider.vars.directionNav) methods.directionNav.update();

        if (target === slider.last) {
          // API: end() of cycle Callback
          slider.vars.end(slider);
          // SLIDESHOW && !INFINITE LOOP:
          if (!slider.vars.animationLoop) slider.pause();
        }

        // SLIDE:
        if (!fade) {
          var dimension = (vertical) ? slider.slides.filter(':first').height() : slider.computedW,
              margin, slideString, calcNext;

          // INFINITE LOOP / REVERSE:
          if (carousel) {
            //margin = (slider.vars.itemWidth > slider.w) ? slider.vars.itemMargin * 2 : slider.vars.itemMargin;
            margin = slider.vars.itemMargin;
            calcNext = ((slider.itemW + margin) * slider.move) * slider.animatingTo;
            slideString = (calcNext > slider.limit && slider.visible !== 1) ? slider.limit : calcNext;
          } else if (slider.currentSlide === 0 && target === slider.count - 1 && slider.vars.animationLoop && slider.direction !== "next") {
            slideString = (reverse) ? (slider.count + slider.cloneOffset) * dimension : 0;
          } else if (slider.currentSlide === slider.last && target === 0 && slider.vars.animationLoop && slider.direction !== "prev") {
            slideString = (reverse) ? 0 : (slider.count + 1) * dimension;
          } else {
            slideString = (reverse) ? ((slider.count - 1) - target + slider.cloneOffset) * dimension : (target + slider.cloneOffset) * dimension;
          }
          slider.setProps(slideString, "", slider.vars.animationSpeed);
          if (slider.transitions) {
            if (!slider.vars.animationLoop || !slider.atEnd) {
              slider.animating = false;
              slider.currentSlide = slider.animatingTo;
            }
            
            // Unbind previous transitionEnd events and re-bind new transitionEnd event
            slider.container.unbind("webkitTransitionEnd transitionend");
            slider.container.bind("webkitTransitionEnd transitionend", function() {
              clearTimeout(slider.ensureAnimationEnd);
              slider.wrapup(dimension);
            });

            // Insurance for the ever-so-fickle transitionEnd event
            clearTimeout(slider.ensureAnimationEnd);
            slider.ensureAnimationEnd = setTimeout(function() {
              slider.wrapup(dimension);
            }, slider.vars.animationSpeed + 100);

          } else {
            slider.container.animate(slider.args, slider.vars.animationSpeed, slider.vars.easing, function(){
              slider.wrapup(dimension);
            });
          }
        } else { // FADE:
          if (!touch) {
            //slider.slides.eq(slider.currentSlide).fadeOut(slider.vars.animationSpeed, slider.vars.easing);
            //slider.slides.eq(target).fadeIn(slider.vars.animationSpeed, slider.vars.easing, slider.wrapup);

            slider.slides.eq(slider.currentSlide).css({"zIndex": 1}).animate({"opacity": 0}, slider.vars.animationSpeed, slider.vars.easing);
            slider.slides.eq(target).css({"zIndex": 2}).animate({"opacity": 1}, slider.vars.animationSpeed, slider.vars.easing, slider.wrapup);

          } else {
            slider.slides.eq(slider.currentSlide).css({ "opacity": 0, "zIndex": 1 });
            slider.slides.eq(target).css({ "opacity": 1, "zIndex": 2 });
            slider.wrapup(dimension);
          }
        }
        // SMOOTH HEIGHT:
        if (slider.vars.smoothHeight) methods.smoothHeight(slider.vars.animationSpeed);
      }
    };
    slider.wrapup = function(dimension) {
      // SLIDE:
      if (!fade && !carousel) {
        if (slider.currentSlide === 0 && slider.animatingTo === slider.last && slider.vars.animationLoop) {
          slider.setProps(dimension, "jumpEnd");
        } else if (slider.currentSlide === slider.last && slider.animatingTo === 0 && slider.vars.animationLoop) {
          slider.setProps(dimension, "jumpStart");
        }
      }
      slider.animating = false;
      slider.currentSlide = slider.animatingTo;
      // API: after() animation Callback
      slider.vars.after(slider);
    };

    // SLIDESHOW:
    slider.animateSlides = function() {
      if (!slider.animating && focused ) slider.flexAnimate(slider.getTarget("next"));
    };
    // SLIDESHOW:
    slider.pause = function() {
      clearInterval(slider.animatedSlides);
      slider.animatedSlides = null;
      slider.playing = false;
      // PAUSEPLAY:
      if (slider.vars.pausePlay) methods.pausePlay.update("play");
      // SYNC:
      if (slider.syncExists) methods.sync("pause");
    };
    // SLIDESHOW:
    slider.play = function() {
      if (slider.playing) clearInterval(slider.animatedSlides);
      slider.animatedSlides = slider.animatedSlides || setInterval(slider.animateSlides, slider.vars.slideshowSpeed);
      slider.started = slider.playing = true;
      // PAUSEPLAY:
      if (slider.vars.pausePlay) methods.pausePlay.update("pause");
      // SYNC:
      if (slider.syncExists) methods.sync("play");
    };
    // STOP:
    slider.stop = function () {
      slider.pause();
      slider.stopped = true;
    };
    slider.canAdvance = function(target, fromNav) {
      // ASNAV:
      var last = (asNav) ? slider.pagingCount - 1 : slider.last;
      return (fromNav) ? true :
             (asNav && slider.currentItem === slider.count - 1 && target === 0 && slider.direction === "prev") ? true :
             (asNav && slider.currentItem === 0 && target === slider.pagingCount - 1 && slider.direction !== "next") ? false :
             (target === slider.currentSlide && !asNav) ? false :
             (slider.vars.animationLoop) ? true :
             (slider.atEnd && slider.currentSlide === 0 && target === last && slider.direction !== "next") ? false :
             (slider.atEnd && slider.currentSlide === last && target === 0 && slider.direction === "next") ? false :
             true;
    };
    slider.getTarget = function(dir) {
      slider.direction = dir;
      if (dir === "next") {
        return (slider.currentSlide === slider.last) ? 0 : slider.currentSlide + 1;
      } else {
        return (slider.currentSlide === 0) ? slider.last : slider.currentSlide - 1;
      }
    };

    // SLIDE:
    slider.setProps = function(pos, special, dur) {
      var target = (function() {
        var posCheck = (pos) ? pos : ((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo,
            posCalc = (function() {
              if (carousel) {
                return (special === "setTouch") ? pos :
                       (reverse && slider.animatingTo === slider.last) ? 0 :
                       (reverse) ? slider.limit - (((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo) :
                       (slider.animatingTo === slider.last) ? slider.limit : posCheck;
              } else {
                switch (special) {
                  case "setTotal": return (reverse) ? ((slider.count - 1) - slider.currentSlide + slider.cloneOffset) * pos : (slider.currentSlide + slider.cloneOffset) * pos;
                  case "setTouch": return (reverse) ? pos : pos;
                  case "jumpEnd": return (reverse) ? pos : slider.count * pos;
                  case "jumpStart": return (reverse) ? slider.count * pos : pos;
                  default: return pos;
                }
              }
            }());

            return (posCalc * -1) + "px";
          }());

      if (slider.transitions) {
        target = (vertical) ? "translate3d(0," + target + ",0)" : "translate3d(" + target + ",0,0)";
        dur = (dur !== undefined) ? (dur/1000) + "s" : "0s";
        slider.container.css("-" + slider.pfx + "-transition-duration", dur);
         slider.container.css("transition-duration", dur);
      }

      slider.args[slider.prop] = target;
      if (slider.transitions || dur === undefined) slider.container.css(slider.args);

      slider.container.css('transform',target);
    };

    slider.setup = function(type) {
      // SLIDE:
      if (!fade) {
        var sliderOffset, arr;

        if (type === "init") {
          slider.viewport = $('<div class="' + namespace + 'viewport"></div>').css({"overflow": "hidden", "position": "relative"}).appendTo(slider).append(slider.container);
          // INFINITE LOOP:
          slider.cloneCount = 0;
          slider.cloneOffset = 0;
          // REVERSE:
          if (reverse) {
            arr = $.makeArray(slider.slides).reverse();
            slider.slides = $(arr);
            slider.container.empty().append(slider.slides);
          }
        }
        // INFINITE LOOP && !CAROUSEL:
        if (slider.vars.animationLoop && !carousel) {
          slider.cloneCount = 2;
          slider.cloneOffset = 1;
          // clear out old clones
          if (type !== "init") slider.container.find('.clone').remove();
          // slider.container.append(slider.slides.first().clone().addClass('clone').attr('aria-hidden', 'true')).prepend(slider.slides.last().clone().addClass('clone').attr('aria-hidden', 'true'));
		      methods.uniqueID( slider.slides.first().clone().addClass('clone').attr('aria-hidden', 'true') ).appendTo( slider.container );
		      methods.uniqueID( slider.slides.last().clone().addClass('clone').attr('aria-hidden', 'true') ).prependTo( slider.container );
        }
        slider.newSlides = $(slider.vars.selector, slider);

        sliderOffset = (reverse) ? slider.count - 1 - slider.currentSlide + slider.cloneOffset : slider.currentSlide + slider.cloneOffset;
        // VERTICAL:
        if (vertical && !carousel) {
          slider.container.height((slider.count + slider.cloneCount) * 200 + "%").css("position", "absolute").width("100%");
          setTimeout(function(){
            slider.newSlides.css({"display": "block"});
            slider.doMath();
            slider.viewport.height(slider.h);
            slider.setProps(sliderOffset * slider.h, "init");
          }, (type === "init") ? 100 : 0);
        } else {
          slider.container.width((slider.count + slider.cloneCount) * 200 + "%");
          slider.setProps(sliderOffset * slider.computedW, "init");
          setTimeout(function(){
            slider.doMath();
            slider.newSlides.css({"width": slider.computedW, "float": "left", "display": "block"});
            // SMOOTH HEIGHT:
            if (slider.vars.smoothHeight) methods.smoothHeight();
          }, (type === "init") ? 100 : 0);
        }
      } else { // FADE:
        slider.slides.css({"width": "100%", "float": "left", "marginRight": "-100%", "position": "relative"});
        if (type === "init") {
          if (!touch) {
            //slider.slides.eq(slider.currentSlide).fadeIn(slider.vars.animationSpeed, slider.vars.easing);
            if (slider.vars.fadeFirstSlide == false) {
              slider.slides.css({ "opacity": 0, "display": "block", "zIndex": 1 }).eq(slider.currentSlide).css({"zIndex": 2}).css({"opacity": 1});
            } else {
              slider.slides.css({ "opacity": 0, "display": "block", "zIndex": 1 }).eq(slider.currentSlide).css({"zIndex": 2}).animate({"opacity": 1},slider.vars.animationSpeed,slider.vars.easing);
            }
          } else {
            slider.slides.css({ "opacity": 0, "display": "block", "webkitTransition": "opacity " + slider.vars.animationSpeed / 1000 + "s ease", "zIndex": 1 }).eq(slider.currentSlide).css({ "opacity": 1, "zIndex": 2});
          }
        }
        // SMOOTH HEIGHT:
        if (slider.vars.smoothHeight) methods.smoothHeight();
      }
      // !CAROUSEL:
      // CANDIDATE: active slide
      if (!carousel) slider.slides.removeClass(namespace + "active-slide").eq(slider.currentSlide).addClass(namespace + "active-slide");

      //FlexSlider: init() Callback
      slider.vars.init(slider);
    };

    slider.doMath = function() {
      var slide = slider.slides.first(),
          slideMargin = slider.vars.itemMargin,
          minItems = slider.vars.minItems,
          maxItems = slider.vars.maxItems;

      slider.w = (slider.viewport===undefined) ? slider.width() : slider.viewport.width();
      slider.h = slide.height();
      slider.boxPadding = slide.outerWidth() - slide.width();

      // CAROUSEL:
      if (carousel) {
        slider.itemT = slider.vars.itemWidth + slideMargin;
        slider.minW = (minItems) ? minItems * slider.itemT : slider.w;
        slider.maxW = (maxItems) ? (maxItems * slider.itemT) - slideMargin : slider.w;
        slider.itemW = (slider.minW > slider.w) ? (slider.w - (slideMargin * (minItems - 1)))/minItems :
                       (slider.maxW < slider.w) ? (slider.w - (slideMargin * (maxItems - 1)))/maxItems :
                       (slider.vars.itemWidth > slider.w) ? slider.w : slider.vars.itemWidth;

        slider.visible = Math.floor(slider.w/(slider.itemW));
        slider.move = (slider.vars.move > 0 && slider.vars.move < slider.visible ) ? slider.vars.move : slider.visible;
        slider.pagingCount = Math.ceil(((slider.count - slider.visible)/slider.move) + 1);
        slider.last =  slider.pagingCount - 1;
        slider.limit = (slider.pagingCount === 1) ? 0 :
                       (slider.vars.itemWidth > slider.w) ? (slider.itemW * (slider.count - 1)) + (slideMargin * (slider.count - 1)) : ((slider.itemW + slideMargin) * slider.count) - slider.w - slideMargin;
      } else {
        slider.itemW = slider.w;
        slider.pagingCount = slider.count;
        slider.last = slider.count - 1;
      }
      slider.computedW = slider.itemW - slider.boxPadding;
    };

    slider.update = function(pos, action) {
      slider.doMath();

      // update currentSlide and slider.animatingTo if necessary
      if (!carousel) {
        if (pos < slider.currentSlide) {
          slider.currentSlide += 1;
        } else if (pos <= slider.currentSlide && pos !== 0) {
          slider.currentSlide -= 1;
        }
        slider.animatingTo = slider.currentSlide;
      }

      // update controlNav
      if (slider.vars.controlNav && !slider.manualControls) {
        if ((action === "add" && !carousel) || slider.pagingCount > slider.controlNav.length) {
          methods.controlNav.update("add");
        } else if ((action === "remove" && !carousel) || slider.pagingCount < slider.controlNav.length) {
          if (carousel && slider.currentSlide > slider.last) {
            slider.currentSlide -= 1;
            slider.animatingTo -= 1;
          }
          methods.controlNav.update("remove", slider.last);
        }
      }
      // update directionNav
      if (slider.vars.directionNav) methods.directionNav.update();

    };

    slider.addSlide = function(obj, pos) {
      var $obj = $(obj);

      slider.count += 1;
      slider.last = slider.count - 1;

      // append new slide
      if (vertical && reverse) {
        (pos !== undefined) ? slider.slides.eq(slider.count - pos).after($obj) : slider.container.prepend($obj);
      } else {
        (pos !== undefined) ? slider.slides.eq(pos).before($obj) : slider.container.append($obj);
      }

      // update currentSlide, animatingTo, controlNav, and directionNav
      slider.update(pos, "add");

      // update slider.slides
      slider.slides = $(slider.vars.selector + ':not(.clone)', slider);
      // re-setup the slider to accomdate new slide
      slider.setup();

      //FlexSlider: added() Callback
      slider.vars.added(slider);
    };
    slider.removeSlide = function(obj) {
      var pos = (isNaN(obj)) ? slider.slides.index($(obj)) : obj;

      // update count
      slider.count -= 1;
      slider.last = slider.count - 1;

      // remove slide
      if (isNaN(obj)) {
        $(obj, slider.slides).remove();
      } else {
        (vertical && reverse) ? slider.slides.eq(slider.last).remove() : slider.slides.eq(obj).remove();
      }

      // update currentSlide, animatingTo, controlNav, and directionNav
      slider.doMath();
      slider.update(pos, "remove");

      // update slider.slides
      slider.slides = $(slider.vars.selector + ':not(.clone)', slider);
      // re-setup the slider to accomdate new slide
      slider.setup();

      // FlexSlider: removed() Callback
      slider.vars.removed(slider);
    };

    //FlexSlider: Initialize
    methods.init();
  };

  // Ensure the slider isn't focussed if the window loses focus.
  $( window ).blur( function ( e ) {
    focused = false;
  }).focus( function ( e ) {
    focused = true;
  });

  //FlexSlider: Default Settings
  $.flexslider.defaults = {
    namespace: "flex-",             //{NEW} String: Prefix string attached to the class of every element generated by the plugin
    selector: ".slides > li",       //{NEW} Selector: Must match a simple pattern. '{container} > {slide}' -- Ignore pattern at your own peril
    animation: "fade",              //String: Select your animation type, "fade" or "slide"
    easing: "swing",                //{NEW} String: Determines the easing method used in jQuery transitions. jQuery easing plugin is supported!
    direction: "horizontal",        //String: Select the sliding direction, "horizontal" or "vertical"
    reverse: false,                 //{NEW} Boolean: Reverse the animation direction
    animationLoop: true,            //Boolean: Should the animation loop? If false, directionNav will received "disable" classes at either end
    smoothHeight: false,            //{NEW} Boolean: Allow height of the slider to animate smoothly in horizontal mode
    startAt: 0,                     //Integer: The slide that the slider should start on. Array notation (0 = first slide)
    slideshow: true,                //Boolean: Animate slider automatically
    slideshowSpeed: 7000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
    animationSpeed: 600,            //Integer: Set the speed of animations, in milliseconds
    initDelay: 0,                   //{NEW} Integer: Set an initialization delay, in milliseconds
    randomize: false,               //Boolean: Randomize slide order
    fadeFirstSlide: true,           //Boolean: Fade in the first slide when animation type is "fade"
    thumbCaptions: false,           //Boolean: Whether or not to put captions on thumbnails when using the "thumbnails" controlNav.

    // Usability features
    pauseOnAction: true,            //Boolean: Pause the slideshow when interacting with control elements, highly recommended.
    pauseOnHover: false,            //Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
    pauseInvisible: true,   		//{NEW} Boolean: Pause the slideshow when tab is invisible, resume when visible. Provides better UX, lower CPU usage.
    useCSS: true,                   //{NEW} Boolean: Slider will use CSS3 transitions if available
    touch: true,                    //{NEW} Boolean: Allow touch swipe navigation of the slider on touch-enabled devices
    video: false,                   //{NEW} Boolean: If using video in the slider, will prevent CSS3 3D Transforms to avoid graphical glitches

    // Primary Controls
    controlNav: true,               //Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
    directionNav: true,             //Boolean: Create navigation for previous/next navigation? (true/false)
    prevText: "Previous",           //String: Set the text for the "previous" directionNav item
    nextText: "Next",               //String: Set the text for the "next" directionNav item

    // Secondary Navigation
    keyboard: true,                 //Boolean: Allow slider navigating via keyboard left/right keys
    multipleKeyboard: false,        //{NEW} Boolean: Allow keyboard navigation to affect multiple sliders. Default behavior cuts out keyboard navigation with more than one slider present.
    mousewheel: false,              //{UPDATED} Boolean: Requires jquery.mousewheel.js (https://github.com/brandonaaron/jquery-mousewheel) - Allows slider navigating via mousewheel
    pausePlay: false,               //Boolean: Create pause/play dynamic element
    pauseText: "Pause",             //String: Set the text for the "pause" pausePlay item
    playText: "Play",               //String: Set the text for the "play" pausePlay item

    // Special properties
    controlsContainer: "",          //{UPDATED} jQuery Object/Selector: Declare which container the navigation elements should be appended too. Default container is the FlexSlider element. Example use would be $(".flexslider-container"). Property is ignored if given element is not found.
    manualControls: "",             //{UPDATED} jQuery Object/Selector: Declare custom control navigation. Examples would be $(".flex-control-nav li") or "#tabs-nav li img", etc. The number of elements in your controlNav should match the number of slides/tabs.
    sync: "",                       //{NEW} Selector: Mirror the actions performed on this slider with another slider. Use with care.
    asNavFor: "",                   //{NEW} Selector: Internal property exposed for turning the slider into a thumbnail navigation for another slider

    // Carousel Options
    itemWidth: 0,                   //{NEW} Integer: Box-model width of individual carousel items, including horizontal borders and padding.
    itemMargin: 0,                  //{NEW} Integer: Margin between carousel items.
    minItems: 1,                    //{NEW} Integer: Minimum number of carousel items that should be visible. Items will resize fluidly when below this.
    maxItems: 0,                    //{NEW} Integer: Maxmimum number of carousel items that should be visible. Items will resize fluidly when above this limit.
    move: 0,                        //{NEW} Integer: Number of carousel items that should move on animation. If 0, slider will move all visible items.
    allowOneSlide: true,           //{NEW} Boolean: Whether or not to allow a slider comprised of a single slide

    // Callback API
    start: function(){},            //Callback: function(slider) - Fires when the slider loads the first slide
    before: function(){},           //Callback: function(slider) - Fires asynchronously with each slider animation
    after: function(){},            //Callback: function(slider) - Fires after each slider animation completes
    end: function(){},              //Callback: function(slider) - Fires when the slider reaches the last slide (asynchronous)
    added: function(){},            //{NEW} Callback: function(slider) - Fires after a slide is added
    removed: function(){},           //{NEW} Callback: function(slider) - Fires after a slide is removed
    init: function() {}             //{NEW} Callback: function(slider) - Fires after the slider is initially setup
  };

  //FlexSlider: Plugin Function
  $.fn.flexslider = function(options) {
    if (options === undefined) options = {};

    if (typeof options === "object") {
      return this.each(function() {
        var $this = $(this),
            selector = (options.selector) ? options.selector : ".slides > li",
            $slides = $this.find(selector);

      if ( ( $slides.length === 1 && options.allowOneSlide === true ) || $slides.length === 0 ) {
          $slides.fadeIn(400);
          if (options.start) options.start($this);
        } else if ($this.data('flexslider') === undefined) {
          new $.flexslider(this, options);
        }
      });
    } else {
      // Helper strings to quickly perform functions on the slider
      var $slider = $(this).data('flexslider');
      switch (options) {
        case "play": $slider.play(); break;
        case "pause": $slider.pause(); break;
        case "stop": $slider.stop(); break;
        case "next": $slider.flexAnimate($slider.getTarget("next"), true); break;
        case "prev":
        case "previous": $slider.flexAnimate($slider.getTarget("prev"), true); break;
        default: if (typeof options === "number") $slider.flexAnimate(options, true);
      }
    }
  };
})(jQuery);

/* jshint ignore:end */

'use strict';

$(document).ready(function() {

  $(window).resize(function() {
    $('.textCol').css({'marginTop':$('.imgCol img').height()+'px'});
  });
  $('.textCol').css({'marginTop':$('.imgCol img').height()+'px'});

  $(document).on('click', '.playBtn', function(/*event*/) {
    $('.invitationRequestApproved .banner2').hide();
    $('.video-container').fadeIn();
  });

	$(document).on('click', '.video', function (/*event*/) {
		var videoPlayer = document.getElementById('video');
		videoPlayer.setAttribute('controls','controls');
    var video = $(this).get(0);
    if (video.paused === false) {
      video.pause();
    } else {
      video.play();
    }

    return false;
	});

	//add some controls
	jQuery(function ($) {
    $('div.player').each(function () {
      var player = this;
      var getSetCurrentTime = createGetSetHandler(

      function () {
        $('input.time-slider', player).prop('value', $.prop(this, 'currentTime'));
      }, function () {
        try {
          $('video, audio', player).prop('currentTime', $.prop(this, 'value'));
        } catch (er) {}
      });
      var getSetVolume = createGetSetHandler(
        function () {
          $('input.volume-slider', player).prop('value', $.prop(this, 'volume'));

        }, function () {
          $('video, audio', player).prop('volume', $.prop(this, 'value'));
        }
      );
      $('video, audio', this).bind('durationchange updateMediaState', function () {
        var duration = $.prop(this, 'duration');
        if (!duration) {
          return;
        }
        $('input.time-slider', player).prop({
          'max': duration,
          disabled: false
        });
        $('span.duration', player).text(duration);
      }).bind('progress updateMediaState', function () {
        var buffered = $.prop(this, 'buffered');
        if (!buffered || !buffered.length) {
          return;
        }
        buffered = getActiveTimeRange(buffered, $.prop(this, 'currentTime'));
        $('span.progress', player).text(buffered[2]);
      }).bind('timeupdate', function () {
        $('span.current-time', player).text($.prop(this, 'currentTime'));
      }).bind('timeupdate', getSetCurrentTime.get).bind('emptied', function () {
        $('input.time-slider', player).prop('disabled', true);
        $('span.duration', player).text('--');
        $('span.current-time', player).text(0);
        $('span.network-state', player).text(0);
        $('span.ready-state', player).text(0);
        $('span.paused-state', player).text($.prop(this, 'paused'));
        $('span.height-width', player).text('-/-');
        $('span.progress', player).text('0');
      }).bind('waiting playing loadedmetadata updateMediaState', function () {
        $('span.network-state', player).text($.prop(this, 'networkState'));
        $('span.ready-state', player).text($.prop(this, 'readyState'));
      }).bind('play pause', function () {
        $('span.paused-state', player).text($.prop(this, 'paused'));
      }).bind('volumechange', function () {
        var muted = $.prop(this, 'muted');
        $('span.muted-state', player).text(muted);
        $('input.muted', player).prop('checked', muted);
        $('span.volume', player).text($.prop(this, 'volume'));
      }).bind('volumechange', getSetVolume.get).bind('play pause', function () {
        $('span.paused-state', player).text($.prop(this, 'paused'));
      }).bind('loadedmetadata updateMediaState', function () {
        $('span.height-width', player).text($.prop(this, 'videoWidth') + '/' + $.prop(this, 'videoHeight'));
      }).each(function () {
        if ($.prop(this, 'readyState') > $.prop(this, 'HAVE_NOTHING')) {
          $(this).triggerHandler('updateMediaState');
        }
      });

      $('input.time-slider', player).bind('input', getSetCurrentTime.set).prop('value', 0);
      $('input.volume-slider', player).bind('input', getSetVolume.set);

      $('input.play', player).bind('click', function () {
        $('video, audio', player)[0].play();
      });
      $('input.pause', player).bind('click', function () {
        $('video, audio', player)[0].pause();
      });
      $('input.muted', player).bind('click updatemuted', function () {
        $('video, audio', player).prop('muted', $.prop(this, 'checked'));
      }).triggerHandler('updatemuted');
      $('input.controls', player).bind('click', function () {
        $('video, audio', player).prop('controls', $.prop(this, 'checked'));
      }).prop('checked', true);

      $('select.load-media', player).bind('change', function () {
        var srces = $('option:selected', this).data('src');
        if (srces) {
          //the following code can be also replaced by the following line
          //$('video, audio', player).loadMediaSrc(srces).play();
          $('video, audio', player).removeAttr('src').find('source').remove().end().each(function () {
            var mediaElement = this;
            if (typeof srces === 'string') {
              srces = [srces];
            }
            $.each(srces, function (i, src) {
              if (typeof src === 'string') {
                src = {
                  src: src
                };
              }
              $(document.createElement('source')).attr(src).appendTo(mediaElement);
            });
          })[0].load();
          $('video, audio', player)[0].play();
        }
      }).prop('selectedIndex', 0);
    });
	});

	//helper for createing throttled get/set functions (good to create time/volume-slider, which are used as getter and setter)

	function createGetSetHandler(get, set) {
    var throttleTimer;
    var blockedTimer;
    var blocked;
    return {
      get: function () {
        if (blocked) {
          return;
        }
        return get.apply(this, arguments);
      },
      set: function () {
        clearTimeout(throttleTimer);
        clearTimeout(blockedTimer);

        var that = this;
        var args = arguments;
        blocked = true;
        throttleTimer = setTimeout(function () {
          set.apply(that, args);
          blockedTimer = setTimeout(function () {
            blocked = false;
          }, 30);
        }, 0);
      }
    };
	}

	function getActiveTimeRange(range, time) {
    var len = range.length;
    var index = -1;
    var start = 0;
    var end = 0;
    for (var i = 0; i < len; i++) {
      if (time >= (start = range.start(i)) && time <= (end = range.end(i))) {
        index = i;
        break;
      }
    }
    return [index, start, end];
	}

});

/* jshint ignore:start */

/*
A simple jQuery function that can add listeners on attribute change.
http://meetselva.github.io/attrchange/

About License:
Copyright (C) 2013 Selvakumar Arumugam
You may use attrchange plugin under the terms of the MIT Licese.
https://github.com/meetselva/attrchange/blob/master/MIT-License.txt
*/
(function($) {
   function isDOMAttrModifiedSupported() {
    var p = document.createElement('p');
    var flag = false;
    
    if (p.addEventListener) p.addEventListener('DOMAttrModified', function() {
      flag = true
    }, false);
    else if (p.attachEvent) p.attachEvent('onDOMAttrModified', function() {
      flag = true
    });
    else return false;
    
    p.setAttribute('id', 'target');
    
    return flag;
   }
   
   function checkAttributes(chkAttr, e) {
    if (chkAttr) {
      var attributes = this.data('attr-old-value');
      
      if (e.attributeName.indexOf('style') >= 0) {
        if (!attributes['style']) attributes['style'] = {}; //initialize
        var keys = e.attributeName.split('.');
        e.attributeName = keys[0];
        e.oldValue = attributes['style'][keys[1]]; //old value
        e.newValue = keys[1] + ':' + this.prop("style")[$.camelCase(keys[1])]; //new value
        attributes['style'][keys[1]] = e.newValue;
      } else {
        e.oldValue = attributes[e.attributeName];
        e.newValue = this.attr(e.attributeName);
        attributes[e.attributeName] = e.newValue; 
      }
      
      this.data('attr-old-value', attributes); //update the old value object
    }    
   }

   //initialize Mutation Observer
   var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

   $.fn.attrchange = function(o) {
     
    var cfg = {
      trackValues: false,
      callback: $.noop
    };
    
    //for backward compatibility
    if (typeof o === "function" ) { 
      cfg.callback = o; 
    } else { 
      $.extend(cfg, o); 
    }

      if (cfg.trackValues) { //get attributes old value
        $(this).each(function (i, el) {
          var attributes = {};
          for (var attr, i=0, attrs=el.attributes, l=attrs.length; i<l; i++){
              attr = attrs.item(i);
              attributes[attr.nodeName] = attr.value;
          }
          
          $(this).data('attr-old-value', attributes);
        });
      }
     
    if (MutationObserver) { //Modern Browsers supporting MutationObserver
      /*
         Mutation Observer is still new and not supported by all browsers. 
         http://lists.w3.org/Archives/Public/public-webapps/2011JulSep/1622.html
      */
      var mOptions = {
        subtree: false,
        attributes: true,
        attributeOldValue: cfg.trackValues
      };
  
      var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(e) {
          var _this = e.target;
          
          //get new value if trackValues is true
          if (cfg.trackValues) {
            /**
             * @KNOWN_ISSUE: The new value is buggy for STYLE attribute as we don't have 
             * any additional information on which style is getting updated. 
             * */
            e.newValue = $(_this).attr(e.attributeName);
          }
          
          cfg.callback.call(_this, e);
        });
      });
  
      return this.each(function() {
        observer.observe(this, mOptions);
      });
    } else if (isDOMAttrModifiedSupported()) { //Opera
      //Good old Mutation Events but the performance is no good
      //http://hacks.mozilla.org/2012/05/dom-mutationobserver-reacting-to-dom-changes-without-killing-browser-performance/
      return this.on('DOMAttrModified', function(event) {
        if (event.originalEvent) event = event.originalEvent; //jQuery normalization is not required for us 
        event.attributeName = event.attrName; //property names to be consistent with MutationObserver
        event.oldValue = event.prevValue; //property names to be consistent with MutationObserver 
        cfg.callback.call(this, event);
      });
    } else if ('onpropertychange' in document.body) { //works only in IE    
      return this.on('propertychange', function(e) {
        e.attributeName = window.event.propertyName;
        //to set the attr old value
        checkAttributes.call($(this), cfg.trackValues , e);
        cfg.callback.call(this, e);
      });
    }

    return this;
    }
})(jQuery);

// holy goat, what the fugg is this doing here?
(function(a){var b={numOfCol:5,offsetX:5,offsetY:5,blockElement:'div'};var c,d;var e=[];if(!Array.prototype.indexOf){Array.prototype.indexOf=function(a){var b=this.length>>>0;var c=Number(arguments[1])||0;c=c<0?Math.ceil(c):Math.floor(c);if(c<0)c+=b;for(;c<b;c++){if(c in this&&this[c]===a)return c}return-1}}var f=function(){e=[];for(var a=0;a<b.numOfCol;a++){g('empty-'+a,a,0,1,-b.offsetY)}};var g=function(a,c,d,f,g){for(var h=0;h<f;h++){var i=new Object;i.x=c+h;i.size=f;i.endY=d+g+b.offsetY*2;e.push(i)}};var h=function(a,b){for(var c=0;c<b;c++){var d=i(a+c,'x');e.splice(d,1)}};var i=function(a,b){for(var c=0;c<e.length;c++){var d=e[c];if(b=='x'&&d.x==a){return c}else if(b=='endY'&&d.endY==a){return c}}};var j=function(a,b){var c=[];for(var d=0;d<b;d++){c.push(e[i(a+d,'x')].endY)}var f=Math.min.apply(Math,c);var g=Math.max.apply(Math,c);return[f,g,c.indexOf(f)]};var k=function(a){if(a>1){var b=e.length-a;var c=false;var d,f;for(var g=0;g<e.length;g++){var h=e[g];var i=h.x;if(i>=0&&i<=b){var k=j(i,a);if(!c){c=true;d=k;f=i}else{if(k[1]<d[1]){d=k;f=i}}}}return[f,d[1]]}else{d=j(0,e.length);return[d[2],d[0]]}};var l=function(a,c){if(!a.data('size')||a.data('size')<0){a.data('size',1)}else if(a.data('size')>b.numOfCol){a.data('size',b.numOfCol)}var e=k(a.data('size'));var f=d*a.data('size')-(a.outerWidth()-a.width());a.css({width:f-b.offsetX*2,left:e[0]*d,top:e[1],position:'absolute'});var i=a.outerHeight();h(e[0],a.data('size'));g(a.attr('id'),e[0],e[1],a.data('size'),i)};a.fn.BlocksIt=function(g){if(g&&typeof g==='object'){a.extend(b,g)}c=a(this);d=c.width()/b.numOfCol;f();c.children(b.blockElement).each(function(b){l(a(this),b)});var h=j(0,e.length);c.height(h[1]+b.offsetY);return this}})(jQuery)

/* jshint ignore:end */

'use strict';

// Listing all APIs
angular.module('myApp.invest_msg',[]).constant('INVEST_MSGS',{
	'NO_INVEST_ROUND':'Fundraise coming soon.',
	'END_DATE_CROSSED':'Fundraise has ended.',
	'PENDING_FUNDING':'Funding round is pending.',
	'GOAL_REACHED':'Funding goal has been reached.',
	'MODIFY_STATE':'This funding round is on hold.',
	'COMPANY_POPUP_GOAL_MET':'Sorry, this funding round has already reached its goal.',
	'COMPANY_POPUP_ROUND_CLOSE':'Sorry, this funding round has closed.',
	'COMPANY_POPUP_MODIFY_PENDING':'This funding round is on hold. Please contact us if you would like to be notified if it reopens.',
	'COMPANY_POPUP_END_DATE_REACHED':'Sorry, this funding round has ended. Please contact us for more information.',
	'INVEST_FINISH_WIRE_MSG':'Please wire the funds within 48 hours. Check your email for a copy of the wire details.'

});

'use strict';
// jshint eqeqeq:false
// jshint eqnull:true

/* Controllers */

angular.module('myApp.controllers', []).controller(
  'AdminCtrl',
  ["$rootScope", "$scope", "CompanyForApproval", "$location", "AdminAdminPanel", "AdminCompanyPanel", "ApproveCompany", "AdminMemberPanel", "RejectCompany", "$cookieStore", "$routeParams", "URLS", "Auth", "AdminRejectInvestorStep2", "CreateNewAdmin", "GetRaisedFunding", "ActionOnFunding", "MarkNotificationRead", "MSG", "FlashMessage", "FlashOnNextRoute", "API_PATH", "BASE_PATH", "PPMTemplateId", "GetUsers", "UpdateUserInfo", "UserHistory", "UpdateUserEmail", "GetAllEscrows", "GetCompanyInvestmentDetails", "AdminTask", "DeleteInvestorTransaction", "InvestorHistory", "UpdateFundOptions", "AdminInvestorActions", "AdminAdminActions", "FundingRoundActions", "TimeoutMessage", "InvestorInvestments", "InvestorEvents", "InvestorFollowingList", "CompanyFollowers", "CompanyFundingRounds", "UserNote", "InvestorQuestions", function($rootScope, $scope, CompanyForApproval,  $location, AdminAdminPanel, AdminCompanyPanel, ApproveCompany, AdminMemberPanel, RejectCompany, $cookieStore, $routeParams, URLS, Auth, AdminRejectInvestorStep2, CreateNewAdmin, GetRaisedFunding, ActionOnFunding, MarkNotificationRead, MSG, FlashMessage, FlashOnNextRoute, API_PATH, BASE_PATH, PPMTemplateId, GetUsers, UpdateUserInfo, UserHistory, UpdateUserEmail, GetAllEscrows, GetCompanyInvestmentDetails, AdminTask, DeleteInvestorTransaction, InvestorHistory, UpdateFundOptions, AdminInvestorActions, AdminAdminActions, FundingRoundActions, TimeoutMessage, InvestorInvestments, InvestorEvents, InvestorFollowingList, CompanyFollowers, CompanyFundingRounds, UserNote, InvestorQuestions) {
    $scope.isAdmin = true;
    $scope.user = $rootScope.user;

    $scope.funding_template = 'views/partials/fundraising.html';
    $scope.openToInvestors = true;
    $scope.closedToInvestors = true;
    $scope.rejectedCompanies = true;

    $scope.fundingCompanyList=[];
    $scope.companyFundInvestorList=[];
    $scope.fundingCompanyData={};
    $scope.investorSum=0;

    $scope.company = {};

    $scope.raiseFundError='';

    $scope.open_tasks = true;
    $scope.closed_tasks = false;
    $scope.investor_tasks = true;
    $scope.company_tasks = true;

    $scope.searchTask= {
      meta:{}
    };

    $scope.funding_options=[];

    $scope.addNewAdmin = function(){
      CreateNewAdmin($.cookie('accessToken')).create({first_name: $scope.newAdminFName, last_name: $scope.newAdminLName, email: $scope.newAdminEmail}).$promise.then(
        function(value){
          $scope.allAdmin.push(value);
          $scope.newAdminFName = null;
          $scope.newAdminLName = null;
          $scope.newAdminEmail = null;
          FlashMessage.showFlash('neutral-d',MSG.SUCCESSFULL_INVITE);
          $('#inviteNewMemberModal').modal('hide');
        },
        function(error){
          if(error.data.email[0]){
            $scope.newAdminFName = null;
            $scope.newAdminLName = null;
            $scope.newAdminEmail = null;
            TimeoutMessage.showError('#invite_admin_error_msg',MSG.USER_EXIST);
            FlashMessage.showFlash('error',MSG.USER_EXIST);
          }
          else if(error.data.last_name[0]){
            FlashMessage.showFlash('error','Last name required');
            TimeoutMessage.showError('invite_admin_error_msg','Last name required');
          }
          else{
            FlashMessage.showFlash('error',MSG.FAILURE);
            TimeoutMessage.showError('invite_admin_error_msg','Last name required');
          }
        }
      );
    };

    $scope.fetchAdminTab = function(){
      AdminAdminPanel($.cookie('accessToken')).show().$promise.then(
        function(value){
          $scope.allAdmin=value;/*
          $scope.approvedAdmins = value.approved_admins;
          $scope.preAdmins = value.not_approved_admins;*/
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    $scope.fetchCompanyFundList=function(){
      GetAllEscrows($.cookie('accessToken')).list().$promise.then(
        function(value){
          $scope.fundingCompanyList=[];
          _.each(value.results,function(item){
            $scope.fundingCompanyList.push(item);
          });
        },
        function(/*error*/){}
      );
    };

    $scope.getCompanyDetails=function(company){
      GetCompanyInvestmentDetails($.cookie('accessToken')).list({id:company.cvc_funding_round}).$promise.then(
        function(value){
          $scope.companyFundInvestorList=[];
          $scope.funding_options = [];
          _.each(value.investor_list,function(item){
            $scope.companyFundInvestorList.push(item);
            $scope.investorSum+=parseInt(item.amount);
          });
          $scope.fundingCompanyData=value.escrow_details;
          $scope.fundingCompanyData.end_date= new Date($scope.fundingCompanyData.close_date);
          $scope.fundingCompanyData.funding_id=company.cvc_funding_round;
          $scope.fundingCompanyData.recommended_amounts = value.recommended_amounts;
          $scope.fundingCompanyData.local_maximum = value.local_maximum;
          $scope.fundingCompanyData.local_minimum = value.local_minimum;
          $scope.fundingCompanyData.local_end_date = value.local_end_date;
          _.each(value.recommended_amounts, function(value){
            $scope.funding_options.push(value.amount);
          });
        },
        function(/*error*/){}
      );
    };

    $scope.dateOptions = {
      'year-format': '\'yy\'',
      'starting-day': 1
    };
    $scope.format = 'yyyy-MM-dd';

    $scope.fetchCompanyTab = function(){
      AdminCompanyPanel($.cookie('accessToken')).show().$promise.then(
        function(value){
          $scope.prelimCompanies = value.preliminary_companies;
          $scope.step2_companies = value.step2_approval_companies;
          _.each(value.open_investment_companies, function(company){company.type = 'open';});
          _.each(value.closed_investment_companies, function(company){company.type = 'closed';});
          _.each(value.rejected_companies, function(company){company.type = 'rejected';});
          $scope.listed_companies = _.union(value.open_investment_companies,value.closed_investment_companies,value.rejected_companies);
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    var types = ['open','closed','rejected'];

    $scope.$watchCollection('[openToInvestors,closedToInvestors, rejectedCompanies]',function(value) {
      $scope.companyTypes = [];
      _.each(value, function(val,index){
        if (val){
          $scope.companyTypes.push(types[index]);
        }
      });
    });

    $scope.byCompanyType = function(company){
      return (_.indexOf($scope.companyTypes, company.type) !== -1);
    };

    var tasks = [false,true];
    var feeds = ['investor','entrepreneur'];

    $scope.$watchCollection('[open_tasks,closed_tasks]', function(value){
      $scope.taskType = [];
      _.each(value, function(val, index){
        if(val){
          $scope.taskType.push(tasks[index]);
        }
      });
    });

    $scope.$watchCollection('[investor_tasks,company_tasks]', function(value){
      $scope.feedType = [];
      _.each(value, function(val, index){
        if(val){
          $scope.feedType.push(feeds[index]);
        }
      });
    });

    $scope.byTaskType = function(company){
      return (_.indexOf($scope.taskType, company.is_completed) !== -1);
    };

    $scope.byFeedType = function(company){
      return (_.indexOf($scope.feedType, company.creator.account_type) !== -1);
    };

    $scope.goToCompanyProfile = function(company_id, companyType, companyStatus){
      if (companyType === 'rejected'){
        if(companyStatus === 'step1_rejected'){
          $location.path(URLS.REVIEW_COMPANY_INFO+company_id);
        }
        else{
          $location.path(URLS.PREVIEW_COMPANY+company_id);
        }
      } else {
        $location.path(URLS.COMPANY+company_id);
      }
    };

    $scope.logout = function(){
      FlashOnNextRoute.setMessage({type: 'neutral', text: MSG.LOGOUT});
      Auth.logout();
    };

    $scope.fetchMemberTab = function(){
      AdminMemberPanel($.cookie('accessToken')).show().$promise.then(
        function(value){
          $scope.members = value.members;

          _.each($scope.members, function(item){
            // make the phone number more readable
            var phone_number = item.user.phone_number;
            var temp_phone_array = phone_number.split('ext');
            item.user.phone_number = temp_phone_array[0]+' ext '+temp_phone_array[1];

            // make the status codes readable
            item.investor.status =
              {
                // MAGIC: hardcoded status strings in map key
                'step1_created': 'Created',
                'step1_approved': 'Invited',
                'step1_rejected': 'Rejected',
                'step2_created': 'Registered',
                'step2_approved': 'Approved',
                'step2_rejected': 'Rejected',
                // the other statuses are not used for investors
              }[item.investor.status];
          });
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    $scope.gotoAdminDashboard = function(where){
      if(where === 'user'){
        $location.path(URLS.ADMIN_DASHBOARD + '/user');
      }
      else{
        $location.path(URLS.ADMIN_DASHBOARD);
      }
    };

    $scope.initEntRegistration = function(){
      var id = $routeParams.id;
      CompanyForApproval($.cookie('accessToken')).show({id: id}).$promise.then(
        function(value){
          if (value.status === 'step1_created') {
            $scope.company = {name: value.name, website: value.website, id: value.id};
            $scope.puser = value.users[0];
            $scope.company_signup = value.signup_info;
            $scope.company_documents = [];
            $scope.user_roles = {roles: value.users[0].roles};
          } else {
            FlashOnNextRoute.setMessage({type: 'neutral-d', text: MSG.ALREADY_MODERATED});
            $location.path(URLS.ADMIN_DASHBOARD);
          }
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        });
    };

    $scope.approveCompany = function(id){
      ApproveCompany($.cookie('accessToken')).approve({company_id: id,notify:true}).$promise.then(
        function(/*value*/){
          $location.path(URLS.ADMIN_DASHBOARD);
          FlashMessage.showFlash('positive',MSG.ADMIN_APPROVE);

        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        });
    };

    $scope.approveCompanyWithoutNotification = function(id){
      ApproveCompany($.cookie('accessToken')).approve({company_id: id,notify:false}).$promise.then(
        function(/*value*/){
          $location.path(URLS.ADMIN_DASHBOARD);
          FlashMessage.showFlash('positive',MSG.ADMIN_APPROVE);

        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        });
    };

    $scope.rejectCompany = function(id){
      RejectCompany($.cookie('accessToken')).reject({company_id: id,notify:true}).$promise.then(
        function(/*value*/){
          FlashMessage.showFlash('positive',MSG.ADMIN_REJECT);
          $location.path(URLS.ADMIN_DASHBOARD);

        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        });
    };

    $scope.rejectCompanyWithoutNotification = function(id){
      RejectCompany($.cookie('accessToken')).reject({company_id: id,notify:false}).$promise.then(
        function(/*value*/){
          FlashMessage.showFlash('positive',MSG.ADMIN_REJECT);
          $location.path(URLS.ADMIN_DASHBOARD);

        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        });
    };

    $scope.rejectInvestorStep2=function(id){
      AdminRejectInvestorStep2($.cookie('accessToken')).approve({ id:id, approved_step:'step1_rejected',notify:true}).$promise.then(
        function(/*value*/){
          $location.path(URLS.ADMIN_DASHBOARD);
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        });
    };

    /* Review Funding */

    // Bindings for Bootstrap Date Picker

    $scope.minDate = ( $scope.minDate ) ? null : new Date();
    $scope.open1 = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened1 = true;
    };
    $scope.open2 = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened2 = true;
    };

    $scope.initFunding = function(){
      $scope.fund={};
      $scope.fund.funding_options=[];
      GetRaisedFunding($.cookie('accessToken')).get({ id: $routeParams.id}).$promise.then(
        function(value){
          $scope.amount_seeking = value.funding_details.seeking_amount;
          $scope.series = value.funding_details.series_type;
          $scope.company.name=value.company_name;
          $scope.funding_start_date = new Date(value.funding_details.start_date);
          $scope.funding_close_date = new Date(value.funding_details.end_date);
          $scope.company_id = value.funding_details.company;
          $scope.documents = value.documents;
          $scope.fund.local_maximum_funding_amount = value.funding_details.local_maximum;
          $scope.fund.local_minimum_funding_amount = value.funding_details.local_minimum;
          _.each(value.recommended_amounts, function(val){
            $scope.fund.funding_options.push(val.amount);
          });
          if(value.funding_details.ppm!='') {
            $scope.has_ppm_template_id=true;
            $scope.upload_template_id=value.funding_details.ppm;
          }
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    $scope.actionOnFunding = function(series,amount_seeking,funding_start_date,funding_close_date,message,action){
      var start_date = funding_start_date.getFullYear()+'-'+(funding_start_date.getMonth()+1)+'-'+funding_start_date.getDate();
      var end_date = funding_close_date.getFullYear()+'-'+(funding_close_date.getMonth()+1)+'-'+funding_close_date.getDate();

      var recommended_amount_array =[];
      var do_not_continue = false;
      $scope.raiseFundError ='';

      if(!$scope.has_ppm_template_id){
        do_not_continue = true;
        $scope.raiseFundError = 'Please add template id';
      }

      _.each($scope.fund.funding_options, function(item){
        if(isNaN(parseFloat(item))){
          do_not_continue = true;
          $scope.raiseFundError = 'Recommended amount cannot be left blank';
        }
        if(parseFloat(item) < parseFloat($scope.fund.local_minimum_funding_amount) || parseFloat(item)>parseFloat($scope.fund.local_maximum_funding_amount)){
          do_not_continue = true;
          $scope.raiseFundError = 'Recommended amount should be less than local maximum funding amount and greater than local minimum funding amount.';
        }

        var option_object = {};
        option_object.funding_round = $routeParams.id;
        option_object.amount = item;
        recommended_amount_array.push(option_object);
      });

      if($scope.fund.funding_options.length < 2){
        do_not_continue = true;
        $scope.raiseFundError = 'Please add at least two recommended amounts';
      }

      if(parseFloat($scope.fund.local_minimum_funding_amount) >= parseFloat($scope.fund.local_maximum_funding_amount)){
        do_not_continue = true;
        $scope.raiseFundError = 'Local minimum cannot be equal or greater than local maximum';
      }

      if(!($scope.fund.local_maximum_funding_amount) || !($scope.fund.local_minimum_funding_amount)){
        do_not_continue = true;
        $scope.raiseFundError = 'Please add local maximum and local minimum funding amount';
      }

      if(do_not_continue){
        $('.loader-overlay').fadeOut('fast');
        return;
      }
      var data = {
        id: $routeParams.id,
        series_type: series,
        seeking_amount: amount_seeking,
        start_date: start_date,
        company: $scope.company_id,
        end_date: end_date,
        action: action,
        comment: message,
        over_funding_amount: amount_seeking,
        minimum_funding_amount: $scope.fund.local_minimum_funding_amount,
        maximum_funding_amount: $scope.fund.local_maximum_funding_amount,
        recommended_amounts: recommended_amount_array,
        local_minimum: $scope.fund.local_minimum_funding_amount,
        local_maximum: $scope.fund.local_maximum_funding_amount,
        bbx_goal: amount_seeking,
        price_per_share: $scope.fund.price_per_share,
        valuation: $scope.fund.valuation
      };
      if (action === 'REVERTED'){
        data.url = URLS.REWORK_FUND_RAISING+'%s';
      }

      ActionOnFunding($.cookie('accessToken')).post(data).$promise.then(
        function(/*value*/){
          FlashMessage.showFlash('positive',MSG.SUCCESS);
          $location.path(URLS.ADMIN_DASHBOARD);
        },
        function(error){
          if(error.data.message){
            $scope.raiseFundError=error.data.message;
          }
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    $scope.returnFromPdfView = function(){
      $rootScope.back_template_stack='';
      $scope.funding_template = 'views/partials/fundraising.html';
    };

    /* End of Review Funding */

    $scope.gotoCompany=function(slug){
      $location.path(URLS.COMPANY+slug);
    };

    $scope.sendPPMTemplateId=function(template_id){
      PPMTemplateId($.cookie('accessToken')).upload({company:$scope.company_id, ppm:template_id}).$promise.then(
        function(value){
          $scope.has_ppm_template_id=true;
          $scope.upload_template_id=value.ppm;
          $scope.template_id='';
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    $scope.users={};

    $scope.getAllUsers = function(){
      GetUsers($.cookie('accessToken')).fetch({}).$promise.then(
        function(value){
          $scope.users.investors=value.investor_users;
          $scope.users.company_users=value.company_users;
        },
        function(/*error*/){
          FlashMessage.showFlash('error', MSG.FAILURE);
        }
      );
    };

    $scope.showUserDetails= function(a,b,c,d,e,f, type){
      $scope.showuser={};
      $scope.showuser.first_name=a;
      $scope.showuser.middle_name=b;
      $scope.showuser.last_name=c;
      $scope.showuser.type = type;

      var temp_user_no = d;
      var temp_user_array = temp_user_no.split('ext');
      $scope.showuser.phone_number = temp_user_array[0];
      $scope.showuser.phone_number_ext = temp_user_array[1];
      $scope.showuser.id=e;
      $scope.showuser.email=f;
    };

    $scope.clearProfileVar=function(){
      $scope.showuser={};
    };

    $scope.updateUserInfo = function(obj){
      var user_phone_number = obj.phone_number + 'ext' + obj.phone_number_ext;
      UpdateUserInfo($.cookie('accessToken')).update({id:obj.id, first_name:obj.first_name, middle_name:obj.middle_name, last_name:obj.last_name, phone_number:user_phone_number, email:obj.email}).$promise.then(
        function(/*value*/){
          FlashMessage.showFlash('neutral-d',MSG.SUCCESS);
          if(obj.type==='company')
          {
            $scope.companyObject.user.first_name = obj.first_name;
            $scope.companyObject.user.middle_name = obj.middle_name;
            $scope.companyObject.user.last_name = obj.last_name;
            $scope.companyObject.user.phone_number = user_phone_number;
          }
          else if(obj.type === 'investor')
          {
            $scope.investorObject.user.first_name = obj.first_name;
            $scope.investorObject.user.middle_name = obj.middle_name;
            $scope.investorObject.user.last_name = obj.last_name;
            $scope.investorObject.user.phone_number = user_phone_number;
          }
          $scope.getAllUsers();
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    $scope.userHistoryPage=function(id){
      $location.path(URLS.USER_HISTORY+id);
    };

    $scope.user_history_next_value=0;
    $scope.user_history_previous_value=0;

    $scope.fetchUserHistory=function(page_no){
      $scope.user_history=[];
      var id=$routeParams.id;
      UserHistory($.cookie('accessToken')).fetch({id:id, page:page_no}).$promise.then(
        function(value){
          $scope.user_history=value.results;
          if(value.next!=null){
            $scope.user_history_next_value=parseInt(value.next.substr(value.next.indexOf('=')+1, value.next.length));
            $scope.user_history_next=true;
          }
          else{
            $scope.user_history_next=false;
          }
          if(value.previous!=null){
            $scope.user_history_previous_value=parseInt(value.previous.substr(value.previous.indexOf('=')+1, value.previous.length));
            $scope.user_history_previous=true;
          }
          else{
            $scope.user_history_previous=false;
          }
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    $scope.editUserEmail=function(a,b, type){
      $scope.email_update={};
      $scope.email_update.new_email='';
      $scope.email_update.email=a;
      $scope.email_update.id=b;
      $scope.email_update.type= type;
    };

    $scope.updateUserEmailInfo=function(){
      UpdateUserEmail($.cookie('accessToken')).change({id:$scope.email_update.id, new_email:$scope.email_update.new_email, old_email:$scope.email_update.email}).$promise.then(
        function(/*value*/){
          FlashMessage.showFlash('neutral-d',MSG.SUCCESS);
          if($scope.email_update.type === 'company'){
            $scope.companyObject.user.email = $scope.email_update.new_email;
          }
          else if($scope.email_update.type === 'investor'){
            $scope.investorObject.user.email = $scope.email_update.new_email;
          }
          $scope.email_update={};
          $('.loader-overlay').fadeIn('fast');
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    $scope.fetchTaskList = function(){
      var user = $cookieStore.get('user');
      AdminTask($.cookie('accessToken')).list({id:user.entity_id}).$promise.then(
        function(value){
          $scope.task_list=value;
        },
        function(/*error*/){}
      );
    };

    $scope.gotoTask = function(note){
      if (!note.is_read) {
        MarkNotificationRead($.cookie('accessToken')).mark({id: note.id, is_read: 1}).$promise.then(
          function(/*value*/){
            note.is_read = true;
          },
          function(/*error*/){
            FlashMessage.showFlash('error',MSG.FAILURE);
          });
      }
      if (note.link){
        $location.path(note.link);
      }
    };

    $scope.addInvestAmountOption = function(){
      if($scope.fund.funding_options.length === 0){
        $scope.fund.funding_options.push(parseFloat($scope.fund.local_minimum_funding_amount));
      }
      else{
        $scope.fund.funding_options.push(parseFloat($scope.fund.local_maximum_funding_amount));
      }
    };

    $scope.deleteInvestor = function(investor){
      DeleteInvestorTransaction($.cookie('accessToken')).del({id:investor.id}).$promise.then(
        function(/*value*/){
          $scope.companyFundInvestorList = _.without($scope.companyFundInvestorList, investor);
          $scope.fetchCompanyFundList();
        },
        function(error){
          FlashMessage.showFlash('error',error.data.message);
        }
      );
    };

    $scope.copyDeletingInvestor = function(investor){
      $scope.deleteInvestorTransaction = angular.copy(investor);
    };

    $scope.showInvestorHistory = function(investor){
      InvestorHistory($.cookie('accessToken')).list({id:investor.cvc_investor}).$promise.then(
        function(value){
          $scope.investor_history=groupByTimestamp(value.results);
        },
        function(/*error*/){}
      );
    };

    function groupByTimestamp(value){
      return _.groupBy(value, function(history){ return setTimePeriod(history.created_at); });
    }

    function setTimePeriod(str){
      var date = new Date(str);
      var monthNames = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ];
      var month = monthNames[date.getMonth()];
      var year = date.getFullYear();
      return month+' '+year;
    }

    $scope.changeOptionAmount = function(which, amount){
      if(which === 'min'){
        if($scope.funding_options.length){
          $scope.funding_options[0] = which;
        }
      }
      else if(which === 'max'){
        if($scope.funding_options.length){
          $scope.funding_options[$scope.funding_options.length - 1] = amount;
        }
      }
    };

    $scope.updateFundOptions = function(id){
      $scope.recommended_amount_error='';
      var recommended_amounts=[];
      var i=0;
      _.each($scope.funding_options ,function(val){
        var option={
          amount:val,
          funding_round:id,
          id:$scope.fundingCompanyData.recommended_amounts[i].id
        };
        i++;
        recommended_amounts.push(option);
      });
      if(parseInt($scope.fundingCompanyData.local_minimum) < parseInt($scope.fundingCompanyData.minimum))
      {
        $scope.recommended_amount_error='Local minimum cannot be less than '+$scope.fundingCompanyData.minimum;
        return;
      }
      if(parseInt($scope.fundingCompanyData.local_maximum) > parseInt($scope.fundingCompanyData.maximum)){
        $scope.recommended_amount_error='Local maximum cannot be greater than '+$scope.fundingCompanyData.maximum;
        return;
      }
      UpdateFundOptions($.cookie('accessToken')).update({id:id, recommended_amounts:recommended_amounts, local_maximum:$scope.fundingCompanyData.local_maximum, local_minimum:$scope.fundingCompanyData.local_minimum}).$promise.then(
        function(/*value*/){
          $('#recommendedOptionsModal').modal('hide');
          FlashMessage.showFlash('neutral-d',MSG.SUCCESS);
        },
        function(error){
          $scope.recommended_amount_error = error.data.message;
          FlashMessage.showFlash('error', MSG.FAILURE);
        }
      );
    };

    $scope.adminInvestorActions = function(inv,status,flag){
      AdminInvestorActions($.cookie('accessToken')).update({id:inv, type:status, value:!flag }).$promise.then(
        function(/*value*/){
          FlashMessage.showFlash('neutral-d',MSG.SUCCESS);
        },
        function(/*error*/){
          FlashMessage.showFlash('error', MSG.FAILURE);
        }
      );
    };

    $scope.adminDoAdminsActions = function(admin, status, flag){
      AdminAdminActions($.cookie('accessToken')).update({id:admin, type:status, value:!flag}).$promise.then(
        function(/*value*/){
          FlashMessage.showFlash('neutral-d',MSG.SUCCESS);
        },
        function(/*error*/){
          FlashMessage.showFlash('error', MSG.FAILURE);
        }
      );
    };

    $scope.fundingRoundAction = function(fund,tag){
      if(tag === 'is_hidden'){
        FundingRoundActions($.cookie('accessToken')).update({id:fund.cvc_funding_round, is_hidden:!fund.is_hidden, is_recommended:fund.is_recommended}).$promise.then(
          function(value){
            fund.is_hidden = value.is_hidden;
            fund.is_recommended = value.is_recommended;
          },
          function(/*error*/){
            FlashMessage.showFlash('error',MSG.FAILURE);
          }
        );
      }
      else if(tag === 'is_recommended'){
        FundingRoundActions($.cookie('accessToken')).update({id:fund.cvc_funding_round, is_hidden:fund.is_hidden, is_recommended:!fund.is_recommended}).$promise.then(
          function(value){
            fund.is_hidden = value.is_hidden;
            fund.is_recommended = value.is_recommended;
          },
          function(/*error*/){
            FlashMessage.showFlash('error',MSG.FAILURE);
          }
        );
      }
    };

    $scope.seeDocsOnFund = function(doc_path){
      if(doc_path){
        $scope.pdfUrl = doc_path;
      }

      $('.loader-overlay-2').fadeIn('fast');

      $scope.funding_template = 'components/pdf-viewer/pdf-viewer.html';
      $.getScript( 'bower_components/pdfjs-dist/build/pdf.worker.js' )
      .done(function( script, textStatus,data ) {
        $('.loader-overlay').fadeOut('fast');
        if(data.status == 200 || data.status == 201){
          $('.loader-overlay-2').fadeOut('fast');
        }
      })
      .fail(function( /*jqxhr, settings, exception*/ ) {
        $('.loader-overlay-2').fadeOut('fast');
        $('.loader-overlay').fadeOut('fast');
      });
    };

    $scope.getInvestorInvestments = function(investor_id){
      $scope.showInvestorInvestments = !$scope.showInvestorInvestments;
      InvestorInvestments($.cookie('accessToken')).fetch({id:investor_id}).$promise.then(
        function(value){
          $scope.investorInvestments = value;
        },
        function(error){
          FlashMessage.showFlash('error',error.data.message);
        }
      );
    };

    $scope.setInvestorForDetails = function(inv_obj){
      $scope.investorObject = inv_obj;
      $scope.showInvestorInvestments = false;
      $scope.showInvestorLogs = false;
      $scope.showInvestorLogPage = 1;
      $scope.showInvestorFollowing = false;
      $scope.showInvestorQuestions = false;
      $scope.getNote(inv_obj.user.id);
    };

    $scope.setCompanyForDetails = function(com_obj){
      $scope.companyObject = com_obj;
      $scope.showFollowers = false;
      $scope.showFundingRounds = false;
      $scope.getNote(com_obj.user.id);
    };

    $scope.getInvestorEvents = function(investor_id){
      $scope.showInvestorLogs = !$scope.showInvestorLogs;
      InvestorEvents($.cookie('accessToken')).fetch({id:investor_id,page:$scope.showInvestorLogPage}).$promise.then(
        function(value){
          $scope.investorEvents = value;
        },
        function(error){
          FlashMessage.showFlash('error',error.data.message);
        }
      );
    };

    $scope.getInvestorFollowingList = function(investor_id){
      $scope.showInvestorFollowing = !$scope.showInvestorFollowing;
      InvestorFollowingList($.cookie('accessToken')).fetch({id:investor_id}).$promise.then(
        function(value){
          $scope.investorFollowingList = value;
        },
        function(error){
          FlashMessage.showFlash('error',error.data.message);
        }
      );
    };

    $scope.getCompanyFollowers = function(company_id){
      $scope.showFollowers =! $scope.showFollowers;
      CompanyFollowers($.cookie('accessToken')).fetch({id:company_id}).$promise.then(
        function(value){
          $scope.companyFollowers = value;
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    $scope.getCompanyFundingRounds = function(company_id){
      $scope.showFundingRounds = !$scope.showFundingRounds;
      CompanyFundingRounds($.cookie('accessToken')).fetch({id:company_id}).$promise.then(
        function(value){
          $scope.companyFundingRounds = value;
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    $scope.getNote = function(user_id){
      UserNote($.cookie('accessToken')).get({id:user_id}).$promise.then(
        function(value){
          $scope.user_notes = value;
        },
        function(/*error*/){}
      );
    };

    $scope.makeNewNote = function(user_id){
      UserNote($.cookie('accessToken')).create({id:user_id,type:'reminder'}).$promise.then(
        function(value){
          $scope.new_note = value;
          $scope.make_a_new_note = true;
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    $scope.saveNote = function(note_,user_id){
      UserNote($.cookie('accessToken')).add({id:note_.id, note:note_.note, type:note_.type,user_id:user_id}).$promise.then(
        function(/*value*/){
          FlashMessage.showFlash('neutral-d',MSG.NOTE_SAVE);
          $scope.make_a_new_note = false;
          $scope.getNote(user_id);
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    $scope.deleteNote = function(note_,user_id,hide_note){
      UserNote($.cookie('accessToken')).del({user_id:user_id, id:note_.id}).$promise.then(
        function(/*value*/){
          FlashMessage.showFlash('neutral-d',MSG.SUCCESS);
          if(hide_note){
            $scope.make_a_new_note = false;
          }
          $scope.getNote(user_id);
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    $scope.editNote = function(/*note*/){
      //TODO
    };

    $scope.getInvestorQuestions = function(inv_id){
      $scope.showInvestorQuestions = !$scope.showInvestorQuestions;
      InvestorQuestions($.cookie('accessToken')).get({id:inv_id}).$promise.then(
        function(value){
          $scope.investorQuestions = value;
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    $scope.removeRecommendedAmount = function(index){
      $scope.fund.funding_options.splice(index,1);
    };

    $scope.getAndLoadPDF = function(obj){
      if(obj.url.document_url!=null || obj.url.document_url == ''){
        window.open(BASE_PATH.API() + API_PATH.PDF_DATA+'?url='+obj.url.document_url);
      }
      else{
        FlashMessage.showFlash('error',MSG.CANNOT_OPEN_PDF);
      }
    };

  }]
);

'use strict';
// jshint eqeqeq:false
// jshint eqnull:true

/* Controllers */

angular.module('myApp.controllers').controller(
  'UtilCtrl',
  ["$rootScope", "$scope", "$cookieStore", "$location", "$interval", "Auth", "$routeParams", "LinkToken", "URLS", "ForgotPassword", "MSG", "FlashMessage", "FlashOnNextRoute", "SellShares", "Users", "Brokerages", function(
    $rootScope,
    $scope,
    $cookieStore,
    $location,
    $interval,
    Auth,
    $routeParams,
    LinkToken,
    URLS,
    ForgotPassword,
    MSG,
    FlashMessage,
    FlashOnNextRoute,
    SellShares,
    Users,
    Brokerages
  ) {
    $scope.enableSignInAs = $location.search().enableSignInAs;
    $scope.share = {}; // used by stocks.html
    $scope.errors={
      message:{value:''},
      password:{value:''},
      forgot:{value:''}
    };

    $scope.forgot_password = {};

    $scope.goTo = function(path){
      $location.path(path);
    };

    var userRole = {
      'admin': 1,
      'entrepreneur': 2,
      'investor': 4
    };

    // TODO: horribly bad name, these are actually messages, should be renamed all the way out to $rootScope.user_type
    var user_types = {
      'entrepreneur':MSG.REGISTER_AS_ENTREPRENEUR,
      'investor':MSG.REGISTER_AS_INVESTOR,
      'admin':MSG.REGISTER_AS_ADMIN
    };

    $scope.init = function(){
      var user = $cookieStore.get('user');
      if (user){
        kulizaSpaghettiGoto10(user);
      }
    };

    function kulizaSpaghettiGoto10 (user){
      var authUser = Users.authorized();

      if (user){
        if (user.role == 1){
          $location.path(URLS.ADMIN_DASHBOARD);
        }
        if (user.role == 2){
          if (user.status === 'step1_approved'){
            $location.path(URLS.ENT_REGISTRATION_II+user.slug);
          }
          if (user.status === 'step2_created'){
            $location.path(URLS.SUCCESSFUL_SUBMISSION);
          }
          if (user.status === 'step2_approved' || user.status === 'edited' || user.status === 'edit_approved' || user.status === 'edit_rejected') {
            $location.path(URLS.COMPANY+user.slug);
          }
        }
        if (user.role == 4){
          if(user.status === 'step1_approved'){
            $location.path(URLS.INVESTOR_QUESTIONNAIRE_I+user.entity_id);
          }
          if (user.status === 'step2_created'){
            $location.path(URLS.INVESTOR_LANDING);
          }
          if(user.status === 'step2_approved'){
            $location.path(URLS.INVESTOR_DASHBOARD);
          }
        }
        if (authUser) {
          if (authUser.isInGroup(Users.Groups.BROKERAGE_ADMINS)) {
            Brokerages.active().then(function(brokerage) {
              $location.path(URLS.FIRM_SETTINGS(brokerage.slug()));
            });
          }
        }
      }
    }
    $scope.initHome=function(){
      if($location.path() === '/sign-in'){
        $('.modal-backdrop').remove();
      }
    };

    $scope.createAndSignIn = function(username,password){
      Auth.activate.go({token: $rootScope.token, grant_type: 'password', username: username, password: password, client_id: 'test', client_secret: 'testcvc'}).$promise.then(

        function(value){
          var authInfo;

          if (!value.message){
            $.removeCookie('accessToken');
            $cookieStore.remove('user');
            FlashOnNextRoute.clearQueue();
            $cookieStore.remove('refreshToken');
            $rootScope.user = null;
            authInfo = {
              accessToken: value.access_token,
              isImpersonation: value.is_impersonation
            };
            Users.setAuthorized(value.user_api2, authInfo);
            var date = new Date();
            var s = value.expires_in;
            date.setTime(date.getTime() + (s * 1000));
            $.cookie('accessToken', value.access_token, { expires: date, path: '/' });
            $cookieStore.put('refreshToken', value.refresh_token);
            var role = userRole[value.account_type];
            $rootScope.user = {
              'username': value.username,
              'role': role,
              'status': value.status,
              'entity_id': value.entity_id,
              'slug':value.company_slug,
              'profile': value.user
            };
            if (role == 4) {
              $cookieStore.put('user', {'username': value.username, 'slug':value.company_slug,
                               'role': role, 'status': value.status, 'entity_id': value.entity_id, 'profile': value.user, 'investor_type': value.investor_type, 'is_verified':value.is_verified});
              $rootScope.user.investor_type = value.investor_type;
              $rootScope.user.is_verified = value.is_verified;
            } else {
              $cookieStore.put('user', {'username': value.username, 'slug':value.company_slug,
                               'role': role, 'status': value.status, 'entity_id': value.entity_id, 'profile': value.user});
            }
            kulizaSpaghettiGoto10($rootScope.user);
          } else {
            FlashMessage.showFlash('error',value.message);
          }
        },
        function(error){
          console.log(error);
          if(error.data.message){
            $scope.errors.message.value=error.data.message;
          }
          if (error.status === 404){
            $scope.errors.message.value=MSG.NO_SUCH_USER;
          }
          delete $rootScope.user;
        }
      );
    };

    $scope.login = function(username,password,signInAs){
      var requestParams = {
        grant_type: 'password',
        username: username,
        password: password,
        client_id: 'test',
        client_secret: 'testcvc'
      };

      if(signInAs) {
        requestParams.sign_in_as = signInAs;
      }
      Auth.login.getToken(requestParams).$promise.then(
        function(value){
          var authInfo;
          if (!value.message){
            $.removeCookie('accessToken');
            $cookieStore.remove('user');
            FlashOnNextRoute.clearQueue();
            $cookieStore.remove('refreshToken');
            $rootScope.user = null;
            if(value.user.is_suspended){
              $location.path(URLS.ACCOUNT_SUSPENDED);
              return;
            }
            authInfo = {
              accessToken: value.access_token,
              isImpersonation: value.is_impersonation
            };
            Users.setAuthorized(value.user_api2, authInfo);
            var date = new Date();
            var s = value.expires_in;
            date.setTime(date.getTime() + (s * 1000));
            $.cookie('accessToken', value.access_token, { expires: date, path: '/' });
            $cookieStore.put('refreshToken', value.refresh_token);
            var role = userRole[value.account_type];
            $rootScope.a_msg_on_sign_in_screen = '';
            $rootScope.user = {
              'username': value.username,
              'role': role,
              'status': value.status,
              'entity_id': value.entity_id,
              'slug':value.company_slug,
              'profile': value.user
            };
            if (role == 4) {
              $cookieStore.put('user', {'username': value.username, 'slug':value.company_slug,
                               'role': role, 'status': value.status, 'entity_id': value.entity_id, 'profile': value.user, 'investor_type': value.investor_type, 'is_verified':value.is_verified});
              $rootScope.user.investor_type = value.investor_type;
              $rootScope.user.is_verified = value.is_verified;
              $rootScope.user.progress_list = value.progress_list;
            } else {
              $cookieStore.put('user', {'username': value.username, 'slug':value.company_slug,
                               'role': role, 'status': value.status, 'entity_id': value.entity_id, 'profile': value.user});
            }
            kulizaSpaghettiGoto10($rootScope.user);
          } else {
            $scope.errors.message.value = value.message;
          }
        },
        function(error){
          $rootScope.a_msg_on_sign_in_screen = '';
          if(error.data.message){
            $scope.errors.message.value=error.data.message;
          }
          if (error.status === 404){
            $scope.errors.message.value=MSG.NO_SUCH_USER;
          }
          delete $rootScope.user;
        }
      );
    };

    $scope.logout = function(){
      FlashOnNextRoute.setMessage({type: 'neutral', text: MSG.LOGOUT});
      Auth.logout();
    };

    $scope.fetchToken = function(){
      LinkToken.fetch({token: $routeParams.token}).$promise.then(
        function(value){
          if (value.is_activated){
            $cookieStore.remove('user');
            $.removeCookie('accessToken');
            $rootScope.user = null;
            $cookieStore.remove('refreshToken');
            FlashOnNextRoute.clearQueue();
            $rootScope.a_msg_on_sign_in_screen = MSG.ALREADY_ACTIVATED;
            $location.path(URLS.SIGN_IN);
          } else {
            $rootScope.user_type = user_types[value.user.account_type];
            $rootScope.token = $routeParams.token;
            $rootScope.account_type=userRole[value.user.account_type];
            $scope.email = value.user.email;
          }
        },
        function(error){
          console.log(error);
          if(error.data.message==='Company Profile does not exist'){
            Auth.logout();
          }
        }
      );
    };

    $scope.forgotPassword=function(){
      if($scope.forgot_password.email){
        ForgotPassword($.cookie('accessToken')).forgot({email:$scope.forgot_password.email}).$promise.then(

          function(/*value*/){
            $scope.errors.message.value='An email has been sent to '+$scope.forgot_password.email+'. Please check your email.';
            $scope.forgotPassEmailClean();
            $('.forgotPasswordModal, .forgotPasswordModalOverlay').hide();
          },
          function(error){
            console.log(error);
            $scope.errors.forgot.value=error.data.message;
          }
        );
      }
    };

    $scope.forgotPassEmailClean = function(){
      $scope.forgot_password.email = '';
      $scope.errors.forgot.value = '';
    };

    $scope.ourGoto=function(view_name){
      if(view_name === 'about'){
        $location.path(URLS.ABOUT);
      }
    };

    $scope.removeSignInError=function(){
      $scope.errors.message.value='';
    };

    // TODO - for investors to request us to help them sell shares in a private company that they hold... currently never called but eventually fix up stocks.html and hook this back up
    $scope.share = {};
    $scope.sellShares = function(){
      SellShares($.cookie('accessToken')).sell({name:$scope.share.name, email:$scope.share.email, contact:$scope.share.phone_no, company_name:$scope.share.company, shares_count: $scope.share.no, message:$scope.share.message}).$promise.then(
        function(value){
          console.log(value);
        },
        function(error){
          console.log(error);
        }
      );
    };

  }]
);

'use strict';
// jshint eqeqeq:false
// jshint eqnull:true

/* Controllers */

angular.module('myApp.controllers').controller(
  'CompanyCtrl',
  ["$scope", "formatters", "$rootScope", "Auth", "$cookieStore", "CompanyPrivateView", "$routeParams", "$location", "URLS", "GetCompanyUpdates", "RaiseCompanyFund", "GetRaisedFunding", "DoDocusign", "$sce", "MSG", "FlashMessage", "DocusignResult", "FlashOnNextRoute", "RaiseCompanyFunding", "ContactCitizen", "RecommendCompany", "PPMTemplateId", "CompanySnapshot", "BBXEscrowFunding", "$timeout", "InvestorWireDetails", "InvestQuestions", "RFAQuestions", "DoIRS", "IRSResult", "INVEST_MSGS", "DocSignStep3", "Users", function(
    $scope,
    formatters,
    $rootScope,
    Auth,
    $cookieStore,
    CompanyPrivateView,
    $routeParams,
    $location,
    URLS,
    GetCompanyUpdates,
    RaiseCompanyFund,
    GetRaisedFunding,
    DoDocusign,
    $sce,
    MSG,
    FlashMessage,
    DocusignResult,
    FlashOnNextRoute,
    RaiseCompanyFunding,
    ContactCitizen,
    RecommendCompany,
    PPMTemplateId,
    CompanySnapshot,
    BBXEscrowFunding,
    $timeout,
    InvestorWireDetails,
    InvestQuestions,
    RFAQuestions,
    DoIRS,
    IRSResult,
    INVEST_MSGS,
    DocSignStep3,
    Users
  ) {
    var id = $routeParams.id;
    $scope.company_template = 'views/founder-dashboard.html';
    $scope.funding_template = 'views/partials/fundraising.html';
    $scope.legal_documents = 'views/partials/investor-flow-one.html';
    $scope.legal_documents_bank = 'views/partials/investor-flow-six.html';
    $scope.contact_citizen={};
    $scope.docusign={};
    $scope.pdfUrl = '';
    $scope.ques = {};
    $scope.ques.ent_ques = [];
    $scope.rsa = {};

    $scope.investment_round = {
      'no_round': INVEST_MSGS.NO_INVEST_ROUND,
      'end_date_crossed':INVEST_MSGS.END_DATE_CROSSED,
      'goal_reached':INVEST_MSGS.GOAL_REACHED,
      'modify_state':INVEST_MSGS.MODIFY_STATE,
      'pending_state':INVEST_MSGS.PENDING_FUNDING
    };

    $scope.embed='';

    $scope.statesMap = {
      'started':URLS.LEGAL_DOCUMENTS,
      'ppm':URLS.BANK_PAYMENT,
      'payment':URLS.INVESTOR_QUESTIONNAIRE,
      'questionnaire':URLS.RISK_FACTOR_ACK,
      'ra':URLS.IRA
    };

    // This functions runs on page initiation
    $scope.init = function(){
      var user = $cookieStore.get('user');
      $scope.founder_dashboard_left_card = 'views/founderCard1.html';
      if($routeParams.snapshot){
        CompanySnapshot($.cookie('accessToken')).details({id:$routeParams.snapshot}).$promise.then(
          function(value){
            setSnapshotData(value);
            $scope.setTemplates(user);
          },
          function(error){
            console.log(error);
          }
        );
      }
      else{
        CompanyPrivateView($.cookie('accessToken')).show({id: id}).$promise.then(
          function(value){
            setCompanyData(value);
            $scope.setTemplates(user);
          },
          function(error){
            console.log(error);
          }
        );
      }
    };

    $scope.setTemplates=function(user){
      // Admin View
      if (user.role == 1 ){
        if($scope.apply_button){
          $scope.another_card='views/partials/investor_no_current_funding.html';
          $scope.company.msg = INVEST_MSGS.NO_INVEST_ROUND;
        }
        else if(!$scope.apply_button && $scope.funding == null){
          $scope.another_card='views/partials/company_funding_pending_card.html';
        }
        else if(!$scope.apply_button && $scope.funding != null){
          $scope.another_card='views/partials/company_with_fund_card.html';
        }
        $scope.showInvOpt = false;
      }
      // Entrepreneur View
      if (user.role == 2 ){
        if($scope.apply_button){
          $scope.another_card='views/partials/company_apply_fund_card.html';
        }
        else if(!$scope.apply_button && $scope.funding == null){
          $scope.another_card='views/partials/company_funding_pending_card.html';
        }
        else if(!$scope.apply_button && $scope.funding != null){
          $scope.another_card='views/partials/company_with_fund_card.html';
        }
        $scope.showInvOpt = false;
      }
      // Investor View
      if (user.role == 4 ){
        if($scope.funding != null){
          $scope.another_card = 'views/partials/investor_card.html';
        }
        else{
          $scope.another_card = 'views/partials/investor_no_current_funding.html';
        }
        $scope.showInvOpt = true;
      }
    };

    $scope.logout = function(){
      FlashOnNextRoute.setMessage({type: 'neutral', text: MSG.LOGOUT});
      Auth.logout();
    };

    $scope.gotoEditCompany = function(){
      $location.path(URLS.EDIT_COMPANY+$routeParams.id);
    };

    /* Updates Tab */

    $scope.loadUpdatesTab = function(){
      GetCompanyUpdates($.cookie('accessToken')).get({id: $rootScope.company_id}).$promise.then(
        function(value){
          $scope.updates = groupByTimestamp(value);
        },
        function(error){
          console.log(error);
        });
    };

    function groupByTimestamp(value){
      return _.groupBy(value, function(update){ return setTimePeriod(update.modified_at); });
    }

    function setTimePeriod(str){
      var date = new Date(str);
      var monthNames = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ];
      var month = monthNames[date.getMonth()];
      var year = date.getFullYear();
      return month+' '+year;
    }

    /* End of updates tab */


    /* Company Funding */

    $scope.gotoFunding = function(){
      $location.path(URLS.FUND_RAISING);
    };

    $scope.raiseFunding = function(series, amount_seeking, funding_start_date, funding_close_date){
      var start_date = funding_start_date;
      var end_date = funding_close_date;
      start_date = start_date.getFullYear()+'-'+(start_date.getMonth()+1)+'-'+start_date.getDate();
      end_date = end_date.getFullYear()+'-'+(end_date.getMonth()+1)+'-'+end_date.getDate();
      if($location.path()===URLS.FUND_RAISING){
        RaiseCompanyFund($.cookie('accessToken')).post({series_type:series, seeking_amount: amount_seeking, start_date: start_date, company: $cookieStore.get('user').entity_id, end_date: end_date, url: URLS.REVIEW_FUND_RAISING+'%s'}).$promise.then(
          function(/*value*/){
            $location.path(URLS.COMPANY+$cookieStore.get('user').slug);
          },
          function(error){
            if(error.data.seeking_amount){
              $scope.raiseFundError=error.data.seeking_amount[0];
            }
            if(error.data.series_type){
              $scope.raiseFundError=error.data.series_type[0];
            }
          }
        );
      }
      else{
        RaiseCompanyFunding($.cookie('accessToken')).put({id: $routeParams.id, series_type:series, seeking_amount: amount_seeking, start_date: start_date, company: $cookieStore.get('user').entity_id, end_date: end_date, url: URLS.REVIEW_FUND_RAISING+'%s'}).$promise.then(
          function(/*value*/){
            $location.path(URLS.COMPANY+$cookieStore.get('user').slug);
          },
          function(error){
            console.log(error);
          }
        );
      }
    };

    $scope.initFunding = function(){
      var path = $location.path();
      var lastIndexOfSlash=path.lastIndexOf('/');
      var currentPath =path.substring(0,lastIndexOfSlash+1);
      if (currentPath === URLS.REWORK_FUND_RAISING){
        GetRaisedFunding($.cookie('accessToken')).get({ id: $routeParams.id}).$promise.then(
          function(value){
            $scope.message = value.funding_details.comment;
            $scope.amount_seeking = value.funding_details.seeking_amount;
            $scope.series = value.funding_details.series_type;
            $scope.funding_start_date = new Date(value.funding_details.start_date);
            $scope.funding_close_date = new Date(value.funding_details.end_date);
            $scope.company = value.company;
            $scope.documents = value.documents;
          },
          function(error){
            console.log(error);
          }
        );
      }
    };

    // Bindings for Bootstrap Date Picker

    $scope.minDate =  new Date();
    $scope.open1 = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened1 = true;
    };
    $scope.open2 = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened2 = true;
    };
    $scope.dateOptions = {
      'year-format': '\'yy\'',
      'starting-day': 1
    };
    $scope.format = 'yyyy-MM-dd';


    /* End of company funding */

    $scope.beginLegalProcess=function(amount){
      if(amount === null || amount.toString().trim() === ''){
        $scope.company_invest_error=MSG.VALID_AMOUNT;
        return;
      }

      var fundraiseRemaining = Math.max(parseFloat($scope.funding.seeking_amount) - parseFloat($scope.so_far_raised), 0.0);
      var maxInvestment = Math.min(fundraiseRemaining, $scope.funding.local_maximum);
      var minInvestment = Math.min(parseFloat($scope.funding.local_minimum), maxInvestment);

      if (amount < minInvestment || amount > maxInvestment) {
        $scope.company_invest_error = MSG.FUNDING_AMOUNT_OUT_OF_RANGE(
          formatters.numberAsMoney(minInvestment),
          formatters.numberAsMoney(maxInvestment)
        );
        return;
      }
      var id=$routeParams.id;
      $rootScope.invest_amount=amount;
      $rootScope.invest_company_name = $scope.company.name;
      var cookie_data = {
        'company_id':$scope.funding.company,
        'funding_id':$scope.funding.id,
        'invest_amount':amount,
        'company_name':$scope.company.name
      };
      $.cookie('company',JSON.stringify(cookie_data),{path:'/'});
      $location.path(URLS.LEGAL_DOCUMENTS+id);
    };

    $scope.initInvestStep1 = function(){
      var company_cookie = JSON.parse($.cookie('company'));
      $rootScope.invest_amount = company_cookie.invest_amount;
      $scope.invest_company_name = company_cookie.company_name;
    };

    $scope.removeInvestAmountError = function(){
      $scope.company_invest_error='';
    };

    $scope.$watch('embed',function(){
    });

    function trustSrc(src) {
      return $sce.trustAsResourceUrl(src);
    }

    /* Methods for legal-document */

    $scope.docSignStep2=function(){
      var company_data = JSON.parse($.cookie('company'));
      DoDocusign($.cookie('accessToken')).do({company:company_data.company_id/*<--could get from $routeParams.id instead?*/, amount:$rootScope.invest_amount}).$promise.then(
        function(value){
          $scope.legal_documents='views/partials/investor-flow-two.html';
          $scope.embed=trustSrc(value.embed_url);
        },
        function(error){
          console.log(error);
          $('#docusign_error').modal('show');
        }
      );
    };

    $scope.signIRSInit = function(){
      var company_data = JSON.parse($.cookie('company'));
      $scope.irs = {};
      $('.loader-overlay').fadeIn('fast');
      DoIRS($.cookie('accessToken')).do({'company':company_data.company_id/*<--could get from $routeParams.id instead?*/, 'progress_id':company_data.progress_id}).$promise.then(
        function(value){
          $scope.irs.embed = trustSrc(value.embed_url);
        },
        function(error){
          console.log(error);
          $('#docusign_error').modal('show');
        }
      );
    };

    $scope.gotoDocSignStep7 = function(){
      $scope.completeInvestment();
    };

    $scope.transfer_type={};
    $scope.completeInvestment = function(){
      $scope.investment_error={};
      var company_id = $routeParams.id;
      var cookie_data = JSON.parse($.cookie('company')); //Cookie data
      BBXEscrowFunding($.cookie('accessToken')).create({amount:cookie_data.invest_amount, id:cookie_data.funding_id, details:$scope.docusign.details, progress_id:cookie_data.progress_id }).$promise.then(
        function(value){
          var company = JSON.parse($.cookie('company'));
          var put_cookie_data = {
            'company_id':company.company_id,
            'funding_id':company.funding_id,
            'invest_amount':company.invest_amount,
            'company_name':company.company_name,
            'progress_id':company.progress_id,
            'investment_id':value.investment_id
          };
          $.cookie('company',JSON.stringify(put_cookie_data),{path:'/'});
          $location.path(URLS.INVESTOR_QUESTIONNAIRE+company_id);
        },
        function(error){
          console.log(error);
          $scope.investment_error.error=error.data.message;
        }
      );
    };

    /*Goto function */

    $scope.ourGoto=function(view_name){
      if(view_name === 'dashboard'){
        $location.path(Users.authorized().homeUrl());
      }
    };

    /* Required for docusign iframe */
    $scope.docusign_iframe=function(){
      var queryString=getQueryVariable('event');

      var successId=$routeParams.id;

      var parentQuery=window.parent.location.toString();
      var indexOfSlash=parentQuery.lastIndexOf('/');
      var companyId=parentQuery.substr(indexOfSlash+1,parentQuery.length);

      if(queryString==='signing_complete'){
        var _company = JSON.parse($.cookie('company'));
        var amount_to_send = _company.invest_amount;
        DocusignResult($.cookie('accessToken')).status({id:successId,status:'success', amount:amount_to_send}).$promise.then(
          function(value){
            var put_cookie_data = {
              'company_id':_company.company_id,
              'funding_id':_company.funding_id,
              'invest_amount':amount_to_send,
              'company_name':_company.company_name,
              'progress_id':value.progress_id
            };
            $.cookie('company',JSON.stringify(put_cookie_data),{path:'/'});
            window.parent.location.href='/#/bank-payment/'+companyId;
          },
          function(/*error*/){
            window.parent.location.href='/#/agreement/'+companyId;
          }
        );
      }
      else{
        $('#docusign_error').modal('show');
      }
    };

    $scope.ira_iframe = function(){
      var queryString=getQueryVariable('event');

      var successId=$routeParams.id;

      var parentQuery=window.parent.location.toString();
      var indexOfSlash=parentQuery.lastIndexOf('/');
      var companyId=parentQuery.substr(indexOfSlash+1,parentQuery.length);

      if(queryString==='signing_complete'){
        IRSResult($.cookie('accessToken')).status({id:successId,status:'success',progress_id:JSON.parse($.cookie('company')).progress_id}).$promise.then(
          function(/*value*/){
            window.parent.location.href='/#'+URLS.INVESTMENT_COMPLETE+companyId;
          },
          function(/*error*/){
            window.parent.location.href='/#/risk-factor-ack/'+companyId;
          }
        );
      }
      else{
        $('#docusign_error').modal('show');
      }
    };

    function getQueryVariable(variable){
      var query=window.location.toString();
      var indexOf=query.indexOf('?');
      var substr=query.substr(indexOf+1,query.length);
      var vars=substr.split('=');
      if(vars[0]==variable){
        return vars[1];
      }
      return false;
    }

    $scope.contactCitizenVC=function(){
      if($scope.contact_citizen.from == null){
        $scope.contact_citizen.error='Please enter a name.';
        return;
      }
      if($scope.contact_citizen.email == null){
        $scope.contact_citizen.error='Please enter an email id.';
        return;
      }
      if($scope.contact_citizen.message == null){
        $scope.contact_citizen.error='Please enter a message.';
        return;
      }
      ContactCitizen().send({message:$scope.contact_citizen.message, email:$scope.contact_citizen.email, name:$scope.contact_citizen.from}).$promise.then(
        function(/*value*/){
          FlashMessage.showFlash('positive',MSG.MSG_SENT);
          $scope.contact_citizen={};
          $scope.send_message=true;
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    $scope.recommend=function(slug){
      RecommendCompany($.cookie('accessToken')).recommend({id:slug}).$promise.then(
        function(value){
          if(value.is_published){
            $scope.company.is_published=true;
            FlashMessage.showFlash('positive',MSG.COMPANY_RECOMMEND);
          }
          else{
            $scope.company.is_published=false;
            FlashMessage.showFlash('positive',MSG.COMPANY_DERECOMMENDED);
          }
        },
        function(error){
          if(error.data.message==='Company cannot be published'){
            FlashMessage.showFlash('error',MSG.COMPANY_CANT_RECOMMEND);
          }
          else{
            FlashMessage.showFlash('error',MSG.FAILURE);
          }
        }
      );
    };

    $scope.sendPPMTemplateId=function(template_id){
      PPMTemplateId($.cookie('accessToken')).upload({company:$scope.company_id, ppm:template_id}).$promise.then(
        function(/*value*/){},
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    function setCompanyData(value){
      $scope.company = value.company;
      $scope.company.description = $sce.trustAsHtml($scope.company.description);
      $rootScope.company = value.company;
      $rootScope.company_id = value.company.id;
      $rootScope.company.id = id;
      $scope.funding = (value.funding_details ? value.funding_details : null);
      if($scope.funding != null){
        if($scope.funding.status === 'open'){
          $scope.company.state = 'OPEN';
        }
        else if($scope.funding.status === 'modify pending'){
          $scope.company.msg = value.card_message;
          $scope.company.state = 'ON HOLD';
        }
        else if($scope.funding.status === 'open pending'){
          $scope.company.msg = value.card_message;
          $scope.company.state = 'ESCROW PENDING';
        }
        else{
          $scope.company.msg = value.card_message;
          $scope.company.state = 'CLOSED';
        }
      }
      else{
        $scope.company.msg = value.card_message;
        $scope.company.state = 'COMING SOON';
      }
      var todayDate = new Date();
      var anotherToday = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate());
      if($scope.funding!=null){
        $scope.funding.end_date_crossed = anotherToday > new Date($scope.funding.end_date) ;
        $scope.so_far_raised=(value.funding_details.raised_amount ? value.funding_details.raised_amount:null);
      }

      /*
      if(parseFloat($scope.so_far_raised) >= parseFloat($scope.funding.seeking_amount)){
        if(value.progress_data && value.progress_data.state!=='ppm')
          $scope.funding.goal_reached = false;
        else
          $scope.funding.goal_reached = true;
      }
      else
        $scope.funding.goal_reached = false;
      */

      if(value.investment != null){
        $scope.your_investment = value.investment;
      }
      $scope.progress_data = value.progress_data;
      $scope.companyHighlights = value.highlights;
      $rootScope.documents = value.documents;
      $scope.companyLeaders = value.social_links;
      $scope.invest_options = value.recommended_amounts;
      $scope.apply_button = value.apply_button;

      if(value.funding_details != null){
        var investmentProgress={dataPoint:[]};
        var deno = parseFloat($scope.funding.raised_amount) + parseFloat($scope.funding.seeking_amount);

        var a = parseFloat($scope.funding.seeking_amount)/deno;
        var b = 0;
        var c = 0;
        if($scope.your_investment){
          b = (parseFloat($scope.funding.raised_amount)-parseFloat($scope.your_investment))/deno;
          c = parseFloat($scope.your_investment) / deno;
        } else{
          b = parseFloat($scope.funding.raised_amount)/deno;
        }

        if(parseFloat($scope.funding.raised_amount) >= parseFloat($scope.funding.seeking_amount)){
          a=0;
        }

        investmentProgress.dataPoint.push(a);
        investmentProgress.dataPoint.push(b);
        investmentProgress.dataPoint.push(c);

        $scope.dataset=investmentProgress;
      }

      var video_link = _.find(value.documents, function(item){ return (item.document_type==='company_video_link'); });
      if (video_link){
        $scope.video_link = $sce.trustAsResourceUrl(video_link.link);
      }
      var pitch_file = _.find(value.documents, function(item){ return (item.document_type==='company_pitch_file'); });
      if(pitch_file){
        $scope.pdfUrl = pitch_file.file;
      }

      $scope.$parent.invest_amount=null;

      $scope.showInvestment=true;
    }

    function setSnapshotData(value){
      $scope.company = value.company;
      $rootScope.company = value.company;
      $rootScope.company_id = value.company.id;
      $rootScope.company.id = id;
      $scope.funding = (value.funding_details ? value.funding_details : null);
      $scope.so_far_raised=(value.so_far_raised ? value.so_far_raised:null);
      $scope.your_investment=(value.investment ? value.investment:null);
      $scope.companyHighlights = value.highlights;
      $rootScope.documents = value.documents;
      $scope.companyLeaders = value.social_links;
      $scope.progress_data = value.progress_data;
      $scope.so_far_raised=(value.funding_details.raised_amount ? value.funding_details.raised_amount:null);
      $scope.apply_button = value.apply_button;
      if(value.investment != null){
        $scope.your_investment = value.investment;
      }
      if($scope.funding != null){
        if($scope.funding.status === 'open'){
          $scope.company.state = 'OPEN';
        }
        else if($scope.funding.status === 'modify pending'){
          $scope.company.msg = value.card_message;
          $scope.company.state = 'ON HOLD';
        }
        else if($scope.funding.status === 'open pending'){
          $scope.company.msg = value.card_message;
          $scope.company.state = 'ESCROW PENDING';
        }
        else{
          $scope.company.msg = value.card_message;
          $scope.company.state = 'CLOSED';
        }
      }
      else{
        $scope.company.msg = value.card_message;
        $scope.company.state = 'COMING SOON';
      }
      var video_link = _.find(value.documents, function(item){ return (item.document_type==='company_video_link'); });
      if (video_link){
        $scope.video_link = $sce.trustAsResourceUrl(video_link.link);
      }
      var pitch_file = _.find(value.documents, function(item){ return (item.document_type==='company_pitch_file'); });
      if(pitch_file) {
        $scope.pdfUrl = pitch_file.file;
      }
      if(value.funding_details != null){
        var dataset={dataPoint:[]};
        var deno = parseFloat($scope.funding.raised_amount) + parseFloat($scope.funding.seeking_amount);

        var a = parseFloat($scope.funding.seeking_amount)/deno;
        var b = 0;
        var c = 0;
        if($scope.your_investment){
          b = (parseFloat($scope.funding.raised_amount)-parseFloat($scope.your_investment))/deno;
          c = parseFloat($scope.your_investment) / deno;
        }
        else{
          b = parseFloat($scope.funding.raised_amount)/deno;
        }

        if(parseFloat($scope.funding.raised_amount) === parseFloat($scope.funding.seeking_amount)){
          a=0;
        }

        dataset.dataPoint.push(a);
        dataset.dataPoint.push(b);
        dataset.dataPoint.push(c);

        $scope.dataset=dataset;
      }

      $cookieStore.remove('company');

      $scope.showInvestment=false;
    }

    $scope.seeAllLeaders = function(){
      $rootScope.back_template_stack='company';
      $scope.company_template = 'views/founder-list.html';
    };

    $scope.returnFromPdfView = function(){
      $rootScope.back_template_stack='';
      $scope.company_template = 'views/founder-dashboard.html';
      $scope.funding_template = 'views/partials/fundraising.html';
    };

    $scope.docusignTryAgain = function(){
      $('#docusign_error').modal('hide');
      $timeout(function(){
        var id=$routeParams.id;
        $location.path(URLS.COMPANY+id);
      }, 500);
    };

    $scope.seePitchDeck = function(){
      var pitch_file = _.find($scope.documents, function(item){ return (item.document_type==='company_pitch_file'); });
      if (!pitch_file) { return; }

      $rootScope.back_template_stack = 'company';
      $('.loader-overlay-2').fadeIn('fast');
      $scope.pdfUrl = pitch_file.file;
      $scope.company_template = 'components/pdf-viewer/pdf-viewer.html';
      invokePdfWorker();
    };

    $scope.getNavStyle = function(scroll) {
      return scroll > 100? 'pdf-controls fixed': 'pdf-controls';
    };

    $scope.getWireDetails = function(){
      InvestorWireDetails($.cookie('accessToken')).wire().$promise.then(
        function(value){
          $scope.transfer_type.wire_details = $sce.trustAsHtml(value.details);
          $scope.transfer_type.help_text = $sce.trustAsHtml(value.help_text);
        },
        function(/*error*/){}
      );
    };

    $('#pdf-canvas').on('load', function(/*event*/){
    });

    $scope.viewDoc = function(doc,print){
      $rootScope.back_template_stack = 'company';
      $scope.showPrintButton = print;
      $scope.pdfUrl = doc.file;
      $('.loader-overlay-2').fadeIn('fast');
      $scope.company_template = 'components/pdf-viewer/pdf-viewer.html';
      invokePdfWorker();
    };

    $scope.initInvestQuest = function(){
      $scope.ques={};
      $scope.ques.ent_accept = false;
      $scope.ques.ind_ques = [false,false];
      $scope.ques.ent_ques = [null,null,null,null,null,null,null];
      $scope.ques.ques8 = [false,false,false,false,false,false,false,false,false,false,false,false];
      $scope.ques.professional_advisor_capacity = false;
      $scope.ques.economic_risk = false;
      $scope.ques.does_not_exceed = false;
      $scope.ques.mutual_funds=null;
      $scope.ques.stocks = null;
      $scope.ques.bonds = null;
      $scope.ques.variable_annuities = null;
      $scope.ques.venture_capital = null;
      $scope.ques.unregistered_securities = null;
      $scope.ques.lp_llc = null;
      $scope.ques.options = null;
      $scope.ques.liqidity_need_yrs = '0';
      var company = JSON.parse($.cookie('company'));
      $scope.ques.progress_id = company.progress_id;
      $scope.ques.investment_id = company.investment_id;

      var user = $cookieStore.get('user');
      $scope.ques.investor_type = user.investor_type;
      $scope.ques.show_ind = ($scope.ques.investor_type === 'individual');

      DocSignStep3($.cookie('accessToken')).fetch().$promise.then(
        function(value){
          $scope.ques.name = value.investor_name;
          $scope.ques.jurisdiction = {
            isAmerican: value.is_american_jurisdiction,
            usState: value.is_american_jurisdiction? value.jurisdiction: null,
            internationalLocation: value.is_american_jurisdiction? null: value.jurisdiction
          };
          $scope.ques.auth_addr = value.address;
          var temp_phone_no = value.investor_phone_number;
          var temp_phone_array = temp_phone_no.split('ext');
          $scope.ques.phone_number = temp_phone_array[0];
          $scope.ques.phone_number_ext = temp_phone_array[1];
        },
        function(/*error*/){}
      );
      $scope.ques.auth_name = user.profile.first_name + ' ' + user.profile.middle_name + ' ' + user.profile.last_name;
      $scope.ques.auth_email = user.profile.email;
      var temp_phone_no = user.profile.phone_number;
      var temp_array = temp_phone_no.split('ext');
      $scope.ques.auth_phone_number = temp_array[0];
      $scope.ques.auth_phone_number_ext = temp_array[1];
    };

    $scope.submitInvestQuestions = function(){
      var data = {
        'investor_type':$scope.ques.investor_type,
        'progress_id':$scope.ques.progress_id,
        'investment':$scope.ques.investment_id,
        user_data:{
          'name':$scope.ques.name,
          'address':$scope.ques.addr,
          'address2':$scope.ques.addr2,
          'city':$scope.ques.city,
          'state':$scope.ques.state,
          'zipcode':$scope.ques.zipcode,
          'country':$scope.ques.country,
          'email':$scope.ques.email,
          'telephone':$scope.ques.phone_number + 'ext' + $scope.ques.phone_number_ext,
          'investment':$scope.ques.investment_id
        },
        questionnaire:{
          'investment':$scope.ques.investment_id
        },
        suitability_questionnaire:{
          'investment':$scope.ques.investment_id,
          'mutual_funds':$scope.ques.mutual_funds,
          'stocks':$scope.ques.stocks,
          'bonds':$scope.ques.bonds,
          'variable_annuities':$scope.ques.variable_annuities,
          'venture_capital':$scope.ques.venture_capital,
          'unregistered_securities':$scope.ques.unregistered_securities,
          'lp_llc':$scope.ques.lp_llc,
          'options':$scope.ques.options
        }
      };
      if($scope.ques.show_ind){
        data.questionnaire.q1 = $scope.ques.ind_ques[0];
        data.questionnaire.q2 = $scope.ques.ind_ques[1];
        data.suitability_questionnaire.occupation = $scope.ques.occupation;
        data.suitability_questionnaire.present_employer = $scope.ques.present_employer;
        data.suitability_questionnaire.number_of_years = $scope.ques.no_of_years;
        data.suitability_questionnaire.position = $scope.ques.title;
        data.suitability_questionnaire.business_address = $scope.ques.business_addr;
        data.suitability_questionnaire.business_address2 = $scope.ques.business_addr2;
        data.suitability_questionnaire.business_city = $scope.ques.business_city;
        data.suitability_questionnaire.business_state = $scope.ques.business_state;
        data.suitability_questionnaire.business_zipcode = $scope.ques.business_zipcode;
        data.suitability_questionnaire.business_country = $scope.ques.business_country;
        data.suitability_questionnaire.business_telephone = $scope.ques.business_telephone;
        data.suitability_questionnaire.educational_background = $scope.ques.edu_back;
        data.suitability_questionnaire.nature_and_extent = $scope.ques.nature_extent;
        data.suitability_questionnaire.professional_advisor_capacity = $scope.ques.professional_advisor_capacity;
        data.suitability_questionnaire.economic_risk = $scope.ques.economic_risk;
        data.suitability_questionnaire.does_not_exceed = $scope.ques.does_not_exceed;
      }
      else{
        var jurisdiction = $scope.ques.jurisdiction;
        data.user_data.jurisdiction = jurisdiction.isAmerican? jurisdiction.usState: jurisdiction.internationalLocation;
        data.user_data.auth_name= $scope.ques.auth_name;
        data.user_data.auth_address= $scope.ques.auth_addr;
        data.user_data.auth_address2 = $scope.ques.auth_addr2;
        data.user_data.auth_city = $scope.ques.auth_city;
        data.user_data.auth_state = $scope.ques.auth_state;
        data.user_data.auth_zipcode = $scope.ques.auth_zipcode;
        data.user_data.auth_country = $scope.ques.auth_country;
        data.user_data.auth_email = $scope.ques.auth_email;
        data.user_data.auth_telephone = $scope.ques.auth_phone_number;
        data.questionnaire.q1 = $scope.ques.ent_ques[0];
        data.questionnaire.q2 = $scope.ques.ent_ques[1];
        data.questionnaire.q3 = $scope.ques.ent_ques[2];
        data.questionnaire.q4 = $scope.ques.ent_ques[3];
        data.questionnaire.q5 = $scope.ques.ent_ques[4];
        data.questionnaire.q6 = $scope.ques.ent_ques[5];
        data.questionnaire.q7 = $scope.ques.ent_ques[6];
      }
      if(parseInt($scope.ques.liquidity_needs) == 0){
        data.suitability_questionnaire.liquidity_needs = 'None';
      }
      else if(parseInt($scope.ques.liquidity_needs) == 1){
        data.suitability_questionnaire.liquidity_needs = 'Immediate Need';
      }
      else if(parseInt($scope.ques.liquidity_needs) == 2){
        data.suitability_questionnaire.liquidity_needs = 'Needed in '+$scope.ques.liqidity_need_yrs+' Yrs.';
      }

      InvestQuestions($.cookie('accessToken')).update(data).$promise.then(
        function(/*value*/){
          var company_id = $routeParams.id;
          $location.path(URLS.RISK_FACTOR_ACK+company_id);
        },
        function(error){
          $scope.ques.error = error.data.message;
        }
      );
    };

    $scope.completeUnfinishedInvestment = function(){
      var redirection_path = $scope.statesMap[$scope.progress_data.state];
      var company_slug = $routeParams.id;
      if($scope.progress_data.state === 'ppm'){
        var a_date = new Date();
        var b_date = new Date(a_date.getFullYear(), a_date.getMonth(), a_date.getDay());

        if(parseFloat($scope.funding.raised_amount) >= parseFloat($scope.funding.seeking_amount)){
          $scope.company.modal_msg = INVEST_MSGS.COMPANY_POPUP_GOAL_MET;
          $('#completeUnfinishedModal').modal('show');
          return;
        }
        else if($scope.company.state === 'CLOSED'){
          $scope.company.modal_msg =  INVEST_MSGS.COMPANY_POPUP_ROUND_CLOSE;
          $('#completeUnfinishedModal').modal('show');
          return;
        }
        else if($scope.funding.status === 'modify pending'){
          $scope.company.modal_msg =  INVEST_MSGS.COMPANY_POPUP_MODIFY_PENDING;
          $('#completeUnfinishedModal').modal('show');
          return;
        }
        else if(b_date > new Date($scope.funding.end_date)){
          $scope.company.modal_msg = INVEST_MSGS.COMPANY_POPUP_END_DATE_REACHED;
          $('#completeUnfinishedModal').modal('show');
          return;
        }
      }
      var cookie_data = {
        'company_id':$scope.funding.company,
        'funding_id':$scope.funding.id,
        'invest_amount':$scope.progress_data.amount,
        'company_name':$scope.company.name
      };
      switch($scope.progress_data.state){
        case 'started':
          break;
        case 'ppm':
          angular.extend(cookie_data, {
            'progress_id':$scope.progress_data.id
          });
          break;
        case 'payment':
        case 'questionnaire':
        case 'ra':
          angular.extend(cookie_data, {
            'progress_id':$scope.progress_data.id,
            'investment_id':$scope.progress_data.investment
          });
          break;
      }
      $.cookie('company',JSON.stringify(cookie_data),{path:'/'});
      $location.path(redirection_path+company_slug);
    };

    $scope.rfaSubmit = function(){
      var company_id = $routeParams.id;
      var company_data = JSON.parse($.cookie('company'));
      RFAQuestions($.cookie('accessToken')).update({'q1':$scope.rsa.q1, 'q2':$scope.rsa.q2, 'q3':$scope.rsa.q3, 'q4':$scope.rsa.q4, 'q5':$scope.rsa.q5, 'q6':$scope.rsa.q6,'investment':company_data.investment_id, 'progress_id':company_data.progress_id }).$promise.then(
        function(/*value*/){
          $location.path(URLS.IRA +company_id);
        },
        function(/*error*/){}
      );
    };

    $scope.$watchCollection('[ques.ent_ques[0], ques.ent_ques[2]]', function(/*value*/){
      if($scope.ques.ent_ques[0]==false && $scope.ques.ent_ques[2]==false){
        $scope.ques.show_other_ent_ques = false;
      }
      else{
        $scope.ques.show_other_ent_ques = true;
      }
      if($scope.ques.ent_ques[2]){
        $scope.ques.accept_ques_chkbox_show = true;
      }
      else{
        $scope.ques.accept_ques_chkbox_show = false;
      }
    });

    $scope.seeDocsOnFund = function(doc_path){
      if(doc_path){
        $scope.pdfUrl = doc_path;
      }

      $('.loader-overlay-2').fadeIn('fast');

      $scope.funding_template = 'components/pdf-viewer/pdf-viewer.html';
      invokePdfWorker();
    };

    function invokePdfWorker(){
      $.getScript( 'bower_components/pdfjs-dist/build/pdf.worker.js' )
      .done(function( script, textStatus, data ) {
        $('.loader-overlay').fadeOut('fast');
        if(data.status == 200 || data.status == 201){
          $('.loader-overlay-2').fadeOut('fast');
        }
      })
      .fail(function(/*jqxhr, settings, exception*/) {
        $('.loader-overlay-2').fadeOut('fast');
        $('.loader-overlay').fadeOut('fast');
      });
    }

    $scope.investment_complete = function(){
      var invest_details = JSON.parse($.cookie('company'));
      $scope.invest_amount = invest_details.invest_amount;
      $scope.company_name = invest_details.company_name;
      $scope.wire_transfer_msg = INVEST_MSGS.INVEST_FINISH_WIRE_MSG;
    };
  }]
);

'use strict';
// jshint eqeqeq:false
// jshint eqnull:true

/* Controllers */

angular.module('myApp.controllers').controller(
  'RegisterCompanyCtrl',
  ["$scope", "$rootScope", "$upload", "$sce", "RegisterCompany", "Auth", "API_PATH", "$location", "EditCompany", "$cookieStore", "$routeParams", "DeleteHighlight", "URLS", "DeleteCompanyDoc", "DeleteCompanyLeader", "RegisterCompanyII", "EmbedLink", "ApproveStep2Company", "RejectStep2Company", "ChangePassword", "MSG", "FlashMessage", "FlashOnNextRoute", "AllDocuments", "BASE_PATH", "ChangeEmail", "InvestorHistory", "ChangePhone", "TimeoutMessage", "$timeout", function(
    $scope,
    $rootScope,
    $upload,
    $sce,
    RegisterCompany,
    Auth,
    API_PATH,
    $location,
    EditCompany,
    $cookieStore,
    $routeParams,
    DeleteHighlight,
    URLS,
    DeleteCompanyDoc,
    DeleteCompanyLeader,
    RegisterCompanyII,
    EmbedLink,
    ApproveStep2Company,
    RejectStep2Company,
    ChangePassword,
    MSG,
    FlashMessage,
    FlashOnNextRoute,
    AllDocuments,
    BASE_PATH,
    ChangeEmail,
    InvestorHistory,
    ChangePhone,
    TimeoutMessage,
    $timeout
  ) {

    // For loading Company reg step 2 form
    $scope.document={};
    $scope.docs = [];
    $scope.documents = [];
    $scope.company={is_ver_required:true};
    $scope.puser={};
    $scope.isAdmin = false;
    $scope.myProfile = {}; // used on the My Profile page
    $scope.payment={};
    $scope.uploadDocsNo={
      company_pitch_file:0,
      company_video_file:0,
      company_logo:0
    };

    $scope.errors={
      company_name:{value:''},
      company_website:{value:''},
      first_name:{value:''},
      last_name:{value:''},
      email:{value:''},
      role:{value:''},
      series_seeking:{value:''},
      amount_seeking:{value:''},
      funding_raised:{value:''},
      file_title:{value:''},
      file_desc:{value:''}
    };

    $scope.step2errors={
      company_name:{value:''},
      website:{value:''},
      headline:{value:''},
      description:{value:''},
      highlight:{value:''},
      leader:{value:''}
    };

    $scope.path='';
    $scope.highlight={};
    $scope.highlight.file=null;

    $scope.user_roles={};

    var newDate = new Date();
    $scope.company_signup = {recent_funding_event: newDate, fundraising_start_date: newDate, fundraising_end_date: newDate};
    $scope.company_signup.series_seeking_fund='Seed';
    $scope.companyLeaders = [];

    $scope.change_password={};
    $scope.changePasswordError='';

    $scope.founder_step2_error={
      logo:'',
      highlight:'',
      pitch_deck:'',
      company_leader:'',
    };

    $scope.image = null;

    $scope.a={};
    $scope.a.international_company=false;

    $scope.showForm = true;

    $scope.logout = function(){
      FlashOnNextRoute.setMessage({type: 'neutral', text: MSG.LOGOUT});
      Auth.logout();
    };

    $scope.video_embed_link='';
    $scope.company_pitch_file={};
    $scope.company_video_file={};
    $scope.company_pitch_link='';
    $scope.company_video_link='';

    $scope.initEntRegistration = function(){
      $scope.company.is_ver_required = false;
    };

    $scope.checkChange=function(val){
      if($scope.errors[val].value.length>0) {
        $scope.errors[val].value='';
      }
    };

    // Bindings for Bootstrap Date Picker

    $scope.minDate = ( $scope.minDate ) ? null : new Date();
    $scope.minDate1 = null;
    $scope.maxDate = new Date();

    $scope.open4 = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened4 = true;
    };
    $scope.dateOptions = {
      'year-format': '\'yy\'',
      'starting-day': 1
    };
    $scope.format = 'yyyy-MM-dd';

    $scope.registration_form={};

    // Form submission
    $scope.submitForm = function(){
      if($scope.registration_form.$invalid)
      {
        FlashMessage.showFlash('error',MSG.INVALID_FORM);
        $('.loader-overlay').fadeOut('fast');
        return;
      }
      else{
        // call api to submit form with/without validations
        addNewCompany();
      }
    };

    function _date2str(date) {
      return date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
    }

    // Add a new company
    function addNewCompany(){

      if($scope.puser.is_ver_required == null) { $scope.puser.is_ver_required=false; }

      var recent_date = _date2str($scope.company_signup.recent_funding_event);
      var start_date = _date2str($scope.company_signup.fundraising_start_date);
      var end_date = _date2str($scope.company_signup.fundraising_end_date);

      var phone_number_ = $scope.puser.phone_number + 'ext';
      if($scope.puser.phone_number_ext != undefined){
        phone_number_ = phone_number_ + $scope.puser.phone_number_ext;
      }

      var company = $scope.company;
      company.website = checkHTTP($scope.company.website);

      RegisterCompany($.cookie('accessToken')).create({'user':$scope.puser, 'company': company, 'company_signup': {'fundraising_end_date': end_date, 'fundraising_start_date': start_date, 'recent_funding_event': recent_date, 'relevant_links': [], 'series_seeking_fund': $scope.company_signup.series_seeking_fund}, 'documents': $scope.docs, 'user_roles': $scope.user_roles, 'url': URLS.REVIEW_COMPANY_INFO+'%s', phone_number:phone_number_}).$promise.then(

        function(/*value*/){
          FlashMessage.showFlash('positive',MSG.SUCCESSFULL_INVITE);
          $location.path(URLS.ADMIN_DASHBOARD);
        },
        function(error){
          console.log(error);
          FlashMessage.showFlash('error',MSG.FAILURE);
          if(_.has(error.data,'company')){
            if(_.has(error.data.company[0],'name')){
              $scope.errors.company_name.value=error.data.company[0].name[0];
            }
            if(_.has(error.data.company[0],'website')){
              $scope.errors.company_website.value=error.data.company[0].website[0];
            }

          }
          if(_.has(error.data,'company_signup')){
            if(_.has(error.data.company_signup[0],'series_seeking_fund')){
              $scope.errors.series_seeking.value=error.data.company_signup[0].series_seeking_fund[0];
            }
            if(_.has(error.data.company_signup[0],'raised_amount')){
              $scope.errors.funding_raised.value=error.data.company_signup[0].raised_amount[0];
            }
            if(_.has(error.data.company_signup[0],'seeking_amount')){
              $scope.errors.amount_seeking.value=error.data.company_signup[0].seeking_amount[0];
            }
          }
          if(_.has(error.data,'user')){
            if(_.has(error.data.user[0],'first_name')){
              $scope.errors.first_name.value=error.data.user[0].first_name[0];
            }
            if(_.has(error.data.user[0],'last_name')){
              $scope.errors.last_name.value=error.data.user[0].last_name[0];
            }
            if(_.has(error.data.user[0],'email')){
              $scope.errors.email.value=error.data.user[0].email[0];
            }
          }
        }
      );
    }

    // Initiate company registration step 2

    $scope.highlight = {date: newDate, description:''};
    $scope.companyHighlights = [];
    $scope.companyLeaders = [];

    $scope.initEditCompany = function(){
      $scope.template = 'views/partials/new-reg-founder.html';
      $scope.founder_dashboard_left_card = 'views/founderCard1.html';
      var id = $routeParams.id;
      var path=$location.path();
      $scope.path=path.substring(1, path.lastIndexOf('/'));
      $scope.submitButtonText = 'Submit for approval';
      if ($cookieStore.get('user').role === 1){
        $scope.submitButtonText = 'Apply changes';
      }
      $scope.showReadonly = true;
      if($scope.path==='ent-registration-step2'){
        $scope.admin_view = false;
        $scope.showReadonly = false;
      }
      else if($scope.path==='review-complete-company-info') {
        $scope.admin_view=true;
      }

      if($scope.company){
        EditCompany($.cookie('accessToken')).get({id: id}).$promise.then(
          function(value){
            if($scope.path==='review-complete-company-info' && !(value.status === 'step2_created' || value.status ==='edited')){
              FlashOnNextRoute.setMessage({type: 'neutral-d', text: MSG.ALREADY_MODERATED});
              $location.path(URLS.ADMIN_DASHBOARD);
            } else {
              $scope.company = value.company;
              $scope.company.id = value.company.id;
              $scope.companyHighlights = value.highlights;
              $scope.documents = value.documents;
              $scope.companyLeaders = value.social_links;
              $scope.payment = value.company_member_BBX;

              if($scope.payment.dob!=null || $scope.payment.dob!='') { $scope.payment.dob=new Date($scope.payment.dob); }
              $scope.is_ver_required = value.is_ver_required;
              $scope.a.international_company = value.is_international;
              var video_link = _.find(value.documents, function(item){ return (item.document_type==='company_video_link'); });
              if (video_link){
                $scope.video_link = $sce.trustAsResourceUrl(video_link.link);
              }
              _.each(value.documents, function(item){
                if(item.document_type==='company_pitch_file'){
                  $scope.uploadDocsNo.company_pitch_file++;
                  $scope.company_pitch_file=item;
                }
                else if(item.document_type==='company_video_file'){
                  $scope.uploadDocsNo.company_video_file++;
                  $scope.company_video_file=item;
                }
                else if(item.document_type==='company_logo'){
                  $scope.uploadDocsNo.company_logo++;
                }
                else if(item.document_type==='company_video_link'){
                  $scope.uploadDocsNo.company_video_file++;
                  $scope.company_video_link=item;
                }
                else if(item.document_type==='company_pitch_link'){
                  //$scope.uploadDocsNo.company_pitch_file++;
                  $scope.company_pitch_link=item;
                }
              });
            }
          },
          function(error){
            FlashMessage.showFlash('error',MSG.FAILURE);
            console.log(error);
          }
        );
      }
    };

    $scope.addHighlightImage = function(files){
      $scope.step2errors.highlight.value='';

      if(files.length == 0){
        $scope.step2errors.highlight.value='Please upload a valid jpg, jpeg or png image of  size 625*375.';
        return;
      }

      /*
      if(!validImage(files[0].type)){
        scope.step2errors.highlight.value='Please upload a valid jpg, jpeg or png image.';
        return;
      }
      */
      $scope.highlight.file = files[0];
    };

    /*
    function validImage(type){
      var type_jpeg= 'image/jpeg';
      var type_png = 'image/png';
      var type_jpg= 'image/jpg';
      return type === type_jpeg || type === type_png || type === type_jpg;
    }
    */

    $scope.onHighlightSelect = function(company_id) {
      if($scope.highlight.file==null) {
        $scope.step2errors.highlight.value='Please upload an image for your Company Highlight.';
        return;
      }
      if($scope.highlight.description==''){
        $scope.step2errors.highlight.value='Please enter a description for your Company Highlight.';
        return;
      }
      var file = $scope.highlight.file;

      if($scope.highlight.date == null){
        $scope.step2errors.highlight.value='Please select/enter date in correct format(MM-DD-YYYY)';
        return;
      }

      var date;
      if(!$scope.highlight.date.getFullYear()){
        date = _date2str(new Date($scope.highlight.date)); // WTF?
      }
      else{
        date = _date2str($scope.highlight.date);
      }

      $scope.upload = $upload.upload({
        url: BASE_PATH.API()+API_PATH.UPLOAD_HIGHLIGHT,
        data: {date: date, description: $scope.highlight.description, company: company_id, url: URLS.REVIEW_COMPANY_INFO_II+'%s'},
        headers: {'Authorization': 'Bearer '+ $.cookie('accessToken')},
        file: file,
      }).progress(function(/*event*/) {
        $('.loader-overlay-2').fadeIn('fast');
      }).success(function(data, status) {
        if(status == 201 || status == 200){
          $('.loader-overlay-2').fadeOut('fast');
        }
        // file is uploaded successfully
        $scope.companyHighlights.unshift(data);
        // $scope.docs.push({'key': data.key});
        $scope.highlight.file = null;
        $scope.highlight.tempFile = null;
        $scope.highlight.description = null;
        $scope.highlight.date = newDate;
        $scope.step2errors.highlight.value='';
        $scope.founder_step2_error.highlight = '';
        clearHighlightPreviewImage();
        $('#addingHighlight').modal('hide');
      }).error(function(error){
        $('.loader-overlay-2').fadeOut('fast');
        console.log(error);
        $scope.founder_step2_error.highlight = error.data.message;
        if(error.description){
          $scope.step2errors.highlight.value=error.description[0];
        }
        if(error.file){
          $scope.step2errors.highlight.value=error.file[0];
        }
        if(error.data){
          $scope.step2errors.highlight.value=error.data.message;
        }
        clearHighlightPreviewImage();
      });
    };

    $scope.returnToForm = function(){
      $scope.template = 'views/partials/new-reg-founder.html';
      $scope.showForm = true;
    };

    $scope.deleteLeader = function(leader){
      DeleteCompanyLeader($.cookie('accessToken')).delete({id: leader.id, url: URLS.REVIEW_COMPANY_INFO_II+'%s'}).$promise.then(
        function(/*value*/){
          _.each($scope.companyLeaders, function(obj,index){
            if (obj.id == leader.id){
              $scope.companyLeaders.splice(index,1);
            }
          });
        },
        function(error){
          console.log(error);
        });
    };

    $scope.removeHighlight = function(highlight){
      DeleteHighlight($.cookie('accessToken')).delete({id: highlight.id, url: URLS.REVIEW_COMPANY_INFO_II+'%s'}).$promise.then(
        function(/*value*/){
          _.each($scope.companyHighlights, function(obj,index){
            if (obj.id == highlight.id){
              $scope.companyHighlights.splice(index,1);
            }
          });
        },
        function(error){
          console.log(error);
        });
    };

    $scope.addTheLink=function(company_id,type,link){
      $scope.embed_error={};

      link=checkHTTP(link);

      EmbedLink($.cookie('accessToken')).add({'link':link, 'company':company_id, 'document_type':type, url: URLS.REVIEW_COMPANY_INFO_II+'%s'}).$promise.then(
        function(value){
          if(type==='company_pitch_link'){
            $scope.uploadDocsNo.company_pitch_file++;
          }
          else if(type==='company_video_link'){
            $scope.uploadDocsNo.company_video_file++;
            $scope.company_video_link = value;
            $scope.video_link = $sce.trustAsResourceUrl(value.link);
            $scope.video_embed_link = '';
          }
        },
        function(error){
          console.log(error);
          if(error.data.message){
            $scope.embed_error.error = error.data.message;
          }
          else if(error.data.link[0]){
            $scope.embed_error.error = error.data.link[0];
          }
        }
      );
    };

    $scope.uploadOtherDocuments = function(company_id,title,doc_type){
      var urlPath = BASE_PATH.API()+API_PATH.ADD_COMPANY_DOCUMENT;
      var linkUrl = URLS.REVIEW_COMPANY_INFO_II+'%s';
      var file = $scope.otherDocsToUpload;

      if(file.type !== 'application/pdf'){
        $scope.founder_step2_error.pitch_deck = 'Please upload only PDF files';
      }

      var docTitle = (typeof title === 'undefined') ? null : title;
      if (doc_type === null) {
        urlPath = BASE_PATH.API()+API_PATH.ADD_COMPANY_LOGO;
        linkUrl = null;
      }
      $scope.upload = $upload.upload({
        url: urlPath,
        data: {document_type: doc_type, company: company_id, title: docTitle, url: linkUrl},
        headers: {'Authorization': 'Bearer '+ $.cookie('accessToken')},
        file: file,
      }).progress(function(/*event*/) {
        $('.loader-overlay-2').fadeIn('fast');
        $('.loader-overlay').fadeOut('fast');
      }).success(function(data, status) {
        if(status == 201){
          $('.loader-overlay-2').fadeOut('fast');
        }
        $scope.documents.push(data);
        $scope.otherDocsToUpload = null;
        $scope.a.otherDocName = '';
      }).error(function(data){
        $('.loader-overlay-2').fadeOut('fast');
        console.log(data);
      });
    };

    $scope.selectOtherDocument = function(files){
      $scope.otherDocsToUpload = files[0];
    };

    $scope.uploadDocs = function(company_id, doc_type, files, title, desc){
      var urlPath = BASE_PATH.API()+API_PATH.ADD_COMPANY_DOCUMENT;
      var linkUrl = URLS.REVIEW_COMPANY_INFO_II+'%s';
      var file = files[0];

      if((doc_type === 'company_document_file' || doc_type === 'company_pitch_file')){
        $scope.founder_step2_error.pitch_deck = '';
        $scope.founder_step2_error.upload_docs = '';
        if(file.type !== 'application/pdf'){
          if(doc_type === 'company_pitch_file'){
            $scope.founder_step2_error.pitch_deck = 'Please upload only PDF files';
          }
          else if(doc_type === 'company_document_file'){
            $scope.founder_step2_error.upload_docs = 'Please upload only PDF files';
          }
          return;
        }
      }
      if(doc_type == null){
        if(files.length == 0){
          $scope.founder_step2_error.logo ='Please upload a valid JPG, JPEG or PNG file of size 260*90 only.';
          return;
        }
      }
      var docTitle = (typeof title === 'undefined') ? null : title;
      var docDesc = (typeof desc === 'undefined') ? null : desc;
      if (doc_type === null) {
        urlPath = BASE_PATH.API()+API_PATH.ADD_COMPANY_LOGO;
        linkUrl = null;
      }
      $scope.upload = $upload.upload({
        url: urlPath,
        data: {document_type: doc_type, company: company_id, title: docTitle, description: docDesc, url: linkUrl},
        headers: {'Authorization': 'Bearer '+ $.cookie('accessToken')},
        file: file,
      }).progress(function(/*event*/) {
        $('.loader-overlay-2').fadeIn('fast');
        $('.loader-overlay').fadeOut('fast');
      }).success(function(data, status) {
        if(status == 201){
          $('.loader-overlay-2').fadeOut('fast');
        }
        if(doc_type==='company_pitch_file') {
          $scope.uploadDocsNo.company_pitch_file++;
          $scope.company_pitch_file=data;
        }
        if(doc_type==='company_video_file'){
          $scope.uploadDocsNo.company_video_file++;
          $scope.company_video_file=data;
        }
        if(doc_type==='company_logo'){
          $scope.uploadDocsNo.company_logo++;
          $scope.founder_step2_error.logo ='';
        }
        $scope.documents.push(data);
        if(doc_type==='company_document_file') {
          $scope.newFile = null;
          $scope.document = null;
        }
        if (doc_type === null) {
          $scope.company.logo = data.logo;
          $scope.selectlogo='';
          $scope.founder_step2_error.logo ='';
        }
      }).error(function(data){
        $('.loader-overlay-2').fadeOut('fast');
        console.log(data);
      });
    };

    $scope.deleteDoc = function(doc){
      $('.loader-overlay').fadeIn('fast');
      DeleteCompanyDoc($.cookie('accessToken')).delete({id: doc.id, url: URLS.REVIEW_COMPANY_INFO_II+'%s'}).$promise.then(
        function(/*value*/){
          _.each($scope.documents, function(obj,index){
            if (obj == doc){
              $scope.documents.splice(index,1);
            }
          });
          if(doc.document_type==='company_logo'){
            $scope.uploadDocsNo.company_logo--;
            $scope.registration_form.companylogo.value='';
          }
          if(doc.document_type==='company_pitch_file') { $scope.uploadDocsNo.company_pitch_file--; }
          if(doc.document_type==='company_video_file') { $scope.uploadDocsNo.company_video_file--; }
          if(doc.document_type==='company_video_link'){
            $scope.uploadDocsNo.company_video_file--;
            $scope.company_video_link = null;
          }
        },
        function(error){
          console.log(error);
        });
    };

    $scope.saveCompanyStep2 = function(){
      var dob= $scope.payment.dob;
      if(dob!=null && dob!='' && dob!=undefined){
        dob = dob.getFullYear()+'-'+(dob.getMonth()+1)+'-'+dob.getDate();
      }

      $scope.company.website=checkHTTP($scope.company.website);
      $scope.company.facebook_link=checkHTTP($scope.company.facebook_link);
      $scope.company.twitter_link=checkHTTP($scope.company.twitter_link);
      $scope.company.linkedin_link=checkHTTP($scope.company.linkedin_link);

      $scope.company.name = checkNull($scope.company.name);
      $scope.company.headline = checkNull($scope.company.headline);
      $scope.company.description = checkNull($scope.company.description);

      for (var key in $scope.payment){
        if ($scope.payment.hasOwnProperty(key)) {
          $scope.payment[key] = checkNull($scope.payment[key]);
        }
      }

      var company_data = {
        save_company: true,
        id: $scope.company.id,
        name: $scope.company.name,
        headline: $scope.company.headline,
        website: $scope.company.website,
        description: $scope.company.description,
        about: $scope.company.about,
        bank_account_number: $scope.payment.bank_account_number,
        bank_account_type: $scope.payment.bank_account_type,
        bank_account_holder: $scope.payment.bank_account_holder,
        bank_account_routing: $scope.payment.bank_account_routing,
        url: URLS.REVIEW_COMPANY_INFO_II+'%s',
        facebook_link: $scope.company.facebook_link,
        twitter_link: $scope.company.twitter_link,
        linkedin_link: $scope.company.linkedin_link,
      };
      if($scope.a.international_company == false){
        angular.extend(company_data, {
          is_international: false,
          dob: dob,
          address: $scope.payment.address,
          ssn: $scope.payment.ssn,
          city: $scope.payment.city,
          state: $scope.payment.state,
          zip: $scope.payment.zip,
        });
      }
      else if($scope.a.international_company){
        angular.extend(company_data, {
          is_international: true,
          country: $scope.payment.country,
        });
      }
      else if($scope.a.international_company == null){
        angular.extend(company_data, {
          is_international: null,
        });
      }

      $('.loader-overlay-2').fadeIn('fast');
      $scope.upload = $upload.upload({
        url: BASE_PATH.API()+API_PATH.REGISTER_COMPANY_STEP_II+$scope.company.id,
        method: 'PUT',
        data: company_data,
        headers: {'Authorization': 'Bearer '+ $.cookie('accessToken')},
      }).progress(function(/*event*/) {
        $('.loader-overlay').fadeOut('fast');
      }).success(function(data, status) {
        if(status == 201 || status == 200){
          $('.loader-overlay-2').fadeOut('fast');
        }
        TimeoutMessage.showSuccess('#saveCompanyDataSuccess','Form saved');
      }).error(function(error){
        console.log(error);
        $('.loader-overlay-2').fadeOut('fast');
        if(error.description || error.headline || error.website){
          $scope.save_company_error = MSG.SAVE_COMPANY_FIELDS_REQUIRED_ERROR;
        }
        else{
          $scope.save_company_error = MSG.SAVE_COMPANY_ERROR;
        }
      });

      for (var key2 in $scope.payment){
        if ($scope.payment.hasOwnProperty(key2)) {
          $scope.payment[key2] = checkSpace($scope.payment[key2]);
        }
      }
    };

    $scope.submitStep2 = function(){
      $scope.company.website=checkHTTP($scope.company.website);
      $scope.company.facebook_link=checkHTTP($scope.company.facebook_link);
      $scope.company.twitter_link=checkHTTP($scope.company.twitter_link);
      $scope.company.linkedin_link=checkHTTP($scope.company.linkedin_link);
      $scope.founder_step2_error={
        logo:'',
        highlight:'',
        pitch_deck:'',
        company_leader:'',
      };
      if($scope.registration_form.$invalid || !($scope.uploadDocsNo.company_pitch_file>0 && $scope.companyHighlights.length>0 && $scope.company.logo!=null && $scope.companyLeaders.length>0))
      {
        if($scope.company.logo == null){
          $scope.founder_step2_error.logo='Please upload a logo.';
        }
        if($scope.companyHighlights.length == 0){
          $scope.founder_step2_error.highlight='Please add a Company Highlight.';
        }
        if($scope.uploadDocsNo.company_pitch_file == 0){
          $scope.founder_step2_error.pitch_deck='Please upload a pitch deck document. It must be PDF format.';
        }
        if($scope.companyLeaders.length == 0){
          $scope.founder_step2_error.company_leader = 'Please add at least add one company leader.';
        }
        $('.loader-overlay').fadeOut(0);
        return;
      }
      var dob = $scope.payment.dob;
      if(dob!=null){
        dob = _date2str(dob);
      }

      if(!$scope.a.international_company){
        RegisterCompanyII($.cookie('accessToken')).edit({id: $scope.company.id, name: $scope.company.name, headline: $scope.company.headline, website: $scope.company.website, description: $scope.company.description, about: $scope.company.about, url: URLS.REVIEW_COMPANY_INFO_II+'%s' ,dob:dob, address:$scope.payment.address, ssn:$scope.payment.ssn, city:$scope.payment.city, state:$scope.payment.state, zip:$scope.payment.zip, bank_account_number:$scope.payment.bank_account_number, bank_account_type:$scope.payment.bank_account_type, bank_account_holder:$scope.payment.bank_account_holder, bank_account_routing:$scope.payment.bank_account_routing , is_international:false, facebook_link:$scope.company.facebook_link, twitter_link:$scope.company.twitter_link, linkedin_link:$scope.company.linkedin_link}).$promise.then(
        function(/*value*/){
          if($scope.path==='ent-registration-step2'){
            $location.path(URLS.SUCCESSFUL_SUBMISSION);
          }
          else if ($scope.path === 'edit-company'){
            $location.path(URLS.COMPANY+$routeParams.id);
          }
          else if($scope.path==='review-complete-company-info'){
            $location.path(URLS.COMPANY+$routeParams.id);
          }
        },
        function(error){
          console.log(error);
          $scope.founder_step2_error.error = error.data.message;
          if(_.has(error.data,'description')){
            $scope.step2errors.description.value=error.data.description[0];
          }
          if(_.has(error.data,'headline')){
            $scope.step2errors.headline.value=error.data.headline[0];
          }
          if(_.has(error.data,'website')){
            $scope.step2errors.website.value=error.data.website[0];
          }
        });
      }
      else{
        $scope.upload = $upload.upload({
          url: BASE_PATH.API()+API_PATH.REGISTER_COMPANY_STEP_II+$scope.company.id,
          method: 'PUT',
          data: {id: $scope.company.id, name: $scope.company.name, headline: $scope.company.headline, website: $scope.company.website, description: $scope.company.description, about: $scope.company.about, country: $scope.payment.country, bank_account_number:$scope.payment.bank_account_number, bank_account_type:$scope.payment.bank_account_type, bank_account_holder:$scope.payment.bank_account_holder, bank_account_routing:$scope.payment.bank_account_routing , url: URLS.REVIEW_COMPANY_INFO_II+'%s', is_international:true, facebook_link:$scope.company.facebook_link, twitter_link:$scope.company.twitter_link, linkedin_link:$scope.company.linkedin_link},
          headers: {'Authorization': 'Bearer '+ $.cookie('accessToken')},
        }).progress(function(/*event*/) {
          $('.loader-overlay-2').fadeIn('fast');
          $('.loader-overlay').fadeOut('fast');
        }).success(function(data, status) {
          if(status == 201){
            $('.loader-overlay-2').fadeOut('fast');
          }
          // file is uploaded successfully
          $scope.companyHighlights.push(data);
          // $scope.docs.push({'key': data.key});
          $scope.highlight.file = null;
          $scope.highlight.tempFile = null;
          $scope.highlight.description = null;
          $scope.highlight.date = newDate;
          $scope.step2errors.highlight.value='';
          clearHighlightPreviewImage();

          if($scope.path==='ent-registration-step2'){
            $location.path(URLS.SUCCESSFUL_SUBMISSION);
          }
          else if ($scope.path === 'edit-company'){
            $location.path(URLS.COMPANY+$routeParams.id);
          }
          else if($scope.path==='review-complete-company-info'){
            $location.path(URLS.REVIEW_COMPANY_INFO_II+$routeParams.id+'/');
          }
        }).error(function(error){
          console.log(error);
          $scope.founder_step2_error.error = error.data.message;
          $('.loader-overlay-2').fadeOut('fast');
          if(error.description){
            $scope.step2errors.highlight.value=error.description[0];
          }
          if(error.file){
            $scope.step2errors.highlight.value=error.file[0];
          }
          clearHighlightPreviewImage();
          if(_.has(error.data,'description')){
            $scope.step2errors.description.value=error.data.description[0];
          }
          if(_.has(error.data,'headline')){
            $scope.step2errors.headline.value=error.data.headline[0];
          }
          if(_.has(error.data,'website')){
            $scope.step2errors.website.value=error.data.website[0];
          }
        });
      }
    };

    /* preview page */
    $scope.seePreview = function(){
      $scope.template='views/partials/preview.html';
      $scope.showForm = false;
      $scope.company.facebook_link=checkHTTP($scope.company.facebook_link);
      $scope.company.twitter_link=checkHTTP($scope.company.twitter_link);
      $scope.company.linkedin_link=checkHTTP($scope.company.linkedin_link);
    };

    $scope.init = function(){
      if ($location.path().indexOf('preview') != -1){
        var id = $routeParams.id;
        $scope.isPreview= true;
        EditCompany($.cookie('accessToken')).get({id: id}).$promise.then(
          function(value){
            $scope.company_template = 'views/partials/preview.html';
            $scope.company = value.company;
            $scope.company.id = id;
            $scope.companyHighlights = value.highlights;
            $scope.documents = value.documents;
            var video_link = _.find(value.documents, function(item){ return (item.document_type==='company_video_link'); });
            if (video_link) {
              $scope.video_link = $sce.trustAsResourceUrl(video_link.link);
            }
            var pitch_file = _.find(value.documents, function(item){ return (item.document_type==='company_pitch_file'); });
            if(pitch_file){
              $scope.pdfUrl = pitch_file.file;
            }
            $scope.companyLeaders = value.social_links;
          },
          function(error){
            console.log(error);
          }
        );
      }
    };

    /* Admin view methods */
    $scope.approveStep2Company = function(id){
      ApproveStep2Company($.cookie('accessToken')).approve({company_id: id, notify:true}).$promise.then(
        function(/*value*/){
          FlashOnNextRoute.setMessage({type: 'positive', text: MSG.ADMIN_APPROVE});
          $location.path(URLS.ADMIN_DASHBOARD);
        },
        function(error){
          $scope.founder_step2_error.error = error.data.message;
          FlashMessage.showFlash('error',MSG.FAILURE);
          console.log(error);
        }
      );
    };

    $scope.approveStep2CompanyWithoutNotification = function(id){
      ApproveStep2Company($.cookie('accessToken')).approve({company_id: id, notify:false}).$promise.then(
        function(/*value*/){
          FlashMessage.showFlash('positive',MSG.ADMIN_APPROVE);
          $location.path(URLS.ADMIN_DASHBOARD);
        },
        function(error){
          $scope.founder_step2_error.error = error.data.message;
          FlashMessage.showFlash('error',MSG.FAILURE);
          console.log(error);
        }
      );
    };

    $scope.rejectStep2Company = function(id){
      RejectStep2Company($.cookie('accessToken')).reject({company_id: id, notify:true}).$promise.then(
        function(/*value*/){
          $location.path(URLS.ADMIN_DASHBOARD);
          FlashMessage.showFlash('positive',MSG.ADMIN_REJECT);
        },
        function(error){
          FlashMessage.showFlash('error',MSG.FAILURE);
          $scope.founder_step2_error.error = error.data.message;
          console.log(error);
        }
      );
    };

    $scope.rejectStep2CompanyWithoutNotification = function(id){
      RejectStep2Company($.cookie('accessToken')).reject({company_id: id,notify:false}).$promise.then(
        function(/*value*/){
          $location.path(URLS.ADMIN_DASHBOARD);
          FlashMessage.showFlash('positive',MSG.ADMIN_REJECT);
        },
        function(error){
          FlashMessage.showFlash('error',MSG.FAILURE);
          $scope.founder_step2_error.error = error.data.message;
          console.log(error);
        }
      );
    };

    /* To check http/https in field */

    function checkHTTP(text){
      if(text == null || text == '') { return ''; }
      var checkString=new RegExp('^(http|https)://', 'i');
      var checkResult=checkString.test(text.toString());
      if(!checkResult) { return 'http://'+text; }
      return text;
    }

    function checkNull(data){
      if(data == null || data == '' || data == 'null' || data == undefined){
        return '';
      }
      else{
        return data;
      }
    }

    function checkSpace(data){
      if(data == '' || data == undefined){
        return null;
      }
      else{
        return data;
      }
    }

    /*My profile */

    $scope.kulizaInitMyProfile=function(){
      var userData=$cookieStore.get('user');
      var userName=userData.username;
      $scope.myProfile.userName=userName;

      var temp_phone_number = userData.profile.phone_number;
      var temp_phone_array = temp_phone_number.split('ext');

      $scope.myProfile.userPhoneNumber = temp_phone_array[0];
      $scope.myProfile.userPhoneNumber_ext = temp_phone_array[1];

      $scope.userFirstName = userData.profile.first_name;
      $scope.userLastName = userData.profile.last_name;
    };

    $scope.getDocumentsUploaded=function(){
      AllDocuments($.cookie('accessToken')).docs({ id: $cookieStore.get('user').entity_id}).$promise.then(
        function(value){
          $scope.user_docs=[];
          _.each(value,function(item){
            $scope.user_docs.push(item);
          });
        },
        function(error){
          console.log(error);
        }
      );
    };

    $scope.changePassword=function(){
      if($scope.change_password.old_password == $scope.change_password.new_password){
        $scope.changePasswordError='New password must be different from old password.';
        $('.loader-overlay').fadeOut('fast');
        return;
      }
      if($scope.change_password.new_password!=$scope.change_password.new_password_2){
        $scope.changePasswordError='New passwords do not match.';
        $('.loader-overlay').fadeOut('fast');
        return;
      }
      if($scope.change_password.new_password.length < 6){
        $scope.changePasswordError='New password must be at least 6 characters long.';
        $('.loader-overlay').fadeOut('fast');
        return;
      }
      ChangePassword($.cookie('accessToken')).change({old_password:$scope.change_password.old_password,new_password:$scope.change_password.new_password}).$promise.then(
        function(/*value*/){
          FlashMessage.showFlash('positive',MSG.CHANGE_PASSWORD);
          $scope.changePasswordError='';
          $scope.change_password={};
          $scope.logout();
        },
        function(error){
          if(error.data){
            $scope.changePasswordError=error.data.message;
          }
          console.log(error);
        }
      );
    };

    $scope.hideDocuments = function(){
      return ($cookieStore.get('user') && $cookieStore.get('user').role == 4);
    };

    $scope.changeEmail=function(){
      ChangeEmail($.cookie('accessToken')).change({new_email:$scope.myProfile.userName}).$promise.then(
        function(/*value*/){
          $timeout(function(){
            Auth.logout();
            $rootScope.a_msg_on_sign_in_screen = MSG.EMAIL_CHANGE_LOGOUT_MSG;
          },500);
        },
        function(error){
          console.log(error);
          $scope.change_email_error=error.data.message;
        }
      );
    };

    $scope.aspectRatio = false;
    $scope.removeLogo = function() {
      $scope.company.logo=null;
      document.getElementById('previewLogo').setAttribute('src', '');
    };

    $scope.getInvestorHistory = function(){
      InvestorHistory($.cookie('accessToken')).list({id:$cookieStore.get('user').entity_id}).$promise.then(
        function(value){
          $scope.investor_history=groupByTimestamp(value.results);
        },
        function(error){
          console.log(error);
        }
      );
    };

    function groupByTimestamp(value){
      return _.groupBy(value, function(history){ return setTimePeriod(history.created_at); });
    }

    function setTimePeriod(str){
      var date = new Date(str);
      var monthNames = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ];
      var month = monthNames[date.getMonth()];
      var year = date.getFullYear();
      return month+' '+year;
    }

    $scope.addLeaderImage = function(files){
      $scope.step2errors.leader.value = '';
      if(files.length == 0){
        $scope.step2errors.leader.value = 'Please only upload JPEG, JPG or PNG image of size 300*200 only.';
        return;
      }
      /*
      if(!validImage(files[0].type)){
        $scope.step2errors.leader.value = 'Please only upload JPEG, JPG or PNG image';
        return;
      }
      */
      $scope.leader.file = files[0];
    };

    $scope.leader={};
    $scope.onLeaderAdd = function(company_id) {
      if($scope.leader.file==null) {
        $scope.step2errors.leader.value='Please upload an image for your Company Highlight.';
        return;
      }
      if($scope.leader.description==''){
        $scope.step2errors.leader.value='Please enter a description for your Company Highlight.';
        return;
      }
      var file = $scope.leader.file;
      $scope.upload = $upload.upload({
        url: BASE_PATH.API()+API_PATH.ADD_LEADER,
        data: {name:$scope.leader.name ,headline:$scope.leader.headline ,description:$scope.leader.description , company: company_id, url: URLS.REVIEW_COMPANY_INFO_II+'%s'},
        headers: {'Authorization': 'Bearer '+ $.cookie('accessToken')},
        file: file,
      }).progress(function(evt) {
        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
      }).success(function(data) {
        // file is uploaded successfully
        $scope.companyLeaders.push(data);
        // $scope.docs.push({'key': data.key});
        $scope.leader.file = null;
        $scope.leader.tempFile = null;
        $scope.leader.description = null;
        $scope.leader.name = null;
        $scope.leader.headline = null;
        $scope.step2errors.leader.value='';
        $scope.founder_step2_error.company_leader='';
        clearLeaderPreviewImage();
      }).error(function(error){
        console.log(error);
        $scope.previewImage=null;
        if(error.description){
          $scope.step2errors.leader.value=error.description[0];
        }
        if(error.file){
          $scope.step2errors.leader.value=error.file[0];
        }
        clearLeaderPreviewImage();
      });
    };

    function clearLeaderPreviewImage(){
      $('#preview-image-leader').attr('src','');
      $scope.leader.file=null;
    }

    function clearHighlightPreviewImage(){
      $('#preview-image-highlight').attr('src','');
      $scope.highlight.file =null;
    }

    $scope.clearLeaderPreviewImage = function(){
      clearLeaderPreviewImage();
    };

    $scope.clearHighlightPreviewImage = function(){
      clearHighlightPreviewImage();
    };

    $scope.checkEmptyObject = function(obj){
      return _.isEmpty(obj);
    };

    $scope.seeAllLeaders = function(){
      $scope.template = 'views/founder-list.html';
    };

    $scope.returnFromPdfView = function(){
      $scope.template = 'views/partials/preview.html';
    };

    $scope.seePitchDeck = function(){
      var pitch_file = $scope.company_pitch_file;

      if(pitch_file){
        $scope.pdfUrl = pitch_file.file;
      }

      $scope.template = 'components/pdf-viewer/pdf-viewer.html';
      $.getScript( 'bower_components/pdfjs-dist/build/pdf.worker.js' )
        .done(function( /*script, textStatus*/ ) {
          $('.loader-overlay').fadeOut('fast');
        })
        .fail(function( /*jqxhr, settings, exception*/ ) {
          $('.loader-overlay').fadeOut('fast');
        });
    };

    $scope.getNavStyle = function(scroll) {
      if(scroll > 100) { return 'pdf-controls fixed'; }
      else { return 'pdf-controls'; }
    };

    $scope.updatePhoneNumber = function(){
      $scope.change_phone_error='';
      var phone_number_ = $scope.myProfile.userPhoneNumber + 'ext' + ($scope.myProfile.userPhoneNumber_ext? $scope.myProfile.userPhoneNumber_ext: '');
      ChangePhone($.cookie('accessToken')).update({phone_number: phone_number_}).$promise.then(
        function(/*value*/){
          var userData=$cookieStore.get('user');
          userData.profile.phone_number = phone_number_;
          $cookieStore.put('user',userData);
          $scope.edit_phone_number = false;
          TimeoutMessage.showSuccess('#changePhonrNumberFlash','Phone number changed');
        },
        function(error){
          $scope.change_phone_error = error.data.message;
          console.log(error);
          TimeoutMessage.showError('#changePhonrNumberFlash', 'Phone number not changed');
        }
      );
    };

    $scope.viewDoc = function(doc,print){
      $scope.pdfUrl = doc.file ;
      $scope.showPrintButton = print;
      $('.loader-overlay-2').fadeIn('fast');
      $scope.template = 'components/pdf-viewer/pdf-viewer.html';
      $.getScript( 'bower_components/pdfjs-dist/build/pdf.worker.js' )
      .done(function( script, textStatus,data ) {
        $('.loader-overlay').fadeOut('fast');
        if(data.status == 200 || data.status == 201) {
          $('.loader-overlay-2').fadeOut('fast');
        }
      })
      .fail(function( /*jqxhr, settings, exception*/ ) {
        $('.loader-overlay-2').fadeOut('fast');
        $('.loader-overlay').fadeOut('fast');
      });
    };

    $scope.resetHighlight = function() {
      var newDate = new Date();
      $scope.highlight.file = null;
      $scope.highlight.date = newDate;
      $scope.highlight.description = '';
      $('#preview-image-highlight').attr('src','');
      $scope.step2errors.highlight.value = '';
    };

    $scope.resetFounder = function() {
      $scope.leader.name = '';
      $scope.leader.headline = '';
      $scope.leader.description = '';
      $scope.leader.file = null;
      $('#preview-image-leader').attr('src','');
      $scope.step2errors.leader.value = '';
    };

    $scope.cancelEmailChange = function(){
      var userData=$cookieStore.get('user');
      var userName=userData.username;
      $scope.myProfile.userName=userName;
    };

  }]
);

'use strict';
// jshint eqeqeq:false
// jshint eqnull:true

/* Controllers */

angular.module('myApp.controllers').controller(
  'RegisterInvestorCtrl',
  ["$scope", "Auth", "RegisterIndInvestor", "$location", "URLS", "$cookieStore", "$routeParams", "GetInvestorInformation", "AnswerInvestorQuestion", "GetInvestorQuestions", "PutIndivisualInvestorInformation", "AdminApproveInvestorStep2", "FlashMessage", "MSG", "FlashOnNextRoute", "AdminRejectInvestorStep2", function(
    $scope,
    Auth,
    RegisterIndInvestor,
    $location,
    URLS,
    $cookieStore,
    $routeParams,
    GetInvestorInformation,
    AnswerInvestorQuestion,
    GetInvestorQuestions,
    PutIndivisualInvestorInformation,
    AdminApproveInvestorStep2,
    FlashMessage,
    MSG,
    FlashOnNextRoute,
    AdminRejectInvestorStep2
  ) {
    $scope.isAdmin = false;
    $scope.inst_errors={
      first_name:{value:''},
      middle_name:{value:''},
      last_name:{value:''},
      inst_name:{value:''},
      website:{value:''},
      email:{value:''},
      phoneno:{value:''},
      location:{value:''}
    };

    $scope.regis_errors={
      first_name:{value:''},
      middle_name:{value:''},
      last_name:{value:''},
      email:{value:''},
      phoneno:{value:''}
    };

    $scope.investor={};

    $scope.investor_question={};

    $scope.logout = function(){
      FlashOnNextRoute.setMessage({type: 'neutral', text: MSG.LOGOUT});
      Auth.logout();
    };


    // Form submission
    $scope.submitIndForm = function(){
      if($scope.investor_registration.$invalid){
        FlashMessage.showFlash('error',MSG.INVALID_FORM);
        $('.loader-overlay').fadeOut('fast');
        return;
      }

      // call api to submit form with/without validations
      addNewInvestor();
    };

    // Add a new individual investor
    function addNewInvestor(){
      if($scope.investor.bypass_approval == null) { $scope.investor.bypass_approval=false; }

      var phone_no = $scope.investor.phone_number + 'ext';
      if($scope.investor.phone_number_ext != null) {
        phone_no = phone_no + $scope.investor.phone_number_ext;
      }

      RegisterIndInvestor($.cookie('accessToken')).create({'salutation':$scope.investor.salutation, 'first_name': $scope.investor.first_name, 'middle_name': $scope.investor.middle_name, 'last_name': $scope.investor.last_name, 'email': $scope.investor.email, 'phone_number': phone_no, bypass_approval:$scope.investor.bypass_approval}).$promise.then(

        function(/*value*/){
          FlashMessage.showFlash('positive',MSG.SUCCESSFULL_INVITE);
          $location.path(URLS.ADMIN_DASHBOARD);
        },
        function(error){
          FlashMessage.showFlash('error',MSG.FAILURE);
          console.log(error);
          if(error.data.first_name){
            $scope.regis_errors.first_name.value=error.data.first_name[0];
          }
          if(error.last_name){
            $scope.regis_errors.last_name.value=error.last_name[0];
          }
          if(error.data.email){
            $scope.regis_errors.email.value=error.data.email[0];
          }
          if(error.data.phone_number){
            $scope.regis_errors.phoneno.value = error.data.phone_number[0];
          }
        }
      );
    }

    $scope.isApproval = function(){
      if($location.path().indexOf('review') === -1){
        return false;
      }
      return true;
    };

    /********************* Investor Step 2 ****************************************************/

    $scope.initInvestorGeneralInfo=function(){
      var type=$routeParams.type;
      var id= $routeParams.id;
      var user = $cookieStore.get('user');
      if(user.role == 1){
        $scope.isAdmin=true;
      }
      $scope.hideIndividualFields=false;
      if(type === 'individual'){
        $scope.investor_head='Individual Investor';
        $scope.hideIndividualFields=true;
      }
      else if(type === 'corporation'){
        $scope.investor_head='Corporation, LLC or Partnership';
      }
      else if(type === 'trust'){
        $scope.investor_head='Trust';
      }
      else if(type === 'other'){
        $scope.investor_head='Other Entity';
      }

      GetInvestorInformation($.cookie('accessToken')).show({id:id}).$promise.then(

        function(value){
          if ($scope.isApproval() && value.status !== 'step2_created') {
            FlashOnNextRoute.setMessage({type: 'neutral-d', text: MSG.ALREADY_MODERATED});
            $location.path(URLS.ADMIN_DASHBOARD);
          } else {
            $scope.investor=value;
            $scope.investor.id=id;

            var temp_phone_array = value.phone_number.split('ext');
            $scope.investor.phone_number = temp_phone_array[0];
            if(temp_phone_array[1]!='undefined'){
              $scope.investor.phone_number_ext = temp_phone_array[1];
            }
            else{
              $scope.investor.phone_number_ext = '';
            }

            if(value.investor_phone_number != null){
              temp_phone_array = value.investor_phone_number.split('ext');
              $scope.investor_phone_number = temp_phone_array[0];
              $scope.investor_phone_number_ext = temp_phone_array[1];
            }
            else{
              $scope.investor_phone_number = '';
              $scope.investor_phone_number_ext = '';
            }

            $scope.jurisdiction = {
              isAmerican: value.is_american_jurisdiction,
              usState: value.is_american_jurisdiction? value.jurisdiction || 'DE': null,
              internationalLocation: value.is_american_jurisdiction? null: value.jurisdiction
            };
          }
        },
        function(error){
          FlashMessage.showFlash('error',MSG.FAILURE);
          console.log(error);
        }
      );
    };

    $scope.jurisdiction={};

    var currentJurisdiction = function() {
      if ($scope.jurisdiction.isAmerican) {
        return $scope.jurisdiction.usState;
      } else {
        return $scope.jurisdiction.internationalLocation;
      }
    };

    $scope.takeEntityToQuestions=function(){
      if($scope.investor_general_info.$invalid){
        $scope.investor_general_info_error = MSG.INVALID_FORM;
        $('.loader-overlay').fadeOut('fast');
        return;
      }
      if($scope.investor_general_info.phoneno.$invalid){
        $('.loader-overlay').fadeOut('fast');
        return;
      }
      if($scope.investor.is_US_resident == null){
        $scope.investor_step_2.error.us_resident = 'Please select radio button for US Citizen';
        $('.loader-overlay').fadeOut('fast');
        return;
      }

      var type=$routeParams.type;
      var id=$routeParams.id;
      var role={roles:[]};
      role.roles.push($scope.investor.role);
      if($scope.investor.is_US_resident == null) { $scope.investor.is_US_resident=false; }
      var phone_number_ = $scope.investor.phone_number + 'ext';
      if($scope.investor.phone_number_ext != undefined) {
        phone_number_ = phone_number_ + $scope.investor.phone_number_ext;
      }

      var ent_investor_phone_number = $scope.investor_phone_number + 'ext' + $scope.investor_phone_number_ext;

      PutIndivisualInvestorInformation($.cookie('accessToken')).create({
        'id':id,
        'salutation': $scope.investor.salutation,
        'first_name': $scope.investor.first_name,
        'last_name': $scope.investor.last_name,
        'phone_number': phone_number_,
        'investor_type': type,
        'role': role,
        'middle_name': $scope.investor.middle_name,
        'institution_name': $scope.investor.institution_name,
        'investor_phone_number': ent_investor_phone_number,
        'jurisdiction': currentJurisdiction(),
        'is_US_resident': $scope.investor.is_US_resident
      }).$promise.then(
        function(/*value*/){
          if ($location.path().indexOf('review') === -1){
            $location.path(URLS.NEW_ACCREDITED_INVESTOR_QUESTIONS1+type+'/'+id);
          }
          else{
            $location.path(URLS.REVIEW_ACCREDITED_QUESTIONS+type+'/'+id);
          }
        },
        function(error){
          $scope.investor_general_info_error='Error';
          console.log(error.data);
        }
      );
    };

    $scope.initInvestorQuestions= function(){
      var id = $routeParams.id;
      var type = $routeParams.type;
      var user = $cookieStore.get('user');
      $scope.investor.id=id;
      if(user.role == 1){
        $scope.isAdmin=true;
      }
      GetInvestorQuestions($.cookie('accessToken')).query({id:id, investor_type:type}).$promise.then(
        function(value){
          if (value.new_questionnaire || user.status === 'step1_approved' || user.role == 1)
          {
            $scope.questions = value.question_list;
          }
          else
          {
            $location.path(URLS.INVESTOR_DASHBOARD);
          }
        },
        function(error){
          console.log(error);
        }
      );
    };

    $scope.isOptionChecked = function(answerArray, currentOption){
      return (_.indexOf(answerArray, currentOption) !== -1);
    };

    $scope.pushAccreditedAnswer = function(question, questionType, option, questionIndex)
    {
      if (questionType === 'single_select' && question.answer !== [option.id])
      {
        question.answer = [option.id];
        _.each(question.options, function(opt){
          opt.checked = false;
        });
        option.checked = true;
      }
      if (questionType === 'multi_select')
      {
        option.checked = !option.checked;
        if (option.checked)
        {
          question.answer.push(option.id);
        }
        else
        {
          question.answer = _.reject(question.answer, function(optId){
            return optId === option.id;
          });
        }
      }
      if (questionType === 'boolean' && question.answer !== {answer: option})
      {
        question.answer = {answer: option};

        // Question Hiding Logic for Corporation Questionnaire
        if ($routeParams.type === 'corporation')
        {
          if (questionIndex === 0 || questionIndex === 1)
          {
            if(option === 'true'){
              $scope.questions[2].hideQuestion = true;
            }
            else{
              var remnantQuestion;
              if (questionIndex === 0){
                remnantQuestion = $scope.questions[1];
              }
              else{
                remnantQuestion = $scope.questions[0];
              }
              if (!remnantQuestion.answer || remnantQuestion.answer.answer === 'false'){
                $scope.questions[2].hideQuestion = false;
              }
            }
          }
        }
      }
    };

    function areAllQuestionsAnswered(){
      if ($routeParams.type === 'corporation')
      {
        return true;
      }
      else{
        var numberAnswered = 0;
        _.each($scope.questions, function(ques){
          if (ques.answer && ((ques.question.question_type === 'boolean' && (ques.answer.answer === 'true' || ques.answer.answer === 'false')) || (ques.answer.length > 0))){
            numberAnswered++;
          }
        });
        return (numberAnswered === $scope.questions.length);
      }
    }

    $scope.submitAccredited = function()
    {
      var user = $cookieStore.get('user');
      AnswerInvestorQuestion($.cookie('accessToken')).create({questions: $scope.questions, investor: $routeParams.id, url: URLS.REVIEW_INVESTOR_ANSWERS+'%s/%s'}).$promise.then(
        function(/*value*/)
        {
          if(user.status === 'step2_approved'){
            FlashOnNextRoute.setMessage({type: 'neutral-d', text: MSG.SUCCESS});
            $location.path(URLS.INVESTOR_DASHBOARD);
          }
          else{
            $location.path(URLS.INVESTOR_LANDING);
          }
        },
        function(error){
          $scope.investor_question.error=error.data.message;
          console.log(error);
        }
      );
    };

    $scope.setCertified = function(){
      $scope.investor_question.error='';
      if (areAllQuestionsAnswered()){
        $scope.showCertifyOverlay = true;
      }
      else{
        $scope.investor_question.error='You must select one of the above to verify your status as an accredited investor';
        $scope.showCertifyOverlay = false;
      }
    };

    /********************* End of Investor Step 2 ****************************************************/

    /******* Admin view for page ************/

    /* Step 2 approval form, admin view */

    /* Admin approving the investors step 2 */
    $scope.approveInvestorStep2=function(notify){
      var id = $routeParams.id;
      AdminApproveInvestorStep2($.cookie('accessToken')).approve({ id:id, approved_step:'step2_approved', notify:notify}).$promise.then(

        function(/*value*/){
          FlashMessage.showFlash('positive',MSG.ADMIN_APPROVE);
          $location.path(URLS.ADMIN_DASHBOARD);
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    $scope.rejectInvestorStep2=function(notify){
      var id = $routeParams.id;
      AdminRejectInvestorStep2($.cookie('accessToken')).approve({ id:id, reject_step:'step2_rejected',notify:notify}).$promise.then(

        function(/*value*/){
          FlashMessage.showFlash('positive',MSG.ADMIN_REJECT);
          $location.path(URLS.ADMIN_DASHBOARD);
        },
        function(/*error*/){
          FlashMessage.showFlash('error',MSG.FAILURE);
        }
      );
    };

    $scope.gotoInvestorQuestions=function(type){
      var id=$routeParams.id;
      if(type === 'individual'){
        $location.path(URLS.NEW_INVESTOR_GENERAL_INFO+'individual/'+id);
      }
      else if(type === 'corp'){
        $location.path(URLS.NEW_INVESTOR_GENERAL_INFO+'corporation/'+id);
      }
      else if(type === 'trust'){
        $location.path(URLS.NEW_INVESTOR_GENERAL_INFO+'trust/'+id);
      }
      else if(type === 'other'){
        $location.path(URLS.NEW_INVESTOR_GENERAL_INFO+'other/'+id);
      }
    };

    $scope.checkValueChange=function(obj){
      if(obj === 'email'){
        if($scope.inst_errors.email.value.length>0){
          $scope.inst_errors.email.value='';
        }
        else if($scope.regis_errors.email.value.length>0){
          $scope.regis_errors.email.value='';
        }
      }
    };

  }]
);

'use strict';
// jshint eqeqeq:false
// jshint eqnull:true

/* Controllers */

angular.module('myApp.controllers').controller(
  'InvestorCtrl',
  ["$scope", "Auth", "$location", "URLS", "$cookieStore", "$routeParams", "$rootScope", "INVEST_MSGS", "InvestorDashboard", "FlashMessage", "MSG", "FlashOnNextRoute", "$timeout", "moment", function(
    $scope,
    Auth,
    $location,
    URLS,
    $cookieStore,
    $routeParams,
    $rootScope,
    INVEST_MSGS,
    InvestorDashboard,
    FlashMessage,
    MSG,
    FlashOnNextRoute,
    $timeout,
    moment
  ) {
    var days_until = function(target_date_string) {
      var now_moment = moment(new Date());
      var end_moment = moment(target_date_string);
      var diff_as_ms = end_moment.diff(now_moment);
      var diff_as_duration = moment.duration(diff_as_ms);
      var diff_as_days = diff_as_duration.asDays();

      return Math.round(diff_as_days);
    };

    $scope.companies={
      invested_companies:[],
      funding_gallery_companies:[],
      open_companies:[],
      recommended_companies:[],
      following_companies:[],
      all:[]
    };

    $scope.show_more_company={
      type:'',
      companies:[]
    };

    $scope.investor_dashboard='';
    $scope.investor = {};

    $scope.investor_dashboard_msg={};

    $scope.statesMap = {
      'ppm':URLS.BANK_PAYMENT,
      'payment':URLS.INVESTOR_QUESTIONNAIRE,
      'questionnaire':URLS.RISK_FACTOR_ACK,
      'ra':URLS.IRA
    };

    $scope.logout = function(){
      FlashOnNextRoute.setMessage({type: 'neutral', text: MSG.LOGOUT});
      Auth.logout();
    };

    $scope.investorDashboardInit=function(){
      $scope.investor_dashboard='views/partials/investor-dashboard-new.html';
      var user=$cookieStore.get('user');
      var id=user.entity_id;
      InvestorDashboard($.cookie('accessToken')).get({id:id}).$promise.then(

        function(value){
          $scope.companies.following_companies=value.following_companies;
          $scope.companies.invested_companies=value.invested_companies;
          $scope.companies.open_companies=value.open_companies;
          $scope.companies.funding_gallery_companies=value.funding_gallery_companies;
          $scope.companies.recommended_companies=value.recommended_companies;
          $scope.companies.show_all = $scope.sortCompanies(value.show_all_companies);

          if($scope.companies.open_companies.length == 0){
            $scope.investor_dashboard_msg.open_companies = MSG.NO_COMPANIES_FOR_INVESTMENT;
          }

          function prepCardData(item){
            if(item.fund == null){
              item.state = 'COMING SOON';
              item.card_template = 'views/investorcards/comingsoon.html';
            }
            else{
              if(item.fund.status === 'modify pending'){
                item.state = 'ON HOLD';
                item.card_template = 'views/investorcards/onhold.html';
              }
              else if(item.fund.status === 'open pending'){
                item.state = 'ESCROW PENDING';
                item.card_template = 'views/investorcards/escrowpending.html';
              }
              else if(item.fund.status === 'open'){
                item.state = 'OPEN';
                item.card_template = 'views/investorcards/open.html';
                item.days_remaining = days_until(item.fund.end_date);
              }
              else{
                item.state = 'CLOSED';
                item.card_template = 'views/investorcards/closed.html';
              }
              var dataset={dataPoint:[]};
              var deno = parseFloat(item.fund.seeking_amount);

              var a = parseFloat(item.fund.seeking_amount)/deno;
              var c = 0;
              var b = 0;
              
              if(item.your_investment){
                b = (parseFloat(item.fund.raised_amount)-parseFloat(item.your_investment))/deno;
                c = parseFloat(item.your_investment) / deno;
              } else {
                b = parseFloat(item.fund.raised_amount)/deno;

              }

              if(parseFloat(item.fund.raised_amount) >= parseFloat(item.fund.seeking_amount)){
                a=0;
              }

              dataset.dataPoint.push(a);
              dataset.dataPoint.push(b);
              dataset.dataPoint.push(c);

              var progress = Math.floor((b + c) * 100);
              if (progress < 1) {
                progress = 1;
              } else if (progress > 100) {
                progress = 100;
              }

              item.progress = progress;
              item.dataset=dataset;
            }
          }
          function handleSearchItem(item, type){
            var sample_com={};

            if (item.company) {
              sample_com = {
                'type':type,
                'slug':(item.company.slug || ''),
                'name':(item.company.name || '')
              };

              $scope.companies.all.push(sample_com);
            }
          }
          _.each($scope.companies.show_all, function(item){
            prepCardData(item);
          });

          _.each($scope.companies.following_companies, function(item){
            prepCardData(item);
            handleSearchItem('Following');
          });

          _.each($scope.companies.invested_companies, function(item){
            prepCardData(item);
            handleSearchItem('Invested');
          });

          _.each($scope.companies.open_companies, function(item){
            prepCardData(item);
            handleSearchItem('Open');
          });

          _.each($scope.companies.funding_gallery_companies, function(item){
            prepCardData(item);
            handleSearchItem('Funding Gallery');
          });

          _.each($scope.companies.recommended_companies, function(item){
            prepCardData(item);
            handleSearchItem('Recommended');
          });
        },
        function(error){
          console.log(error);
        }
      );

      if($rootScope.user.progress_list && $rootScope.user.progress_list.length > 0 ){
        $scope.investment ={};
        $scope.investment.complete_popup = $rootScope.user.progress_list;
        $timeout(function(){
          $('#completeInvestmentModal').modal('show');
        },1000);
        $rootScope.user.progress_list=[];
      }
    };

    $scope.sortCompanies = function (companies) { //sort companies by fund date and fund status
      var sortedCompanies = _(companies).chain()
        .sortBy(function(company){ //sort by start date first
          if(company.fund){
            return company.fund.start_date;
          } else { //if funding hasn't started, just return 0
            return '0';
          }
        })
        .sortBy( function (company) { //sort by fund status, currently it's sorted by OPEN, CLOSED, ANYTHING NOT COMING SOON, COMING SOON
          if (company.fund) {
            switch (company.fund.status) {
            case 'closed':
              return 2;
            case 'open':
              return 1;
            default:
              return 3;
            }
          } else {
            return 4;
          }
        }).value();

      return sortedCompanies;
    };

    $scope.showAll=function(type){
      if(type==='open'){
        $scope.show_more_company.companies=$scope.companies.open_companies;
        $scope.show_more_company.type='Companies open for investments';
      }
      else if(type==='invested'){
        $scope.show_more_company.companies=$scope.companies.invested_companies;
        $scope.show_more_company.type='Your Investments';
      }
      else if(type==='following'){ // currently not used
        $scope.show_more_company.companies=$scope.companies.following_companies;
        $scope.show_more_company.type='Companies you are following';
      }
      else if(type==='gallery'){
        $scope.show_more_company.companies=$scope.companies.funding_gallery_companies;
        $scope.show_more_company.type='Companies soon open for investment';
      }
      else if(type === 'recommend'){
        $scope.show_more_company.companies=$scope.companies.recommended_companies;
        $scope.show_more_company.type='Companies that we recommend for you';
      }
      else if(type === 'all'){
        $scope.show_more_company.companies = $scope.companies.show_all;
        $scope.show_more_company.type = 'All companies';
      }

      $scope.investor_dashboard='views/partials/investor_dashboard_show_more.html';
    };

    $scope.goBackToMainInvestorDashboard=function(){
      $scope.show_more_company.type='';
      $scope.show_more_company.companies=[];

      $scope.investor_dashboard='views/partials/investor-dashboard-new.html';
    };

    $scope.goToCompany=function(company){
      if(company.snapshot){
        $location.path(URLS.COMPANY+company.company.slug+'/snapshot/'+company.fund.snapshot);
      }
      else{
        $location.path(URLS.COMPANY+company.company.slug);
      }
    };

    $scope.goToCompanyFromInvestorDash=function(company){
      if (typeof company === 'object' && typeof company.fund === 'object' && ((company.fund && company.fund.status !== 'closed') || !company.fund)) {
        if(company.snapshot){
          $location.path(URLS.COMPANY+company.company.slug+'/snapshot/'+company.fund.snapshot);
        }
        else{
          $location.path(URLS.COMPANY+company.company.slug);
        }
      }
    };

    $scope.gotoCompanyFromPopup =  function(com){
      $('#completeInvestmentModal').modal('hide');
      var redirection_path = $scope.statesMap[com.progress_data.state];
      var company_slug = com.slug;
      switch(com.progress_data.state){
        case 'started':
          break;

        case 'ppm':
          $cookieStore.put('company',{'company_id':com.id, 'funding_id':com.fund.id, 'invest_amount':com.progress_data.amount,'company_name':com.name, 'progress_id':com.progress_data.id}, {path:'/'});
          var is_end_date = new Date(com.fund.end_date);
          var goal_has_reached = parseFloat(com.fund.raised_amount) >= parseFloat(com.fund.seeking_amount);
          if(new Date() > is_end_date){
            $('#cant_invest').modal('show');
            $scope.invest_error_msg = INVEST_MSGS.END_DATE_CROSSED;
          }
          else if(goal_has_reached){
            $scope.invest_error_msg = INVEST_MSGS.GOAL_REACHED;
            $('#cant_invest').modal('show');
          }
          else{
            $location.path(redirection_path+company_slug);
          }
          break;

        case 'payment':
        case 'questionnaire':
        case 'ra':
          $cookieStore.put('company',{'company_id':com.id, 'funding_id':com.fund.id, 'invest_amount':com.progress_data.amount,'company_name':com.name, 'progress_id':com.progress_data.id, 'investment_id':com.progress_data.investment}, {path:'/'});
          $location.path(redirection_path+company_slug);
          break;
      }
    };

    $scope.completeUnfinishedInvestment = function(com){
      var redirection_path = $scope.statesMap[com.progress_data.state];
      var company_slug = com.company.slug;
      switch(com.progress_data.state){
        case 'started':
          break;

        case 'ppm':
          $cookieStore.put('company',{'company_id':com.company.id, 'funding_id':com.fund.id, 'invest_amount':com.progress_data.amount,'company_name':com.company.name, 'progress_id':com.progress_data.id}, {path:'/'});
          var is_end_date = new Date(com.fund.end_date);
          var goal_has_reached = parseFloat(com.fund.raised_amount) >= parseFloat(com.fund.seeking_amount);
          if(new Date() > is_end_date){
            $('#cant_invest').modal('show');
            $scope.invest_error_msg = INVEST_MSGS.END_DATE_CROSSED;
          }
          else if(goal_has_reached){
            $scope.invest_error_msg = INVEST_MSGS.GOAL_REACHED;
            $('#cant_invest').modal('show');
          }
          else{
            $location.path(redirection_path+company_slug);
          }
          break;

        case 'payment':
        case 'questionnaire':
        case 'ra':
          $cookieStore.put('company',{'company_id':com.company.id, 'funding_id':com.fund.id, 'invest_amount':com.progress_data.amount,'company_name':com.company.name, 'progress_id':com.progress_data.id, 'investment_id':com.progress_data.investment}, {path:'/'});
          console.log($cookieStore.get('company'));
          $location.path(redirection_path+company_slug);
          break;
      }
    };

  }]
);

'use strict';


// Listing all APIs
angular.module('myApp.api',[]).
	factory('BASE_PATH', ['$rootScope', '$window', 'ENV', function($rootScope, $window, ENV){
    return {
      API: function (){
        return ENV.apiEndpoint;
      }
    };
	}]).
	constant('API_PATH', {
		'MEDIA':'/media/',
		'GET_FAVICON': 'http://www.google.com/s2/favicons?domain=',
		'GET_ACCESS_LEVELS': '/v1.0/access-levels', // [currently unused]
		'CREATE_NEW_ADMIN': '/v1.0/cvc/admin/create-admin',
		'REGISTER_COMPANY': '/v1.0/company/register-company',
		'REGISTER_COMPANY_STEP_II': '/v1.0/company/edit/company-details/',
		'REGISTER_IND_INVESTOR': '/v1.0/investor/register/individual',
		'COMPANY_PRIVATE': '/v1.0/company/company-page/',
		'ASK_COMPANY_QUESTION': '/v1.0/company/posted-question',
		'RAISE_COMPANY_FUND': '/v1.0/company/raise-fund',
		'ACTION_ON_FUNDING': '/v1.0/company/funding-action/',
		'LOGIN': '/v1.0/get-token',
		'ADMIN_ADMIN_PANEL': '/v1.0/admin/get-admin-info',
		'ADMIN_COMPANY_PANEL': '/v1.0/admin/get-founder-pages-info',
		'ADMIN_MEMBER_PANEL': '/v1.0/admin/get-member-pages-info',
		'GET_COMPANY_FOR_APPROVAL': '/v1.0/company/profile/',
		'GET_INVESTOR_FOR_STEP_2':'/v1.0/investor/secondary-information/',
		'GET_COMPANY_FOR_EDIT': '/v1.0/company/edit/',
		'APPROVE_COMPANY': '/v1.0/admin/approve-step1c-profile',
		'REJECT_COMPANY': '/v1.0/admin/reject-step1c-profile',
		'APPROVE_STEP2_COMPANY': '/v1.0/admin/approve-step2c-profile',
		'REJECT_STEP2_COMPANY': '/v1.0/admin/reject-step2c-profile',
		'APPROVE_INVESTOR': '/v1.0/cvc/admin/approve/',
		'REJECT_INVESTOR': '/v1.0/cvc/admin/reject/',
		'FETCH_WITH_LINK_TOKEN': '/v1.0/admin/get-profile-token-info/',
		'ACTIVATE_USER': '/v1.0/admin/activate-user',
		'UPLOAD_HIGHLIGHT': '/v1.0/company/edit/add-highlight',
		'DELETE_HIGHLIGHT': '/v1.0/company/edit/delete-highlight/',
		'DELETE_COMPANY_LEADER': '/v1.0/company/edit/delete-leader/',
		'ADD_COMPANY_DOCUMENT': '/v1.0/company/edit/add-document',
		'ADD_COMPANY_LOGO': '/v1.0/company/add-logo',
		'DELETE_COMPANY_DOCUMENT': '/v1.0/company/edit/delete-document/',
		'GET_INVESTOR_QUESTIONS': '/v1.0/investor/question/',
		'ANSWER_INVESTOR_QUESTION':'/v1.0/investor/answer',
		'GET_ADMIN_NOTIFICATIONS': '/v1.0/cvc/admin/updates/',
		'GET_COMPANY_NOTIFICATIONS': '/v1.0/company/updates/',
		'GET_INVESTOR_NOTIFICATIONS': '/v1.0/investor/updates/',
		'INVESTOR_DASHBOARD':'/v1.0/investor/dashboard/',
		'MARK_NOTIFICATION_READ': '/v1.0/feed/is-read',
		'MARK_ALL_NOTIFICATIONS_READ': '/v1.0/feed/all-read',
		'CHANGE_PASSWORD':'/v1.0/change-password',
		'FORGOT_PASSWORD':'/v1.0/reset-password',
		'UPLOAD_PPM':'/v1.0/company/add-ppm',
		'DOCUSIGN':'/v1.0/document/sign-document',
		'CONTACT_CITIZEN':'/v1.0/contact/admin',
		'GET_DOCUMENTS':'/v1.0/investor/all-documents/',
		'RECOMMEND_COMPANY':'/v1.0/cvc/admin/publish/',
		'LOGOUT':'/v1.0/logout/',
		'CHANGE_EMAIL':'/v1.0/change-email/',
		'COMPANY_SNAPSHOT':'/v1.0/company/snapshot/',
		'GET_USERS':'/v1.0/cvc/admin/users/get',
		'UPDATE_USER':'/v1.0/cvc/admin/user/update/',
		'USER_HISTORY':'/v1.0/cvc/admin/get-login-history/',
		'UPDATE_USER_EMAIL':'/v1.0/admin-change-email',
		'BBX_FUND_ESCROW':'/v1.0/banc/fund-escrow/',
		'BANK_PAYMENTS_LIST':'/v1.0/banc/get-all-escrow',
		'GET_COMPANY_INVESTORS_LIST':'/v1.0/banc/get-escrow-investors/',
		'INVESTOR_HISTORY':'/v1.0/banc/investor-transaction/',
		'ADD_LEADER': '/v1.0/company/edit/add-leader',
		'DELETE_LEADER':'/v1.0/company/delete-leader/',
		'ADMIN_TASK':'/v1.0/cvc/admin/tasks/',
		'DELETE_INVESTOR_TRANS':'/v1.0/cvc/admin/cancel-investment/',
		'UPDATE_FUND_OPTIONS':'/v1.0/cvc/admin/change-recommended-investment/',
		'INVESTOR_ACTIONS':'/v1.0/cvc/admin/investor-actions/',
		'ADMIN_ADMIN_ACTIONS':'/v1.0/cvc/admin/admin-actions/',
		'WIRE_DETAILS':'/v1.0/cvc/admin/wire-transaction',
		'SELL_SHARES':'/v1.0/cvc/admin/stock-holder',
		'FUNDING_ROUND_ACTIONS':'/v1.0/cvc/admin/admin-fund-round-action/',
		'CHANGE_PHONE':'/v1.0/change-phone-number',
		'INVEST_QUESTIONS':'/v1.0/investment-flow/questions',
		'RFA_QUESTIONS':'/v1.0/investment-flow/risk-questions',
		'IRS_SIGN':'/v1.0/document/sign-irs',
		'DOC_STEP3':'/v1.0/investment-flow/init-questionnaire',
		'INVESTOR_INVESTMENTS':'/v1.0/cvc/admin/investor-investments/',
		'INVESTOR_EVENT_LOGS':'/v1.0/cvc/admin/investor-event-log/',
		'INVESTOR_FOLLOWING_LIST':'/v1.0/cvc/admin/investor-following/',
		'COMPANY_FOLLOWERS_LIST':'/v1.0/cvc/admin/company-following-list/',
		'COMPANY_FUNDING_ROUNDS':'/v1.0/cvc/admin/company-funding-round/',
		'USER_NOTE':'/v1.0/cvc/admin/user-note/',
		'GET_NOTE':'/v1.0/cvc/admin/get-note/',
		'ADD_NOTE':'/v1.0/cvc/admin/add-note/',
		'INVESTOR_QUESTIONS':'/v1.0/cvc/admin/investor-question/',
		'PDF_DATA':'/v1.0/document/get-doc'
  }
);

'use strict';
// jshint eqeqeq:false
// jshint eqnull:true

/* Services */
angular.module('myApp.services', ['ngResource']).
  value('version', '0.1').
  factory('CreateNewAdmin', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.CREATE_NEW_ADMIN, {}, {
        create: {method: 'POST', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('RegisterCompany', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken) {
      return $resource(BASE_PATH.API()+API_PATH.REGISTER_COMPANY, {}, {
        create: {method: 'POST', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('RegisterCompanyII', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.REGISTER_COMPANY_STEP_II+':id', {id: '@id'}, {
        edit: {method: 'PUT', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('RegisterIndInvestor', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken) {
      return $resource(BASE_PATH.API()+API_PATH.REGISTER_IND_INVESTOR, {}, {
        create: {method: 'POST', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('DeleteCompanyLeader', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.DELETE_COMPANY_LEADER+':id', {id: '@id'}, {
        delete: {method: 'DELETE', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('LinkToken', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return $resource(BASE_PATH.API()+API_PATH.FETCH_WITH_LINK_TOKEN+':token', {token: '@token'}, {
      fetch: {method: 'GET'}
    });
  }]).
  factory('CompanyPrivateView', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.COMPANY_PRIVATE+':id', {id: '@id'}, {
        show: {method: 'GET', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('GetCompanyQuestions', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.ASK_COMPANY_QUESTION+'/:id', {id: '@id'}, {
        get: {method: 'GET', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('AskCompanyQuestion', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.ASK_COMPANY_QUESTION, {}, {
        ask: {method: 'POST', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('GetCompanyUpdates', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.GET_COMPANY_NOTIFICATIONS+':id', {id: '@id'}, {
        get: {method: 'GET', headers: {'Authorization': 'Bearer '+ accessToken}, isArray: true}
      });
    };
  }]).
  factory('RaiseCompanyFund', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.RAISE_COMPANY_FUND, {}, {
        post: {method: 'POST', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('RaiseCompanyFunding', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.RAISE_COMPANY_FUND+'/:id', {id: '@id'}, {
        put: {method: 'PUT', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('GetRaisedFunding', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.RAISE_COMPANY_FUND+'/:id', {id: '@id'}, {
        get: {method: 'GET', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('ActionOnFunding', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.ACTION_ON_FUNDING+':id', {id: '@id'}, {
        post: {method: 'PUT', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('CompanyForApproval', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.GET_COMPANY_FOR_APPROVAL+':id', {id: '@id'}, {
        show: {method: 'GET', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('GetInvestorInformation',['$resource','API_PATH','BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.GET_INVESTOR_FOR_STEP_2+':id', {id:'@id'}, {
        show: {method: 'GET', headers: {'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('PutIndivisualInvestorInformation',['$resource','API_PATH','BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.GET_INVESTOR_FOR_STEP_2+':id',{id:'@id'},{
        create: {method: 'PUT', headers: {'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('ApproveCompany', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.APPROVE_COMPANY, {}, {
        approve: {method: 'POST', headers:{'Authorization': 'Bearer '+accessToken}}
      });
    };
  }]).
  factory('RejectCompany', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.REJECT_COMPANY, {}, {
        reject: {method: 'POST', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('ApproveStep2Company', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.APPROVE_STEP2_COMPANY, {}, {
        approve: {method: 'POST', headers:{'Authorization': 'Bearer '+accessToken}}
      });
    };
  }]).
  factory('RejectStep2Company', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.REJECT_STEP2_COMPANY, {}, {
        reject: {method: 'POST', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('AdminApproveInvestorStep2', ['$resource','API_PATH','BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.APPROVE_INVESTOR+':id', {id: '@id', approved_step:'@approved_step'},{
        approve: {method: 'POST' ,headers: {'Authorization': 'Bearer '+accessToken}}
      });
    };
  }]).
  factory('AdminRejectInvestorStep2', ['$resource','API_PATH','BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.REJECT_INVESTOR+':id', {id: '@id', approved_step:'@approved_step'},{
        approve: {method: 'POST' ,headers: {'Authorization': 'Bearer '+accessToken}}
      });
    };
  }]).
  factory('AnswerInvestorQuestion',['$resource','API_PATH','BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.ANSWER_INVESTOR_QUESTION,{}, {
        create: {method: 'POST', headers: {'Authorization': 'Bearer '+accessToken}}
      });
    };
  }]).
  factory('GetInvestorQuestions', ['$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.GET_INVESTOR_QUESTIONS+':id',{id: '@id'},{
        query: {method: 'GET', headers: {'Authorization': 'Bearer '+accessToken}}
      });
    };
  }]).
  factory('ChangeInvestorAnswer', ['$resource','API_PATH','BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.ANSWER_INVESTOR_QUESTION+'/:id',{id:'@id'},{
        change: {method: 'PUT', headers: {'Authorization': 'Bearer '+accessToken}}
      });
    };
  }]).
  factory('AdminCompanyPanel', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.ADMIN_COMPANY_PANEL, {}, {
        show: {method: 'GET', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('AdminAdminPanel', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.ADMIN_ADMIN_PANEL, {}, {
        show: {method: 'GET', headers: {'Authorization': 'Bearer '+ accessToken}, isArray:true}
      });
    };
  }]).
  factory('AdminMemberPanel', [ '$resource', 'BASE_PATH', 'API_PATH', function($resource,BASE_PATH, API_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.ADMIN_MEMBER_PANEL, {}, {
        show: { method: 'GET', headers: {'Authorization': 'Bearer '+ accessToken} }
      });
    };
  }]).
  factory('EditCompany', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.GET_COMPANY_FOR_EDIT+':id', {id: '@id'}, {
        get: {method: 'GET', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('DeleteHighlight', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.DELETE_HIGHLIGHT+':id', {id: '@id'}, {
        delete: {method: 'DELETE', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('DeleteCompanyDoc', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.DELETE_COMPANY_DOCUMENT+':id', {id: '@id'}, {
        delete: {method: 'DELETE', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('EmbedLink', ['$resource','API_PATH','BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.ADD_COMPANY_DOCUMENT,{},{
        add: {method: 'POST' ,headers:{'Authorization':'Bearer '+ accessToken }}
      });
    };
  }]).
  factory('AdminNotifications',['$resource','API_PATH','BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.GET_ADMIN_NOTIFICATIONS+':id',{id:'@id'},{
        get: {method: 'GET', headers: {'Authorization':'Bearer '+ accessToken}, isArray: true}
      });
    };
  }]).
  factory('CompanyNotifications',['$resource','API_PATH','BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.GET_COMPANY_NOTIFICATIONS+':id',{id:'@id'},{
        get: {method: 'GET', headers: {'Authorization':'Bearer '+accessToken}, isArray: true}
      });
    };
  }]).
  factory('InvestorNotifications',['$resource','API_PATH','BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.GET_INVESTOR_NOTIFICATIONS+':id',{id:'@id'},{
        get: {method: 'GET', headers: {'Authorization':'Bearer '+accessToken}, isArray: true}
      });
    };
  }]).
  factory('InvestorDashboard', ['$resource','API_PATH','BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.INVESTOR_DASHBOARD+':id',{id:'@id'},{
        get: {method: 'GET', headers: {'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('MarkNotificationRead', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.MARK_NOTIFICATION_READ, {}, {
        mark: {method: 'POST', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('MarkAllNotificationsRead', [ '$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.MARK_ALL_NOTIFICATIONS_READ, {}, {
        mark: {method: 'POST', headers: {'Authorization': 'Bearer '+ accessToken}}
      });
    };
  }]).
  factory('ChangePassword', ['$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.CHANGE_PASSWORD,{},{
        change: {method: 'POST', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('ForgotPassword', ['$resource','API_PATH','BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.FORGOT_PASSWORD,{},{
        forgot: {method: 'POST', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('DoDocusign', ['$resource','API_PATH','BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.DOCUSIGN,{},{
        do: {method: 'POST', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('DocusignResult', ['$resource','API_PATH','BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.DOCUSIGN+'/:id',{id:'@id'},{
        status: {method: 'PUT', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('ContactCitizen', ['$resource','API_PATH','BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(){
      return $resource(BASE_PATH.API()+API_PATH.CONTACT_CITIZEN, {},{
        send: {method: 'POST'}
      });
    };
  }]).
  factory('AllDocuments', ['$resource','API_PATH','BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.GET_DOCUMENTS+':id',{id:'@id'},{
        docs: {method:'GET',headers:{'Authorization':'Bearer '+accessToken}, isArray:true}
      });
    };
  }]).
  factory('RecommendCompany', ['$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.RECOMMEND_COMPANY+':id',{id:'@id'},{
        recommend: {method: 'PUT', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('ChangeEmail', ['$resource', 'API_PATH', 'BASE_PATH', function($resource, API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.CHANGE_EMAIL, {}, {
        change: {method: 'POST', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('CompanySnapshot', ['$resource', 'API_PATH', 'BASE_PATH',function($resource, API_PATH, BASE_PATH) {
    return function(accessToken) {
      return $resource(BASE_PATH.API() + API_PATH.COMPANY_SNAPSHOT+':id', {id:'@id'}, {
        details:{method: 'GET', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('FlashMessage', ['$rootScope', '$timeout', function($rootScope, $timeout){
    var FlashMessage={};

    FlashMessage.showFlash=function(msgType,msgString){ // TODO msgString is passed in in zillions of calls; why is it not used?
      $('.' + msgType + '-wrap').fadeIn('fast');

      $timeout(function () {
        $('.' + msgType + '-wrap').fadeOut('fast');
      }, 5000);
    };

    return FlashMessage;
  }]).
  factory('PPMTemplateId', ['$resource', 'API_PATH','BASE_PATH', function($resource, API_PATH, BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.UPLOAD_PPM, {},{
        upload: {method: 'POST', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('FlashOnNextRoute', function() {
    var queue = [];
    var currentMessage = '';

    return {
      setMessage: function(message) {
        queue.push(message);
      },
      getMessage: function() {
        currentMessage = queue.shift() || '';
        return currentMessage;
      },
      clearQueue: function() {
        queue = [];
      }
    };
  }).
  factory('LogoutUser', ['$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.LOGOUT , {}, {
        logout :{method : 'POST',headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('GetUsers', ['$resource', 'API_PATH', 'BASE_PATH', function($resource, API_PATH, BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() +API_PATH.GET_USERS, {}, {
        fetch: {method: 'POST', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('UpdateUserInfo', ['$resource', 'API_PATH', 'BASE_PATH', function($resource, API_PATH, BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.UPDATE_USER+':id', {id:'@id'},{
        update: {method: 'PUT', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('UserHistory', ['$resource' , 'API_PATH', 'BASE_PATH', function($resource, API_PATH, BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.USER_HISTORY+':id', {id:'@id'}, {
        fetch: {method: 'GET', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('UpdateUserEmail', ['$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH, BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.UPDATE_USER_EMAIL, {},{
        change: {method: 'POST', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('BBXEscrowFunding', ['$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH, BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.BBX_FUND_ESCROW+':id', {id:'@id'}, {
        create: {method: 'POST', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('GetAllEscrows', ['$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH, BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.BANK_PAYMENTS_LIST, {}, {
        list: {method: 'GET', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('GetCompanyInvestmentDetails', ['$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH, BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.GET_COMPANY_INVESTORS_LIST+':id', {id:'@id'}, {
        list: {method: 'GET', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('InvestorHistory', ['$resource', 'API_PATH', 'BASE_PATH',function($resource,API_PATH, BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.INVESTOR_HISTORY+':id',{id:'@id'}, {
        list: {method: 'GET', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('AdminTask', ['$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH, BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.ADMIN_TASK + ':id', {id:'@id'}, {
        list: {method: 'GET', headers:{'Authorization':'Bearer '+accessToken}, isArray: true}
      });
    };
  }]).
  factory('DeleteInvestorTransaction', ['$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH, BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.DELETE_INVESTOR_TRANS + ':id', {id:'@id'}, {
        del: {method: 'POST', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('UpdateFundOptions', ['$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH, BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.UPDATE_FUND_OPTIONS + ':id' , {id:'@id'}, {
        update: {method: 'PUT', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('AdminInvestorActions', ['$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH, BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.INVESTOR_ACTIONS + ':id', {id:'@id'}, {
        update: {method: 'PUT', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('AdminAdminActions', ['$resource', 'API_PATH', 'BASE_PATH', function($resource,API_PATH, BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.ADMIN_ADMIN_ACTIONS + ':id', {id:'@id'}, {
          update: {method: 'PUT', headers:{'Authorization':'Bearer '+accessToken}}
        });
    };
  }]).
  factory('InvestorWireDetails', ['$resource', 'API_PATH', 'BASE_PATH',function($resource,API_PATH, BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.WIRE_DETAILS, {}, {
        wire: {method:'GET', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('SellShares', ['$resource', 'API_PATH', 'BASE_PATH',function($resource,API_PATH, BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.SELL_SHARES, {}, {
        sell: {method: 'POST', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('FundingRoundActions', ['$resource', 'API_PATH', 'BASE_PATH',function($resource,API_PATH, BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.FUNDING_ROUND_ACTIONS +':id', {id:'@id'}, {
        update: {method: 'PUT', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('ChangePhone', ['$resource', 'API_PATH', 'BASE_PATH',function($resource,API_PATH, BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.CHANGE_PHONE, {}, {
        update: {method: 'PUT', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('InvestQuestions', ['$resource', 'API_PATH', 'BASE_PATH',function($resource,API_PATH, BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.INVEST_QUESTIONS, {}, {
        update: {method: 'POST', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('RFAQuestions', ['$resource', 'API_PATH', 'BASE_PATH',function($resource,API_PATH, BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.RFA_QUESTIONS, {}, {
        update: {method: 'POST', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('DoIRS', ['$resource','API_PATH','BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.IRS_SIGN,{},{
        do: {method: 'POST', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('IRSResult', ['$resource','API_PATH','BASE_PATH', function($resource,API_PATH,BASE_PATH){
    return function(accessToken){
      return $resource(BASE_PATH.API()+API_PATH.IRS_SIGN+'/:id',{id:'@id'},{
        status: {method: 'PUT', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('DocSignStep3', ['$resource','API_PATH','BASE_PATH',function($resource,API_PATH,BASE_PATH) {
    return function(accessToken) {
      return $resource(BASE_PATH.API()+API_PATH.DOC_STEP3,{},{
        fetch: {method: 'GET', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('InvestorInvestments', ['$resource','API_PATH','BASE_PATH',function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.INVESTOR_INVESTMENTS+':id', {id:'@id'}, {
        fetch: {method: 'GET', headers:{'Authorization':'Bearer '+accessToken}, isArray:true}
      });
    };
  }]).
  factory('InvestorEvents', ['$resource','API_PATH','BASE_PATH',function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.INVESTOR_EVENT_LOGS+':id', {id:'@id'}, {
        fetch: {method: 'GET', headers:{'Authorization':'Bearer '+accessToken}, isArray:true}
      });
    };
  }]).
  factory('InvestorFollowingList', ['$resource','API_PATH','BASE_PATH',function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.INVESTOR_FOLLOWING_LIST+':id', {id:'@id'}, {
        fetch: {method: 'GET', headers:{'Authorization':'Bearer '+accessToken}, isArray:true}
      });
    };
  }]).
  factory('TimeoutMessage', ['$timeout',function ($timeout) {
    var TimeoutMessage = {};

    TimeoutMessage.showSuccess = function(_id,message){
      $(_id).removeClass('hide');
      $(_id).addClass('success-message-fade');
      $(_id).html(message);
      $timeout(function(){
        $(_id).removeClass('success-message-fade');
        $(_id).html('');
        $(_id).addClass('hide');
      },4000);
    };

    TimeoutMessage.showError = function(_id,message){
      $(_id).removeClass('hide');
      $(_id).addClass('error-message-fade');
      $(_id).html(message);
      $timeout(function(){
        $(_id).removeClass('error-message-fade');
        $(_id).html('');
        $(_id).addClass('hide');
      },4000);
    };

    return TimeoutMessage;
  }]).
  factory('CompanyFollowers', ['$resource','API_PATH','BASE_PATH',function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.COMPANY_FOLLOWERS_LIST+':id', {id:'@id'}, {
        fetch: {method: 'GET', headers:{'Authorization':'Bearer '+accessToken}, isArray:true}
      });
    };
  }]).
  factory('CompanyFundingRounds', ['$resource','API_PATH','BASE_PATH',function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.COMPANY_FUNDING_ROUNDS+':id', {id:'@id'}, {
        fetch: {method: 'GET', headers:{'Authorization':'Bearer '+accessToken}, isArray:true}
      });
    };
  }]).
  factory('GetNote', ['$resource','API_PATH','BASE_PATH',function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.GET_NOTE+':id', {id:'@id'}, {
        fetch: {method: 'GET', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('AddNote', ['$resource','API_PATH','BASE_PATH',function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.ADD_NOTE+':id', {id:'@id'}, {
        add: {method: 'PUT', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('UserNote', ['$resource','API_PATH','BASE_PATH',function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.USER_NOTE+':id', {id:'@id'}, {
        add: {method: 'PUT', headers:{'Authorization':'Bearer '+accessToken}},
        get: {method: 'GET', headers:{'Authorization':'Bearer '+accessToken}, isArray:true},
        create: {method: 'POST', headers:{'Authorization':'Bearer '+accessToken}},
        del: {method: 'DELETE', headers:{'Authorization':'Bearer '+accessToken}}
      });
    };
  }]).
  factory('InvestorQuestions', ['$resource','API_PATH','BASE_PATH',function($resource,API_PATH,BASE_PATH) {
    return function(accessToken){
      return $resource(BASE_PATH.API() + API_PATH.INVESTOR_QUESTIONS+ ':id', {id:'@id'}, {
        get: {method: 'GET', headers:{'Authorization':'Bearer '+accessToken}, isArray:true}
      });
    };
  }]).
  service('formatters', ['$filter', function($filter) {
    return {
      numberAsMoney: function(amount) {
        return '$' + $filter('number')(amount, 0);
      }
    };
  }]);


// Authentication module
angular.module('myApp.authentication', ['ngResource']).
  factory('Auth', [ '$rootScope', '$resource', 'API_PATH', 'BASE_PATH', '$location', '$cookieStore', 'URLS', 'FlashOnNextRoute','LogoutUser', 'localStorageService', '$timeout', 'cvcConfig', function($rootScope, $resource,API_PATH,BASE_PATH,$location,$cookieStore,URLS, FlashOnNextRoute, LogoutUser, localStorageService, $timeout, cvcConfig) {
    var logActivityPromise;

    var self = {
      login: $resource(BASE_PATH.API()+API_PATH.LOGIN, {}, {
        getToken: {
          method: 'POST' ,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          transformRequest: function(obj) {
            var str = [];
            for(var p in obj) {
              str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
            }
            return str.join('&');
          }
        }
      }),
      activate: $resource(BASE_PATH.API()+API_PATH.ACTIVATE_USER, {}, {
        go: {
          method: 'POST' ,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          transformRequest: function(obj) {
            var str = [];
            for(var p in obj) {
              str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
            }
            return str.join('&');
          }
        }
      }),
      logActivity: function() {
        var activityTimestamp = new Date().getTime();
        if (logActivityPromise) {
          $timeout.cancel(logActivityPromise);
        }
        logActivityPromise = $timeout(
          function() { localStorageService.set('user.lastActivityDate', activityTimestamp); },
          100, // delay in millis.
          false // don't trigger within $apply block
        );
      },
      lastActivityDate: function() {
        var lastActivityDateMillis = localStorageService.get('user.lastActivityDate');
        if (lastActivityDateMillis) {
          return new Date(lastActivityDateMillis);
        }
      },
      isLoginTimedOut: function() {
        if (!self.isLoggedIn()) {
          return false; // can't timeout if not logged in
        }
        if (!self.lastActivityDate()) {
          return true; // if we have no record of activity we have to log you out
        }

        var maxIdleSeconds = cvcConfig.get('idleTimeoutInSeconds', 60 * 30);
        var maxIdleMillis = maxIdleSeconds * 1000;
        var lastActivityDateMillis = self.lastActivityDate().getTime();
        var nowMillis = new Date().getTime();
        var millisElapsedSinceLastActivity = nowMillis - lastActivityDateMillis;
        return millisElapsedSinceLastActivity > maxIdleMillis;
      },
      logout: function() {
        localStorageService.clearAll('user\\.'); // clear all localStorage entries relating to the logged-in with 'user'
        LogoutUser($.cookie('accessToken')).logout();
        $cookieStore.remove('user');
        $.removeCookie('accessToken');
        $cookieStore.remove('refreshToken');
        $rootScope.user = null;
        $rootScope.a_msg_on_sign_in_screen = '';

        FlashOnNextRoute.clearQueue();
        $location.path(URLS.SIGN_IN);
      },
      isLoggedIn: function() {
        return ($.cookie('accessToken'));
      },
      // To authorize a user to access a defined route
      authorize: function(accessLevel, role) {
        if(role === undefined){
          var user = $cookieStore.get('user');
          role = user ? user.role : 8;
          role = role ? role: 8;
        }
        return accessLevel & role;
      }
    };

    return self;
  }]).
  run(['$rootScope', '$location', 'Auth', '$cookieStore', '$route', 'FlashOnNextRoute', 'FlashMessage', 'MSG', 'URLS', 'cvcConfig', 'Users', function ($rootScope, $location, Auth, $cookieStore, $route, FlashOnNextRoute, FlashMessage, MSG, URLS, cvcConfig, Users) {
    var idleInterval;
    var checkLoginTimeout = function() {
      if(Auth.isLoginTimedOut()) {
        window.clearInterval(idleInterval);
        Auth.logout();
        $rootScope.a_msg_on_sign_in_screen = MSG.INACTIVITY_LOGOUT_MSG;
        $('.modal-backdrop').remove();
      }
    };

    $('body').mousemove(function(){
      Auth.logActivity();
    });
    $('body').keypress(function(){
      Auth.logActivity();
    });

    $rootScope.$on('$routeChangeStart', function (event, next, current) {
      checkLoginTimeout();
      var authedUser = Users.authorized();
      var isRedirect;
      var authorizedByAccess;
      var authorizedByPermissions;
      var authorized;

      if (authedUser && authedUser.requiresVersionUpgrade()) {
        Auth.logout();
      } else {
        isRedirect = next.redirectTo !== undefined;
        authorizedByAccess = next.access && Auth.authorize(next.access);
        authorizedByPermissions = authedUser && next.permission && authedUser.hasPermission(next.permission);
        authorized = authorizedByAccess || authorizedByPermissions;

        if($rootScope.back_template_stack === 'company'){
          $rootScope.back_template_stack='';
          current.scope.company_template = 'views/founder-dashboard.html';
          $location.path('/company/'+current.params.id);
          return false;
        }
        if($rootScope.user === undefined){
          $rootScope.user = $cookieStore.get('user');
        }

        if(Auth.isLoggedIn()){
          if(idleInterval) {
            window.clearInterval(idleInterval);
          }
          var idlePollIntervalMillis = cvcConfig.get('idlePollIntervalMillis', 5000);
          idleInterval = setInterval(checkLoginTimeout, idlePollIntervalMillis);
        }
        else{
          if(idleInterval) {
            window.clearInterval(idleInterval);
          }
        }

        Auth.logActivity();

        if (!authorized && !isRedirect) {
          if (Auth.isLoggedIn()) {
            $location.path(URLS.FOUROFOUR).replace();
          } else {
            Auth.logout();
          }
        }
      }
    });

    $rootScope.$on('$locationChangeStart', function(/*event, newUrl, oldUrl*/){
      if($rootScope.user === undefined){
        if($cookieStore.get('user')){
          $rootScope.user = $cookieStore.get('user');
        }
      }
    });

    // Display Flash messages on change of route
    $rootScope.$on('$routeChangeSuccess', function() {
      var currentMessage = FlashOnNextRoute.getMessage();
      if (currentMessage){
        FlashMessage.showFlash(currentMessage.type, currentMessage.text);
      }
    });

  }]);

angular.module('myExceptions', []).factory('$exceptionHandler',
  ['$window', '$log','ENV', function ($window, $log, ENV) {
    if (ENV.sentryURL != null && ENV.sentryURL != '' && window.Raven) {
      console.log(ENV.sentryURL);
      console.log('Using the RavenJS exception handler.');
      Raven.config(ENV.sentryURL, {
        whitelistUrls: [
          'static.dev.citizen.vc/scripts',
          'www.dev.citizen.vc/scripts',
          '*.citizen.vc'
        ],
        ignoreErrors: [
          // Random plugins/extensions
          'top.GLOBALS',
          // See: http://blog.errorception.com/2012/03/tale-of-unfindable-js-error. html
          'originalCreateNotification',
          'canvas.contentDocument',
          'MyApp_RemoveAllHighlights',
          'http://tt.epicplay.com',
          'Can\'t find variable: ZiteReader',
          'jigsaw is not defined',
          'ComboSearch is not defined',
          'http://loading.retry.widdit.com/',
          'atomicFindClose',
          // Facebook borked
          'fb_xd_fragment',
          // ISP "optimizing" proxy - `Cache-Control: no-transform` seems to reduce this. (thanks @acdha)
          // See http://stackoverflow.com/questions/4113268/how-to-stop-javascript-injection-from-vodafone-proxy
          'bmi_SafeAddOnload',
          'EBCallBackMessageReceived',
          // See http://toolbar.conduit.com/Developer/HtmlAndGadget/Methods/JSInjection.aspx
          'conduitPage'
        ]
      }).install();
      return function (exception/*, cause*/) {
        $log.error.apply($log, arguments);
        Raven.captureException(exception);
      };
    } else {
      return function (/*exception, cause*/) {
        $log.error.apply($log, arguments);
      };
    }
  }]);

// TODO: Integrate refresh token authentication

'use strict';

/**
 * Utils. General utilities that couldn't find another home.
 *
 * Members:
 *   .assert(condition, message) -- Assert a predicate condition, otherwise throw an error
 *     with the provided string message.
 *
 *   .resolveDeferred(thing) -- Return a promise that immediately resolves to the provided
 *     object.
 *
 *   .rejectDeferred(thing) -- Return a promise that immediately rejects with the provided
 *     object.
 *
 *   .indexById(things) -- Returns a map indexed by the result of things[n].id().
 *
 *   .extendPrototype(thing) -- Extends a prototype, with some controls for strict extension.
 *     (see tests for better examples)
 */
angular.module('myApp.services').service(
'Utils',
["$q", "cvcConfig", function($q, cvcConfig) {
  var assertsEnabled = function() {
    return cvcConfig.get('assertsEnabled', false);
  };
  var Utils = {
    assert: function(condition, message) {
      if (assertsEnabled() && !condition) {
        throw new Error('Assertion Failed: ' + message);
      }
    },

    resolveDeferred: function(anything) {
      var deferred = $q.defer();
      var promise = deferred.promise;
      deferred.resolve(anything);

      return promise;
    },

    rejectDeferred: function(anything) {
      var deferred = $q.defer();
      var promise = deferred.promise;
      deferred.reject(anything);

      return promise;
    },

    indexById: function(arrayOfThings) {
      return _.object(_.map(arrayOfThings, function(thing) {
        return [thing.id(), thing];
      }));
    },

    resourceIdSpace: function(idString) {
      var colonIndex = idString.indexOf(':');
      Utils.assert(colonIndex > 0, 'All resource IDs must have a namespace preceded by a colon. There was no colon in: ' + idString);

      return idString.substring(0, colonIndex);
    },

    extendPrototype: function(originalObject, extension) {
      var originalPrototype = originalObject.prototype;
      var extended = _.extend({}, originalPrototype, extension);

      return _.mapObject(extended, function(value, key) {
        var extensionSpec = value.extend;
        if (extensionSpec) {
          Utils.assert(value.def, 'Extending with an extension spec requires the new function to be defined as "def"');
          if (extensionSpec === 'override') {
            Utils.assert(originalPrototype[key], '"override" was specified, but base prototype does not contain the key "' + key + '"');
            value = value.def;
          }
        }

        return value;
      });
    }
  };

  return Utils;
}]);

'use strict';

/**
 * Enum. An enumerated type. It is probably best explained by example.
 *   var Composers = Enums.create({
 *     JS_BACH: {value: 'jsbach', humanReadable: 'Johann Sebastian Bach'},
 *     BEETHOVEN: {value: 'beethoven', humanReadable: 'Ludwig van Beethoven'}
 *   });
 *
 *   Composers.forValue('jsbach') === Composers.JS_BACH // true
 *   Composers.all() === [Composers.JS_BACH, Composers.BEETHOVEN] // true
 *
 *   Composers.JS_BACH.humanReadable() === 'Johann Sebastian Bach' // true
 *   Composers.JS_BACH.value() === 'jsbach' // true
 *   Composers.JS_BACH.key() === 'JSBAC' // true
 *
 */

/**
 * Enums. Factory for creating new enumerated types.
 *
 * Members:
 *   .create(entries) --
 *     Creates a new Enum from the provided entries. Entries should be a
 *     javascript objects keyed by some identifier, valued by an object
 *     with the following properties:
 *       .value -- the conceptual "value" of the enum (usually this is the one set by our API)
 *       .humanReadable -- a human readable representation of the value
 *
 *     Example:
 *       var Composers = Enums.create({
 *         JS_BACH: {value: 'jsbach', humanReadable: 'Johann Sebastian Bach'},
 *         BEETHOVEN: {value: 'beethoven', humanReadable: 'Ludwig van Beethoven'}
 *       });
 */
angular.module('myApp.services').service(
'Enums',
["Utils", function(Utils) {
  var Enums = {
    create: function(entries) {
      var Enum = function(key, value, humanReadable) {
        this._key = key;
        this._value = value;
        this._humanReadable = humanReadable;
      };

      Enum.prototype = {
        key: function() {
          return this._key;
        },

        value: function() {
          return this._value;
        },

        humanReadable: function() {
          return this._humanReadable;
        }
      };

      var enums = _.map(entries, function(entry, key) {
        Utils.assert(entry.value);
        var value = entry.value;
        var humanReadable = entry.humanReadable || value;

        return new Enum(key, value, humanReadable);
      });

      var enumsByValue = _.object(_.map(enums, function(enumObj) {
        return [enumObj.value(), enumObj];
      }));

      var enumsByKey = _.object(_.map(enums, function(enumObj) {
        return [enumObj.key(), enumObj];
      }));

      Enum.all = function() {
        return enums;
      };

      Enum.forValue = function(value) {
        var enumObj = enumsByValue[value];
        Utils.assert(enumObj, 'Encountered unexpected enum value "' + value + '".');
        return enumObj;
      };

      _.extend(Enum, enumsByKey);

      return Enum;
    }
  };

  return Enums;
}]);

'use strict';

/**
 * APIObjects. Factory for all domain objects given only API json to work with.
 * Example of domain objects: Series, InvestmentApplication, Offering, etc.
 *
 * Members:
 *   .createIncludedObjectsDB(httpResponse) -- Returns an ObjectDB populated with
 *     All of the correct domain objects in the HTTP response data payload's "included" array.
 *
 *     For example, if the array contained [{ 'id': 'series:1' }], then this function
 *     would return an ObjectDB matching 'series:1' -> (an instance of Deals.series)
 *
 *     Params:
 *       httpResponse -- An http response produced by the $http service.
 *
 *   .createOrUpdateDBWithResponse(httpResponse, ObjectDB) -- Returns an ObjectDB populated
 *     with all of the correct domain objects in the following fields of the  HTTP response
 *     data payload: record, records, included. It returns the following structure:
 *     {
 *       records: <Array of APIObjects (e.g. Series) (or undefined if not present in response)>
 *       record: <APIObject (or undefined if not present in response)>
 *       db: <ObjectDB populated with records, record, and included>
 *     }
 */
angular.module('myApp.services').service(
'APIObjects',
["$injector", "Utils", "ObjectDBs", function($injector, Utils, ObjectDBs) {

  var objectFactoriesByIdSpace;

  var populateObjectFactories = function() {
    // Use the injector to get specific object services
    // in order to avoid circular dependency.
    var Deals = $injector.get('Deals');
    var Docs = $injector.get('Docs');
    var Brokerages = $injector.get('Brokerages');
    var Investments = $injector.get('Investments');

    // The registry that allows us to create a domain object from
    // its JSON representation. As new domain objects are created register
    // their factories here. A factory must take only 1 argument: the json
    // received from the API.
    objectFactoriesByIdSpace = {
      'company': Deals.company,
      'doc': Docs.instance,
      'security': Deals.security,
      'offering': Deals.offering,
      'access': Deals.access,
      'series': Deals.series,
      'investment_packet_doc': Deals.investmentPacketDoc,
      'brokerage': Brokerages.create,
      'investment': Investments.create,
      'series_participation': Investments.seriesParticipation,
      'investment_application': Investments.investmentApplication,
      'investment_application_progress': Investments.investmentApplicationProgress,
      'signature_packet': Investments.signaturePacket
    };
  };

  var APIObjects = {
    createIncludedObjectsDB: function(httpResponse) {
      Utils.assert(httpResponse.data && httpResponse.data.included, 'httpResponse object must have a "data.included" field.');

      var apiObjects = APIObjects._createMany(httpResponse.data.included);
      return ObjectDBs.temporary().put(apiObjects);
    },

    createOrUpdateDBWithResponse: function(httpResponse, db) {
      Utils.assert(httpResponse.data, 'httpResponse object must have a "data" field. That field should also have "included", "records", and/or "record" objects.');
      db = db || ObjectDBs.temporary();
      var includedObjects = httpResponse.data.included;
      var recordsJson = httpResponse.data.records;
      var recordJson = httpResponse.data.record;
      var records;
      var record;

      if (includedObjects) {
        db.put(APIObjects._createMany(includedObjects));
      }

      if (recordsJson) {
        records = APIObjects._createMany(recordsJson);
        db.put(records);
      }

      if (recordJson) {
        record = APIObjects._create(recordJson);
        db.put(record);
      }

      return {
        record: record,
        records: records,
        db: db
      };
    },

    //
    // Protected members
    //
    _create: function(apiJson) {
      Utils.assert(apiJson.id, 'All API objects must have an id field.');
      if (!objectFactoriesByIdSpace) {
        populateObjectFactories();
      }

      var idSpace = Utils.resourceIdSpace(apiJson.id);
      var factory = objectFactoriesByIdSpace[idSpace];
      Utils.assert(factory, 'No factory found for resource type: ' + idSpace);

      return factory(apiJson);
    },

    _createMany: function(apiJsons) {
      return _.map(apiJsons, function(apiJson) {
        return APIObjects._create(apiJson);
      });
    }
  };

  return APIObjects;
}]);

'use strict';

/**
 * MoneyAmount. A representation of a specific amount of currency.
 *
 * Members:
 *   .amount() -- the numerical amount of money represented by the MoneyAmount.
 *
 *   .symbol() -- The symbol of the currency being represented (e.g. '$')
 *
 *   .asLargeCurrency() -- A string representation that is suitable for large numbers
 *     such as company valuations. (e.g. '$1 Million', '$1.55 Billion')
 *
 *   .asPreciseCurrency() -- A string representation that is suitable for smaller numbers
 *     such as a company's price-per-share (e.g. '$10.22')
 */

/**
 * MoneyAmounts. A factory for creating MoneyAmount instances.
 *
 * Members:
 *   .create(config) -- Create a MoneyAmount.
 *     Config should be of the form:
 *       .amount -- the numerical amount of money represented
 *       .currency -- the currency code (e.g. 'USD') of the money represented
 */
angular.module('myApp.services').service(
'MoneyAmounts',
["$filter", "Utils", function($filter, Utils) {
  //
  // Currency
  //
  var currenciesByCode = {
    'USD': {
      symbol: '$',
    },

    'EUR': {
      symbol: '€'
    }
  };

  //
  // MoneyAmount
  //
  var MoneyAmount = function(apiMoney) {
    this._apiMoney = apiMoney;
  };

  MoneyAmount.prototype = {
    amount: function() {
      return this._apiMoney.amount;
    },

    symbol: function() {
      return this._currency().symbol;
    },

    asLargeCurrency: function() {
      return this._currency().symbol + $filter('largecurrency')(this._amount());
    },

    asPreciseCurrency: function() {
      return this._currency().symbol + $filter('number')(this._amount(), 2);
    },

    _currency: function() {
      var currency = currenciesByCode[this._apiMoney.currency];
      Utils.assert(currency, 'Currency type "' + this._apiMoney.currency + '" is not supported.');
      return currency;
    },

    _amount: function() {
      return this._apiMoney.amount;
    }
  };

  var MoneyAmounts = {
    create: function(apiMoney) {
      return new MoneyAmount(apiMoney);
    },

    usd: function(amount) {
      return new MoneyAmount({amount: amount, currency: 'USD'});
    }
  };

  return MoneyAmounts;
}]);

'use strict';

/**
 * User. A user from the /users API.
 *
 * Members:
 *   .id() -- return the user's ID (e.g. "user:10")
 *
 *   .phone() -- return the user's phone number, or undefined if the user has none.
 *
 *   .firstName() -- return the user's first name
 *
 *   .lastName() -- return the user's last name
 *
 *   .fullName() -- return the user's full name
 *
 *   .employedBrokerIds() -- return an array of the IDs of brokers registered for
 *     this client on our platform.
 *
 *   .administratedBrokerageIds() -- return an array of the IDs of brokerages this
 *     client directly administrates on our platform.
 *
 *   .brokerId() -- returns the user's brokerage ID if he/she has one
 *     otherwise returns undefined.
 *
 *   .fullNameTokens() -- return the user's full name as an array of space-delimited tokens.
 *      e.g. given that a user's name is "Joey Bloey", this returns ["Joey", "Bloey"]
 *
 *   .email() -- return the user's email address.
 *
 *   .investors() -- the array of Investors to which this user belongs.
 *     See Investors.js for more info.
 */

/**
 * AuthorizedUser. The currently authorized user.
 *
 * Persisted Version History:
 *   1 -- ?
 *   2 -- ?
 *   3 -- ?
 *   4 -- Add created_at to the python API, and use it to make fingerprint
 *        for all related calls.
 *
 * Members (in addition to all members provided on User):
 *   .hasPermission(perm, config) -- return true that the user has a particular site permission
 *     e.g. if (autherUser.hasPermission('can_administrate_site'))) { ... }
 *     perm -- a string (e.g. 'can_administrate_site')
 *     config -- an object configuring permission lookup. It is shaped like this:
 *       {
 *         forceSuperuserAccess: true // optional. boolean. defaults to true. gives superusers all permissions.
 *       }
 *
 *   .isSuperuser() -- return true that the user is a site superuser.
 *
 *   .homeUrl() -- return the user's home-page URL.
 *     This is the link that should be used on the CVC logo.
 *
 *   .refresh() -- returns a promise that resolves after the API has
 *     been queried for the current representation of the AuthorizedUser,
 *     and that representation has been stored away using Users.setAuthorized.
 *
 *   .isRegistrationComplete() -- return true that the user has completed registration
 *
 *   .authorizationHeader() -- return a valid Authorization header for the authed user.
 *
 *   .completedTours() -- return an array of Tour IDs that have been completed by the user
 *     (see Tours.js for more about Tours)
 *
 *   .setting(key) -- return the value for a particular Users.Settings key.
 *
 *   .putSetting(key, value) -- Given a key from Users.Settings, return a promise that
 *     resolves once the back-end has stored the key's new value. The AuthorizedUser with
 *     that new settings value will be set as the current AuthorizedUser before the promise
 *     resolves.
 *
 *   .fingerprint() -- return a string unique to this user. For use in fingerprinting
 *     API calls, to stop sensitive browser-cached responses from being accessible to people who are not logged in.
 *
 *   .isImpersonatedByAdmin() -- return true that the acting user is actually an admin
 *      impersonating the user for purposes of customer service or QA.
 *
 *   .requiresVersionUpgrade() -- return true that the persisted user has an old,
 *      incompatible persisted data version.
 */

/**
 * Users. A factory for all users. Provides accessors for the current authorized user as well as other
 * users accessible via the /users endpoint.
 *
 * Members:
 *   .authorized() -- return the currently authorized AuthorizedUser
 *
 *   .setAuthorized(apiJson, authInfo) -- set the currently authorized user.
 *     apiJson -- the Users json from our API.
 *     authInfo -- the authInfo object assembled in our login code.
 *
 *   .getById(id) -- return a promise to the User with the specified id.
 *     NOTE: this is only currently implemented in the back-end for the special
 *     identifier 'me', which always returns the currently authorized User.
 *
 *   .query(query) -- return a promise to the set of Users matching the query
 *     query -- query-string params for hitting the /users endpoint.
 *     Examples:
 *       Users.query({q:'brokerage_admins', brokerage:'brokerage:10'})
 *
 *   .Errors -- an object containing the errors this service can produce
 *     in the rejected case of any returned promise.
 *     Values:
 *       BAD_CREDENTIALS -- the call failed due to bad credentials.
 *       UNKNOWN -- the call failed for unknown reasons.
 *
 *   .Groups -- An object containing the various groups a user can belong to.
 *     Members:
 *       .USERS
 *       .BROKERAGE_ADMINS
 *       .INVESTORS
 *       .COMPANY_REPS
 */

/**
 * For all asynchronous methods here, the promise will reject with the
 * following signature if it fails: reject(Error, response), where Error
 * is a member of Users.Errors, and response is angular's version of the
 * HTTP response.
 */
angular.module('myApp.services').service(
'Users',
["localStorageService", "api2", "URLS", "$injector", "$q", "Crypto", "Investors", "Utils", "Enums", function(localStorageService, api2, URLS, $injector, $q, Crypto, Investors, Utils, Enums) {
  var $http = function() { return $injector.get('$http'); };
  var groups = {
    USERS: 'users',
    BROKERAGE_ADMINS: 'brokerage_admins',
    INVESTORS: 'investors',
    COMPANY_REPS: 'company_reps'
  };
  var CURRENT_AUTH_USER_VERSION = 4;
  var permissionPredicates = {
    'can_administrate_brokerages': function(user) {
      return user.isInGroup(groups.BROKERAGE_ADMINS);
    },
    'can_administrate_site': function(user) {
      return user.isSuperuser();
    },
    'can_create_brokerages': function(user) {
      return user.isSuperuser();
    },
    'can_view_investor_dashboard': function(user) {
      return user.isInGroup(groups.INVESTORS) && user.isRegistrationComplete();
    },
    'can_administrate_company': function(user) {
      return user.isInGroup(groups.COMPANY_REPS) && user.isRegistrationComplete();
    },
    'can_invest': function(user) {
      return (user.isSuperuser() || (user.isRegistrationComplete() && user.isInGroup(groups.INVESTORS)));
    },
    'can_view_some_investments': function(user) {
      return user.isSuperuser() || user.isInGroup(groups.INVESTORS) || user.isInGroup(groups.BROKERAGE_ADMINS);
    },
    'can_use_site': function(user) {
      return user;
    }
  };

  var _assertValidSettingKey = function(key) {
    Utils.assert(key.value, 'Key must be one of the valid values of Users.Settings');
    Utils.assert(Users.Settings.forValue(key.value()), 'Key must be one of the valid values of Users.Settings');
  };

  function User(apiUser) {
    this._apiUser = apiUser;
  }

  User.prototype = {
    id: function() {
      return this._apiUser.id;
    },

    brokerId: function() {
      return this._apiUser.as_broker && this._apiUser.as_broker.id;
    },

    employedBrokerIds: function() {
      return this._apiUser.brokers || [];
    },

    firstName: function() {
      return this._apiUser.first_name;
    },

    lastName: function() {
      return this._apiUser.last_name;
    },

    fullName: function() {
      return this.firstName() + ' ' + this.lastName();
    },

    fullNameTokens: function() {
      return this.fullName().split(' ');
    },

    email: function() {
      return this._apiUser.email;
    },

    investors: function() {
      return _.map(this._apiUser.investor_relations || [], function(investorRelation) {
        return Investors.create(investorRelation.investor);
      });
    },

    phone: function() {
      var rawPhone = this._apiUser.phone_number;
      var indexOfExtension;
      var phoneWithoutExtension;
      var hasNoExtension;
      var isEmptyPhone;
      if (rawPhone) {
        indexOfExtension = rawPhone.indexOf('ext');
        hasNoExtension = (rawPhone.length - indexOfExtension) === 3;
        if (hasNoExtension) {
          phoneWithoutExtension = rawPhone.substring(0, indexOfExtension);
          isEmptyPhone = phoneWithoutExtension === '' || phoneWithoutExtension === 'undefined';
          return !isEmptyPhone?  phoneWithoutExtension: undefined;
        } else {
          return rawPhone;
        }
      }
    },

    administratedBrokerageIds: function() {
      return this._apiUser.administrated_brokerages || [];
    }
  };

  function AuthorizedUser(apiUser, authInfo, metaData) {
    this._apiUser = apiUser;
    this._authInfo = authInfo;
    this._metaData = metaData;
  }

  AuthorizedUser.prototype = {
    id: User.prototype.id,
    brokerId: User.prototype.brokerId,
    firstName: User.prototype.firstName,
    lastName: User.prototype.lastName,
    fullName: User.prototype.fullName,
    fullNameTokens: User.prototype.fullNameTokens,
    email: User.prototype.email,
    phone: User.prototype.phone,
    employedBrokerIds: User.prototype.employedBrokerIds,
    administratedBrokerageIds: User.prototype.administratedBrokerageIds,
    investors: User.prototype.investors,

    hasPermission: function(permissionName, _config) {
      var config = angular.extend({forceSuperuserAccess: true}, _config || {});
      var superuserOverride = config.forceSuperuserAccess && this.isSuperuser();
      var permissionFunc = permissionPredicates[permissionName];

      if (permissionFunc) {
        return superuserOverride || permissionFunc(this);
      } else {
        throw 'Not a valid permission: ' + permissionName;
      }
    },

    isSuperuser: function() {
      return this.isInGroup('admins');
    },

    completedTours: function() {
      var tours = this.setting(Users.Settings.TOURS_COMPLETED) || [];

      return angular.copy(tours);
    },

    setting: function(key) {
      _assertValidSettingKey(key);
      return this._settings()[key.value()];
    },

    putSetting: function(key, value) {
      _assertValidSettingKey(key);
      var self = this;
      var newApiUser = angular.copy(this._apiUser);

      newApiUser.settings[key.value()] = value;
      return $http().patch(api2.users() + '/me', newApiUser).then(
        function (response) {
          var responseApiUser = response.data;
          Users.setAuthorized(responseApiUser, self._authInfo);
          return Users.authorized();
        },
        Users._adaptErrors
      );
    },

    isRegistrationComplete: function() {
      return this._apiUser.is_registration_complete;
    },

    authorizationHeader: function() {
      return 'Bearer ' + this._authInfo.accessToken;
    },

    fingerprint: function() {
      return Crypto.md5('saltysaltysaltsalt' + this.id() + this._apiUser.created_at);
    },

    homeUrl: function() {
      // This function makes me very, very sad. Nobody knows what this step2 / step1 status
      // BS is about, but I have just copied the logic from utilCtrl.js so that it can be
      // encapsulated.
      var path = '';
      if (this.isInGroup(groups.INVESTORS)) {
        switch(this._deprecatedStatus()) {
          case 'step2_created':
            path = URLS.INVESTOR_LANDING;
            break;
          case 'step2_approved':
            path = URLS.INVESTOR_DASHBOARD;
            break;
        }
      } else if (this.isInGroup(groups.COMPANY_REPS)) {
        switch(this._deprecatedStatus()) {
          case 'step1_approved':
            break;
          case 'step2_created':
            path = URLS.PREVIEW_COMPANY + this._company().slug;
            break;
          default:
            path = URLS.COMPANY + this._company().slug;
        }
      } else if (this.isSuperuser()) {
        path = URLS.ADMIN_DASHBOARD;
      }

      return path;
    },

    refresh: function() {
      var self = this;
      return Users.getById('me').then(function(user) {
        Users.setAuthorized(user._apiUser, self._authInfo);
        return true;
      });
    },

    isImpersonatedByAdmin: function() {
      return this._authInfo.isImpersonation;
    },

    requiresVersionUpgrade: function() {
      return this._version() !== CURRENT_AUTH_USER_VERSION;
    },

    _version: function() {
      var hasPersistedVersion = this._metaData && this._metaData.version;
      if (!hasPersistedVersion) {
        return 1;
      } else {
        return this._metaData.version;
      }
    },

    _groups: function() {
      return this._apiUser.groups;
    },

    isInGroup: function(group_name) {
      return _.contains(this._groups(), group_name);
    },

    _deprecatedStatus: function() {
      return this._apiUser.deprecated_status;
    },

    _settings: function() {
      return this._apiUser.settings || {};
    },

    _company: function() {
      var companies = this._apiUser.companies || [];
      return companies[0];
    }
  };

  var Users = {
    /** Return the currently authorized user. */
    authorized: function() {
      var loginJson = localStorageService.get('user.api_json');
      if (loginJson) {
        return new AuthorizedUser(loginJson, loginJson._authInfo, loginJson._metaData);
      }
    },

    /** Set the currently authorized user */
    setAuthorized: function(apiJson, authInfo) {
      authInfo = angular.extend({isImpersonation: false}, authInfo);
      apiJson._authInfo = authInfo;
      apiJson._metaData = {
        version: CURRENT_AUTH_USER_VERSION
      };
      localStorageService.set('user.api_json', apiJson);
    },

    /** Return a promise to the set of users */
    query: function(query) {
      return $http().get(api2.users(), {params: query}).then(
        function(response) {
          var records = response.data.records;
          return _.map(records, function(apiUser) {
            return Users._create(apiUser);
          });
        },
        Users._adaptErrors
      );
    },

    getById: function(id) {
      return $http().get(api2.users() + '/' + id).then(
        function (response) {
          return Users._create(response.data);
        },
        Users._adaptErrors
      );
    },

    Errors: {
      BAD_CREDENTIALS: 'bad_credentials',
      UNKNOWN: 'unknown_error'
    },

    Settings: Enums.create({
      TOURS_COMPLETED: {value: 'tours_completed', humanReadable: 'The tour IDs that have been completed by the user'}
    }),

    Groups: groups,

    _create: function(apiJson) {
      return new User(apiJson);
    },

    _adaptErrors: function(response) {
      var errorCode = Users.Errors.UNKNOWN;
      if (response.status === 401) {
        errorCode = Users.Errors.BAD_CREDENTIALS;
      }

      return $q.reject(errorCode, response);
    }
  };

  return Users;
}])
.config(["$httpProvider", function($httpProvider) {
  //
  // Lifecycle hook. Intercept all API calls and add authorization info if logged in.
  //
  var authInterceptor = ['Users', 'api2', function(Users, api2) {
    return {
      request: function(config) {
        var headers = config.headers || {};
        var isApiRequest = config.url.indexOf(api2.host()) === 0;
        var authedUser = Users.authorized();

        if (isApiRequest && authedUser) {
          if (config.method === 'GET') {
            // For GET requests, which could be browser-cached, always provide
            // the user's fingerprint so that another user on the same browser
            // will never accidentally be served the secure assets of the first user
            // due to a browser cache hit.
            config.params = config.params || {};
            config.params.fingerprint = authedUser.fingerprint();
          }
          headers.Authorization = authedUser.authorizationHeader();

          if (authedUser.isImpersonatedByAdmin()) {
            headers['X-Impersonate-User'] = authedUser.email();
          }
          config.headers = headers;
        }

        return config;
      }
    };
  }];

  $httpProvider.interceptors.push(authInterceptor);
}])
.run(["Users", "Auth", "$rootScope", "logging", function(Users, Auth, $rootScope, logging) {
  //
  // Lifecycle hook. On each route change attempt to refresh the currently
  // authorized user if exists.
  //
  // If the refresh fails due to bad credentials then log the user out.
  //
  var log = logging.namespace('Users');
  $rootScope.$on('$routeChangeStart', function() {
    var authedUser = Users.authorized();

    if (authedUser) {
      // Update the authorized user with the current representation from the server
      authedUser.refresh().catch(function(error) {
        // If the update failed due to bad credentials, just log us out.
        if (error === Users.Errors.BAD_CREDENTIALS) {
          log('Refreshing the current user failed due to bad credentials. Logging out.');
          Auth.logout();
        }
      });
    }
  });
}]);

'use strict';

/**
 * Form. A form from the /forms API.
 *
 * Members:
 *   .id() -- return the Form's ID (e.g. 'form:1')
 *
 *   .save() -- Save the current state of the form to the /forms API, returning a promise to the same
 *     form with errors and state updated by the server.
 *
 *   .delete() -- Delete the form from the forms API.
 *     returns a promise that resolves when the form has been successfully
 *
 *   .errors() -- this form's errors, indexed by field name. Has 'non_field_errors' as the field name
 *     for errors unassociated with specific fields
 *
 *   .data() -- this form's data, indexed by field name
 *
 *   .isComplete() -- return true that this form has been successfully executed by the server.
 *
 *   .withSubmittedState() -- returns a copy of this form in the SUBMITTED state.
 *
 *   .withState(state) -- returns a copy of this form in the given state.
 *
 *   .withData(data) -- returns a copy of this form that contains the provided data rather
 *     than the previous data.
 *
 *   .isValid() -- returns true that the form has no errors.
 *
 *   .isSubmittable() -- returns true that the form has no errors and is a work in progress
 *
 *   .isWorkInProgress() -- returns true that the form is neither fully executed nor in a pristine state
 *
 *   .description() -- return a human-readable description of the form.
 */

/**
 * Forms. A factory for all Forms. Provides accessors to get Form instance from the /forms endpoint.
 *
 * Members:
 *   .getNewOrIncomplete(formType, formContext) -- Returns a promise to a Form instance with the given
 *     type and context.
 *
 *   .instance(apiForm) -- Return a new Form instance given an API json object from
 *     the /forms API.
 *
 *   .States -- Returns the FORM_STATES dictionary containing all
 *     states in the forms API state machine:
 *       .NEW('new') -- The form is in its initial, pristine state.
 *       .IN_PROGRESS('in_progress') -- The form has been edited from its pristine state
 *       .SUBMITTED('submitted') -- The form is being submitted for execution
 *       .BEING_CORRECTED('being_corrected') -- The form was submitted for exeuction,
 *         but additional correctable errors were found upon execution.
 *       .COMPLETE('complete') -- The form has been completed. Feel free to continue
 *         whatever user flow the form appeared in.
 */
angular.module('myApp.services').service(
'Forms',
["logging", "$http", "api2", function(logging, $http, api2) {
  var log = logging.namespace('Forms');

  var _formResponseToForm = function(apiFormRequest) {
    return Forms.instance(apiFormRequest.data);
  };
  var FORM_STATES = {
    NEW: 'new',
    IN_PROGRESS: 'in_progress',
    SUBMITTED: 'submitted',
    BEING_CORRECTED: 'being_corrected',
    COMPLETE: 'complete'
  };

  var Form = function(apiForm) {
    this._apiForm = apiForm;
  };

  Form.prototype = {
    //
    // Public API
    //
    id: function() {
      return this._apiForm.id;
    },

    save: function() {
      return $http.put(this._url(), this._apiForm).then(_formResponseToForm);
    },
    delete: function() {
      return $http.delete(this._url()).then(function() { return; });
    },
    errors: function() {
      return this._apiForm.errors || {};
    },
    data: function() {
      return this._apiForm.data;
    },
    isComplete: function() {
      return this._status() === FORM_STATES.COMPLETE;
    },
    withSubmittedState: function() {
      var newApiForm = angular.copy(this._apiForm);
      newApiForm.status = FORM_STATES.SUBMITTED;

      return Forms.instance(newApiForm);
    },
    withState: function(state) {
      var newApiForm = angular.copy(this._apiForm);
      newApiForm.status = state;

      return Forms.instance(newApiForm);
    },
    withData: function(newData) {
      var newApiForm = angular.copy(this._apiForm);
      newApiForm.data = newData;

      return Forms.instance(newApiForm);
    },
    isValid: function() {
      var numErrors = Object.keys(this.errors()).length;
      return numErrors === 0;
    },
    isSubmittable: function() {
      return this._status() !== FORM_STATES.NEW && this.isValid();
    },
    description: function() {
      return 'Form<id=' + this._id() +
        ', type=' + this._type() +
        ', context=' + this._context() +
        ', status=' + this._apiForm.status +
        ', created=' + this._apiForm.created_at +
        ', modified=' + this._apiForm.modified_at +
        '>';
    },
    isWorkInProgress: function() {
      return this._status() === FORM_STATES.IN_PROGRESS || this._status() === FORM_STATES.BEING_CORRECTED;
    },

    //
    // Private API
    //
    _url: function() {
      return api2.forms() + '/' + this._id();
    },
    _id: function() {
      return this._apiForm.id;
    },
    _type: function() {
      return this._apiForm.form_type;
    },
    _context: function() {
      return this._apiForm.context;
    },
    _status: function() {
      return this._apiForm.status;
    }
  };

  var Forms = {
    getNewOrIncomplete: function(formType, formContext) {
      var queryParams = {form_type: formType};
      if (formContext) {
        queryParams.context = formContext;
      }
      return $http.post(api2.forms(), queryParams).then(_formResponseToForm);
    },

    instance: function(apiForm) {
      return new Form(apiForm);
    },

    States: FORM_STATES
  };

  return Forms;
}]);

'use strict';

/**
 * CVCFile. Abstraction of an API File record.
 * This is returned by any method that doesn't return raw.
 *
 * Members:
 *   .id() -- the file's id (e.g. 'file:1')
 *
 *   .filename() -- the file's name
 *
 *   .contentType() -- the file's mime type
 *
 *   .raw() -- a promise to the file's raw representation (RawFile)
 */

/**
 * RawFile. Abstraction of the raw bytes of an API File
 * This is returned by any method that returns raw.
 *
 * Members:
 *   .mimeType() -- the mime type of a file
 *
 *   .isImage() -- return true that this file is an image
 *
 *   .size() -- the file's size in bytes
 *
 *   .dataUri() -- the file's contents as a data uri
 *
 *   .base64() -- the file's contents base64 encoded.
 *
 *   .uint8Array() -- the file's contents as a TypedArray of type Uint8Array
 */

/**
 * Files. Factory for the citizen.vc files API.
 *
 * Members:
 *   .upload(file) -- upload a file.
 *       The file should be one that is selected from a file-type <input>.
 *       Returns promise to a CVCFile. This promise also has a '.progress' option
 *       that accepts progress updates.
 *
 *   .get(fileId, config) -- get a file by id.
 *       Thin wrapper over /api/2/files/<file_id>.
 *       pass any additional query params into config as {params: {'param1': 'value1'}}
 *       Returns a CVCFile if non-raw format was requested. Otherwise returns RawFile.
 *
 *   .isFileId(id) -- return true that the provided input is a file ID
 *      (e.g. it matches the pattern: 'file:<something>')
 *
 *   .Formats -- An object with the various ACCEPT formats supported by
 *       the files API.
 */
angular.module('myApp.services').service(
'Files',
['api2', '$http', '$upload', 'logging',
function(api2, $http, $upload, logging) {
  var log = logging.namespace('Files');
  function CVCFile(apiFile) {
    this._apiFile = apiFile;
  }

  function RawFile(rawResponse) {
    this._rawResponse = rawResponse;
  }

  CVCFile.prototype = {
    id: function() { return this._apiFile.id; },

    contentType: function() { return this._apiFile.content_type; },

    filename: function() { return this._apiFile.filename; },

    raw: function() {
      return Files.get(this.id(), {
        params: {fmt: 'application/vnc.citizenvc.raw'}
      });
    }
  };

  RawFile.prototype = {
    mimeType: function() { return this._rawResponse.headers('Content-Type'); },

    size: function() { return this._rawResponse.headers('Content-Length'); },

    dataUri: function() {
      return 'data:' + this.mimeType() + ';base64,' + this.base64();
    },

    base64: function() {
      var binary = '';
      var bytes = this.uint8Array();
      var len = bytes.byteLength;
      for (var i = 0; i < len; i++) {
        binary += String.fromCharCode( bytes[ i ] );
      }
      return window.btoa(binary);
    },

    uint8Array: function() {
      return new Uint8Array(this._rawResponse.data);
    },

    isImage: function() {
      var mimeType = this.mimeType() || '';
      return mimeType.indexOf('image') > -1;
    }
  };

  var Files = {
    upload: function(file) {
      var uploadPromise = $upload.upload({
        url: api2.files(),
        file: file
      });
      var resultPromise = uploadPromise.then(function(success) {
        return new CVCFile(success.data);
      });

      var progress = function(fn) {
        uploadPromise.progress(fn);
        return resultPromise;
      };

      resultPromise.progress = progress;
      return resultPromise;
    },

    get: function(id, config) {
      config = angular.extend({params: {}}, config);
      var isRawRequest = config.params.fmt && config.params.fmt.indexOf('raw') != -1;
      if (isRawRequest) {
        config.responseType = 'arraybuffer';
      }
      return $http.get(api2.files() + '/' + id, config).then(function(response) {
        return isRawRequest? new RawFile(response): new CVCFile(response.data);
      });
    },

    isFileId: function(id) {
      return id.indexOf('file:') === 0;
    },

    Formats: {
      RAW: 'application/vnd.citizenvc.raw',
      WATERMARKED: 'application/vnd.citizenvc.raw.watermarked'
    }
  };

  return Files;
}]);

'use strict';

/**
 * Brokerage. A thin wrapper over the API object at /brokerages.
 * Members:
 *   .id() -- the brokerage's id (e.g. 'brokerage:10')
 *
 *   .logo() -- the brokerage logo's file ID
 *
 *   .name() -- the brokerage's name (e.g. "Joseph Gunnar & Co")
 *
 *   .slug() -- the brokerage's slug (e.g. "joseph-gunnar-co")
 *
 *   .refresh() -- return a promise to the latest version of this brokerage
 *     from the API.
 *
 *   .description() -- return a human-readable text description of the Brokerage.
 *
 *   .isDefault() -- return true that this is the "default" brokerage (citizen.vc in any production env)
 *
 *   .removeAdmin(userIdToRemove) -- returns a promise to a copy of this Brokerage
 *     after the specified user ID has been removed from being an admin.
 *     If the promise fails then assume that the user has not been
 *     removed.
 *
 *   .removeBroker(brokerIdToRemove) -- returns a promise to a copy of this brokerage
 *     after the specified broker ID (e.g. 'broker:1') has been removed from the brokerage.
 *     If the promise fails then assume the broker has not been removed.
 *
 *   .removeClient(userIdToRemove) -- returns a promise to a copy of this Brokerage
 *     after the specified user ID has been removed from being a client.
 *     If the promise fails then assume that the user has not been removed.
 *
 *   .brandMatches(Brokerage) -- return true that the provided brokerage
 *     matches the brand of the receiving brokerage. The brand consists
 *     of the brokerage's logo and name.
 *
 *   .admins() -- a promise to the set of Users that administrate this
 *     brokerage.
 *
 *   .isAdmin(userId) -- returns true that the provided userId (e.g. 'user:1') is an administrator
 *     of this brokerage.
 *
 *   .brokers() -- a promise to the set of Users that are brokers of
 *     this brokerage
 *
 *   .clients() -- a promise to the set of Users that are clients of
 *     this brokerage.
 *
 *   .onBrandChange($scope, fn(Brokerage)) -- register a function on the scope to be called
 *     when the specified brokerage's brand changes (e.g. its name or its logo).
 *     The callback receives a version of the new Brokerage with the updated brand.
 *
 *   .announceHasNewBrand() -- announce to anybody listening that this brokerage's
 *     brand (e.g. its name and logo) has changed.
 *
 */

/**
 * Brokerages. A Factory for all brokerages.
 *
 * API:
 *   .administrated() -- return a promise to an array of Brokerages
 *     administrated by the current user.
 *
 *   .all() -- return a promise to all Brokerages that exist. This call
 *     will only be honored by the API if made by a citizen.vc superuser.
 *
 *   .allWithUserAsClient(config) -- return a promise to all Brokerages of which
 *     the currently authorized user is client.
 *     Arguments:
 *       config -- an object of the following structure
 *         .db - Optional. An ObjectDB. If provided, the db will be updated with the
 *           objects returned from this request before the promise resolves.
 *
 *   .getByUniqueKey(key, config) -- return a promise to the Brokerage matching the target
 *     slug (joseph-gunnar-co), or ID (1, brokerage:1).
 *     Args:
 *       key -- ID or slug of the target brokerage
 *       config -- optional
 *         .db -- An ObjectDB. If populated, the method will stuff the retrieved Brokerage into the ObjectDB.
 *
 *   .active() -- return a promise to the "active" brokerage. For brokerage
 *     administrators this will be the first brokerage in their list of brokerages
 *     administrated. For clients this is the first brokerage in their list of brokerages
 *     retained. We should eventually cache this locally and have it user-selectable
 *     as a performance and UX improvement.
 *
 *   .onActiveChange($scope, fn(Brokerage)) -- Registers a function on the
 *     provided scope to be called when a new brokerage has been made active.
 *
 *   .onAdministrationSelect($scope, fn(Brokerage)) -- Registers a function
 *     on the provided scope to be called when a new brokerage is being administered.
 */
angular.module('myApp.services').service(
'Brokerages',
["api2", "$http", "Users", "$rootScope", "logging", "$q", "APIObjects", function(api2, $http, Users, $rootScope, logging, $q, APIObjects) {
  var log = logging.namespace('Brokerages');
  var BROKERAGE_SELECTED_FOR_ADMINISTRATION = 'Brokerages.beingAdministrated';

  var removeIdFromUserCollection = function(userIdToRemove, userCollection) {
    var userIds = _.map(userCollection, function(user) { return user.id(); });
    var newUserIds = _.filter(userIds, function(userId) {
      return userId !== userIdToRemove;
    });

    return newUserIds;
  };

  function Brokerage(_apiBrokerage) {
    this._apiBrokerage = _apiBrokerage;
  }

  Brokerage.prototype = {
    id: function() { return this._apiBrokerage.id; },

    name: function() { return this._apiBrokerage.name; },

    logo: function() { return this._apiBrokerage.logo; },

    slug: function() { return this._apiBrokerage.slug; },

    description: function() {
      return 'Brokerage<id=' + this.id() +
        ', name=' + this.name() +
        '>';
    },

    isDefault: function() {
      return this._apiBrokerage.is_default_brokerage;
    },

    admins: function() {
      return Users.query({q: 'brokerage_admins', brokerage: this.id()});
    },

    isAdmin: function(userId) {
      return _.contains(this._apiBrokerage.admins, userId);
    },

    brokers: function() {
      return Users.query({q: 'brokers', brokerage: this.id()});
    },

    clients: function() {
      return Users.query({q: 'clients', brokerage: this.id()});
    },

    removeClient: function(clientIdToRemove) {
      log('Removing client ' + clientIdToRemove);
      var self = this;

      return this.clients().then(function(clients) {
        var newClientIds = removeIdFromUserCollection(clientIdToRemove, clients);
        return self._copy({ clients: newClientIds} )._save();
      });
    },

    removeAdmin: function(userIdToRemove) {
      log('Removing admin ' + userIdToRemove);

      var self = this;
      return this.admins().then(function(admins) {
        log('Retrieved current list of admins');
        var newAdminIds = removeIdFromUserCollection(userIdToRemove, admins);

        return self._copy({ admins: newAdminIds})._save();
      });
    },

    onBrandChange: function(scope, callback) {
      scope.$on(this._brandChangedSignal(), function(evt, updatedBrokerage) {
        callback(updatedBrokerage);
      });
    },

    announceHasNewBrand: function() {
      $rootScope.$broadcast(this._brandChangedSignal(), this);
    },

    removeBroker: function(brokerIdToRemove) {
      log('Copying brokerage ' + this.name() + ' without broker ' + brokerIdToRemove);
      var self = this;

      return this.brokers().then(function(brokerUsers) {
        log('Retrieved current list of brokers');
        var newBrokerIds = _
          .filter(brokerUsers, function(brokerUser) {
            return brokerUser.brokerId() !== brokerIdToRemove;
          })
          .map(function(brokerUser) {
            return brokerUser.brokerId();
          });

        return self._copy({ brokers: newBrokerIds })._save();
      });
    },

    refresh: function() {
      return Brokerages.getByUniqueKey(this.id());
    },

    brandMatches: function(otherBrokerage) {
      return this.name() === otherBrokerage.name() && this.logo() === otherBrokerage.logo();
    },

    //
    // Protected API
    //
    _copy: function(newValues) {
      var newApiBrokerage = angular.copy(this._apiBrokerage);
      angular.extend(newApiBrokerage, newValues);

      return Brokerages.create(newApiBrokerage);
    },

    _save: function() {
      return $http.patch(api2.brokerages() + '/' + this.id(), this._apiBrokerage)
        .then(function(response) {
          log('Saved updates to the brokerage');
          return Brokerages.create(response.data);
        });
    },

    _brandChangedSignal: function() {
      return 'Brokerages.' + this.id() + '.brandChanged';
    }
  };

  var Brokerages = {
    administrated: function() {
      return $http.get(api2.brokerages(), {params:{relationship: 'admin'}})
        .then(function(response) {
          return Brokerages.createMany(response.data.records);
        });
    },

    all: function() {
      return $http.get(api2.brokerages()).then(function(response) {
        return Brokerages.createMany(response.data.records);
      });
    },

    allWithUserAsClient: function(config) {
      config = config || {};
      return $http.get(api2.brokerages(), {params: {'relationship': 'client'}}).then(function(response) {
        var apiObjects = APIObjects.createOrUpdateDBWithResponse(response, config.db);
        return apiObjects.records;
      });
    },

    active: function() {
      return $q(function(resolve, reject) {
        var user = Users.authorized();
        var grabFirstBrokerage = function(brokerages) {
          var hasBrokerages = brokerages.length > 0;
          if (hasBrokerages) {
            resolve(brokerages[0]);
          } else {
            reject('no_brokerages');
          }
        };
        var brokeragesPromise;

        if (user.isInGroup(Users.Groups.BROKERAGE_ADMINS)) {
          log('User was brokerage admin. Querying administrated brokerages as active.');
          brokeragesPromise = Brokerages.administrated();
        } else {
          log('User was not a brokerage admin. Querying brokerages to which user is client.');
          brokeragesPromise = Brokerages.allWithUserAsClient();
        }

        return brokeragesPromise.then(grabFirstBrokerage);
      });
    },

    getByUniqueKey: function(key, config) {
      return $http.get(api2.brokerages() + '/' + key).then(function(response) {
        var brokerage = Brokerages.create(response.data);
        if (config && config.db) {
          config.db.put(brokerage);
        }

        return brokerage;
      });
    },

    onActiveChange: function(scope, callback) {
      return Brokerages.onAdministrationSelect(scope, callback);
    },

    onAdministrationSelect: function(scope, callback) {
      scope.$on(BROKERAGE_SELECTED_FOR_ADMINISTRATION, function(evt, newBrokerage) {
        callback(newBrokerage);
      });
    },

    // Factory method for creating a Brokerage from its API representation
    create: function(apiBrokerage) {
      return new Brokerage(apiBrokerage);
    },

    // Factory method for creating many Brokerages from an array of brokerage API representations
    createMany: function(apiBrokerages) {
      return _.map(apiBrokerages, function(apiBrokerage) {
        return Brokerages.create(apiBrokerage);
      });
    }
  };

  return Brokerages;
}]);

'use strict';

/**
 * SeriesParticipation. Represents any investment or attempted investment to
 * a Series (Deals.js).
 *
 * Members:
 *   .id() -- The participation's ID (e.g. 'series_participation:1')
 *
 *   .local(db) -- Return an object capable of walking this SeriesParticipation's relations
 *     using data in the provided ObjectDB.
 *
 *   .local(db).applications() -- Return all InvestmentApplications associated with this
 *     instance.
 *
 *   .local(db).activeApplication() -- Return the current active InvestmentApplication associated
 *     associated with this instance. Returns undefined if there is none.
 *
 *   .local(db).series() -- Return the series being participated in by the investing user.
 */

/**
 * Investment. A successful investment recorded on the platform (though
 * it could have been applied to and executed on-platform or off-platform)
 *
 * Members:
 *   .id() -- The investment's ID (e.g. 'investment:1')
 *
 *   .amount() -- The amount of the investment as a MoneyAmount.
 */

/**
 * InvestmentApplication. An investor's application to invest in a Series on-platform.
 *
 * Members:
 *   .id() -- The application ID (e.g. 'investment_application:1')
 *
 *   .local(db) -- Return an object capable of walking this application's relations
 *     using data in the provided ObjectDB.
 *
 *   .local(db).progress(config) -- Return an array of this application's
 *     InvestmentApplicationProgress. Returns all if config is not provided,
 *     otherwise filters by the parameters in config.
 *     Args:
 *       .config
 *         .eventType -- Optional Investments.ProgressType.
 *           ensures that only progress instances with the provided eventType are returned.
 *
 *   .local(db).progressExists(config) -- Return true that there is a result that matches
 *     the provided config. (see .local(db).progress(config))
 *
 *   .local(db).investingUserStartedForms() -- Return true if a progress type of
 *     INVESTING_USER_STARTED_FORMS exists in the application.
 *
 *   .local(db).investingUserSelectedEntity() -- Return true if a progress type of
 *     INVESTING_USER_SELECTED_ENTITY exists in the application.
 *
 *   .local(db).investingUserFilledInvestmentAmount() -- Return true if a progress type of
 *     INVESTING_USER_FILLED_INVESTMENT_AMOUNT exists in the application.
 *
 *   .local(db).investingUserSignedPacket() -- Return true if a progress type of
 *     INVESTING_USER_SIGNED_PACKET exists in the application.
 *
 *   .local(db).series() -- Return the Series being applied to in this application.
 *
 *   .local(db).participation() -- Return the SeriesParticipation this application is a part of.
 *
 *   .local(db).logProgress(eventType) -- Call the API to log a new event type, and return
 *     a promise to the copy of this InvestmentApplication. The db will be updated with the
 *     new application and progress representations before the promise resolves.
 *
 *   .local(db).company() -- Return the Company associated with this application's series.
 */

/**
 * InvestmentApplicationProgress. A single line-item in the event log of an application.
 *
 * Members:
 *   .id() -- This progress item's ID (e.g. 'investment_application_progress:10).
 *
 *   .eventType() -- This item's ProgressType.
 *
 *   .timestamp() -- A Date representing the moment in time that the event occurred.
 */

/**
 * SignaturePacket. The packet of documents for a user to sign.
 *
 * Members:
 *   .id() -- This packet's ID (e.g. 'signature_packet:1')
 */

/**
 * ProgressType. An enum representing every type of InvestmentApplicationProgress (e.g. ProgressType)
 *
 * Members:
 *   CREATED -- The investment was created
 *   INVESTING_USER_VIEWED_FORMS -- The investing user viewed the details about this investment (usually fundraise page)
 *   INVESTING_USER_STARTED_FORMS -- The investing user started the forms associated with this investment
 *   INVESTING_USER_SELECTED_ENTITY -- The investing user selected an entity to invest with (e.g. their family trust)
 *   INVESTING_USER_FILLED_INVESTMENT_AMOUNT -- The investing user chose an amount to invest
 *   INVESTING_USER_SIGNED_PACKET -- The investing user signed their SignaturePacket
 */

/**
 * Investments. A factory for many types relating to investment.
 *
 * Members:
 *   .getOrCreateParticipation(config) -- Get or create a SeriesParticipation for an investor. Return a
 *     promise to retrieved or created SeriesParticipation.
 *     Args:
 *       .config -- required object.
 *         .apiParams -- the API params to pass to the SeriesParticipation endpoint
 *           .series -- Required series ID (e.g. 'series:1')
 *         .db -- Optional ObjectDB. If provided, the DB will be populated with all objects in
 *           the request by the time the promise resolves.
 *
 *   .getOrCreateActiveApplication(config) -- Get or create the active application for a given series
 *     Return a promise to the retrieved or created InvestmentApplication.
 *     Args:
 *       .config -- (see .getOrCreateParticipation)
 *
 *   .ProgressType -- returns the ProgressType enum (see Enums.js for how these behave)
 *
 *   .seriesParticipation(json),
 *   .investment(json),
 *   .investmentApplication(json),
 *   .investmentApplicationProgress(json),
 *   .signaturePacket(json) --
 *     Factories for creating domain objects out of JSON. Mostly used by the APIObjects service.
 */
angular.module('myApp.services').service(
'Investments',
["Utils", "MoneyAmounts", "$http", "api2", "APIObjects", "Enums", "ObjectDBs", function(Utils, MoneyAmounts, $http, api2, APIObjects, Enums, ObjectDBs) {
  var assert = Utils.assert;

  //
  // SeriesParticipation
  //
  var SeriesParticipation = function(apiParticipation) {
    this._apiParticipation = apiParticipation;
  };

  SeriesParticipation.prototype = {
    id: function() {
      return this._apiParticipation.id;
    },

    local: function(db) {
      return new SeriesParticipationRelationsDB(this, db);
    },

    //
    // Protected API
    //
    _applicationIds: function() {
      return this._apiParticipation.applications;
    },

    _seriesId: function() {
      return this._apiParticipation.series;
    }
  };

  var SeriesParticipationRelationsDB = function(participation, db) {
    this._participation = participation;
    this._db = db;
  };

  SeriesParticipationRelationsDB.prototype = {
    applications: function() {
      var ids = this._participation._applicationIds();
      return this._db.get(ids, {strict: true});
    },

    activeApplication: function() {
      // TODO: this just grabs the first application. We should filter whether it is active or not.
      return _.first(this.applications());
    },

    series: function() {
      return this._db.get(this._participation._seriesId(), {strict: true});
    }
  };

  //
  // Investment
  //
  var Investment = function(apiInvestment) {
    this._apiInvestment = apiInvestment;
  };

  Investment.prototype = {
    id: function() {
      return this._apiInvestment.id;
    },

    amount: function() {
      return MoneyAmounts.create(this._apiInvestment.amount);
    }
  };

  //
  // InvestmentApplication
  //
  var InvestmentApplication = function(apiApplication) {
    this._apiApplication = apiApplication;
  };

  InvestmentApplication.prototype = {
    id: function() {
      return this._apiApplication.id;
    },

    local: function(db) {
      return new InvestmentApplicationRelationsDB(this, db);
    },

    //
    // Protected members
    //
    _progressIds: function() {
      return this._apiApplication.all_progress;
    },

    _participationId: function() {
      return this._apiApplication.participation;
    }
  };

  var InvestmentApplicationRelationsDB = function(investApp, db) {
    this._investApp = investApp;
    this._db = db;
  };

  InvestmentApplicationRelationsDB.prototype = {
    progress: function(config) {
      config = config || {};
      var progress = this._db.get(this._investApp._progressIds(), {strict: true});
      var eventTypeFilter = config.eventType;
      if (eventTypeFilter) {
        progress = _.filter(progress, function(item) {
          return item.eventType() === eventTypeFilter;
        });
      }

      return progress;
    },

    progressExists: function(config) {
      return this.progress(config).length > 0;
    },

    investingUserStartedForms: function() {
      return this.progressExists({eventType: ProgressType.INVESTING_USER_STARTED_FORMS});
    },

    investingUserSelectedEntity: function() {
      return this.progressExists({eventType: ProgressType.INVESTING_USER_SELECTED_ENTITY});
    },

    investingUserFilledInvestmentAmount: function() {
      return this.progressExists({eventType: ProgressType.INVESTING_USER_FILLED_INVESTMENT_AMOUNT});
    },

    investingUserSignedPacket: function() {
      return this.progressExists({eventType: ProgressType.INVESTING_USER_SIGNED_PACKET});
    },

    series: function() {
      return this.participation().local(this._db).series();
    },

    participation: function() {
      return this._db.get(this._investApp._participationId(), {strict: true});
    },

    company: function() {
      return this.series().local(this._db).company();
    },

    logProgress: function(eventType) {
      var self = this;
      var series = this.series();
      var request = $http.post(api2.investmentAppProgress(), {
        application: this._investApp.id(),
        event_type: eventType.value()
      });

      return request.then(function() {
        // Refresh the application
        var investmentGetParams = {
          apiParams: {
            series: series.id()
          },
          db: self._db
        };
        return Investments.getOrCreateActiveApplication(investmentGetParams).then(function(activeApp) {
          Utils.assert(activeApp.id() === self._investApp.id(), 'Expected the retrieved application after logProgress to be the same as the one whose progress was logged');

          return activeApp;
        });
      });
    }
  };

  //
  // InvestmentApplicationProgress
  //
  var InvestmentApplicationProgress = function(apiProgressItem) {
    this._apiProgressItem = apiProgressItem;
  };

  InvestmentApplicationProgress.prototype = {
    id: function() {
      return this._apiProgressItem.id;
    },

    eventType: function() {
      return ProgressType.forValue(this._apiProgressItem.event_type);
    },

    timestamp: function() {
      return new Date(this._apiProgressItem.created_at); // TODO: does javascript parse the date format that we ship down?
    }
  };

  var ProgressType = Enums.create({
    CREATED: {value: 'created', humanReadable: 'created'},
    INVESTING_USER_SIGNED_PACKET: {value: 'investing_user_signed_packet', humanReadable: 'Signing packet was completed'},
    INVESTING_USER_VIEWED_FORMS: {value: 'investing_user_viewed_forms', humanReadable: 'Investor viewed the investment application forms'},
    INVESTING_USER_STARTED_FORMS: {value: 'investing_user_started_forms', humanReadable: 'Investor started entering the investment application forms'},
    INVESTING_USER_SELECTED_ENTITY: {value: 'investing_user_selected_entity', humanReadable: 'Investor selected an investing entity'},
    INVESTING_USER_FILLED_INVESTMENT_AMOUNT: {value: 'investing_user_filled_investment_amount', humanReadable: 'Investor chose an investment amount'}
  });

  //
  // SignaturePacket
  //
  var SignaturePacket = function(apiPacket) {
    this._apiPacket = apiPacket;
  };

  SignaturePacket.prototype = {
    id: function() {
      return this._apiPacket.id;
    }
  };

  var Investments = {
    getOrCreateParticipation: function(config) {
      assert(config.apiParams && config.apiParams.series, 'Participations require a valid Series ID');

      return $http.post(api2.seriesParticipations(), config.apiParams).then(
        function participationGetSuccess(httpResponse) {
          var apiObjects = APIObjects.createOrUpdateDBWithResponse(httpResponse, config.db);

          return apiObjects.record;
        }
      );
    },

    getOrCreateActiveApplication: function(config) {
      config.db = config.db || ObjectDBs.temporary();
      return Investments.getOrCreateParticipation(config).then(function(participation) {
        return participation.local(config.db).activeApplication();
      });
    },

    seriesParticipation: function(apiParticipation) {
      return new SeriesParticipation(apiParticipation);
    },

    investment: function(apiInvestment) {
      return new Investment(apiInvestment);
    },

    investmentApplication: function(apiApplication) {
      return new InvestmentApplication(apiApplication);
    },

    investmentApplicationProgress: function(apiProgress) {
      return new InvestmentApplicationProgress(apiProgress);
    },

    signaturePacket: function(apiPacket) {
      return new SignaturePacket(apiPacket);
    },

    ProgressType: ProgressType,
  };

  return Investments;
}]);

'use strict';

/**
 * Company. A company whose securities are being offered on the citizen.vc platform.
 *
 * Members:
 *   .id() -- The company's id as a string (e.g. 'company:1')
 *
 *   .name() -- The company's name as a string (e.g. 'Jumio')
 *
 *   .logo() -- The company's logo as a file ID. (e.g. 'file:10')
 */

/**
 * Security. A security that is traded on the citizen.vc platform.
 *
 * Members:
 *   .id() -- The security's ID (e.g. 'security:1')
 *
 *   .name() -- The security's name (e.g. 'Common Stock')
 */

/**
 * Offering. A particular quantity of a Company's Security that is being trade on-platform.
 *
 * Members:
 *   .id() -- The offering's ID (e.g. 'offering:1')
 *
 *   .pricePerShare() -- A MoneyAmount specifying the offering's price-per-share.
 *
 *   .goal() -- A MoneyAmount specifying the offering's fundraise goal.
 *
 *   .preMoneyValuation() -- A MoneyAmount specifying the company's valuation
 *     for the duration of this offering.
 *
 *   .local(db) -- Return an object for synchronously accessing the objects
 *     related to this Offering (e.g. accesses, series) from an ObjectDB.
 *     Params:
 *       db -- an ObjectDB that has already indexed all objects related to this Offering.
 *
 *   .accesses() -- A promise to all the Accesses associated with this Offering.
 *
 *   .accessIds() -- Return an array of IDs to the accesses associated with this Offering.
 *
 *   .allSeries() -- A promise to all the Series hanging off of the accesses available to this Offering.
 *
 *   .allSeriesInReverseIdOrder() -- Returns all series in reverse Series ID order.
 *
 *   .local(db) -- Return an OfferingRelationsDB for synchronously accessing the objects
 *     related to this Offering (e.g. accesses, series).
 *     Params:
 *       db -- an ObjectDB that has already indexed all objects related to this Offering.
 *
 *   .local(db).allSeries() -- The array of all Series hanging off of accesses to this Offering. Returns in reversed order sorted by series Id.
 *
 *   .local(db).accesses() -- The array of all Accesses associated with this Offering.
 *
 *   .local(db).investmentPacketDocsIds() -- Array of Investment Packet Doc IDs in the Offering
 *   .local(db).allInvestmentPacketDocs() -- Array of Investment Packet Doc objects in the Offering
 *   .local(db).allDocs() -- A promise to Doc objects related to Investment Packet Docs
 
 */

/**
 * Access. A brokerage's access to an offering.
 *
 * Members:
 *   .id() -- The access's id (e.g. 'access:1')
 *
 *   .offeringId() -- ID of the Offering this Access gives access to (e.g. 'offering:1')
 *
 *   .brokerageId() -- ID of the Brokerage that is granted access to this Offering (e.g. 'brokerage:1')
 *
 *   .seriesIds() -- An array of IDs for the investment Series that have been opened against this access.
 *
 *   .status() -- an AccessStatus.
 *
 *   .allSeries() -- A promise to all the Series that have been opened against this access.
 *
 *   .local(db) -- Return an object for synchronously accessing the objects related to this
 *      Access.
 *      Params:
 *        db -- an ObjectDB that has already indexed all objects related to this Access.
 *
 *   .local(db).allSeries() -- The array of all investment Series created against this Access.
 *
 *   .local(db).brokerage() -- The Brokerage to which this Access has granted the Offering.
 *
 *   .local(db).offering() -- The Offering that has been granted.
 */

/**
 * AccessStatus. An Enum instance representing the status of an Access object.
 *   (See Enums.js)
 * Members:
 *   OPEN
 *   CLOSED
 */

/**
 * Series. The investable unit of a deal.
 *
 * Members:
 *
 *   .id() -- The series' ID (e.g. 'series:1')
 *
 *   .name() -- The series' name, as specified by the owning brokerage (e.g. 'Index Series I-1')
 *
 *   .status() -- A SeriesStatus showing the current series' status.
 *
 *   .accessId() -- ID of the Access against which this Series was created.
 *
 *   .feePercentage() - The fee percentage which the Brokerage charges for this Series.
 *
 *   .carryPercentage() - The carry percentage which the Brokerage charges for this Series.
 *
 *   .commissionPercentage() - The commission percentage which the Brokerage charges for this Series
 *
 *   .placementFeePercentage() - The placement fee percentage which the Brokerage charges for this Series
 *
 *   .recommendedMinInvestment() - The recommended minimum investment amount which the Brokerage sets. Returns as MoneyAmount
 *
 *   .investmentPacketDocsIds() - Array of Investment Packet Doc IDs associated to a Series.
 *
 *   .isClosed() - Is the series status set to CLOSED
 *
 *   .isNotYetOpened() - Is the series status set to NOT YET OPENED
 *
 *   .isOnHold() - Is the series status set to ON HOLD
 *
 *   .isOpen() - Is the series status set to OPEN
 *
 *   .local(db) -- Return an object for synchronously accessing the objects related
 *     to this Series.
 *     Params:
 *       db -- an ObjectDB that has already indexed all objects relating to this Series.
 *
 *   .local(db).access() -- The Access against which this Series was created.
 *
 *   .local(db).offering() -- The Offering that this Series gives investors access to
 *
 *   .local(db).brokerage() -- The Brokerage that owns this Series.
 *
 *   .local(db).allInvestmentPacketDocs() -- Array of instances of Investment Packet Doc objects
 *
 *   .local(db).allDocIds() -- Array of Doc Ids of all the Investment Packet Docs associated to the Series
 *
 *   .local(db).allDocs() -- Array of Doc objects of all the Investment Packet Docs associated to the Series
 */

/**
 * SeriesStatus. An Enum instance representing the various states that a Series can occupy.
 * Mostly for use in comparing the result of Series.status()
 *
 * Members:
 *   NOT_YET_OPENED,
 *   OPEN,
 *   ON_HOLD,
 *   CLOSED
 */

 /**
 * InvestmentPacketDoc. A document associated to the Series.
 *
 * Members:
 *
 *  .nonUSIndividualMustSign() -- Boolean Non US Individual Must Sign the doc
 *
 *  .nonUSOrganizationMustSign() -- Boolean Non US Organization must sign the doc
 *
 *  .sequenceNumber() -- Sequence Number for displaying doc
 *
 *  .series() -- Series associated to the doc
 *
 *  .USIndividualMustSign() -- Boolean US Individual must sign the doc
 *
 *  .USOrganizationMustSign() -- Boolean US Organization must sign the doc
 *
 *  .docId() -- Doc namespaced ID 
 *
 *  .local(db) -- Return an object for synchronously accessing the objects related
 *     to this Investment Packet Doc.
 *     Params:
 *       db -- an ObjectDB that has already indexed all objects relating to this Investment Packet Doc.
 *
 *  .local(db).doc() -- Instance of Doc object related to the Investment Packet Doc
 *
 *
 */

/**
 * Deals. A factory for many types relating to Deals.
 *
 * Members:
 *   .offeringsForInvestment(config) -- A promise to the array of Offerings available
 *     to the user for investment.
 *     Args:
 *       config -- Optional object.
 *         .db -- Optional ObjectDB. If provided, the offerings and all included objects (e.g. series, accesses)
 *           will be populated into the DB before the promise resolves.
 *
 *   .offeringsFromBrokerage(brokerageId, config) -- A promise to all of the Offerings offered by a Brokerage.
 *     This should only be callable a brokerage admin.
 *     Args:
 *       brokerageId -- ID of the brokerage whose Offerings should be returned (e.g. 'offering:1')
 *       config -- Optional object.
 *         .db -- Optional ObjectDB. If provided, the offering and all included objects will be put into the DB
 *           before the promise returns.
 *
 *   .getOfferings(config) -- A promise to the specified collection of Offerings.
 *     Args:
 *       config -- Required object.
 *         .apiParams -- Required object with query parameters (GET) or body parameters (POST) for consumption
 *            by the API
 *
 *          .db -- Optional ObjectDB. If provided the offering and all included objects will be put into the DB
 *            before the promise returns.
 *
 *   .getSeries(seriesId, config) -- A promise to a Series with the specified id.
 *     Args:
 *       seriesId -- ID of the target series (e.g. 'series:1')
 *       config -- Optional object.
 *         .db -- Optional ObjectDB. If provided, the series and all included objects will be put into the DB
 *           before the promise returns.
 *
 *   .getAllSeriesFromOfferings(offerings, db) -- Given an array of Offerings,
 *     return an array of all the offerings' series.
 *     Args:
 *       offerings -- array of Offerings.
 *       db -- an ObjectDB populated with the Offerings' relations.
 *
 *   .SeriesStatus -- the SeriesStatus Enum type (see above for docs)
 *
 *   .AccessStatus -- the AccessStatus Enum type (see above for docs)
 *
 *   .offering(json),
 *   .security(json),
 *   .company(json),
 *   .access(json),
 *   .series(json),
 *   .investmentPacketDoc(json) --
 *      create a domain object from its API representation. This is usually used in the APIObjects service to populate an
 *      ObjectDB.
 *
 */


angular.module('myApp.services').service(
'Deals',
["MoneyAmounts", "Utils", "$q", "$http", "api2", "APIObjects", "ObjectDBs", "Enums", "Docs", function(MoneyAmounts, Utils, $q, $http, api2, APIObjects, ObjectDBs, Enums, Docs) {
  //
  // Company
  //
  var Company = function(apiCompany) {
    this._apiCompany = apiCompany;
  };

  Company.prototype = {
    id: function() {
      return this._apiCompany.id;
    },

    name: function() {
      return this._apiCompany.name;
    },

    logo: function() {
      return this._apiCompany.logo;
    },

    description: function() {
      return this._apiCompany.description;
    },

    slug: function() {
      return this._apiCompany.slug;
    }
  };

  //
  // Security
  //
  var Security = function(apiSecurity) {
    this._apiSecurity = apiSecurity;
  };

  Security.prototype = {
    id: function() {
      return this._apiSecurity.id;
    },

    name: function() {
      return this._apiSecurity.name;
    },

    companyId: function() {
      return this._apiSecurity.company;
    },

    local: function(db) {
      return new SecurityRelationsDB(this, db);
    }
  };

  var SecurityRelationsDB = function(security, db) {
    this._db = db;
    this._security = security;
  };

  SecurityRelationsDB.prototype = {
    company: function() {
      return this._db.get(this._security.companyId(), {strict: true});
    }
  };

  //
  // Offering
  //
  var Offering = function(apiOffering) {
    this._apiOffering = apiOffering;
  };

  Offering.prototype = {
    id: function() {
      return this._apiOffering.id;
    },

    pricePerShare: function() {
      return MoneyAmounts.create(this._apiOffering.price_per_share);
    },

    goal: function() {
      return MoneyAmounts.create(this._apiOffering.goal_amt);
    },

    preMoneyValuation: function() {
      return MoneyAmounts.create(this._apiOffering.pre_money_valuation);
    },

    local: function(db) {
      return new OfferingRelationsDB(this, db);
    },

    accessIds: function() {
      return this._apiOffering.accesses;
    },

    securityId: function() {
      return this._apiOffering.security;
    }
  };

  var OfferingRelationsDB = function(offering, db) {
    this._offering = offering;
    this._db = db;
  };

  OfferingRelationsDB.prototype = {
    allSeries: function () {
      var self = this;
      var seriesSets = _.map(this.accesses(), function(access) {
        return access.local(self._db).allSeries();
      });

      return _.flatten(seriesSets);
    },

    allSeriesInReverseIdOrder: function () {
      var seriesSets = this.allSeries();

      seriesSets = _.chain(seriesSets)
        .sortBy(function(series) {
          var id = series.id();
          id = parseInt(id.slice('series:'.length));
          return id;
        })
        .reverse()
        .value();

      return seriesSets;
    },

    accesses: function () {
      return this._db.get(this._offering.accessIds(), {strict: true});
    },

    security: function () {
      return this._db.get(this._offering.securityId(), {strict: true});
    },

    investmentPacketDocsIds: function () {
      var self = this;

      var allSeries = this.allSeries();

      var packetDocsIds = _.chain(allSeries)
        .filter(function (series){
          if(series.investmentPacketDocsIds().length > 0) {
            return series;
          }
        })
        .map(function (series){ return series.investmentPacketDocsIds();})
        .flatten()
        .value();

      return packetDocsIds;
    },

    allInvestmentPacketDocs: function () {
      var packetDocsIds = this.investmentPacketDocsIds();
      return this._db.get(packetDocsIds);
    },

    allDocs: function () {
      var self = this;
      var packetDocs = this.allInvestmentPacketDocs();

      var ids = _.map(packetDocs, function(packetDoc){
        return packetDoc.docId();
      });

      return Docs.getByIds({ids:ids, db: this._db}).then(function(docs){
        return docs;
      });
    }

  };

  //
  // Access
  //
  var Access = function(apiAccess) {
    this._apiAccess = apiAccess;
  };

  Access.prototype = {
    id: function() {
      return this._apiAccess.id;
    },

    offeringId: function() {
      return this._apiAccess.offering;f.value
    },

    brokerageId: function() {
      return this._apiAccess.brokerage;
    },

    status: function() {
      return AccessStatus.forValue(this._apiAccess.status);
    },

    seriesIds: function() {
      return this._apiAccess.series;
    },

    local: function(db) {
      return new AccessRelationsDB(this, db);
    }
  };

  var AccessRelationsDB = function(access, db) {
    this._access = access;
    this._db = db;
  };

  AccessRelationsDB.prototype = {
    allSeries: function() {
      return this._db.get(this._access.seriesIds(), {strict: true});
    },

    brokerage: function() {
      return this._db.get(this._access.brokerageId(), {strict: true});
    },

    offering: function() {
      return this._db.get(this._access.offeringId(), {strict: true});
    }
  };

  var AccessStatus = Enums.create({
    OPEN: {value: 'open', humanReadable: 'open'},
    CLOSED: {value: 'closed', humanReadable: 'closed'}
  });

  //
  // Series
  //
  var Series = function(apiSeries) {
    this._apiSeries = apiSeries;
  };

  Series.prototype = {
    id: function() {
      return this._apiSeries.id;
    },

    accessId: function() {
      return this._apiSeries.access;
    },

    name: function() {
      return this._apiSeries.name;
    },

    status: function() {
      return SeriesStatus.forValue(this._apiSeries.status.toLowerCase());
    },

    local: function(db) {
      return new SeriesRelationsDB(this, db);
    },

    feePercentage: function () {
      return this._apiSeries.fee_pct;
    },

    carryPercentage: function () {
      return this._apiSeries.carry_pct;
    },

    commissionPercentage: function () {
      return this._apiSeries.commission_pct;
    },

    placementFeePercentage: function () {
      return this._apiSeries.placement_fee_pct;
    },

    recommendedMinInvestment: function () {
      return MoneyAmounts.create(this._apiSeries.recommended_min_investment);
    },

    investmentPacketDocsIds: function () {
      return this._apiSeries.investment_packet_docs;
    },

    isClosed: function () {
      return (this._apiSeries.status === SeriesStatus.CLOSED.value());
    },

    isNotYetOpened: function () {
      return (this._apiSeries.status === SeriesStatus.NOT_YET_OPENED.value());
    },

    isOnHold: function () {
      return (this._apiSeries.status === SeriesStatus.ON_HOLD.value());
    },

    isOpen: function () {
      return (this._apiSeries.status === SeriesStatus.OPEN.value());
    }
  };

  var SeriesRelationsDB = function(series, db) {
    this._series = series;
    this._db = db;
  };

  SeriesRelationsDB.prototype = {
    access: function() {
      return this._db.get(this._series.accessId(), {strict: true});
    },

    offering: function() {
      return this.access().local(this._db).offering();
    },

    brokerage: function() {
      return this.access().local(this._db).brokerage();
    },

    security: function() {
      return this.offering().local(this._db).security();
    },

    company: function() {
      return this.security().local(this._db).company();
    },

    allInvestmentPacketDocs: function () {
      return this._db.get(this._series.investmentPacketDocsIds(), {strict: true});
    },

    allDocIds: function () {
      return _.map(this.allInvestmentPacketDocs(), function (packetDoc){
        return packetDoc.docId();
      });
    },

    allDocs: function () {
      return this._db.get(this.allDocIds(), {strict: true});
    }
 
  };

  var SeriesStatus = Enums.create({
    NOT_YET_OPENED: {value: 'not_yet_opened', humanReadable: 'not yet opened'},
    OPEN: {value: 'open', humanReadable: 'open'},
    ON_HOLD: {value: 'on_hold', humanReadable: 'on hold'},
    CLOSED: {value: 'closed', humanReadable: 'closed'}
  });

  //
  // InvestmentPacketDoc
  //
  var InvestmentPacketDoc = function(apiPacketDoc) {
    this._apiPacketDoc = apiPacketDoc;
  };

  InvestmentPacketDoc.prototype = {
    id: function() {
      return this._apiPacketDoc.id;
    },

    investorMustSign: function() {
      return this._apiPacketDoc.investor_must_sign;
    },

    firmMustSign: function() {
      return this._apiPacketDoc.firm_must_sign;
    },

    nonUSIndividualMustSign: function() {
      return this._apiPacketDoc.non_us_individual_must_sign;
    },

    nonUSOrganizationMustSign: function() {
      return this._apiPacketDoc.non_us_organization_must_sign;
    },

    sequenceNumber: function() {
      return this._apiPacketDoc.sequence_number;
    },

    series: function() {
      return this._apiPacketDoc.series;
    },

    USIndividualMustSign: function() {
      return this._apiPacketDoc.us_individual_must_sign;
    },

    USOrganizationMustSign: function() {
      return this._apiPacketDoc.us_organization_must_sign;
    },

    docId: function() {
      return this._apiPacketDoc.doc;
    },

    local: function (db) {
      return new InvestmentPacketDocRelationsDB(this, db);
    }
  };

  var InvestmentPacketDocRelationsDB = function (investmentPacketDoc, db) {
    this._investmentPacketDoc = investmentPacketDoc;
    this._db = db;
  };

  InvestmentPacketDocRelationsDB.prototype = {
    doc: function () {
      return this._db.get(this._investmentPacketDoc.docId(), {strict: true});
    }
  };

  var Deals = {
    offeringsForInvestment: function(config) {
      config = _.extend(config || {}, {
        apiParams: {
          q: 'investable'
        }
      });

      return Deals.getOfferings(config);
    },

    offeringsFromBrokerage: function(brokerageId, config) {
      config = _.extend(config || {}, {
        apiParams: {
          q: 'managed',
          brokerage: brokerageId
        }
      });

      return Deals.getOfferings(config);
    },

    getOfferings: function (config) {
      config = config || {};

      var apiParams = config.apiParams || {};
      var offeringsRequest = $http.get(api2.offerings(), {
        params: apiParams
      });
      return offeringsRequest.then(
        function offeringsGetSuccess(httpResponse) {
          var apiObjects = APIObjects.createOrUpdateDBWithResponse(httpResponse, config.db);
          var offerings = apiObjects.records;

          return offerings;
      });
    },

    getSeries: function(seriesId, config) {
      config = config || {};
      var db = config.db || ObjectDBs.temporary();

      var requestConfig = _.extend(config || {}, {
        apiParams: {
          q: 'series',
          series_id: seriesId
        },
        db: db
      });

      return Deals.getOfferings(requestConfig).then(function (offerings) {
        var allSeries = Deals.getAllSeriesFromOfferings(offerings, db);
        var series = _.filter(allSeries, function(series) {
          return series.id() === seriesId;
        });

        Utils.assert(series.length === 1, 'Number of series matching the requested ID was ' + series.length + ' when 1 was expected.');

        return series[0];
      });
    },

    getAllSeriesFromOfferings: function(offerings, db) {
      var allSeries = _.flatten(_.map(offerings, function(offering) {
        return offering.local(db).allSeries();
      }));

      return allSeries;
    },

    offering: function(offeringJson) {
      return new Offering(offeringJson);
    },

    security: function(securityJson) {
      return new Security(securityJson);
    },

    company: function(companyJson) {
      return new Company(companyJson);
    },

    access: function(accessJson) {
      return new Access(accessJson);
    },

    series: function(seriesJson) {
      return new Series(seriesJson);
    },

    investmentPacketDoc: function(apiPacketDoc) {
      return new InvestmentPacketDoc(apiPacketDoc);
    },

    SeriesStatus: SeriesStatus,

    AccessStatus: AccessStatus
  };

  return Deals;
}]);

'use strict';

/**
 * Doc. A single document.
 *
 * Members:
 *   .id() -- The document's ID (e.g. 'document:1')
 *
 *   .name() -- The document's name (e.g. 'Jumio Series I Subscription Agreement')
 *
 *   .type() -- A valid DocType.
 *
 *   .status() -- This Doc's DocStatus.
 *
 *   .externalRef() -- ID/URL reference to external doc. Mostly used for Docusign documents.
 */

/**
 * DocStatus. An Enum instance representing the status of a Doc.
 *   (See Enums.js)
 *
 * Members:
 *   .PUBLISHED -- The document is visible to people other than the author (and authorized others)
 *   .NOT_PUBLISHED -- The document is only visible to the author (and other authorized admins)
 */

/**
 * DocType. An Enum instance representing the type of a Doc.
 *   (See Enums.js)
 *
 * Members:
 *   .SUBSCRIPTION_AGREEMENT -- A subscription agreement for an associated series.
 *   .WIRING_INSTRUCITONS -- The wiring instructions for an associated series.
 *   .OTHER -- Some other type of document
 */

/**
 * Docs. A factory for creating Doc instances.
 *
 * Members:
 *   .getByIds(ids) -- Return a promise to the Docs with the provided IDs.
 *     IDs should be an array of IDs (e.g. ['document:1', 'document:2']).
 *     If an array of ids is not given, it returns an empty promise. Boo-hoo.
 *
 *   .instance(apiDoc) - Return a new instance of Doc given the JSON config
 *
 *   .Status -- see above for DocStatus constant
 *
 *   .Type -- see above DocType constant
 */
angular.module('myApp.services').service(
'Docs',
["$http", "api2", "APIObjects", "ObjectDBs", "Enums", "$q", function($http, api2, APIObjects, ObjectDBs, Enums, $q) {
  var Doc = function(apiDoc) {
    this._apiDoc = apiDoc;
  };

  Doc.prototype = {
    id: function() {
      return this._apiDoc.id;
    },

    name: function() {
      return this._apiDoc.name;
    },

    file: function () {
      return this._apiDoc.file;
    },

    type: function() {
      return DocType.forValue(this._apiDoc.type);
    },

    status: function() {
      return DocStatus.forValue(this._apiDoc.status);
    },

    externalRef: function() {
      return this._apiDoc.external_ref;
    }
  };

  var DocStatus =  Enums.create({
    PUBLISHED: {value: 'published', humanReadable: 'published'},
    NOT_PUBLISHED: {value: 'not_published', humanReadable: 'not published'}
  });

  var DocType =  Enums.create({
    SUBSCRIPTION_AGREEMENT: {value: 'subscription_agreement', humanReadable: 'subscription agreement'},
    WIRING_INSTRUCTIONS: {value: 'wiring_instructions', humanReadable: 'wiring instructions'},
    OTHER: {value: 'other', humanReadable: 'other'}
  });

  var Docs = {
    getByIds: function(config) {
      config = config || {};
      var apiParams = {ids: config.ids};

      var docsRequest;

      if (config.ids.length > 0) {
        docsRequest = $http.get(api2.docs(), {
          params: apiParams
        });

        return docsRequest.then(
          function docsGetSuccess(httpResponse) {
            var apiObjects = APIObjects.createOrUpdateDBWithResponse(httpResponse, config.db);
            
            return apiObjects.records;
        });      
      } else {
        return $q.when([]);
      }
    },

    instance: function(apiDoc) {
      return new Doc(apiDoc);
    },

    Status: DocStatus,

    Type: DocType
    
  };

  return Docs;
}]);

'use strict';
// jshint eqeqeq:false
// jshint eqnull:true

/* Filters */

angular.module('myApp.filters', []).
  filter('interpolate', ['version', function(version) {
    return function(text) {
      return String(text).replace(/\%VERSION\%/mg, version);
    };
  }]).
  filter('bytes', function() {
    return function(bytes, precision) {
      if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) {
        return '-';
      }
      if (typeof precision === 'undefined') {
        precision = 1;
      }
      var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
        number = Math.floor(Math.log(bytes) / Math.log(1024));
      return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
    };
  }).
  filter('filePath', ['API_PATH', 'BASE_PATH', function(API_PATH, BASE_PATH) {
    return function(path) {
      if(path == null) {
        return null;
      }
      // TODO: remove custom url generation
      if(path.substring(0, 4) === 'http') {
        return path;
      }
      return BASE_PATH.API()+path;
    };
  }]).
  filter('apiPath', ['BASE_PATH', function(BASE_PATH) {
    return function(path) {
      return BASE_PATH.API()+path;
    };
  }]).
  filter('getFavIcon', ['API_PATH', function(API_PATH) {
    return function(path) {
      return API_PATH.GET_FAVICON+path;
    };
  }]).
  filter('evenIndex', function() {
    return function(arr) {
      return _.reject(arr, function(obj,index){ return index % 2 === 0; });
    };
  }).
  filter('oddIndex', function() {
    return function(arr) {
      return _.reject(arr, function(obj,index){ return index % 2 !== 0; });
    };
  }).
  filter('prefix', function() {
    return function(text,preStr) {
      return preStr+text;
    };
  }).
  filter('seriesText', function() {
    return function(val) {
      var seriesType = {
        'seed': 'Seed',
        'series_a': 'Series A',
        'series_b': 'Series B',
        'series_c+': 'Series C+',
        'secondary': 'Secondary'
      };
      return (seriesType[val] ? seriesType[val] : val);
    };
  }).
  filter('fileType', function(){
    return function(val){
      var fileTypes = {
        'investor_accredited_profile':'Accredited Investor Profile',
        'company_pitch_file':'Company Pitch File',
        'company_video_link':'Company Video Link',
        'company_video_file':'Company Video File',
        'company_pitch_link':'Company Pitch Link',
        'company_document_file':'Company Document',
        'company_signup_file':'Document File',
        'company_logo':'logo',
        'other':'Other investor documents'
      };
      return (fileTypes[val] ? fileTypes[val] : val);
    };
  }).
  filter('titleize', function () {
    return function (text) {
      if (text == null) {
        return '';
      }
      return String(text).replace(/(?:^|\s)\S/g, function(c) {
        return c.toUpperCase();
      });
    };
  }).
  filter('startFrom', function() {
    return function(input, start) {
      if(input == null) {
        return;
      }
      start = +start; //parse to int
      return input.slice(start*9);
    };
  }).
  filter('rename', function () {
    return function (text) {
      if (text === 'institution') {
        return 'entity';
      }
      else if (text.indexOf('http:') !== -1) {
        text.replace('http:','');
      }
      else if (text.indexOf('https:') !== -1) {
        text.replace('https:','');
      }
      else {
        return text;
      }
    };
  }).
  filter('limitText', function () {
    return function (input, limit) {
      input = input.toString();
      limit = parseInt(limit);
      if(limit < input.length){
        if(input[limit] === ' '){
          return input.substring(0,limit);
        }
        else{
          while(input[limit] !== ' '){
            if(limit>0){
              limit--;
            }
            else{
              return ' ';
            }
          }
          return input.substring(0,limit);
        }
      }
      else{
        return input;
      }
    };
  }).
  filter('largecurrency', ['$filter', function($filter) {
    var units = [
      {lower_bound: 1e12, suffix: 'Trillion'},
      {lower_bound: 1e9, suffix: 'Billion'},
      {lower_bound: 1e6, suffix: 'Million'},
    ];
    var num_units = units.length;

    return function(number) {
      if (number === undefined) {
        return number;
      }

      for (var i = 0; i < num_units; i++) {
        var unit = units[i];
        var abs = Math.abs(number);
        if (abs >= unit.lower_bound) {
          // Calculate the number in units, multiplying by 100 then dividing in order
          // to retain 2 decimal points.
          var number_in_units = Math.round(number * 100 / unit.lower_bound) / 100;
          // Adjust for when rounding could have up-rounded us to the next unit (e.g. 1000K)
          if (number_in_units === 1000 && i !== 0) {
            number_in_units = 1;
            unit = units[i - 1];
          }
          // Calculate the precision with which we should render the number
          var number_is_integer = number_in_units % 1 === 0;
          var number_is_tenth_precision = (number_in_units * 10) % 1 === 0;
          var num_decimals = number_is_integer? 0: number_is_tenth_precision? 1: 2;
          var formatted_number = number_in_units.toFixed(num_decimals) + ' ' + unit.suffix;
          return formatted_number;
        }
      }

      // Number is small. Don't do anything fancy.
      return $filter('number')(number);
    };
  }]);

'use strict';
// jshint eqeqeq:false
// jshint eqnull:true

/* Directives */

angular.module('myApp.directives', [])
  // directive for tab dispplay of Members, Companies and Admins in Admin dashboard
  .directive('panelTab',function(){
    return {
      restrict:'A',
      link: function(scope,element){
        element.bind('click', function() {

          // Activate tab
          $('.sidebar_list li').removeClass('active');
          $('.sidebar_arrow').hide();
          element.addClass('active');
          $(element).children('.sidebar_arrow').show();

          // Display respective dashboard
          $('.dashboard_blocks').hide();
          var show_block = '.' + $(this).data('target');
          $(show_block).show();
        });
      }
    };
  })
  .directive('accessibleform', function() {
    return {
      retrict: 'EA',
      link: function (scope, elem) {
        // set up event handler on the form element
        elem.on('submit', function () {
          // find the first invalid element
          var firstInvalid = angular.element(
            elem[0].querySelector('.ng-invalid'))[0];
          // if we find one, set focus
          if (firstInvalid) {
            //firstInvalid.focus();
            $('html, body').animate({scrollTop:$('#'+firstInvalid.id).offset().top -100}, 'slow');
          }
        });
      }
    };
  })
  //directive for multiselect bootstrap
  .directive('multiSelect',['$compile','$timeout', function ($compile,$timeout) {
    return {
      restrict: 'A',
      link : function (scope,element) {
        $timeout(function(){
          if (element.hasClass('addOption')) {
            element.multiselect({
              onDropdownShow: function(/*event*/) {
                element.next('.btn-group').find('.newInputContainer').remove();
                $compile('<li class="newInputContainer"><div class="form-group  has-feedback"><input type="text" onkeydown="if (event.keyCode == 13) return false;"  add-option-input class="addOptionInput form-control"  placeholder="Other"><span class="glyphicon glyphicon-plus form-control-feedback" add-option-button data-target="#' + element.attr('id') + '"></span></div></li>')(
                  // what's going on here?
                  scope, function (compiledBtn, scope){
                    element.next('.btn-group').find('.multiselect-container.dropdown-menu').append(compiledBtn);
                  }
                );
                // $compile(element.next('.btn-group').contents())(scope);
              }
            });
          }
          else {
            element.multiselect();
          }
        },500);
      }
    };
  }])
  .directive('addOptionButton', function () {
    return {
      restrict: 'A',
      link : function (scope,element) {
        element.on('click', function (e) {
          var addoptionVal = element.prev('.addOptionInput').val();
          e.stopImmediatePropagation();
          if (addoptionVal !== '') {
            $(element.data('target')).append('<option value="' + addoptionVal + '">' + addoptionVal + '</option>');
            //$('.multiselect-container li').find('input').attr('checked', false);

            //$(element.data('target')).multiselect( 'refresh' );
            //$(element.data('target')).multiselect('select', addoptionVal);
            $(element.data('target')).multiselect('rebuild');
          }
        });
      }
    };
  })
  .directive('addOptionInput', function () {
    return {
      restrict: 'A',
      link : function (scope,element) {
        element.on('focus click', function(e) {
          e.stopImmediatePropagation();
          $(this).parents('.btn-group').addClass('open');
        });
      }
    };
  })
  .directive('countryCodeDropdown', function () {
    return {
      restrict: 'AE',
      require:'ngModel',
      link : function (scope,element,attrs,ngModel) {
        function read(){
          ngModel.$setViewValue(element.val());
          element.intlTelInput();
          if(element.val() == ''){
            ngModel.$pristine = true;
          }
          var is_valid_no=element.intlTelInput('isValidNumber');
          ngModel.$valid = is_valid_no;
          ngModel.$invalid = !is_valid_no;
          if(element.val() != '' && !is_valid_no) {
            element.addClass('ng-invalid');
          }else{
            element.removeClass('ng-invalid');
          }
          scope.$apply();
        }
        element.on('blur keyup change', function(/*event*/){
          element.intlTelInput('setNumber', element.val());
          read();
        });
      }
    };
  })
  .directive('rolesDirective',function(){
    return {
      restrict: 'A',
      require: 'ngModel' ,
      link : function(scope,element,attrs){
        if(attrs.ngModel.length>0){
          if($(element).hasClass('ng-invalid')){
            $(element).removeClass('ng-invalid');
          }
          $(element).addClass('ng-valid');
        }
      }
    };
  })
  .directive('pwCheck', [function () {
    return {
      require: 'ngModel',
      link: function (scope, elem, attrs, ctrl) {
        var firstPassword = '#' + attrs.pwCheck;
        $(firstPassword).on('keyup',function(){
          if(elem.val()!=''){
            scope.$apply(function () {
              var v = elem.val()===$(firstPassword).val();
              ctrl.$setValidity('pwmatch', v);
            });
          }
        });

        elem.on('keyup', function(){
          scope.$apply(function () {
            var v = elem.val()===$(firstPassword).val();
            ctrl.$setValidity('pwmatch', v);
          });
        });
      }
    };
  }])
  .directive('required', function(){
    return {
      restrict: 'A',
      link : function(scope,element){
        if ($(element).siblings('label').length > 0){
          $(element).siblings('label').addClass('mandatory');
        }
        else{
          $(element).parents().siblings('label').addClass('mandatory');
        }
      }
    };
  })
  .directive('showPass',function(){
    return {
      restrict: 'A',
      replace: true,
      scope:{},
      template:'<input id="showPassword" type="checkbox" name="showPassBoy" value="showPass" ng-model="checkBoxValue">',
      link:function(scope){
        scope.checkBoxValue=false;

        scope.$watch('checkBoxValue' ,function(value){
          if (value) {
            $('#passwordone').prop('type','text');
            $('#passwordtwo').prop('type','text');
          } else {
            $('#passwordone').prop('type','password');
            $('#passwordtwo').prop('type','password');
          }
        });
      }
    };
  })
  .directive('clearField', function(){
    return {
      restrict:'A',
      link: function(scope,element){
        element.on('click', function(){
          $(element).prop('value','');
        });
      }
    };
  })
  .directive('showLoader', function (){
    return{
      restrict: 'A',
      replace: false,
      link: function(scope,element){
        element.on('click',function(){
          $('.loader-overlay').fadeIn('fast');
        });
      }
    };
  })
  .directive('postAnswer', function(){
    return {
      restrict:'A',
      link: function(scope,element){
        element.on('click', function(){
          $(element).parent().siblings('.answer-toggle').click();
          $(element).siblings('textarea').val('');
        });
      }
    };
  })
  .directive('postQuestion', function(){
    return {
      restrict:'A',
      link: function(scope,element){
        element.on('click', function(){
          $(element).siblings('textarea').val('');
        });
      }
    };
  })
  .directive('showDetails', function(){
    return {
      restrict: 'A',
      link: function(scope,element){
        element.on('click',function(/*event*/){
          $('.companies-list-wrapper, .investor-list-wrapper').toggleClass('hide');
        });
      }
    };
  })
  .directive('flexslider',function(){
    return function(scope) {
      scope.$watch('$last',function(/*value*/){
        var winWidth = $(window).width();
        var itemWidth;
        if(winWidth > 660) {
          itemWidth = 335;
        }else{
          itemWidth = 300;
        }
        $('.flexslider').flexslider({
          animation: 'slide',
          animationLoop: false,
          itemWidth: itemWidth,
          itemMargin: 5
        });
      });
    };
  })
  .directive('tabactive',function(){
    return {
      restrict: 'A',
      link: function(scope, elem) {

        elem.on('click', function(/*event*/) {
          $('#myTab li').css({'color':'#8e8e8e', 'border-bottom':'0px'});
          this.style.color = '#000';
          this.style.borderBottom = '2px solid #2eab63';
        });
      }
    };
  })
  .directive('donutchart',function(){
    return {
      restrict: 'A',
      scope: {
        graphpoints: '=',
        height: '=',
        width: '='
      },
      link: function(scope, elem) {
        var dataset = scope.graphpoints;
        if(isNaN(dataset.dataPoint[0])) {
          dataset.dataPoint[0] = 0;
        }
        if(isNaN(dataset.dataPoint[1])) {
          dataset.dataPoint[1] = 0;
        }
        if(isNaN(dataset.dataPoint[2])) {
          dataset.dataPoint[2] = 0;
        }

        if(dataset.dataPoint[2] == 1) {
          dataset.dataPoint[1] = 0;
        }

        var height = 106;
        var innerRadius = 26;
        var outerRadius = 13;
        if(scope.height != undefined) {
          height = scope.height;
          innerRadius = 44;
          outerRadius = 22;
        }

        var width = 200;
        if(scope.width != undefined) {
          width = scope.width;
        }

        var radius = Math.min(width, height) / 2;

        //var color = d3.scale.category20();
        var color = d3.scale.ordinal()
          .range(['#dfdfdf', '#1d1d1b', '#2eab63']);

        var pie = d3.layout.pie()
          .sort(null);

        var arc = d3.svg.arc()
          .innerRadius(radius - innerRadius)
          .outerRadius(radius - outerRadius);

        var svg = d3.select(elem[0]).append('svg')
          .attr('width', width)
          .attr('height', height)
          .append('g')
          .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');

        svg.selectAll('path')
          .data(pie(dataset.dataPoint))
          .enter().append('path')
          .attr('fill', function(d, i) { return color(i); })
          .attr('d', arc);
      }
    };
  })
  .directive('scrolltop',function(){
    return {
      restrict: 'A',
      link: function(scope, elem) {
        elem.on('click', function(/*event*/) {
          $('html, body').animate({
            scrollTop: '0px'
          }, 10);
        });
      }
    };
  })
  .directive('pagescrolltop',function(){
    return {
      restrict: 'A',
      link: function() {
        $('html, body').animate({
          scrollTop: '0px'
        }, 10);
      }
    };
  })
  .directive('scrollto',function(){
    return {
      restrict: 'A',
      link: function(scope, elem) {
        elem.on('click', function(/*event*/) {
          var winWidth = $(window).width();
          var scrollHeight = 1500;
          if(winWidth > 992) {
            scrollHeight = 500;
          }else if(winWidth > 767 && winWidth < 992){
            scrollHeight = 1000;
          }
          $('html, body').animate({
            scrollTop: scrollHeight+'px'
          }, 1000);
        });
      }
    };
  })
  .directive('accordiontoggle',function(){
    return {
      restrict: 'A',
      link: function(scope, elem) {
        elem.on('click', function(/*event*/) {
          if($(this).hasClass('upArrow')){
            $('.panel-title .pointer').removeClass('upArrow downArrow').addClass('downArrow');
          }else if($(this).hasClass('downArrow')){
            $('.panel-title .pointer').removeClass('upArrow downArrow').addClass('downArrow');
            $(this).removeClass('downArrow').addClass('upArrow');
          }
        });
      }
    };
  })
  .directive('divstacking',function(){
    return {
      restrict: 'A',
      link: function() {
        $('.imgCol img').load(function() {
          $('.textCol').css({'marginTop':$('.imgCol img').height()+'px'});
        });
      }
    };
  })
  .directive('browser', ['$window', function($window) { // good luck figuring out if this is ever used
    return{
      restrict: 'EA',
      link: function() {

        var userAgent = $window.navigator.userAgent;

        var browsers = {chrome: /chrome/i, safari: /safari/i, firefox: /firefox/i, ie: /internet explorer/i};

        for(var key in browsers) {
          if (browsers[key].test(userAgent)) {
            if(key !== 'safari') {
              $('#bgvid').append('<source src="videos/video2.webm" type="video/webm" id="videoSourceWebm"><source src="videos/video2.ogv" type="video/ogv" id="videoSourceOgv">');
            }else{
              // var myVideo = document.getElementById('bgvid');
              // elem[0].play();
              $('#bgvid').trigger('click');
            }
          }
        }

        return 'unknown';
      }
    };
  }])
  .directive('windowheight',['$window',function($window){
    return {
      restrict: 'A',
      link: function(scope, elem) {
        var winHeight = $(window).height();
        var winWidth = $(window).width();
        var headerHeight = $('header').height();
        if(winWidth > 767){
          elem.css({'height':winHeight-(headerHeight+75)+'px'});
        }

        scope.onResize = function() {
          var winHeight = $(window).height();
          var winWidth = $(window).width();
          var headerHeight = $('header').height();

          if(winWidth > 767){
            elem.css({'height':winHeight-(headerHeight+75)+'px'});
          }
        };
        scope.onResize();

        setTimeout(function () {
          scope.onResize();
        }, 500);

        angular.element($window).bind('resize', function() {
          scope.onResize();
        });
      }
    };
  }])
  .directive('fullscreen',function(){
    return {
      restrict: 'A',
      link: function() {
        var winHeight = $(window).height()-273;
        var winWidth = $(window).width();
        if(winWidth > 1024) {
          $('.fullscreen').css({'height': winHeight+'px'});
        }
      }
    };
  })
  .directive('activityevents',function(){
    return {
      restrict: 'A',
      link: function() {
       // elem.bind('click', function(e) {
       //    if(e.target.className !== 'socialMediaddl' && !$('.socialMediaddl').find(e.target).length && e.target.id !== 'document' && e.target.id !== 'socialMedia' && e.target.id !== 'tre1' && e.target.id !== 'tre2' && e.target.id !== 'tre3' && e.target.id !== 'learnMore'){
       //      $('.socialMediaddl').hide();
       //      $('.cardDdl .downArrow').removeClass('upArrow');
       //  }
       // });
      }
    };
  })
  .directive('xpandacc',function(){
    return {
      restrict: 'A',
      link: function(scope, elem) {
        elem.bind('click', function() {
          $('#accordion .panel .collapse').toggleClass('in');
          if ($('#accordion .panel .collapse').hasClass('in')) {
            elem.html('Collapse all [-]');
          } else {
            elem.html('Expand all [+]');
          }
        });
      }
    };
  })
  .directive('closecmodal',function(){
    return {
      restrict: 'A',
      link: function(scope, elem) {
        elem.bind('click', function() {
          $('.forgotPasswordModal, .forgotPasswordModalOverlay').hide();
        });
      }
    };
  })
  .directive('opencmodal',function(){
    return {
      restrict: 'A',
      link: function(scope, elem) {
        elem.bind('click', function() {
          $('.forgotPasswordModal, .forgotPasswordModalOverlay').show();
        });
      }
    };
  })
  .directive('tooltip',function(){
    return {
      restrict: 'A',
      link: function(scope, elem) {
        elem.tooltip();
      }
    };
  })
  .directive('statelist',function(){
    return {
      restrict: 'A',
      replace: true,
      scope:{},
      template:'<option ng-repeat="list in states" value="{{list.codes}}">{{list.state}} ({{list.codes}})</option>',
      link:function(scope){
        scope.states = [{'state':'Alabama','codes':'AL'},{'state':'Alaska','codes':'AK'},{'state':'Arizona','codes':'AZ'},{'state':'Arkansas','codes':'AR'},{'state':'California','codes':'CA'},{'state':'Colorado','codes':'CO'},{'state':'Connecticut','codes':'CT'},{'state':'Delaware','codes':'DE'},{'state':'District of Columbia','codes':'DC'},{'state':'Florida','codes':'FL'},{'state':'Georgia','codes':'GA'},{'state':'Hawaii','codes':'HI'},{'state':'Idaho','codes':'ID'},{'state':'Illinois','codes':'IL'},{'state':'Indiana','codes':'IN'},{'state':'Iowa','codes':'IA'},{'state':'Kansas','codes':'KS'},{'state':'Kentucky','codes':'KY'},{'state':'Louisiana','codes':'LA'},{'state':'Maine','codes':'ME'},{'state':'Maryland','codes':'MD'},{'state':'Massachusetts','codes':'MA'},{'state':'Michigan','codes':'MI'},{'state':'Minnesota','codes':'MN'},{'state':'Mississippi','codes':'MS'},{'state':'Missouri','codes':'MO'},{'state':'Montana','codes':'MT'},{'state':'Nebraska','codes':'NE'},{'state':'Nevada','codes':'NV'},{'state':'New Hampshire','codes':'NH'},{'state':'New Jersey','codes':'NJ'},{'state':'New Mexico','codes':'NM'},{'state':'New York','codes':'NY'},{'state':'North Carolina','codes':'NC'},{'state':'North Dakota','codes':'ND'},{'state':'Ohio','codes':'OH'},{'state':'Oklahoma','codes':'OK'},{'state':'Oregon','codes':'OR'},{'state':'Pennsylvania','codes':'PA'},{'state':'Rhode Island','codes':'RI'},{'state':'South Carolina','codes':'SC'},{'state':'South Dakota','codes':'SD'},{'state':'Tennessee','codes':'TN'},{'state':'Texas','codes':'TX'},{'state':'Utah','codes':'UT'},{'state':'Vermont','codes':'VT'},{'state':'Virginia','codes':'VA'},{'state':'Washington','codes':'WA'},{'state':'West Virginia','codes':'WV'},{'state':'Wisconsin','codes':'WI'},{'state':'Wyoming','codes':'WY'}];
      }
    };
  })
  .directive('countrylist',function(){
    return {
      restrict: 'A',
      replace: true,
      scope:{},
      template:'<option ng-repeat="list in countries" value="{{list}}">{{list}}</option>',
      link:function(scope){
        scope.countries = ['Afghanistan', 'Albania', 'Algeria', 'Andorra', 'Angola', 'Antarctica', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia', 'Bosnia and Herzegovina', 'Botswana', 'Brazil', 'Brunei', 'Bulgaria', 'Burkina Faso', 'Burma', 'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Cape Verde', 'Central African Republic', 'Chad', 'Chile', 'China', 'Colombia', 'Comoros', 'Congo, Democratic Republic', 'Congo, Republic of the', 'Costa Rica', 'Cote d\'Ivoire', 'Croatia', 'Cuba', 'Cyprus', 'Czech Republic', 'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'East Timor', 'Ecuador', 'Egypt', 'El Salvador', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Fiji', 'Finland', 'France', 'Gabon', 'Gambia', 'Georgia', 'Germany', 'Ghana', 'Greece', 'Greenland', 'Grenada', 'Guatemala', 'Guinea', 'Guinea-Bissau', 'Guyana', 'Haiti', 'Honduras', 'Hong Kong', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Israel', 'Italy', 'Jamaica', 'Japan', 'Jordan', 'Kazakhstan', 'Kenya', 'Kiribati', 'Korea, North', 'Korea, South', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macedonia', 'Madagascar', 'Malawi', 'Malaysia', 'Maldives', 'Mali', 'Malta', 'Marshall Islands', 'Mauritania', 'Mauritius', 'Mexico', 'Micronesia', 'Moldova', 'Mongolia', 'Morocco', 'Monaco', 'Mozambique', 'Namibia', 'Nauru', 'Nepal', 'Netherlands', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Norway', 'Oman', 'Pakistan', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru', 'Philippines', 'Poland', 'Portugal', 'Qatar', 'Romania', 'Russia', 'Rwanda', 'Samoa', 'San Marino', ' Sao Tome', 'Saudi Arabia', 'Senegal', 'Serbia and Montenegro', 'Seychelles', 'Sierra Leone', 'Singapore', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia', 'South Africa', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Swaziland', 'Sweden', 'Switzerland', 'Syria', 'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand', 'Togo', 'Tonga', 'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Uganda', 'Ukraine', 'United Arab Emirates', 'United Kingdom', 'United States', 'Uruguay', 'Uzbekistan', 'Vanuatu', 'Venezuela', 'Vietnam', 'Yemen', 'Zambia', 'Zimbabwe'];
      }
    };
  })
  .directive('play',function(){
    return {
      restrict:'A',
      link: function(scope,element){
        element.bind('click', function() {
          var myVideo = document.getElementById('bgvid');

          myVideo.play();
          $('.videoPlay').hide();
        });
      }
    };
  })

  .directive('videoplaypause',function(){
    return {
      restrict:'A',
      link: function(scope,element){
        element.bind('click', function() {
          var myVideo = document.getElementById('video');
          if (myVideo.paused) {
            myVideo.play();
          }
          else {
            myVideo.pause();
          }
        });
      }
    };
  })
  .directive('image', ["$q", function($q) {
    var URL = window.URL || window.webkitURL;

    var getResizeArea = function () {
      var resizeAreaId = 'fileupload-resize-area';

      var resizeArea = document.getElementById(resizeAreaId);

      if (!resizeArea) {
        resizeArea = document.createElement('canvas');
        resizeArea.id = resizeAreaId;
        resizeArea.style.visibility = 'hidden';
        document.body.appendChild(resizeArea);
      }

      return resizeArea;
    };

    var resizeImage = function (origImage, options) {
      var quality = options.resizeQuality || 0.7;
      var type = options.resizeType || 'image/jpg';

      var canvas = getResizeArea();

      var height = origImage.height;
      var width = origImage.width;

      /*
      // calculate the width and height, constraining the proportions
      var maxHeight = options.resizeMaxHeight || 200;
      var maxWidth = options.resizeMaxWidth || 65;
      if (width > height) {
        if (width > maxWidth) {
          height = Math.round(height *= maxWidth / width);
          width = maxWidth;
        }
      } else {
        if (height > maxHeight) {
          width = Math.round(width *= maxHeight / height);
          height = maxHeight;
        }
      }
      */

      canvas.width = width;
      canvas.height = height;

      //draw image on canvas
      var ctx = canvas.getContext('2d');
      ctx.drawImage(origImage, 0, 0, width, height);

      // get the data from canvas as 70% jpg (or specified type).
      return canvas.toDataURL(type, quality);
    };

    var createImage = function(url, callback) {
      var image = new Image();
      image.onload = function() {
        callback(image);
      };
      image.src = url;
    };

    var fileToDataURL = function (file) {
      var deferred = $q.defer();
      var reader = new FileReader();
      reader.onload = function (e) {
        deferred.resolve(e.target.result);
      };
      reader.readAsDataURL(file);
      return deferred.promise;
    };

    return {
      restrict: 'A',
      scope: {
        image: '=',
        resizeMaxHeight: '@?',
        resizeMaxWidth: '@?',
        resizeQuality: '@?',
        resizeType: '@?',
      },
      link: function postLink(scope, element, attrs) {

        var doResizing = function(imageResult, callback) {
          createImage(imageResult.url, function(image) {
            var dataURL = resizeImage(image, scope);
            imageResult.resized = {
              dataURL: dataURL,
              type: dataURL.match(/:(.+\/.+);/)[1],
            };
            callback(imageResult);
          });
        };

        var applyScope = function(imageResult) {
          scope.$apply(function() {
            if(attrs.multiple) {
              scope.image.push(imageResult);
            }
            else {
              scope.image = imageResult;
            }
          });
        };

        element.bind('change', function (evt) {
          //when multiple always return an array of images
          if(attrs.multiple){
            scope.image = [];
          }

          var files = evt.target.files;
          for(var i = 0; i < files.length; i++) {
            //create a result object for each file in files
            var imageResult = {
              file: files[i],
              url: URL.createObjectURL(files[i])
            };

            fileToDataURL(files[i]).then(function (dataURL) {
              imageResult.dataURL = dataURL;
            });

            if(scope.resizeMaxHeight || scope.resizeMaxWidth) { //resize image
              doResizing(imageResult, function(imageResult) {
                applyScope(imageResult);
              });
            }
            else { //no resizing
              applyScope(imageResult);
            }
          }
        });
      }
    };
  }])
  .directive('dobchecker',function(){
    return {
      restrict:'A',
      require:'ngModel',
      link: function(scope,element,attrs,ngModel){
        function validateDOB(dob){
          var pattern =/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
          if (!pattern.test(dob)){
            ngModel.$pristine = false;
            ngModel.$valid = false;
            ngModel.$invalid = true;
            element.addClass('ng-invalid');
            //return false;
          }
          else{
            var dobNew = dob.replace(/-/g, '/');
            var inputDate = new Date(dobNew);
            var todaysDate = new Date();
            todaysDate.setMonth(todaysDate.getMonth() - (18*12));
            return(inputDate <= todaysDate);
          }
        }

        function validate(dob) {
          if(validateDOB(dob)) {
            ngModel.$pristine = true;
            ngModel.$valid = true;
            ngModel.$invalid = false;
            element.removeClass('ng-invalid');
          }else{
            ngModel.$pristine = false;
            ngModel.$valid = false;
            ngModel.$invalid = true;
            element.addClass('ng-invalid');
          }
        }

        element.on('blur', {scope:scope}, function(/*event*/) {
          var dob = element[0].value;
          validate(dob);
        });
      }
    };
  })
  .directive('currentdate',function(){
    return {
      restrict:'A',
      require:'ngModel',
      link: function(scope,element,attrs,ngModel){
        function currentdateChecker(date){
          var pattern =/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
          if (!pattern.test(date)){
            ngModel.$pristine = false;
            ngModel.$valid = false;
            ngModel.$invalid = true;
            element.addClass('ng-invalid');
            //return false;
          }
          else{
            var dateNew = date.replace(/-/g, '/');
            var inputDate = new Date(dateNew);
            var adate = new Date();
            var todaysDate = new Date(adate.getFullYear(), adate.getMonth(), adate.getDate());
            return(!(inputDate < todaysDate));
          }
        }

        function validate(date) {
          if(currentdateChecker(date)) {
            ngModel.$pristine = true;
            ngModel.$valid = true;
            ngModel.$invalid = false;
            element.removeClass('ng-invalid');
          }else{
            ngModel.$pristine = false;
            ngModel.$valid = false;
            ngModel.$invalid = true;
            element.addClass('ng-invalid');
          }
        }

        scope.$watch(function () {
          return element.val();
        },
        function (newValue, oldValue) {
          if (newValue != oldValue) {
            var date = element[0].value;
            validate(date);
          }
        });

        element.on('blur', {scope:scope}, function(/*event*/) {
          var date = element[0].value;
          validate(date);
        });
      }
    };
  })
  .directive('validSubmit', ['$parse',function ($parse) {
    return {
      compile: function compile(){
        return {
          post: function postLink(scope, element, iAttrs){
            var form = element.controller('form');
            form.$submitted = false;
            var fn =$parse(iAttrs.validSubmit);
            element.on('submit', function(event){
              scope.$apply(function() {
                element.addClass('ng-submitted');
                form.$submitted = true;
                if(form.$valid) {
                  fn(scope, {$event:event});
                }
                else {
                  $('.loader-overlay').fadeOut('fast');
                }
              });
              scope.$watch(function() { return form.$valid; }, function(isValid) {
                if(form.$submitted == false) { return; }
                if(isValid) {
                  element.removeClass('has-error').addClass('has-success');
                } else {
                  element.removeClass('has-success');
                  element.addClass('has-error');
                }
              });
            });
          }
        };
      }
    };
  }])
  .directive('autoSize', [function () {
    return {
      restrict: 'A',
      link: function (scope, iElement) {

        iElement.on('load keyup', function(/*value*/){
          $(this).height(0);
          $(this).height(iElement[0].scrollHeight);
        });
      }
    };
  }])
  .directive('printPdf', [function () {
    return {
      restrict: 'AE',
      link: function (scope, iElement) {
        iElement.bind('click',function(/*event*/){
          window.open(scope.pdfUrl);
        });
      }
    };
  }])
  .directive('printDiv', [function () {
    return {
      restrict: 'A',
      link: function (scope, element, iAttrs) {
        element.bind('click', function(e){
          e.preventDefault();
          var id_to_print = iAttrs.printDiv;
          var content_to_print = $(id_to_print).html();
          var my_window = window.open('','Print Window', 'width = 700, height = 700');
          my_window.document.write(
            '<html><head><title></title><style>@page {margin: 25mm 0mm 25mm 5mm}</style>' + // Your styles here, I needed the margins set up like this
            '</head><body><div>' +
            content_to_print +
            '</div></body></html>'
          );
          my_window.print();
          my_window.close();
        });
      }
    };
  }])
  .directive('checkStrength', [function () {
    return {
      replace:false,
      restrict: 'EACM',
      link: function (scope, iElement, iAttrs) {
        var strength = {
          colors: ['#F00', '#F90', '#FF0', '#9F0', '#0F0'],
          measureStrength: function (p) {
            var _force = 0;
            var _regex = /[$-+@#&$%*()/:-?{}-~!"^_`\[\]]/g ;

            var _lowerLetters = /[a-z]+/.test(p);
            var _upperLetters = /[A-Z]+/.test(p);
            var _numbers = /[0-9]+/.test(p);
            var _symbols = _regex.test(p);

            var _flags = [_lowerLetters, _upperLetters, _numbers, _symbols];
            var _passedMatches = $.grep(_flags, function (el) { return el === true; }).length;

            _force += 2 * p.length + ((p.length >= 6) ? 5 : 0);
            _force += _passedMatches * 10;

            // penality (short password)
            _force = (p.length < 6) ? Math.min(_force, 10) : _force;

            // penality (poor variety of characters)
            _force = (_passedMatches == 1) ? Math.min(_force, 10) : _force;
            _force = (_passedMatches == 2) ? Math.min(_force, 20) : _force;
            _force = (_passedMatches == 3) ? Math.min(_force, 40) : _force;

            return _force;
          },
          getColor: function (s) {
            var idx = 0;
            if (s <= 10) { idx = 0; }
            else if (s <= 20) { idx = 1; }
            else if (s <= 30) { idx = 2; }
            else if (s <= 40) { idx = 3; }
            else { idx = 4; }

            return { idx: idx + 1, col: this.colors[idx] };
          }
        };
        //iElement.css({'display': 'none'});
        iElement.css({'opacity': 0});
        var passTextField = '#'+iAttrs.checkStrength;
        $(passTextField).on('keyup', function(/*event*/){
          if($(passTextField).val() == ''){
            Element.css({'opacity': 0});
            //iElement.css({'display': 'none'});
          }
          else{
            iElement.css({'opacity': 1});
            var c = strength.getColor(strength.measureStrength($(passTextField).val()));
            iElement.children('li')
              .css({ 'background': '#DDD' })
              .slice(0, c.idx)
              .css({ 'background': c.col });
          }
        });
      },
      template: '<li class="point"></li><li class="point"></li><li class="point"></li><li class="point"></li><li class="point"></li>'
      //template: '<li class="password_strength"></li>'
    };
  }])
  .directive('toLower', [function () {
    return {
      restrict: 'A',
      link: function (scope, iElement) {
        iElement.on('blur', function(/*event*/){
          var holy_text=$(this).val();
          holy_text = holy_text.toString().toLowerCase();
          $(this).val(holy_text);
        });
      }
    };
  }])
  .directive('dateFix', ['dateFilter', function(dateFilter) {
    // return the directive link function. (compile function not needed)
    return {
      restrict: 'EA',
      require: 'ngModel', // get a hold of NgModelController

      link: function(scope, element, attrs, ngModel) {

        var format = attrs.datepickerPopup;
        var maxDate = scope[attrs.max];
        var minDate = scope[attrs.min];
        var model = ngModel;

        ngModel.$parsers.push(function(viewValue) {
          var newDate = model.$viewValue;
          var date = null;

          // pass through if we clicked date from popup
          if (typeof newDate === 'object' || newDate == '') { return newDate; }

          // build a new date according to initial localized date format
          if (format === 'MM/dd/yyyy') {
            // extract day, month and year
            var splitted = newDate.split('/');

            var month = parseInt(splitted[0])-1;
            date = new Date(splitted[2], month, splitted[1]);
            //if maxDate,minDate is set make sure we do not allow greater values
            if (maxDate && date > maxDate) { date = maxDate; }
            if (minDate && date < minDate) { date = minDate; }

            model.$setValidity('date', true);
            model.$setViewValue(date);
          }
          return date ? date : viewValue;
        });

        element.on('keydown', {scope:scope, varOpen:attrs.isOpen}, function(e) {
          var response = true;
          // the scope of the date control
          var scope = e.data.scope;
          // the variable name for the open state of the popup (also controls it!)
          var openId = e.data.varOpen;

          switch (e.keyCode) {
          case 13: // ENTER
            scope[openId] = !scope[openId];
            // update manually view
            if (!scope.$$phase) { scope.$apply(); }
            response = false;
            break;

          case 9: // TAB
            scope[openId] = false;
            // update manually view
            if (!scope.$$phase) { scope.$apply(); }
            break;
          }

          return response;
        });

        //set input to the value set in the popup, which can differ if input was manually!
        element.on('blur', {scope:scope}, function(/*event*/) {
          // the value is an object if date has been changed! Otherwise it was set as a string.
          if (typeof model.$viewValue === 'object') {
            element.context.value = isNaN(model.$viewValue) ? '' : dateFilter(model.$viewValue, format);
            if (element.context.value == '') { model.$setValidity('required', false); }
          }
        });
      }
    };
  }])
  .directive('cccodeflag', function(){
    return {
      restrict: 'A',
      link: function(scope, element) {
        element.intlTelInput({autoFormat: true});

        scope.$watch(function() {
          return element.val();
        }, function(newValue){
          element.intlTelInput('setNumber', newValue);
        });
      }
    };
  })
  .directive('blocksit',['$window', function ($window){
    return{
      restrict: 'A',
      link: function(){
        //alert(1);
        $('.founderList row').BlocksIt({
          numOfCol: 3,
          offsetX: 15,
          offsetY: 15
        });
        var w = angular.element($window);
        //var currentWidth = 1100;
        w.resize(function() {
        });
      }
    };
  }])
  .directive('onFinishRender',['$timeout', '$window', function ($timeout, $window) {
    return {
      restrict: 'A',
      link: function (scope) {
        if (scope.$last === true) {
          $timeout(function () {
            var w = angular.element($window);
            var col;

            function callBlocksIt() {
              var winWidth = w.width();
              var conWidth;
              if(winWidth < 992 && winWidth > 768) {
                conWidth = winWidth;
                col = 2;
              } else if(winWidth < 768) {
                conWidth =winWidth;
                col = 1;
              } else if(winWidth > 992){
                conWidth = 1260;
                col = 3;
              }
              $('#founderList').BlocksIt({
                numOfCol: col,
                offsetX: 0,
                offsetY: 15
              });
            }

            callBlocksIt();

            w.resize(function() {
              callBlocksIt();
            });
          });
        }
      }
    };
  }])
  .directive('modalsizingdynamic', function() {
    return{
      restrict: 'A',
      link: function (scope) {
        $('.modal').on('shown.bs.modal', function() {
          var that = $(this);
          var contentHeight = $(this).find('.modal-content').height();
          var footerHeight = $(this).find('.modal-footer').outerHeight() || 0;
          var windowHeight = $(window).height();

          $(this).find('.modal-body').css({
            'max-height': function () {
              if(contentHeight > (windowHeight - 120)) {
                return (windowHeight - 300);
              }else{
                return (contentHeight + 100 - (footerHeight));
              }
            }
          });

          $(this).find('.modal-dialog').css({
            'top': '50%',
            'left': '50%',
            'position': 'absolute',
            'margin': '0px',
            'margin-top': function () {
              return -($(this).outerHeight() / 2);
            },
            'margin-left': function () {
              return -($(this).outerWidth() / 2);
            }
          });

          $(this).find('.close').bind('click', function() {
            $('.modal-body').css('max-height', 'none');
          });

          scope.$watch(function () {
            return that.find('.formWrapper').height();
          },
          function (newValue, oldValue) {
            if (newValue != oldValue) {
              that.find('.modal-body').scrollTop(that.find('.modal-body')[0].scrollHeight);
            }
          });
        });
      }
    };
  })
  .directive('othercompanydoc',['$document', function($document){
    return {
      restrict: 'E',
      // require: '?ngModel',
      // scope: {
      //   choices: '=',
      //   selected: '='
      // },
      templateUrl: 'views/partials/otherCompanyDocs.html',
      replace: true,
      link: function($scope, element){

        $document.bind('click', function(event){
          var isClickedElementChildOfPopup = element
            .find(event.target)
            .length > 0;

          if (isClickedElementChildOfPopup){
            return;
          }

          $scope.$apply();
        });
      }
    };
  }])
  .directive('otherlegaldoc',['$document', function($document){
    return {
      restrict: 'E',
      // require: '?ngModel',
      // scope: {
      //   choices: '=',
      //   selected: '='
      // },
      templateUrl: 'views/partials/otherLegalDocs.html',
      replace: true,
      link: function($scope, element){

        $scope.isLegalVisible = false;

        $scope.toggleSelectLegal = function(){
          $scope.isLegalVisible = !$scope.isLegalVisible;
        };

        $document.bind('click', function(event){
          var isClickedElementChildOfPopup = element
            .find(event.target)
            .length > 0;

          if (isClickedElementChildOfPopup) {
            return;
          }

          $scope.isLegalVisible = false;
          $scope.$apply();
        });
      }
    };
  }])
  .directive('fcsaNumber', function() {
    var addCommasToInteger, commasRegex, controlKeys, hasMultipleDecimals, isNotControlKey, isNotDigit, isNumber, makeIsValid, makeMaxDecimals, makeMaxDigits, makeMaxNumber, makeMinNumber;
    isNumber = function(val) {
      return !isNaN(parseFloat(val)) && isFinite(val);
    };
    isNotDigit = function(which) {
      return which < 45 || which > 57 || which === 47;
    };
    controlKeys = [0, 8, 13];
    isNotControlKey = function(which) {
      return controlKeys.indexOf(which) === -1;
    };
    hasMultipleDecimals = function(val) {
      return (val != null) && val.toString().split('.').length > 2;
    };
    makeMaxDecimals = function(maxDecimals) {
      var regexString, validRegex;
      if (maxDecimals > 0) {
        regexString = '^-?\\d*\\.?\\d{0,' + maxDecimals + '}$';
      } else {
        regexString = '^-?\\d*$';
      }
      validRegex = new RegExp(regexString);
      return function(val) {
        return validRegex.test(val);
      };
    };
    makeMaxNumber = function(maxNumber) {
      return function(val, number) {
        return number <= maxNumber;
      };
    };
    makeMinNumber = function(minNumber) {
      return function(val, number) {
        return number >= minNumber;
      };
    };
    makeMaxDigits = function(maxDigits) {
      var validRegex;
      validRegex = new RegExp('^-?\\d{0,' + maxDigits + '}(\\.\\d*)?$');
      return function(val) {
        return validRegex.test(val);
      };
    };
    makeIsValid = function(options) {
      var validations;
      validations = [];
      if (options.maxDecimals != null) {
        validations.push(makeMaxDecimals(options.maxDecimals));
      }
      if (options.max != null) {
        validations.push(makeMaxNumber(options.max));
      }
      if (options.min != null) {
        validations.push(makeMinNumber(options.min));
      }
      if (options.maxDigits != null) {
        validations.push(makeMaxDigits(options.maxDigits));
      }
      return function(val) {
        var i, number, _i, _ref;
        if (val === '-') {
          return true;
        }
        if (!isNumber(val)) {
          return false;
        }
        if (hasMultipleDecimals(val)) {
          return false;
        }
        number = Number(val);
        for (i = _i = 0, _ref = validations.length; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
          if (!validations[i](val, number)) {
            return false;
          }
        }
        return true;
      };
    };
    commasRegex = /,/g;
    addCommasToInteger = function(val) {
      return val.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    };
    return {
      restrict: 'A',
      require: 'ngModel',
      scope: {
        options: '@fcsaNumber'
      },
      link: function(scope, elem, attrs, ngModelCtrl) {
        var isValid, options;
        options = {};
        if (scope.options != null) {
          options = scope.$eval(scope.options);
        }
        isValid = makeIsValid(options);
        ngModelCtrl.$parsers.unshift(function(viewVal) {
          if (isValid(viewVal) || !viewVal) {
            ngModelCtrl.$setValidity('fcsaNumber', true);
            return viewVal;
          } else {
            ngModelCtrl.$setValidity('fcsaNumber', false);
            return void 0;
          }
        });
        ngModelCtrl.$formatters.push(function(val) {
          if ((options.nullDisplay != null) && (!val || val === '')) {
            return options.nullDisplay;
          }
          if ((val == null) || !isValid(val)) {
            return val;
          }
          ngModelCtrl.$setValidity('fcsaNumber', true);
          return addCommasToInteger(val);
        });
        elem.on('blur', function() {
          var formatter, viewValue, _i, _len, _ref;
          viewValue = ngModelCtrl.$modelValue;
          if ((viewValue == null) || !isValid(viewValue)) {
            return;
          }
          _ref = ngModelCtrl.$formatters;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            formatter = _ref[_i];
            viewValue = formatter(viewValue);
          }
          ngModelCtrl.$viewValue = viewValue;
          return ngModelCtrl.$render();
        });
        elem.on('focus', function(e) {
          var target, val;
          target = angular.element(e.target);
          val = target.val();
          target.val(val.replace(commasRegex, ''));
          return target.select();
        });
        if (options.preventInvalidInput === true) {
          return elem.on('keypress', function(e) {
            if (isNotDigit(e.which && isNotControlKey(e.which))) {
              return e.preventDefault();
            }
          });
        }
      }
    };
  })
  .directive('manageInvestor', function(){
    return {
      restrict: 'A',
      link: function(scope,element){
        element.on('click',function(/*event*/){
          $('.investor-data-list-wrapper, .investor-manager-wrapper').toggleClass('hide');
        });
      }
    };
  })
  .directive('manageCompany', [function () {
    return {
      restrict: 'A',
      link: function (scope, element) {
        element.on('click',function(/*event*/){
          $('.company_list_container, .company-manager-wrapper').toggleClass('hide');
        });
      }
    };
  }])
 .directive('cvcDisableAnimation', ["$animate", function($animate){
    // used to disable ng-animation of carousel due to https://github.com/angular-ui/bootstrap/issues/1350.
    return {
      restrict: 'A',
      link: function($scope, $element, $attrs){
        $attrs.$observe('cvcDisableAnimation', function(value){
          $animate.enabled(!value, $element);
        });
      }
    };
  }]);

'use strict';

/* Directives */

angular.module('myApp.directives').
// directive for pdf viewer
directive('pdfViewer', ['logging', 'Files', '$q', function(logging, Files, $q) {
  var log = logging.namespace('pdf-viewer');
  return {
    restrict:'E',

    templateUrl: function(element, attr) {
      return attr.templateUrl ? attr.templateUrl : 'partials/viewer.html';
    },

    link: function(scope, element, attrs) {
      //
      // Disable workers to avoid yet another cross-origin issue (workers need
      // the URL of the script to be loaded, and dynamically loading a cross-origin
      // script does not work).
      //
      // PDFJS.disableWorker = true;
      var
        pdfUrl = scope.$parent.pdfUrl || '', //explicitly showing that pdfUrl is from parent scope
        fileId = attrs.fileId || '',
        urlOrFileId = fileId || pdfUrl,
        pdfDoc = null,
        pageNum = 1,
        pageRendering = false,
        pageNumPending = null,
        scale = attrs.scale ? attrs.scale : 0.8,
        canvas = document.getElementById('pdf-viewer-canvas'),
        ctx = canvas ? canvas.getContext('2d') : null,
        STATUS = {
          'START': 'PDF_VIEWER_START',
          'DONE': 'PDF_VIEWER_DONE'
        };

      scope.pdfViewerStatus = null;
      scope.$watch('pdfViewerStatus', function(newV){
        switch (newV) {
          case STATUS.START:
            $('.loader-overlay-2').fadeIn('fast');
            break;

          case STATUS.DONE:
            $('.loader-overlay-2').fadeOut('fast');
            break;
        }
      });

      var fetchDocumentInitParameters = function() {
        if (Files.isFileId(urlOrFileId)) {

          scope.pdfViewerStatus = STATUS.START;

          log('Rendering file ' + urlOrFileId + ' as watermarked PDF');
          return Files.get(urlOrFileId, {params: {fmt: Files.Formats.WATERMARKED}})
            .then(function(rawFile) {
              return {
                data: rawFile.uint8Array()
              };
            }, function(){
              scope.pdfViewerStatus = STATUS.DONE; //failure case
            });
        } else {
          log('Rendering URL ' + urlOrFileId + ' as PDF.');
          return $q(function(resolve) {

            resolve({url: urlOrFileId});
          });
        }
      };

      if (ctx === null) {
        return;
      }

      scope.pageNum = pageNum;

      /**
       * Get page info from document, resize canvas accordingly, and render page.
       * @param num Page number.
       */
      scope.renderPage = function (num) {
        pageRendering = true;
        // Using promise to fetch the page
        pdfDoc.getPage(num).then(function(page) {
          var viewport = (scale === 1 && $(window).width() < 800) ? page.getViewport(canvas.width / page.getViewport(1.0).width) : page.getViewport(scale);

          canvas.height = viewport.height;
          canvas.width = viewport.width;

          // Render PDF page into canvas context
          var renderContext = {
            canvasContext: ctx,
            viewport: viewport
          };

          var renderTask = page.render(renderContext);

          // Wait for rendering to finish
          renderTask.promise.then(function () {
            pageRendering = false;
            if (pageNumPending !== null) {
              // New page rendering is pending
              scope.renderPage(pageNumPending);
              pageNumPending = null;
            }
            scope.$apply(function() {
              scope.pageNum = pageNum;
              scope.pdfViewerStatus = STATUS.DONE;
            });
          });
        });
      };

      /**
       * If another page rendering in progress, waits until the rendering is
       * finised. Otherwise, executes rendering immediately.
       */
      scope.queueRenderPage = function(num) {
        if (pageRendering) {
          pageNumPending = num;
        } else {
          scope.renderPage(num);
        }
      };

      /**
       * Displays previous page.
       */
      scope.goPrevPage = function () {
        if (pageNum <= 1) {
          return;
        }
        pageNum--;
        scope.queueRenderPage(pageNum);
      };

      /**
       * Displays next page.
       */
      scope.goNextPage = function () {
        if (pageNum >= pdfDoc.numPages) {
          return;
        }
        pageNum++;
        scope.queueRenderPage(pageNum);
      };

      scope.rotate = function() {
        if (canvas.getAttribute('class') === 'rotate0') {
          canvas.setAttribute('class', 'rotate90');
        } else if (canvas.getAttribute('class') === 'rotate90') {
          canvas.setAttribute('class', 'rotate180');
        } else if (canvas.getAttribute('class') === 'rotate180') {
          canvas.setAttribute('class', 'rotate270');
        } else {
          canvas.setAttribute('class', 'rotate0');
        }
      };


      scope.zoomIn = function() {
        scale = parseFloat(scale) + 0.2;
        scope.queueRenderPage(pageNum);
        return scale;
      };

      scope.zoomOut = function() {
        scale = parseFloat(scale) - 0.2;
        scope.queueRenderPage(pageNum);
        return scale;
      };

      scope.goToPage = function() {
        var num = parseInt(scope.pageNum);

        if (num > 0 && num <= scope.pageCount) {
          pageNum = num;
          scope.queueRenderPage(num);
        }
      };

      /**
       * Asynchronously downloads PDF.
       */

      fetchDocumentInitParameters()
        .then(function(documentInitParameters) {
          return PDFJS.getDocument(documentInitParameters);
        })
        .then(function(_pdfDoc) {
          pdfDoc = _pdfDoc;
          scope.pageCount = pdfDoc.numPages;

          // Initial/first page rendering
          scope.renderPage(pageNum);
        });
    }
  };
}]);



'use strict';

/**
 * PDF Viewer Standalone Controller
 * This is only used for viewing pdf in standalone mode aka non-kuliza mode
 */
angular.module('myApp.controllers').controller(
'PDFViewerStandaloneController',
["$scope", "$routeParams", function($scope, $routeParams) {
  $scope.theme = 'v2';
  $scope.fileId = $routeParams.fileId;
}]);

'use strict';

/**
 * cvc-chrome directive. Render our standard header, footer, and associated behavior.
 *
 * Usage:
 *   <cvc-chrome>page body</cvc-chrome>
 *   <cvc-chrome enable-sign-in="false" mode="brokerage">page body</cvc-chrome>
 *
 * @attribute {boolean} enable-sign-in - Turns on/off the sign-in
 * @attribute {string} mode - Change the chrome into different modes.
 *                            By default, it's an empty string. Use "brokerage"
 *                            to go into brokerage mode.
 */
angular.module('myApp.directives').directive('cvcChrome', ["$document", "logging", "Brokerages", function($document, logging, Brokerages) {
  var log = logging.namespace('cvcChrome');
  return {
    restrict: 'E',
    scope: {
      enableSignIn: '=?',
      theme: '=?', // default will be 'v2'
      title: '@?pageTitle',
      mode: '=?',
      forceFullPage: '=?' //default will be false
    },
    templateUrl: 'components/cvc-chrome/cvc-chrome.html',
    transclude: 'element',
    replace: true,
    link: function(scope, element, attr, ctrl, transclude) {
      var DEFAULT_TITLE = 'citizen.vc';
      $document[0].title = DEFAULT_TITLE;
      attr.$observe('pageTitle', function(newPageTitle) {
        var title;
        if (!newPageTitle || newPageTitle === '') {
          // if title is undefined, then default to citizen.vc
          title = DEFAULT_TITLE;
        } else {
          title = 'citizen.vc | ' + newPageTitle;
        }

        $document[0].title = title;
      });

      $document.bind('click', function(event){
        var isClickedElementChildOfPopup = element
          .find(event.target)
          .length > 0;

        if (isClickedElementChildOfPopup) {
          return;
        }
      });

      scope.$parent.forceFullPage = scope.forceFullPage;
      // Tie the transcluded dom to the Controller's scope  (this scope's parent)
      // instead of creating a sub-scope for the transcluded dom.
      // This helps the controller scope access form elements created in the transcluded html.
      transclude(scope.$parent, function(transcludedContent, currScope) {
        var chromeContent = element.find('.cvc-chrome-content');
        if (currScope.forceFullPage) {
          chromeContent.addClass('force-full-page');
        }
        chromeContent.append(transcludedContent);
      });
    },
    controller: '_CvcChromeController'
  };
}])
.controller(
'_CvcChromeController',
["$scope", "MSG", "URLS", "$location", "FlashOnNextRoute", "Auth", "Users", "Brokerages", "logging", function($scope, MSG, URLS, $location, FlashOnNextRoute, Auth, Users, Brokerages, logging) {
  var log = logging.namespace('_CvcChromeController');
  $scope.theme = ($scope.theme || 'v2');
  var authedUser = Users.authorized();
  var userIsLoggedIn = (authedUser !== undefined && authedUser !== null);
  var pageSupportsSignin = ($scope.enableSignIn || $scope.enableSignIn === undefined);
  var brokerage;
  var showSlideInMenu = false;
  var MODES = {
    BROKERAGE : 'BROKERAGE'
  };

  var SubnavLink = function(path, text, id, tooltip) {
    this.path = path;
    this.text = text;
    this.id = id;
    this.tooltip = tooltip;
  };

  SubnavLink.prototype = {
    isCurrent: function() {
      return $location.path() === this.path;
    },

    url: function() {
      return URLS.url(this.path);
    }
  };

  var _subnavLinks = function() {
    var links = [];
    var isPermittedNoSuperusers = function(permission) {
      return authedUser.hasPermission(permission, {forceSuperuserAccess: false});
    };
    var numAdministratedBrokerages;
    if (authedUser) {

      if (isPermittedNoSuperusers('can_view_investor_dashboard')) {
        links.push(new SubnavLink(URLS.INVESTOR_DASHBOARD, 'Investments'));
      }

      if (isPermittedNoSuperusers('can_administrate_company')) {
        links.push(new SubnavLink(authedUser.homeUrl(), 'My Company'));
      }

      if (isPermittedNoSuperusers('can_administrate_site')) {
        links.push(new SubnavLink(URLS.INVITE, 'Invite'));
        links.push(new SubnavLink(URLS.ADMIN_DASHBOARD, 'Dashboard'));
      }

      if (authedUser.hasPermission('can_administrate_brokerages')) {
        numAdministratedBrokerages = authedUser.administratedBrokerageIds().length;
        if (numAdministratedBrokerages > 1 || authedUser.isSuperuser()) {
          links.push(new SubnavLink(URLS.FIRMS, 'Firms'));
        }

        if (numAdministratedBrokerages === 1 && brokerage) {
          links.push(new SubnavLink('#', 'Deals', 'chrome-deals', 'Deal management available soon.'));
          links.push(new SubnavLink(URLS.FIRM_SETTINGS(brokerage.slug()), 'Firm', 'chrome-firm'));
        }
      }
    }

    return links;
  };

  if (authedUser) {
    Brokerages.active().then(function(result) {
      brokerage = result;
      $scope.mode = MODES.BROKERAGE;
      $scope.subnavLinks = _subnavLinks();
      result.onBrandChange($scope, function(updatedBrokerage) {
        brokerage = updatedBrokerage;
      });
    });
  }

  $scope.user = authedUser;
  $scope.signInUrl = URLS.url(URLS.SIGN_IN);
  $scope.showSignInLink = pageSupportsSignin && !userIsLoggedIn;
  $scope.urls = URLS;
  $scope.loggedIn = userIsLoggedIn;
  $scope.enableSlideinMenu = userIsLoggedIn;

  $scope.subnavLinks = _subnavLinks();

  $scope.footerLinks = [
    new SubnavLink(URLS.ABOUT, 'About Us'),
    new SubnavLink(URLS.TEAMS, 'Team'),
    new SubnavLink(URLS.ADVISORS, 'Advisors'),
    new SubnavLink(URLS.CONTACT_US, 'Contact')
  ];

  $scope.brokerageLogoFileId = function() {
    var showBrokerageLogo = brokerage && !brokerage.isDefault();
    if (showBrokerageLogo) {
      return brokerage.logo();
    }
  };

  $scope.brokerageLink = function() {
    if (brokerage && authedUser) {
      if (authedUser.isInGroup(Users.Groups.BROKERAGE_ADMINS)) {
        return URLS.FIRM_SETTINGS(brokerage.slug());
      } else {
        return authedUser.homeUrl();
      }
    }
  };

  $scope.logout = function() {
    FlashOnNextRoute.setMessage({type: 'neutral', text: MSG.LOGOUT});
    brokerage = undefined;
    Auth.logout();
  };

  $scope.showSlideInMenu = function() {
    return showSlideInMenu;
  };

  $scope.slideInMenuClass = function() {
    if (showSlideInMenu) {
      return 'open';
    }
  };

  $scope.homeUrl = function() {
    if (authedUser) {
      if (authedUser.isInGroup(Users.Groups.BROKERAGE_ADMINS) && brokerage) {
        return $scope.brokerageLink();
      } else {
        return authedUser.homeUrl();
      }
    } else {
      return '/';
    }
  };

  $scope.toggleShowSlideInMenu = function() {
    showSlideInMenu = !showSlideInMenu;
  };
}]);

'use strict';

/** A material-based loading spinner */
angular.module('myApp.directives')
.directive('loader', [
  function() {
    return {
      scope: {
        defaultPosition: '='
      },
      template: '<div class="loader" ng-class="{\'loader-default-position\': defaultPosition}">' +
                  '<svg class="circular">' +
                    '<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="5" />' +
                  '</svg>' +
                '</div>'
    };
  }
]);

'use strict';

/**
 * Specify an image src from a file ID stored in the citizen.vc files API.
 *
 * Securely downloads the image from the file API and puts its contents into
 * the image's "src" attribute as a data URI.
 *
 * Usage:
 *   <img file-api-src="'file:1'">
 *   <img file-api-src="myScopeFile.id()">
 */
angular.module('myApp.directives').directive(
'fileApiSrc',
["logging", "Files", "ENV", function(logging, Files, ENV) {
  var log = logging.namespace('fileApiSrc');
  return {
    restrict: 'A',
    scope: {
      fileApiSrc: '='
    },
    link: function(scope, elem) {
      scope.$watch('fileApiSrc', function(fileId) {
        if (!fileId) {
          elem.attr('src', ENV.emptyImageDataUri);
        } else {
          log('Rendering an image with file ID === ' + scope.fileApiSrc);

          var filePromise = Files.get(fileId, {
            params: {
              fmt: Files.Formats.RAW
            }
          });
          filePromise.then(function(rawFile) {
            elem.attr('src', rawFile.dataUri());
          });
        }
      });
    }
  };
}]);

'use strict';

/*global angular*/
/**
 * UI Element for managing file upload.
 *
 * Under the hood it achieves the following via two-way binding:
 *   * Provides a drop-box and button for specifying a new file to upload
 *   * Once a file is selected, automatically uploads it to the citizen.vc files API.
 *   * Once upload is complete, it updates the "ng-model" with the created file's ID.
 *   * Provides a way to delete the file altogether (reappearing the dropbox)
 *   * If the ng-model is updated by external processes to point to a new file ID, it will
 *     automatically update and present the new file ID contents.
 *
 * Usage:
 *  <form name="myFileForm" cvc-form cvc-form-type="my_form_type">
 *    <file-upload name="logo" // The ngModelController will be available on the form by this name
 *                 ng-model="myFile.file_id" // The selected File ID will be inserted at these coordinates on the scope
 *                 controls="true" // true if controls should be shown. Otherwise it will just show the image.
 *                 initializing="someModel" // optional. if set, the view will not populate until this value
 *                                          // is truthy and the initial value has been populated with a thumbnail.
 *                 cvc-form-field> // Changes will automatically be synced with the remote server (works along with resource-form)
 *  </form>
 *
 */
angular.module('myApp.directives')
.directive('fileUpload',
["logging", "Files", "$q", "$interval", "$timeout", "ENV", function(logging, Files, $q, $interval, $timeout, ENV) {
  var log = logging.namespace('resource-form');
  var EMPTY_DATA_URI = ENV.emptyImageDataUri;
  return {
    restrict: 'E',
    scope: {
      controls: '=?',
      clientInitializing: '=?initializing',
    },
    templateUrl: 'components/file-upload/file-upload.html',
    require: ['ngModel'],
    link: function(scope, element, attrs, requirements) {
      var ngModel = requirements[0];
      var fileUpload = {
        //
        // Non-scope state
        //
        hasSelectedFile: false,
        thumbnailIsReady: false,
        currentFile: null,

        //
        // Non-scope functionality
        //
        toggleLoading: function(promise) {
          scope.uploadingImage = true;
          var uploadComplete = function() { scope.uploadingImage = false; };
          promise.then(uploadComplete, uploadComplete);
        },

        createThumbnail: function(droppedFile) {
          // Read the file data as base64 if its an image
          return $q(function(resolve, reject) {
            if (droppedFile === null) {
              reject('Dropped file was inaccessible');
            } else  {
              if (droppedFile.type.indexOf('image') > -1) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL(droppedFile);
                fileReader.onload = function(e) {
                  resolve(e.target.result);
                };
              } else {
                reject('Can not generate thumbnail from non-image');
              }
            }
          });
        },

        unsetThumbnailData: function() {
          // Don't unset the thumbnail data until animations are complete
          var resolutionPromise = $interval(function() {
            var viewModeElement = element.find('.view-file-mode');

            var stillHiding = viewModeElement.hasClass('ng-hide-animate');
            if (!stillHiding) {
              log('Finally zeroing out the thumbnailUri');
              $interval.cancel(resolutionPromise);
              scope.thumbnailUri = EMPTY_DATA_URI;
            }
          }, 50);
        },

        hasThumbnailData: function() {
          return scope.thumbnailUri !== EMPTY_DATA_URI;
        },

        // Executing a function to inNonInteractiveMode disables animations that would have
        // normally occurred due to side-effects from the provided function.
        inNonInteractiveMode: function(fn) {
          element.addClass('non-interactive');
          fn();
          var resolutionPromise = $interval(function() {
            var stillAnimating = element.find('.ng-animate').length > 0;
            if (!stillAnimating) {
              $interval.cancel(resolutionPromise);
              element.removeClass('non-interactive');
            }
          }, 50);
        }
      };

      //
      // Initialize scope state
      //
      if (scope.controls === undefined) {
        scope.controls = true;
      }
      if (scope.clientInitializing === undefined) {
        scope.clientInitializing = false;
      }
      scope.initializing = true;

      //
      // Watch for client-initialized state and updates to the model
      //
      scope.$watch('clientInitializing', function(clientInitializing) {
        if (!clientInitializing && !ngModel.$viewValue) {
          // If client is done initializing and there is no file to load
          // then we can go ahead and display our widget
          scope.initializing = false;
        }
      });

      ngModel.$render = function() {
        var fileId = ngModel.$viewValue;
        if (fileId) {
          fileUpload.hasSelectedFile = true;
          Files.get(fileId, {params: {fmt: Files.Formats.RAW}}).then(function(rawFile) {
            fileUpload.inNonInteractiveMode(function() {
              fileUpload.thumbnailIsReady = true;
              scope.initializing = false;
              if (rawFile.isImage()) {
                scope.thumbnailUri = rawFile.dataUri();
              }
            });
          });
          Files.get(fileId).then(function(file) {
            fileUpload.currentFile = file;
          });
        } else {
          log('Unsetting the current file');
          fileUpload.inNonInteractiveMode(function() {
            scope.discardCurrentFile();
          });
        }
      };


      //
      // Watch for user-driven changes
      //
      scope.$watch('selectedFile', function(files) {
        if (!files || files.length < 1) {
          log('Not uploading because no file was selected');
          return;
        }
        var file = files[0];
        var uploadPromise = Files.upload(file);
        ngModel.$setTouched(true);
        fileUpload.createThumbnail(file).then(function(dataUri) {
          scope.thumbnailUri = dataUri;
        }).finally(function() {
          fileUpload.hasSelectedFile = true;
          fileUpload.thumbnailIsReady = true;
        });
        uploadPromise.progress(function(event) {
          log('File upload at ' + 100 * (event.loaded / event.total) + '% complete');
          log(event.loaded);
        }).then(function(cvcFile) {
          log('File upload complete');
          fileUpload.currentFile = cvcFile;
          ngModel.$setViewValue(cvcFile.id());
        });

        fileUpload.toggleLoading(uploadPromise);
      });

      scope.isVisible = function() {
        return !scope.initializing;
      };

      scope.showFileSelector = function() {
        return !scope.showImageView();
      };

      scope.showImageView = function() {
        return fileUpload.hasSelectedFile && fileUpload.thumbnailIsReady;
      };

      scope.showDefaultThumbnail = function() {
        return fileUpload.hasSelectedFile && !fileUpload.hasThumbnailData();
      };

      scope.showControls = function() {
        return scope.controls;
      };

      scope.discardCurrentFile = function() {
        fileUpload.currentFile = null;
        fileUpload.hasSelectedFile = false;
        ngModel.$setViewValue(undefined);
        fileUpload.unsetThumbnailData();
      };
    }
  };
}]);



'use strict';

/*
  Minimize Content by:

  1. Text Height

  2. Number of dom elements eg LI elements in given a parent UL/OL.
     Get content height and then use overflow: hidden
*/

angular.module('myApp.directives').
  // directive for minimize-content
  directive('minimizeContent',function(){
  return {
    restrict:'E',
    transclude: true,
    scope: true,
    templateUrl: 'components/minimize-content/minimize-content.html',
    link: function(instanceScope, $element, $attrs) {

      instanceScope.init = function () {
        instanceScope.mode = $attrs.ngMode ? $attrs.ngMode : 'text';
        instanceScope.toggled = false;
        instanceScope.textLength = $attrs.ngTextLength ? $attrs.ngTextLength : 0;
        instanceScope.numNodesToHide = $attrs.ngNumNodesToHide ? $attrs.ngNumNodesToHide : 0;
        instanceScope.toggle();

        $element.find('.toggle').on('click', function(evt){
          var target = evt.target;
          instanceScope.toggle(target);
        });
      };

      instanceScope.toggle = function () {
        instanceScope.toggleCTAButton();

        switch (instanceScope.mode) {
        case 'text':
          instanceScope.toggleText(instanceScope.toggled);
          break;

        case 'dom':
          instanceScope.toggleDomNode(instanceScope.toggled);
          break;
        }

        if (instanceScope.toggled) {
          instanceScope.toggled = false;
        } else {
          instanceScope.toggled = true;
        }

      };

      instanceScope.toggleCTAButton = function() {
        var target = $element.find('.toggle'),
        $target = $(target),
        $content = $element.find('.content'),
        string = $content.html(),
        $node = $element.find('.minimize-content .content'),
        children = $node.find('.date-item');

        switch (instanceScope.mode) {
          case 'text':
            if (instanceScope.textLength > 0 ) {
              if (instanceScope.textLength < string.length) {
                $target.addClass('on');
              }
            }
            break;

          case 'dom':
            if (instanceScope.numNodesToHide < children.length) {
              $target.addClass('on');
            }
            break;
        }

        if (instanceScope.toggled) {
          $target.html('Collapse');
        } else {
          $target.html('Read More');
        }
      };

      instanceScope.toggleText = function(flag) {
        var $content = $element.find('.content');

        if (flag === false) { //truncate
          if (!instanceScope.originalText) {
            instanceScope.originalText = $content.html();
          }

          if (!instanceScope.truncatedText) {
            instanceScope.truncatedText = instanceScope.truncateText(instanceScope.originalText);
          }

          //display truncated text
          instanceScope.displayText(instanceScope.truncatedText);

        } else { //show all text
          //display original text
          instanceScope.displayText(instanceScope.originalText);
        }
      };

      instanceScope.truncateText = function (text) {
        var len = instanceScope.textLength;

        if (text.length > len) {
          return text.substring(0, len) + '...';
        } else {
          return text;
        }
      };

      instanceScope.displayText = function (text) {
        var $content = $element.find('.content');

        $content.html(text);
      };

      instanceScope.toggleDomNode = function(flag) {

        if (flag === false) {
          //set a few child nodes to display none
          instanceScope.displayChildNodes(false);
        } else {
          //set all child nodes to display block
          instanceScope.displayChildNodes(true);
        }
      };

      instanceScope.displayChildNodes = function(show){
        var numNodesToHide,
            len,
            startIdx,
            $node,
            children;

        $node = $element.find('.minimize-content .content');
        children = $node.find('.date-item');
        len = children.length;

        if (show === true || len < 4) { //show if show = true or there are only 3 items
          for (var i = 0; i < len; i++) {
            $(children[i]).css('display', 'block');
          }

        } else {
          numNodesToHide = instanceScope.numNodesToHide;

          //if nodes to hide  > length of tree, just use half
          if (numNodesToHide >= len) {
            startIdx = Math.ceil(len / 2);
          } else {
            startIdx = len - numNodesToHide;
          }

          for (var ii = startIdx; ii < len; ii++) {
            $(children[ii]).css('display', 'none');
          }
        }
      };

      instanceScope.$watch('companyHighlights', function(newValue) {
        if (typeof newValue === 'object') {
          setTimeout(function(){
            instanceScope.init();
          }, 0);
        }
      });

    }

  };

});



'use strict';

/**
 * Angular service that enables shimming of config files at real-time. Generally used for selenium testgs.
 * 
 * Usage: as long as the deployed version of Gruntfile.js contains configuration "enableRealtimeConfig: true",
 *        then you can set window.citizenvc.config["your config name"] = "your config value", and the application will
 *        immediately start responding to it (where it hadn't already cached previous config values)
 */
angular.module('myApp.services').service('cvcConfig', ['ENV', function(ENV) {
  window.citizenvc = window.citizenvc || {};
  window.citizenvc.config = window.citizenvc.config || {};
  var realtimeConfig = window.citizenvc.config;

  return {
    get: function(configName, defaultValue) {
      var result;

      if (ENV.enableRealtimeConfig) {
        result = realtimeConfig[configName];
      }
      if (result === undefined) {
        result = ENV[configName];
      }
      if (result === undefined) {
        result = defaultValue;
      }

      return result;
    }
  };
}]);

'use strict';

/**
 * Controller for the advisors page (/advisors)
 */
angular.module('myApp.controllers').controller(
'AdvisorsController',
['$scope',
function($scope) {
  $scope.theme = 'v2';

}]);

'use strict';

/**
 * Controller for the not found / 404 page
 */
angular.module('myApp.controllers').controller(
'NotFoundController',
['$scope',
function($scope) {
  $scope.theme = 'v2';

}]);

'use strict';

/**
 * Controller for the list of brokerages. This should be visible only to
 * admins and brokerage-admins who administrate more than 1 brokerage.
 */
angular.module('myApp.controllers').controller(
'BrokeragesPageController',
["$scope", "Users", "Brokerages", "logging", "$modal", "URLS", function($scope, Users, Brokerages, logging, $modal, URLS) {
  $scope.theme = 'v2';

  var log = logging.namespace('BrokeragesPageController');
  var user = Users.authorized();

  var updateBrokerages = function() {
    log('Updating brokerage list...');
    var brokeragesPromise = user.isSuperuser()? Brokerages.all(): Brokerages.administrated();
    $scope.loading = true;

    brokeragesPromise.then(function(brokerages) {
      var alphabeticalBrokerages = _.sortBy(brokerages, function(brokerage) {
        return brokerage.name().toLowerCase();
      });

      $scope.brokerages = alphabeticalBrokerages;
    });
  };

  $scope.brokerageUrl = function(brokerage) {
    return URLS.FIRM_SETTINGS(brokerage.slug());
  };

  $scope.showAddBrokerageDialog = function() {
    var modalInstance = $modal.open({
      templateUrl: 'components/brokerage-modules/add-brokerage-dialog.html',
      controller: '_BrokerageAddController',
      size: 'sm'
    });

    modalInstance.result.then(function(newBrokerageName) {
      log('Brokerage was created: "' + newBrokerageName);
      updateBrokerages();
    }, function() {
      log('Brokerage creation canceled and dialog dismissed');
    });
  };

  // Initialization
  $scope.canCreateBrokerages = user.hasPermission('can_create_brokerages');
  $scope.loading = false;

  updateBrokerages();
}])
.controller(
'_BrokerageAddController',
["$scope", "Users", "$modalInstance", function($scope, Users, $modalInstance) {
  $scope.cancel = function() {
    $scope.newBrokerageForm.$cvcForm.$dismissChanges();
    $modalInstance.dismiss('cancel');
  };

  $scope.submitForm = function() {
    $scope.newBrokerageForm.$cvcForm.$submit().then(function() {
      $modalInstance.close($scope.newBrokerage.name);
    });
  };
}]);

'use strict';

/**
 * Controller for the brokerage settings page (a page for admins and brokerage-admins only)
 */
angular.module('myApp.controllers').controller(
'BrokerageSettingsPageController',
["$scope", "$routeParams", "Brokerages", "logging", "$timeout", "Users", "$interpolate", "Tours", function($scope, $routeParams, Brokerages, logging, $timeout, Users, $interpolate, Tours) {
  var user = Users.authorized();
  var log = logging.namespace('BrokerageSettingsPageController');
  $scope.theme = 'v2';

  Brokerages.getByUniqueKey($routeParams.firmSlug).then(function(brokerage) {
    $scope.brokerage = brokerage;

    _handleTour();
  });

  var _handleTour = function() {
    var tour = Tours.firmPageTour({
      firstName: function() {
        return user.firstName();
      }
    });
    var tourHasNeverBeenCompleted = !tour.isCompleted();
    var enterTour = $routeParams.tour || tourHasNeverBeenCompleted;
    var logCompletionWhenDone = !$routeParams.tour && tourHasNeverBeenCompleted;

    if (enterTour) {
      $timeout(function() {
        tour.start().then(function() {
          if (logCompletionWhenDone) {
            log('Logging tour "' + tour.id() + '" as complete');
            tour.setCompleted();
          } else {
            log('Not logging tour "' + tour.id() + '" as complete because it was entered via a routeParam rather than organically.');
          }
        });
      }, 1000);
    }
  };

  $scope.manuallyTriggerTour = function() {
    var tour = Tours.create({
      steps: Tours.steps({match: /^firm/})
    });
    tour.start();
  };
}]);

'use strict';

/**
 * Directive and controllers for brokerage administration in the brokerage admin panel.
 */
angular.module('myApp.directives')
.directive('brokerageInformation',
function() {
  return {
    restrict: 'A',
    scope: {
      brokerage: '='
    },
    controller: '_BrokerageInformationController',
    templateUrl: 'components/brokerage-modules/brokerage-information.html'
  };
})
.controller('_BrokerageInformationController',
["$scope", "logging", "Files", "Brokerages", "$location", "Tours", function($scope, logging, Files, Brokerages, $location, Tours) {
  var log = logging.namespace('_BrokerageInformationController');
  var whileLoading = function(fn) {
    var stopLoading = function(value) {
      $scope.loading = false;
      return value;
    };

    $scope.loading = true;
    return fn().then(stopLoading, stopLoading);
  };

  $scope.saveBrand = function() {
    log('Saving brand form');
    var submitPromise = $scope.brokerageBrandForm.$cvcForm.$submit();
    submitPromise
      .then(function() { return $scope.brokerage.refresh(); })
      .then(function(updatedBrokerage) {
        var oldBrokerage = $scope.brokerage;
        var brandHasChanged = !oldBrokerage.brandMatches(updatedBrokerage);
        var url = $location.url();
        var locationContainsUrl = url.indexOf(oldBrokerage.slug()) !== -1;
        var newUrl;
        updatedBrokerage.announceHasNewBrand();

        if (locationContainsUrl) {
          newUrl = url.replace(oldBrokerage.slug(), updatedBrokerage.slug());
          $location.url(newUrl);
        }
      }
    );
  };

  $scope.dismissBrandChanges = function() {
    log('Dismissing updates to brand');
    $scope.brokerageBrandForm.$cvcForm.$dismissChanges().then(function() {
      log('Dismissed updates to brand');
    });
  };

  $scope.toggleEditMode = function() {
    var isWorkInProgress = $scope.brokerageBrandForm.$cvcForm.$isWorkInProgress();
    var cvcForm = $scope.brokerageBrandForm.$cvcForm;
    if (isWorkInProgress) {
      whileLoading(function() {
        return cvcForm.$dismissChanges();
      });
    } else {
      cvcForm.$setWorkInProgress();
    }
  };

  $scope.mode = function() {
    var isWorkInProgress= $scope.brokerageBrandForm.$cvcForm.$isWorkInProgress();
    return isWorkInProgress? 'edit': 'view';
  };

  $scope.startSettingsAndPreferencesTour = function() {
    var tour = Tours.create({
      steps: Tours.steps({ match: /^firm.SettingsAndPreferences/ })
    });

    tour.start();
  };

  $scope.$watch('brokerage', function(newBrokerage) {
    if (newBrokerage) {
      log('new brokerage is ' + newBrokerage.name());
    }
    log('setting new brokerage to:');
    log(newBrokerage);
    $scope.brokerage = newBrokerage;
  });
}]);

'use strict';

/**
 * Directive and controllers for adding and removing brokerage administrators
 */
angular.module('myApp.directives')
.directive('brokerageAdministrators',
function() {
  return {
    restrict: 'A',
    scope: {
      brokerage: '='
    },
    controller: '_BrokerageAdministratorsController',
    templateUrl: 'components/brokerage-modules/brokerage-administrators.html'
  };
})
.controller('_BrokerageAdministratorsController',
["$scope", "logging", "Files", "Brokerages", "$modal", "PersonCardModels", "Users", function($scope, logging, Files, Brokerages, $modal, PersonCardModels, Users) {
  var log = logging.namespace('_BrokerageAdministratorsController');
  var _authUser = Users.authorized();
  var _notifyRemoveAdminFailed = false;

  var getAdmins = function() {
    return $scope.brokerage.admins().then(function(admins) {
      $scope.admins = admins;

      return admins;
    });
  };

  var personCardModel = function(admin) {
    var cardModel = PersonCardModels.createFromUser(admin);
    if (admin.id() !== Users.authorized().id()) {
      cardModel.setBehavior({
        onRemove: function() {
          var oldAdmins = $scope.admins;
          $scope.admins = _.filter(oldAdmins, function(oldAdmin) {
            return oldAdmin.id() !== admin.id();
          });

          $scope.brokerage.removeAdmin(admin.id())
            .then(function removeAdminSuccess(newBrokerage) {
              $scope.brokerage = newBrokerage;
              getAdmins();
            }, function removeAdminFail() {
              $scope.admins = oldAdmins;
              _notifyRemoveAdminFailed = true;
            });
        }
      });
    }

    return cardModel;
  };

  $scope.closeAdminsAddedNotification = function() {
    $scope.displayNumAdminsAdded = false;
  };

  $scope.openAddAdmins = function() {
    var opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      scope: $scope,
      controller: '_BrokerageAdministratorsAddModalController',
      templateUrl : 'components/brokerage-modules/add-admins-dialog.html',
      resolve: {} // empty storage
    };

    var addAdminsModal = $modal.open(opts);

    addAdminsModal.result.then(function(numAdminsAdded) {
      log('Added ' + numAdminsAdded + ' admins, yo!');
      $scope.numAdminsAdded = numAdminsAdded;
      $scope.displayNumAdminsAdded = true;
      getAdmins();
    });
  };

  $scope.showRemoveAdminFailedNotification = function() {
    return _notifyRemoveAdminFailed;
  };

  $scope.acknowledgeRemoveAdminFailed = function() {
    _notifyRemoveAdminFailed = false;
  };

  $scope.$watch('brokerage', function(newBrokerage) {
    if (newBrokerage) {
      getAdmins();
    }
  });

  $scope.$watch('admins', function(newAdmins) {
    $scope.cardModelsByAdminId = {};

    _.forEach(newAdmins, function(admin) {
      $scope.cardModelsByAdminId[admin.id()] = personCardModel(admin);
    });
  });
}])
.controller('_BrokerageAdministratorsAddModalController',
["$scope", "$modalInstance", "logging", "$timeout", function($scope, $modalInstance, logging, $timeout) {
  var log = logging.namespace('_BrokerageAdministratorsAddModalController');
  var _cvcAnimateEnabled = false;

  $scope.submit = function() {
    log('Saving changes yo!');
    _cvcAnimateEnabled = false;
    $scope.addBrokerageAdminsForm.$cvcForm.$submit().then(function() {
      log('Successfully added administrators');
      $modalInstance.close($scope.addBrokerageAdmins.admins.length);
    }).catch(function() {
      $modalInstance.dismiss('something broke');
    });
  };

  $scope.dismissChanges = function() {
    _cvcAnimateEnabled = false;
    $scope.addBrokerageAdminsForm.$cvcForm.$dismissChanges().then(function() {
      $modalInstance.dismiss('Canceled');
    });
  };

  $scope.incrementAdminFormCount = function() {
    log('Adding another admin!!');
    $scope.addBrokerageAdmins.admins.push({});
    $scope.addBrokerageAdminsForm.$cvcForm.$saveCurrentState();
  };

  $scope.removeAdminForm = function(index) {
    $scope.addBrokerageAdmins.admins.splice(index, 1);
    $scope.addBrokerageAdminsForm.$cvcForm.$saveCurrentState();
  };

  $scope.log = function() {
    log($scope.addBrokerageAdminsForm);
  };

  $scope.adminForm = function(index) {
    return $scope.addBrokerageAdminsForm['admins[' + index + ']'];
  };

  $scope.isFormEnabled = function() {
    var isSubmitting = $scope.addBrokerageAdminsForm.$cvcForm.$submitting;
    return !isSubmitting;
  };

  $scope.isSubmitEnabled = function() {
    return $scope.isFormEnabled() && $scope.addBrokerageAdminsForm.$cvcForm.$isReadyForSubmit();
  };

  $scope.isCvcAnimateEnabled = function() {
    return _cvcAnimateEnabled;
  };

  $scope.multipleAdminFormsPresent = function() {
    return $scope.addBrokerageAdmins && $scope.addBrokerageAdmins.admins && $scope.addBrokerageAdmins.admins.length > 1;
  };

  $scope.$watch('addBrokerageAdminsForm', function(form) {
    if (form) {
      form.$cvcForm.$initialLoad.then($timeout(function() {
        _cvcAnimateEnabled = true;
      }, 100));
    }
  });
}]);

'use strict';

/**
 * Directive and controllers for adding and removing brokerage clients
 */
angular.module('myApp.directives')
.directive('brokerageClients',
function() {
  return {
    restrict: 'A',
    scope: {
      brokerage: '='
    },
    controller: '_BrokerageClientsController',
    templateUrl: 'components/brokerage-modules/brokerage-clients.html'
  };
})
.controller('_BrokerageClientsController',
["$scope", "logging", "Files", "Brokerages", "$modal", "PersonCardModels", "$q", "Tours", function($scope, logging, Files, Brokerages, $modal, PersonCardModels, $q, Tours) {
  var log = logging.namespace('_BrokerageClientsController');
  var _notifyRemoveClientFailed = false;
  var initialDataHasSynced = false;
  var displayNumClientsAdded = false;
  var numClientsAdded = 0;
  var cardModelsByClientId = {};

  var getClientsAndBrokers = function() {
    var promises = {
      brokers: $scope.brokerage.brokers(),
      clients: $scope.brokerage.clients()
    };

    return $q.all(promises).then(function(results) {
      $scope.brokersByBrokerId = {};
      _.forEach(results.brokers, function(brokerUser) {
        $scope.brokersByBrokerId[brokerUser.brokerId()] = brokerUser;
      });
      $scope.brokers = results.brokers;
      $scope.clients = results.clients;
      initialDataHasSynced = true;
      return results;
    });
  };

  var hasRegisteredBrokers = function() {
    return $scope.brokers && $scope.brokers.length > 0;
  };

  $scope.showAlertCanNotAddClients = function() {
    return initialDataHasSynced && !hasRegisteredBrokers();
  };

  $scope.disableInviteClients = function() {
    return !hasRegisteredBrokers();
  };

  var personCardModel = function(client) {
    var cardModel = PersonCardModels.createFromUser(client);
    var broker = _.chain(client.employedBrokerIds())
      .map(function(brokerId) {
        return $scope.brokersByBrokerId[brokerId];
      })
      .filter(function(brokerOrUndefined) {
        return brokerOrUndefined !== undefined;
      })
      .first()
      .value();

    if (broker !== undefined) {
      cardModel.setBroker(broker.fullName());
    }

    cardModel.setBehavior({
      onRemove: function() {
        var oldClients = $scope.clients;
        $scope.clients = _.filter(oldClients, function(oldClient) {
          return oldClient.id() !== client.id();
        });

        $scope.brokerage.removeClient(client.id())
          .then(function removeClientSuccess(newBrokerage) {
            $scope.brokerage = newBrokerage;
            getClientsAndBrokers();
          }, function removeClientFail() {
            $scope.clients = oldClients;
            _notifyRemoveClientFailed = true;
          });
      }
    });

    return cardModel;
  };

  $scope.numClients = function() {
    return $scope.clients.length;
  };

  $scope.clientsListIsVisible = function() {
    return $scope.clients.length > 0;
  };

  $scope.closeClientsAddedNotification = function() {
    displayNumClientsAdded = false;
  };

  $scope.displayNumClientsAdded = function() {
    return displayNumClientsAdded;
  };

  $scope.openAddClients = function() {
    var opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      scope: $scope,
      controller: 'BrokerageClientsAddModalController',
      templateUrl : 'components/brokerage-modules/add-clients-dialog.html',
      resolve: {} // empty storage
    };

    var addClientsModal = $modal.open(opts);

    addClientsModal.result.then(function(numClientsAddedFromModal) {
      numClientsAdded = numClientsAddedFromModal;
      displayNumClientsAdded = true;
      getClientsAndBrokers();
    });
  };

  $scope.numClientsAdded = function() {
    return numClientsAdded;
  };

  $scope.showRemoveClientFailedNotification = function() {
    return _notifyRemoveClientFailed;
  };

  $scope.acknowledgeRemoveClientFailed = function() {
    _notifyRemoveClientFailed = false;
  };

  $scope.cardModelsByClientId = function(id) {
    return cardModelsByClientId[id];
  };

  $scope.manuallyTriggerTour = function() {
    var tour = Tours.create({
      steps: Tours.steps({ match: /^firm.ClientList/ })
    });
    tour.start();
  };

  $scope.$watch('brokerage', function(newBrokerage) {
    if (newBrokerage) {
      getClientsAndBrokers();
    }
  });

  $scope.$watch('clients', function(newClients) {
    cardModelsByClientId = {};

    _.forEach(newClients, function(client) {
      cardModelsByClientId[client.id()] = personCardModel(client);
    });
  });

  $scope.clients = [];
  $scope.brokers = [];
}]);

'use strict';

angular.module('myApp.controllers')
.controller('BrokerageClientsAddModalController',
["$scope", "$modalInstance", "logging", "$timeout", function($scope, $modalInstance, logging, $timeout) {
  var log = logging.namespace('BrokerageClientsAddModalController');
  var _cvcAnimateEnabled = false;

  $scope.submit = function() {
    log('Saving changes yo!');
    _cvcAnimateEnabled = false;
    $scope.addBrokerageClientsForm.$cvcForm.$submit().then(function() {
      log('Successfully added clients');
      $modalInstance.close($scope.addBrokerageClients.clients.length);
    }).catch(function() {
      $modalInstance.dismiss('something broke');
    });
  };

  $scope.dismissChanges = function() {
    _cvcAnimateEnabled = false;
    $scope.addBrokerageClientsForm.$cvcForm.$dismissChanges().then(function() {
      $modalInstance.dismiss('Canceled');
    });
  };

  $scope.isDefaultBroker = function(broker) {
    return broker === $scope.brokers[0];
  };

  $scope.incrementClientFormCount = function() {
    log('Adding another client!!');
    var numClients = $scope.addBrokerageClients.clients.length;
    var newClient = {};
    var lastClientBroker;

    if (numClients > 0) {
      lastClientBroker = $scope.addBrokerageClients.clients[numClients - 1].broker;
      if (lastClientBroker) {
        newClient.broker = lastClientBroker;
      }
    }

    $scope.addBrokerageClients.clients.push(newClient);
    $scope.addBrokerageClientsForm.$cvcForm.$saveCurrentState();
  };

  $scope.removeClientForm = function(index) {
    $scope.addBrokerageClients.clients.splice(index, 1);
    $scope.addBrokerageClientsForm.$cvcForm.$saveCurrentState();
  };

  $scope.log = function() {
    log($scope.addBrokerageClientsForm);
  };

  $scope.clientForm = function(index) {
    return $scope.addBrokerageClientsForm['clients[' + index + ']'];
  };

  $scope.isFormEnabled = function() {
    var isSubmitting = $scope.addBrokerageClientsForm.$cvcForm.$submitting;
    return !isSubmitting;
  };

  $scope.isSubmitEnabled = function() {
    return $scope.isFormEnabled() && $scope.addBrokerageClientsForm.$cvcForm.$isReadyForSubmit();
  };

  $scope.isCvcAnimateEnabled = function() {
    return _cvcAnimateEnabled;
  };

  $scope.multipleClientFormsPresent = function() {
    return $scope.addBrokerageClients.clients.length > 1;
  };

  $scope.brokers = [];

  $scope.addBrokerageClients = { clients: [] };
  $scope.brokerage.brokers().then(function(brokers) {
    log('GOT SOME BROKERS');
    log(brokers.length);
    $scope.brokers = _.sortBy(brokers, function(broker) { return broker.fullName(); });
  });

  $scope.$watch('addBrokerageClientsForm', function(form) {
    if (form) {
      form.$cvcForm.$initialLoad.then($timeout(function() {
        _cvcAnimateEnabled = true;
      }, 100));
    }
  });
}]);

'use strict';

/**
 * Directive and controllers for administrating a list of brokers on the
 * brokerage administration panel.
 */
angular.module('myApp.directives')
.directive('brokersList',
function() {
  return {
    restrict: 'A',
    scope: {
      brokerage: '='
    },
    controller: '_BrokersListController',
    templateUrl: 'components/brokerage-modules/brokers-list.html'
  };
})
.controller('_BrokersListController',
["$scope", "logging", "Brokerages", "$modal", "Tours", function($scope, logging, Brokerages, $modal, Tours) {
  var log = logging.namespace('_BrokersListController');
  var numBrokersAdded = 0;
  var showNumBrokersAdded = false;
  var showRemoveBrokerFailed = false;

  var getBrokers = function() {
    return $scope.brokerage.brokers().then(function(brokers) {
      $scope.brokers = brokers;
      return brokers;
    });
  };

  $scope.openAddBrokers = function() {
    var opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      scope: $scope,
      controller: 'AddBrokersDialogController',
      templateUrl : 'components/brokerage-modules/add-brokers-dialog.html',
      resolve: {} // empty storage
    };

    var addBrokersModal = $modal.open(opts);
    addBrokersModal.result.then(function(numBrokersAddedFromModal) {
      numBrokersAdded = numBrokersAddedFromModal;
      showNumBrokersAdded = true;
      getBrokers();
    });
  };

  $scope.removeBroker = function(brokerToRemove) {
    var oldBrokers = $scope.brokers;

    $scope.brokers = _.filter(oldBrokers, function(broker) { return broker.brokerId() !== brokerToRemove.brokerId(); });
    $scope.brokerage.removeBroker(brokerToRemove.brokerId())
      .then(function removeBrokerSucceed(newBrokerage) {
        $scope.brokerage = newBrokerage;
      }, function removeBrokerFail() {
        $scope.brokers = oldBrokers;
        showRemoveBrokerFailed = true;
      });
  };

  $scope.allowRemoveBroker = function(broker) {
    var isAdmin = $scope.brokerage && $scope.brokerage.isAdmin(broker.id());
    return !isAdmin;
  };

  $scope.brokers = [];

  $scope.$watch('brokerage', function(newBrokerage) {
    if (newBrokerage) {
      getBrokers();
    }
  });

  $scope.showNumBrokersAdded = function() {
    return showNumBrokersAdded;
  };

  $scope.acknowledgeBrokersAdded = function() {
    showNumBrokersAdded = false;
  };

  $scope.showRemoveBrokerFailedNotification = function() {
    return showRemoveBrokerFailed;
  };

  $scope.numBrokersAdded = function() {
    return numBrokersAdded;
  };

  $scope.acknowledgeRemoveBrokerFailed = function() {
    showRemoveBrokerFailed = false;
  };

  $scope.startBrokersListTour = function() {
    var tour = Tours.create({
      steps: Tours.steps({ match: /^firm.BrokersList/ })
    });

    tour.start();
  };
}]);

'use strict';

angular.module('myApp.controllers')
.controller('AddBrokersDialogController',
["$scope", "$modalInstance", "logging", "$timeout", function($scope, $modalInstance, logging, $timeout) {
  var log = logging.namespace('_AddBrokersDialogController');
  var _cvcAnimateEnabled = false;

  $scope.submit = function() {
    log('Saving changes yo!');
    _cvcAnimateEnabled = false;
    $scope.addBrokersForm.$cvcForm.$submit().then(function() {
      log('Successfully added brokers');
      $modalInstance.close($scope.addBrokers.brokers.length);
    }).catch(function() {
      $modalInstance.dismiss('something broke');
    });
  };

  $scope.dismissChanges = function() {
    _cvcAnimateEnabled = false;
    $scope.addBrokersForm.$cvcForm.$dismissChanges().then(function() {
      $modalInstance.dismiss('Canceled');
    });
  };

  $scope.incrementBrokerFormCount = function() {
    log('Adding another broker!!');
    $scope.addBrokers.brokers.push({});
    $scope.addBrokersForm.$cvcForm.$saveCurrentState();
  };

  $scope.removeBrokerForm = function(index) {
    $scope.addBrokers.brokers.splice(index, 1);
    $scope.addBrokersForm.$cvcForm.$saveCurrentState();
  };

  $scope.brokerForm = function(index) {
    return $scope.addBrokersForm['brokers[' + index + ']'];
  };

  $scope.isFormEnabled = function() {
    var isSubmitting = $scope.addBrokersForm.$cvcForm.$submitting;
    return !isSubmitting;
  };

  $scope.isSubmitEnabled = function() {
    return $scope.isFormEnabled() && $scope.addBrokersForm.$cvcForm.$isReadyForSubmit();
  };

  $scope.isCvcAnimateEnabled = function() {
    return _cvcAnimateEnabled;
  };

  $scope.multipleBrokerFormsPresent = function() {
    return $scope.addBrokers && $scope.addBrokers.brokers && $scope.addBrokers.brokers.length > 1;
  };

  $scope.$watch('addBrokersForm', function(form) {
    if (form) {
      form.$cvcForm.$initialLoad.then($timeout(function() {
        _cvcAnimateEnabled = true;
      }, 100));
    }
  });
}]);


'use strict';

/**
 * Directive and controllers for an offerings list carousel
 *
 */
angular.module('myApp.directives')
.directive('offeringsListCarousel',
function() {
  return {
    restrict: 'E',
    scope: {
      selectedOfferingId: '=',
      brokerage: '=',
      offeringsDb: '=',
      offerings: '='
    },
    controller: '_OfferingsListController',
    templateUrl: 'components/brokerage-modules/offerings-list-carousel.html'
  };
})
.controller('_OfferingsListController',
["$scope", "logging", "$timeout", function ($scope, logging, $timeout) {
  var log = logging.namespace('_OfferingsListController');
  var inited = false;

  //this massages the offerings into carousels of 4 offerings per slide
  function _prepareCollection () {
    $scope.offeringsCollection = _.chain($scope.offerings).groupBy(function (num, i) {
      var index = i+1;
      return Math.ceil(index/4);
    }).toArray().value();
  }


  //if there is an offering selected, set the slide it sits in to 'active'
  function _setDefaultActiveSlide (selectedOfferingId) {
    var slideIndex;

    if (selectedOfferingId) {
      slideIndex =  _.findIndex($scope.offeringsCollection, function (offerings) {
        return _.find(offerings, function (offering) {
            if (offering.id() === selectedOfferingId) {
              return true;
            }
          });
      });
      
      if (slideIndex && slideIndex >= 0) { //if slideIndex >= 0, then there is such offering
        $scope.offeringsCollection[slideIndex].active = true;
      }
    }
  };
 
  $scope.$watch('offerings', function(newVal){
    if (newVal && !inited ) {
      inited = true;
      _prepareCollection();
      _setDefaultActiveSlide($scope.selectedOfferingId);
    }
  });

}]).controller('_OfferingsListRepeatController', 
["$scope", "logging", "$timeout", function ($scope, logging, $timeout) {
  var currentOffering = $scope.offerings[$scope.$index];
  $scope.currentCompany = currentOffering.local($scope.offeringsDb).security().local($scope.offeringsDb).company();
  $scope.currentOfferingId = currentOffering.id();
}]);

'use strict';

/**
 * Directive and controllers for display the details of a selected offering or an overview
 * of ALL offerings a brokerage admin can see
 *
 */
angular.module('myApp.directives')
.directive('offeringDetails',
function() {
  return {
    restrict: 'E',
    scope: {
      selectedOfferingId: '=',
      series: '=?',
      brokerage: '=',
      offeringsDb: '=',
      offerings: '='
    },
    controller: '_OfferingDetailsController',
    templateUrl: 'components/brokerage-modules/offering-details.html'
  };
})
.controller('_OfferingDetailsController',
["$scope", "logging", "$modal", function($scope, logging,  $modal) {
  var log = logging.namespace('_OfferingDetailsController');

  $scope.currentOffering = null;

  $scope.$watch('offerings', function(newVal, oldVal){
    if (newVal && $scope.selectedOfferingId) {
      _setCurrentOffering();
    }
  });

  function _setCurrentOffering () {
    if (!$scope.currentOffering) {
      $scope.currentOffering = _.find($scope.offerings, function(currOffering) {
        return (currOffering.id() === $scope.selectedOfferingId);
      });
    }
  }

}]);

'use strict';

/**
 * Directive and controllers for display the details of a selected offering or an overview
 * of ALL offerings a brokerage admin can see
 *
 */
angular.module('myApp.directives')
.directive('offeringSeries',
function() {
  return {
    restrict: 'E',
    scope: {
      selectedOfferingId: '=',
      brokerage: '=',
      offeringsDb: '=',
      offerings: '='
    },
    controller: '_OfferingSeriesController',
    templateUrl: 'components/brokerage-modules/offering-series.html'
  };
})
.controller('_OfferingSeriesController',

["$scope", "logging", "$modal", "Deals", "URLS", "$http", "api2", "APIObjects", "ObjectDBs", function($scope, logging,  $modal, Deals, URLS, $http, api2, APIObjects, ObjectDBs) {
  var log = logging.namespace('_OfferingSeriesController');
  $scope.SeriesStatus = Deals.SeriesStatus;
  var SeriesFormTypes = {
    'add_new_offering_series': 'add_new_offering_series',
    'edit_series': 'edit_series',
    'create_investmentpacketdoc': 'create_investmentpacketdoc',
    'edit_investmentpacketdoc': 'edit_investmentpacketdoc'
  };

  $scope.$watch('offerings', function(newVal){
    if (newVal && $scope.selectedOfferingId) {
      _setCurrentOffering();
      _init();
    }
  });

  $scope.addNewSeries = function () {
    var config = {
      formContext:  $scope.currentOffering.accessIds()[0],
      formType: SeriesFormTypes.add_new_offering_series,
      title: 'Create New Series'
    };

    var dialogViewModel = new DialogViewModel(config);
    _openSeriesDialog(dialogViewModel);
  };


  var SeriesViewModel = function (series) {
    this._series = series;
    this._init();
  };

  SeriesViewModel.prototype = {
    _init: function () {
      this.allDocModels = this._setDocModels();
    },

    id: function () {
      return this._series.id();
    },

    name: function () {
      return this._series.name();
    },

    status: function () {
      return this._series.status();
    },

    isClosed: function () {
      return this._series.isClosed();
    },

    isNotYetOpened: function () {
      return this._series.isNotYetOpened();
    },

    investorViewLink: function () {
      var db = $scope.offeringsDb;
      var companySlug = this._series.local(db).company().slug();
      return URLS.FUNDRAISE(companySlug, this._series.id());
    },

    feePercentage: function() {
      return this._series.feePercentage();
    },

    carryPercentage: function() {
      return this._series.carryPercentage();
    },

    commissionPercentage: function() {
      return this._series.commissionPercentage();
    },

    placementFeePercentage: function() {
      return this._series.placementFeePercentage();
    },

    recommendedMinInvestment: function() {
      return this._series.recommendedMinInvestment();
    },

    investmentPacketDocs: function() {
      return this._series.investmentPacketDocs();
    },

    allInvestmentPacketDocs: function() {
      var docs = this._series.local($scope.offeringsDb).allInvestmentPacketDocs();
      docs = _.sortBy(docs, function(doc){
        return doc.local($scope.offeringsDb).doc().name().toLowerCase();
      });
      
      return docs;
    },

    allDocIds: function() {
      return this._series.local($scope.offeringsDb).allDocIds();
    },

    _setDocModels: function() {
      var docs = [];
      var packetDocs = this.allInvestmentPacketDocs();
      if (packetDocs.length > 0) {
        docs = _.map(this.allInvestmentPacketDocs(), function (packetDoc){
          return new SeriesDocViewModel(packetDoc);
        });
      }
      
      return docs;
    },

    edit: function () {
      var config = {
        formContext:  this.id(),
        formType: SeriesFormTypes.edit_series,
        title: 'Edit Series - ' + this.name()
      };

      var dialogViewModel = new DialogViewModel(config);
      _openSeriesDialog(dialogViewModel);
    },

    changeStatus: function (newStatus) {
      var seriesRequest = $http.patch(api2.series() + '/' + this.id()+'?status='+newStatus);

      return seriesRequest.then(
        function seriesGetSuccess(httpResponse) {
          Deals.offeringsFromBrokerage($scope.brokerage.id(), {db: $scope.offeringsDb}).then(function(offerings){
            $scope.offerings = offerings;
          });
      });
    },

    addNewDoc: function () {
      var config = {
        formContext: this.id(),
        formType: SeriesFormTypes.create_investmentpacketdoc
      };

      var dialogViewModel = new DialogViewModel(config);
      _openDocDialog(dialogViewModel);
    }
  }


  var SeriesDocViewModel = function (packetDoc) {
    this._packetDoc = packetDoc;
    this.confirmDeleteConfig = {};
    this._init();
  };

  SeriesDocViewModel.prototype = {
    _init: function () {
      var self = this;
      this.confirmDeleteConfig = {
        'title': 'Remove Document - ' + self.name(),
        'message': 'Are you sure you want to delete this document?',
        'buttonContent': 'X',
        'action': function () {
          return self._remove();
        }
      };
    },

    packetDocId: function () {
      return this._packetDoc.id();
    },

    series: function () {
      return this._packetDoc.series();
    },

    externalRef: function () {
      return this._packetDoc.local($scope.offeringsDb).doc().externalRef();
    },

    name: function () {
      return this._packetDoc.local($scope.offeringsDb).doc().name();
    },

    investorMustSign: function() {
      return this._packetDoc.investorMustSign();
    },

    firmMustSign: function() {
      return this._packetDoc.firmMustSign();
    },

    nonUSIndividualMustSign: function() {
      return this._packetDoc.nonUSIndividualMustSign();
    },

    nonUSOrganizationMustSign: function() {
      return this._packetDoc.nonUSOrganizationMustSign();
    },

    sequenceNumber: function() {
      return this._packetDoc.sequenceNumber();
    },

    USIndividualMustSign: function() {
      return this._packetDoc.USIndividualMustSign();
    },

    USOrganizationMustSign: function() {
      return this._packetDoc.USOrganizationMustSign();
    },

    docId: function() {
      return this._packetDoc.doc();
    },

    doc: function () {
      return this._packetDoc.local($scope.offeringsDb).doc();
    },

    type: function () {
      return this._packetDoc.local($scope.offeringsDb).doc().type();
    },

    _remove: function () {
      var removeRequest = $http.delete(api2.investmentPacketDocs() + '/' + this.packetDocId());

      return removeRequest.then(
        function removeSuccess(httpResponse) {
          Deals.offeringsFromBrokerage($scope.brokerage.id(), {db: $scope.offeringsDb}).then(function(offerings){
            var offering;

            if ($scope.selectedOfferingId) {
              offering = _.find(offerings, function(offering){
                return offering.id() === $scope.selectedOfferingId;
              });

              offering.local($scope.offeringsDb).allDocs().then(function(){
                $scope.offerings = offerings;
              });
              
            } else {
              $scope.offerings = offerings;
            }

          });
      });
    },

    edit: function () {
      var config = {
        formContext: this.packetDocId(),
        formType: SeriesFormTypes.edit_investmentpacketdoc
      };

      var dialogViewModel = new DialogViewModel(config);
      _openDocDialog(dialogViewModel);
    }
  }


  var DialogViewModel = function (config) {
    this._config = config;
  };

  DialogViewModel.prototype = {
    formContext: function () {
      return this._config.formContext;
    },

    formType: function () {
      return this._config.formType;
    },

    title: function () {
      return this._config.title;
    }
  }

  function _openConfirmDialog (viewModel) {
    var opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      scope: $scope,
      controller: 'ConfirmModalController',
      templateUrl : 'components/brokerage-modules/confirm-modal.html',
      resolve: {
        'dialogViewModel': function () {
          return viewModel;
        }
      }
    };

    var confirmModal = $modal.open(opts);
    confirmModal.result.then(function() {});
  }  

  function _openDocDialog (viewModel) {
    var opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      scope: $scope,
      controller: 'OfferingSeriesDocModalController',
      templateUrl : 'components/brokerage-modules/offering-series-doc-dialog.html',
      resolve: {
        'dialogViewModel': function () {
          return viewModel;
        },

        'SeriesFormTypes': function () {
          return SeriesFormTypes
        }
      }
    };

    var docModal = $modal.open(opts);

    docModal.result.then(function() {
      Deals.offeringsFromBrokerage($scope.brokerage.id(), {db: $scope.offeringsDb}).then(function(offerings){
        var offering;

        if ($scope.selectedOfferingId) {
          offering = _.find(offerings, function(offering){
            return offering.id() === $scope.selectedOfferingId;
          });

          offering.local($scope.offeringsDb).allDocs().then(function(){
            $scope.offerings = offerings;
          });
          
        } else {
          $scope.offerings = offerings;
        }

      });      
    });
  }

  function _openSeriesDialog (viewModel) {
    var opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      scope: $scope,
      controller: 'OfferingSeriesModalController',
      templateUrl : 'components/brokerage-modules/offering-series-dialog.html',
      resolve: {
        'dialogViewModel': function () {
          return viewModel;
        },

        'SeriesFormTypess' : function () {
          return SeriesFormTypes
        }
      }
    };

    var seriesModal = $modal.open(opts);

    seriesModal.result.then(function() {
      Deals.offeringsFromBrokerage($scope.brokerage.id(), {db: $scope.offeringsDb}).then(function(offerings){
        $scope.offerings = offerings;
      });
    });
  }

  function _setCurrentOffering () {
    if (!$scope.currentOffering) {
      $scope.currentOffering = _.find($scope.offerings, function(currOffering) {
        return (currOffering.id() === $scope.selectedOfferingId);
      });
    }
  }

  function _init () {
    var allSeries = $scope.currentOffering.local($scope.offeringsDb).allSeriesInReverseIdOrder();

    $scope.seriesViewModels = _.map(allSeries, function(series) {
      return new SeriesViewModel(series);
    });
  }
}]);

'use strict';

/**
 * Controller for the brokerage offerings dashboard page (a page for admins and brokerage-admins only)
 */
angular.module('myApp.controllers').controller(
'BrokerageOfferingsDashboardPageController',
["$scope", "$routeParams", "logging", "Brokerages", "Deals", "ObjectDBs", function($scope, $routeParams, logging, Brokerages, Deals, ObjectDBs) {
  var log = logging.namespace('BrokerageOfferingsDashboardPageController');
  var db = ObjectDBs.temporary();
  $scope.theme = 'v2';
  $scope.loading = true;
  $scope.selectedOfferingId = $routeParams.offeringId || null;
  $scope.offeringsDb = db;

  Brokerages.getByUniqueKey($routeParams.firmSlug, {db: db}).then(function(brokerage) {
    $scope.brokerage = brokerage;

    Deals.offeringsFromBrokerage(brokerage.id(), {db: db}).then(function(offerings){
      var offering;

      if ($scope.selectedOfferingId) {
        offering = _.find(offerings, function(offering){
          return offering.id() === $scope.selectedOfferingId;
        });

        offering.local($scope.offeringsDb).allDocs().then(function(){
          $scope.offerings = offerings;
        });
        
      } else {
        $scope.offerings = offerings;
      }

      /*
        MAKE SURE SELECTED OFFERING IS IN OFFERINGS, else go to error page
      */
    });
  });
}]);

'use strict';

angular.module('myApp.controllers')
.controller('OfferingSeriesModalController',
["$scope", "$modalInstance", "Deals", "logging", "$timeout", "dialogViewModel", function($scope, $modalInstance, Deals, logging, $timeout, dialogViewModel) {
  var log = logging.namespace('OfferingSeriesModalController');
  $scope.formData = {};
  $scope.dialogViewModel = dialogViewModel;

  $scope.submit = function() {
    log('Saving changes yo!');
    
    $scope.offeringSeriesForm.$cvcForm.$submit().then(function() {
      log('Successfully added series');
      $modalInstance.close();
    }).catch(function() {
      $modalInstance.dismiss('uh oh, the data didn\'t submit Successfully');
    });
    
  };

  $scope.dismissChanges = function() {

    $scope.offeringSeriesForm.$cvcForm.$dismissChanges().then(function() {
      $modalInstance.dismiss('Canceled');
    });
  };

  $scope.isFormEnabled = function() {
    var isSubmitting = $scope.offeringSeriesForm.$cvcForm.$submitting;
    return !isSubmitting;
  };

  $scope.isSubmitEnabled = function() {
    return $scope.isFormEnabled() && $scope.offeringSeriesForm.$cvcForm.$isReadyForSubmit();
  };


}]);

'use strict';

angular.module('myApp.controllers')
.controller('OfferingSeriesDocModalController',
["$scope", "$modalInstance", "Deals", "logging", "$timeout", "dialogViewModel", "Docs", "SeriesFormTypes", function($scope, $modalInstance, Deals, logging, $timeout, dialogViewModel, Docs, SeriesFormTypes) {
  var log = logging.namespace('OfferingSeriesDocModalController');
  $scope.dialogViewModel = dialogViewModel;
  $scope.formData = {};

  function _mapData () {
    $scope.dialogViewModel = angular.extend(
      $scope.dialogViewModel,
      {
        DocsStatus: Docs.Status,
        DocsType: Docs.Type
      }
    );
  }

  $scope.submit = function() {
    log('Saving changes yo!');
    
    $scope.offeringSeriesDocForm.$cvcForm.$submit().then(function(data) {
      log('Successfully added series');
      $modalInstance.close(data);
    }).catch(function() {
      $modalInstance.dismiss('uh oh, the data didn\'t submit Successfully');
    });
    
  };

  $scope.isEditingDocument = function () {
    return $scope.dialogViewModel.formType() === SeriesFormTypes.edit_investmentpacketdoc;
  };

  $scope.isAddingDocument = function () {
    return $scope.dialogViewModel.formType() === SeriesFormTypes.create_investmentpacketdoc;
  };

  $scope.dismissChanges = function() {
    $scope.offeringSeriesDocForm.$cvcForm.$dismissChanges().then(function() {
      $modalInstance.dismiss('Canceled');
    });
  };

  $scope.isFormEnabled = function() {
    var isSubmitting = $scope.offeringSeriesDocForm.$cvcForm.$submitting;
    return !isSubmitting;
  };

  $scope.isSubmitEnabled = function() {
    return $scope.isFormEnabled() && $scope.offeringSeriesDocForm.$cvcForm.$isReadyForSubmit();
  };

  $scope.fileExists = function () {
    return ($scope.formData && $scope.formData.file);
  }

  _mapData();

}]);

'use strict';

/**
  Confirmation Modal Directive
  
  Attributes:
    config (object)
      {
        title: 'A title for the dialog',
        message: 'A Message for the dialog',
        action: function () {
          return preferrablyThisWillReturnAPromise();
        },
        buttonContent: 'What Gets Displayed In the Button'
      }


 */

angular.module('myApp.directives')
.directive('confirmModal',
["logging", "$q", "$modal", "URLS", "$http", "api2", "APIObjects", "ObjectDBs", function(logging, $q, $modal, URLS, $http, api2, APIObjects, ObjectDBs) {
  var log = logging.namespace('confirmModal');
  return {
    restrict: 'E',
    
    scope: {
      config: '=?'
    },
    
    templateUrl: 'components/confirm-modal/confirm-modal.html',

    link: function(scope, element, attrs) {
                
      var DialogViewModel = function (config) {
        this._config = config;
      };

      DialogViewModel.prototype = {
        title: function () {
          return this._config.title;
        },

        message: function () {
          return this._config.message;
        },

        action: function () {
          return this._config.action();
        }
      };

      _.defaults(scope.config, {
          title: 'Dialog Title',
          message: 'Dialog Message',
          buttonContent: 'X',
          action: function () { return true;}
      });

      scope.openConfirmDialog = function () {
        var dialogViewModel = new DialogViewModel(scope.config);
        
        var opts = {
          backdrop: true,
          backdropClick: true,
          dialogFade: false,
          keyboard: true,
          scope: scope,
          controller: '_ConfirmModalDialogController',
          templateUrl : 'components/confirm-modal/confirm-modal-content.html',
          resolve: {
            'dialogViewModel': function () {
              return dialogViewModel;
            }
          }
        };

        var confirmModal = $modal.open(opts);
        confirmModal.result.then(function() {});

      }

    }
  }
}])
.controller('_ConfirmModalDialogController',
["$scope", "logging", "$modalInstance", "dialogViewModel", function($scope, logging, $modalInstance, dialogViewModel) {
  
  var log = logging.namespace('_ConfirmModalDialogController');

  $scope.dialogViewModel = dialogViewModel;
  
  $scope.dismissChanges = function () {
    $modalInstance.dismiss('Canceled');
  };

  $scope.performAction = function () {
    $scope.dialogViewModel.action().then(function(data) {
      $modalInstance.close(data);
    });
  }

}]);

'use strict';

/**
 * PersonCardModel. The data and behavior that drive the person-card
 * directive and its associated view.
 *
 * Members:
 *   .hasDetails() -- return true that there are details to render
 *
 *   .setBroker(brokerName) -- set the name of the person's broker.
 *
 *   .behavior() / .setBehavior(behavior) -- return the behavior for the person card.
 *      behavior is an object can have the following properties:
 *        .onRemove() -- optional. a function to execute when the "X" has
 *          been clicked on the person card. If nothing is provided, the "X"
 *          does not render.

 *   .data() -- return the data for the person card.
 *      It contains the following fields:
 *        .phone -- the person's phone number
 *        .email -- the person's email address
 *        .nameTokens -- an array of the tokens in a person's name, space delimited.
 */

/**
 * PersonCardModels. Factory for accessing PersonCardModel instances.
 *
 * Members:
 *   .create(config) -- return a PersonCardModel instance. Config
 *      is an object containing the following:
 *        .data -- required. Contains the basic person data for rendering the card.
 *          See PersonCardModel.data().
 *        .behavior -- optional. See PersonCardModel.behavior().
 *
 *    .createFromUser(User) -- return a PersonCardModel instance from a User object.
 */
angular.module('myApp.services').service(
'PersonCardModels',
["logging", function(logging) {
  var log = logging.namespace('PersonCardModels');

  var PersonCardModel = function(data, behavior) {
    this._data = data;
    this._behavior = behavior || {};
  };

  PersonCardModel.prototype = {
    behavior: function() { return this._behavior; },
    data: function() { return this._data; },
    setBroker: function(brokerName) {
      this._data.brokerName = brokerName;
    },
    setBehavior: function(behavior) {
      this._behavior = behavior;
    },

    hasDetails: function() {
      return this._data.email || this._data.phone;
    }
  };

  var PersonCardModels = {
    create: function(config) {
      return new PersonCardModel(
        config.data || {},
        config.behavior || {}
      );
    },

    createFromUser: function(user) {
      return PersonCardModels.create({
        data: {
          nameTokens: user.fullNameTokens(),
          email: user.email(),
          phone: user.phone()
        }
      });
    }
  };

  return PersonCardModels;
}]);

'use strict';

// Functionality for person-cards; the unified way to view
// summary information about a person in our CRM
angular.module('myApp.directives')
.directive(
'personCard',
["logging", function(logging) {
  var log = logging.namespace('personCard');
  return {
    restrict: 'A',
    scope: {
      person: '=personCardModel'
    },
    templateUrl: 'components/person-card/person-card.html'
  };
}]);

'use strict';

angular.module('myApp.directives')
.directive(
'seriesCard',
["logging", function(logging) {
  var log = logging.namespace('seriesCard');
  return {
    restrict: 'A',
    scope: {
      cardModel: '=seriesCardModel'
    },
    templateUrl: 'components/series-card/series-card.html',
    controller: '_SeriesCardController'
  };
}])
.controller(
'_SeriesCardController',
["$scope", "logging", "Utils", function($scope, logging, Utils) {
  var log = logging.namespace('_SeriesCardController');
  $scope.$watch('cardModel', function(cardModel) {
    // Make some asserts here
    if (cardModel) {
      Utils.assert(cardModel.pricePerShare, 'Series card model must have a "pricePerShare" method that returns a MoneyAmount');
      Utils.assert(cardModel.valuation, 'Series card model must have a "valuation" method that returns a MoneyAmount');
      Utils.assert(cardModel.seriesName, 'Series card model must have a "seriesName" method that returns a string');
      Utils.assert(cardModel.description, 'Series card model must have a "description" method that returns a string');
      Utils.assert(cardModel.offeringFirm, 'Series card model must have an "offeringFirm" method that returns a string');
      Utils.assert(cardModel.companyName, 'Series card model must have a "companyName" method that returns a string');
    }
  });
}]);

'use strict';

/**
 * UiForm. A convenient class for performing recursive operations
 * against FormControllers. (see https://docs.angularjs.org/api/ng/type/form.FormController)
 *
 * Members:
 *   .setSaving(saving) -- sets the $saving flag onto the contained FormController's $cvcForm.
 *     does nothing if there was no $cvcForm.
 *     Args:
 *       saving -- boolean. true that the $saving flag should be set to true.
 *
 *   .setSavingRecursive(saving) -- sets the $saving flag onto the contained FormController.
 *     Also calls calls $$setSaving on all contained controls if its present. If $$setSaving
 *     is not present on the controls then it sets a $saving flag on the control.
 *     Args:
 *       saving -- boolean. true that $$setSaving should be called with true.
 *
 *   .setSubmitting(submitting) -- sets the $submitting flag onto the contained FormController.$cvcForm
 *     if it was present. Does nothing if there was no $cvcForm.
 *     Args:
 *       submitting -- boolean. true that the $submitting flag should be set to true
 *
 *   .touchPresentFieldsRecursive(touched) -- sets the $touched flag to true recursively on all fields that have
 *     active modelValues, recursively.
 *
 *   .resetRemoteErrors(errorsByField) -- resets the error state of the forms to match the
 *     errorsByField structure sent in. That structure must be the same structure as is
 *     communicated by the Forms API.
 */

/**
 * UiForms. A way to instantiate a UiForm instance.
 *
 * Members:
 *   .instance(formCtrl) -- create a UiForm instance from an
 *     angular FormController (see https://docs.angularjs.org/api/ng/type/form.FormController)
 */
angular.module('myApp.services')
.service(
'UiForms',
["$parse", "logging", function($parse, logging) {
  var log = logging.namespace('UiForm');

  var UiFormArray = function(uiForms) {
    this._uiForms = uiForms;
  };

  UiFormArray.prototype = {
    _applyRemoteErrors: function(errorArray) {
      var self = this;
      if (errorArray.length === undefined) {
        var msg = 'Can not set non-array errors into UiFormArray';
        log(errorArray);
        throw msg;
      }

      _.forEach(errorArray, function(formErrors, index) {
        var uiForm = self._uiForms[index];
        if (uiForm) {
          uiForm._applyRemoteErrors(formErrors);
        } else {
          log('Could not find sub-form at index ' + index +'. These are all the sub-forms we have:');
          log(self._uiForms);
        }
      });
    },

    _clearRemoteErrors: function() {
      _.forEach(this._uiForms, function(uiForm) { uiForm._clearRemoteErrors(); });
    },

    _controlsAsArrayRecursive: function() {
      var controls = [];
      _.forEach(this._uiForms, function(uiForm) {
        controls = controls.concat(uiForm._controlsAsArrayRecursive());
      });

      return controls;
    }
  };

  var UiForm = function(formCtrl) {
    this._formCtrl = formCtrl;
  };

  UiForm.prototype = {
    setSaving: function(saving) {
      if (this._formCtrl.$cvcForm) {
        this._formCtrl.$cvcForm.$saving = saving;
      }
    },

    setSavingRecursive: function(saving) {
      this.setSaving(saving);
      _.forEach(this._controlsAsArrayRecursive(), function(control) {
        if (control.$$setSaving) {
          control.$$setSaving(saving);
        } else {
          control.$saving = saving;
        }
      });
    },

    setSubmitting: function() {
      if (this._formCtrl.$cvcForm) {
        this._formCtrl.$cvcForm.$submitting = true;
      }
    },

    _clearRemoteErrors: function() {
      var clearControlErrors = function(control) {
        _.forEach(control.$error, function(isError, errorName) {
          var errorIsFromServer = errorName.indexOf('remote__') === 0;
          if (isError && errorIsFromServer) {
            control.$setValidity(errorName, true);
          }
        });
      };

      var controls = this._controlsByName();
      _.forEach(controls, clearControlErrors);
      clearControlErrors(this._formCtrl);

      _.forEach(this._subUiFormsByName(), function(subUiForm) {
        subUiForm._clearRemoteErrors();
      });
    },

    touchPresentFieldsRecursive: function(touched) {
      if (touched === undefined) {
        touched = true;
      }

      _.forEach(this._controlsAsArrayRecursive(), function(control) {
        if (control.$modelValue !== undefined) {
          control.$setTouched(touched);
        }
      });
    },

    _applyRemoteErrors: function(errorsByField) {
      if (!errorsByField) {
        return;
      }
      var self = this;
      var _controlsByName = this._controlsByName();
      var _subUiFormsByName = this._subUiFormsByName();
      var affectedNgModelControl;
      var affectedSubUiForm;
      log('Applying errors: ');
      log(errorsByField);

      _.forEach(errorsByField, function(fieldOrSubFormErrors, fieldName) {
        affectedNgModelControl = _controlsByName[fieldName];
        affectedSubUiForm = _subUiFormsByName[fieldName];
        if (fieldName === 'non_field_errors') {
          affectedNgModelControl = self._formCtrl;
        }
        if (affectedSubUiForm) {
          affectedSubUiForm._applyRemoteErrors(fieldOrSubFormErrors);
        } else if (affectedNgModelControl) {
          _.forEach(fieldOrSubFormErrors, function(error) {
            var errorName = 'remote__' + error;
            affectedNgModelControl.$setValidity(errorName, false);
          });
        } else {
          log('Could not apply remote error with name: ' + fieldName + '. No control or sub-form has that name.');
        }
      });
    },

    resetRemoteErrors: function(errorsByField) {
      this._clearRemoteErrors();
      this._applyRemoteErrors(errorsByField);
    },

    _controlsByName: function() {
      var _controlsByName = {};

      _.forEach(this._formCtrl, function(control) {
        var isNgModelController = control && control.$render !== undefined;
        if (isNgModelController) {
          $parse(control.$name).assign(_controlsByName, control);
          _controlsByName[control.$name] = control;
        }
      });

      return _controlsByName;
    },

    _controlsAsArrayRecursive: function() {
      var controls = [];
      _.forEach(this._controlsByName(), function(control) {
        controls.push(control);
      });

      _.forEach(this._subUiFormsByName(), function(uiForm) {
        controls = controls.concat(uiForm._controlsAsArrayRecursive());
      });

      return controls;
    },

    _subUiFormsByName: function() {
      var _subUiFormsByName = {};
      var self = this;
      _.forEach(this._formCtrl, function(control) {
        var controlIsFormController = control && control.$$parentForm !== undefined;
        var controlIsNotSelfParent = control !== self._formCtrl.$$parentForm;
        var controlIsSubform = controlIsFormController && controlIsNotSelfParent;
        var openBracketIndex;
        var isIndexed;
        var preIndexName;
        if (controlIsSubform) {
          // If this subform is indexed by number (e.g. clients[0]), then
          // make sure that _subUiFormsByName is ready to populate an array.
          // otherwise $parse will automatically create an object rather than
          // an array
          openBracketIndex = control.$name.indexOf('[');
          isIndexed = openBracketIndex !== -1;
          if (isIndexed) {
            preIndexName = control.$name.substring(0, openBracketIndex);
            var doInitializeFormArray = !_subUiFormsByName[preIndexName];
            if (doInitializeFormArray) {
              _subUiFormsByName[preIndexName] = [];
            }
          }
          $parse(control.$name).assign(_subUiFormsByName, new UiForm(control));
        }
      });

      // Transform all arrays of uiForms into UiFormArrays
      _.forEach(_subUiFormsByName, function(uiFormOrArray, fieldName) {
        if (uiFormOrArray.length !== undefined) {
          _subUiFormsByName[fieldName] = new UiFormArray(uiFormOrArray);
        }
      });

      return _subUiFormsByName;
    }
  };

  return {
    instance: function(formCtrl) {
      return new UiForm(formCtrl);
    }
  };
}]);

'use strict';

/*global angular*/
/**
 * Directives: cvc-form, cvc-form-field, cvc-form-field-focus, cvc-form-field-error
 *
 * These directives handle asynchronous, automatic and immediate posting and validation of UI inputs
 * with our forms API.
 *
 * After placing these attributes, this API decorates your form controller (e.g. $scope.newUserForm)
 * by adding the $cvcForm member, which is the cvc-form directive's controller.
 *
 * $cvcForm performs the following tasks for you:
 *   * Saves the UI data to the forms API milliseconds after a registered input changes, triggering remote validation.
 *   * Sets error states onto whatever inputs are found by the server to be in error. These error states
 *     will be prefaced by 'remote__'.
 *     Example: $scope.register.email.$errors['remote__not_unique'] will be true if the server returns an error 'not_unique'
 *   * Provides a public API for dismissing form changes, detecting errors that should be displayed,
 *     and submitting form contents.
 *   * Provides a per-input public API for detecting errors that should be displayed.
 *
 * .$cvcForm API (e.g. newUserForm.$cvcForm.<method>):
 *   .$submit() -- Submits the current state of the form's UI controls, telling the form API that
 *       the form is complete. Returns a promise that is resolved when the submission is
 *       successfully completed (as determined by status === 'completed').
 *
 *   .$dismissChanges() -- Deletes the current remote form and resets it to its initial
 *       values. Populates the form's UI inputs with said initial values. Returns
 *       a promise that is resolved when the inputs are returned to their initial values.
 *
 *   .$submitting -- true that the UI input is currently submitting its final contents to the forms api.
 *
 *   .$saving -- true that changes to the UI inputs are currently being saved to the forms API for validation.
 *
 *   $initialLoad() -- returns a promise that is resolved when the initial form data is populated.
 *
 *   .$isMakingRemoteRequest() -- Returns true that the form is either:
 *       (1) saving state to the forms API
 *       (2) submitting to the forms API
 *       (3) Retrieving a new (or incomplete) form from the forms API
 *
 *   .$isReadyForSubmit() -- Returns true that the form is ready for final submission.
 *       Use this to determine whether or not to enable a submit control. A form is in this state when the
 *       following criteria are met:
 *         (1) The form data has been altered from its initial values (in the current session or a previous one)
 *         (2) All alteration have been saved to the forms API
 *         (3) There were no errors in the saved data
 *
 *   .$isNotReadyForSubmit() -- Converse of .$isReadyForSubmit()
 *
 *   .$isWorkInProgress() -- Returns true that the form is not in its initial state:
 *       it has been edited by the user either in this session or in a previous one.
 *       Use this when determining whether or not to show Cancel / Save controls.
 *
 *   .$hasDisplayableErrors() -- Returns true that the form has errors that should be
 *       displayed. A displayable error meets the following criteria:
 *         (1) The form is in an error state
 *         (2) The form is a work in progress (it is not in its initial state)
 *         (3) The form is not currently saving its data to the forms API for review, which could
 *             potentially report that the error has already been fixed.
 *
 *  per-input API (e.g. newUserForm.email.<api>):
 *    .$saving -- true that this input has been changed, and the change is currently
 *        being saved to the forms API.
 *    .$isDisplayableError(errorName) -- Returns true that the specified error name is present,
 *        and that it is suitable for display to the user. A suitable error meets these criteria:
 *        (1) The model value exhibits the specified error.
 *        (2) The user has already interacted with the specified control. This can be a touch, a blur,
 *            or any other custom action for custom inputs (such as the file uploader)
 *        (3) The input is not currently being saved to the forms API, which could report that
 *            the error has already been fixed.
 *
 *        Example: <div ng-show="newUserForm.email.$isDisplayableError('remote__not_unique')">This email is taken!</div>
 *
 *    class="cvc-form-saving" -- class is applied to any 'cvc-form-field' element while its value
 *        is being saved.
 *
 *    class="cvc-form-not-saving" -- class is applied to any 'cvc-form-field' element while its value
 *        is not currently being saved.
 *
 * Complete Example:
 *   <div ng-controller='MyPageController'>
 *      <form cvc-form
 *            cvc-form-type="new_user" // as registered on the back-end
 *            cvc-form-context="brokerage.id()" // Only if the registered back-end form needs a context.
 *            name="newUserForm" // The form's standard FormController is placed on the current scope with this name
 *            ng-submit="submitForm()"> // function to call once a final submission is requested by the user.
 *                                      // define this on your own scope. Implementation should be
 *                                      // e.g. $scope.newUserForm.$cvcForm.$submit()
 *        <input type="text"
 *               cvc-form-field // Registers this inputs model as a data member of a cvc form
 *               cvc-form-delay="10" // Delay in milliseconds before submitting the changed input for validation
 *                                   // to the forms API. defaults to 500 milliseconds.
 *               name="email" // Must match the field name on the back-end form
 *               ng-model="newUser.email" // Assign this to whatever model you want, but the ng-model attribute must be there!
 *               >
 *         // Show errors with this input
 *         <alert type="danger" ng-show="newUserForm.email.$hasDisplayableErrors()">
 *           <ol cvc-form-field-focus="newUserForm.email">
 *             // Each of these fields will only appear if their error has been detected on the field's ngModelController
 *             // through $newUserForm.email.$isDisplayableError(<the error>);
 *             <li cvc-form-field-error="remote__not_unique">This brokerage name is already in use</li>
 *             <li cvc-form-field-error="remote__required">This brokerage name is already in use</li>
 *           </ol>
 *           // Alternate, more wordy and more direct way to show errors
 *           <div ng-show="newUserForm.email.$isDisplayableError('remote__not_unique')>
 *             This brokerage name is already in use
 *           </div>
 *           <div ng-show="newUserForm.email.$isDisplayableError('remote__required')">
 *             We're gonna need this one!
 *           </div>
 *         </alert>
 *       </form>
 *       <alert type="danger" ng-show="newUserForm.$cvcForm.$hasDisplayableErrors()">
 *         Oh snap! Looks like there's an error in your form.
 *       </alert>
 *       <div class="submit-controls" ng-show="newUserForm.$cvcForm.$isWorkInProgress()">
 *          <button class="submit"
 *                  ng-click="submitForm()"
 *                  ng-disabled="newUserForm.$cvcForm.$isNotReadyForSubmit()">
 *            Submit
 *          </button>
 *          <button class="cancel" ng-click="dismissChanges()">Dismiss changes</button>
 *       </div>
 *       <div ng-show="newUserForm.$cvcForm.$submitting">Loading...</div>
 *   </div>
 *   <script>
 *     angular.module('myApp.controllers').controller('MyPageController', function($scope) {
 *       $scope.submitForm = function() {
 *         $scope.newUserForm.$cvcForm.$submit().then(function() { console.log('Submission successful!'); })
 *       }
 *       $scope.dismissChanges = function() {
 *         $scope.newUserForm.$cvcForm.$dismissChanges().then(function() { console.log('Changes canceled!'); });
 *       }
 *     });
 *   </script>
 *
 */

angular.module('myApp.directives')
.directive(
'cvcForm',
["logging", "UiForms", function(logging, UiForms) {
  var log = logging.namespace('cvc-form');

  return {
    restrict: 'A',
    scope: {
      cvcFormType: '@',
      cvcFormData: '=',
      cvcFormContext: '=?'
    },
    controller: ['$scope', '$http', '$timeout', '$parse', '$attrs', '$interpolate', 'api2', '$q', 'Forms', function($scope, $http, $timeout, $parse, $attrs, $interpolate, api2, $q, Forms) {
      var _formCtrl;
      var _uiForm;
      var _scheduledSubmission;
      var _remoteForm;
      var _initialLoad = $q.defer();
      var cvcForm = this;

      //
      // Public API
      //
      cvcForm.$submit = function() {
        _remoteForm = _remoteForm.withSubmittedState();
        _uiForm.setSubmitting(true);
        return cvcForm.$saveCurrentState().then(function() {
          return $q(function(resolve, reject) {
            if(_remoteForm.isComplete()) {
              resolve(cvcForm.$$getRemoteForm());
            } else {
              reject();
            }
          });
        }).finally(function() {
          _uiForm.setSubmitting(false);
        });
      };

      cvcForm.$dismissChanges = function() {
        var deletedForm = _remoteForm;
        log('Deleting form ID ' + deletedForm.description() + '...');
        return _remoteForm.delete().then(function() {
          log('Deleted it');
          return cvcForm.$$getRemoteForm().then(function() {
            log('Replaced deleted form ' + deletedForm.description() + ' with ' + _remoteForm.description());
            _uiForm.resetRemoteErrors(_remoteForm.errors());
          });
        });
      };

      cvcForm.$isReadyForSubmit = function() {
        if (_remoteForm) {
          return _remoteForm.isSubmittable();
        }
      };

      cvcForm.$isNotReadyForSubmit = function() {
        return !cvcForm.$isReadyForSubmit();
      };

      cvcForm.$isMakingRemoteRequest = function() {
        return cvcForm.$saving || cvcForm.$submitting || cvcForm.$retrieving;
      };

      cvcForm.$isWorkInProgress = function() {
        return _remoteForm && _remoteForm.isWorkInProgress();
      };

      cvcForm.$setWorkInProgress = function() {
        _remoteForm = _remoteForm.withState(Forms.States.IN_PROGRESS);
      };

      cvcForm.$initialLoad = function() {
        return _initialLoad.promise;
      };

      cvcForm.$hasDisplayableErrors = function() {
        var errorsExist = !_formCtrl.$valid;
        var shouldShowError =  _remoteForm && _remoteForm.isWorkInProgress() && _formCtrl.$invalid && !cvcForm.$saving;

        return errorsExist && shouldShowError;
      };

      //
      // Protected API
      //
      cvcForm.$$setFormCtrl = function(formCtrl) {
        _formCtrl = formCtrl;
        _uiForm = UiForms.instance(formCtrl);
        formCtrl.$cvcForm = cvcForm;
      };

      cvcForm.$$getRemoteForm = function() {
        // Do not get the remote form if a formContext was specified but not yet available.
        if (_isFormContextSpecified() && !_formContext()) {
          log('Not retrieving remote form "' + _formType() + '" because cvc-form-context was specified but empty');
          return;
        }

        // Get the remote form
        log('Retrieving remote form "' + _formType() + '"' + (_formContext()? ' (context=' + _formContext() + ')': ''));
        cvcForm.$retrieving = true;
        var resultPromise = Forms.getNewOrIncomplete(_formType(), _formContext()).then(function(form) {
          log('Retrieved ' + form.description());
          _remoteForm = form;

          $scope.cvcFormData = form.data();

          // the form touch and error status will be updated on the next $digest
          // because if we did it right now then angular wouldn't have yet applied
          // new array-based inputs from the downloaded cvc form data into their NgController
          // instances.
          _updateUiTouchedStateOneDigestFromNow();
          _initialLoad.resolve(form);
          return form;
        });

        resultPromise.finally(function() {
          cvcForm.$retrieving = false;
        });

        return resultPromise;
      };

      cvcForm.$saveCurrentState = function(config) {
        var beginPromise;
        config = angular.extend({
          delayMs: 0
        }, config || {});

        // Cancel any already scheduled submissions
        if (_scheduledSubmission) {
          $timeout.cancel(_scheduledSubmission);
        }
        _uiForm.setSaving(true);
        if (config.delayMs === 0) {
          var deferred = $q.defer();
          beginPromise = deferred.promise;
          deferred.resolve();
        } else {
          _scheduledSubmission = $timeout(function() { }, config.delayMs);
          beginPromise = _scheduledSubmission;
        }
        return beginPromise.then(function() {
          var formToSubmit = _remoteForm.withData($scope.cvcFormData);
          log('Submitting current form:');
          log(formToSubmit.description());
          return formToSubmit.save().then(function(savedForm) {
            _remoteForm = savedForm;
            _uiForm.setSavingRecursive(false);
            _uiForm.resetRemoteErrors(savedForm.errors());
          });
        });
      };

      //
      // Private API
      //
      /** Return all of the form's ngModelControllers, indexed by name */
      var _updateUiTouchedStateOneDigestFromNow = function() {
        var digestCount = 0;
        var stopWatching = $scope.$watch(function countDigestsSinceErrorResetRequest() {
          try {
            if (digestCount === 1 && _remoteForm) {
              _uiForm.resetRemoteErrors(_remoteForm.errors());

              if (_remoteForm.isWorkInProgress()) {
                _uiForm.touchPresentFieldsRecursive();
              }
            }
          } finally {
            digestCount += 1;
          }

          if (digestCount > 1) {
            stopWatching();
          }
        });
      };

      var _formType = function() {
        return $scope.cvcFormType;
      };

      var _isFormContextSpecified = function() {
        return 'cvcFormContext' in $attrs;
      };

      var _formContext = function() {
        return $scope.cvcFormContext;
      };

      var _init = function() {
        if (_isFormContextSpecified()) {
          $scope.$watch('cvcFormContext', function(newValue, oldValue) {
            if (newValue !== oldValue) {
              cvcForm.$$getRemoteForm();
            }
          });
        }
      };

      _init();
    }],

    require: ['form', 'cvcForm'],
    link: function cvcFormLink (scope, element, attrs, requisites) {
      var form = requisites[0];
      var cvcForm = requisites[1];

      cvcForm.$$setFormCtrl(form);
      var futureResult = cvcForm.$$getRemoteForm();
      cvcForm.$initialLoad = futureResult;
    }
  };
}])
.directive(
'cvcFormField',
["logging", "$parse", function(logging, $parse) {
  var log = logging.namespace('cvc-form-field');

  return {
    restrict: 'A',
    require: ['^cvcForm', 'ngModel'],
    controller: function(){},
    link: function(scope, element, attrs, requisites) {
      var cvcForm = requisites[0];
      var inputControl = requisites[1];
      var submissionDelayMs = ('cvcFormDelay' in attrs)? parseInt(attrs['cvcFormDelay']): 500;
      var submitFormWithDelay = function(delayMs) {
        inputControl.$$setSaving(true);
        cvcForm.$saveCurrentState({delayMs: delayMs});
      };
      //
      // Public API
      //
      inputControl.$isDisplayableError = function(errorName) {
        // Only controls that are invalid, have been interacted by the user, and are not currently saving
        // are worthy of display to the user.
        var errorExists = inputControl.$error[errorName];
        var shouldShowError = inputControl.$invalid && inputControl.$touched && !inputControl.$saving;

        return errorExists && shouldShowError;
      };

      inputControl.$hasDisplayableErrors = function() {
        return inputControl.$invalid && inputControl.$touched && !inputControl.$saving;
      };

      //
      // Protected API
      //
      inputControl.$$setSaving = function(saving) {
        inputControl.$saving = saving;
        if (saving) {
          element.addClass('cvc-form-saving');
          element.removeClass('cvc-form-not-saving');
        } else {
          element.removeClass('cvc-form-saving');
          element.addClass('cvc-form-not-saving');
        }
      };

      inputControl.$$writeRemoteValue = function(value) {
        $parse(attrs.ngModel).assign(scope, value);
      };

      inputControl.$$setSaving(false);
      inputControl.$viewChangeListeners.push(function() {
        submitFormWithDelay(submissionDelayMs);
      });
    }
  };
}])
.directive(
'cvcFormFieldFocus',
["logging", function(logging) {
  var log = logging.namespace('cvcFormFieldFocus');
  return {
    restrict: 'A',
    scope: {
      ngModelController: '=cvcFormFieldFocus'
    },
    controller: ["$scope", "$attrs", function($scope, $attrs) {
      var self = this;
      var checkValidity = function() {
        if (!('cvcFormFieldFocus' in $attrs)) {
          throw 'The directive "cvc-form-field-focus" requires a scoped ngModelController as a parameter. ' +
            'e.g. <div cvc-form-field-focus="myFormController.email"></cvc-form-field-focus>.' ;
        }
      };
      this.ngModelController = undefined;

      $scope.$watch('ngModelController', function(newModelController) {
        self.ngModelController = newModelController;
      });

      checkValidity();
    }]
  };
}])
.directive(
'cvcFormFieldError',
["logging", function(logging) {
  var log = logging.namespace('cvcFormFieldError');
  return {
    restrict: 'A',
    scope: {},
    require: ['^cvcFormFieldFocus'],
    transclude: true,
    template: '<div class="cvc-form-field-error" ng-show="errorIsDisplayable()"><ng-transclude></ng-transclude></div>',
    link: function cvcFormFieldErrorLink(scope, elem, attrs, requisites) {
      var cvcFormFieldFocus = requisites[0];
      var errorToWatch = attrs.cvcFormFieldError;
      var checkValidity = function() {
        if (!errorToWatch) {
          throw 'The directive "cvc-form-field-error" requires a specific error to watch. Set it with e.g. cvc-form-field-error="remote__not_unique"';
        }
      };

      scope.errorIsDisplayable = function() {
        var ngModelController = cvcFormFieldFocus.ngModelController;
        return ngModelController && ngModelController.$isDisplayableError(attrs.cvcFormFieldError);
      };

      checkValidity();
    }
  };
}]);

'use strict';

/*
 * Configurable logging service. Disabled by default. Use like this:
 *
 * var log = logging.namespace('MyController');
 * log('herp'); // [MyController.js:24] herp
 * log('derp'); // [MyController.js:25] derp
 */
angular.module('myApp.services')
.service('logging',
['cvcConfig', '$log',
function(cvcConfig, $log) {
  var noop = angular.noop;
  var config = function() {
    return cvcConfig.get('logging', {
      level: 'none'
    });
  };
  var enabled = function() {
    return config().level === 'all';
  };
  var disabled = function() {
    return !enabled();
  };
  var log = function(msg) {
    if (enabled()) {
      $log.log(msg);
    }
  };
  var callContext = function() {

    try {
      var error = (new Error());
      var stack = error.stack;
      var frames = _.filter(stack.split('\n'), function(frame) { return frame !== 'Error'; });
      var framesFromCallingMethod = _.filter(frames, function(frame) { return frame.indexOf('logging.js') === -1; });
      var loggingFrame = framesFromCallingMethod[0];
      var lineNumberRegex = /([^\/]*)\.js:([0-9]+):[0-9+]/;
      var match = lineNumberRegex.exec(loggingFrame);
      if (match !== null && match.length > 1) {
        return {
          fileName: match[1] + '.js',
          lineNumber: match[2]
        };
      }
    } catch(err) {
      return undefined;
    }
  };

  var logging = {
    namespace: function(ns) {
      var nsString = ns.toString();

      return disabled()? noop: function(message) {
        try {
          var context = callContext();
          var namespace = (context && context.fileName)? context.fileName: nsString;
          var nsAndLine = context? (namespace + ':' + context.lineNumber): namespace;
          var nsPrefix = '[' + nsAndLine + ']';

          if (typeof message === 'object') {
            log(nsPrefix + ' (logging object)');
            log(message);
          } else {
            log(nsPrefix + ' ' + message);
          }
        } catch(err) {
          $log.error('Oops -- logging module is broken. This is all we know:');
          $log.error(err);
        }
      };
    }
  };

  return logging;
}]);

'use strict';

/*
 * Provides accessors and URLs for the citizen.vc v2 API
 */
angular.module('myApp.services')
.service('api2',
['ENV',
function(ENV) {
  var api2 = {
    host: function() { return ENV.apiEndpoint; },

    base: function() { return api2.host() + '/api/2'; },

    forms: function() {
      return api2.base() + '/forms';
    },

    brokerages: function() {
      return api2.base() + '/brokerages';
    },

    users: function() {
      return api2.base() + '/users';
    },

    files: function() {
      return api2.base() + '/files';
    },

    offerings: function() {
      return api2.base() + '/offerings';
    },

    series: function() {
      return api2.base() + '/series';
    },

    seriesParticipations: function() {
      return api2.base() + '/series_participations';
    },

    investmentAppProgress: function() {
      return api2.base() + '/investment_application_progress';
    },

    docs: function () {
      return api2.base() + '/docs';
    },

    investmentPacketDocs: function () {
      return api2.base() + '/investment_packet_docs';
    }
  };

  return api2;
}]);

'use strict';

angular.module('myApp.services').service('moment', ['$window', function($window) {
  return $window.moment;
}]);

'use strict';

/**
 * Controller for company page
 */
angular.module('myApp.controllers').controller(
  'CompanyPageController',
  ["$scope", "$modal", "formatters", "$rootScope", "Auth", "$cookieStore", "CompanyPrivateView", "$routeParams", "GetCompanyQuestions", "$location", "URLS", "GetRaisedFunding", "$sce", "MSG", "FlashOnNextRoute", "BASE_PATH", "CompanySnapshot", "INVEST_MSGS", "Users", "Deals", "ObjectDBs", function(
    $scope,
    $modal,
    formatters,
    $rootScope,
    Auth,
    $cookieStore,
    CompanyPrivateView,
    $routeParams,
    GetCompanyQuestions,
    $location,
    URLS,
    GetRaisedFunding,
    $sce,
    MSG,
    FlashOnNextRoute,
    BASE_PATH,
    CompanySnapshot,
    INVEST_MSGS,
    Users,
    Deals,
    ObjectDBs)
  {
    var _dealData;
    $scope.theme = 'v2';
    $scope.userObj = Users.authorized();

    var ModalCtrl =[ '$scope', '$modalInstance', function( $scope, $modalInstance ) {
      $scope.modalTeamMember = _.where($scope.companyLeaders, {id: $scope.modalSelectedId})[0];

      $scope.cancelModal = function () {
        $modalInstance.close();
      };
    }];

    $scope.statesMap = {
      'started':URLS.LEGAL_DOCUMENTS,
      'ppm':URLS.BANK_PAYMENT,
      'payment':URLS.INVESTOR_QUESTIONNAIRE,
      'questionnaire':URLS.RISK_FACTOR_ACK,
      'ra':URLS.IRA
    };

    if($routeParams.series) {
      var db = ObjectDBs.temporary();
      Deals.getSeries($routeParams.series, {db: db}).then(function(series) {
        _dealData = {
          db: db,
          series: series,
          offering: series.local(db).offering(),
          company: series.local(db).company(),
          security: series.local(db).security()
        };
      });
    }

    $scope.showMember = function (id) {
      $scope.modalSelectedId = id;
      $scope.opts = {
        backdrop: true,
        backdropClick: true,
        scope: $scope,
        dialogFade: false,
        keyboard: true,
        controller: ModalCtrl,
        templateUrl : 'components/company-page/_team-modal.html',
        resolve: {} // empty storage
      };

      $modal.open($scope.opts);
    };

    var id = $routeParams.id;
    $scope.company_template = 'components/company-page/_base.html';

    //$scope.series = 'seed';
    $scope.questions = [];
    $scope.payment={};
    $scope.contact_citizen={};
    $scope.pdfUrl = '';
    $scope.ques = {};
    $scope.ques.ent_ques = [];
    $scope.newQuestion = {};

    $scope.investment_round = {
      'no_round': INVEST_MSGS.NO_INVEST_ROUND,
      'end_date_crossed':INVEST_MSGS.END_DATE_CROSSED,
      'goal_reached':INVEST_MSGS.GOAL_REACHED,
      'modify_state':INVEST_MSGS.MODIFY_STATE,
      'pending_state':INVEST_MSGS.PENDING_FUNDING
    };

    $scope.embed='';

    /**
     *  This functions runs on page initiation
     *
     */
    $scope.init = function (){
      if($routeParams.snapshot) {
        CompanySnapshot($.cookie('accessToken')).details({id:$routeParams.snapshot}).$promise.then(
          function(value){
            setSnapshotData(value);

          },
          function(error){
            console.log(error);
          }
        );
      } else {
        CompanyPrivateView($.cookie('accessToken')).
          show({id: id}).
          $promise.then(
            function(value){
              setCompanyData(value);
            },
            function(/*error*/){}
          );
      }
      $('.loader-overlay').fadeIn('fast');

    };

    $scope.initFunding = function(){
      var path = $location.path();
      var lastIndexOfSlash=path.lastIndexOf('/');
      var currentPath =path.substring(0,lastIndexOfSlash+1);

      if (currentPath === URLS.REWORK_FUND_RAISING){
        GetRaisedFunding($.cookie('accessToken')).get({ id: $routeParams.id}).$promise.then(
          function(value){
            $scope.message = value.funding_details.comment;
            $scope.amount_seeking = value.funding_details.seeking_amount;
            $scope.series = value.funding_details.series_type;
            $scope.funding_start_date = new Date(value.funding_details.start_date);
            $scope.funding_close_date = new Date(value.funding_details.end_date);
            $scope.company = value.company;
            $scope.documents = value.documents;
          },
          function(/*error*/){}
        );
      }
    };

    $scope.logout = function(){
      FlashOnNextRoute.setMessage({type: 'neutral', text: MSG.LOGOUT});
      Auth.logout();
    };

    $scope.gotoEditCompany = function(){
      $location.path(URLS.EDIT_COMPANY+$routeParams.id);
    };

    function setCompanyData(value){
      $scope.company = value.company;
      $scope.company.description = $sce.trustAsHtml($scope.company.description);
      $rootScope.company = value.company;
      $rootScope.company_id = value.company.id;
      $rootScope.company.id = id;
      $scope.funding = (value.funding_details ? value.funding_details : null);
      if($scope.funding!=null){
        if($scope.funding.status === 'open'){
          $scope.company.state = 'OPEN';
        }
        else if($scope.funding.status === 'modify pending'){
          $scope.company.msg = value.card_message;
          $scope.company.state = 'ON HOLD';
        }
        else if($scope.funding.status === 'open pending'){
          $scope.company.msg = value.card_message;
          $scope.company.state = 'ESCROW PENDING';
        }
        else{
          $scope.company.msg = value.card_message;
          $scope.company.state = 'CLOSED';
        }
      }
      else{
        $scope.company.msg = value.card_message;
        $scope.company.state = 'COMING SOON';
      }
      var todayDate = new Date();
      var anotherToday = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate());
      if($scope.funding != null){
        $scope.funding.end_date_crossed = anotherToday > new Date($scope.funding.end_date) ;
        $scope.so_far_raised=(value.funding_details.raised_amount ? value.funding_details.raised_amount:null);
      }

      if(value.investment != null){
        $scope.your_investment = value.investment;
      }
      $scope.progress_data = value.progress_data;
      $scope.companyHighlights = value.highlights;
      $rootScope.documents = value.documents;
      $scope.companyLeaders = value.social_links;
      $scope.members = value.company.members;
      $scope.invest_options = value.recommended_amounts;
      $scope.apply_button = value.apply_button;
      $scope.processTimelineData();

      if(value.funding_details != null){
        var investmentProgress={dataPoint:[]};
        var deno = parseFloat($scope.funding.seeking_amount);

        var a = parseFloat($scope.funding.seeking_amount)/deno;
        var b = 0;
        var c = 0;
        if($scope.your_investment){
          b = (parseFloat($scope.funding.raised_amount)-parseFloat($scope.your_investment))/deno;
          c = parseFloat($scope.your_investment) / deno;
        } else {
          b = parseFloat($scope.funding.raised_amount)/deno;
        }

        if(parseFloat($scope.funding.raised_amount) >= parseFloat($scope.funding.seeking_amount)){
          a=0;
        }

        investmentProgress.dataPoint.push(a);
        investmentProgress.dataPoint.push(b);
        investmentProgress.dataPoint.push(c);

        $scope.investmentProgress=investmentProgress;
      }

      var video_link = _.find(value.documents, function(item){ return (item.document_type==='company_video_link'); });
      if (video_link){
        $scope.video_link = $sce.trustAsResourceUrl(video_link.link);
      }
      var pitch_file = _.find(value.documents, function(item){ return (item.document_type==='company_pitch_file'); });
      if(pitch_file){
        if(pitch_file.file.substring(0, 4) === 'http') {
          $scope.pdfUrl = pitch_file.file;
        }
        else{
          $scope.pdfUrl = BASE_PATH.API() + pitch_file.file;
        }
      }

      if ($routeParams.tab === 'qna') {
        $scope.loadQuestionsTab();
        $('html, body').animate({scrollTop:$('#info_container').offset().top -50}, 1000);
      }

      $scope.$parent.invest_amount=null;

      $scope.showInvestment=true;
      $('.loader-overlay').fadeOut('fast');

    }

    function setSnapshotData(value){
      $scope.company = value.company;
      $rootScope.company = value.company;
      $rootScope.company_id = value.company.id;
      $rootScope.company.id = id;
      $scope.funding = (value.funding_details ? value.funding_details : null);
      $scope.so_far_raised=(value.so_far_raised ? value.so_far_raised:null);
      $scope.your_investment=(value.investment ? value.investment:null);
      $scope.companyHighlights = value.highlights;
      $rootScope.documents = value.documents;
      $scope.companyLeaders = value.social_links;
      $scope.members = value.company.members;
      $scope.progress_data = value.progress_data;
      $scope.so_far_raised=(value.funding_details.raised_amount ? value.funding_details.raised_amount:null);
      $scope.apply_button = value.apply_button;
      $scope.processTimelineData();

      if(value.investment != null){
        $scope.your_investment = value.investment;
      }
      if($scope.funding != null){
        if($scope.funding.status === 'open'){
          $scope.company.state = 'OPEN';
        }
        else if($scope.funding.status === 'modify pending'){
          $scope.company.msg = value.card_message;
          $scope.company.state = 'ON HOLD';
        }
        else if($scope.funding.status === 'open pending'){
          $scope.company.msg = value.card_message;
          $scope.company.state = 'ESCROW PENDING';
        }
        else{
          $scope.company.msg = value.card_message;
          $scope.company.state = 'CLOSED';
        }
      }
      else{
        $scope.company.msg = value.card_message;
        $scope.company.state = 'COMING SOON';
      }
      var video_link = _.find(value.documents, function(item){ return (item.document_type==='company_video_link'); });
      if (video_link){
        $scope.video_link = $sce.trustAsResourceUrl(video_link.link);
      }
      var pitch_file = _.find(value.documents, function(item){ return (item.document_type==='company_pitch_file'); });
      if(pitch_file){
        if(pitch_file.file.substring(0, 4) === 'http') {
          $scope.pdfUrl = pitch_file.file;
        }
        else{
          $scope.pdfUrl = BASE_PATH.API() + pitch_file.file;
        }
      }
      if(value.funding_details != null){
        var investmentProgress={dataPoint:[]};
        var deno = parseFloat($scope.funding.seeking_amount);

        var a = parseFloat($scope.funding.seeking_amount)/deno;
        var b = 0;
        var c = 0;
        if($scope.your_investment){
          b = (parseFloat($scope.funding.raised_amount)-parseFloat($scope.your_investment))/deno;
          c = parseFloat($scope.your_investment) / deno;
        }
        else{
          b = parseFloat($scope.funding.raised_amount)/deno;
        }

        if(parseFloat($scope.funding.raised_amount) === parseFloat($scope.funding.seeking_amount)){
          a=0;
        }

        investmentProgress.dataPoint.push(a);
        investmentProgress.dataPoint.push(b);
        investmentProgress.dataPoint.push(c);

        $scope.investmentProgress=investmentProgress;
      }

      $cookieStore.remove('company');

      $scope.showInvestment=false;
    }

    /**
     * process the timeline data so that it is displaying most recent on top,
     * and adds
     */
    $scope.processTimelineData = function () {
      var currYear, dateObj, newHighlights = [];
      if ($scope.companyHighlights) {
        $scope.companyHighlights = _.sortBy($scope.companyHighlights , function(highlight){ return highlight.date; }).reverse();

        for (var highlight in $scope.companyHighlights) {
          dateObj = new Date($scope.companyHighlights[highlight].date);
          currYear = dateObj.getUTCFullYear();
          $scope.companyHighlights[highlight].year = currYear;
          newHighlights.push($scope.companyHighlights[highlight]);
        }
      }

      $scope.processedHighlights = newHighlights;
    };

    $scope.returnFromPdfView = function () {
      $scope.company_template = 'components/company-page/_base.html';
    };

    $scope.viewStandalonePDF = function(doc/*,print*/){ // print flag ignored for now, soon to be restored
      $scope.showPrintButton = false;
      $location.path(URLS.PDF_VIEWER(doc.file));
    };

    $scope.$on('COMPANY_RESET_INVEST_ERROR', function(){
      $scope.company_invest_error = null;
    });


    $scope.beginInvestmentLegalProcess = function(amount){
      if(!amount || amount.toString().trim() === '') {
        $scope.company_invest_error=MSG.VALID_AMOUNT;
        return;
      }

      var fundraiseRemaining = Math.max(parseFloat($scope.funding.seeking_amount) - parseFloat($scope.so_far_raised), 0.0);
      var maxInvestment = Math.min(fundraiseRemaining, $scope.funding.local_maximum);
      var minInvestment = Math.min(parseFloat($scope.funding.local_minimum), maxInvestment);

      if (parseFloat(amount) < minInvestment || parseFloat(amount) > maxInvestment) {
        $scope.company_invest_error = MSG.FUNDING_AMOUNT_OUT_OF_RANGE(
          formatters.numberAsMoney(minInvestment),
          formatters.numberAsMoney(maxInvestment)
        );
        return;
      }

      var id=$routeParams.id;

      $rootScope.invest_amount=amount;
      $rootScope.invest_company_name = $scope.company.name;

      var cookie_data ={
        'company_id':$scope.funding.company,
        'funding_id':$scope.funding.id,
        'invest_amount':amount,
        'company_name':$scope.company.name
      };

      $scope.$broadcast('COMPANY_CLOSE_INVEST_MODAL', {});
      $.cookie('company',JSON.stringify(cookie_data),{path:'/'});
      $location.path(URLS.LEGAL_DOCUMENTS+id);
    };

    $scope.completeUnfinishedInvestment = function(){
      var redirection_path = $scope.statesMap[$scope.progress_data.state];
      var company_slug = $routeParams.id;
      if($scope.progress_data.state === 'ppm'){
        var a_date = new Date();
        var b_date = new Date(a_date.getFullYear(), a_date.getMonth(), a_date.getDay());

        if(parseFloat($scope.funding.raised_amount) >= parseFloat($scope.funding.seeking_amount)){
          $scope.company.modal_msg = INVEST_MSGS.COMPANY_POPUP_GOAL_MET;
          $('#completeUnfinishedModal').modal('show');
          return;
        }
        else if($scope.company.state === 'CLOSED'){
          $scope.company.modal_msg =  INVEST_MSGS.COMPANY_POPUP_ROUND_CLOSE;
          $('#completeUnfinishedModal').modal('show');
          return;
        }
        else if($scope.funding.status === 'modify pending'){
          $scope.company.modal_msg =  INVEST_MSGS.COMPANY_POPUP_MODIFY_PENDING;
          $('#completeUnfinishedModal').modal('show');
          return;
        }
        else if(b_date > new Date($scope.funding.end_date)){
          $scope.company.modal_msg = INVEST_MSGS.COMPANY_POPUP_END_DATE_REACHED;
          $('#completeUnfinishedModal').modal('show');
          return;
        }
      }
      var cookie_data = {
        'company_id':$scope.funding.company,
        'funding_id':$scope.funding.id,
        'invest_amount':$scope.progress_data.amount,
        'company_name':$scope.company.name
      };
      switch($scope.progress_data.state){
        case 'started':
          break;
        case 'ppm':
          angular.extend(cookie_data, {
            'progress_id':$scope.progress_data.id
          });
          break;
        case 'payment':
        case 'questionnaire':
        case 'ra':
          angular.extend(cookie_data, {
            'progress_id':$scope.progress_data.id,
            'investment_id':$scope.progress_data.investment
          });
          break;
      }
      $.cookie('company',JSON.stringify(cookie_data),{path:'/'});
      $location.path(redirection_path+company_slug);
    };

    function getKulizaFundingStates() {
      var status;
      if($scope.funding.status === 'open'){
        status = Deals.SeriesStatus.OPEN;
      } else if($scope.funding.status === 'closed'){
        status = Deals.SeriesStatus.CLOSED;
      } else if($scope.funding.status === 'open pending'){
        status = Deals.SeriesStatus.OPEN_PENDING;
      } else{
        status = Deals.SeriesStatus.ON_HOLD;
      }

      return status;
    }


    //
    // Series-specific stuff
    //
    $scope.seriesName = function() {
      return _dealData && _dealData.series.name();
    };

    $scope.companyName = function() {
      return _dealData && _dealData.company.name();
    };

    $scope.securityName = function() {
      return _dealData && _dealData.security.name();
    };

    $scope.offeringGoalAmount = function() {
      return _dealData && _dealData.offering.goal();
    };

    $scope.offeringPricePerShare = function() {
      return _dealData && _dealData.offering.pricePerShare();
    };

    $scope.offeringValuation = function() {
      return _dealData && _dealData.offering.preMoneyValuation();
    };

    $scope.seriesCommissionPercentage = function() {
      return _dealData && _dealData.series.commissionPercentage();
    };

    $scope.seriesCarriedInterestPercentage = function() {
      return _dealData && _dealData.series.carryPercentage();
    };

    $scope.seriesPlacementFeePercentage = function() {
      return _dealData && _dealData.series.placementFeePercentage();
    };

    $scope.seriesStatus = function() {
      return (typeof _dealData !== 'undefined') ? (_dealData && _dealData.series.status()) : getKulizaFundingStates();
    };
  }]
);

'use strict';

angular.module('myApp.directives').directive('companySubNav',function(){
  return {
    restrict:'E',
    transclude: true,
    templateUrl: 'components/company-page/_subnav.html',
    link: function($scope) {
      $scope.init = function () {
        $(document).on('scroll', function() {
          $scope.makeSticky($('.right-col .sub-nav-menu'), $('.banner'));
        });
      };

      $scope.animateScroll = function(node) {
        $('html, body').animate({
          scrollTop: $('#' + node).offset().top - $('#nav-content').height()
        }, 750);
      };


      /**
       * Make an HTML element sticky -- mostly for sticky right rail
       * @param {Object} $stickyEl - the element to stick
       * @param {Object} $topEl - this is for other sticky elements on top, eg a sticky nav which we need to
       * factor in
       */
      $scope.makeSticky = function ($stickyEl, $topEl) {
        if($topEl) {
          if ($scope.isElementInViewport($topEl[0])) {
            $($stickyEl).removeClass('sticky');

          } else {
            $($stickyEl).addClass('sticky');
          }
        } else {
          $($stickyEl).addClass('sticky');
        }
      };


      /**
       * isElementInViewport -- helper method mostly used with $scope.makeSticky
       *
       * @param {Object} el
       */
      $scope.isElementInViewport = function (el) {
        var top, left, width, height;

        if (el) {
          top = el.offsetTop - $('#nav-content').height();
          left = el.offsetLeft;
          width = el.offsetWidth;
          height = el.offsetHeight;

          while(el.offsetParent) {
            el = el.offsetParent;
            top += el.offsetTop;
            left += el.offsetLeft;
          }

          return (
            top < (window.pageYOffset + window.innerHeight) &&
            left < (window.pageXOffset + window.innerWidth) &&
            (top + height) > window.pageYOffset &&
            (left + width) > window.pageXOffset
          );
        } else {
          return false;
        }
      };


      $scope.init();

    }
  };
});



'use strict';

angular.module('myApp.directives').directive('timelineItem',["$compile", "$rootScope", function($compile, $rootScope){
  var itemTemplate = ''+
    '<li class="date-item {{timelineDirection}}">'+
    '  <div class="date-indicator"></div>'+
    '  <div class="date-content-wrap">'+
    '    <div class="date-content">'+
    '      <div class="details">'+
    '        <img class="highlight-image" ng-src="{{highlight.file | filePath}}"  alt="Highlight" alt="Image"/>'+
    '        <div data-ng-bind-html="highlight.description"></div>'+
    '      </div>'+
    '    </div>'+
    '  </div>'+
    '  <div class="date-arrow"></div>'+
    '</li>';

  var yearTemplate = '<li class="date-year">{{highlight.year}}</li>';

  return {
    restrict:'E',
    link: function($scope, $element) {

      $scope.setDirection = function () {
        if (!$rootScope.timelineDirection) {
          $rootScope.timelineDirection = 'left';
        } else if ($rootScope.timelineDirection === 'left') {
          $rootScope.timelineDirection = 'right';
        } else {
          $rootScope.timelineDirection = 'left';
        }
        $scope.timelineDirection = $rootScope.timelineDirection;
      };



      $scope.setDirection();

      if (!$rootScope.prevYear) {
        $rootScope.prevYear = $scope.highlight.year;
        $rootScope.currYear = $scope.highlight.year;

        $element.html(yearTemplate + itemTemplate).show();

      } else if($rootScope.currYear != $scope.highlight.year) {
        $rootScope.prevYear =  $rootScope.currYear;
        $rootScope.currYear = $scope.highlight.year;

        $element.html(yearTemplate + itemTemplate).show();
      } else {

        $element.html(itemTemplate).show();
      }

      $compile($element.contents())($scope);

    }
  };
}]);



'use strict';

angular.module('myApp.directives').
  directive('companyInvest',["$modal", function($modal){
  return {
    restrict:'E',
    templateUrl: 'components/company-page/_invest.html',
    link: function($scope) {

      var ModalCtrl = [ '$scope', '$modalInstance', function( $scope, $modalInstance ) {
        $scope.processClick = function() {
          $scope.showConfirm = true;
        };

        $scope.enterAmount = function(amt) {
          $scope.showConfirm = true;
          $scope.investingAmount = amt;
        };

        $scope.cancelModal = function () {
          $modalInstance.dismiss();
        };

        $modalInstance.result.then(function () {}, function () {
          $scope.$emit('COMPANY_RESET_INVEST_ERROR', {}); //emitting event when modal is closing
        });

        $scope.$on('COMPANY_CLOSE_INVEST_MODAL', function() {
          $modalInstance.close();
        });
      }];

      $scope.invest = function(){
        $scope.opts = {
          backdrop: true,
          backdropClick: true,
          scope: $scope,
          windowClass: 'fade in',
          backdropClass: 'fade in',
          dialogFade: false,
          keyboard: true,
          controller: ModalCtrl,
          templateUrl : 'components/company-page/_invest-modal.html',
          resolve: {} // empty storage
        };

        $modal.open($scope.opts);
      };

    }
  };
}]);



'use strict';

angular.module('myApp.directives').directive('companyQuestionsToggle',function(){
  return {
    restrict:'A',
    link: function(scope,element){
      element.bind('click', function() {
        $(this).parent().find('.answer-container').toggleClass('on');
      });
    }
  };
});



'use strict';

angular.module('myApp.directives').directive('companyQuestionsAsk',["$rootScope", "AskCompanyQuestion", "$cookieStore", "URLS", function($rootScope, AskCompanyQuestion, $cookieStore, URLS){
  return {
    restrict:'A',
    link: function($scope){
      $scope.askQuestion = function(){
        var question = $scope.newQuestion.newQuestion;
        AskCompanyQuestion($.cookie('accessToken')).ask({question: question, company: $rootScope.company_id, investor: $cookieStore.get('user').entity_id, url: URLS.COMPANY+'%s/qna'}).$promise.then(
          function(value){
            var userProfile = $cookieStore.get('user').profile;
            userProfile.presented_name = 'You';
            $scope.questions.push({question: {question: value.question, modified_at: new Date()}, answers: [], author: userProfile});
            $scope.newQuestion.newQuestion = null;
          },
          function(error){
            console.log(error);
          }
        );
      };
    }
  };
}]);

'use strict';

angular.module('myApp.directives').
  // directive for companyCard
  directive('companyCard',["$http", "$compile", "Deals", function($http, $compile, Deals){
  return {
    restrict:'E',
    scope: true,
    templateUrl: 'components/cards/_company-card.html',
    link: function($scope, $element, $attrs) {
      var TEMPLATES = {
        OPEN:         'components/cards/_company-card-open.html',
        CLOSED:       'components/cards/_company-card-closed.html',
        CANCELLED:    'components/cards/_company-card-cancelled.html',
        OPEN_PENDING: 'components/cards/_company-card-open-pending.html',
        ON_HOLD:      'components/cards/_company-card-on-hold.html'
      };
      var SeriesStatus = Deals.SeriesStatus;

      $attrs.$observe('fundingStatus', function(newValue) {
        var tmpl = null;

        if (newValue !== null && newValue !== '') {
          $scope.fundingStatus = $attrs.fundingStatus;
          $scope.canInvest = $scope.$eval($attrs.canInvest);
          $scope.user = $scope.$eval($attrs.user);

          tmpl = $scope.getTemplate();

          $http.get(tmpl)
            .then(function(response){
              $element.html($compile(response.data)($scope));
              if ($scope.fundingStatus) {
                $scope.drawProgressBar();
              }
            }
          );
        }
      });

      $scope.getTemplate = function() {
        var template = TEMPLATES.OPEN_PENDING;
        var seriesStatus = $scope.seriesStatus();

        if (seriesStatus === SeriesStatus.OPEN) {
          template = TEMPLATES.OPEN;
        } else if (seriesStatus === SeriesStatus.CLOSED) {
          template = TEMPLATES.CLOSED;
        } else if (seriesStatus === SeriesStatus.NOT_YET_OPENED) {
          template = TEMPLATES.OPEN_PENDING;
        } else if (seriesStatus === SeriesStatus.ON_HOLD) {
          template = TEMPLATES.ON_HOLD;
        }

        return template;
      };

      /**
       * Additional CSS classes
       */
      $scope.getClasses = function() {
        var classes = ($attrs.ngClasses ? $attrs.ngClasses : '');

        return classes;
      };

      $scope.drawProgressBar = function () {
        var investmentProgress = $scope.$eval($attrs.investmentProgress),
        total = (investmentProgress.dataPoint) ? Math.floor((investmentProgress.dataPoint[1] + investmentProgress.dataPoint[2]) * 100) : 0;
        if (total < 1) {
          total = 1;
        } else if (total > 100) {
          total = 100;
        }

        $('.progress-bar .completeness').css('width', total+'%');
      };


    }
  };
}]);



'use strict';

/**
 * Controller for the Our Investments page (/our-investments)
 */
angular.module('myApp.controllers').controller(
'OurInvestmentsController',
['$scope',
function($scope) {
  $scope.theme = 'v2';

}]);

'use strict';

/**
 * Back Button!
 */
angular.module('myApp.directives').
directive('backButton', ["$window", function($window) {
  return {
    restrict: 'A',
    link: function (scope, elem) {
      elem.bind('click', function () {
        $window.history.back();
      });
    }
  };
}]);


'use strict';

/**
 * Crypto. A service for performing cryptography.
 *
 * Basically just wraps CryptoJS, our current cryptography vendor of choice.
 *
 * Members:
 *   .md5(str) -- returns the Base64 representation of the target string's
 *     MD5 hash.
 */
angular.module('myApp.services').service(
'Crypto',
function() {
  var CryptoJS = window.CryptoJS;
  var Crypto = {
    md5: function(str) {
      return CryptoJS.MD5(str).toString(CryptoJS.enc.Base64);
    }
  };

  return Crypto;
});

'use strict';

/**
 * Directive and controllers for adding and removing brokerage clients
 */
angular.module('myApp.directives')
.directive('investableOfferings',
function() {
  return {
    restrict: 'A',
    scope: {},
    controller: '_InvestableOfferingsController',
    templateUrl: 'components/investment-modules/investable-offerings.html'
  };
})
.controller('_InvestableOfferingsController',
["$scope", "Deals", "Brokerages", "SeriesCardModels", "ObjectDBs", "$q", function($scope, Deals, Brokerages, SeriesCardModels, ObjectDBs, $q) {
  var _seriesCardsBySeriesId = {};


  var _getRemoteData = function() {
    var db = ObjectDBs.temporary();
    var promises = {
      brokerages: Brokerages.allWithUserAsClient({db: db}),
      offerings: Deals.offeringsForInvestment({db: db})
    };

    return $q.all(promises).then(function(results) {
      _populateScope(results.offerings, db);
    });
  };

  var _populateScope = function(offerings, db) {
    var allSeries = _.flatten(_.map(offerings, function(offering) {
      return offering.local(db).allSeries();
    }));
    _seriesCardsBySeriesId = _.object(_.map(allSeries, function(series) {
      var seriesCard = SeriesCardModels.fromSeries(series, db, {
        investable: true
      });
      return [series.id(), seriesCard];
    }));

    $scope.allSeries = allSeries;
  };

  $scope.seriesModelById = function(seriesId) {
    return _seriesCardsBySeriesId[seriesId];
  };

  $scope.numSeries = function() {
    return $scope.allSeries && $scope.allSeries.length;
  };

  _getRemoteData();

}]);

'use strict';

angular.module('myApp.directives')
.directive('investorChooser',
function() {
  return {
    restrict: 'A',
    scope: {
      selectedInvestor: '=?',
      defaultInvestorId: '=',
      applicationAndDb: '=',
    },
    controller: '_InvestorChooserController',
    templateUrl: 'components/investment-modules/investor-chooser.html'
  };
})
.controller('_InvestorChooserController',
["$scope", "Investors", "logging", "$modal", "Users", "Investments", function($scope, Investors, logging, $modal, Users, Investments) {
  var log = logging.namespace('_InvestorChooserController');
  var ProgressType = Investments.ProgressType;
  var _loggingEntityChosen = false;

  var _withAppAndDb = function(fn) {
    if ($scope.applicationAndDb) {
      var appId = $scope.applicationAndDb.applicationId;
      var db = $scope.applicationAndDb.db;
      return fn(db.get(appId, {strict: true}), db);
    }
  };

  var InvestorItemModel = function(investor) {
    this._investor = investor;
  };

  InvestorItemModel.prototype = {
    id: function() {
      return this._investor.id();
    },

    isSelected: function() {
      return this._selected;
    },

    select: function() {
      $scope.selectedInvestor = this._investor;
    },

    name: function() {
      return this._investor.name();
    },

    selected: function() {
      return $scope.selectedInvestor.id() === this._investor.id();
    },

    entityType: function() {
      var geoString = this._investor.isTaxedInUS()? 'US ': 'Non-US ';
      return geoString + this._investor.entityType().humanReadable();
    }
  };

  var refreshInvestorItems = function() {
    $scope.investors = _.map(Investors.forCurrentUser(), function(investor) {
      return new InvestorItemModel(investor);
    });
  };

  $scope.openAddInvestor = function() {
    var opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      scope: $scope,
      controller: 'AddInvestmentEntityDialogController',
      templateUrl: 'components/investment-modules/add-investment-entity-dialog.html',
      resolve: {}
    };

    var addInvestorModal = $modal.open(opts);
    addInvestorModal.result.then(function() {
      Users.authorized().refresh().then(function() {
        refreshInvestorItems();
      });
    });
  };

  $scope.$watch('defaultInvestorId', function(newDefault) {
    _.each($scope.investors, function(investor) {
      if (investor.id() === newDefault) {
        log('Defaulting to investor ' + newDefault);
        investor.select();
        $scope.selectedInvestor = investor;
      }
    });

    if (!$scope.selectedInvestor && $scope.investors[0]) {
      $scope.investors[0].select();
    }
  });

  $scope.showContinueButton = function() {
    return _withAppAndDb(function(app, db) {
      return !app.local(db).investingUserSelectedEntity();
    });
  };

  $scope.disableContinueButton = function() {
    return _loggingEntityChosen;
  };

  $scope.logSelectedEntityAcknowledged = function() {
    _withAppAndDb(function(app, db) {
      _loggingEntityChosen = true;
      app.local(db).logProgress(ProgressType.INVESTING_USER_SELECTED_ENTITY).finally(function() {
        _loggingEntityChosen = false;
      });
    });
  };

  refreshInvestorItems();
}]);

'use strict';

angular.module('myApp.directives')
.directive('applicationInvestmentDetails',
function() {
  return {
    restrict: 'A',
    scope: {
      applicationAndDb: '='
    },
    controller: '_ApplicationInvestmentDetailsController',
    templateUrl: 'components/investment-modules/application-investment-details.html'
  };
})
.controller('_ApplicationInvestmentDetailsController',
["$scope", "Investments", "logging", function($scope, Investments, logging) {
  $scope.investAmount = 1e5;
  var _loggingAmountSelected = false;
  var ProgressType = Investments.ProgressType;

  var _withAppAndDb = function(fn) {
    if ($scope.applicationAndDb) {
      var appId = $scope.applicationAndDb.applicationId;
      var db = $scope.applicationAndDb.db;
      return fn(db.get(appId, {strict: true}), db);
    }
  };

  $scope.showProceedButton = function() {
    return _withAppAndDb(function(app, db) {
      return !app.local(db).investingUserFilledInvestmentAmount();
    });
  };

  $scope.disableProceedButton = function() {
    return _loggingAmountSelected;
  };

  $scope.logInvestmentAmountChosen = function() {
    _withAppAndDb(function(app, db) {
      _loggingAmountSelected = true;
      app.local(db).logProgress(ProgressType.INVESTING_USER_FILLED_INVESTMENT_AMOUNT).finally(function() {
        _loggingAmountSelected = false;
      });
    });
  };
}]);

'use strict';

angular.module('myApp.directives')
.directive('applicationOfferingSummary',
function() {
  return {
    restrict: 'A',
    scope: {
      applicationAndDb: '=',
    },
    controller: '_ApplicationOfferingSummaryController',
    templateUrl: 'components/investment-modules/application-offering-summary.html'
  };
})
.controller('_ApplicationOfferingSummaryController',
["$scope", "Deals", "SeriesCardModels", "Investments", function($scope, Deals, SeriesCardModels, Investments) {
  var ProgressType = Investments.ProgressType;
  var _loggingInstructionsAcknowledged = true;
  var _withAppAndDb = function(fn) {
    if ($scope.applicationAndDb) {
      var appId = $scope.applicationAndDb.applicationId;
      var db = $scope.applicationAndDb.db;
      return fn(db.get(appId, {strict: true}), db);
    }
  };

  $scope.$watch('applicationAndDb', function() {
    _withAppAndDb(function(app, db) {
      var series = app.local(db).series();
      $scope.seriesCard = SeriesCardModels.fromSeries(series, db, {
        investable: false
      });
      $scope.series = series;
      $scope.company = series.local(db).company();
    });
  });

  $scope.displayGetStartedButton = function() {
    return _withAppAndDb(function(app, db) {
      return !app.local(db).investingUserStartedForms();
    });
  };

  $scope.disableGetStartedButton = function() {
    return _loggingInstructionsAcknowledged;
  };

  $scope.logInstructionsAcknowledged = function() {
    return _withAppAndDb(function(app, db) {
      _loggingInstructionsAcknowledged = true;
      app.local(db).logProgress(ProgressType.INVESTING_USER_STARTED_FORMS).finally(function() {
        _loggingInstructionsAcknowledged = false;
      });
    });
  };
}]);

'use strict';

angular.module('myApp.directives')
.directive('applicationSignDocuments',
function() {
  return {
    restrict: 'A',
    scope: {
      applicationAndDb: '='
    },
    controller: '_ApplicationSignDocumentsController',
    templateUrl: 'components/investment-modules/application-sign-documents.html'
  };
})
.controller('_ApplicationSignDocumentsController',
["$scope", "logging", "Investments", function($scope, logging, Investments) {
  var ProgressType = Investments.ProgressType;
  var _makingRemoteCall = false;

  var _withAppAndDb = function(fn) {
    if ($scope.applicationAndDb) {
      var appId = $scope.applicationAndDb.applicationId;
      var db = $scope.applicationAndDb.db;
      return fn(db.get(appId, {strict: true}), db);
    }
  };

  $scope.showContinueButton = function() {
    return _withAppAndDb(function(app, db) {
      return !app.local(db).investingUserSignedPacket();
    });
  };

  $scope.disableContinueButton = function() {
    return _makingRemoteCall;
  };

  $scope.signDocuments = function() {
    return _withAppAndDb(function(app, db) {
      _makingRemoteCall = true;
      app.local(db).logProgress(ProgressType.INVESTING_USER_SIGNED_PACKET).finally(function() {
        _makingRemoteCall = false;
      });
    });
  };
}]);

'use strict';

angular.module('myApp.directives')
.directive('applicationComplete',
function() {
  return {
    restrict: 'A',
    scope: {
      applicationAndDb: '='
    },
    controller: '_ApplicationCompleteController',
    templateUrl: 'components/investment-modules/application-complete.html'
  };
})
.controller('_ApplicationCompleteController',
["$scope", "Investors", "logging", "MoneyAmounts", function($scope, Investors, logging, MoneyAmounts) {
  var _withAppAndDb = function(fn) {
    if ($scope.applicationAndDb) {
      var appId = $scope.applicationAndDb.applicationId;
      var db = $scope.applicationAndDb.db;
      return fn(db.get(appId, {strict: true}), db);
    }
  };

  $scope.companyName = function() {
    return _withAppAndDb(function(app, db) {
      return app.local(db).company().name();
    });
  };

  $scope.seriesName = function() {
    return _withAppAndDb(function(app, db) {
      return app.local(db).series().name();
    });
  };

  $scope.amountInvested = function() {
    return MoneyAmounts.usd(5e5);
  };
}]);

'use strict';

angular.module('myApp.controllers')
.controller('AddInvestmentEntityDialogController',
["$scope", "$modalInstance", "logging", "$timeout", function($scope, $modalInstance, logging, $timeout) {
  var log = logging.namespace('AddInvestmentEntityDialogController');
  var _cvcAnimateEnabled = false;

  $scope.submit = function() {
    log('Saving changes yo!');
    _cvcAnimateEnabled = false;
    $scope.newInvestorForm.$cvcForm.$submit().then(function() {
      $modalInstance.close();
    }).catch(function() {
      $modalInstance.dismiss('something broke');
    });
  };

  $scope.dismissChanges = function() {
    _cvcAnimateEnabled = false;
    $scope.newInvestorForm.$cvcForm.$dismissChanges().then(function() {
      $modalInstance.dismiss('Canceled');
    });
  };

  $scope.isFormEnabled = function() {
    var isSubmitting = $scope.newInvestorForm.$cvcForm.$submitting;
    return !isSubmitting;
  };

  $scope.isSubmitEnabled = function() {
    return $scope.isFormEnabled() && $scope.newInvestorForm.$cvcForm.$isReadyForSubmit();
  };

  $scope.isCvcAnimateEnabled = function() {
    return _cvcAnimateEnabled;
  };

  $scope.investingEntityNameInPrompts = function() {
    var name = 'the investing entity';
    if ($scope.newInvestor.name) {
      name = $scope.newInvestor.name;
    }

    return name;
  };

  $scope.$watch('newInvestorForm', function(form) {
    if (form) {
      form.$cvcForm.$initialLoad.then($timeout(function() {
        _cvcAnimateEnabled = true;
      }, 100));
    }
  });
}]);


'use strict';

angular.module('myApp.controllers').controller(
'InvestmentOfferingsPageController',
["$scope", function($scope) {
  $scope.theme = 'v2';

}]);

'use strict';

angular.module('myApp.controllers').controller(
'InvestmentApplicationPageController',
["logging", "$scope", "Deals", "$routeParams", "Brokerages", "Investments", "ObjectDBs", "$q", function(logging, $scope, Deals, $routeParams, Brokerages, Investments, ObjectDBs, $q) {
  var ProgressType = Investments.ProgressType;
  var log = logging.namespace('InvestmentApplicationPageController');
  var seriesId = $routeParams.seriesId;

  var _getRemoteData = function() {
    var db = ObjectDBs.temporary();
    var promises = {
      application: Investments.getOrCreateActiveApplication({
        apiParams: {
          series: seriesId
        },
        db: db
      }),
      series: Deals.getSeries(seriesId, {db: db}),
      brokerages: Brokerages.allWithUserAsClient({db: db})
    };

    $q.all(promises).then(function(results) {
      $scope.remoteData = {
        applicationId: results.application.id(),
        db: db
      };
    });
  };

  var _withAppAndDb = function(fn) {
    var appId = $scope.remoteData && $scope.remoteData.applicationId;
    if (appId) {
      var db = $scope.remoteData.db;
      return fn(db.get(appId, {strict: true}), db);
    }
  };

  $scope.theme = 'v2';
  $scope.defaultInvestorId = $routeParams.investorId;

  var _appComplete = function() {
    return _withAppAndDb(function(app, db) {
      return app.local(db).investingUserSignedPacket();
    });
  };

  $scope.showAppCompleteModule = function() {
    return _appComplete();
  };

  $scope.showOfferingSummaryModule = function() {
    return !_appComplete();
  };

  $scope.showInvestorChooserModule = function() {
    return !_appComplete() && _withAppAndDb(function(app, db) {
      return app.local(db).investingUserStartedForms();
    });
  };

  $scope.showInvestmentDetailsModule = function() {
    return !_appComplete() && _withAppAndDb(function(app, db) {
      return app.local(db).investingUserSelectedEntity();
    });
  };

  $scope.showSignDocumentsModule = function() {
    return !_appComplete() && _withAppAndDb(function(app, db) {
      return app.local(db).investingUserFilledInvestmentAmount();
    });
  };

  $scope.$watch('selectedInvestor', function(newInvestor) {
    log('New investor was ');
    log(newInvestor);
  });

  _getRemoteData();
}]);

'use strict';

/**
 * TemporaryObjectDB. An ObjectDB backed by a javascript object.
 *
 * Members:
 *   .put(thing) -- Put an object or many objects into the database.
 *     Params:
 *       thing -- Any of the following:
 *         (1) A js object that has a .id() field that returns a string.
 *         (2) A js array with objects in it that satisfies (1)
 *         (3) A $q promise that resolves to an object that satisfies (1)
 *         (4) An array of $q promises that resolve to an object that satisfies (1)
 *         (5) A $q promise to an array that satisfies (2) or (4)
 *         (6) Any combination of (1) through (5)
 *
 *   .get(id, [config]) -- Return the thing by its ID.
 *     Return undefined if not found. Fails an assert if configured with strict: true.
 *     Params:
 *       config -- Optional.
 *         .strict -- true if you want to assert that the object _must_ be in the database.
 */

/**
 * ObjectDBs. Tools for ferreting away and querying objects that contain IDs.
 *
 * Members:
 *   .temporary() -- Return a new, temporary ObjectDB that is backed by a javascript object.
 */
angular.module('myApp.services').service(
'ObjectDBs',
["Utils", function(Utils) {

  var TemporaryObjectDB = function() {
    this._dataById = {};
  };

  TemporaryObjectDB.prototype = {
    put: function(thing) {
      if (thing.constructor === Array) {
        return this._putMany(thing);
      } else if (thing.then) {
        return this._putPromise(thing);
      } else if (thing._putOtherDB) {
        return this._putOtherDB(thing);
      } else {
        return this._putSingle(thing);
      }
    },

    get: function(id, config) {
      Utils.assert(id, 'TemporaryObject.get requires an ID as its first argument.');
      config = config || { strict: false };
      if (id.constructor === Array) {
        return this._getMany(id, config);
      } else {
        return this._getSingle(id, config);
      }
    },

    _getMany: function(ids, config) {
      var self = this;
      return _.map(ids, function(id) {
        return self.get(id, config);
      });
    },

    _getSingle: function(id, config) {
      var result = this._dataById[id];
      if (config.strict) {
        Utils.assert(result !== undefined, 'The ID "' + id + '" found no result when querying an ObjectDB in strict mode.');
      }
      return result;
    },

    _putOtherDB: function(other) {
      _.extend(this._dataById, other._dataById);

      return this;
    },

    _putSingle: function(thing) {
      Utils.assert(thing.id, 'Can not put anything without an id field into an ObjectDB');
      this._dataById[thing.id()] = thing;
      return this;
    },

    _putPromise: function(promise) {
      var self = this;
      return promise.then(function(thing) {
        return self.put(thing);
      });
    },

    _putMany: function(things) {
      var self = this;
      // an array of things
      return _.reduce(things, function(db, thing) {
        var rval;
        if (db.then) {
          rval = db.then(function(futureDB) {
            return futureDB.put(thing);
          });
        } else {
          rval = db.put(thing);
        }

        return rval;
      }, self);
    }
  };

  var ObjectDBs = {
    temporary: function() {
      return new TemporaryObjectDB();
    }
  };

  return ObjectDBs;
}]);

'use strict';

/**
 * Tour. A visual product tour of the current page.
 *
 * Members:
 *   .id() -- Returns an id if available, otherwise triggers an assert.
 *     (e.g. 'product_tour:firm_page_admin')
 *
 *   .isCompleted() -- Returns true if, according to the API, this tour
 *     has been completed some time in the past.
 *
 *   .setCompleted() -- Sets onto the back-end that this tour has been completed.
 *     returns a promise that is resolved once the back-end returns that the data
 *     was stored successfully.
 *
 *   .start() -- Begin the tour, return a promise that is resolved when the
 *     tour ends.
 */

/**
 * Tours. A factory for Tour instances.
 *
 * Members:
 *   .create(config) -- Create a Tour with the specified config.
 *     Args:
 *       config.steps -- an array of steps (attained via Tours.step(id))
 *
 *   .step(id) -- Return the Step corresponding to the provided ID
 *     Args:
 *       id -- the ID of the step. (e.g. 'firm.Conclusion'). You can find these values
 *         in this file.
 *
 *   .steps(config) -- Return an array of matching steps
 *     Args:
 *       config.match -- a Regex that matches a step ID
 *
 *   .firmPageTour(data) -- Create the tour for the Firm page.
 *     Args:
 *       config.firstName -- the current user's first name.
 */
angular.module('myApp.services').service(
'Tours',
["Users", "Utils", "$q", function(Users, Utils, $q) {
  var defaultTourOptions = {
    skipLabel: 'Done',
    tooltipClass: 'modal-cvc tour-cvc',
    overlayOpacity: 0.3,
    nextLabel: 'Next',
    prevLabel: 'Back',
    showStepNumbers: false,
    scrollToElement: true
  };

  var Tour = function(options) {
    Utils.assert(options.steps, 'Options needs to have an array of steps');
    this._options = options;
  };

  Tour.prototype = {
    isCompleted: function() {
      var completedTours = Users.authorized().completedTours();

      return _.contains(completedTours, this.id());
    },

    setCompleted: function() {
      var completedTours = Users.authorized().completedTours();
      completedTours.push(this.id());

      return Users.authorized().putSetting(Users.Settings.TOURS_COMPLETED, completedTours);
    },

    start: function() {
      var self = this;
      return $q(function(resolve, reject) {
        var intro = introJs();
        var steps = self._options.steps;
        var options = _.extend(angular.copy(defaultTourOptions), self._options);
        if (steps.length === 1) {
          options.showBullets = false;
          options.tooltipClass = options.tooltipClass + ' single-step-tour';
        } else {
          options.tooltipClass = options.tooltipClass + ' multi-step-tour';
        }
        intro.oncomplete(resolve);
        intro.onexit(resolve);

        intro.setOptions(options);
        intro.start();
      });
    },

    id: function() {
      Utils.assert(this._options.id, 'Tour data needs an "id" field (provide one in the constructor)');
      return 'product_tour:' + this._options.id;
    },
  };


  var Steps = {
    'pep.OnboardGreeting': function(data) {
      Utils.assert(data.firstName, 'This tour step requires a firstName be provided as data');
      return {
        intro: '<h3>Hi, ' + data.firstName() + '</h3>' +
               '<section class="tour-step-content">' +
               '  <p>Welcome to the citizen.vc pre-release Private Equity Platform</p>' +
               '  <p>This tour will lead you through the features of this early release.</p>' +
               '</section>'
      };
    },

    'firm.PageSummary': function(data) {
      return {
        element: '#chrome-firm',
        position: 'bottom-right-aligned',
        intro: '<section class="tour-step-content">' +
               '  <p class="first-step-content">You are on the Firm page, your hub for managing the people (admins, brokers and clients) connected to your account, as well as your brand settings (firm name and logo)</p>' +
               '</section>'
      };
    },

    'pep.DealPageSummary': function() {
      return {
        element: '#chrome-deals',
        position: 'bottom-right-aligned',
        intro: '<section class="tour-step-content">' +
               '  <p class="first-step-content">The Deals page contains information about current and past deals. Here you’ll review and approve investment applications, and see new offerings from citizen.vc.</p>' +
               '  <p>This page will be available in a future release!</p>' +
               '</section>'
      };
    },

    'firm.ClientList': function() {
      return {
        element: '#brokerage-clients-module',
        position: 'bottom',
        intro: '<section class="tour-step-content">' +
               '  <p class="first-step-content">This is your client list.</p>' +
               '  <p>Once you’ve added your brokers, click <i>Add Client</i> and enter your client info on the pop-up form. When you submit that form, an email will be sent to that client, inviting them to the platform on behalf of their broker.</p>' +
               '</section>'
      };
    },

    'firm.BrokersList': function() {
      return {
        element: '#brokers-list-module',
        position: 'top',
        intro: '<section class="tour-step-content">' +
               '  <p class="first-step-content">This is your broker list.</p>' +
               '  <p>Click <i>Add Brokers</i> to add all your brokers.</p>' +
               '  <p>(The brokers will not receive an email, but the address you entered will be used in emails to their clients)</p>' +
               '</section>'
      };
    },

    'firm.SettingsAndPreferences': function() {
      return {
        element: '#brokerage-information-module',
        position: 'top',
        intro: '<section class="tour-step-content">' +
               '  <p class="first-step-content">The <strong>Settings & Preferences</strong> module is where you manage your brand settings</p>' +
               '</section>'
      };
    },

    'firm.SettingsAndPreferences.HowToEdit': function() {
      return {
        element: '#brokerage-info-edit-button',
        position: 'top',
        intro: '<section class="tour-step-content">' +
               '  <p class="first-step-content">Just click the pencil to edit!</p>' +
               '</section>'
      };
    },

    'firm.SettingsAndPreferences.AdminsList': function() {
      return {
        element: '#brokerage-administrators-module',
        position: 'top',
        intro: '<section class="tour-step-content">' +
               '  <p class="first-step-content">These are your site administrators.</p>' +
               '  <p>You can invite additional admins by clicking the Add Admins button, or remove admins by selecting the <button class="btn-icon-control-small"><span class="glyphicon glyphicon-remove"></button> in the top right corner of their Admin card.</p>' +
               '</section>'
      };
    },

    'firm.Conclusion': function() {
      return {
        element: '#brokerage-settings-help-icon',
        position: 'bottom-right-aligned',
        intro: '<section class="tour-step-content">' +
               '  <p class="first-step-content">That’s it for now - we’ll be in touch as we release new features.</p>' +
               '  <p>If you have any questions or feedback, please don’t hesitate to contact us at help@citizen.vc.</p>' +
               '  <p>(If you’d like to take this tour again at any time, just click the little <span class="glyphicon glyphicon-question-sign" style="margin-left:3px; margin-right: 3px;"></span> icon in the upper right.)</p>' +
               '</section>'
      };
    }

  };

  var Tours = {
    create: function(config) {
      return new Tour(config);
    },

    firmPageTour: function(data) {
      Utils.assert(data.firstName, 'This tour step requires a firstName be provided as data');

      return Tours.create({
        id: 'firm_page_admin',
        steps: [
          Tours.step('pep.OnboardGreeting', {
            firstName: data.firstName
          }),
          Tours.step('firm.PageSummary'),
          Tours.step('pep.DealPageSummary'),
          Tours.step('firm.ClientList'),
          Tours.step('firm.BrokersList'),
          Tours.step('firm.SettingsAndPreferences'),
          Tours.step('firm.SettingsAndPreferences.HowToEdit'),
          Tours.step('firm.SettingsAndPreferences.AdminsList'),
          Tours.step('firm.Conclusion')
        ]
      });
    },

    step: function(id, data) {
      var step = Steps[id];
      Utils.assert(step, 'Invalid step ID');

      return step(data);
    },

    steps: function(config) {
      Utils.assert(config.match, 'Need a regex to match against.');

      return _.chain(Steps)
        .keys()
        .filter(function(key) {
          return config.match.test(key);
        })
        .map(function(key) {
          return Tours.step(key);
        })
        .value();
    }
  };

  return Tours;
}]);

'use strict';

/* Directive for displaying a progress bar, mostly used for displaying completeness of an investment */

angular.module('myApp.directives').
directive('bargraphSimple', ["logging", "$q", function(logging, $q) {
  var log = logging.namespace('bargraph-simple');
  return {
    restrict:'A',

    templateUrl: 'components/bargraph-simple/bargraph-simple.html',

    scope: {
      'percentage' : '='
    },

    link: function(scope, element) {
      scope.$watch('percentage', function(newVal, oldVal, scope){
        if (newVal) {
          scope.adjustWidth(newVal);
        }
      });

      scope.adjustWidth = function(width) {
        if (width > 100) {
          width = 100;
        }

        element.find('.bargraph-simple .completeness').css('width', width + '%');
      };
    }
  };
}]);



'use strict';

/**
 * Investor. An investing entity (natural or incorporated).
 *
 * Members:
 *   .id() -- Return the investing entity's ID.
 *   .name() -- Return the investing entity's name.
 */

/**
 * Investors. A factory for Investor instances.
 *
 * Members:
 *   .create(apiInvestor) -- Creates a new Investor from its API representation.
 *   .forCurrentUser() -- Returns the Investor objects retained by the current user.
 */
angular.module('myApp.services').service(
'Investors',
["$injector", "Enums", function($injector, Enums) {
  var Users = function() {
    return $injector.get('Users');
  };

  var Investor = function(apiInvestor) {
    this._apiInvestor = apiInvestor;
  };

  Investor.prototype = {
    id: function() {
      return this._apiInvestor.id;
    },

    name: function() {
      return this._apiInvestor.name;
    },

    entityType: function() {
      return InvestorType.forValue(this._apiInvestor.type);
    },

    isTaxedInUS: function() {
      return this._apiInvestor.w9_is_us_person;
    }
  };

  var InvestorType = Enums.create({
    PERSON: {value: 'person', humanReadable: 'person'},
    TRUST: {value: 'trust', humanReadable: 'trust'},
    CORPORATION: {value: 'corporation', humanReadable: 'corporation'},
    OTHER: {value: 'other', humanReadable: 'entity'}
  });

  var Investors = {
    create: function(apiInvestor) {
      return new Investor(apiInvestor);
    },

    forCurrentUser: function() {
      return Users().authorized().investors();
    }
  };

  return Investors;
}]);

'use strict';

/**
 * SeriesCardModels. A tool for creating valid objects for rendering
 * a series card (see series-card.js)
 *
 * Members:
 *   .fromSeries(series, db, config) -- Return a valid model for the series
 *     from a Deals.Series object, and associated ObjectDB.
 *     Params:
 *       series -- the Series to render
 *       db -- an ObjectDB containing objects related to the series (at least access, offering, brokerage)
 *       config -- an object with the following fields:
 *         investable -- Boolean. Renders an Invest button if present.
 */
angular.module('myApp.services').service(
'SeriesCardModels',
["URLS", "Deals", function(URLS, Deals) {
  var SeriesStatus = Deals.SeriesStatus;
  var SeriesCardModels = {
    fromSeries: function(series, db, config) {
      config = config || { investable: true };
      var seriesRelations = series.local(db);
      var offering = seriesRelations.offering();
      var brokerage = seriesRelations.brokerage();
      var company = seriesRelations.company();
      var security = seriesRelations.security();

      var card = {
        status: function() {
          return series.status();
        },

        showStatusBand: function() {
          return series.status() !== SeriesStatus.OPEN;
        },

        showControls: function() {
          return series.status() === SeriesStatus.OPEN && this.investUrl;
        },

        pricePerShare: function() {
          return offering.pricePerShare();
        },

        valuation: function() {
          return offering.preMoneyValuation();
        },

        offeringFirm: function() {
          return brokerage.name();
        },

        seriesName: function() {
          return series.name();
        },

        description: function() {
          return company.description();
        },

        companyName: function() {
          return company.name();
        },

        logoFile: function() {
          return company.logo();
        },

        firmLogoFile: function() {
          return brokerage.logo();
        },

        goalAmount: function() {
          return offering.goal();
        },

        security: function() {
          return security.name();
        }
      };

      if (config.investable) {
        card.investUrl = function() {
//          return URLS.FUNDRAISE(company.slug(), series.id());
          return URLS.INVESTMENT_APPLICATION(series.id());
        };
      }

      return card;
    }
  };

  return SeriesCardModels;
}]);

'use strict';

/**
  Confirmation Modal Directive
  
  Attributes:
    config (object)
      {
        title: 'A title for the dialog',
        message: 'A Message for the dialog',
        action: function () {
          return preferrablyThisWillReturnAPromise();
        },
        buttonContent: 'What Gets Displayed In the Button'
      }


 */

angular.module('myApp.directives')
.directive('confirmModal',
function(logging, $q, $modal, URLS, $http, api2, APIObjects, ObjectDBs) {
  var log = logging.namespace('confirmModal');
  return {
    restrict: 'E',
    
    scope: {
      config: '=?'
    },
    
    templateUrl: 'components/confirm-modal/confirm-modal.html',

    link: function(scope, element, attrs) {
                
      var DialogViewModel = function (config) {
        this._config = config;
      };

      DialogViewModel.prototype = {
        title: function () {
          return this._config.title;
        },

        message: function () {
          return this._config.message;
        },

        action: function () {
          return this._config.action();
        }
      };

      _.defaults(scope.config, {
          title: 'Dialog Title',
          message: 'Dialog Message',
          buttonContent: 'X',
          action: function () { return true;}
      });

      scope.openConfirmDialog = function () {
        var dialogViewModel = new DialogViewModel(scope.config);
        
        var opts = {
          backdrop: true,
          backdropClick: true,
          dialogFade: false,
          keyboard: true,
          scope: scope,
          controller: '_ConfirmModalDialogController',
          templateUrl : 'components/confirm-modal/confirm-modal-content.html',
          resolve: {
            'dialogViewModel': function () {
              return dialogViewModel;
            }
          }
        };

        var confirmModal = $modal.open(opts);
        confirmModal.result.then(function() {});

      }

    }
  }
})
.controller('_ConfirmModalDialogController',
function($scope, logging, $modalInstance, dialogViewModel) {
  
  var log = logging.namespace('_ConfirmModalDialogController');

  $scope.dialogViewModel = dialogViewModel;
  
  $scope.dismissChanges = function () {
    $modalInstance.dismiss('Canceled');
  };

  $scope.performAction = function () {
    $scope.dialogViewModel.action().then(function(data) {
      $modalInstance.close(data);
    });
  }

});

'use strict';

describe('Docs', function() {
  var Docs;
  var sampleDocJSON;
  
  beforeEach(function() {
    module('cvcTestHarness');

    sampleDocJSON = {
      file: 10,
      id: "doc:5",
      name: "This is a name for the file",
      type: "other"
    };

  });

  beforeEach(inject(function(_Docs_) {
    Docs = _Docs_;
  }));

  it('should have a doc id', function() {
    var doc = Docs.instance(sampleDocJSON);
    expect(doc.id()).toEqual(sampleDocJSON.id);
  });

  it('should have a name', function() {
    var doc = Docs.instance(sampleDocJSON);
    expect(doc.name()).toEqual(sampleDocJSON.name);
  });

  it('should have a file id', function() {
    var doc = Docs.instance(sampleDocJSON);
    expect(doc.file()).toEqual(sampleDocJSON.file);
  });


});

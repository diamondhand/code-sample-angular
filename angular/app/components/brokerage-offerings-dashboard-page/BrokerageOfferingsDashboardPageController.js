'use strict';

/**
 * Controller for the brokerage offerings dashboard page (a page for admins and brokerage-admins only)
 */
angular.module('myApp.controllers').controller(
'BrokerageOfferingsDashboardPageController',
function($scope, $routeParams, logging, Brokerages, Deals, ObjectDBs) {
  var log = logging.namespace('BrokerageOfferingsDashboardPageController');
  var db = ObjectDBs.temporary();
  $scope.theme = 'v2';
  $scope.loading = true;
  $scope.selectedOfferingId = $routeParams.offeringId || null;
  $scope.offeringsDb = db;

  Brokerages.getByUniqueKey($routeParams.firmSlug, {db: db}).then(function(brokerage) {
    $scope.brokerage = brokerage;

    Deals.offeringsFromBrokerage(brokerage.id(), {db: db}).then(function(offerings){
      var offering;

      if ($scope.selectedOfferingId) {
        offering = _.find(offerings, function(offering){
          return offering.id() === $scope.selectedOfferingId;
        });

        offering.local($scope.offeringsDb).allDocs().then(function(){
          $scope.offerings = offerings;
        });
        
      } else {
        $scope.offerings = offerings;
      }

      /*
        MAKE SURE SELECTED OFFERING IS IN OFFERINGS, else go to error page
      */
    });
  });
});

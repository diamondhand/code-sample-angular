'use strict';

/* Directive for displaying a progress bar, mostly used for displaying completeness of an investment */

angular.module('myApp.directives').
directive('bargraphSimple', function(logging, $q) {
  var log = logging.namespace('bargraph-simple');
  return {
    restrict:'A',

    templateUrl: 'components/bargraph-simple/bargraph-simple.html',

    scope: {
      'percentage' : '='
    },

    link: function(scope, element) {
      scope.$watch('percentage', function(newVal, oldVal, scope){
        if (newVal) {
          scope.adjustWidth(newVal);
        }
      });

      scope.adjustWidth = function(width) {
        if (width > 100) {
          width = 100;
        }

        element.find('.bargraph-simple .completeness').css('width', width + '%');
      };
    }
  };
});



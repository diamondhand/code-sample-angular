'use strict';

/**
 * Doc. A single document.
 *
 * Members:
 *   .id() -- The document's ID (e.g. 'document:1')
 *
 *   .name() -- The document's name (e.g. 'Jumio Series I Subscription Agreement')
 *
 *   .type() -- A valid DocType.
 *
 *   .status() -- This Doc's DocStatus.
 *
 *   .externalRef() -- ID/URL reference to external doc. Mostly used for Docusign documents.
 */

/**
 * DocStatus. An Enum instance representing the status of a Doc.
 *   (See Enums.js)
 *
 * Members:
 *   .PUBLISHED -- The document is visible to people other than the author (and authorized others)
 *   .NOT_PUBLISHED -- The document is only visible to the author (and other authorized admins)
 */

/**
 * DocType. An Enum instance representing the type of a Doc.
 *   (See Enums.js)
 *
 * Members:
 *   .SUBSCRIPTION_AGREEMENT -- A subscription agreement for an associated series.
 *   .WIRING_INSTRUCITONS -- The wiring instructions for an associated series.
 *   .OTHER -- Some other type of document
 */

/**
 * Docs. A factory for creating Doc instances.
 *
 * Members:
 *   .getByIds(ids) -- Return a promise to the Docs with the provided IDs.
 *     IDs should be an array of IDs (e.g. ['document:1', 'document:2']).
 *     If an array of ids is not given, it returns an empty promise. Boo-hoo.
 *
 *   .instance(apiDoc) - Return a new instance of Doc given the JSON config
 *
 *   .Status -- see above for DocStatus constant
 *
 *   .Type -- see above DocType constant
 */
angular.module('myApp.services').service(
'Docs',
function($http, api2, APIObjects, ObjectDBs, Enums, $q) {
  var Doc = function(apiDoc) {
    this._apiDoc = apiDoc;
  };

  Doc.prototype = {
    id: function() {
      return this._apiDoc.id;
    },

    name: function() {
      return this._apiDoc.name;
    },

    file: function () {
      return this._apiDoc.file;
    },

    type: function() {
      return DocType.forValue(this._apiDoc.type);
    },

    status: function() {
      return DocStatus.forValue(this._apiDoc.status);
    },

    externalRef: function() {
      return this._apiDoc.external_ref;
    }
  };

  var DocStatus =  Enums.create({
    PUBLISHED: {value: 'published', humanReadable: 'published'},
    NOT_PUBLISHED: {value: 'not_published', humanReadable: 'not published'}
  });

  var DocType =  Enums.create({
    SUBSCRIPTION_AGREEMENT: {value: 'subscription_agreement', humanReadable: 'subscription agreement'},
    WIRING_INSTRUCTIONS: {value: 'wiring_instructions', humanReadable: 'wiring instructions'},
    OTHER: {value: 'other', humanReadable: 'other'}
  });

  var Docs = {
    getByIds: function(config) {
      config = config || {};
      var apiParams = {ids: config.ids};

      var docsRequest;

      if (config.ids.length > 0) {
        docsRequest = $http.get(api2.docs(), {
          params: apiParams
        });

        return docsRequest.then(
          function docsGetSuccess(httpResponse) {
            var apiObjects = APIObjects.createOrUpdateDBWithResponse(httpResponse, config.db);
            
            return apiObjects.records;
        });      
      } else {
        return $q.when([]);
      }
    },

    instance: function(apiDoc) {
      return new Doc(apiDoc);
    },

    Status: DocStatus,

    Type: DocType
    
  };

  return Docs;
});

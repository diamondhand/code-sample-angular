'use strict';

/*
 * Provides accessors and URLs for the citizen.vc v2 API
 */
angular.module('myApp.services')
.service('api2',
['ENV',
function(ENV) {
  var api2 = {
    host: function() { return ENV.apiEndpoint; },

    base: function() { return api2.host() + '/api/2'; },

    forms: function() {
      return api2.base() + '/forms';
    },

    brokerages: function() {
      return api2.base() + '/brokerages';
    },

    users: function() {
      return api2.base() + '/users';
    },

    files: function() {
      return api2.base() + '/files';
    },

    offerings: function() {
      return api2.base() + '/offerings';
    },

    series: function() {
      return api2.base() + '/series';
    },

    seriesParticipations: function() {
      return api2.base() + '/series_participations';
    },

    investmentAppProgress: function() {
      return api2.base() + '/investment_application_progress';
    },

    docs: function () {
      return api2.base() + '/docs';
    },

    investmentPacketDocs: function () {
      return api2.base() + '/investment_packet_docs';
    }
  };

  return api2;
}]);

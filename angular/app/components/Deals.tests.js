'use strict';

describe('Deals', function() {
  var Deals;
  var TestUtils;
  var sampleSeriesJson;
  var sampleAccessJson;
  var sampleOfferingJson;
  var sampleInvestmentPacketDocJson;
  
  beforeEach(function() {
    module('cvcTestHarness');

    sampleSeriesJson = {
      'id': 'series:10',
      'access': 'access:10',
      'name': 'Jumio Series A1',
      'status': 'open',
      'status_message': 'Herp derp derp',
      'fee_pct': 3.00,
      'carry_pct': 3.00,
      'commission_pct': 3.00,
      'placement_fee_pct': 1.00,
      'all_clients': true,
      'recommended_min_investment': {'amount': 100.00, 'currency': 'USD'},
      'investment_documents': [
        {
          'id': 'investment_document:10',
          'series': 'series:10',
          'sequence_number': '0.2',
          'document': 'document:1',
          'investor_must_sign': true,
          'firm_must_sign': true
        },
        {
          'id': 'investment_document:11',
          'series': 'series:10',
          'sequence_number': '0.1',
          'document': 'document:10',
          'investor_must_sign': true,
          'firm_must_sign': true
        }        
      ]
    };

    sampleInvestmentPacketDocJson = {
      'id': 'investment_document:10',
      'series': 'series:10',
      'sequence_number': '0.2',
      'document': 'document:1',
      'investor_must_sign': true,
      'firm_must_sign': true
    };

    sampleAccessJson = {
      'id': 'access:10',
      'offering': 'offering:10',
      'source': 'series:10',
      'brokerage': 'brokerage:10',
      'status': 'OPEN',
      'series': [sampleSeriesJson]
    };

    sampleOfferingJson = { // an array of all available offerings
      'id': 'offering:10',
      'legacy_fundraise': 'fundraise:10',
      'security': {
        'id': 'security:10',
        'company': {
          'id': 'company:10',
          'name': 'Jumio',
          'legacy_company': 'legacy_company:10'
        },
      },
      'price_per_share': {'amount': 100.00, currency: 'USD'},
      'pre_money_valuation': {'amount': 100.00, currency: 'USD'},
      'goal_amt': {'amount': 100.00, currency: 'USD'},
      'accesses': [sampleAccessJson]
    };
  });

  beforeEach(inject(function(_Deals_, _TestUtils_) {
    Deals = _Deals_;
    TestUtils = _TestUtils_;
  }));

  describe('Offering', function() {

  });

  describe('Series', function() {
    it('should have a name and id', function() {
      var series = Deals.series(sampleSeriesJson);
      expect(series.id()).toEqual(sampleSeriesJson.id);
      expect(series.name()).toEqual(sampleSeriesJson.name);
    });

    it('should have statuses', function() {
      var series = Deals.series(sampleSeriesJson);
      var expectations = [
        ['open', Deals.SeriesStatus.OPEN],
        ['closed', Deals.SeriesStatus.CLOSED],
        ['not_yet_opened', Deals.SeriesStatus.NOT_YET_OPENED],
        ['on_hold', Deals.SeriesStatus.ON_HOLD]
      ];

      _.each(expectations, function(expectation) {
        var apiValue = expectation[0];
        var enumInstance = expectation[1];

        sampleSeriesJson.status = apiValue;
        expect(series.status()).toBe(enumInstance);
      });
    });

  });

  describe('InvestmentPacketDoc', function() {
    it('should have a doc id and packet doc id', function() {
      var packetDoc = Deals.investmentPacketDoc(sampleInvestmentPacketDocJson);
      expect(packetDoc.id()).toEqual(sampleInvestmentPacketDocJson.id);
      expect(packetDoc.docId()).toEqual(sampleInvestmentPacketDocJson.doc);
    });

    it('should have an investor must sign status', function() {
      var packetDoc = Deals.investmentPacketDoc(sampleInvestmentPacketDocJson);
      expect(packetDoc.investorMustSign()).toEqual(sampleInvestmentPacketDocJson.investor_must_sign);
    });
  });
});

'use strict';

/**
 * Company. A company whose securities are being offered on the citizen.vc platform.
 *
 * Members:
 *   .id() -- The company's id as a string (e.g. 'company:1')
 *
 *   .name() -- The company's name as a string (e.g. 'Jumio')
 *
 *   .logo() -- The company's logo as a file ID. (e.g. 'file:10')
 */

/**
 * Security. A security that is traded on the citizen.vc platform.
 *
 * Members:
 *   .id() -- The security's ID (e.g. 'security:1')
 *
 *   .name() -- The security's name (e.g. 'Common Stock')
 */

/**
 * Offering. A particular quantity of a Company's Security that is being trade on-platform.
 *
 * Members:
 *   .id() -- The offering's ID (e.g. 'offering:1')
 *
 *   .pricePerShare() -- A MoneyAmount specifying the offering's price-per-share.
 *
 *   .goal() -- A MoneyAmount specifying the offering's fundraise goal.
 *
 *   .preMoneyValuation() -- A MoneyAmount specifying the company's valuation
 *     for the duration of this offering.
 *
 *   .local(db) -- Return an object for synchronously accessing the objects
 *     related to this Offering (e.g. accesses, series) from an ObjectDB.
 *     Params:
 *       db -- an ObjectDB that has already indexed all objects related to this Offering.
 *
 *   .accesses() -- A promise to all the Accesses associated with this Offering.
 *
 *   .accessIds() -- Return an array of IDs to the accesses associated with this Offering.
 *
 *   .allSeries() -- A promise to all the Series hanging off of the accesses available to this Offering.
 *
 *   .allSeriesInReverseIdOrder() -- Returns all series in reverse Series ID order.
 *
 *   .local(db) -- Return an OfferingRelationsDB for synchronously accessing the objects
 *     related to this Offering (e.g. accesses, series).
 *     Params:
 *       db -- an ObjectDB that has already indexed all objects related to this Offering.
 *
 *   .local(db).allSeries() -- The array of all Series hanging off of accesses to this Offering. Returns in reversed order sorted by series Id.
 *
 *   .local(db).accesses() -- The array of all Accesses associated with this Offering.
 *
 *   .local(db).investmentPacketDocsIds() -- Array of Investment Packet Doc IDs in the Offering
 *   .local(db).allInvestmentPacketDocs() -- Array of Investment Packet Doc objects in the Offering
 *   .local(db).allDocs() -- A promise to Doc objects related to Investment Packet Docs
 
 */

/**
 * Access. A brokerage's access to an offering.
 *
 * Members:
 *   .id() -- The access's id (e.g. 'access:1')
 *
 *   .offeringId() -- ID of the Offering this Access gives access to (e.g. 'offering:1')
 *
 *   .brokerageId() -- ID of the Brokerage that is granted access to this Offering (e.g. 'brokerage:1')
 *
 *   .seriesIds() -- An array of IDs for the investment Series that have been opened against this access.
 *
 *   .status() -- an AccessStatus.
 *
 *   .allSeries() -- A promise to all the Series that have been opened against this access.
 *
 *   .local(db) -- Return an object for synchronously accessing the objects related to this
 *      Access.
 *      Params:
 *        db -- an ObjectDB that has already indexed all objects related to this Access.
 *
 *   .local(db).allSeries() -- The array of all investment Series created against this Access.
 *
 *   .local(db).brokerage() -- The Brokerage to which this Access has granted the Offering.
 *
 *   .local(db).offering() -- The Offering that has been granted.
 */

/**
 * AccessStatus. An Enum instance representing the status of an Access object.
 *   (See Enums.js)
 * Members:
 *   OPEN
 *   CLOSED
 */

/**
 * Series. The investable unit of a deal.
 *
 * Members:
 *
 *   .id() -- The series' ID (e.g. 'series:1')
 *
 *   .name() -- The series' name, as specified by the owning brokerage (e.g. 'Index Series I-1')
 *
 *   .status() -- A SeriesStatus showing the current series' status.
 *
 *   .accessId() -- ID of the Access against which this Series was created.
 *
 *   .feePercentage() - The fee percentage which the Brokerage charges for this Series.
 *
 *   .carryPercentage() - The carry percentage which the Brokerage charges for this Series.
 *
 *   .commissionPercentage() - The commission percentage which the Brokerage charges for this Series
 *
 *   .placementFeePercentage() - The placement fee percentage which the Brokerage charges for this Series
 *
 *   .recommendedMinInvestment() - The recommended minimum investment amount which the Brokerage sets. Returns as MoneyAmount
 *
 *   .investmentPacketDocsIds() - Array of Investment Packet Doc IDs associated to a Series.
 *
 *   .isClosed() - Is the series status set to CLOSED
 *
 *   .isNotYetOpened() - Is the series status set to NOT YET OPENED
 *
 *   .isOnHold() - Is the series status set to ON HOLD
 *
 *   .isOpen() - Is the series status set to OPEN
 *
 *   .local(db) -- Return an object for synchronously accessing the objects related
 *     to this Series.
 *     Params:
 *       db -- an ObjectDB that has already indexed all objects relating to this Series.
 *
 *   .local(db).access() -- The Access against which this Series was created.
 *
 *   .local(db).offering() -- The Offering that this Series gives investors access to
 *
 *   .local(db).brokerage() -- The Brokerage that owns this Series.
 *
 *   .local(db).allInvestmentPacketDocs() -- Array of instances of Investment Packet Doc objects
 *
 *   .local(db).allDocIds() -- Array of Doc Ids of all the Investment Packet Docs associated to the Series
 *
 *   .local(db).allDocs() -- Array of Doc objects of all the Investment Packet Docs associated to the Series
 */

/**
 * SeriesStatus. An Enum instance representing the various states that a Series can occupy.
 * Mostly for use in comparing the result of Series.status()
 *
 * Members:
 *   NOT_YET_OPENED,
 *   OPEN,
 *   ON_HOLD,
 *   CLOSED
 */

 /**
 * InvestmentPacketDoc. A document associated to the Series.
 *
 * Members:
 *
 *  .nonUSIndividualMustSign() -- Boolean Non US Individual Must Sign the doc
 *
 *  .nonUSOrganizationMustSign() -- Boolean Non US Organization must sign the doc
 *
 *  .sequenceNumber() -- Sequence Number for displaying doc
 *
 *  .series() -- Series associated to the doc
 *
 *  .USIndividualMustSign() -- Boolean US Individual must sign the doc
 *
 *  .USOrganizationMustSign() -- Boolean US Organization must sign the doc
 *
 *  .docId() -- Doc namespaced ID 
 *
 *  .local(db) -- Return an object for synchronously accessing the objects related
 *     to this Investment Packet Doc.
 *     Params:
 *       db -- an ObjectDB that has already indexed all objects relating to this Investment Packet Doc.
 *
 *  .local(db).doc() -- Instance of Doc object related to the Investment Packet Doc
 *
 *
 */

/**
 * Deals. A factory for many types relating to Deals.
 *
 * Members:
 *   .offeringsForInvestment(config) -- A promise to the array of Offerings available
 *     to the user for investment.
 *     Args:
 *       config -- Optional object.
 *         .db -- Optional ObjectDB. If provided, the offerings and all included objects (e.g. series, accesses)
 *           will be populated into the DB before the promise resolves.
 *
 *   .offeringsFromBrokerage(brokerageId, config) -- A promise to all of the Offerings offered by a Brokerage.
 *     This should only be callable a brokerage admin.
 *     Args:
 *       brokerageId -- ID of the brokerage whose Offerings should be returned (e.g. 'offering:1')
 *       config -- Optional object.
 *         .db -- Optional ObjectDB. If provided, the offering and all included objects will be put into the DB
 *           before the promise returns.
 *
 *   .getOfferings(config) -- A promise to the specified collection of Offerings.
 *     Args:
 *       config -- Required object.
 *         .apiParams -- Required object with query parameters (GET) or body parameters (POST) for consumption
 *            by the API
 *
 *          .db -- Optional ObjectDB. If provided the offering and all included objects will be put into the DB
 *            before the promise returns.
 *
 *   .getSeries(seriesId, config) -- A promise to a Series with the specified id.
 *     Args:
 *       seriesId -- ID of the target series (e.g. 'series:1')
 *       config -- Optional object.
 *         .db -- Optional ObjectDB. If provided, the series and all included objects will be put into the DB
 *           before the promise returns.
 *
 *   .getAllSeriesFromOfferings(offerings, db) -- Given an array of Offerings,
 *     return an array of all the offerings' series.
 *     Args:
 *       offerings -- array of Offerings.
 *       db -- an ObjectDB populated with the Offerings' relations.
 *
 *   .SeriesStatus -- the SeriesStatus Enum type (see above for docs)
 *
 *   .AccessStatus -- the AccessStatus Enum type (see above for docs)
 *
 *   .offering(json),
 *   .security(json),
 *   .company(json),
 *   .access(json),
 *   .series(json),
 *   .investmentPacketDoc(json) --
 *      create a domain object from its API representation. This is usually used in the APIObjects service to populate an
 *      ObjectDB.
 *
 */


angular.module('myApp.services').service(
'Deals',
function(MoneyAmounts, Utils, $q, $http, api2, APIObjects, ObjectDBs, Enums, Docs) {
  //
  // Company
  //
  var Company = function(apiCompany) {
    this._apiCompany = apiCompany;
  };

  Company.prototype = {
    id: function() {
      return this._apiCompany.id;
    },

    name: function() {
      return this._apiCompany.name;
    },

    logo: function() {
      return this._apiCompany.logo;
    },

    description: function() {
      return this._apiCompany.description;
    },

    slug: function() {
      return this._apiCompany.slug;
    }
  };

  //
  // Security
  //
  var Security = function(apiSecurity) {
    this._apiSecurity = apiSecurity;
  };

  Security.prototype = {
    id: function() {
      return this._apiSecurity.id;
    },

    name: function() {
      return this._apiSecurity.name;
    },

    companyId: function() {
      return this._apiSecurity.company;
    },

    local: function(db) {
      return new SecurityRelationsDB(this, db);
    }
  };

  var SecurityRelationsDB = function(security, db) {
    this._db = db;
    this._security = security;
  };

  SecurityRelationsDB.prototype = {
    company: function() {
      return this._db.get(this._security.companyId(), {strict: true});
    }
  };

  //
  // Offering
  //
  var Offering = function(apiOffering) {
    this._apiOffering = apiOffering;
  };

  Offering.prototype = {
    id: function() {
      return this._apiOffering.id;
    },

    pricePerShare: function() {
      return MoneyAmounts.create(this._apiOffering.price_per_share);
    },

    goal: function() {
      return MoneyAmounts.create(this._apiOffering.goal_amt);
    },

    preMoneyValuation: function() {
      return MoneyAmounts.create(this._apiOffering.pre_money_valuation);
    },

    local: function(db) {
      return new OfferingRelationsDB(this, db);
    },

    accessIds: function() {
      return this._apiOffering.accesses;
    },

    securityId: function() {
      return this._apiOffering.security;
    }
  };

  var OfferingRelationsDB = function(offering, db) {
    this._offering = offering;
    this._db = db;
  };

  OfferingRelationsDB.prototype = {
    allSeries: function () {
      var self = this;
      var seriesSets = _.map(this.accesses(), function(access) {
        return access.local(self._db).allSeries();
      });

      return _.flatten(seriesSets);
    },

    allSeriesInReverseIdOrder: function () {
      var seriesSets = this.allSeries();

      seriesSets = _.chain(seriesSets)
        .sortBy(function(series) {
          var id = series.id();
          id = parseInt(id.slice('series:'.length));
          return id;
        })
        .reverse()
        .value();

      return seriesSets;
    },

    accesses: function () {
      return this._db.get(this._offering.accessIds(), {strict: true});
    },

    security: function () {
      return this._db.get(this._offering.securityId(), {strict: true});
    },

    investmentPacketDocsIds: function () {
      var self = this;

      var allSeries = this.allSeries();

      var packetDocsIds = _.chain(allSeries)
        .filter(function (series){
          if(series.investmentPacketDocsIds().length > 0) {
            return series;
          }
        })
        .map(function (series){ return series.investmentPacketDocsIds();})
        .flatten()
        .value();

      return packetDocsIds;
    },

    allInvestmentPacketDocs: function () {
      var packetDocsIds = this.investmentPacketDocsIds();
      return this._db.get(packetDocsIds);
    },

    allDocs: function () {
      var self = this;
      var packetDocs = this.allInvestmentPacketDocs();

      var ids = _.map(packetDocs, function(packetDoc){
        return packetDoc.docId();
      });

      return Docs.getByIds({ids:ids, db: this._db}).then(function(docs){
        return docs;
      });
    }

  };

  //
  // Access
  //
  var Access = function(apiAccess) {
    this._apiAccess = apiAccess;
  };

  Access.prototype = {
    id: function() {
      return this._apiAccess.id;
    },

    offeringId: function() {
      return this._apiAccess.offering;f.value
    },

    brokerageId: function() {
      return this._apiAccess.brokerage;
    },

    status: function() {
      return AccessStatus.forValue(this._apiAccess.status);
    },

    seriesIds: function() {
      return this._apiAccess.series;
    },

    local: function(db) {
      return new AccessRelationsDB(this, db);
    }
  };

  var AccessRelationsDB = function(access, db) {
    this._access = access;
    this._db = db;
  };

  AccessRelationsDB.prototype = {
    allSeries: function() {
      return this._db.get(this._access.seriesIds(), {strict: true});
    },

    brokerage: function() {
      return this._db.get(this._access.brokerageId(), {strict: true});
    },

    offering: function() {
      return this._db.get(this._access.offeringId(), {strict: true});
    }
  };

  var AccessStatus = Enums.create({
    OPEN: {value: 'open', humanReadable: 'open'},
    CLOSED: {value: 'closed', humanReadable: 'closed'}
  });

  //
  // Series
  //
  var Series = function(apiSeries) {
    this._apiSeries = apiSeries;
  };

  Series.prototype = {
    id: function() {
      return this._apiSeries.id;
    },

    accessId: function() {
      return this._apiSeries.access;
    },

    name: function() {
      return this._apiSeries.name;
    },

    status: function() {
      return SeriesStatus.forValue(this._apiSeries.status.toLowerCase());
    },

    local: function(db) {
      return new SeriesRelationsDB(this, db);
    },

    feePercentage: function () {
      return this._apiSeries.fee_pct;
    },

    carryPercentage: function () {
      return this._apiSeries.carry_pct;
    },

    commissionPercentage: function () {
      return this._apiSeries.commission_pct;
    },

    placementFeePercentage: function () {
      return this._apiSeries.placement_fee_pct;
    },

    recommendedMinInvestment: function () {
      return MoneyAmounts.create(this._apiSeries.recommended_min_investment);
    },

    investmentPacketDocsIds: function () {
      return this._apiSeries.investment_packet_docs;
    },

    isClosed: function () {
      return (this._apiSeries.status === SeriesStatus.CLOSED.value());
    },

    isNotYetOpened: function () {
      return (this._apiSeries.status === SeriesStatus.NOT_YET_OPENED.value());
    },

    isOnHold: function () {
      return (this._apiSeries.status === SeriesStatus.ON_HOLD.value());
    },

    isOpen: function () {
      return (this._apiSeries.status === SeriesStatus.OPEN.value());
    }
  };

  var SeriesRelationsDB = function(series, db) {
    this._series = series;
    this._db = db;
  };

  SeriesRelationsDB.prototype = {
    access: function() {
      return this._db.get(this._series.accessId(), {strict: true});
    },

    offering: function() {
      return this.access().local(this._db).offering();
    },

    brokerage: function() {
      return this.access().local(this._db).brokerage();
    },

    security: function() {
      return this.offering().local(this._db).security();
    },

    company: function() {
      return this.security().local(this._db).company();
    },

    allInvestmentPacketDocs: function () {
      return this._db.get(this._series.investmentPacketDocsIds(), {strict: true});
    },

    allDocIds: function () {
      return _.map(this.allInvestmentPacketDocs(), function (packetDoc){
        return packetDoc.docId();
      });
    },

    allDocs: function () {
      return this._db.get(this.allDocIds(), {strict: true});
    }
 
  };

  var SeriesStatus = Enums.create({
    NOT_YET_OPENED: {value: 'not_yet_opened', humanReadable: 'not yet opened'},
    OPEN: {value: 'open', humanReadable: 'open'},
    ON_HOLD: {value: 'on_hold', humanReadable: 'on hold'},
    CLOSED: {value: 'closed', humanReadable: 'closed'}
  });

  //
  // InvestmentPacketDoc
  //
  var InvestmentPacketDoc = function(apiPacketDoc) {
    this._apiPacketDoc = apiPacketDoc;
  };

  InvestmentPacketDoc.prototype = {
    id: function() {
      return this._apiPacketDoc.id;
    },

    investorMustSign: function() {
      return this._apiPacketDoc.investor_must_sign;
    },

    firmMustSign: function() {
      return this._apiPacketDoc.firm_must_sign;
    },

    nonUSIndividualMustSign: function() {
      return this._apiPacketDoc.non_us_individual_must_sign;
    },

    nonUSOrganizationMustSign: function() {
      return this._apiPacketDoc.non_us_organization_must_sign;
    },

    sequenceNumber: function() {
      return this._apiPacketDoc.sequence_number;
    },

    series: function() {
      return this._apiPacketDoc.series;
    },

    USIndividualMustSign: function() {
      return this._apiPacketDoc.us_individual_must_sign;
    },

    USOrganizationMustSign: function() {
      return this._apiPacketDoc.us_organization_must_sign;
    },

    docId: function() {
      return this._apiPacketDoc.doc;
    },

    local: function (db) {
      return new InvestmentPacketDocRelationsDB(this, db);
    }
  };

  var InvestmentPacketDocRelationsDB = function (investmentPacketDoc, db) {
    this._investmentPacketDoc = investmentPacketDoc;
    this._db = db;
  };

  InvestmentPacketDocRelationsDB.prototype = {
    doc: function () {
      return this._db.get(this._investmentPacketDoc.docId(), {strict: true});
    }
  };

  var Deals = {
    offeringsForInvestment: function(config) {
      config = _.extend(config || {}, {
        apiParams: {
          q: 'investable'
        }
      });

      return Deals.getOfferings(config);
    },

    offeringsFromBrokerage: function(brokerageId, config) {
      config = _.extend(config || {}, {
        apiParams: {
          q: 'managed',
          brokerage: brokerageId
        }
      });

      return Deals.getOfferings(config);
    },

    getOfferings: function (config) {
      config = config || {};

      var apiParams = config.apiParams || {};
      var offeringsRequest = $http.get(api2.offerings(), {
        params: apiParams
      });
      return offeringsRequest.then(
        function offeringsGetSuccess(httpResponse) {
          var apiObjects = APIObjects.createOrUpdateDBWithResponse(httpResponse, config.db);
          var offerings = apiObjects.records;

          return offerings;
      });
    },

    getSeries: function(seriesId, config) {
      config = config || {};
      var db = config.db || ObjectDBs.temporary();

      var requestConfig = _.extend(config || {}, {
        apiParams: {
          q: 'series',
          series_id: seriesId
        },
        db: db
      });

      return Deals.getOfferings(requestConfig).then(function (offerings) {
        var allSeries = Deals.getAllSeriesFromOfferings(offerings, db);
        var series = _.filter(allSeries, function(series) {
          return series.id() === seriesId;
        });

        Utils.assert(series.length === 1, 'Number of series matching the requested ID was ' + series.length + ' when 1 was expected.');

        return series[0];
      });
    },

    getAllSeriesFromOfferings: function(offerings, db) {
      var allSeries = _.flatten(_.map(offerings, function(offering) {
        return offering.local(db).allSeries();
      }));

      return allSeries;
    },

    offering: function(offeringJson) {
      return new Offering(offeringJson);
    },

    security: function(securityJson) {
      return new Security(securityJson);
    },

    company: function(companyJson) {
      return new Company(companyJson);
    },

    access: function(accessJson) {
      return new Access(accessJson);
    },

    series: function(seriesJson) {
      return new Series(seriesJson);
    },

    investmentPacketDoc: function(apiPacketDoc) {
      return new InvestmentPacketDoc(apiPacketDoc);
    },

    SeriesStatus: SeriesStatus,

    AccessStatus: AccessStatus
  };

  return Deals;
});

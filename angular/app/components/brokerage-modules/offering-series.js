'use strict';

/**
 * Directive and controllers for display the details of a selected offering or an overview
 * of ALL offerings a brokerage admin can see
 *
 */
angular.module('myApp.directives')
.directive('offeringSeries',
function() {
  return {
    restrict: 'E',
    scope: {
      selectedOfferingId: '=',
      brokerage: '=',
      offeringsDb: '=',
      offerings: '='
    },
    controller: '_OfferingSeriesController',
    templateUrl: 'components/brokerage-modules/offering-series.html'
  };
})
.controller('_OfferingSeriesController',

function($scope, logging,  $modal, Deals, URLS, $http, api2, APIObjects, ObjectDBs) {
  var log = logging.namespace('_OfferingSeriesController');
  $scope.SeriesStatus = Deals.SeriesStatus;
  var SeriesFormTypes = {
    'add_new_offering_series': 'add_new_offering_series',
    'edit_series': 'edit_series',
    'create_investmentpacketdoc': 'create_investmentpacketdoc',
    'edit_investmentpacketdoc': 'edit_investmentpacketdoc'
  };

  $scope.$watch('offerings', function(newVal){
    if (newVal && $scope.selectedOfferingId) {
      _setCurrentOffering();
      _init();
    }
  });

  $scope.addNewSeries = function () {
    var config = {
      formContext:  $scope.currentOffering.accessIds()[0],
      formType: SeriesFormTypes.add_new_offering_series,
      title: 'Create New Series'
    };

    var dialogViewModel = new DialogViewModel(config);
    _openSeriesDialog(dialogViewModel);
  };


  var SeriesViewModel = function (series) {
    this._series = series;
    this._init();
  };

  SeriesViewModel.prototype = {
    _init: function () {
      this.allDocModels = this._setDocModels();
    },

    id: function () {
      return this._series.id();
    },

    name: function () {
      return this._series.name();
    },

    status: function () {
      return this._series.status();
    },

    isClosed: function () {
      return this._series.isClosed();
    },

    isNotYetOpened: function () {
      return this._series.isNotYetOpened();
    },

    investorViewLink: function () {
      var db = $scope.offeringsDb;
      var companySlug = this._series.local(db).company().slug();
      return URLS.FUNDRAISE(companySlug, this._series.id());
    },

    feePercentage: function() {
      return this._series.feePercentage();
    },

    carryPercentage: function() {
      return this._series.carryPercentage();
    },

    commissionPercentage: function() {
      return this._series.commissionPercentage();
    },

    placementFeePercentage: function() {
      return this._series.placementFeePercentage();
    },

    recommendedMinInvestment: function() {
      return this._series.recommendedMinInvestment();
    },

    investmentPacketDocs: function() {
      return this._series.investmentPacketDocs();
    },

    allInvestmentPacketDocs: function() {
      var docs = this._series.local($scope.offeringsDb).allInvestmentPacketDocs();
      docs = _.sortBy(docs, function(doc){
        return doc.local($scope.offeringsDb).doc().name().toLowerCase();
      });
      
      return docs;
    },

    allDocIds: function() {
      return this._series.local($scope.offeringsDb).allDocIds();
    },

    _setDocModels: function() {
      var docs = [];
      var packetDocs = this.allInvestmentPacketDocs();
      if (packetDocs.length > 0) {
        docs = _.map(this.allInvestmentPacketDocs(), function (packetDoc){
          return new SeriesDocViewModel(packetDoc);
        });
      }
      
      return docs;
    },

    edit: function () {
      var config = {
        formContext:  this.id(),
        formType: SeriesFormTypes.edit_series,
        title: 'Edit Series - ' + this.name()
      };

      var dialogViewModel = new DialogViewModel(config);
      _openSeriesDialog(dialogViewModel);
    },

    changeStatus: function (newStatus) {
      var seriesRequest = $http.patch(api2.series() + '/' + this.id()+'?status='+newStatus);

      return seriesRequest.then(
        function seriesGetSuccess(httpResponse) {
          Deals.offeringsFromBrokerage($scope.brokerage.id(), {db: $scope.offeringsDb}).then(function(offerings){
            $scope.offerings = offerings;
          });
      });
    },

    addNewDoc: function () {
      var config = {
        formContext: this.id(),
        formType: SeriesFormTypes.create_investmentpacketdoc
      };

      var dialogViewModel = new DialogViewModel(config);
      _openDocDialog(dialogViewModel);
    }
  }


  var SeriesDocViewModel = function (packetDoc) {
    this._packetDoc = packetDoc;
    this.confirmDeleteConfig = {};
    this._init();
  };

  SeriesDocViewModel.prototype = {
    _init: function () {
      var self = this;
      this.confirmDeleteConfig = {
        'title': 'Remove Document - ' + self.name(),
        'message': 'Are you sure you want to delete this document?',
        'buttonContent': 'X',
        'action': function () {
          return self._remove();
        }
      };
    },

    packetDocId: function () {
      return this._packetDoc.id();
    },

    series: function () {
      return this._packetDoc.series();
    },

    externalRef: function () {
      return this._packetDoc.local($scope.offeringsDb).doc().externalRef();
    },

    name: function () {
      return this._packetDoc.local($scope.offeringsDb).doc().name();
    },

    investorMustSign: function() {
      return this._packetDoc.investorMustSign();
    },

    firmMustSign: function() {
      return this._packetDoc.firmMustSign();
    },

    nonUSIndividualMustSign: function() {
      return this._packetDoc.nonUSIndividualMustSign();
    },

    nonUSOrganizationMustSign: function() {
      return this._packetDoc.nonUSOrganizationMustSign();
    },

    sequenceNumber: function() {
      return this._packetDoc.sequenceNumber();
    },

    USIndividualMustSign: function() {
      return this._packetDoc.USIndividualMustSign();
    },

    USOrganizationMustSign: function() {
      return this._packetDoc.USOrganizationMustSign();
    },

    docId: function() {
      return this._packetDoc.doc();
    },

    doc: function () {
      return this._packetDoc.local($scope.offeringsDb).doc();
    },

    type: function () {
      return this._packetDoc.local($scope.offeringsDb).doc().type();
    },

    _remove: function () {
      var removeRequest = $http.delete(api2.investmentPacketDocs() + '/' + this.packetDocId());

      return removeRequest.then(
        function removeSuccess(httpResponse) {
          Deals.offeringsFromBrokerage($scope.brokerage.id(), {db: $scope.offeringsDb}).then(function(offerings){
            var offering;

            if ($scope.selectedOfferingId) {
              offering = _.find(offerings, function(offering){
                return offering.id() === $scope.selectedOfferingId;
              });

              offering.local($scope.offeringsDb).allDocs().then(function(){
                $scope.offerings = offerings;
              });
              
            } else {
              $scope.offerings = offerings;
            }

          });
      });
    },

    edit: function () {
      var config = {
        formContext: this.packetDocId(),
        formType: SeriesFormTypes.edit_investmentpacketdoc
      };

      var dialogViewModel = new DialogViewModel(config);
      _openDocDialog(dialogViewModel);
    }
  }


  var DialogViewModel = function (config) {
    this._config = config;
  };

  DialogViewModel.prototype = {
    formContext: function () {
      return this._config.formContext;
    },

    formType: function () {
      return this._config.formType;
    },

    title: function () {
      return this._config.title;
    }
  }

  function _openConfirmDialog (viewModel) {
    var opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      scope: $scope,
      controller: 'ConfirmModalController',
      templateUrl : 'components/brokerage-modules/confirm-modal.html',
      resolve: {
        'dialogViewModel': function () {
          return viewModel;
        }
      }
    };

    var confirmModal = $modal.open(opts);
    confirmModal.result.then(function() {});
  }  

  function _openDocDialog (viewModel) {
    var opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      scope: $scope,
      controller: 'OfferingSeriesDocModalController',
      templateUrl : 'components/brokerage-modules/offering-series-doc-dialog.html',
      resolve: {
        'dialogViewModel': function () {
          return viewModel;
        },

        'SeriesFormTypes': function () {
          return SeriesFormTypes
        }
      }
    };

    var docModal = $modal.open(opts);

    docModal.result.then(function() {
      Deals.offeringsFromBrokerage($scope.brokerage.id(), {db: $scope.offeringsDb}).then(function(offerings){
        var offering;

        if ($scope.selectedOfferingId) {
          offering = _.find(offerings, function(offering){
            return offering.id() === $scope.selectedOfferingId;
          });

          offering.local($scope.offeringsDb).allDocs().then(function(){
            $scope.offerings = offerings;
          });
          
        } else {
          $scope.offerings = offerings;
        }

      });      
    });
  }

  function _openSeriesDialog (viewModel) {
    var opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      scope: $scope,
      controller: 'OfferingSeriesModalController',
      templateUrl : 'components/brokerage-modules/offering-series-dialog.html',
      resolve: {
        'dialogViewModel': function () {
          return viewModel;
        },

        'SeriesFormTypess' : function () {
          return SeriesFormTypes
        }
      }
    };

    var seriesModal = $modal.open(opts);

    seriesModal.result.then(function() {
      Deals.offeringsFromBrokerage($scope.brokerage.id(), {db: $scope.offeringsDb}).then(function(offerings){
        $scope.offerings = offerings;
      });
    });
  }

  function _setCurrentOffering () {
    if (!$scope.currentOffering) {
      $scope.currentOffering = _.find($scope.offerings, function(currOffering) {
        return (currOffering.id() === $scope.selectedOfferingId);
      });
    }
  }

  function _init () {
    var allSeries = $scope.currentOffering.local($scope.offeringsDb).allSeriesInReverseIdOrder();

    $scope.seriesViewModels = _.map(allSeries, function(series) {
      return new SeriesViewModel(series);
    });
  }
});

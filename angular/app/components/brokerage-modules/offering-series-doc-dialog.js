'use strict';

angular.module('myApp.controllers')
.controller('OfferingSeriesDocModalController',
function($scope, $modalInstance, Deals, logging, $timeout, dialogViewModel, Docs, SeriesFormTypes) {
  var log = logging.namespace('OfferingSeriesDocModalController');
  $scope.dialogViewModel = dialogViewModel;
  $scope.formData = {};

  function _mapData () {
    $scope.dialogViewModel = angular.extend(
      $scope.dialogViewModel,
      {
        DocsStatus: Docs.Status,
        DocsType: Docs.Type
      }
    );
  }

  $scope.submit = function() {
    log('Saving changes yo!');
    
    $scope.offeringSeriesDocForm.$cvcForm.$submit().then(function(data) {
      log('Successfully added series');
      $modalInstance.close(data);
    }).catch(function() {
      $modalInstance.dismiss('uh oh, the data didn\'t submit Successfully');
    });
    
  };

  $scope.isEditingDocument = function () {
    return $scope.dialogViewModel.formType() === SeriesFormTypes.edit_investmentpacketdoc;
  };

  $scope.isAddingDocument = function () {
    return $scope.dialogViewModel.formType() === SeriesFormTypes.create_investmentpacketdoc;
  };

  $scope.dismissChanges = function() {
    $scope.offeringSeriesDocForm.$cvcForm.$dismissChanges().then(function() {
      $modalInstance.dismiss('Canceled');
    });
  };

  $scope.isFormEnabled = function() {
    var isSubmitting = $scope.offeringSeriesDocForm.$cvcForm.$submitting;
    return !isSubmitting;
  };

  $scope.isSubmitEnabled = function() {
    return $scope.isFormEnabled() && $scope.offeringSeriesDocForm.$cvcForm.$isReadyForSubmit();
  };

  $scope.fileExists = function () {
    return ($scope.formData && $scope.formData.file);
  }

  _mapData();

});

'use strict';

/**
 * Back Button!
 */
angular.module('myApp.directives').
directive('backButton', function($window) {
  return {
    restrict: 'A',
    link: function (scope, elem) {
      elem.bind('click', function () {
        $window.history.back();
      });
    }
  };
});

